<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036800343</ID>
<ANCIEN_ID>JG_L_2018_04_000000401767</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/80/03/CETATEXT000036800343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 13/04/2018, 401767</TITRE>
<DATE_DEC>2018-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401767</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401767.20180413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 23 janvier 2014 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, après avoir annulé la décision de l'inspecteur du travail du 22 mai 2013, a autorisé la société normande d'études et de réalisation à le licencier. Par un jugement n° 1400377 du 20 novembre 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NT00139 du 24 mai 2016, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 juillet et 26 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société normande d'études et de réalisation la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M. B...et à la SCP Foussard, Froger, avocat de la société Normande d'études et de réalisation ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, sur recours hiérarchique de M.B..., salarié protégé, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, par une décision du 23 janvier 2014, annulé la décision du 22 mai 2013 de l'inspecteur du travail du Calvados autorisant la société normande d'études et de réalisation à le licencier et autorisé cette société à le licencier ; que M. B... se pourvoit en cassation contre l'arrêt du 24 mai 2016 par lequel la cour administrative d'appel de Nantes a rejeté son appel dirigé contre le jugement du 20 novembre 2014 par lequel le tribunal administratif de Caen a rejeté sa demande d'annulation de la décision du 23 janvier 2014 ; <br/>
<br/>
              2. Considérant que, lorsqu'il est saisi, sur le fondement des dispositions de l'article R. 2422-1 du code du travail, d'un recours hiérarchique contre une décision d'un inspecteur du travail ayant statué sur une demande d'autorisation de licenciement, le ministre chargé du travail doit, soit confirmer cette décision, soit, si celle-ci est illégale, l'annuler, puis se prononcer de nouveau sur la demande d'autorisation de licenciement, compte tenu des circonstances de droit et de fait à la date à laquelle il prend sa propre décision ;<br/>
<br/>
              3. Considérant que, lorsque le motif de licenciement invoqué par l'employeur fait obligation à l'administration d'apprécier le sérieux des recherches préalables de reclassement effectuées par celui-ci, l'inspecteur du travail doit apprécier les possibilités de reclassement du salarié à compter du moment où le licenciement est envisagé et jusqu'à la date à laquelle il statue sur la demande de l'employeur ; qu'en vertu de la règle rappelée au point précédent, le ministre saisi d'un recours hiérarchique doit, lorsqu'il statue sur la légalité de la décision de l'inspecteur du travail, apprécier le sérieux des recherches de reclassement jusqu'à la date de cette décision ; que si le ministre annule la décision de l'inspecteur du travail et se prononce de nouveau sur la demande d'autorisation de licenciement, il doit alors, en principe, apprécier le sérieux des recherches de reclassement jusqu'à la date à laquelle il statue ;<br/>
<br/>
              4. Considérant cependant que, dans ce dernier cas, si l'inspecteur du travail a autorisé le licenciement demandé et que le salarié a été licencié par l'employeur avant que le ministre ne se prononce sur son recours hiérarchique ou sur le recours formé en son nom, il n'y a lieu, pour le ministre qui a annulé la décision de l'inspecteur du travail, d'apprécier les possibilités de reclassement du salarié que jusqu'à la date de son licenciement ; qu'à cette fin, le ministre doit prendre en compte l'ensemble des éléments portés à sa connaissance, y compris ceux qui, bien que postérieurs à la date du licenciement, sont de nature à éclairer l'appréciation à porter sur le sérieux de la recherche de reclassement jusqu'à cette date ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant que l'obligation de recherche de reclassement de l'employeur avait pris fin à la date du licenciement du salarié et que le ministre avait, de ce fait, pu légalement apprécier la réalité des recherches de reclassement à la date du licenciement de M.B..., la cour administrative d'appel de Nantes, devant laquelle il n'était pas soutenu que des éléments postérieurs au licenciement de M. B...étaient de nature à éclairer le sérieux de la recherche de reclassement jusqu'à cette date, n'a pas entaché son arrêt d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que ses conclusions doivent être rejetées, y compris, par voie de conséquence, celles qu'il présente au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la société normande d'études et de réalisation ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté. <br/>
Article 2 : Les conclusions présentées par la société normande d'études et de réalisation sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à la société normande d'études et de réalisation.<br/>
Copie en sera transmise à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-03-04 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. RECOURS HIÉRARCHIQUE. - MOTIF DE LICENCIEMENT FAISANT OBLIGATION À L'ADMINISTRATION D'APPRÉCIER LE SÉRIEUX DES RECHERCHES PRÉALABLES DE RECLASSEMENT DU SALARIÉ EFFECTUÉES PAR L'EMPLOYEUR - OBLIGATIONS DU MINISTRE DANS L'HYPOTHÈSE D'UNE NOUVELLE DÉCISION SUR LA DEMANDE DE LICENCIEMENT APRÈS ANNULATION DE LA DÉCISION DE L'INSPECTEUR DU TRAVAIL - 1) PRINCIPE - APPRÉCIATION DU SÉRIEUX DES RECHERCHES DE RECLASSEMENT JUSQU'À LA DATE À LAQUELLE IL STATUE [RJ1] - 2) CAS OÙ LE LICENCIEMENT EST DÉJÀ INTERVENU - A) APPRÉCIATION DU SÉRIEUX DES RECHERCHES DE RECLASSEMENT JUSQU'À LA DATE DU LICENCIEMENT [RJ2] - B) ELÉMENTS À PRENDRE EN COMPTE.
</SCT>
<ANA ID="9A"> 66-07-01-03-04 Recours hiérarchique, présenté sur le fondement de l'article R. 2422-1 du code du travail, contre une décision d'un inspecteur du travail ayant statué sur une demande d'autorisation de licenciement.... ,,1) Lorsque le motif de licenciement invoqué par l'employeur fait obligation à l'administration d'apprécier le sérieux des recherches préalables de reclassement effectuées par celui-ci, l'inspecteur du travail doit apprécier les possibilités de reclassement du salarié à compter du moment où le licenciement est envisagé et jusqu'à la date à laquelle il statue sur la demande de l'employeur. Le ministre saisi d'un recours hiérarchique doit, lorsqu'il statue sur la légalité de la décision de l'inspecteur du travail, apprécier le sérieux des recherches de reclassement jusqu'à la date de cette décision. Si le ministre annule la décision de l'inspecteur du travail et se prononce de nouveau sur la demande d'autorisation de licenciement, il doit alors, en principe, apprécier le sérieux des recherches de reclassement jusqu'à la date à laquelle il statue.,,,2) a) Cependant, dans ce dernier cas, si l'inspecteur du travail a autorisé le licenciement demandé et que le salarié a été licencié par l'employeur avant que le ministre ne se prononce sur son recours hiérarchique ou sur le recours formé en son nom, il n'y a lieu, pour le ministre qui a annulé la décision de l'inspecteur du travail, d'apprécier les possibilités de reclassement du salarié que jusqu'à la date de son licenciement.... ,,b) A cette fin, le ministre doit prendre en compte l'ensemble des éléments portés à sa connaissance, y compris ceux qui, bien que postérieurs à la date du licenciement, sont de nature à éclairer l'appréciation à porter sur le sérieux de la recherche de reclassement jusqu'à cette date.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 5 septembre 2008, Société Sorelait, n° 303992, p. 319.,,[RJ2] Rappr. Cass. soc., 30 mars 1999, n° 97-41.265, Bull. 1999 V. n° 146 ; Cass. soc., 1er juin 2010, n° 09-40421, Bull. 2010 V, n° 121.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
