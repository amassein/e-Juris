<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043935104</ID>
<ANCIEN_ID>JG_L_2021_07_000000454892</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/93/51/CETATEXT000043935104.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 29/07/2021, 454892, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454892</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:454892.20210729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Par une requête et un mémoire complémentaire, enregistrés les 23 et 25 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... D..., M. E... A... et M. C... D... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution du f du II de l'article 1er du décret n° 2021-955 du 19 juillet 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire.<br/>
<br/>
<br/>
               Ils soutiennent que :<br/>
               - la condition d'urgence est satisfaite dès lors que les dispositions contestées entrent en vigueur à partir du 21 juillet 2021 et pour une durée indéterminée ; <br/>
               - l'acte attaqué porte une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ;<br/>
               - le décret contesté méconnaît l'article 4 de la déclaration des droits de l'homme et du citoyen et le onzième alinéa du Préambule de la Constitution du 27 octobre 1946 dès lors que, d'une part, la mesure apparaît disproportionnée à l'objectif poursuivi en ce que le risque de transmission du virus entre joueurs présents sur un terrain de golf, hors manifestations exceptionnelles ou compétitions, est limité, voire nul et, d'autre part, en imposant le " passe sanitaire " à un joueur de golf, quand bien même il joue seul et observe une distanciation physique de plusieurs centaines de mètres, le décret ne tend pas à limiter la propagation du virus et par conséquent elle n'a pas une finalité préventive mais punitive ;<br/>
               - ce décret porte une atteinte disproportionnée à la liberté d'aller et venir et au droit de pratiquer un loisir dès lors que, en premier lieu, ils ne peuvent fournir une preuve de guérison de la Covid-19 de moins de six mois et devront présenter un test PCR ou antigénique négatif de moins de quarante-huit heures pour accéder aux terrains de golf, en deuxième lieu, la réalisation des tests implique une anticipation et des contraintes souvent incompatibles avec leur activité professionnelle, en troisième lieu, la disponibilité des laboratoires ou pharmacies peut ne pas coïncider avec leurs emplois du temps professionnels et, en dernier lieu, la programmation d'un test avec quarante-huit heures d'anticipation empêchera la plupart de leurs déplacements sur des terrains de golf.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
               Vu : <br/>
               - la loi n° 2021-689 du 31 mai 2021 ; <br/>
               - le décret n° 2021-699 du 1 er juin 2021 ;<br/>
               - le décret n°2021-724 du 7 juin 2021 ; <br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En outre, en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
               Sur les circonstances et le cadre du litige : <br/>
<br/>
               2. En raison de l'amélioration progressive de la situation sanitaire, les mesures de santé publique destinées à prévenir la circulation du virus de la Covid 19 prises dans le cadre de l'état d'urgence sanitaire ont été remplacées, après l'expiration de celui-ci le 1 er juin 2021, par celles de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire et du décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de la crise sanitaire. <br/>
<br/>
               3. En vertu du A du II de l'article 1er de la loi du 31 mai 2021, le Premier ministre peut, à compter du 2 juin 2021 et jusqu'au 30 septembre 2021, imposer la présentation du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la Covid 19, un justificatif de statut vaccinal concernant la Covid 19 ou un certificat de rétablissement à la suite d'une contamination par la Covid 19, d'une part, aux personnes souhaitant se déplacer à destination ou en provenance du territoire hexagonal, de la Corse ou de l'une des collectivités mentionnées à l'article 72-3 de la Constitution et, d'autre part, aux personnes souhaitant accéder à certains lieux, établissements ou événements impliquant de grands rassemblements de personnes pour des activités de loisirs ou des foires ou salons professionnels. <br/>
<br/>
               4. Le décret du 7 juin 2021 a modifié le décret du 1er juin 2021 pris en application de cette loi, afin d'imposer la présentation du passe sanitaire pour les déplacements mentionnés au titre 2 bis de ce décret et pour l'accès aux établissements, lieux et évènements mentionnés au chapitre 7 de son titre 4 et d'en déterminer les modalités d'utilisation. Pour les établissements, lieux et évènements énumérés au II de l'article 47-1 issu de ce décret, l'accès des personnes âgées de onze ans et plus est subordonné, lorsque ces établissements, lieux et évènements accueillent un nombre de visiteurs ou de spectateurs au moins égal à mille personnes, à la présentation de l'un des documents énumérés au I du même article, à savoir " 1° Le résultat d'un test ou examen de dépistage mentionné au 1° de l'article 2-2 réalisé moins de 48 heures avant l'accès à l'établissement, au lieu ou à l'évènement. Les seuls tests antigéniques pouvant être valablement présentés pour l'application du présent 1° sont ceux permettant la détection de la protéine N du SARS-CoV-2 ; 2° Un justificatif du statut vaccinal délivré dans les conditions mentionnées au 2° de l'article 2-2 ; 3° Un certificat de rétablissement délivré dans les conditions mentionnées au 3° de l'article 2-2 /... ". Le dernier alinéa du I de l'article 47-1 précise qu'à défaut de présentation de l'un de ces documents, l'accès à l'établissement, au lieu ou à l'évènement est refusé. <br/>
<br/>
               5. Le décret litigieux du 19 juillet 2021 a modifié les dispositions de l'article 47-1 du décret du 1er juin 2021 afin d'étendre à plusieurs nouvelles catégories d'établissements l'obligation de présenter les documents énumérés au I du même article, faute de quoi l'accès à ces établissements est refusé, et d'abaisser le seuil d'application de cette limitation de l'accès à l'accueil de 50 personnes. <br/>
<br/>
               Sur la demande en référé :<br/>
<br/>
               6. Les requérants demandent la suspension du décret du 19 juillet 2021 en tant qu'il a étendu le passe sanitaire aux établissements de plein air, relevant du type PA, en particulier les terrains de golf. Ils font valoir que la mise en œuvre du décret attaqué porte une atteinte grave et manifestement illégale notamment à la liberté d'aller et venir et au droit de pratiquer un loisir dès lors qu'il leur est interdit d'accéder à ces lieux sans la présentation du " passe sanitaire ".<br/>
<br/>
               7. Il résulte toutefois des données scientifiques disponibles qu'à la date du 21 juillet 2021 la situation sanitaire s'est de nouveau dégradée en raison de la diffusion croissante du variant Delta du virus de la Covid 19 sur le territoire, avec près de 80,2 % des tests révélant sa présence, et que la transmissibilité de ce virus est augmentée de 60 % par rapport au variant Alpha. A la date du décret litigieux, le taux d'incidence était marqué par une forte augmentation de la circulation du virus (+ 111 % sur la période du 11 au 17 juillet 2021 par rapport à la période du 4 au 10 juillet, + 244 % par rapport à la période du 27 juin au 3 juillet). Au 21 juillet 2021 le taux d'incidence était de 98,2 pour 100 000 habitants, soit + 143 % par rapport à la semaine du 5 au 11 juillet. Le nombre des entrées à l'hôpital et des admissions en services de soins critiques a augmenté de respectivement 57 % et 67 % pour la semaine du 14 au 21 juillet par rapport à la semaine précédente. Ces données, qui montrent une dégradation de la situation sanitaire au cours de la période très récente, pourraient se révéler encore plus préoccupantes au début du mois d'août, selon les modélisations de l'Institut Pasteur, dans le contexte de diffusion de ce variant, ceci alors que la couverture vaccinale de la population dont, au 20 juillet 2021, seule 46,4 % avait reçu un schéma vaccinal complet, n'est pas suffisante pour conduire à un reflux durable de l'épidémie. <br/>
<br/>
               8. Il résulte par ailleurs de l'instruction que doit intervenir à très court terme une modification de la loi du 31 mai 2021 afin notamment de redéfinir le périmètre des lieux, établissements, services ou évènements dont l'accès est subordonné à la présentation d'un passe sanitaire et de supprimer la limitation de l'utilisation du passe sanitaire aux seuls grands rassemblements ayant lieu dans ces lieux, établissements ou évènements. Il en résulte que les mesures prévues par le décret n'auront qu'une application brève.<br/>
<br/>
               9. Dans ces conditions, compte tenu du caractère très évolutif de cette situation, avec un risque d'augmentation de l'épidémie à court terme, et alors que doit intervenir à très court terme une modification de la loi du 31 mai 2021 rendant caduque l'application du décret litigieux, le décret attaqué ne porte pas, à la date de la présente ordonnance, une atteinte grave et manifestement illégale aux libertés dont se prévalent les requérants. <br/>
<br/>
               10. Il résulte de tout ce qui précède que la requête doit être rejetée selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme D... et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... D..., première requérante dénommée. <br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
