<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042737146</ID>
<ANCIEN_ID>JG_L_2020_12_000000428800</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/73/71/CETATEXT000042737146.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 23/12/2020, 428800, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428800</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428800.20201223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... C... et Mme B... A... ont demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir les décisions des 17 avril, 9 septembre et 5 octobre 2015 par lesquelles la maire de Paris a refusé de leur reconnaître le bénéfice d'une autorisation de changement d'usage d'un local dont ils sont propriétaires. Par un jugement n° 1518197 du 24 novembre 2016, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 17PA00002 du 17 janvier 2019, la cour administrative d'appel de Paris a rejeté l'appel formé par M. C... et Mme A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 13 mars et 28 mai 2019 et le 23 octobre 2020, M. C... et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 86-1290 du 23 décembre 1986 ;<br/>
              - l'ordonnance n° 2005-655 du 8 juin 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. C... et de Mme A... et à la SCP Foussard, Froger, avocat de la ville de Paris.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... et M. C... sont propriétaires indivis d'un appartement à usage d'habitation d'une superficie de 105 m² situé rue Bonaparte à Paris. Par une décision du 10 mars 1983 prise en application des dispositions de l'article L. 631-7 du code de la construction et de l'habitation, le Préfet de police a autorisé Mme A... à y exercer sa profession de chirurgien-dentiste, conjointement avec un praticien exerçant, dans le même appartement, la profession de médecin. Après le départ en retraite de l'intéressée, Mme A... et M. C... ont demandé au maire de Paris de confirmer que l'usage professionnel du local pouvait se poursuivre pour deux autres professionnels de santé. Par deux courriers des 9 décembre 2014 et 17 avril 2015, la maire de Paris a toutefois indiqué, respectivement, à Mme A... et M. C..., que l'autorisation du 10 mars 1983 avait été accordée à Mme A... à titre personnel et qu'en conséquence, les deux nouveaux professionnels occupant l'appartement devaient solliciter la régularisation de leur situation, sous peine de poursuites pénales. Mme A... et M. C... ont demandé l'annulation de ces décisions ainsi que de la décision du 5 octobre 2015 les confirmant sur recours gracieux, d'abord devant le tribunal administratif de Paris, qui a rejeté leurs demandes par un jugement du 24 novembre 2016, puis devant la cour administrative d'appel de Paris, qui a rejeté leur requête par l'arrêt du 17 janvier 2019 contre lequel ils se pourvoient en cassation.<br/>
<br/>
              2. Aux termes de l'article L. 631-7 du code de la construction et de l'habitation dans sa rédaction en vigueur jusqu'au 24 décembre 1986 : " Dans les communes définies à l'article 10-7 de la loi n° 48-1360 du 1er  septembre 1948 modifiée : / 1. Les locaux à usage d'habitation ne peuvent être ni affectés à un autre usage, ni transformés en meublés, hôtels, pensions de famille ou autres établissements similaires (...) / Il ne peut être dérogé à ces interdictions que par autorisation administrative préalable et motivée, après avis du maire. / Le préfet peut autoriser l'exercice, sous certaines conditions, dans une partie d'un local d'habitation, d'une profession qui ne puisse à aucun moment revêtir un caractère commercial si ce local constitue en même temps la résidence du demandeur (...) ". La loi du 23 décembre 1986 tendant à favoriser l'investissement locatif, l'accession à la propriété de logements sociaux et le développement de l'offre foncière a ajouté à cet article les deux alinéas suivants : " Ces dérogations et autorisations sont accordées à titre personnel. Cependant, les bénéficiaires membres d'une profession libérale réglementée, qui rendent à l'habitation le local qui était devenu totalement ou partiellement professionnel, peuvent être autorisés à transformer un autre local d'habitation en local professionnel pour une surface équivalente. / La dérogation et l'autorisation cessent de produire effet lorsqu'il est mis fin, à titre définitif, pour quelque raison que ce soit, à l'exercice professionnel du bénéficiaire ". Il résulte toutefois des termes, de l'objet et de l'économie générale des dispositions de la loi du 1er septembre 1948, dont est issu l'article L. 631-7 du code de la construction et de l'habitation, que les dérogations et autorisations de changement d'affectation étaient attachées à la personne et non au local, avant même l'adoption des dispositions de la loi du 23 décembre 1986 citées ci-dessus. <br/>
<br/>
              3. En premier lieu, en jugeant que l'autorisation du 10 mars 1983 avait, malgré l'ambiguïté de certains de ses termes, été accordée conformément au principe rappelé ci-dessus et qu'ainsi elle ne constituait pas une décision attachée au local qu'elle désignait, mais une autorisation accordée à titre personnel à Mme A..., en sa qualité de membre d'une profession libérale, la cour administrative d'appel a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En second lieu, les articles 24 à 28 de l'ordonnance du 8 juin 2005 relative au logement et à la construction, entrés en vigueur le 10 juin 2005, ont réformé la réglementation relative au changement d'usage des locaux d'habitation, notamment pour prévoir, à l'article L. 631-7-1 du code de la construction et de l'habitation que l'autorisation de changement d'usage, en principe délivrée à titre personnel, peut " être subordonnée à une compensation, sous la forme de la transformation concomitante en habitation de locaux ayant un autre usage " et que, " lorsque l'autorisation est subordonnée à une compensation, le titre est attaché au local et non à la personne ". L'article 29 de la même ordonnance, relatif aux dispositions transitoires, comporte un II qui dispose : " Les autorisations définitives accordées sur le fondement du même article L. 631-7 avant l'entrée en vigueur de la présente ordonnance et qui ont donné lieu à compensation effective, sont attachées, à compter de cette entrée en vigueur, au local et non à la personne ". En estimant que l'engagement pris par Mme A..., en 1983, de rendre à l'habitation la superficie de 39 m² qu'elle utilisait pour un usage professionnel dans un autre appartement de 180 m2 dans lequel elle résidait, rue des Ecoles, ne pouvait être regardé comme une " compensation effective " au sens de ces dispositions, la cour a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation. Elle a pu, par suite, sans erreur de droit, en déduire que cet engagement ne conférait pas un caractère réel à l'autorisation préfectorale du 10 mars 1983.<br/>
<br/>
              5. Il résulte de tout ce qui précède que M. C... et Mme A... ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la ville de Paris qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des requérants la somme de 3 000 euros que demande, à ce titre, la ville de Paris.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. C... et Mme A... est rejeté.<br/>
<br/>
Article 2 : M. C... et Mme A... verseront solidairement à la ville de Paris une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C..., premier requérant dénommé, à la ville de Paris et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
