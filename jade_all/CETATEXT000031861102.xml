<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861102</ID>
<ANCIEN_ID>JG_L_2015_12_000000369311</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861102.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 30/12/2015, 369311, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369311</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:369311.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Euro Park Service, venant aux droits et obligations de la société Cairnbulg Nanteuil, a demandé au tribunal administratif de Paris de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle sur cet impôt, ainsi que des pénalités correspondantes auxquelles elle a été assujettie au titre de l'exercice clos le 26 novembre 2004. Par un jugement n° 0901424 du 6 juillet 2011, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11PA03449 du 11 avril 2013, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Euro Park Service contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 juin 2013, 12 septembre 2013 et 14 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Euro Park Service demande au Conseil d'Etat :<br/>
<br/>
              1°) - à titre principal, d'annuler l'arrêt n° 11PA03449 du 11 avril 2013 de la cour administrative d'appel de Paris ;<br/>
<br/>
              - à titre subsidiaire, de renvoyer à la Cour de justice de l'Union européenne la question de savoir si la taxation immédiate en France des plus-values mobilières latentes enregistrées par une société dissoute à l'occasion d'une opération de fusion par transmission universelle de son patrimoine à une société d'un autre Etat membre contrevient aux principes énoncés à l'article 49 du traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité instituant la Communauté européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive n° 90/434/CEE du Conseil, du 23 juillet 1990 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de la société Euro Park Service, venant aux droits et obligations de la société Cairnbulg Nanteuil ;<br/>
<br/>
<br/>
<br/>1. Considérant  qu'aux termes de l'article 210 A du code général des impôts : " 1. Les plus-values nettes et les profits dégagés sur l'ensemble des éléments d'actif apportés du fait d'une fusion ne sont pas soumis à l'impôt sur les sociétés (...) 3. L'application de ces dispositions est subordonnée à la condition que la société absorbante s'engage, dans l'acte de fusion, à respecter les prescriptions suivantes : (...) b. elle doit se substituer à la société absorbée pour la réintégration des résultats dont la prise en compte avait été différée pour l'imposition de cette dernière ; c. elle doit calculer les plus-values réalisées ultérieurement à l'occasion de la cession des immobilisations non amortissables qui lui sont apportées d'après la valeur qu'elles avaient, du point de vue fiscal, dans les écritures de la société absorbée ; d. elle doit réintégrer dans ses bénéfices imposables les plus-values dégagées lors de l'apport des biens amortissables (...) " ; qu'aux termes de l'article 210 C du même code : " 1. Les dispositions des articles 210 A et 210 B s'appliquent aux opérations auxquelles participent exclusivement des personnes morales ou organismes passibles de l'impôt sur les sociétés. 2. Ces dispositions ne sont applicables aux apports faits à des personnes morales étrangères par des personnes morales françaises que si ces apports ont été préalablement agréés dans les conditions prévues au 3 de l'article 210 B (...)." et qu'aux termes du 3 de l'article 210 B de ce code : " (...) L'agrément est délivré lorsque, compte tenu des éléments faisant l'objet de l'apport : a. l'opération est justifiée par un motif économique, se traduisant notamment par l'exercice par la société bénéficiaire de l'apport d'une activité autonome ou l'amélioration des structures, ainsi que par une association entre les parties ; b. l'opération n'a pas comme objectif principal ou comme un de ses objectifs principaux la fraude ou l'évasion fiscales ; c. les modalités de l'opération permettent d'assurer l'imposition future des plus-values mises en sursis d'imposition " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SCI Cairnbulg Nanteuil a fait l'objet, le 26 novembre 2004, d'une opération de dissolution sans liquidation en application de l'article 1844-5 du code civil, de la part et au profit de son associé unique, la société de droit luxembourgeois Euro Park Service ; qu'à cette occasion, elle a opté, dans la déclaration de résultats qu'elle a souscrite, le 25 janvier 2005, au titre de l'exercice clos le 26 novembre 2004, pour le régime spécial des fusions prévu aux articles 210 A et suivants du code général des impôt ; qu'elle n'a en conséquence pas soumis à l'impôt sur les sociétés les plus-values nettes et les profits dégagés sur les actifs dont elle avait fait apport à la société Euro Park Service ; que ces apports, constitués de biens immobiliers, ont été évalués à leur valeur nette comptable, soit 9 387 700 euros, dans l'acte notarié du 19 avril 2005 constatant la transmission universelle du patrimoine de la SCI Cairnbulg Nanteuil à la société Euro Park Service ; que cette dernière a cédé le même jour ces mêmes biens immobiliers à la SCI IBC Ferrier au prix de 15 776 000 euros, correspondant à la valeur vénale que ces éléments d'actif avaient acquise au 26 novembre 2004 ;<br/>
<br/>
              3. Considérant qu'à la suite d'un contrôle, l'administration fiscale a remis en cause le bénéfice du régime spécial des fusions prévu à l'article 210 A du code général des impôts aux motifs, d'une part, que la SCI Cairnbulg Nanteuil n'avait pas sollicité l'agrément ministériel prévu par le 2 de l'article 210 C du code général des impôts en cas d'apports effectués au profit d'une  société étrangère et, d'autre part, que cet agrément ne lui aurait, en toute hypothèse, pas été accordé dès lors que sa dissolution n'était pas justifiée par une raison économique mais poursuivait un but de fraude ou d'évasion fiscale ; qu'en conséquence, des suppléments d'impôts sur les sociétés et de contributions à l'impôt sur les sociétés, assortis des pénalités prévues par l'article 1729 du code général des impôts en cas de manquement délibéré, ont été mis à la charge de la société Euro Park Service venant aux droits de la SCI Cairnbulg Nanteuil ; que, par un jugement du 6 juillet 2011, le tribunal administratif de Paris a rejeté la demande de la société Euro Park Service tendant à la décharge de ces impositions et pénalités ; que la société se pourvoit en cassation contre l'arrêt du 11 avril 2013 par lequel la cour administrative d'appel de Paris a confirmé ce jugement ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article 43 du traité instituant la Communauté européenne, repris à l'article 49 du traité sur le fonctionnement de l'Union européenne : " (...) les restrictions à la liberté d'établissement des ressortissants d'un Etat membre dans le territoire d'un autre Etat membre sont interdites. Cette interdiction s'étend également aux restrictions à la création d'agences, de succursales ou de filiales, par les ressortissants d'un Etat membre établis sur le territoire d'un Etat membre " ; qu'aux termes du premier alinéa de l'article 48 du même traité, repris à l'article 54 du traité sur le fonctionnement de l'Union européenne : " Les sociétés constituées en conformité de la législation d'un État membre et ayant leur siège statutaire, leur administration centrale ou leur principal établissement à l'intérieur de l'Union sont assimilées, pour l'application des dispositions du présent chapitre, aux personnes physiques ressortissantes des États membres " ; qu'à l'appui de son pourvoi, la société Euro Park Service soutient notamment que la cour a commis une erreur de droit en écartant le moyen tiré de l'incompatibilité de l'article 210 C du code général des impôts avec les stipulations précitées de l'article 43 du traité de Rome, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne, alors qu'en soumettant à une procédure d'agrément préalable les seuls apports faits à des personnes morales étrangères à l'exclusion des apports faits à des personnes morales françaises, le 2 de cet article institue une restriction injustifiée au principe de la liberté d'établissement ; <br/>
<br/>
              5. Considérant que les dispositions de l'article 210 C du code général des impôts assurent la transposition en droit interne de la directive du 23 juillet 1990 modifiée concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents ; qu'aux termes de l'article 4 du titre II de cette directive : " 1. La fusion ou la scission n'entraîne aucune imposition des plus-values qui sont déterminées par différence entre la valeur réelle des éléments d 'actif et de passif transférés et leur valeur fiscale (...) " ; que l'article 11 prévoit toutefois que : " 1.Un Etat membre peut refuser d'appliquer tout ou partie des dispositions des titres II, III et IV ou en retirer le bénéfice lorsque l'opération de fusion, de scission, d'apports d'actif ou d'échange d'actions : a) a comme objectif principal ou comme un de ses objectifs principaux la fraude ou l'évasion fiscales ; le fait qu'une des opérations visées à l'article 1er n'est pas effectuée pour des motifs économiques valables, tels que la restructuration ou la rationalisation des activités des sociétés participant à l'opération, peut constituer une présomption que cette opération a comme objectif principal ou comme un de ses objectifs principaux la fraude ou l'évasion fiscales (...) " ; <br/>
<br/>
              6. Considérant que la réponse au moyen soulevé par la société requérante et rappelé au point 4 ci-dessus dépend des questions de savoir, en premier lieu, si lorsqu'un Etat membre fait usage de la faculté offerte par le 1 de l'article 11 précité de la directive du 23 juillet 1990, il y a place pour un contrôle des actes pris pour la mise en oeuvre de cette faculté au regard du droit primaire de l'Union européenne et, en second lieu, en cas de réponse positive à la première question, si les stipulations de l'article 43 du traité instituant la Communauté européenne, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne, doivent être interprétées comme faisant obstacle à ce qu'une législation nationale, dans un but de lutte contre la fraude ou l'évasion fiscales, subordonne le bénéfice du régime fiscal commun applicable aux fusions et opérations assimilées à une procédure d'agrément préalable en ce qui concerne les seuls apports faits à des personnes morales étrangères, à l'exclusion des apports faits à des personnes morales de droit national ;<br/>
<br/>
              7. Considérant que ces questions sont déterminantes pour la solution du litige que doit trancher le Conseil d'Etat ; qu'elles présentent une difficulté sérieuse ; qu'il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le pourvoi de la société Euro Park Service ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Il est sursis à statuer sur le pourvoi de la société Euro Park Service, jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
1°) Lorsqu'une législation nationale d'un Etat membre utilise en droit interne la faculté offerte par le 1 de l'article 11 de la directive n° 90/434/CEE du Conseil, du 23 juillet 1990 modifiée, concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'Etats membres différents, y-a-t-il place pour un contrôle des actes pris pour la mise en oeuvre de cette faculté au regard du droit primaire de l'Union européenne ' <br/>
2°) En cas de réponse positive à la première question, les stipulations de l'article 43 du traité instituant la Communauté européenne, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne, doivent-elles être interprétées comme faisant obstacle à ce qu'une législation nationale, dans un but de lutte contre la fraude ou l'évasion fiscales, subordonne le bénéfice du régime fiscal commun applicable aux fusions et opérations assimilées à une procédure d'agrément préalable en ce qui concerne les seuls apports faits à des personnes morales étrangères à l'exclusion des apports faits à des personnes morales de droit national '<br/>
Article 2 : La présente décision sera notifiée à la société Euro Park Service, au ministre des finances et des comptes publics et à la Cour de justice de l'Union européenne.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
