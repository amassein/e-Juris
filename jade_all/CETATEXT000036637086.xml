<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036637086</ID>
<ANCIEN_ID>JG_L_2018_02_000000401043</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/70/CETATEXT000036637086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 21/02/2018, 401043</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401043</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401043.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres ont demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir l'arrêté du 21 mars 2013 par lequel le maire de Strasbourg a délivré un permis de construire et un permis de démolir à la SCI La Villa Mimosas, en vue de la construction d'un immeuble d'habitation et la décision du 6 juin 2013 rejetant leur recours gracieux. Par un jugement n° 1303565 du 3 mars 2015, le tribunal administratif de Strasbourg a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15NC00825 du 28 avril 2016, la cour administrative d'appel de Nancy a, sur l'appel de l'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres, annulé ce jugement, l'arrêté du 21 mars 2013 et la décision du 6 juin 2013 du maire de Strasbourg. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés respectivement les 28 juin et 28 septembre 2016 et le 5 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, la SCI La Villa Mimosas demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de la SCI La Villa Mimosas et à Me Le Prado, avocat de l'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 21 mars 2013, le maire de Strasbourg a délivré à la SCI La Villa Mimosas un permis de construire et un permis de démolir en vue de la construction d'un immeuble d'habitation comportant neuf logements ; que, par un jugement du 3 mars 2015, le tribunal administratif de Strasbourg a rejeté la demande de l'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres tendant à l'annulation de cet arrêté et de la décision du 6 juin 2013 par laquelle le maire de Strasbourg a rejeté leur recours gracieux ; que, par un arrêt du 28 avril 2016, contre lequel la SCI La Villa Mimosas se pourvoit en cassation, la cour administrative d'appel de Nancy a annulé ce jugement ainsi que l'arrêté du 21 mars 2013 et la décision du 6 juin 2013 du maire de Strasbourg ; que, pour annuler ces décisions, elle a retenu deux motifs, tirés de la méconnaissance des articles 11 UD et 7 UD du règlement du plan d'occupation des sols de Strasbourg relatifs, d'une part, à l'aspect extérieur des constructions, aux aménagements de leurs abords et à la protection des immeubles et des éléments de paysage et, d'autre part, à l'emprise au sol des constructions ; <br/>
<br/>
              2. Considérant, en premier lieu, que le moyen tiré de ce que l'arrêté attaqué ne viserait et n'analyserait pas avec une précision suffisante les moyens et conclusions des parties, en méconnaissance des dispositions de l'article R. 741-2 du code de justice administrative, ne peut qu'être écarté ; <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 11 UD du règlement du plan d'occupation des sols de la ville de Strasbourg : " Le permis de construire peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains, ainsi qu'à la conservation des perspectives monumentales. La définition volumétrique et architecturale des façades et des toitures doit s'intégrer à la composition de la rue, de la place, de l'îlot. / (...) En outre, les constructions nouvelles doivent s'intégrer harmonieusement à la séquence dans laquelle elles s'insèrent, en tenant notamment compte des hauteurs des constructions riveraines et voisines. / Pour cette raison, il peut être imposé des hauteurs inférieures aux maximales fixées à l'article 10 UD ci dessus. De même, parmi les règles alternatives d'implantation figurant le cas échéant aux articles 6 et 7 UD ci dessus, certaines d'entre elles peuvent être imposées. " ; qu'eu égard à la teneur de ces dispositions, il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, d'apprécier si l'autorité administrative a pu légalement autoriser la construction projetée, compte tenu de ses caractéristiques et de celles des lieux avoisinants, sans méconnaître les exigences résultant de cet article et la marge d'appréciation qu'elles laissent à l'autorité administrative pour accorder ou refuser de délivrer une autorisation d'urbanisme ; que la cour a relevé que le secteur du quartier de la Robertsau, dans lequel doit s'intégrer la construction litigieuse, présente un intérêt et un caractère architectural particuliers qui résultent de l'aménagement cohérent des constructions dans le cadre du lotissement à l'origine de l'urbanisation de ce secteur, et que le projet litigieux, qui se présente sous la forme d'une construction cubique avec des bardages en métal, ne s'intègre pas, par la définition volumétrique et architecturale de ses façades et de sa toiture, à la composition de la rue ; qu'en exerçant ainsi son contrôle, la cour n'a pas, contrairement à ce qui est soutenu, écarté toute marge d'appréciation pour l'autorité administrative et n'a pas méconnu la portée des dispositions de l'article 11 UD du règlement du document d'urbanisme ; que la société requérante ne saurait reprocher sur ce point à la cour de ne pas avoir tenu compte des dispositions des deux derniers alinéas de cet article, qui n'ont pas pour objet d'accroître cette marge d'appréciation mais de la réduire en renforçant les exigences qui s'imposent aux constructions nouvelles ; que, par ailleurs, la cour a tenu compte, ainsi qu'elle le devait, tant des caractéristiques de la construction projetée que de celles des lieux avoisinants ; que, sur ces différents points, l'arrêt  n'est entaché d'aucune erreur de droit ; qu'enfin, en estimant, au vu des éléments qu'elle a souverainement appréciés, que les dispositions de l'article 11 UD avaient été en l'espèce méconnues, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'aux termes de l'article 9 UD du règlement du plan d'occupation des sols : " Dans les différentes zones, l'emprise au sol des constructions de toute nature, rapportée à la surface du terrain ne peut excéder le pourcentage suivant : (...) / A la Robertsau : zone  ROB UD 2 : 40% (...) " ; cet article précise également que " dans toutes les zones où le pourcentage précité est limité à 40 ou 50 %, celui-ci peut-être majoré de 10 % au maximum pour des constructions d'une hauteur hors tout égale ou inférieure à 3,50 mètres " et que : " (...) Les surplombs situés à plus de 2,50 mètres, mesurés à compter du niveau de la voie de desserte du terrain, ne sont pas pris en compte pour le calcul de l'emprise au sol. " ; qu'aux termes de l'article R. 420-1 du code de l'urbanisme, dans sa version alors en vigueur : " L'emprise au sol (...) est la projection verticale du volume de la construction, tous débords et surplombs inclus " ; que la cour a estimé que devaient être pris en compte, pour le calcul de l'emprise au sol de la construction projetée, une surface végétalisée sur une dalle en béton aménagée sur la partie avancée du sous-sol et faisant corps avec le gros oeuvre de la construction, au motif qu'elle " consommait des mètres carrés " ; qu'elle en a déduit que l'emprise au sol de la construction projetée excédait la majoration de 10 % autorisée par l'article 9 UD, dont les dispositions avaient été ainsi, selon elle, méconnues ; que, toutefois, en l'absence de prescriptions particulières dans le document d'urbanisme précisant la portée de cette notion, sauf pour les surplombs, l'emprise au sol s'entend, en principe, comme la projection verticale du volume de la construction, tous débords inclus ; qu'il en résulte qu'en tenant compte d'une dalle en béton située sous une surface végétalisée et ne dépassant pas le niveau du sol, la cour a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              5. Considérant, en dernier lieu, qu'aux termes de l'article L. 421-6 du code de l'urbanisme : " Le permis de construire ou d'aménager ne peut être accordé que si les travaux projetés sont conformes aux dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords et s'ils ne sont pas incompatibles avec une déclaration d'utilité publique. / Le permis de démolir peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les travaux envisagés sont de nature à compromettre la protection ou la mise en valeur du patrimoine bâti ou non bâti, du patrimoine archéologique, des quartiers, des monuments et des sites. " ; que l'article R. 431-21 du code de l'urbanisme dispose que : " Lorsque les travaux projetés nécessitent la démolition de bâtiments soumis au régime du permis de démolir, la demande de permis de construire ou d'aménager doit : / a) Soit être accompagnée de la justification du dépôt de la demande de permis de démolir ; / b) Soit porter à la fois sur la démolition et sur la construction ou l'aménagement. " ; qu'il résulte de ces dispositions que, si le permis de construire et le permis de démolir peuvent être accordés par une même décision, au terme d'une instruction commune, ils constituent des actes distincts comportant des effets propres ; qu'en annulant l'arrêté du 21 mars 2013 en son entier, pour des motifs tirés de la seule illégalité du permis de construire, la cour administrative d'appel de Nancy a commis une erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, sur les deux motifs retenus par la cour pour annuler le jugement du tribunal administratif de Strasbourg et juger illégaux l'arrêté du 21 mars 2013, en tant qu'il accorde un permis de construire, et la décision rendue sur recours gracieux contre ce permis, le motif tiré la méconnaissance de l'article 11 UD du règlement du plan d'occupation des sols justifie légalement le dispositif de l'arrêt attaqué ; qu'en revanche, la SCI La Villa Mimosas est fondée à demander l'annulation de l'arrêt attaqué en tant qu'il annule le permis de démolir du 21 mars 2013 ainsi que la décision du 6 juin 2013 rejetant le recours gracieux contre ce permis;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SCI La Villa Mimosas qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres la somme de 2 000 euros à verser à la SCI La Villa Mimosas, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 avril 2016 de la cour administrative d'appel de Nancy est annulé en tant qu'il annule le permis de démolir délivré par le maire de Strasbourg le 21 mars 2013 et la décision du 6 juin 2013 rejetant le recours gracieux contre ce permis. <br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : L'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres verseront à la SCI La Villa Mimosas une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi et les conclusions présentées par l'association des résidents du quartier rue des Mimosas - impasse des Lavandes et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la SCI La Villa Mimosas et à l'association des résidents du quartier rue des Mimosas - impasse des Lavandes, premier dénommé, pour tous ses cosignataires. <br/>
      Copie en sera adressée à la ville de Strasbourg.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-02-09 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. EMPRISE AU SOL. - NOTION (ART. R. 420-1 DU CODE DE L'URBANISME).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. - TRAVAUX SOUMIS À PERMIS DE CONSTRUIRE NÉCESSITANT LA DÉMOLITION DE BÂTIMENTS SOUMIS AU RÉGIME DU PERMIS DE DÉMOLIR - PERMIS DE CONSTRUIRE ET PERMIS DE DÉMOLIR ACCORDÉS PAR UNE MÊME DÉCISION - ACTES DISTINCTS AYANT DES EFFETS PROPRES, NONOBSTANT CETTE CIRCONSTANCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-04-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. PERMIS DE DÉMOLIR. PROCÉDURE D'OCTROI. - TRAVAUX SOUMIS À PERMIS DE CONSTRUIRE NÉCESSITANT LA DÉMOLITION DE BÂTIMENTS SOUMIS AU RÉGIME DU PERMIS DE DÉMOLIR - PERMIS DE CONSTRUIRE ET PERMIS DE DÉMOLIR ACCORDÉS PAR UNE MÊME DÉCISION - ACTES DISTINCTS AYANT DES EFFETS PROPRES, NONOBSTANT CETTE CIRCONSTANCE.
</SCT>
<ANA ID="9A"> 68-01-01-02-02-09 En l'absence de prescriptions particulières dans le règlement du document local d'urbanisme précisant la portée de cette notion l'emprise au sol s'entend, en principe, comme la projection verticale du volume de la construction, tous débords inclus ainsi que le prévoit l'article R. 420-1 du code de l'urbanisme dans sa rédaction issue du décret n° 2011-2054 du 29 décembre 2011.... ,,Commet ainsi une erreur de droit la cour administrative d'appel qui tient compte, pour le calcul de l'emprise au sol de la construction projetée, d'une dalle en béton située sous une surface végétalisée et ne dépassant pas le niveau du sol.</ANA>
<ANA ID="9B"> 68-03-02 Il résulte des articles L. 421-6 et R. 431-21 du code de l'urbanisme que, si le permis de construire et le permis de démolir peuvent être accordés par une même décision, au terme d'une instruction commune, ils constituent des actes distincts ayant des effets propres. Commet une erreur de droit la cour administrative d'appel qui annule une telle décision dans son entier, pour des motifs tirés de la seule illégalité du permis de construire.</ANA>
<ANA ID="9C"> 68-04-01-02 Il résulte des articles L. 421-6 et R. 431-21 du code de l'urbanisme que, si le permis de construire et le permis de démolir peuvent être accordés par une même décision, au terme d'une instruction commune, ils constituent des actes distincts ayant des effets propres. Commet une erreur de droit la cour administrative d'appel qui annule une telle décision dans son entier, pour des motifs tirés de la seule illégalité du permis de construire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
