<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717838</ID>
<ANCIEN_ID>JG_L_2014_03_000000353066</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717838.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 12/03/2014, 353066</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353066</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:353066.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 30 septembre 2011 et 29 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés par le Comité Harkis et Vérité, dont le siège est BP 23 à Le Mée Sur Seine (77350), et M. B...A..., demeurant au ... ; le Comité Harkis et Vérité et M. A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le télégramme du ministre des armées n° 1334/MA/CAB/DIR du 12 mai 1962, les télégrammes des 16 mai 1962 et 15 juillet 1962 du ministre d'Etat, ministre des affaires algériennes, la circulaire du ministre des rapatriés du 31 janvier 1964 relative à l'accès au logement et la circulaire n° 2010-25 du 20 décembre 2010 relative à la programmation des contrats aidés ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978 ;<br/>
<br/>
              Vu le décret n° 97-244 du 18 mars 1977 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, Auditeur,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur les conclusions tendant à l'annulation de télégrammes ministériels des 12 mai, 16 mai et 15 juillet 1962, ainsi que de la circulaire du ministre des rapatriés du 31 janvier 1964 relative à l'accès au logement :<br/>
<br/>
              1. Considérant que le désistement du Comité Harkis et Vérité de ses conclusions tendant à l'annulation des télégrammes de 1962 et de la circulaire du 31 janvier 1964 est pur et simple ; que rien ne s'oppose à ce qu'il en soit donné acte ; <br/>
<br/>
              2. Considérant que les télégrammes ministériels attaqués ont trait aux conditions de rapatriement des harkis en métropole au lendemain des accords signés le 18 mars 1962 à Evian-les-Bains par les représentants du Gouvernement de la République et ceux du Front de libération nationale ; que la circulaire ministérielle attaquée est relative au logement des harkis en France au lendemain de l'accession de l'Algérie à l'indépendance ; qu'il est constant que les dispositions impératives que comportaient ces télégrammes et cette circulaire et qui trouvaient leur origine dans les événements qui ont précédé ou suivi l'indépendance de l'Algérie, n'étaient plus susceptibles, en raison de la disparition des situations qu'ils entendaient régir, de donner lieu à des décisions prises sur leur fondement lorsqu'ont été enregistrées les conclusions tendant à leur annulation ; qu'il suit de là que ces conclusions, qui sont dirigées contre des actes frappés de caducité, sont dépourvues d'objet et doivent être rejetées comme irrecevables ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de la circulaire du 20 décembre 2010 relative à la programmation des contrats aidés en 2011 :<br/>
<br/>
              3. Considérant, en premier lieu, que le délégué général à l'emploi et à la formation professionnelle était compétent, en vertu de l'article 3 décret du 18 mars 1977 portant création d'une délégation générale à l'emploi et à la formation, pour fixer par voie de circulaire la programmation, les objectifs et les priorités retenues en matière de contrats aidés ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'en indiquant que, pour les régions concernées, les enfants de harkis pouvaient être mentionnés dans les publics éligibles aux contrats uniques d'insertion, la circulaire attaquée s'est bornée à fixer les conditions dans lesquelles les dispositifs favorisant l'accès à la formation initiale et professionnelle et à l'emploi seraient mobilisés en faveur de différentes catégories de publics considérés comme prioritaires ; que, contrairement à ce que soutiennent les requérants, elle n'a ni pour objet ni pour effet de méconnaître le principe d'égalité ;<br/>
<br/>
              5. Considérant, en troisième et dernier lieu, qu'aux termes de l'article 2 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " La présente loi s'applique aux traitements automatisés de données à caractère personnel, ainsi qu'aux traitements non automatisés de données à caractère personnel contenues ou appelées à figurer dans des fichiers, à l'exception des traitements mis en oeuvre pour l'exercice d'activités exclusivement personnelles, lorsque leur responsable remplit les conditions prévues à l'article 5. / (...) Constitue un traitement de données à caractère personnel toute opération ou tout ensemble d'opérations portant sur de telles données, quel que soit le procédé utilisé, et notamment la collecte, l'enregistrement, l'organisation, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, ainsi que le verrouillage, l'effacement ou la destruction. / Constitue un fichier de données à caractère personnel tout ensemble structuré et stable de données à caractère personnel accessibles selon des critères déterminés (...) " ;<br/>
<br/>
              6. Considérant que les dispositions de la circulaire attaquée se bornent à prévoir que le bénéficiaire d'un contrat unique d'insertion, son employeur et l'Etat signent une convention individuelle, selon un modèle dont aucune des mentions ne permet d'identifier les enfants de harkis ; que, contrairement à ce qui est soutenu, elles n'ont, par elles-mêmes, ni pour objet ni pour effet de créer un traitement de données à caractère personnel au sens de l'article 2 de la loi du 6 janvier 1978 ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les requérants ne sont pas fondés à demander l'annulation de la circulaire du 20 décembre 2010 relative à la programmation des contrats aidés en 2011 ;<br/>
<br/>
              Sur les conclusions des requérants présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il est donné acte du désistement du Comité Harkis et Vérité de ses conclusions tendant à l'annulation des télégrammes des 12 mai, 16 mai et 15 juillet 1962 et de la circulaire du 31 janvier 1964. <br/>
Article 2 : La requête de M. A...et le surplus des conclusions de la requête du Comité Harkis et Vérité sont rejetés. <br/>
Article 3 : La présente décision sera notifiée au Comité Harkis et Vérité, à M. B...A..., au Premier ministre (Mission interministérielle aux rapatriés) et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. CADUCITÉ. - EXISTENCE - DISPARITION DES SITUATIONS QU'UNE CIRCULAIRE ENTENDAIT RÉGIR.
</SCT>
<ANA ID="9A"> 01-08-04 Une circulaire qui n'est plus susceptible, en raison de la disparition des situations qu'elle entendait régir, de recevoir application, doit être regardée comme frappée de caducité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
