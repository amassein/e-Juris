<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993656</ID>
<ANCIEN_ID>JG_L_2017_06_000000383693</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/36/CETATEXT000034993656.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 21/06/2017, 383693, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383693</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Vincent Ploquin-Duchefdelaville</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:383693.20170621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société AXA France IARD, venant aux droits de la société Axa Courtage IARD, a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 1 237 905,80 euros à titre d'intérêts moratoires sur le remboursement de crédit de taxe sur la valeur ajoutée de 2 166 172,33 euros qui a été accordé à la société Axa Courtage IARD le 12 juillet 2010 au titre du quatrième trimestre 1995. Par jugement n° 1022238 du 20 mars 2012, le tribunal administratif de Paris a condamné l'Etat à verser à la société AXA France IARD les intérêts moratoires sur la somme de 2 166 172,33 euros, calculés du 15 février 1996 au 12 juillet 2010, dans la limite du quantum de 1 206 934,90 &#128;, les intérêts de cette somme étant capitalisés à la date du 24 février 2012 pour produire eux-mêmes intérêts.<br/>
<br/>
              Par arrêt n° 12PA02760 du 5 juin 2014, la cour administrative d'appel de Paris, sur appel du ministre de l'économie et des finances, a annulé ce jugement et rejeté la demande de la société AXA France IARD présentée devant le tribunal administratif de Paris.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 août et 13 novembre 2014 et le 25 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société AXA France IARD demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Ploquin-Duchefdelaville, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de la société Axa France Iard.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Uni Europe Assistance a demandé à l'administration, par une lettre datée du 15 février 1996 et reçue le 2 mai suivant, le remboursement d'un crédit de taxe sur la valeur ajoutée d'un montant de 14 209 159 francs (2 166 172,33 euros) dont elle s'estimait titulaire sur le Trésor au titre du quatrième trimestre de l'année 1995. Cette demande a conduit l'administration à vérifier sa comptabilité, puis à lui notifier, le 29 juillet 1997, un rappel de taxe au titre de ce même trimestre, d'un montant de 19 716  000 francs (3 005 684,82 euros) procédant de la reprise d'une fraction de la taxe précédemment déduite au titre de la même période. Ce rappel a  annulé le crédit dont se prévalait la société et l'administration a, en conséquence, rejeté, le 12 novembre 1997, la demande de remboursement de ce crédit présentée par la société le 2 mai 1996, puis a mis en recouvrement, le 5 mars 1998, le surplus du rappel non compensé par le crédit de taxe, soit la somme de 5 506 841 francs (839 512,50 euros). <br/>
<br/>
              2. La société AXA Courtage IARD, devenue AXA France IARD, qui a succédé à la société Uni Europe Assistance, a introduit deux instances devant le tribunal administratif de Paris puis la cour administrative d'appel de Paris à l'encontre, d'une part, de la remise en cause par le service de ses droits à déduction d'un montant de 3 005 684,82 euros et de l'annulation qui en est résulté de son crédit de taxe, et, d'autre part, du rappel de taxe mis à sa charge. Dans le cadre de l'instance pendante devant le Conseil d'Etat contre l'arrêt de la cour administrative d'appel du 2 octobre 2006 qui avait rejeté sa requête dirigée contre le refus d'imputation de la taxe déductible, l'administration a, le 25 juin 2010, prononcé le dégrèvement des rappels de taxe, puis, le 12 juillet suivant, accordé à la société le remboursement du crédit de taxe demandé. Ce remboursement n'ayant pas été assorti des intérêts moratoires prévus à l'article L. 208 du livre des procédures fiscales, la société a, par lettre du 30 juillet 2010, sollicité le versement de ces intérêts auprès de l'administration, laquelle a refusé de faire droit à sa demande au motif que la réclamation du 15 février 1996, reçue le 2 mai suivant, tendant au remboursement du crédit de taxe était tardive et irrégulière, et, qu'en conséquence, le dégrèvement dont elle avait bénéficié avait été prononcé d'office. La société a alors saisi le tribunal administratif de Paris qui, par jugement du 20 mars 2012, après avoir estimé que la demande de remboursement n'était pas tardive, a condamné l'Etat à lui verser les intérêts moratoires en cause, dans la limite d'un montant de 1 206 934,90 euros. La société AXA France IARD se pourvoit en cassation contre l'arrêt du 5 juin 2014 de la cour administrative d'appel de Paris qui, sur appel du ministre de l'économie et des finances, a annulé ce jugement et rejeté sa demande de première instance.<br/>
<br/>
              3. Aux termes du IV de l'article 271 du code général des impôts : " La taxe déductible dont l'imputation n'a pu être opérée peut faire l'objet d'un remboursement dans les conditions, selon les modalités et dans les limites fixées par décret en Conseil d'Etat " ; aux termes de l'article 242-0 A de l'annexe II au code général des impôts, pris sur le fondement de l'article 271 précité, dans sa rédaction alors applicable : " Le remboursement de la taxe sur la valeur ajoutée déductible dont l'imputation n'a pu être opérée doit faire l'objet d'une demande des assujettis. Le remboursement porte sur le crédit de taxe déductible constaté au terme de chaque année civile " ; aux termes de l'article 242-0 C de la même annexe, dans sa rédaction alors en vigueur : " I. 1. Les demandes de remboursement doivent être déposées au cours du mois de janvier (...). / II. 1. Par dérogation aux dispositions du I, les assujettis soumis de plein droit ou sur option au régime normal d'imposition peuvent demander un remboursement au titre d'un trimestre civil lorsque chacune des déclarations de ce trimestre fait apparaître un crédit de taxe déductible. La demande de remboursement est déposée au cours du mois suivant le trimestre considéré (...) ". <br/>
<br/>
              4. Ces dispositions, qui imposent au redevable de demander le remboursement du crédit de taxe sur la valeur ajoutée dont il dispose dans des délais déterminés, n'ont pas pour objet ou pour effet de faire obstacle à ce que ce redevable puisse ultérieurement, en cas de persistance d'un crédit, non seulement l'imputer sur une taxe due, mais encore, le cas échéant, demander son remboursement au cours du mois de janvier de l'année suivante ou au cours du mois suivant un trimestre civil où chacune des déclarations de ce trimestre fait apparaître un crédit de taxe déductible. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond, d'une part, que la société avait la faculté de former une réclamation régulière afin d'obtenir le remboursement de la taxe sur la valeur ajoutée correspondant au quatrième trimestre 1995 jusqu'au 31 janvier 1996 mais n'a présenté cette demande que le 2 mai 1996 et, d'autre part, que la réclamation présentée par la société le 8 avril 1998, qui tendait uniquement à la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge par avis de mise en recouvrement du 5 mars 1998, avait un objet différent et ne pouvait donc être regardée comme une nouvelle demande de remboursement du crédit de taxe litigieux. Ainsi, en se fondant, pour refuser à la requérante le bénéfice des intérêts moratoires sollicités, sur le caractère tardif de la demande de remboursement du crédit de taxe sur la valeur ajoutée qu'elle a formée le 15 février 1996 et sur le fait qu'aucune demande ultérieure de même nature n'avait été présentée, la cour, qui a souverainement apprécié les faits qui lui étaient soumis, n'a pas commis d'erreur de droit. <br/>
<br/>
              6. La cour n'a pas méconnu l'article L. 208 du livre des procédures fiscales en jugeant que le dégrèvement accordé le 12 juillet 2010, même s'il avait été accordé dans le cadre d'une  instance contentieuse, avait été prononcé d'office sur le fondement de l'article R. 211-1 du livre des procédures fiscales et ne pouvait être regardé comme étant intervenu à la suite d'une instance contentieuse présentée après le rejet d'une réclamation régulière. Elle n'a pas davantage commis d'erreur de droit en jugeant, de manière suffisamment motivée, que la circonstance que ce dégrèvement serait intervenu après l'expiration du délai prévu à l'article R. 211-1 du livre des procédures fiscales n'était pas susceptible de le faire regarder comme intervenu à la suite d'une instance contentieuse. <br/>
<br/>
              7. Aux termes de l'article L. 80 A du livre des procédures fiscales : " Il ne sera procédé à aucun rehaussement d'impositions antérieures si la cause du rehaussement poursuivi par l'administration est un différend sur l'interprétation par le redevable de bonne foi du texte fiscal et s'il est démontré que l'interprétation sur laquelle est fondée la première décision a été, à l'époque, formellement admise par l'administration. / Lorsque le redevable a appliqué un texte fiscal selon l'interprétation que l'administration avait fait connaître par ses instructions ou circulaires publiées et qu'elle n'avait pas rapportée à la date des opérations en cause, elle ne peut poursuivre aucun rehaussement en soutenant une interprétation différente. Sont également opposables à l'administration, dans les mêmes conditions, les instructions ou circulaires publiées relatives au recouvrement de l'impôt et aux pénalités fiscales ".<br/>
<br/>
              8. La cour a estimé que la société se prévalait, sur le fondement de l'article L. 80 A du livre des procédures fiscales, des paragraphes 2 et 3 de la documentation administrative de base référencée 13 O-1511 dans sa version mise à jour le 30 avril 1996, qui prévoient, d'une part, que si le dégrèvement consécutif à une condamnation de l'Etat devant la juridiction compétente entraîne un remboursement, il ouvre droit au paiement des intérêts à raison de l'impôt remboursé ayant fait l'objet de la requête et, d'autre part, qu'il en est de même lorsque, à défaut de condamnation explicite de l'Etat, le dégrèvement est prononcé d'office par l'administration pendant l'instruction des demandes soumises à ces juridictions. La société AXA France IARD, qui demande le versement d'intérêts moratoires, ne peut invoquer le bénéfice de ces énonciations, qui ne sont relatives ni à l'assiette ou au recouvrement d'une imposition ni à des pénalités fiscales. Ce motif doit être substitué au motif erroné retenu par l'arrêt attaqué, dont il justifie le dispositif. <br/>
<br/>
              9. Par suite, le pourvoi de la société doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                  --------------<br/>
<br/>
Article 1er : Le pourvoi de la société AXA France IARD est rejeté.<br/>
<br/>
Article 2: La présente décision sera notifiée à la société AXA France IARD et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
