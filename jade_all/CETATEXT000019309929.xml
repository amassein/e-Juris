<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000019309929</ID>
<ANCIEN_ID>JG_L_2008_08_000000285719</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/19/30/99/CETATEXT000019309929.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 06/08/2008, 285719, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2008-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>285719</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Karin  Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Collin Pierre</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 octobre 2005 et 2 février 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMPAGNIE NATIONALE DE NAVIGATION, dont le siège est 128 boulevard Haussmann à Paris (75008), venant aux droits de la Compagnie morbihannaise et nantaise de navigation ; la COMPAGNIE NATIONALE DE NAVIGATION demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 20 juin 2005 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 28 mars 2003 du tribunal administratif de Nantes rejetant sa demande tendant à la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 1993 et 1994, pour des montants respectifs de 2 410 690 F (367 507,32 euros) et 1 440 355 F (219 580,70 euros) et à l'obtention de cette décharge ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui accorder la décharge des impositions en litige ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la COMPAGNIE NATIONALE DE NAVIGATION, <br/>
<br/>
              - les conclusions de M. Pierre Collin, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la COMPAGNIE NATIONALE DE NAVIGATION a obtenu des dégrèvements de ses cotisations de taxe professionnelle pour les années 1993 et 1994, en application de l'article 1647 B sexies du code général des impôts, relatif au plafonnement de cette taxe en fonction de la valeur ajoutée ; que l'administration fiscale a remis en cause ces dégrèvements en estimant que la société aurait dû comptabiliser dans la valeur ajoutée prise en compte pour le plafonnement de la taxe professionnelle les recettes et charges qu'elle inscrivait en comptabilité dans un compte de transfert de charges et qui étaient afférentes à ses activités de gestion de bacs fluviaux, pour le compte du département de Loire-Atlantique, et de liaisons maritimes, pour le compte du département du Morbihan ; que l'administration fiscale a en conséquence remis à la charge de la société les montants de taxe professionnelle s'élevant à 2 410 690 F (367 507,32 euros) pour l'année 1993 et à 1 440 355 F (219 580,70 euros) pour l'année 1994 dont elle avait été initialement dégrevée ; que la COMPAGNIE NATIONALE DE NAVIGATION se pourvoit en cassation contre l'arrêt du 20 juin 2005 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 28 mars 2003 du tribunal administratif de Nantes qui avait rejeté sa demande en décharge des compléments de taxe professionnelle susmentionnés ;<br/>
<br/>
              Considérant, en premier lieu, que, lorsque l'administration, qui, à la demande du contribuable, a fait droit à une demande de dégrèvement de cotisations de taxe professionnelle au titre de l'article 1647 B sexies du code général des impôts, estime ultérieurement qu'elle a accordé ce dégrèvement à tort, il lui appartient, conformément aux dispositions de l'article 1658 du même code, de procéder par voie de rôle pour le recouvrement de la créance fiscale dont elle se prévaut ; que la cour, qui a relevé que l'administration avait émis des rôles supplémentaires pour la mise en recouvrement des cotisations de taxe professionnelle en litige, n'a, contrairement à ce que soutient la COMPAGNIE NATIONALE DE NAVIGATION, dénaturé ni ses écritures en jugeant qu'elle ne contestait pas la régularité de ces rôles, ni les pièces du dossier par suite d'une confusion entre les imprimés modèle n° 1413 qui ont été adressés à la société pour la mise en recouvrement de la créance fiscale dont s'est prévalue l'administration et les rôles supplémentaires établis pour le recouvrement de cette créance ; que la cour n'a pas non plus entaché son arrêt d'erreur de droit en jugeant que, bien que les imprimés modèle n° 1413 ne fussent pas spécifiquement destinés à la mise en recouvrement des impositions en litige, ils pouvaient être utilisés comme un avis d'imposition, au sens de l'article L. 253 du livre des procédures fiscales ; <br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 1647 B sexies du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : I. Sur demande du redevable, la cotisation de taxe professionnelle de chaque entreprise est plafonnée à 3,5 % de la valeur ajoutée produite au cours de l'année au titre de laquelle l'imposition est établie ou au cours du dernier exercice de douze mois clos au cours de cette même année lorsque cet exercice ne coïncide pas avec l'année civile. La valeur ajoutée est définie selon les modalités prévues au II. (...) / II. 1. La valeur ajoutée mentionnée au I est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers constaté pour la période définie au I. / 2. Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : / d'une part, les ventes, les travaux, les prestations de services ou les recettes ; les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; / et, d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks au début de l'exercice. / Les consommations de biens et services en provenance de tiers comprennent : les travaux, fournitures et services extérieurs, à l'exception des loyers afférents aux biens pris en crédit-bail, les frais de transports et déplacements, les frais divers de gestion. (...) ;<br/>
<br/>
              Considérant que ces dispositions fixent la liste limitative des catégories d'éléments comptables qui doivent être pris en compte dans le calcul de la valeur ajoutée en fonction de laquelle sont plafonnées les cotisations de taxe professionnelle ; que, pour déterminer si une charge ou un produit se rattache à l'une de ces catégories, il y a lieu de se reporter aux dispositions du plan comptable général dans leur rédaction en vigueur lors de l'année d'imposition concernée ; qu'aux termes de l'article 38 quater de l'annexe III au code général des impôts : Les entreprises doivent respecter les définitions édictées par le plan comptable général, sous réserve que celles-ci ne soient pas incompatibles avec les règles applicables pour l'assiette de l'impôt. ;<br/>
<br/>
              Considérant que les activités en cause dans le présent litige constituent, pour l'application des dispositions précitées de l'article 1647 B sexies du code général des impôts, des prestations de services concourant à la détermination de la production de l'exercice et auraient pu être comptabilisées comme telles ; que la cour, après avoir relevé que la circonstance que les sommes en cause aient été enregistrées dans un compte de transfert de charges ne faisait pas obstacle à ce qu'elles fussent prises en compte pour le calcul de la valeur ajoutée au sens de l'article 1647 B sexies du code général des impôts, dès lors qu'elles pouvaient être rattachées à l'une des catégories d'éléments comptables mentionnées à l'article 1647 B sexies du code général des impôts, a jugé que tel était bien le cas en l'espèce, les activités en cause constituant, au sens et pour l'application de l'article 1647 B sexies, des prestations de services ; qu'elle a ainsi suffisamment répondu au moyen de la société tiré de ce que l'administration aurait dû préalablement démontrer, avant de pouvoir légalement prendre en compte les sommes en cause dans le calcul de la valeur ajoutée au sens de l'article 1647 B sexies, qu'elle avait commis une irrégularité comptable en les inscrivant dans un compte de transfert de charges ; qu'elle n'a en outre pas commis d'erreur de droit en jugeant que l'administration avait à bon droit pris en compte ces sommes pour le calcul de la valeur ajoutée produite par la société servant au plafonnement de la taxe professionnelle ;<br/>
<br/>
              Considérant, enfin, que si l'instruction administrative 6 E-10-85 du 18 décembre 1985 préconise de déterminer les éléments comptables nécessaires à la détermination de la valeur ajoutée à prendre en compte pour le plafonnement de la taxe professionnelle à partir du compte de résultat de l'exercice tel qu'il est retracé sur les déclarations d'impôt sur les sociétés souscrites par les entreprises, ces énonciations n'impliquent nullement que les sommes inscrites dans un compte de transfert de charges seraient, alors même que cette instruction ne mentionne pas la ligne transfert de charges figurant sur les imprimés de la déclaration de résultat, nécessairement exclues du calcul de cette valeur ajoutée ; qu'ainsi, en jugeant que cette instruction ne contenait aucune interprétation formelle de la loi fiscale dont la société puisse se prévaloir sur le fondement de l'article L. 80 A du livre des procédures fiscales, la cour n'a, en tout état de cause, pas entaché son arrêt d'une erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la COMPAGNIE NATIONALE DE NAVIGATION n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que, par voie de conséquence, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que cette société demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la COMPAGNIE NATIONALE DE NAVIGATION est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la COMPAGNIE NATIONALE DE NAVIGATION et au ministre du budget, des comptes publics et de la fonction publique.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
