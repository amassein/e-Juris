<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032408983</ID>
<ANCIEN_ID>JG_L_2016_04_000000384188</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/89/CETATEXT000032408983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 15/04/2016, 384188, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384188</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384188.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Marseille de condamner l'Etat à l'indemniser des préjudices ayant résulté pour lui d'un accident de service survenu le 18 octobre 2007. Par un jugement n° 1201950 du 30 mai 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13MA02736 du 8 juillet 2014, la cour administrative d'appel de Marseille, statuant sur l'appel de M.A..., a partiellement fait droit à ses conclusions et a condamné l'Etat à lui verser la somme de 10 132 euros, assortie des intérêts au taux légal. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 septembre et 21 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant que, par l'article 3 de son dispositif, il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              - l'arrêté du 9 décembre 2009 fixant le montant de la prime de restructuration et de l'allocation d'aide à la mobilité du conjoint dans le cadre de la réorganisation des services relevant de la direction générale de la police nationale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...A..., brigadier-chef de police au commissariat de Digne-les-Bains, a subi un accident le 18 octobre 2007 ; que, par un arrêté du 11 juillet 2011, consécutif à un jugement du 24 mars 2011 du tribunal administratif de Marseille, l'administration a reconnu que cet accident était imputable au service ; que par un arrêt du 8 juillet 2014, la cour administrative d'appel de Marseille a partiellement fait droit aux conclusions par lesquelles M. A...lui demandait de condamner l'Etat à l'indemniser des préjudices ayant résulté pour lui de cet accident ; que M. A... demande l'annulation de cet arrêt en tant qu'il rejette le surplus de ses conclusions indemnitaires ; <br/>
<br/>
              2. Considérant, en premier lieu, que M. A...a demandé pour la première fois en appel à être indemnisé de préjudices nouveaux constitués, d'une part, par la perte de primes et indemnités pour les mois de décembre 2011 et janvier 2012, d'autre part, par des frais médicaux, de déplacement et de correspondance relatifs à cette même période ; que si la cour a statué sur les conclusions relatives aux frais médicaux, de déplacement et de correspondance, qu'elle a inclus parmi les préjudices dont elle a mis la réparation à la charge de l'Etat, elle a omis de statuer sur les conclusions relatives à la perte de primes et indemnités pour les mois de décembre 2011 et janvier 2012 ; que son arrêt doit être annulé en tant qu'il a omis de statuer sur ces conclusions ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il résulte des mentions non contestées de l'arrêt attaqué que M. A...a été placé en congé de longue durée du 18 octobre 2007 au 17 décembre 2010 ; qu'il ressort des écritures produites devant les juges du fond que l'intéressé soutenait en se fondant sur les dispositions de l'article 34 de la loi du 11 janvier 1984 que le ministre de l'intérieur l'avait illégalement placé à mi-traitement du 18 octobre au 17 décembre 2010 et qu'il demandait à être indemnisé du préjudice correspondant ; qu'il suit de là qu'en jugeant qu'il ne contestait pas utilement que l'ensemble des indemnités et traitements auxquels il avait droit au titre de cette période lui avaient été versées, la cour administrative d'appel a inexactement apprécié la portée de ses écritures ; que l'arrêt attaqué doit être annulé dans la mesure où il statue sur les conclusions relatives aux pertes de revenus pour la période du 18 octobre au 17 décembre 2010 ; <br/>
<br/>
              4. Considérant, en troisième lieu, que M. A...a demandé à la cour administrative d'appel de condamner l'Etat à l'indemniser de frais de santé constitués par des franchises restées à sa charge à hauteur de 147,50 euros, dont il a justifié par la production de documents émanant des organismes sociaux ; qu'il en résulte qu'en jugeant qu'il ne soutenait pas que son accident de service avait entraîné des frais qui n'auraient pas été pris en charge par le régime de protection sociale dont il bénéficiait, la cour a également inexactement apprécié la portée de ses écritures ; que l'arrêt attaqué doit, par suite, être annulé dans la mesure où il statue sur les frais de santé ; <br/>
<br/>
              5. Considérant, en quatrième lieu, que M. A...a demandé à être indemnisé d'un " préjudice moral et de santé ", qu'il a évalué à 500 euros par mois pendant trente-huit mois, soit 19 000 euros, et de troubles dans les conditions d'existence consécutifs à son changement d'affectation, qu'il a proposé d'évaluer par référence à l'article 3 de l'arrêté du 9 décembre 2009 visé ci-dessus ; qu'après avoir constaté que l'arrêté du 9 décembre 2009 n'était pas applicable à la situation de M.A..., la cour administrative d'appel lui a accordé une indemnité forfaitaire de 2 000 euros au titre de son préjudice moral, de son préjudice de santé et de ses troubles dans les conditions d'existence confondus ; qu'en statuant ainsi, la cour a suffisamment motivé son arrêt au regard de l'argumentation qui lui était soumise et n'a pas dénaturé les pièces du dossier ;<br/>
<br/>
              6. Considérant en dernier lieu que M. A...a demandé à être indemnisé de frais de correspondance à hauteur de 142 euros ; qu'en retenant qu'au vu des pièces du dossier, il serait fait une juste appréciation de ce chef de préjudice en l'évaluant à la somme de 50 euros, la cour administrative d'appel, qui pouvait souverainement écarter de la recension des frais invoqués par M. A...ceux qu'elle jugeait être sans lien direct avec la procédure en cause, a suffisamment motivé son arrêt sur ce point et n'a pas dénaturé les pièces du dossier ; <br/>
<br/>
               7. Considérant qu'il résulte de tout ce qui précède que l'arrêt de la cour administrative d'appel de Marseille doit être annulé en tant qu'il a omis de statuer sur l'indemnisation de la perte des primes et indemnités de M. A...pour les mois de décembre 2011 et janvier 2012 et en tant qu'il statue sur l'indemnisation de la perte de revenus pour la période du 18 octobre 2010 au 17 décembre 2010 et sur l'indemnisation des frais de santé ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A...de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 8 juillet 2014 de la cour administrative d'appel de Marseille est annulé en tant qu'il a omis de statuer sur l'indemnisation de la perte des primes et indemnités de M. A... pour les mois de décembre 2011 et janvier 2012 et en tant qu'il statue sur l'indemnisation de la perte de revenus pour la période du 18 octobre au 17 décembre 2010 et sur l'indemnisation des frais de santé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille dans la limite de la cassation ainsi prononcée.<br/>
<br/>
Article 3 : L'Etat versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
