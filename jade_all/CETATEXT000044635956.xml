<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044635956</ID>
<ANCIEN_ID>JG_L_2021_12_000000445451</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/63/59/CETATEXT000044635956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 29/12/2021, 445451, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445451</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DUHAMEL - RAMEIX - GURY- MAITRE ; SARL MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445451.20211229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, d'une part, de suspendre l'exécution, sur le fondement de l'article L. 521-1 du code de justice administrative, de la décision du 19 août 2020 par laquelle la commission d'appel de France Galop lui a interdit de monter dans toutes les courses régies par le code des courses au galop, pour une durée de six mois, à compter du quatorzième jour suivant la notification de cette décision, d'autre part, d'enjoindre à France Galop de l'inscrire, à titre provisoire et jusqu'à ce qu'il soit statué au fond, dans les courses au galop avec ou sans obstacles. <br/>
<br/>
              Par une ordonnance n° 2008921 du 2 octobre 2020, la juge des référés du tribunal administratif de Cergy-Pontoise a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 19 octobre et 2 novembre 2020, ainsi que le 6 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'association France Galop demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la requête de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des courses au galop ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Duhamel-Rameix-Gury-Maître, avocat de l'association France Galop, et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. B... ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 décembre 2021, présentée par l'association France Galop.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ".<br/>
<br/>
              2.	Aux termes du II de l'article 209 du code des courses au galop : " Les Commissaires de courses ont le pouvoir dans les limites du présent Code : (...) 2. D'interdire à un jockey de monter pour une durée qui ne peut dépasser 6 mois ; ". Aux termes des III et IV de l'article 234 du même code : " III. Pouvoir de suppression ou de modification des sanctions prises. - Les juges d'appel peuvent supprimer des sanctions ou prendre des sanctions différentes. Ils ne peuvent toutefois, prendre une sanction plus sévère à l'égard de la personne sanctionnée lorsque l'appel a été interjeté par celle-ci. / IV. Pouvoir d'évocation. - Les juges d'appel, lorsqu'ils sont saisis, peuvent évoquer un fait non examiné par les premiers juges et statuer sur l'ensemble de l'affaire à l'égard de toutes les parties visées par la décision dont l'appel, même si certaines de ces parties n'ont pas interjeté appel. Ces dernières doivent être régulièrement appelées. / Dans le cas où l'examen de l'affaire ainsi évoquée ferait apparaître des fautes ou des infractions non examinées par les premiers juges, les juges d'appel peuvent prendre des sanctions à l'égard des contrevenants après les avoir entendus en leurs explications. Dans ce cas, les personnes faisant l'objet d'une sanction disciplinaire prononcée pour la première fois, se voient ouvrir la possibilité d'un recours devant la Commission d'appel, autrement composée le cas échéant. ".<br/>
<br/>
              3.	Il ressort des énonciations de l'ordonnance attaquée que MM. B... et D..., jockeys, ont fait l'objet d'une suspension de 15 jours chacun en raison de l'altercation qui les a opposés à l'issue d'une course hippique. Sur appel de M. D..., la suspension le concernant a été annulée tandis que celle de M. B... a été portée à six mois. L'association France Galop demande l'annulation de l'ordonnance du 2 octobre 2020 par laquelle la juge des référés du tribunal administratif de Cergy-Pontoise a suspendu la décision de la commission d'appel de France Galop du 19 août 2020 confirmant la sanction de suspension de six mois de M. B....<br/>
<br/>
              4.	Pour estimer qu'était de nature à créer un doute sérieux sur la légalité de la décision de la commission d'appel le moyen tiré de ce que celle-ci a méconnu l'article 234 du code des courses au galop, la juge des référés a pu, par une appréciation souveraine des faits qui est exempte de dénaturation, estimer que les faits portés à la connaissance de la commission n'étaient pas des faits " non examinés par les premiers juges ", au sens des dispositions de l'article 234 du code des courses au galop et en déduire sans erreur de droit, eu égard à son office, que le moyen tiré de la méconnaissance de ces dispositions était de nature à créer un doute sérieux sur la légalité de la décision attaquée. Elle a pu, en conséquence, en déduire, sans erreur de droit, au regard des mêmes dispositions, que le moyen tiré de ce que la sanction frappant M. B..., qui n'avait pas fait appel, ne pouvait être aggravée, était également de nature à créer un doute sérieux sur la légalité de la décision.<br/>
<br/>
              5.	Il résulte de tout ce qui précède que le pourvoi de France Galop ne peut qu'être rejeté, y compris les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association France Galop, la somme de 3 000 euros à ce titre à verser à M. B....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de France Galop est rejeté.<br/>
Article 2 : L'association France Galop versera la somme de 3 000 euros à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association France Galop et à M. A... B.... Copie en sera adressée à M. E... D....<br/>
              Délibéré à l'issue de la séance du 8 décembre 2021 où siégeaient : M. Jean-Yves Ollier, assesseur, présidant ; Mme Anne Courrèges, conseillère d'Etat et M. Paul Bernard, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 29 décembre 2021.<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Jean-Yves Ollier<br/>
 		Le rapporteur : <br/>
      Signé : M. Paul Bernard<br/>
                 La secrétaire :<br/>
                 Signé : Mme F... C...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
