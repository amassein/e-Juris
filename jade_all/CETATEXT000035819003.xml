<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035819003</ID>
<ANCIEN_ID>JG_L_2017_10_000000408750</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/81/90/CETATEXT000035819003.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/10/2017, 408750, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408750</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:408750.20171016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...et Mme B...A..., agissant en leur nom personnel et au nom de leurs enfants mineurs, ont demandé au juge des référés du tribunal administratif de Nantes, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision implicite de rejet résultant du silence gardé par la commission de recours contre les décisions de refus de visa d'entrée en France sur le recours qu'ils ont formé contre la décision de l'ambassadeur de France en Afghanistan refusant de leur délivrer un visa et d'enjoindre à l'autorité administrative de réexaminer leur demande de visa dans un délai de 15 jours, sous une astreinte de 100 euros par jour de retard.<br/>
<br/>
              Par une ordonnance n° 1609216 du 22 novembre 2016, le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 et 23 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande de suspension et d'enjoindre à l'autorité administrative de réexaminer leur demande dans le délai de huit jours à compter de la décision à intervenir, sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M.A..., ressortissant afghan, a été, entre le 10 octobre 2010 et le 30 septembre 2012, interprète auprès des forces armées françaises alors déployées en Afghanistan ; que les autorités françaises ont annoncé au mois de mai 2012 le retrait des forces françaises d'Afghanistan à partir du mois de juillet ; que M. A...a déposé une demande de visa auprès de l'ambassade de France en Afghanistan le 26 juillet 2015 ; qu'un refus de visa lui ayant été opposé le 10 mars 2016, il a saisi le 26 mai 2016 la commission de recours contre les décisions de refus de visa d'entrée en France ; que sa demande ayant fait l'objet d'une décision implicite de rejet, M. A... a demandé le 9 août 2016 à la commission la communication des motifs du rejet de sa demande ; qu'après avoir reçu communication de ces motifs, M. et Mme A...ont formé un recours contre ce refus de visa devant le tribunal administratif de Nantes ; qu'ils ont, parallèlement, saisi le juge des référés  du tribunal administratif de Nantes d'une demande, fondée sur les dispositions de l'article L. 521-1 du code de justice administrative, tendant à la suspension de l'exécution de la décision de refus de visa prise le 10 mars 2016 ; que, par une ordonnance du 22 novembre 2016, le juge des référés du tribunal administratif a rejeté cette demande de suspension, en estimant  qu'aucun des moyens soulevés par M. et Mme A...ne paraissait de nature, en l'état de l'instruction, à faire naître un doute sérieux quant à la légalité de la décision contestée ; que M. et Mme A...se pourvoient en cassation contre cette ordonnance ; <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3.	Considérant que, pour rejeter la demande de suspension que lui soumettaient M. et MmeA..., le juge des référés du tribunal administratif de Nantes a estimé qu'aucun des moyens qu'ils avaient soulevés ne paraissait de nature à faire naître un doute sérieux quant à la légalité de la décision de refus de visa, notamment celui tiré l'erreur manifeste d'appréciation des conséquences de ce refus ; <br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M. A...a servi en qualité d'interprète franco-dari auprès des forces françaises au sein d'un camp de formation de l'armée afghane à Kaboul puis dans le cadre de l'opération Epidote ; qu'il a participé à des patrouilles et opérations sur le terrain avec les forces françaises dans les provinces de Kapisa et de Kaboul ; qu'il fait valoir qu'il a fait l'objet de menaces de la part des talibans depuis la fin de son contrat et que, notamment, sa maison a été attaquée ; que, dans les circonstances de l'espèce, alors que la situation en Afghanistan s'est dégradée avec une recrudescence des violences qui exposent à des risques élevés les ressortissants afghans qui ont accordé leur concours à des forces armées étrangères, le juge des référés du tribunal administratif a dénaturé les faits de l'espèce en estimant que le moyen d'erreur manifeste n'était pas de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. A...est fondé à demander l'annulation de l'ordonnance qu'il attaque ;<br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur l'intervention : <br/>
<br/>
              6.	Considérant que la Ligue des droits de l'homme et l'association Avocats pour la défense des droits des étrangers justifient, eu égard à la nature et l'objet du litige, d'un intérêt suffisant pour intervenir au soutien de la demande de suspension ; que leur intervention est, par suite, recevable ; <br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              7.	Considérant, d'une part, que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'en l'espèce, il ressort des éléments versés au dossier que la condition d'urgence, eu égard aux risques dont fait état M. A...pour lui et sa famille, doit être regardée comme remplie ; <br/>
<br/>
              8.	Considérant, d'autre part, que le moyen tiré de l'erreur manifeste dont serait entachée la décision de refus de visa contestée, eu égard aux risques encourus par l'intéressé et sa famille du fait des missions accomplies, est, en l'état de l'instruction, propre à créer un doute sérieux sur sa légalité ; <br/>
<br/>
              9.	Considérant qu'il résulte de ce qui précède que M. A...est fondé à demander la suspension de l'exécution de la décision implicite née le 30 juillet 2016 de la commission de recours contre les décisions de refus de visa d'entrée en France ; qu'il y a lieu de prononcer l'injonction sollicitée par M. A...et de prescrire à l'autorité administrative de réexaminer la demande de visa de M.A..., de son épouse et de leurs enfants dans un délai d'un mois à compter de la notification de la présente décision, sans qu'il soit nécessaire de prononcer une astreinte ;<br/>
<br/>
              10.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 000 euros à verser à M. et Mme A...au titre, pour la première instance et la procédure devant le Conseil d'Etat, des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance n° 1609216 du 22 novembre 2016 du juge des référés du tribunal administratif de Nantes est annulée. <br/>
<br/>
Article 2 : L'intervention de la Ligue des droits de l'homme et de l'association Avocats pour la défense des droits des étrangers est admise. <br/>
<br/>
Article 3 : L'exécution de la décision implicite née le 30 juillet 2016 de la commission de recours contre les décisions de refus de visa d'entrée en France est suspendue. <br/>
<br/>
Article 4 : Il est enjoint au ministre d'Etat, ministre de l'intérieur, de réexaminer la demande de visa de M.A..., de Mme A...et de leurs enfants dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
Article 5 : L'Etat versera à M. et Mme A...une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 6 : La présente décision sera notifiée à M. C...A..., à Mme B...A..., à la Ligue des droits de l'homme, à l'association Avocats pour la défense des droits des étrangers et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
