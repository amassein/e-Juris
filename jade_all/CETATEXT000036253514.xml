<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253514</ID>
<ANCIEN_ID>JG_L_2017_12_000000409557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/35/CETATEXT000036253514.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème chambre jugeant seule, 22/12/2017, 409557, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:409557.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 5 avril 2017 au secrétariat du contentieux du Conseil d'Etat, le Conseil national de l'ordre des infirmiers demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les dispositions de l'article 20 de l'ordonnance n° 2017-192 du 16 février 2017 en tant qu'elles s'appliquent à l'ordre des infirmiers ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir les dispositions des articles 10 et 11 du décret n° 2017-319 du 10 mars 2017 en tant qu'elles s'appliquent à l'ordre des infirmiers ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la Constitution ;<br/>
<br/>
              - la loi n° 2016-41 du 26 janvier 2016 ;<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes. <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le Gouvernement a été habilité à modifier par ordonnance les dispositions législatives relatives aux ordres des professions de santé par l'article 212 de la loi du 26 janvier 2016 de modernisation de notre système de santé, aux termes duquel : " I. - Dans les conditions prévues à l'article 38 de la Constitution, dans un délai de dix-huit mois à compter de la promulgation de la présente loi, le Gouvernement est autorisé à prendre par ordonnances les mesures visant à adapter les dispositions législatives relatives aux ordres des professions de santé afin : / (...)2° De modifier la composition des conseils, la répartition des sièges au sein des différents échelons et les modes d'élection et de désignation de manière à simplifier les règles en ces matières et à favoriser l'égal accès des femmes et des hommes aux fonctions de membres dans l'ensemble des conseils ; / 3° De tirer les conséquences de la loi n° 2015-29 du 16 janvier 2015 relative à la délimitation des régions, aux élections régionales et départementales et modifiant le calendrier électoral sur l'organisation des échelons des ordres (...) " ; que l'article 20 de l'ordonnance du 16 février 2017 relative à l'adaptation des dispositions législatives relatives aux ordres des professions de santé, prise sur le fondement de cette habilitation, a renvoyé à un décret en Conseil d'Etat le soin d'organiser un dispositif transitoire, pouvant impliquer une prolongation ou une interruption des mandats en cours, permettant d'assurer la prise en compte lors du prochain renouvellement des instances ordinales de la modification des ressorts territoriaux des régions, de l'instauration d'un renouvellement par moitié et de la présentation des candidatures sous la forme de binômes de candidats des deux sexes ;  que les articles 10 et 11 du décret du 10 mars 2017, pris pour assurer la mise en oeuvre de ces dispositions à l'égard des infirmiers et des masseurs-kinésithérapeutes, prévoient le renouvellement total des conseils régionaux et national de l'ordre des infirmiers en 2017 ; que le Conseil national de l'ordre des infirmiers demande l'annulation pour excès de pouvoir des dispositions de l'article 20 de l'ordonnance du 16 février 2017 et des articles 10 et 11 du décret du 10 mars 2017 en tant qu'elles sont applicables à l'ordre des infirmiers ;<br/>
<br/>
              Sur la légalité externe des dispositions attaquées :<br/>
<br/>
              2. Considérant qu'il résulte des dispositions combinées des articles D. 4381-1 et D. 4381-2 du code de la santé publique que le Haut conseil des professions paramédicales est consulté sur : " (...) Les conditions d'exercice des professions paramédicales, l'évolution de leurs métiers, la coopération entre les professionnels de santé et la répartition de leurs compétences ; (...) La formation et les diplômes " ; que ni les dispositions de l'article 20 de l'ordonnance attaquée ni les dispositions des articles 10 et 11 du décret attaqué, qui organisent les modalités transitoires d'élection des instances de l'ordre des infirmiers, ne portent sur des questions soumises à cette obligation de consultation ; que, par suite, le moyen tiré du défaut de consultation du Haut conseil des professions paramédicales doit être écarté ;<br/>
<br/>
              Sur la légalité interne des dispositions attaquées : <br/>
<br/>
              3. Considérant que, sur le fondement des dispositions de la loi d'habilitation citée au point 1 ci-dessus, l'auteur de l'ordonnance attaquée a, notamment pour tirer les conséquences de la loi du 6 janvier 2015 relative à la délimitation des régions, aux élections régionales et départementales et modifiant le calendrier électoral, prévu un renouvellement total en 2017 des conseils régionaux et national de l'ordre des infirmiers qui se justifiait par la nécessité de prendre en compte des nouvelles limites régionales, alors même qu'il en résultait une interruption des mandats en cours avant leur terme ; qu'il n'a ainsi en tout état de cause pas méconnu les principes de droit au suffrage, d'égalité de suffrage et de périodicité raisonnable des élections ; que le requérant ne peut utilement invoquer un principe de " libre administration " des ordres qui ne résulte d'aucun principe ni d'aucune disposition législative ;<br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que le Conseil national de l'ordre des infirmiers n'est pas fondé à demander l'annulation des dispositions qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme qu'il demande soit mise à la charge de l'Etat, qui n'est pas la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête du Conseil national de l'ordre des infirmiers est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée au Conseil national de l'ordre des infirmiers.<br/>
Copie en sera adressée au Premier ministre et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
