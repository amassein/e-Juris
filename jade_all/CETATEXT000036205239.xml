<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036205239</ID>
<ANCIEN_ID>JG_L_2017_12_000000405916</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/20/52/CETATEXT000036205239.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 13/12/2017, 405916, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405916</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; HAAS</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405916.20171213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Poitiers d'annuler la décision du 31 mai 2012 par laquelle La Poste a refusé de faire droit à sa demande de remboursement de frais de déplacement et d'enjoindre à La Poste de lui verser la somme correspondante assortie des intérêts au taux légal et de leur capitalisation. <br/>
<br/>
              Par un jugement n° 1201955 du 5 février 2014, le tribunal administratif de Poitiers a annulé cette décision, condamné La Poste à procéder au paiement des frais de déplacement de Mme A...à compter du 2 novembre 2011, assortis des intérêts capitalisés au taux légal à compter du 31 juillet 2012, et renvoyé l'intéressée devant La Poste pour qu'il soit procédé à la liquidation de cette indemnité.<br/>
<br/>
              Par un arrêt n° 14BX01129 du 18 octobre 2016, la cour administrative d'appel de Bordeaux, sur appel de La Poste, a annulé ce jugement et rejeté la demande présentée par Mme A...devant le tribunal administratif. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 13 décembre 2016 et 13 mars et 24 novembre 2017, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de La Poste la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - la décision n° 890 du 15 juin 1995 du conseil d'administration de La Poste portant mise en oeuvre d'un nouveau système de prise en charge par La Poste des frais de déplacement en métropole et dans les départements d'outre-mer ;<br/>
              - la décision n° 352 du 15 février 2002 du conseil d'administration de La Poste instituant des indemnités liées aux réorganisations ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de MmeA..., et à Me Haas, avocat de La Poste ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme A..., agent titulaire exerçant à La Poste en qualité de guichetier, a été affectée au bureau Centre de Chauvigny (Vienne) à compter du 2 mai 2007, commune où elle réside, et a été affectée dans un bureau de poste à proximité situé sur la commune de Bonnes ; qu'à la suite d'une réorganisation territoriale, elle a été chargée, à compter du 2 novembre 2011, d'exercer ses fonctions dans trois autres bureaux de poste dits " points de contact ", situés à Saint-Pierre-de-Maillé, Saint-Savin et Saint-Julien-l'Ars, rattachés au bureau Centre de Chauvigny ; qu'elle a sollicité le remboursement de ses frais de transport entre son domicile et ces différents bureaux de poste, sur le fondement de la décision n° 890 du 15 juin 1995 du conseil d'administration de La Poste portant mise en oeuvre d'un nouveau système de prise en charge par La Poste des frais de déplacement en métropole et dans les départements d'outre-mer ; que, par une décision du 31 mai 2012, La Poste a rejeté sa demande ; que, par un jugement du 5 février 2014, le tribunal administratif de Poitiers a annulé cette décision et condamné La Poste à procéder au paiement des frais de déplacement de Mme A...à compter du 2 novembre 2011 ; que Mme A...demande l'annulation de l'arrêt du 18 octobre 2016 par lequel la cour administrative d'appel de Bordeaux, sur appel de La Poste, a annulé le jugement du tribunal administratif de Poitiers et a rejeté sa demande ;<br/>
<br/>
              2.	Considérant que, par une décision n° 890 du 15 juin 1995, le président du conseil d'administration de La Poste a fixé les modalités de prise en charge des frais de déplacement des agents de La Poste ; qu'en vertu du point 31 de cette décision, est considéré comme étant en déplacement " tout agent se déplaçant pour les besoins du service, hors de son agglomération de résidence personnelle et hors de son agglomération d'affectation " et que " tout déplacement professionnel pouvant donner lieu à indemnisation devra faire l'objet d'une information préalable " ; qu'en vertu du point 34 de la décision, si l'établissement d'un ordre de mission n'est plus impératif pour permettre la prise en charge des frais de déplacement, cette prise en charge suppose la présentation de pièces justificatives ; que, selon le point 61 de la décision, relatif à l'indemnisation des frais de transport, de tels frais occasionnés par les déplacements pour les besoins du service ou pour participer à une formation " sont pris en charge par voie de réquisition ou de bon de transport, ou remboursés sur présentation de pièces justificatives " ; qu'il résulte de ces dispositions, comme de l'ensemble des dispositions de la décision du 15 juin 1995, que la prise en charge des frais de déplacement organisée par cette décision vise les déplacements professionnels pour les besoins du service et ne saurait s'appliquer aux trajets habituels effectués par les agents chaque jour entre leur domicile et le lieu d'exercice de leurs fonctions ; <br/>
<br/>
              3.	Considérant que la cour administrative d'appel de Bordeaux a souverainement constaté que la demande de remboursement de frais de transport présentée par Mme A...visait à la prise en charge de déplacements quotidiens entre son domicile et ses différents lieux de travail ; qu'il résulte de ce qui a été dit précédemment que la prise en charge de tels frais ne relève pas du régime prévu par la décision du 15 juin 1995 ; que ce motif, qui répond à un moyen invoqué par La Poste devant les juges du fond et dont l'examen n'implique aucune appréciation nouvelle des circonstances de fait, doit être substitué au motif retenu par l'arrêt attaqué, dont il justifie le dispositif ; que, par suite, les autres moyens de Mme A...sont inopérants ;<br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède que le pourvoi de Mme A...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par La Poste au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
<br/>
Article 2 : Les conclusions de La Poste présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et à La Poste.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
