<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039666532</ID>
<ANCIEN_ID>JG_L_2019_12_000000412996</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/66/65/CETATEXT000039666532.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 24/12/2019, 412996, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412996</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412996.20191224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... B... a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir la décision du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 27 mars 2014 autorisant la société SEAC Guiraud frères à la licencier. Par un jugement n° 1402645 du 15 décembre 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA00514 du 1er juin 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 1er août, 20 octobre 2017, 13 août et 30 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société SEAC Guiraud frères la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de Mme B... et à la SCP Boullez, avocat de la société SEAC Guiraud frères.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 16 mai 2011, l'inspecteur du travail de la section 2 de l'unité territoriale de l'Aude a refusé d'autoriser la société SEAC Guiraud frères à licencier Mme B..., employée administrative et détentrice des mandats de représentant du personnel au comité d'hygiène, de sécurité et des conditions de travail et de conseillère du salarié. Par un jugement du 21 janvier 2014, le tribunal administratif de Montpellier a, sur demande de la société, d'une part, annulé la décision du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 25 novembre 2011 rejetant le recours hiérarchique formé par la société contre cette décision et, d'autre part, enjoint au ministre de prendre une nouvelle décision. Par une décision du 27 mars 2014 prise en exécution de ce jugement, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a annulé le refus de l'inspecteur du travail et autorisé le licenciement de Mme B.... Par un arrêt du 1er juin 2017 contre lequel Mme B... se pourvoit en cassation, la cour administrative d'appel de Marseille a rejeté son appel formé contre le jugement du 15 décembre 2015 par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation de cette décision du ministre.<br/>
<br/>
              Sur l'exception aux fins de non-lieu opposée par la ministre du travail :  <br/>
<br/>
              2. La ministre du travail soutient que l'arrêt du 7 avril 2015, par lequel la cour administrative d'appel de Marseille a, sur appel de Mme B..., d'une part, annulé le jugement du tribunal administratif de Montpellier du 21 janvier 2014 en ce qu'il a omis de statuer sur les conclusions de la société SEAC Guiraud frères dirigées contre la décision de l'inspecteur du travail du 16 mai 2011, d'autre part, rejeté ces conclusions après évocation et, enfin, rejeté le surplus des conclusions de l'appel, a rendu sans objet le pourvoi présenté par Mme B... contre l'arrêt du 1er juin 2017 par lequel la cour administrative d'appel a rejeté son appel formé contre le jugement du tribunal administratif du 15 décembre 2015 rejetant sa demande d'annulation de la décision du ministre du 27 mars 2014 prise en exécution de ce jugement du 21 janvier 2014. Toutefois, l'annulation, par la cour administrative d'appel, du jugement du tribunal administratif du 21 janvier 2014 en tant seulement qu'il a omis de statuer sur la légalité de la décision de l'inspecteur du travail du 16 mai 2011 et le rejet, après évocation, des conclusions dirigées contre cette décision n'ont pas eu pour effet de rétablir la décision du ministre du 25 novembre 2011, annulée par ce même jugement, rejetant le recours hiérarchique formé contre cette décision et n'ont pas entraîné, par suite, la disparition de l'ordonnancement juridique de la décision du 27 mars 2014 prise par le ministre en exécution de l'annulation de sa décision initiale. Dès lors, la ministre du travail n'est, en tout état de cause, pas fondée à soutenir que le pourvoi de Mme B... serait désormais privé d'objet. <br/>
<br/>
              Sur l'arrêt attaqué :<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la décision du directeur général du travail du 24 mars 2014, donnant délégation à M. A..., chef du bureau des recours, du soutien et de l'expertise juridique, pour signer toutes décisions au nom du ministre dans la limite de ses attributions, n'a été régulièrement publiée au Journal officiel de la République française que le 28 mars 2014, soit le lendemain de la date de la décision contestée dont M. A... est le signataire et n'avait, dès lors, pas pris effet à cette date. Contrairement à ce que soutient la société SEAC Guiraud frères en défense, la délégation de signature que l'intéressé avait antérieurement reçue par une décision prise le 18 janvier 2012 par le précédent directeur général du travail avait cessé de produire effet à compter du 21 mars 2014, date de publication au Journal officiel du décret nommant le nouveau directeur général, et ne lui donnait, dès lors, pas davantage compétence pour prendre la décision contestée. Par suite, en s'abstenant de soulever d'office l'incompétence, ressortant des pièces du dossier qui lui était soumis, dont était entachée la décision contestée devant elle, la cour administrative d'appel de Marseille a commis une erreur de droit. Il en résulte que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, Mme B... est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il résulte de ce qui a été dit au point 3 que la décision du 27 mars 2014 autorisant la société SEAC Guiraud frères à licencier Mme B... est entachée d'incompétence. Par suite, sans qu'il soit besoin d'examiner les moyens de sa requête, Mme B... est fondée à soutenir que c'est à tort que le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation de cette décision. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de la société SEAC Guiraud frères les sommes de 1 750 euros chacun à verser à Mme B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de la requérante qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 1er juin 2017 et le jugement du tribunal administratif de Montpellier du 15 décembre 2015 sont annulés.<br/>
Article 2 : La décision du ministre du travail, de l'emploi, de la formation professionnel et du dialogue social du 27 mars 2014 est annulée.<br/>
Article 3 : L'Etat et la société SEAC Guiraud frères verseront à Mme B... les sommes de 1 750 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société SEAC Guiraud frères présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme C... B..., à la ministre du travail et à la société SEAC Guiraud frères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
