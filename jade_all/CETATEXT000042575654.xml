<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575654</ID>
<ANCIEN_ID>JG_L_2020_11_000000417165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/56/CETATEXT000042575654.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 27/11/2020, 417165</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:417165.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Me B... et Me A..., en leur qualité de mandataires liquidateurs de la société AOM Air Liberté, ont demandé au tribunal administratif de Melun de condamner l'Etat à leur verser la somme de 163 701 802 euros en réparation du préjudice qu'auraient subi les créanciers de cette société à raison d'aides accordées par l'Etat ayant contribué à aggraver son passif alors que sa situation économique était irrémédiablement compromise. Par un jugement n° 0809539 du 25 juin 2014, le tribunal administratif de Melun a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 14PA03744 du 9 novembre 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par Me B... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 9 janvier et 9 avril 2018 et le 17 juin 2019 au secrétariat du contentieux du Conseil d'Etat, Me B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 20 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, auditeur,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de Me B..., mandataire liquidateur de la société AOM Air Liberté ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 19 juin 2001, le tribunal de commerce de Créteil a ouvert une procédure de redressement judiciaire à l'encontre des sociétés AOM Air Liberté, Air Liberté, Minerve Antilles Guyane, TAT European Airlines, Hotavia Restauration Services HRS et Air Liberté Industrie, dont le groupe Swissair était actionnaire. Par un jugement du 27 juillet 2001, ce tribunal a arrêté un plan de cession des actifs de ces sociétés au profit de la société Holco, puis a ensuite, par un jugement du 1er août 2001, homologué le protocole transactionnel conclu entre la société Holco et le groupe Swissair prévoyant le versement par ce dernier de contributions financières pour assurer la restructuration et la poursuite des activités reprises. Confrontée à la défaillance du groupe Swissair à lui verser l'intégralité de ces contributions, la société d'exploitation AOM Air Liberté, créée par la société Holco pour la reprise des actifs cédés, a sollicité l'aide des pouvoirs publics afin de réunir les sommes nécessaires à la restructuration de son activité.<br/>
<br/>
              2. Par un contrat du 9 janvier 2002, l'Etat a accordé à la société d'exploitation AOM Air Liberté un prêt d'un montant de 16 500 000 euros pour une durée de six mois. Par un avenant du 28 février 2002, le montant de ce prêt a été porté à 30 500 000 euros. Sa durée a été prolongée de quatre mois par un avenant du 25 septembre 2002, puis jusqu'au 9 janvier 2003 par un nouvel avenant signé en décembre 2002. L'Etat a par ailleurs accordé à la société plusieurs moratoires pour le paiement de la part patronale des cotisations de sécurité sociale et des taxes et redevances aéroportuaires dont elle était redevable.<br/>
<br/>
              3. Le directeur général de l'aviation civile a retiré la licence d'exploitation de transporteur aérien de la société d'exploitation AOM Air Liberté à compter du 6 février 2003. Par un jugement du 17 février 2003, le tribunal de commerce de Créteil a ouvert une procédure de liquidation judiciaire à son égard.<br/>
<br/>
              4. Les mandataires liquidateurs de la société d'exploitation AOM Air Liberté, agissant au nom et pour le compte des créanciers de la société, ont réclamé à l'Etat l'indemnisation du préjudice qu'auraient subi ces derniers du fait que les aides accordées auraient, en prolongeant l'activité de la société, contribué à aggraver son passif, alors que sa situation était, selon eux, déjà irrémédiablement compromise aux dates de leur attribution.  Me B..., devenu l'unique mandataire liquidateur de la société d'exploitation AOM Air Liberté, demande l'annulation de l'arrêt du 9 novembre 2017 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'il avait formé contre le jugement du tribunal administratif de Melun du 25 juin 2014, rejetant ses conclusions aux fins de condamnation de l'Etat.<br/>
<br/>
              5. L'octroi d'une aide publique à une entreprise, alors même que sa situation était irrémédiablement compromise à la date à laquelle elle a été accordée, ne permet de caractériser l'existence d'une faute que si cette aide, qui n'est pas régie par les dispositions de l'article L 650-1 du code du commerce relative à la responsabilité des créanciers soumis aux règles commerciales, a été accordée en méconnaissance des textes applicables ou qu'il est manifeste, qu'à la date de son octroi, cette aide était insusceptible de permettre la réalisation d'un objectif d'intérêt général ou que son montant était sans rapport avec la poursuite de cet objectif. Saisi d'une demande indemnitaire sur le fondement d'une aide illégale accordée à une entreprise, il appartient au juge d'apprécier si le préjudice allégué présente un caractère certain et s'il existe un lien de causalité direct entre la faute de l'administration et le préjudice allégué par les requérants. <br/>
<br/>
              6. Il s'ensuit qu'en jugeant que les aides publiques octroyées à une entreprise privée dont la situation était irrémédiablement compromise à la date de l'octroi de ces aides ne sont pas susceptibles d'engager la responsabilité pour faute de la personne publique du fait que ces aides ont, en permettant à l'entreprise de poursuivre son activité, contribué à creuser son passif, sauf dans l'hypothèse où l'entreprise n'était pas à même de refuser ces aides, en raison notamment d'une contrainte exercée à son égard, d'un dol ou d'une immixtion caractérisée dans sa gestion, ou si les garanties prises étaient disproportionnées aux concours accordés, sans mettre en oeuvre les principes rappelés au point 5 ci-dessus, la cour a commis une erreur de droit.<br/>
<br/>
              7. Il résulte de ce qui précède que Me B..., agissant en qualité de mandataire liquidateur de la société d'exploitation AOM Air Liberté, est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Me B..., agissant en qualité de mandataire liquidateur de la société d'exploitation AOM Air Liberté, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 9 novembre 2017 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à Me B..., agissant en qualité de mandataire liquidateur de la société d'exploitation AOM Air Liberté, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Me B..., agissant en qualité de mandataire liquidateur de la société d'exploitation AOM Air Liberté et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-03-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. MESURES D'INCITATION. SUBVENTIONS. - AIDE PUBLIQUE À UNE ENTREPRISE DONT LA SITUATION EST IRRÉMÉDIABLEMENT COMPROMISE - ENGAGEMENT DE LA RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE - 1) RÉGIME DE FAUTE SIMPLE - 2) A) APPLICATION DU RÉGIME DE L'ACTION EN SOUTIEN ABUSIF (ART. L. 650-1 DU CODE DE COMMERCE) - ABSENCE - B) CRITÈRES [RJ1] - 3) PRÉJUDICE CERTAIN ET LIEN DIRECT DE CAUSALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-07-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. SERVICE PUBLIC ADMINISTRATIF. - RECOURS TENDANT À LA RÉPARATION DES CONSÉQUENCES DOMMAGEABLES DE L'OCTROI PAR L'ETAT D'UNE AIDE PUBLIQUE À UNE ENTREPRISE - AIDE À UNE ENTREPRISE EN DIFFICULTÉ AYANT POUR BUT D'INTÉRÊT GÉNÉRAL LE MAINTIEN DU TISSU INDUSTRIEL ET DE L'EMPLOI - COMPÉTENCE ADMINISTRATIVE (SOL. IMP.).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-02-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. APPLICATION D'UN RÉGIME DE FAUTE SIMPLE. - RESPONSABILITÉ DU FAIT DE L'OCTROI D'UNE AIDE PUBLIQUE À UNE ENTREPRISE.
</SCT>
<ANA ID="9A"> 14-03-02 1) La responsabilité d'une personne publique du fait de l'octroi d'une aide publique à une entreprise n'est pas subordonnée à l'existence d'une faute lourde.,,,2) L'octroi d'une aide publique à une entreprise, alors même que sa situation était irrémédiablement compromise à la date à laquelle elle a été accordée, ne permet de caractériser l'existence d'une faute que si cette aide, a) qui n'est pas régie par les dispositions de l'article L. 650-1 du code du commerce relative à la responsabilité des créanciers soumis aux règles commerciales, b) a été accordée en méconnaissance des textes applicables ou qu'il est manifeste, qu'à la date de son octroi, cette aide était insusceptible de permettre la réalisation d'un objectif d'intérêt général ou que son montant était sans rapport avec la poursuite de cet objectif.,,,3) Saisi d'une demande indemnitaire sur le fondement d'une aide illégale accordée à une entreprise, il appartient au juge d'apprécier si le préjudice allégué présente un caractère certain et s'il existe un lien de causalité direct entre la faute de l'administration et le préjudice allégué par les requérants.</ANA>
<ANA ID="9B"> 17-03-02-07-01 La juridiction administrative est compétente pour connaître de l'action tendant à l'engagement de la responsabilité de l'Etat à raison des conséquences dommageables de l'octroi à une entreprise en difficulté d'une aide publique subordonnée à des conditions tendant à maintenir le tissu industriel et l'emploi (sol. impl.).</ANA>
<ANA ID="9C"> 60-01-02-02-02 La responsabilité d'une personne publique du fait de l'octroi d'une aide publique à une entreprise n'est pas subordonnée à l'existence d'une faute lourde.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de la responsabilité du créancier sur le fondement de l'article L. 650-1 du code de commerce, Cass. com., 27 mars 2012, n° 10-20.0077, Bull. 2012, IV, n° 68.,.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
