<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026502062</ID>
<ANCIEN_ID>JG_L_2012_10_000000357886</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/20/CETATEXT000026502062.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 16/10/2012, 357886, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357886</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques-Henri Stahl</PRESIDENT>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:357886.20121016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 23 mars et 11 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Mutlu B, demeurant au ... ; M. B demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 13 janvier 2012 accordant son extradition aux autorités turques ;    <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              Vu le code pénal ; <br/>
<br/>
              Vu le code de procédure pénale ;  <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, Conseiller d'Etat,<br/>
<br/>
              - les observations de Me Spinosi, avocat de M. B,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à Me Spinosi, avocat de M. B ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              Considérant qu'il ressort des mentions de l'ampliation du décret attaqué figurant au dossier, certifiée conforme par le secrétaire général du gouvernement, que ce décret a été signé par le Premier ministre et contresigné par le garde des sceaux, ministre de la justice ; que l'ampliation notifiée à M. B n'avait pas à être revêtue de ces signatures ; <br/>
<br/>
              Considérant qu'aux termes de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 : " (...) 2. Il sera produit à l'appui de la requête :/ a. l'original ou l'expédition authentique soit d'une décision de condamnation exécutoire, soit d'un mandat d'arrêt ou de tout autre acte ayant la même force, délivré dans les formes prescrites par la loi de la Partie requérante ; (...)/ c. une copie des dispositions légales applicables ou, si cela n'est pas possible, une déclaration sur le droit applicable, ainsi que le signalement aussi précis que possible de l'individu réclamé et tous autres renseignements de nature à déterminer son identité et sa nationalité. " ; que ces stipulations sont, s'agissant des pièces à produire à l'appui de la demande d'extradition, seules applicables en l'espèce à l'exclusion des dispositions de l'article 696-8 du code de procédure pénale, qui n'ont qu'un caractère supplétif ;    <br/>
<br/>
              Considérant que si M. B fait valoir que la demande d'extradition ne comporte ni l'expédition authentique de la décision de condamnation à l'origine de la demande d'extradition ainsi que la justification de son caractère exécutoire, ni les documents nécessaires à son identification, il ressort des pièces du dossier qu'étaient jointes à la demande d'extradition et ont été produites devant la chambre de l'instruction de la cour d'appel de Grenoble la copie authentifiée par les autorités turques, d'une part, de la décision du tribunal correctionnel de Marmaris du 19 septembre 2005 condamnant M. B à la peine de trois ans et six mois d'emprisonnement et, d'autre part, de l'arrêt de la cour de cassation turque du 9 février 2010 rejetant son pourvoi et conférant ainsi à la décision de première instance un caractère définitif ; que ces deux décisions étaient accompagnées de leur traduction certifiée conforme à l'original ; que ces copies, ainsi authentifiées par les autorités de l'Etat requérant, constituent des expéditions authentiques au sens des stipulations du paragraphe 2 de l'article 12 de la convention européenne d'extradition ; que, par ailleurs, ces stipulations n'imposent pas la production d'une photographie et d'une fiche anthropométrique de l'intéressé ; que, par suite, le moyen tiré de la méconnaissance des stipulations de l'article 12 de la convention européenne d'extradition ne peut qu'être écarté ;  <br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              Considérant qu'aux termes de l'article 10 de la convention européenne d'extradition : " L'extradition ne sera pas  accordée si la prescription  de l'action ou de la peine  est acquise d'après la législation, soit  de la Partie  requérante, soit de la Partie requise " ; que si M. B fait valoir que la prescription serait acquise dès lors qu'aucun acte n'a été effectué entre le 19 septembre 2005, date de sa condamnation par le tribunal correctionnel de Marmaris, et le 9 février 2010, date du rejet de son pourvoi en cassation, cette circonstance est dépourvue de conséquence s'agissant d'une extradition accordée pour l'exécution d'une peine ; que, par suite, le moyen tiré de la méconnaissance de l'article 10 de la convention européenne d'extradition ne peut qu'être écarté ; <br/>
<br/>
              Considérant que si M. B soutient que les conditions dans lesquelles la procédure qui a conduit à sa condamnation en Turquie ne satisfont pas à l'exigence d'un procès équitable et méconnaissent, par suite, les réserves émises par la France lors de la ratification de la convention européenne d'extradition et les stipulations de son deuxième protocole additionnel ainsi que celles de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, il n'assortit ses allégations d'aucun élément de nature à les établir ; <br/>
<br/>
              Considérant que si une décision d'extradition est susceptible de porter atteinte, au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au droit au respect de la vie familiale, cette mesure trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, tant le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France que l'exécution, par les mêmes personnes, des condamnations pénales prononcées contre elles à l'étranger pour de tels crimes ou délits ; que, si M. B soutient être marié depuis 2009 avec une ressortissante française, cette circonstance n'est pas à elle seule de nature à faire obstacle, dans l'intérêt de l'ordre public, à l'exécution de son extradition ; que, dès lors, le moyen tiré de la violation de l'article 8 de la convention susvisée doit être écarté ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. B n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 13 janvier 2012 accordant son extradition aux autorités turques ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande M. B au titre des frais avancés par lui et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Mutlu B et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
