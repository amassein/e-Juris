<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036660390</ID>
<ANCIEN_ID>JG_L_2018_02_000000402778</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/66/03/CETATEXT000036660390.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 28/02/2018, 402778, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402778</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:402778.20180228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) Saint-Florent a demandé au tribunal administratif de Melun de prononcer la réduction de la cotisation de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2012 dans les rôles de la commune de Dammarie-les-Lys (Seine-et-Marne) et de lui accorder le sursis de paiement de l'imposition contestée. Par un jugement n° 1310362 du 23 juin 2016, ce tribunal a rejeté sa demande et constaté qu'il n'y avait pas lieu de statuer sur la demande de sursis de paiement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 23 août et 23 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la SCI Saint-Florent demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Saint-Florent ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société civile immobilière (SCI) Saint-Florent est propriétaire d'un immeuble à Dammarie-les-Lys (Seine-et-Marne). En vue de l'établissement de la taxe foncière sur les propriétés bâties au titre de l'année 2012, l'administration a évalué la valeur locative de ce bien par comparaison avec le local-type n° 12 qui était inscrit comme entrepôt au procès-verbal des opérations de révision foncière de la commune. Par un jugement du 23 juin 2016, le tribunal administratif de Melun a rejeté la demande de la société tendant à la réduction de cette imposition et prononcé un non-lieu à statuer sur la demande de sursis de paiement dont cette demande était assortie. La société demande l'annulation de l'article 2 de ce jugement.  <br/>
<br/>
              2. Aux termes de l'article 1498 du code général des impôts, la valeur locative de tous les biens autres que les locaux d'habitation ou à usage professionnel visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : " (...) / 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ". <br/>
<br/>
              3. Pour écarter le moyen tiré de ce que le local-type n° 12 ne constituait plus un terme de comparaison pertinent pour les immeubles à usage d'entrepôt au 1er janvier 2012, le tribunal a jugé que l'administration n'avait à aucun moment constaté la perte de représentativité de ce local. Toutefois, il ressort des pièces du dossier soumis au juge du fond que si ce local-type était inscrit comme entrepôt sur le procès-verbal en 1973, la société avait apporté des éléments non contredits par l'administration selon lesquels ce local avait été utilisé depuis 1993 pour une activité de fourniture, de fabrication et de pose de supports publicitaires et que, si cette activité avait pris fin en 2009, rien ne faisait apparaître que l'immeuble aurait été réaffecté ultérieurement à un usage d'entrepôt. Par suite, alors que la société apportait des éléments à l'appui de sa contestation de la pertinence du choix du local-type n° 12 comme terme de comparaison, le tribunal administratif, en omettant de se prononcer sur ces éléments, a insuffisamment motivé son jugement .  <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la SCI Saint-Florent est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros  à verser à la SCI Saint-Florent au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Melun est annulé.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure au tribunal administratif de Melun.<br/>
Article 3 : L'Etat versera la somme de 2 000 euros à la SCI Saint-Florent au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société civile immobilière Saint-Florent et  au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
