<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852134</ID>
<ANCIEN_ID>JG_L_2021_07_000000450500</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/21/CETATEXT000043852134.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 22/07/2021, 450500, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450500</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450500.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une saisine enregistrée le 18 décembre 2020, la Commission nationale des comptes de campagne et des financements politiques a transmis au tribunal administratif de Lyon, en application de l'article L. 52-15 du code électoral, sa décision du 5 novembre 2020 par laquelle elle a constaté le dépôt hors délai du compte de campagne de Mme A... B..., candidate à l'élection des conseillers municipaux et communautaires de la commune de Roche-la-Molière (Loire) qui s'est tenue le 15 mars 2020.<br/>
<br/>
              Par un jugement n° 2009218 du 23 février 2021, le tribunal administratif de Lyon a jugé que c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques avait constaté le dépôt hors délai du compte de campagne de Mme B... et l'a déclarée en conséquence inéligible pour une durée de six mois à compter de la date à laquelle ce jugement deviendrait définitif.<br/>
<br/>
              Par une requête, enregistrée le 9 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat d'annuler ce jugement en tant qu'il la déclare inéligible pour une durée de six mois.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 118-3 du code électoral, dans sa rédaction antérieure à la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. (...) : Saisi dans les mêmes conditions, le juge de l'élection peut prononcer l'inéligibilité du candidat ou des membres du binôme de candidats qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. ". Aux termes de ce même article dans sa rédaction issue de cette loi : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; (...) ".<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 15 de la loi du 2 décembre 2019 : " La présente loi, à l'exception de l'article 6, entre en vigueur le 30 juin 2020 ". Aux termes du XVI de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " A l'exception de son article 6, les dispositions de la loi n° 2019-1269 du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral ne sont pas applicables au second tour de scrutin régi par la présente loi ". Il résulte de ces dispositions que les dispositions de la loi du 2 décembre 2019 modifiant celles du code électoral, à l'exception de son article 6, ne sont pas applicables aux opérations électorales en vue de l'élection des conseillers municipaux et communautaires organisées les 15 mars et 28 juin 2020, y compris en ce qui concerne les comptes de campagne. <br/>
<br/>
              3. Toutefois, l'inéligibilité prévue par les dispositions de l'article L. 118-3 du code électoral constitue une sanction ayant le caractère d'une punition. Il incombe, dès lors au juge de l'élection, lorsqu'il est saisi de conclusions tendant à ce qu'un candidat dont le compte de campagne est rejeté soit déclaré inéligible et à ce que son élection soit annulée, de faire application, le cas échéant, d'une loi nouvelle plus douce entrée en vigueur entre la date des faits litigieux et celle à laquelle il statue. Le législateur n'ayant pas entendu, par les dispositions citées au point 2, faire obstacle à ce principe, le juge doit faire application aux opérations électorales mentionnées à ce même point des dispositions de cet article dans sa rédaction issue de la loi du 2 décembre 2019. En effet, cette loi nouvelle laisse désormais au juge, de façon générale, une simple faculté de déclarer inéligible un candidat en la limitant aux cas où il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, alors que l'article L. 118-3 dans sa version antérieure, d'une part, prévoyait le prononcé de plein droit d'une inéligibilité lorsque le compte de campagne avait été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité et, d'autre part,  n'imposait pas cette dernière condition pour que puisse être prononcée une inéligibilité lorsque le candidat n'avait pas déposé son compte de campagne dans les conditions et le délai prescrit par l'article L. 52-12 de ce même code.<br/>
<br/>
              4. Aux termes de l'article L. 52-12 du code électoral, dans sa rédaction antérieure à l'intervention de la loi du 2 décembre 2019 : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...). Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés (...) ". Aux termes de l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté (...), la commission saisit le juge de l'élection (...) ".<br/>
<br/>
              5. En application des dispositions précitées de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré.<br/>
<br/>
              6. Il résulte de l'instruction que Mme A... B..., candidate tête de liste aux élections qui se sont déroulées le 15 mars 2020 en vue de la désignation des conseillers municipaux de la commune de Roche-la-Molière, a déposé son compte de campagne le 13 juillet 2020, soit quelques jours après l'expiration du délai prescrit par les dispositions précitées de l'article 19 de la loi du 23 mars 2019. Par suite, c'est à bon droit que la Commission nationale des comptes de campagne a constaté que ce compte avait été déposé hors délai.<br/>
<br/>
              7. Toutefois, eu égard, dans les circonstances de l'espèce, au caractère non délibéré du manquement en cause, celui-ci ne justifie pas que Mme B... soit déclarée inéligible en application des dispositions précitées de l'article L. 118-3 du code électoral.<br/>
              8. Il résulte de tout ce qui précède que Mme B... est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lyon l'a déclarée inéligible pour une durée de six mois.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Lyon du 23 février 2021 est annulé.<br/>
Article 2 : La saisine de la Commission nationale des comptes de campagne et des financements politiques est rejetée.<br/>
Article 3 : La présente décision sera notifiée à Mme A... B... et à la Commission nationale des comptes de campagne et des financements politiques. Copie en sera adressée, pour information, au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
