<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036976455</ID>
<ANCIEN_ID>JG_L_2018_05_000000420439</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/97/64/CETATEXT000036976455.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 29/05/2018, 420439, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420439</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:420439.20180529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre au préfet des Hautes-Pyrénées d'enregistrer sans délai sa demande d'asile en procédure normale et, d'autre part, d'enjoindre au préfet des Hautes-Pyrénées de lui délivrer sans délai une autorisation provisoire de séjour jusqu'à ce qu'il soit statué sur sa demande d'asile. Par une ordonnance n° 1800865 du 23 avril 2018, le juge des référés du tribunal administratif de Pau a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 7 et 22 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) d'enjoindre au préfet des Hautes-Pyrénées de le placer sans délai en procédure d'asile normale et de lui délivrer une autorisation provisoire de séjour jusqu'à ce qu'il soit définitivement statué sur sa demande d'asile ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que, depuis son retour d'Italie, il se trouve dans une situation de grande précarité en raison du refus d'examen de sa demande d'asile et d'accès aux conditions matérielles d'accueil ; <br/>
              - les démarches qu'il a entreprises auprès des autorités françaises depuis son retour d'Italie pour parvenir à déposer sa demande d'asile ne peuvent lui être reprochées pour contester l'urgence ;<br/>
              - il est porté une atteinte grave et manifestement illégale à son droit d'asile et son corolaire le droit de solliciter l'asile dès lors que, après que les autorités italiennes ont refusé d'enregistrer sa demande d'asile et ont pris à son encontre une décision d'expulsion, les autorités françaises refusent aussi d'enregistrer sa demande d'asile en France, qu'il tente de déposer sa demande depuis plus d'un an et qu'en cas de retour en Italie, il risque d'être renvoyé au Soudan ; <br/>
              - le ministre n'apporte aucune précision sur les agissements des autorités italiennes, par ailleurs, la décision d'expulsion ne mentionne en rien une renonciation à demander l'asile, or il ne peut lui être demandé d'apporter la preuve négative de ce qu'il n'aurait pas renoncé à demander l'asile ;<br/>
              - il n'appartenait pas au juge des référés d'apprécier s'il remplissait les conditions pour se voir accorder l'asile mais simplement de vérifier le respect de son droit à déposer une demande d'asile.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 18 mai 2018, le ministre d'Etat, ministre de l'intérieur conclut à l'annulation de l'ordonnance en tant que le juge des référés a considéré que la situation de M. A...caractérisait une situation d'urgence au sens de l'article L. 521-2 du code de justice administrative et au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens de la requête ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, M. A...et, d'autre part, le ministre d'Etat, ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 23 mai 2018 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Rousseau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Tout étranger présent sur le territoire français et souhaitant demander l'asile se présente en personne à l'autorité administrative compétente, qui enregistre sa demande et procède à la détermination de l'Etat responsable en application du règlement (UE) n° 604/2013 du Parlement européen et du Conseil, du 26 juin 2013, établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers ou un apatride, ou en application d'engagements identiques à ceux prévus par le même règlement, dans des conditions fixées par décret en Conseil d'Etat. / (...) /Lorsque l'enregistrement de sa demande d'asile a été effectué, l'étranger se voit remettre une attestation de demande d'asile... " ; qu'aux termes de l'article L. 741-2 de ce code : " Lorsque l'examen de la demande d'asile relève de la compétence de la France, l'étranger introduit sa demande auprès de l'Office français de protection des réfugiés et apatrides (...). L'autorité administrative compétente informe immédiatement l'office de l'enregistrement de la demande et de la remise de l'attestation de demande d'asile. " ; qu'aux termes de l'article L. 742-1 du même code : " Lorsque l'autorité administrative estime que l'examen d'une demande d'asile relève de la compétence d'un autre Etat qu'elle entend requérir, l'étranger bénéficie du droit de se maintenir sur le territoire français jusqu'à la fin de la procédure de détermination de l'Etat responsable de l'examen de sa demande et, le cas échéant, jusqu'à son transfert effectif à destination de cet Etat. L'attestation délivrée en application de l'article L. 741-1 mentionne la procédure dont il fait l'objet. Elle est renouvelable durant la procédure de détermination de l'Etat responsable et, le cas échéant, jusqu'à son transfert effectif à destination de cet Etat. " ;<br/>
<br/>
              3. Considérant que M.A..., ressortissant soudanais, déclare être entré irrégulièrement sur le territoire français le 3 mars 2017 ; qu'il résulte de l'instruction qu'il a présenté une demande d'asile le 21 avril 2017 à la préfecture des Hautes-Pyrénées ; que la consultation du fichier " Eurodac " ayant permis d'établir que ses empreintes digitales avaient été relevées par les autorités italiennes, une demande de prise en charge a été adressée à l'Italie le 16 juin 2017 ; que cette demande ayant été tacitement acceptée par  les autorités italiennes, le préfet des Hautes-Pyrénées a, le 20 septembre 2017, pris à l'égard de M. A...un arrêté de transfert vers l'Italie ; qu'après le rejet, par un jugement du tribunal administratif de Pau du 19 octobre 2017, de sa demande d'annulation de cette décision, M. A...a, le 21 novembre 2017, regagné l'Italie et s'est présenté aux autorités de police de ce pays ; que, par une décision du 22 novembre 2017 produite par le requérant et dont la traduction informelle mentionne que l'un des motifs est que son rapatriement avait été rendu impossible par l'absence de moyen de transport, les autorités italiennes ont obligé M. A...à quitter le territoire italien ; que l'intéressé est alors entré à nouveau irrégulièrement en France ; que, convoqué le 16 février 2018 au guichet unique des demandeurs d'asile de la préfecture de Haute Garonne, il a à nouveau été placé sous procédure " Dublin III ", l'attestation prévue aux dispositions précitées de l'article L. 742-1 lui étant remise par le préfet de la Haute Garonne puis renouvelée jusqu'au 17 juillet 2018 ; que, le 5 avril 2018, les autorités italiennes ont été saisies d'une seconde demande de prise en charge à laquelle, à la date de la présente décision, elle n'ont pas apporté de réponse ; <br/>
<br/>
              4. Considérant que, par une requête en date du 18 avril 2017, M. A...a saisi le juge des référés du tribunal administratif de Pau aux fins que soit enjoint au préfet des Hautes-Pyrénées, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, d'enregistrer sa demande d'asile en procédure normale ; que, par une ordonnance n° 1800865 du 23 avril 2018, le juge des référés du tribunal administratif de Pau a rejeté sa demande ; que M. A...relève appel de cette ordonnance ; qu'en outre,  dans son mémoire en défense, le ministre d'Etat , ministre de l'intérieur demande l'annulation de l'ordonnance en ce que le juge des référés a considéré qu'au regard de la situation de M.A..., la condition d'urgence était remplie ;<br/>
<br/>
              5. Considérant que l'intérêt à faire appel d'un jugement s'apprécie par rapport à son dispositif et non à ses motifs ; qu'ainsi qu'il a été dit ci-dessus, le juge des référés du tribunal administratif de Pau a, par l'ordonnance attaquée, rejeté la demande de M.A... ; qu'il résulte de ce qui précède qu'alors même qu'avant de fonder ce dispositif sur l'absence d'atteinte grave et manifeste au droit d'asile, le juge des référés a admis, eu égard à la situation de M.A..., que la condition particulière d'urgence prévue par l'article L. 521-2 du code de justice administrative était remplie, le ministre d'Etat, ministre de l'intérieur ne justifie pas d'un intérêt à faire appel ; que, par suite, ses conclusions tendant à l'annulation de l'ordonnance doivent rejetées ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que l'attestation de demande d'asile dont M. A...est titulaire ne lui confère aucun droit à se maintenir sur le territoire français pendant l'instruction de sa demande d'asile et l'expose à ce que soit prise à son égard une décision de renvoi vers l'Italie, pays où il déclare n'avoir pu faire enregistrer sa demande d'asile et où il a fait l'objet d'une décision lui ordonnant de quitter le territoire ; qu'en outre, il soutient, sans être utilement contredit, qu'à raison du refus d'instruire sa demande d'asile opposé par les autorités françaises, il se trouve dans une situation de grande précarité ; que, contrairement à ce qu'indique le ministre de l'Intérieur, il n'a pas fait obstacle par son comportement à l'exécution des décisions des autorités françaises le concernant ; que, par suite, et ainsi que l'a retenu à bon droit le juge des référés du tribunal administratif de Pau, la condition particulière d'urgence prévue par l'article L. 521-2 du code de justice administrative, qui doit être appréciée compte tenu des circonstances concrètes de chaque espèce, est remplie ;<br/>
<br/>
              7. Considérant qu'ainsi qu'il a été rappelé au point 3, M. A...s'était, en exécution d'une décision de transfert vers l'Italie, pays responsable de l'examen de sa demande d'asile, présenté le 21 novembre 2017 devant les autorités de ce pays ; que toutefois, le 22 novembre 2017, il a fait l'objet d'une décision d'expulsion notifiée le même jour ; que lorsque M.A..., revenu en France, a sollicité que lui soit octroyé l'asile selon la procédure prévue à l'article L. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, il a, d'une part, fait état de cette décision d'expulsion qu'il a produite accompagnée d'une traduction libre, d'autre part, indiqué qu'il s'était heurté en Italie à un refus d'enregistrer et d'instruire sa demande d'asile et, enfin, soutenu qu'en cas de retour dans ce pays, il risquait d'être renvoyé au Soudan ; que, devant le juge des référés, M. A...a soutenu que, par suite, il existait un risque sérieux que sa demande d'asile ne soit pas traitée par les autorités italiennes dans des conditions conformes à l'ensemble des garanties exigées par le respect du droit d'asile ; que l'administration n'a pas contesté, ni dans ses écritures, ni lors de l'audience devant le Conseil d'Etat, l'existence de la décision d'expulsion ; que, si elle s'est prévalue des principes généraux d'organisation de la procédure administrative en Italie pour en déduire que M. A...avait nécessairement renoncé à demander l'asile dans ce pays, alors même que la décision d'expulsion produite par le requérant ne fait pas état d'une telle renonciation, elle n'a apporté aucune précision à l'appui de cette affirmation et n'a produit aucun document officiel émanant des autorités italiennes concernant M.A... ; que, dans de telles circonstances, le refus d'enregistrer la demande de M. A...au titre de la procédure prévue par l'article L. 741-2 du code de l'entrée et du séjour des étrangers et de l'asile doit être regardé comme portant une atteinte grave et manifestement illégale à son droit, constitutionnellement garanti, de solliciter le statut de réfugié ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. A...est fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Pau a rejeté sa demande ; que l'ordonnance attaquée doit par suite être annulée ; qu'il y a lieu d'enjoindre au préfet des Hautes-Pyrénées d'enregistrer la demande d'asile de M. A...en procédure normale et de lui délivrer l'attestation de demandeur d'asile afférente, dans un délai de quinze jours suivant la notification de la présente ordonnance ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, sans qu'il soit besoin d'admettre provisoirement M. A...au bénéfice de l'aide juridictionnelle, de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance n° 1800865 du 23 avril 2018 du juge des référés du tribunal administratif de Pau est annulée.<br/>
Article 2 : Il est enjoint au préfet des Hautes-Pyrénées, d'enregistrer selon la procédure normale la demande d'asile de M. A...et de lui délivrer l'attestation afférente dans un délai de quinze jours suivant la notification de la présente ordonnance.<br/>
Article 3 : Le surplus des conclusions des parties est rejeté. <br/>
Article 4 : L'Etat versera à M. A...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente ordonnance sera notifiée à M. B...A...et au ministre d'Etat, ministre de l'intérieur.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
