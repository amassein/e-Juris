<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044516357</ID>
<ANCIEN_ID>JG_L_2021_12_000000457114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/51/63/CETATEXT000044516357.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 17/12/2021, 457114, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Hot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:457114.20211217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une protestation enregistrée le 29 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat d'annuler les opérations électorales qui se sont déroulées le 27 juin 2021 en vue de la désignation des membres du conseil régional d'Ile-de-France.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel ; 	<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Hot, auditrice,<br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 décembre 2021, présentée par M. B... ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 361 du code électoral : " Les élections au conseil régional peuvent être contestées dans les dix jours suivant la proclamation des résultats par tout candidat ou tout électeur de la région devant le Conseil d'Etat statuant au contentieux ".<br/>
<br/>
              2. La protestation de M. B..., dirigée contre les opérations électorales qui ont eu lieu le 27 juin 2021 pour l'élection des membres du conseil régional d'Ile-de-France, n'a été enregistrée au secrétariat du contentieux du Conseil d'Etat que le 29 septembre 2021, soit après l'expiration du délai de dix jours fixé par les dispositions citées au point 1.<br/>
<br/>
              3. Toutefois, M. B... soutient que ce délai ne lui est pas opposable au motif qu'il méconnaitrait le droit à un recours juridictionnel effectif garanti par la Constitution et la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              Sur la constitutionnalité des dispositions du premier alinéa de l'article L. 361-1 du code électoral : <br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. Par un mémoire distinct présenté devant le Conseil d'Etat, M. B... soutient que le délai de dix jours fixé au premier alinéa de l'article L. 361 du code électoral méconnaît le droit à un recours juridictionnel effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, en ce qu'il enserre dans des délais trop brefs le droit au recours, sans que cela soit justifié par la nécessité de préserver la sécurité juridique des opérations électorales et la stabilité des institutions démocratiques, faute notamment que le législateur ait fixé aucun délai au Conseil d'Etat pour rendre sa décision sur de tels recours. <br/>
<br/>
              6. Toutefois, eu égard à la nature des contestations qui sont portées devant le juge de l'élection et à l'obligation qui s'impose à lui, même sans texte, de statuer dans un délai raisonnable, le délai de dix jours laissé aux protestataires pour former une réclamation contre ces opérations électorales ne saurait être regardé comme portant une atteinte excessive au droit à un recours juridictionnel effectif. <br/>
<br/>
              7. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur les autres moyens tendant à contester l'opposabilité des dispositions du premier alinéa de l'article L. 361 du code électoral : <br/>
<br/>
              8. En premier lieu, M. B... ne saurait utilement soutenir que les dispositions du premier alinéa de l'article L. 361 du code électoral sont contraires aux stipulations du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, lesquelles ne sont pas applicables aux protestations dirigées contre les résultats des élections. Il ne saurait davantage invoquer l'article 3 du premier protocole additionnel à cette convention énonçant le droit à des élections libres dans les conditions qui assurent la libre expression de l'opinion du peuple sur le choix du corps législatif, qui n'est pas applicable aux élections pour la désignation des membres des conseils régionaux, faute que ces élections puissent être regardées comme portant sur le choix du corps législatif eu égard aux compétences des régions en France, ni, par suite, celles de l'article 13 de cette convention, qui ne peuvent s'appliquer qu'à un droit qu'elle protège. <br/>
<br/>
              9. En second lieu, la circonstance qu'un délai supérieur à celui fixé par les dispositions du premier alinéa de l'article L. 361-1 du code électoral aurait été en l'espèce nécessaire pour réunir les preuves de l'atteinte à la sincérité du scrutin est en tout état de cause sans incidence sur la tardiveté de sa protestation. <br/>
<br/>
              10. Il résulte de tout ce qui précède que la protestation présentée par M. B... est tardive et ne peut, par suite, qu'être rejetée comme irrecevable.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de transmettre au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du premier alinéa de l'article L. 361-1 du code électoral. <br/>
<br/>
Article 2 : La protestation introduite par M. B... est rejetée.<br/>
Article 3 : La présente décision sera notifiée à M. A... B..., au Premier ministre et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : Mme C... F..., assesseure, présidant ; Mme Rozen Noguellou, conseillère d'Etat et Mme Pauline Hot, auditrice-rapporteure. <br/>
<br/>
<br/>
              Rendu le 17 décembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme C... F...<br/>
 		La rapporteure : <br/>
      Signé : Mme Pauline Hot<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... E...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
