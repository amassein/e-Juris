<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026107</ID>
<ANCIEN_ID>JG_L_2017_02_000000402690</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/61/CETATEXT000034026107.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/02/2017, 402690, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402690</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CARBONNIER</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:402690.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un mémoire en réplique, enregistrés les 22 novembre 2016 et 16 janvier 2017 au secrétariat du contentieux du Conseil d'État, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, les communes de Busseaut, Chambain, Essarois, La Chaume, Lucey, Montmoyen, Nod-sur-Seine, Rochefort-sur-Brévon et Terrefondrée (Côte-d'Or) demandent au Conseil d'État, à l'appui de leur requête tendant à l'annulation pour excès de pouvoir de l'arrêté du 7 mars 2016 portant prise en considération du projet de création du Parc national de forêt feuillue de plaine, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 331-1 du code de l'environnement.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la Constitution, notamment son Préambule et ses articles 61-1 et 72 ;<br/>
              -	la Charte de l'environnement, notamment son article 2 ;<br/>
              -	le code de l'environnement ;<br/>
              -	la loi n° 2006-436 du 14 avril 2006 ; <br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Carbonnier, avocat de la commune de Busseaut et autres ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'État (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que l'article L. 331-1 du code de l'environnement, issu de l'article 1er de la loi du 14 avril 2006 relative aux parcs nationaux, aux parcs naturels marins et aux parcs naturels régionaux, dispose que : " Un parc national peut être créé à partir d'espaces terrestres ou maritimes, lorsque le milieu naturel, particulièrement la faune, la flore, le sol, le sous-sol, l'atmosphère et les eaux, les paysages et, le cas échéant, le patrimoine culturel qu'ils comportent présentent un intérêt spécial et qu'il importe d'en assurer la protection en les préservant des dégradations et des atteintes susceptibles d'en altérer la diversité, la composition, l'aspect et l'évolution. / Il est composé d'un ou plusieurs coeurs, définis comme les espaces terrestres et maritimes à protéger, ainsi que d'une aire d'adhésion, définie comme tout ou partie du territoire des communes qui, ayant vocation à faire partie du parc national en raison notamment de leur continuité géographique ou de leur solidarité écologique avec le coeur, ont décidé d'adhérer à la charte du parc national et de concourir volontairement à cette protection. (...) " ; qu'en vertu des dispositions de l'article L. 331-2 du même code " un parc national est créé par un décret qui a notamment pour objet de délimiter le périmètre du ou des coeurs de ce parc, de fixer les règles générales de protection qui s'y appliquent d'approuver la charte du parc et de créer l'établissement public national du parc. " ; que l'article L. 331-6 de ce code prévoit que, à compter de la décision de l'autorité administrative envisageant la création d'un parc national, les travaux, constructions et installations projetés dans les espaces ayant vocation à figurer dans le coeur du parc national qui auraient pour effet de modifier l'état des lieux ou l'aspect des espaces en cause sont soumis à autorisation de l'autorité administrative, ou, s'ils sont soumis à une autorisation d'urbanisme, à son avis conforme ; qu'enfin, l'article L. 331-10 prévoit notamment que le directeur de l'établissement public du parc national exerce, dans le coeur du parc, les compétences attribuées au maire pour la police de la circulation et du stationnement, des chemins ruraux, des cours d'eau, de la destruction des animaux nuisibles ainsi que des chiens et chats errants, les actes réglementaires du directeur devant, sauf en cas d'urgence, être transmis pour avis huit jours au moins avant leur date d'entrée en vigueur aux maires des communes intéressées ; <br/>
<br/>
              3. Considérant que les communes requérantes soutiennent que l'article L. 331-1 précité méconnaît le principe de libre administration des collectivités territoriales garanti par l'article 72 de la Constitution en ne permettant pas aux communes pressenties pour faire partie du ou des coeurs des parcs nationaux de refuser d'y prendre part ;<br/>
<br/>
              4. Considérant qu'il résulte du troisième alinéa de l'article 72 de la Constitution que si les collectivités territoriales " s'administrent librement par des conseils élus ", chacune d'elles le fait " dans les conditions prévues par la loi " ; qu'en outre, l'article 34 de la Constitution réserve au législateur la détermination des principes fondamentaux de la libre administration des collectivités territoriales, de leurs compétences et de leurs ressources ; que l'article 2 de la charte de l'environnement dispose que : " Toute personne a le devoir de prendre part à la préservation et à l'amélioration de l'environnement " ; <br/>
<br/>
              5. Considérant que le législateur a entendu, par l'article L. 331-1 du code de l'environnement, permettre que bénéficient d'une protection renforcée les espaces terrestres ou maritimes dont le milieu naturel, notamment la faune et la flore, présente un intérêt spécial, afin de les protéger des dégradations et des atteintes susceptibles d'en altérer l'aspect, la composition ou l'évolution ; qu'à ce titre, il a prévu la possibilité pour le pouvoir réglementaire, sous le contrôle du juge de l'excès de pouvoir, d'instituer par décret des parcs nationaux, dont la gestion est confiée à un établissement public propre, et de préciser les zones de ces parcs devant bénéficier d'une protection renforcée en raison de leur intérêt particulièrement remarquable ; que, eu égard à la nécessité d'assurer l'effectivité de cette protection, qui implique que la zone dans laquelle cette protection doit être maximale puisse être définie, après enquête publique et consultation des communes concernées, au regard de l'intérêt des milieux naturels à préserver, la circonstance que les communes concernées ne puissent s'y opposer ne porte pas au principe de libre administration des collectivités territoriales une atteinte manifestement disproportionnée ; qu'il en va de même s'agissant du transfert limité, au profit de l'autorité administrative ou de l'établissement public du parc national, de pouvoirs notamment de police qui, touchant aux travaux, constructions et installations, à la circulation, au stationnement, à la gestion des chemins ruraux et des cours d'eau, ainsi qu'aux divagations d'animaux, apparaissent nécessaires à une action cohérente et efficace de préservation des milieux naturels ; que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par les communes de Busseaut, Chambain, Essarois, La Chaume, Lucey, Montmoyen, Nod-sur-Seine, Rochefort-sur-Brévon et Terrefondrée (Côte-d'Or).<br/>
Article 2 : La présente décision sera notifiée à la commune de Busseaut, première requérante dénommée, et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, ainsi qu'au ministre de l'aménagement du territoire, de la ruralité et des collectivités territoriales. Les autres requérantes en seront informées par Maître Carbonnier qui les représente devant le Conseil d'Etat.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
