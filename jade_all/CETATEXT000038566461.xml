<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038566461</ID>
<ANCIEN_ID>JG_L_2019_06_000000431068</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/56/64/CETATEXT000038566461.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 03/06/2019, 431068, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431068</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:431068.20190603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Me C...E..., M. F...B...et M. A...D...ont demandé au juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, de prescrire à l'administration pénitentiaire de Roanne de mettre fin à l'obligation pour les avocats de se munir de la                                    radio portative Motorola DP3441e et, à titre subsidiaire, de suspendre la décision prise par cette administration  d'interdire à Me E... de s'entretenir librement et confidentiellement avec ses clients au parloir avocat du centre de détention de Roanne et ce, sans être contrainte de conserver la radio portative dans le box d'entretien, et d'enjoindre à l'administration pénitentiaire de rétablir pour MeE..., sans délai, l'accès au parloir des avocats pour visiter et communiquer librement, sans radio portative, avec l'ensemble de ses clients détenus,   et ce, sous astreinte de 500 euros par jour de retard à compter de l'ordonnance à intervenir. Par une ordonnance n° 1903572 du 14 mai 2019, le juge des référés du tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 26 mai 2019 au secrétariat du contentieux du Conseil d'Etat, Me E...et MM. B...et D...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leur demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de deux sommes de 4 500 euros et de 504,09 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.   <br/>
<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la radio portative Motorola DP3441e est composée de fonctionnalités d'écoutes susceptibles de porter atteinte à la confidentialité des échanges entre l'avocat et ses clients, au droit des personnes détenues de communiquer librement avec leur avocat ainsi qu'à l'équité de la procédure pénale ;<br/>
              - la condition d'urgence est remplie dès lors que Me E... n'est plus en mesure de s'entretenir de manière confidentielle avec ses clients détenus au centre de détention de Roanne et, partant, d'assister ses clients à brèves échéances dans leur projet de sortie ou dans la préparation de leur dossier d'aménagement de peine ;<br/>
              - il est porté une atteinte grave et manifestement illégale, d'une part, à la liberté de Me E...d'exercer sa profession dès lors qu'elle ne peut plus accéder au parloir des avocats afin de s'entretenir directement avec ses clients et, d'autre part, aux droits de la défense dès lors que l'impossibilité pour Me E...de se rendre au parloir méconnaît le droit d'accès à un avocat tel qu'il résulte du système de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le chef d'établissement du centre de détention de Roanne était incompétent pour refuser l'accès au parloir de MeE... ;<br/>
              - l'interdiction d'accéder aux box d'entretien sans la radio portative est susceptible de méconnaître le principe d'égalité dès lors que ce dispositif n'est pas imposé, d'une part, au Procureur de la République de Roanne, d'autre part, aux membres de la famille d'une personne détenue, enfin, dans d'autres centres pénitentiaires de la région lyonnaise ;<br/>
              - la décision par laquelle la direction du centre de détention de Roanne a interdit à Me E...de s'entretenir avec ses clients n'est ni justifiée, ni proportionnée.<br/>
<br/>
              Par un mémoire en défense, enregistré le 28 mai 2019, la garde des sceaux, ministre de la justice, conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens de la requête ne sont pas fondés.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 28 mai 2019, la Section française de l'Observatoire international des prisons soutient que son intervention est recevable et conclut à ce qu'il soit fait droit aux conclusions d'appel présentées par MeE..., MM B... etD....<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 2009-1436 du 24 novembre 2009 pénitentiaire ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, MeE..., MM. B...et D...et la Section française de l'Observatoire international des prisons et, d'autre part, la garde des sceaux, ministre de la justice ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du mercredi 29 mai 2019 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Rousseau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Me E..., MM B...et D...;<br/>
<br/>
              - les représentants de la garde des sceaux, ministre de la justice ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. MeE..., MM B...et D...ont saisi le juge des référés du tribunal administratif de Lyon, sur le fondement de l'article L. 521-2 du code de justice administrative, lui demandant, à titre principal, de prescrire à l'administration du centre pénitentiaire de  Roanne d'annuler l'obligation faite à l'ensemble des avocats, lors de leurs entretiens avec leurs clients, de se munir de la radio portative Motorola DP3441e, qui ne garantit pas la confidentialité des échanges. Les requérants ont demandé également  au juge des référés, à titre subsidiaire, de suspendre la décision prise par l'administration pénitentiaire d'interdire à  Me E...de s'entretenir librement et confidentiellement avec ses clients, dès lors qu'elle refuse de conserver la radio portative dans le box d'entretien, et d'ordonner à l'administration   de rétablir pour MeE..., sans délai, l'accès au parloir des avocats pour visiter et communiquer librement avec l'ensemble de ses clients détenus. Par une ordonnance n° 1903572 du 14 mai 2019, le juge des référés du tribunal administratif de Lyon a rejeté cette demande. Me E..., MM B...et D...relèvent appel de cette ordonnance. <br/>
<br/>
              Sur l'intervention de la Section française de l'Observatoire international des prisons : <br/>
<br/>
              2. La Section française de l'Observatoire international des prisons justifie, eu égard à son objet statutaire et à la nature du litige, d'un intérêt suffisant pour intervenir dans la présente instance au soutien des conclusions de Me E...et de MM B...etD.... Son intervention est, par suite, recevable.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              3. Aux termes de l'article 22 de la loi du 24 novembre 2009 pénitentiaire : " L'administration pénitentiaire garantit à toute personne détenue le respect de sa dignité et de ses droits. L'exercice de ceux-ci ne peut faire l'objet d'autres restrictions que celles résultant des contraintes inhérentes à la détention, du maintien de la sécurité et du bon ordre des établissements, de la prévention de la récidive et de la protection de l'intérêt des victimes. Ces restrictions tiennent compte de l'âge, de l'état de santé, du handicap et de la personnalité de la personne détenue ". L'article 25 de la même loi précise que : " Les personnes détenues communiquent librement avec leurs avocats ".<br/>
<br/>
              4. L'article R. 57-6-5 du code de procédure pénale dispose que : " Le permis de communiquer est délivré aux avocats, pour les condamnés, par le juge de l'application des peines ou son greffier pour l'application des articles 712-6, 712-7 et 712-8 et, pour les prévenus, par le magistrat saisi du dossier de la procédure. / Dans les autres cas, il est délivré par le chef de l'établissement pénitentiaire ". Aux termes, par ailleurs,  de l'article R. 57-6-6 du même code, relatif aux relations des personnes détenues avec leur défenseur : " La communication se fait verbalement ou par écrit. Aucune sanction ni mesure ne peut supprimer ou restreindre la libre communication de la personne détenue avec son conseil ".<br/>
<br/>
              5. Il résulte de ces dispositions que les détenus disposent du droit de communiquer librement avec leurs avocats. Ce droit implique notamment qu'ils puissent, selon une fréquence qui, eu égard au rôle dévolu à l'avocat auprès des intéressés, ne peut être limitée a priori, recevoir leurs visites, dans des conditions garantissant la confidentialité de leurs échanges. Toutefois, ce droit s'exerce dans les limites inhérentes à la détention. Si les dispositions de l'article R. 57-6-5 du code de procédure pénale prévoient que les avocats doivent obtenir un permis de communiquer pour pouvoir rencontrer leurs clients lorsque ceux-ci sont détenus, afin de préserver le bon ordre et la sécurité des établissements pénitentiaires, elles n'ont ni pour objet, ni pour effet de subordonner l'obtention de ce permis à l'exercice par l'autorité chargée de délivrer le permis, d'un contrôle portant sur l'opportunité ou la nécessité de telles rencontres.<br/>
<br/>
              Sur les pouvoirs que le juge des référés tient de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              6. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
              7. Il résulte de la combinaison des dispositions des articles L. 511-1, L. 521-2 et L. 521-4 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 précité et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, de prendre les mesures qui sont de nature à faire disparaître les effets de cette atteinte. Ces mesures doivent en principe présenter un caractère provisoire, sauf lorsqu'aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Le juge des référés peut, sur le fondement de l'article L. 521-2 du code de justice administrative, ordonner à l'autorité compétente de prendre, à titre provisoire, une mesure d'organisation des services placés sous son autorité lorsqu'une telle mesure est nécessaire à la sauvegarde d'une liberté fondamentale. Toutefois, le juge des référés ne peut, au titre de la procédure particulière prévue par l'article L. 521-2 précité, qu'ordonner les mesures d'urgence qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est porté une atteinte grave et manifestement illégale. Eu égard à son office, il peut également, le cas échéant, décider de déterminer dans une décision ultérieure prise à brève échéance les mesures complémentaires qui s'imposent et qui peuvent également être très rapidement mises en oeuvre. Dans tous les cas, l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 précité est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires.<br/>
<br/>
              Sur l'appel de Me E...et de MM. B...et D...:<br/>
<br/>
              8. Me E...et ses clients soutiennent que la décision par laquelle le directeur de l'établissement pénitentiaire de Roanne a refusé à Me E...l'accès au parloir afin de communiquer avec ses clients détenus, au motif qu'elle refusait le port d'une alarme portative individuelle (API), est de nature à caractériser une atteinte grave et manifestement illégale à la confidentialité de la relation entre les avocats et les détenus et, par suite, aux droits de la défense.<br/>
<br/>
              9. Il résulte de l'instruction que le port d'une alarme portative individuelle au sein du centre de détention de Roanne a été rendue obligatoire pour des raisons de sécurité pour  l'ensemble des intervenants extérieurs, sans en exempter les avocats. Les pièces du dossier, en particulier les attestations des fournisseurs   produites  par la garde des sceaux devant le Conseil d'Etat, ainsi que les échanges lors de l'audience ont établi que ces alarmes portatives individuelles, de type Motorola DP3441e, introduites en décembre 2018, peuvent être équipées d'une fonction d'écoute à distance. Il ressort cependant de ces mêmes pièces que cette fonction exige un équipement et une programmation particulière des appareils, dont ceux du centre pénitentiaire de Roanne sont à ce jour dépourvus, conformément aux stipulations du bail conclu entre l'Etat et la société assurant l'exploitation et la maintenance du centre de Roanne.<br/>
<br/>
              10. Il n'appartient pas au juge du référé, statuant sur le fondement de l'article<br/>
L. 521-2, faute d'urgence, de se prononcer, sur la situation qui résulterait d'une hypothétique  décision de l'administration d'équiper dans le futur ces appareils pour y introduire une fonction d'écoute. Dès lors que l'équipement mis en cause par Me E...et ses clients ne permet pas, à ce jour, à l'administration pénitentiaire d'écouter à leur insu les échanges entre les avocats et les détenus, l'obligation d'utiliser ce dispositif, à la supposer même constituer une contrainte disproportionnée aux besoins compte tenu des dispositifs de sécurité déjà existants, n'est pas de nature à porter une atteinte grave et manifestement illégale à la confidentialité des échanges entre avocats et détenus et au droit des personnes détenues de communiquer avec leurs avocats.<br/>
<br/>
              11. Il résulte de l'ensemble de ce qui précède que c'est à bon droit que le juge des référés du tribunal administratif de Lyon a rejeté la demande de Me E...et de ses clients. Sans qu'il soit besoin de statuer sur l'existence d'une situation d'urgence, il y a lieu de rejeter la requête d'appel de Me E...et de MM B...etD..., y compris les conclusions présentées au titre de l'article L. 761-1   du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Section française de l'Observatoire international des prisons est admise.<br/>
Article 2 : La requête de Me E...et MM. B...et D...est rejetée. <br/>
Article 3 : La présente ordonnance sera notifiée à Me C...E..., M. F... B..., M. A...D...et à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée à la Section française de l'Observatoire international des prisons.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
