<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569458</ID>
<ANCIEN_ID>JG_L_2020_02_000000434931</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569458.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 12/02/2020, 434931, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434931</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434931.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 15 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail - Force ouvrière demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir du décret n° 2019-797 du 26 juillet 2019 relatif au régime d'assurance chômage, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du troisième alinéa de l'article L. 5422-3 du code du travail. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code du travail, notamment son article L. 5422-3 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de Confédération générale du travail - Force ouvrière ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. L'article L. 5421-1 du code du travail prévoit que les personnes aptes au travail et recherchant un emploi ont droit, sous certaines conditions, à un revenu de remplacement. En vertu des articles L. 5421-2, L. 5422-1 et L. 5422-2 du même code, ce revenu prend la forme, pour les travailleurs remplissant certaines conditions tenant à leur âge, à leur activité antérieure et au motif de leur privation d'emploi, et pour une durée limitée, d'une allocation d'assurance. L'article L. 5422-3 du même code prévoit que l'allocation d'assurance est calculée soit en fonction de la rémunération antérieurement perçue dans la limite d'un plafond, soit en fonction de la rémunération ayant servi au calcul des contributions de l'employeur au régime, qu'elle ne peut excéder le montant net de la rémunération antérieurement perçue et, en son troisième alinéa, qu'elle " peut comporter un taux dégressif en fonction de l'âge des intéressés et de la durée de l'indemnisation ".<br/>
<br/>
              3. La Confédération générale du travail - Force ouvrière soutient que les dispositions du troisième alinéa de l'article L. 5422-3 du code du travail méconnaissent le principe d'égalité devant la loi, garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789, en ce qu'elles permettent de traiter différemment les bénéficiaires de l'allocation d'assurance selon leur âge.<br/>
<br/>
              4. Aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen : " La loi est l'expression de la volonté générale. Tous les citoyens ont droit de concourir personnellement, ou par leurs représentants, à sa formation. Elle doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse. (...) ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit.<br/>
<br/>
              5. Par les dispositions contestées du troisième alinéa de l'article L. 5422-3 du code du travail, le législateur a entendu permettre que le montant de l'allocation d'assurance chômage puisse revêtir un caractère dégressif et que l'application de cette dégressivité, dans son principe ou son ampleur, puisse dépendre de l'âge du bénéficiaire. En prévoyant la possibilité d'une telle distinction entre les allocataires, le législateur a pris en compte les difficultés spécifiques que certains d'entre eux sont susceptibles de rencontrer, du fait de leur âge, pour retrouver un emploi. S'il a ainsi permis que soient traitées différemment des personnes placées dans des situations différentes, la différence de traitement qui en résulte est en rapport direct avec l'objet de la loi, qui est d'assurer l'indemnisation des travailleurs involontairement privés d'emploi tout en encourageant la reprise d'une activité professionnelle. Par suite, les dispositions contestées ne méconnaissent pas le principe d'égalité devant la loi.<br/>
<br/>
              6. Il résulte de ce qui précède que la question de la conformité des dispositions du troisième alinéa de l'article L. 5422-3 du code du travail aux droits et libertés que la Constitution garantit, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que le troisième alinéa de l'article L. 5422-3 du code du travail porterait atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Confédération générale du travail - Force ouvrière.<br/>
Article 2 : La présente décision sera notifiée à la Confédération générale du travail - Force ouvrière et à la ministre du travail.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
