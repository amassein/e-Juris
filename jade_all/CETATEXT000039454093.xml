<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039454093</ID>
<ANCIEN_ID>J0_L_2019_12_000001801543</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/45/40/CETATEXT000039454093.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de VERSAILLES, 1ère chambre, 03/12/2019, 18VE01543, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-03</DATE_DEC>
<JURIDICTION>CAA de VERSAILLES</JURIDICTION>
<NUMERO>18VE01543</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. BEAUJARD</PRESIDENT>
<AVOCATS>BOUDRIOT</AVOCATS>
<RAPPORTEUR>Mme Fabienne  MERY</RAPPORTEUR>
<COMMISSAIRE_GVT>M. CHAYVIALLE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure : <br/>
<br/>
       La société à responsabilité limitée (SARL) MARTI CANNES a demandé au Tribunal administratif de Lille la décharge des pénalités pour manquement délibéré qui lui ont été appliquées au titre des rappels de taxe sur la valeur ajoutée afférents aux périodes du 1er mai 2010 au 30 avril 2011 et du 1er mai 2012 au 30 avril 2013.<br/>
<br/>
       Par une ordonnance du 6 janvier 2017, le Président de la section du contentieux du Conseil d'État a transmis sa requête au Tribunal administratif de Montreuil. <br/>
<br/>
       Par un jugement no 1632899 du 15 mars 2018, le Tribunal administratif de Montreuil a rejeté sa demande. <br/>
<br/>
       Procédure devant la Cour : <br/>
<br/>
       Par une requête, enregistrée le 7 mai 2018, la SARL MARTI CANNES, représentée par Me Boudriot, avocat, demande à la Cour :<br/>
<br/>
       1° d'annuler le jugement attaqué ;<br/>
<br/>
       2° de prononcer la décharge des pénalités en litige ; <br/>
<br/>
       3° de mettre à la charge de l'État la somme de 1 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
       La SARL MARTI CANNES soutient que : <br/>
       - les rectifications en litige découlent d'erreurs matérielles ou de négligences, commises de bonne foi ; sa bonne foi est démontrée par la circonstance qu'elle a procédé d'elle-même à la correction des erreurs constatées en janvier 2012 pour l'exercice clos au 30 avril 2011 et en août 2013 pour l'exercice clos au 30 avril 2013 ; sa bonne foi est démontrée par le caractère spontané et immédiat de ses corrections, les erreurs commises en sa défaveur et la proportion limitée des erreurs ou insuffisances constatées par le service dans ses déclarations ;<br/>
       - le service a procédé à une rectification exagérée en matière de cotisation sur la valeur ajoutée des entreprises au titre de l'exercice clos en 2011 ;<br/>
       - le service a calculé de manière erronée l'assiette de la pénalité due au titre de l'exercice clos au 30 avril 2013, dès lors qu'il n'a pas tenu compte de la taxe sur la valeur ajoutée qu'elle a déclarée à tort au cours de l'exercice précédent et dont le montant devait venir en déduction du montant de la taxe sur la valeur ajoutée non déclarée au titre de l'exercice clos en 2013.<br/>
<br/>
       ....................................................................................................<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de Mme A... ;<br/>
       - et les conclusions de M. Chayvialle, rapporteur public.<br/>
<br/>
<br/>
       Considérant ce qui suit : <br/>
<br/>
       1. La SARL MARTI CANNES a pour activité l'exploitation, notamment par bail ou location, de biens immobiliers. Elle a fait l'objet d'une vérification de comptabilité pour la période du 1er mai 2010 au 30 avril 2013, à l'issue de laquelle des rappels de taxe sur la valeur ajoutée lui ont été notifiés, par deux propositions de rectification, du 19 décembre 2014 et du 22 mai 2015. La société a accepté les rectifications proposées, en lien, d'une part, avec des montants de taxe sur la valeur ajoutée collectés et non déclarés, et d'autre part, avec des montants de taxe sur la valeur ajoutée déduits de manière anticipée. Par sa réclamation du 10 décembre 2015, la SARL MARTI CANNES a contesté les pénalités pour manquement délibéré dont les rappels de taxe sur la valeur ajoutée ont été assortis. Elle relève appel du jugement du Tribunal administratif de Montreuil par lequel sa demande de décharge des pénalités en cause a été rejetée. <br/>
<br/>
       Sur le bien-fondé des pénalités en litige :<br/>
<br/>
       2. D'une part, aux termes de l'article 1729 du code général des impôts : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'Etat entraînent l'application d'une majoration de : /a. 40 % en cas de manquement délibéré ; (...) ". D'autre part, l'article L. 195 A du livre des procédures fiscales dispose que : " En cas de contestation des pénalités fiscales appliquées à un contribuable au titre des impôts directs, de la taxe sur la valeur ajoutée et des autres taxes sur le chiffre d'affaires, des droits d'enregistrement, de la taxe de publicité foncière et du droit de timbre, la preuve de la mauvaise foi et des manoeuvres frauduleuses incombe à l'administration. ".<br/>
<br/>
       3. En premier lieu, il est constant que la requérante a, d'une part, procédé à des déclarations de taxe sur la valeur ajoutée minorant les montants de la taxe collectée, aux cours des périodes du 1er mai 2010 au 30 avril 2011, puis du 1er mai 2012 au 30 avril 2013, à hauteur respectivement de 10 779 euros et de 10 528 euros, et, d'autre part, établi des déclarations, au cours de la période du 1er mai 2012 au 30 avril 2013, anticipant la déductibilité de certains montants de taxe sur la valeur ajoutée, à hauteur de la somme globale de 11 168 euros. Ces insuffisances ou inexactitudes ont été constatées par l'administration à la clôture de chacun des exercices en cause. Si la société fait valoir qu'elle a procédé à des régularisations, en janvier 2012 pour l'exercice clos au 30 avril 2011, et en août 2013 pour l'exercice clos au 30 avril 2013, celles-ci sont intervenues postérieurement au dépôt des déclarations de résultat propres à chaque exercice. En outre, la SARL MARTI CANNES ne saurait sérieusement invoquer des erreurs involontaires ou des négligences, dès lors qu'elle a fait figurer au passif de son bilan les montants de taxe sur la valeur ajoutée à régulariser pour les exercices clos en 2011 et 2013, et ce, alors même que, lors d'une précédente vérification de comptabilité effectuée en 2010, des rappels de taxe sur la valeur ajoutée lui avaient été notifiés pour les mêmes motifs que ceux invoqués lors de la vérification en cause, et qu'elle ne pouvait ainsi ignorer les règles applicables. Dans ces conditions, nonobstant l'appréciation du montant de taxe éludé au titre de chaque période, le caractère répété des insuffisances ou inexactitudes constatées dans les déclarations de taxe sur la valeur ajoutée de la SARL MARTI CANNES, qui ont eu pour effet de créer des facilités de trésorerie, traduit une intention délibérée d'éluder le paiement de la taxe, démontrée par l'administration. L'existence, au titre de la période du 1er mai 2011 au 30 avril 2012, d'un montant de taxe déclaré supérieur au montant de taxe exigible n'est pas de nature, à elle seule, à remettre en cause les éléments précédemment évoqués mis en évidence par le service. En conséquence, la SARL MARTI CANNES n'est pas fondée à soutenir que l'administration fiscale n'apporte pas la preuve d'un manquement délibéré au cours des périodes en litige. <br/>
<br/>
       4. En second lieu, si la SARL MARTI CANNES soutient qu'elle a apporté la preuve du caractère exagéré de la rectification effectuée, au titre de l'exercice clos en 2011, en matière de cotisation sur la valeur ajoutée des entreprises, accepté par l'administration dans sa réponse aux observations, cette circonstance est sans incidence sur le bien-fondé de la pénalité contestée. <br/>
<br/>
       Sur l'assiette de la pénalité pour manquement délibéré :<br/>
<br/>
       5. La société ne saurait utilement soutenir qu'elle a déclaré à tort un excédent de taxe sur la valeur ajoutée collectée au titre de la période du 30 avril 2011 au 1er mai 2012 pour solliciter que soit réévalué, dans le cadre du calcul de la pénalité, le montant de l'insuffisance de ses déclarations produites au cours de la période suivante, cet excédent étant sans incidence sur les inexactitudes établies au titre de la période du 1er mai 2012 au 30 avril 2013. Il ne saurait, en outre, être assimilé à un crédit de taxe sur la valeur ajoutée reportable en vertu du II de l'article 208 de l'annexe II du code général des impôts. Par suite, c'est à bon droit que l'administration a calculé la pénalité due en application de l'article 1729 du code général des impôts au titre de cette période à partir du montant des insuffisances constatées à la clôture de l'exercice. En conséquence, la SARL MARTI CANNES n'est pas fondée à se plaindre de ce que le tribunal administratif a rejeté sa demande de réduction de l'assiette de la pénalité litigieuse. <br/>
       6. Il résulte de tout ce qui précède que la SARL MARTI CANNES n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le Tribunal administratif de Montreuil a rejeté sa demande. Par voie de conséquence, ses conclusions, présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être également rejetées. <br/>
<br/>
<br/>
       DÉCIDE :<br/>
<br/>
<br/>
Article 1er : La requête de la SARL MARTI CANNES est rejetée.<br/>
2<br/>
N° 18VE01543<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-01-04-03 Contributions et taxes. Généralités. Amendes, pénalités, majorations. Pénalités pour manquement délibéré (ou mauvaise foi).
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
