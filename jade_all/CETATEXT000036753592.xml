<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036753592</ID>
<ANCIEN_ID>JG_L_2018_03_000000399867</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/75/35/CETATEXT000036753592.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 28/03/2018, 399867</TITRE>
<DATE_DEC>2018-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399867</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2018:399867.20180328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>	Vu la procédure suivante :<br/>
<br/>
              	Mme A...B...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision, non datée, par laquelle le maire de Paris a refusé de reconnaître l'imputabilité au service de son malaise survenu le 8 octobre 2008. Par un jugement n° 1317530 du 15 mai 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              	Par un arrêt n° 14PA03146 du 17 mars 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par Mme B...contre ce jugement.  <br/>
<br/>
              	Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 mai et 17 août 2016 et le 24 février 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              	1°) d'annuler cet arrêt ;<br/>
<br/>
              	2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              	3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B...et à la SCP Foussard, Froger, avocat de la Ville de Paris ;<br/>
<br/>
              	Vu la note en délibéré, enregistrée le 14 mars 2018, présentée par Mme B... ;		<br/>
<br/>
<br/>
<br/>
<br/>
<br/>	1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., adjointe administrative de la Ville de Paris, a été victime le 8 octobre 2008 d'une chute dont l'imputabilité au service n'a pas été reconnue par la Ville de Paris ; que, par un jugement du 15 mai 2014, le tribunal administratif de Paris a rejeté comme tardive sa demande tendant à l'annulation de la décision, non datée, par laquelle le maire de Paris a refusé de reconnaître l'imputabilité au service de cet accident ; que Mme B...se pourvoit en cassation contre l'arrêt du 17 mars 2016 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'elle avait formé contre ce jugement ; <br/>
<br/>
              	2. Considérant, en premier lieu, qu'en estimant que le courrier notifié à Mme B... le 25 septembre 2013 constituait une décision du maire de Paris refusant de reconnaître l'imputabilité au service de l'accident dont elle avait été victime le 8 octobre 2008, la cour administrative d'appel n'a pas inexactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              	3. Considérant, en second lieu, que lorsque le destinataire d'une décision administrative soutient que l'avis de réception d'un pli recommandé portant notification de cette décision à l'adresse qu'il avait lui-même indiqué à l'administration n'a pas été signé par lui, il lui appartient d'établir que le signataire de l'avis n'avait pas qualité pour recevoir le pli en cause ; <br/>
<br/>
              	4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond et n'est d'ailleurs pas contesté que le courrier envoyé par le maire de Paris à Mme B...a fait l'objet d'un accusé de réception, signé le 25 septembre 2013 par le gardien de la résidence où habite MmeB... ; qu'en estimant que Mme B...n'établissait pas que le gardien de cette résidence n'avait pas qualité pour recevoir les plis recommandés qui lui étaient destinés, la cour administrative d'appel s'est livrée à une appréciation souveraine des pièces du dossier exempte de dénaturation ; qu'elle n'a pas commis d'erreur de droit en jugeant que, par suite, la décision du maire de Paris avait été régulièrement notifiée, à cette date, à MmeB... ; <br/>
<br/>
              	5. Considérant qu'il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que son pourvoi doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme que demande la Ville de Paris au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				 --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
<br/>
Article 2 : Les conclusions de la Ville de Paris présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et à la Ville de Paris.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
