<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037124736</ID>
<ANCIEN_ID>JG_L_2018_06_000000410085</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/12/47/CETATEXT000037124736.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 28/06/2018, 410085, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410085</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410085.20180628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 24 avril 2017 au secrétariat du contentieux du Conseil d'État, M. B...A...demande au Conseil d'État :<br/>
<br/>
              À titre principal :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les épreuves de l'examen du certificat d'aptitude à la profession d'avocat au Conseil d'État et à la Cour de cassation (CAPAC) organisées au titre de l'année 2016 en application de l'article 17 du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d'État et à la Cour de cassation, ainsi que l'ensemble des examens de passage en année supérieur ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir les articles 1er et 16 du décret n° 91-1125 du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d'État et à la Cour de cassation ;<br/>
<br/>
              3°) d'annuler pour excès de pouvoir le règlement intérieur de l'Institut de formation et de recherche des avocats aux conseils (IFRAC) ;<br/>
<br/>
              4°) d'annuler pour excès de pouvoir les décisions des 6 novembre 2014 et 17 novembre 2015 par lesquelles le président de l'ordre des avocats au Conseil d'État et à la Cour de cassation a refusé ses demandes d'inscription à l'Institut de formation et de recherche des avocats aux conseils ;<br/>
<br/>
              5°) de le déclarer titulaire du certificat d'aptitude à la profession d'avocat au Conseil d'État et à la Cour de cassation (CAPAC) à compter du 1er janvier 2015 et de donner acte de sa nomination aux fonctions d'avocat au Conseil d'État et à la Cour de cassation, née d'une décision implicite du garde des sceaux, ministre de la justice ;<br/>
<br/>
              6°) de " statuer sur la mise en oeuvre de l'article 7 de la Déclaration des droits de l'homme et du citoyen " ;<br/>
<br/>
              7°) de renvoyer l'ensemble des auteurs présumés des décisions visées dans ses conclusions devant les juridictions répressives ;<br/>
<br/>
              8°) de condamner le conseil de l'ordre des avocats au Conseil d'État et à la Cour de cassation à lui verser la somme de 1 200 000 euros en réparation du préjudice qu'il estime avoir subi du fait des refus de son inscription à l'ordre des avocats au Conseil d'État et à la Cour de cassation ;<br/>
<br/>
              9°) de mettre à la charge de l'ordre des avocats au Conseil d'État et à la Cour de cassation les dépens au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              A titre subsidiaire :<br/>
<br/>
              10°) de déclarer que la loi du 28 avril 1816 sur les finances ainsi que l'ordonnance du 10 septembre 1817 relative aux avocats au Conseil d'État et à la Cour de cassation ne sont pas conformes à la Constitution ;<br/>
<br/>
              11°) de renvoyer une question préjudicielle à la Cour de justice de l'Union européenne concernant la conformité de ces textes au traité sur l'Union européenne ;<br/>
<br/>
              12°) d'annuler pour excès de pouvoir les arrêtés des 28 janvier 2016, 3 mai 2016, 5 juillet 2016, 28 avril 2016 et 6 juin 2016 portant nomination d'avocats au Conseil d'État et à la Cour de cassation.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi du 28 avril 1816 sur les finances ;<br/>
              - l'ordonnance du 10 septembre 1817 ;<br/>
              - le décret n°91-1125 du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d'État et à la Cour de cassation ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. B...A...s'est inscrit en 2012 en première année à l'Institut de formation et de recherche des avocats au Conseil d'État et à la Cour de cassation (IFRAC). Par une décision du 4 octobre 2013, il n'a pas été admis en deuxième année de formation. Par une délibération du 14 novembre 2013, M. A...a été radié du registre prévu à l'article 7 du décret n° 91-1125 du 28 octobre 1995. Par une lettre du 17 novembre 2015, la présidente de l'ordre des avocats au Conseil d'État et à la Cour de cassation a informé M. A...qu'il ne pouvait être fait droit à sa demande d'inscription en troisième année de formation à l'IFRAC. M. A...a alors adressé à la garde des sceaux, ministre de la justice un dossier de " candidature aux fonctions d'avocat au Conseil d'État et à la Cour de cassation " par un courrier du 16 février 2016. La garde des sceaux, ministre de la justice n'a pas répondu à cette demande. <br/>
<br/>
              2. Au terme de l'article R. 351-4 du code de justice administrative : " Lorsque tout ou partie des conclusions dont est saisi (...) le Conseil d'État relève de la compétence d'une juridiction administrative, (...) le Conseil d'État (...) est compétent, nonobstant les règles de répartition des compétences entre juridictions administratives, pour rejeter les conclusions entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance (...) ".<br/>
<br/>
              Sur les conclusions tendant à l'annulation des épreuves du CAPAC pour l'année 2016, de l'ensemble des examens de passage en année supérieure, ainsi que des arrêtés de nomination des avocats au Conseil d'État et à la Cour de cassation intervenus en 2016 :<br/>
              3. Aux termes de l'article 1er du décret du 28 octobre 1991 relatif aux conditions d'accès à la profession d'avocat au Conseil d'État et à la Cour de cassation, l'accès à la profession d'avocat au Conseil d'État et à la Cour de cassation est soumis à la condition d'avoir subi avec succès l'examen d'aptitude à la profession d'avocat au Conseil d'État et à la Cour de cassation auquel on ne peut se présenter qu'après avoir suivi la formation dispensée sous l'autorité du conseil de l'ordre des avocats au Conseil d'État et à la Cour de cassation .<br/>
<br/>
              4. M. A...a été radié du registre de la formation des avocats au Conseil d'État et à la Cour de cassation par une décision du 14 novembre 2013 du conseil de l'ordre des avocats au Conseil d'État et à la Cour de cassation alors qu'il était inscrit en tant que redoublant en première année de formation. Ainsi, il n'a jamais suivi la totalité de la formation. En outre, il ne justifie pas entrer dans le champ des exceptions à la formation prévues par ce décret. Dès lors, M. A...n'ayant pas terminé la scolarité au sein de l'IFRAC et ne remplissant ainsi pas les conditions pour présenter l'examen du CAPAC ni exercer les fonctions d'avocat au Conseil d'État et à la Cour de Cassation, il ne justifie pas d'un intérêt lui donnant qualité pour demander l'annulation des épreuves de l'examen du CAPAC organisée pour l'année 2016 ainsi que des arrêtés de nomination des avocats au Conseil d'État et à la Cour de cassation intervenus en 2016. Par suite, ces demandes doivent être rejetées.<br/>
<br/>
              Sur les conclusions tendant à l'annulation des articles 1er et 16 du décret du <br/>
28 octobre 1991 :<br/>
              5. Le requérant demande, d'une part, l'annulation des dispositions fixant les conditions pour présenter l'examen national du CAPAC et, d'autre part, l'annulation du titre dénommé " certificat de fin de formation " délivré par l'IFRAC. Ces demandes doivent être regardées comme tendant à l'annulation des articles 1er et 16 du décret du 28 octobre 1991 en tant que le premier fixe les conditions d'accès à la profession d'avocat au Conseil d'Etat et à la Cour de cassation et que le second précise qu'à l'issue de la formation, un certificat de fin de formation est délivré aux stagiaires qui ont satisfait à l'ensemble des obligations. L'article 1er du décret attaqué a été modifié par le décret publié le 20 mai 2016 et l'article 16 du décret attaqué a été modifié par le décret publié le 23 décembre 1999. Les conclusions tendant à cette annulation n'ont été enregistrées au secrétariat du contentieux du Conseil d'État que le 24 avril 2017. Dès lors, elles ont été présentées tardivement et se trouvent entachées d'une irrecevabilité manifeste non susceptible d'être couverte en cours d'instance. Elles doivent, par suite, être rejetées.<br/>
<br/>
              Sur les conclusions tendant à l'annulation du règlement intérieur de l'IFRAC :<br/>
<br/>
              6. M. A...demande l'annulation du règlement intérieur de l'IFRAC. Cette demande doit être regardée comme dirigée contre la délibération du 13 octobre 2011 de l'ordre des avocats au Conseil d'État et à la Cour de cassation par laquelle le règlement intérieur a été adopté. Ce règlement a été approuvé par un arrêté du 20 décembre 2011 du garde des sceaux, ministre de la justice et publié le 27 décembre 2011 au Journal officiel de la République française. En vertu de l'article R. 421-1 du code de justice administrative, le délai de recours contre cet acte a expiré le 27 février 2012. Toutefois, les conclusions de M. A...tendant à l'annulation de cet acte n'ont été enregistrées au secrétariat du contentieux du Conseil d'État que le 24 avril 2017, soit après l'expiration de ce délai. Dès lors, elles ont été présentées tardivement et se trouvent entachées d'une irrecevabilité manifeste non susceptible d'être couverte en cours d'instance et, doivent, par suite, être rejetées.<br/>
<br/>
              Sur les conclusions à fin d'annulation des décisions des 6 novembre 2014 et 17 novembre 2015 :<br/>
<br/>
              7. Par une décision du 14 novembre 2013, le conseil de l'ordre des avocats au Conseil d'État et à la Cour de cassation a exclu M. A...de l'Institut de formation et de recherche des avocats aux Conseils. Par la décision du 6 novembre 2014, le président de l'ordre des avocats au Conseil d'État et à la Cour de cassation a signifié à M. A...l'impossibilité de l'inscrire à l'IFRAC en raison de cette exclusion. Enfin, par la décision du 17 novembre 2015, la présidente de l'ordre a rejeté la demande de M. A...tendant à son inscription en troisième année de formation au sein de l'IFRAC au motif qu'il a été exclu de cet institut par la décision du 14 novembre 2013, qu'il n'a pas été admis en deuxième année de formation à l'IFRAC et qu'il ne présente pas les conditions pour pouvoir se prévaloir des dispositions des articles 4 et 10 du décret du 28 octobre 1991.<br/>
<br/>
              8. Les décisions du président de l'ordre des avocats au Conseil d'État et à la Cour de cassation des 6 novembre 2014 et 17 novembre 2015 doivent être regardées comme ayant un caractère purement confirmatif de sa décision du 14 novembre 2013 l'excluant de l'IFRAC, contre laquelle M. A... a déjà formé des recours pour excès de pouvoir. Par suite, les conclusions dirigées contre les décisions des 6 novembre 2014 et 17 novembre 2015 sont manifestement irrecevables et doivent, dès lors, être rejetées.<br/>
<br/>
Sur les conclusions visant, d'une part, à constater que le silence gardé pendant quatre mois par le garde des sceaux, ministre de la justice, vaut décision implicite d'acceptation de sa demande du 15 février 2016 et, d'autre part, à donner acte de sa nomination aux fonctions d'avocat au Conseil d'État et à la Cour de cassation :<br/>
<br/>
              9. Il n'appartient pas au Conseil d'État, statuant au contentieux d'effectuer de simples constatations mais de juger de la légalité d'un acte administratif. Ainsi, il ne peut être saisi de conclusions tendant  au seul constat de l'existence d'un acte et de ses conséquences. Les conclusions de M. A...tendant à constater que le silence gardé pendant quatre mois par le garde des sceaux, ministre de la justice vaudrait décision implicite d'acception ne sont, dès lors, manifestement pas recevables, ni, par voie de conséquence, celles tendant à ce qu'il soit donné acte de sa nomination.<br/>
<br/>
              10. En tout état de cause, l'annexe du décret du 23 octobre 2014 relatif aux exceptions à l'article L. 231-1 du code des relations entre le public et l'administration, pris sur le fondement de l'article L. 231-5 de ce code, fixe la liste des demandes dont le silence gardé pendant deux mois par l'administration vaut décision de rejet. Parmi ces demandes figure la nomination dans un office créé ou vacant d'avocat au conseil d'État et à la Cour de cassation. Par conséquent, si M. A...a adressé au garde des sceaux, ministre de la justice, une demande de nomination aux fonctions d'avocat au Conseil d'État et à la Cour de cassation le 15 février 2016, le silence gardé pendant deux mois sur cette demande a fait naître une décision implicite de rejet, et non une décision d'acceptation. <br/>
<br/>
              Sur la demande relative à la mise en oeuvre de  l'article 7 de la Déclaration des droits de l'homme et du citoyen et celle relative au renvoi devant les juridictions répressives :<br/>
<br/>
              11. Aux termes de l'article 7 de la Déclaration des droits de l'homme et du citoyen : " Nul homme ne peut être accusé, arrêté ni détenu que dans les cas déterminés par la Loi, et selon les formes qu'elle a prescrites. Ceux qui sollicitent, expédient, exécutent ou font exécuter des ordres arbitraires, doivent être punis ; (...). "<br/>
<br/>
              12. Ces conclusions ne sont pas de celles qu'il appartient au juge administratif de connaître. Ainsi, ces demandes ne relèvent manifestement pas de la compétence de la juridiction administrative et doivent, par suite, être rejetées.<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              13. M. A...soutient que les décisions par lesquels le président de l'ordre des avocats aux Conseils l'a radié des registres de l'IFRAC puis a refusé son inscription en troisième année lui ont causé un préjudice qu'il estime à 1 200 000 euros. Toutefois, M. A...n'a pas lié le contentieux en saisissant l'ordre des avocats aux conseils d'une demande indemnitaire préalable. Dès lors, en application de l'article R. 421-1 du code de justice administrative, ces conclusions ne sont manifestement pas recevables et doivent être rejetées<br/>
<br/>
              Sur les conclusions tendant à l'annulation de la loi du 28 avril 1816 :<br/>
<br/>
              14. Le requérant soutient que la loi du 28 avril 1816 sur les finances, dont il demande l'annulation, n'est pas conforme à la Constitution. Il n'appartient toutefois pas au juge administratif d'apprécier la conformité à la Constitution de la loi. Il suit de là que la juridiction administrative n'est pas compétente pour connaître des conclusions de M. A...tendant à l'annulation de la loi du 28 avril 1816 sur les finances. Ces conclusions doivent, par suite, être rejetées.<br/>
<br/>
              Sur les conclusions tendant à l'annulation de l'ordonnance du 10 septembre 1817 :<br/>
<br/>
              15. M. A...soutient que l'ordonnance du 10 septembre 1817, dont il demande l'annulation, n'est pas conforme à la Constitution. D'une part, il n'appartient pas au juge administratif d'apprécier la conformité à la Constitution des dispositions de nature législative de cette ordonnance. D'autre part, les dispositions règlementaires de cette ordonnance ont été modifiées la dernière fois par le décret du 14 décembre 2016. Les conclusions dirigées contre ces dispositions règlementaires,  enregistrées le 24 avril 2017, sont tardives et par suite irrecevables. Il résulte de ce qui précède que les conclusions de M. A...tendant à l'annulation de l'ordonnance précitée doivent être rejetées.<br/>
<br/>
              Sur la demande de renvoi préjudiciel :<br/>
<br/>
              16. Le requérant demande au Conseil d'État de mettre en oeuvre les dispositions des articles R. 771-2 et suivants du code de justice administrative aux fins de contrôler la conventionalité de la loi du 28 avril 1816 et de l'ordonnance du 10 septembre 1817 au regard du Traité sur l'Union européenne. Les articles dont il se prévaut sont relatifs aux questions préjudicielles entre la juridiction administrative et la juridiction judiciaire et non à celles dont la Cour de justice de l'Union européenne peut connaître. Au surplus, les écritures du requérant ne sont pas assorties des précisions permettant d'apprécier l'opportunité de saisir la Cour de justice de l'Union européenne d'une telle question préjudicielle. Sa demande doit, par suite, être rejetée.<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              17. Il résulte de ce qui précède que l'exécution de la présente décision n'implique pas que la garde des sceaux, ministre de la justice ne prenne une mesure d'exécution dans un sens déterminé ni ne prenne à nouveau une décision après une nouvelle instruction. Dès lors, il ne peut être fait application des dispositions des articles L. 911-1 ou L. 911-2 du code de justice administrative et ces conclusions doivent être rejetées. <br/>
<br/>
              18. Il résulte de tout ce qui précède que la requête de M. A...doit être rejetée.<br/>
<br/>
              19. Aux termes de l'article R. 741-12 du code de justice administrative : " Le juge peut infliger à l'auteur d'une requête qu'il estime abusive une amende dont le montant ne peut excéder 10 000 euros ". En l'espèce, la présente requête de M. A...présente un caractère abusif. Dès lors, il y a lieu de condamner M. A...à payer une amende de 3 000 euros. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : M. A...est condamné à payer une amende de 3 000 euros.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., à la garde des sceaux, ministre de la justice, à l'ordre des avocats au Conseil d'État et à la Cour de cassation et au directeur régional des finances publiques d'Ile-de-France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
