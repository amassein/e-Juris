<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027620170</ID>
<ANCIEN_ID>JG_L_2013_06_000000352463</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/62/01/CETATEXT000027620170.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 27/06/2013, 352463, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352463</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>Mme Eliane Chemla</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:352463.20130627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 septembre et 5 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Europe Immobilière, dont le siège est 49, rue Vaneau à Paris (75007) ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n°s 0905679-1008375 du 6 juillet 2011 par lequel le tribunal administratif de Paris a rejeté ses demandes tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties et de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre des années 2008 et 2009 à raison d'un immeuble à usage de bureaux dont elle est propriétaire aux 18, 20 et 22, rue de Berri à Paris ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Eliane Chemla, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, avocat de la société Europe Immobilière ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Europe Immobilière est propriétaire de locaux à usage de bureaux comprenant cinq niveaux d'un immeuble situé 18, 20 et 22 rue de Berri à Paris, à raison desquels elle a été assujettie à la taxe foncière sur les propriétés bâties et à la taxe d'enlèvement des ordures ménagères au titre des années 2008 et 2009 ; qu'ayant réalisé sur cet immeuble des travaux pour lesquels elle a obtenu un permis de démolir et un permis de construire délivrés le 27 novembre 2007, elle a présenté des réclamations tendant à la décharge de ces impositions en soutenant que ces travaux avaient consisté en une opération de démolition suivie d'une reconstruction faisant obstacle à toute imposition jusqu'à son achèvement ; qu'elle se pourvoit en cassation à l'encontre du jugement du 6 juillet 2011 par lequel le tribunal administratif de Paris, après avoir joint ses demandes tendant aux mêmes fins, les a rejetées ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1380 du code général des impôts : " La taxe foncière est établie annuellement sur les propriétés bâties sises en France, à l'exception de celles qui en sont expressément exonérées par les dispositions du présent code " ; qu'aux termes du I de l'article 1383 du même code : " Les constructions nouvelles, reconstructions et additions de constructions sont exonérées de la taxe foncière sur les propriétés bâties durant les deux années qui suivent celle de leur achèvement " ; qu'en vertu des dispositions de l'article 1415 du même code, la taxe foncière sur les propriétés bâties est due pour l'année entière sur les faits existants au 1er janvier de l'année d'imposition ;<br/>
<br/>
              3. Considérant que, statuant sur le terrain de la loi fiscale, le tribunal administratif a relevé, par une appréciation souveraine qui n'est pas arguée de dénaturation, que la restauration et la restructuration des locaux comprenaient notamment la modification d'une toiture et d'une façade, la rénovation partielle des planchers et des escaliers au premier étage, ainsi que le changement de destination de locaux à usage de bureaux en un local d'habitation avec remplacement des menuiseries intérieures et du système d'éclairage et que par suite, ces travaux n'avaient pas porté sur le gros oeuvre ni entraîné une augmentation très importante du volume ou de la surface de la construction ; qu'en déduisant de ces constatations que ces travaux ne pouvaient être regardés comme constituant une opération présentant le caractère d'une reconstruction au sens de l'article 1383 du code général des impôts précité, il n'a ni inexactement qualifié les faits ni commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, toutefois, que, statuant sur le terrain de la garantie prévue par  l'article L. 80 A du livre des procédures fiscales, le tribunal a retenu, pour écarter la documentation  administrative référencée 6 C-1321 du 15 décembre 1988, dont la société requérante revendiquait le bénéfice en application du second alinéa de cet article, que les impositions en litige ne procédaient pas d'un rehaussement d'imposition mais d'impositions primitives et qu'elle n'établissait pas en avoir fait elle-même application ; qu'en statuant ainsi, alors que, d'une part, lorsque le contribuable invoque, sur le fondement de ces dispositions, l'interprétation d'un texte fiscal que l'administration a fait connaître par des instructions ou circulaires publiées, aucune imposition, même primitive, qui serait contraire à cette interprétation, ne peut être établie et que, d'autre part, il ne pouvait être opposé à la société requérante  le fait de ne pas avoir appliqué la doctrine dont elle se prévalait au soutien de ses demandes de décharge des cotisations de taxe foncière sur les propriétés bâties, le tribunal a commis une erreur de droit ; que la société requérante est fondée à demander pour ce motif l'annulation du jugement attaqué ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Europe Immobilière en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
 Article 1er : Le jugement du 6 juillet 2011 du tribunal administratif de Paris est annulé.<br/>
<br/>
 Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
<br/>
Article 3 : L'Etat versera à la société Europe Immobilière une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Europe Immobilière et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
