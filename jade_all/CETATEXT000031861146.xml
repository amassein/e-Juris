<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861146</ID>
<ANCIEN_ID>JG_L_2015_12_000000374081</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861146.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 30/12/2015, 374081, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374081</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374081.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL Le Byzantin a demandé au tribunal administratif de Paris la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'année 2007. Par un jugement n° 1118625/7 du 5 octobre 2012, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12PA04631 du 7 novembre 2013, la cour administrative d'appel de Paris a rejeté l'appel formé par la SARL Le Byzantin contre ce jugement.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 19 décembre 2013 et 20 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la SARL Le Byzantin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la SARL Le Byzantin ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du 1 de l'article 41 bis du code général des impôts : " La plus-value constatée à l'occasion de la cession des éléments corporels et incorporels d'un débit de boissons auquel est attachée une licence de 3e ou de 4e catégorie n'est pas comprise dans le bénéfice imposable lorsque le cessionnaire prend l'engagement dans l'acte de cession, soit de transformer l'exploitation dans un délai maximal de six mois, à compter de la cession, en débit de 1re ou 2e catégorie, soit d'entreprendre, dans le même délai et dans les mêmes locaux, une profession ne comportant pas la vente de boissons, dans les conditions prévues aux 1° et 2° de l'article 1er du décret n° 55-570 du 20 mai 1955. / Lorsque la cession porte sur un établissement dans lequel sont exercées plusieurs activités, le bénéfice de l'exonération ainsi prévue est limité à la fraction de la plus-value se rapportant à la cession du débit de boissons. " ; qu'il résulte de ces dispositions  que seule l'activité de vente de boissons sur place nécessitant, en vertu des dispositions de l'article L. 3331-1  du code de la santé publique, l'usage d'une licence de 3e ou de 4e catégorie peut bénéficier de l'exonération de plus-value en cas de cession assortie d'un engagement de disparition de cette licence ; qu'une activité de restauration, y compris celle comportant, en vertu d'une telle licence, la vente de boissons sur place à titre accessoire des principaux repas ne peut en bénéficier ;<br/>
<br/>
              2. Considérant que, par l'arrêt attaqué, la cour administrative d'appel de Paris n'a admis le bénéfice de l'exonération de la plus-value constatée à l'occasion de la cession d'un fonds de commerce de bar et de brasserie détenu par la SARL Le Byzantin, assortie de l'engagement de disparition de la licence de débit de boissons de 4e catégorie attachée à l'exploitation de ce fonds, qu'à concurrence de la fraction, non contestée, de la plus-value se rapportant à l'activité de débit de boissons, à l'exclusion de la fraction correspondant à l'activité de restauration ; qu'en jugeant que l'exploitation d'un débit de boissons constitue une activité distincte de l'activité de restauration, alors même que ces activités sont exercées au sein du même établissement détenant une seule et même licence de débit de boissons de 4e catégorie, la cour n'a pas méconnu les dispositions précitées de l'article 41 bis du code général des impôts, ni donné aux faits qui lui étaient soumis une qualification juridique erronée ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que le pourvoi de la SARL Le Byzantin ne peut qu'être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la SARL Le Byzantin est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SARL Le Byzantin et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
