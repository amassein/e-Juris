<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088237</ID>
<ANCIEN_ID>JG_L_2019_02_000000417156</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/82/CETATEXT000038088237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 01/02/2019, 417156, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417156</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417156.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 28 octobre 2016 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a refusé de lui reconnaitre la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une décision n° 17000733 du 4 mai 2017, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 9 janvier et 9 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui reconnaitre la qualité de réfugié ou, à défaut, lui accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros à verser à la SCP Baraduc-Duhamel-Rameix, son avocat, au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet  1991.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. M. B...A..., de nationalité bangladaise, se pourvoit en cassation contre la décision du 4 mai 2007 par laquelle la Cour nationale du droit d'asile a rejeté son recours dirigé contre la décision du 28 octobre 2016 de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile.<br/>
<br/>
              2. S'il incombe à l'Office français de protection des réfugiés et apatrides de garantir la confidentialité des éléments d'information susceptibles de mettre en danger les personnes qui sollicitent l'asile ainsi que le respect de la vie privée ou du secret médical, aucune règle ni aucun principe ne font obstacle, de manière absolue, à ce qu'il se fonde, pour apprécier le bien-fondé d'une demande d'asile, sur des éléments issus du dossier d'un tiers.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que pour demander la reconnaissance de la qualité de réfugié ou, à défaut, le bénéfice de la protection subsidiaire, M. A... s'est prévalu des risques personnels encourus du fait de la situation qui a conduit à la reconnaissance du statut de réfugié à son père, d'une part, et à sa mère et à son frère, d'autre part, respectivement par une décision de la Commission de recours des réfugiés en 2007 et par des décisions de l'OFPRA en 2014. En se bornant à se référer à ces décisions pour juger que les faits allégués n'étaient pas établis ni les craintes avancées fondées sans demander communication des dossiers de demande d'asile des trois intéressés dont le contenu était cependant nécessaire à la résolution du litige qui lui était soumis, ainsi qu'il lui était au demeurant demandé, la Cour nationale du droit d'asile a entaché, dans les circonstances de l'espèce, sa décision d'erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. A...est fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              5. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Baraduc-Duhamel-Rameix, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à cette SCP de la somme de 2 000 euros.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 4 mai 2017 est annulée.  <br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à la SCP Baraduc-Duhamel-Rameix, avocat de M.A..., sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat, la somme de 2 000 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
