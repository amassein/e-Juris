<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025528927</ID>
<ANCIEN_ID>JG_L_2012_03_000000331373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/52/89/CETATEXT000025528927.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 12/03/2012, 331373</TITRE>
<DATE_DEC>2012-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>331373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean-Philippe Thiellay</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:331373.20120312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 31 août et 27 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant...,; M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0801570 du 30 juin 2009 par lequel le tribunal administratif de Caen a rejeté sa demande tendant, d'une part, à l'annulation de la décision du 30 avril 2008 par laquelle le ministre du budget, des comptes publics et de la fonction publique a refusé de réviser la pension civile de retraite qui lui a été accordée à compter du 1er janvier 2008 et, d'autre part, à ce qu'il soit procédé à cette révision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ainsi que le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de M. A...B..., <br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de M. A...B..., <br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L 15 du code des pensions civiles et militaires de retraite : " I. - Aux fins de liquidation de la pension, le montant de celle-ci est calculé en multipliant le pourcentage de liquidation tel qu'il résulte de l'application de l'article L. 13 par le traitement ou la solde soumis à retenue afférents à l'indice correspondant à l'emploi, grade, classe et échelon effectivement détenus depuis six mois au moins par le fonctionnaire ou le militaire au moment de la cessation des services valables pour la retraite (...) / II. - Aux fins de liquidation de la pension, le montant de celle-ci est calculé à partir des derniers traitements ou soldes soumis à retenues, afférents (...) à l'un des emplois ci-après détenus au cours des quinze dernières années d'activité pendant au moins deux ans, dans des conditions fixées par un décret en Conseil d'Etat : / (...) / Ces dispositions sont applicables aux personnels relevant du présent code, occupant en position de détachement (...) les emplois fonctionnels relevant de la loi n° 84-53 du 26 janvier 1984 (...) et dont la liste est fixée par décret en Conseil d'Etat (...) " ; qu'aux termes de l'article R. 27 du même code pris pour l'application de ces dispositions, dans sa rédaction issue du décret du 26 décembre 2003 : " (...) La liste des emplois fonctionnels mentionnée dans le II de l'article L. 15 est la suivante : / 1° Pour les emplois relevant de la loi n° 84 - 53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : / - directeur général des services des départements et des régions et directeur général adjoint des services des régions (...) " et qu'aux termes du premier alinéa de l'article R. 76 bis du même code dans sa rédaction issue du même décret : " Lorsque le fonctionnaire ou le militaire détaché dans un emploi conduisant à pension du régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales a acquitté jusqu'à la date de la cessation des services valables pour la retraite la retenue pour pension sur le traitement ou solde afférent aux emplois prévus par le II de l'article L. 15, la liquidation de la pension est effectuée sur la base du traitement ou solde correspondant " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, à la date du 1er janvier 2008 à compter de laquelle M.  B...a été admis à la retraite, celui-ci était premier conseiller de chambre régionale des comptes en position de détachement auprès du département des Hauts-de-Seine pour occuper l'emploi fonctionnel de directeur général adjoint des services du département ; que la pension de retraite qui lui a été accordée à compter de cette date a été liquidée en prenant en compte, sur le fondement du I de l'article L. 15 du code des pensions civiles et militaires de retraite, le traitement afférent à l'indice correspondant au grade et à l'échelon qu'il détenait dans la fonction publique de l'Etat ; que, par une décision du 30 avril 2008, le ministre du budget, des comptes publics et de la fonction publique a rejeté la demande de révision de cette pension que lui présentait M. B..., lequel soutenait que sa pension devait être liquidée, non pas sur le fondement des dispositions du I de l'article L. 15 mais sur le fondement de celles du II du même article, en prenant en compte le traitement afférent à son emploi de détachement  ; que M. B...se pourvoit en cassation contre le jugement du 30 juin 2009 par lequel le tribunal administratif de Caen a rejeté sa demande tendant, d'une part, à l'annulation de cette décision et, d'autre part, à la révision de sa pension ;<br/>
<br/>
              Considérant qu'en se bornant à relever, pour répondre à l'exception d'illégalité que M. B...invoquait à l'encontre des dispositions de l'article R. 27, " qu'il ne résulte pas de l'instruction qu'en faisant figurer à la liste des emplois fonctionnels relevant de la loi n° 84-53 du 26 janvier 1984 mentionnée à l'article L. 15 précité celui de directeur général adjoint des services des régions mais non, avant le 26 juin 2008, celui des services des départements, alors que ces emplois sont visés ensemble à l'article 53 de la loi du 26 janvier 1984 précitée, l'article R. 27 du code des pensions civiles et militaires de retraite aurait méconnu le principe d'égalité ", sans indiquer pour quelle raison il estimait que cette différence de traitement n'est pas contraire au principe d'égalité, le tribunal administratif a insuffisamment motivé son jugement ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, ce jugement doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond  par application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant, en premier lieu que, sauf disposition contraire, le droit à pension de retraite des fonctionnaires doit être apprécié compte tenu des textes en vigueur à la date à compter de laquelle la pension est attribuée ; qu'il en résulte que la pension de retraite attribuée à M. B...à compter du 1er janvier 2008 devait être liquidée en faisant application des dispositions de l'article R. 27 dans sa rédaction résultant du décret du 26 décembre 2003 et non du décret du 23 juin 2008 ; que, dès lors que l'emploi de directeur général adjoint des services du département qu'occupait M. B...n'est pas mentionné dans la liste figurant à l'article R. 27, dans sa rédaction résultant du décret du 26 décembre 2003, l'intéressé ne pouvait bénéficier, pour la liquidation de sa pension de retraite, des dispositions du II de l'article L. 15 du code des pensions civiles et militaires de retraite ni, par suite, de celles de l'article R. 76 bis du même code ; que la circonstance que cet emploi figure sur la liste des emplois fonctionnels pour lesquels l'article 53 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale fixe des règles relatives à la fin du détachement est sans incidence sur les droits à pension de retraite des personnes qui occupent ces emplois fonctionnels ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes du premier alinéa de l'article L. 63 du code des pensions civiles et militaires de retraite : " Toute perception d'un traitement ou solde d'activité (...) est soumise au prélèvement de la retenue visée aux articles L. 61 et L. 62 même si les services ainsi rémunérés ne sont pas susceptibles d'être pris en compte pour la constitution du droit ou pour la liquidation de la pension " ; qu'il résulte de ces dispositions que le moyen par lequel M. B...soutient que des retenues pour pension ont été effectuées sur le traitement qui lui était versé en tant que directeur général adjoint des services administratifs du département des Hauts-de-Seine est inopérant ;<br/>
<br/>
              Considérant, enfin, que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée ; <br/>
<br/>
              Considérant que, s'il résulte des dispositions de l'article R. 27 du code des pensions civiles et militaires de retraite, dans leur rédaction antérieure à celle résultant du décret du 23 juin 2008, que le fonctionnaire détaché dans un emploi fonctionnel de directeur général adjoint des services des régions bénéficie, pour la liquidation de sa pension de retraite, des dispositions du II de l'article L. 15, alors que celui détaché dans un emploi fonctionnel de directeur général adjoint des services des départements n'en bénéficie pas, cette différence de traitement entre des fonctionnaires employés par des catégories de collectivités territoriales différentes ne méconnaît pas le principe d'égalité ; qu'ainsi, cette différence de traitement ne méconnaît pas davantage les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales combinées avec celles de l'article 1er du premier protocole additionnel à la même convention ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M.  B...n'est fondé ni à demander l'annulation de la décision du 30 avril 2008 par laquelle le ministre du budget, des comptes publics et de la fonction publique a refusé de réviser sa pension civile de retraite, ni à demander qu'il soit procédé à cette révision ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Caen du 30 juin 2009 est annulé.<br/>
Article 2 : La demande présentée par  M. B...devant le tribunal administratif de Caen est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre du budget, des comptes publics et de la réforme de l'Etat, porte parole du Gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LE SERVICE PUBLIC. ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS. - DIFFÉRENCE DE TRAITEMENT, POUR LA LIQUIDATION DE LA PENSION, ENTRE DES FONCTIONNAIRES EMPLOYÉS PAR DES CATÉGORIES DE COLLECTIVITÉS TERRITORIALES DIFFÉRENTES - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-02-03 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. LIQUIDATION DE LA PENSION. - FONCTIONNAIRES DÉTACHÉS SUR UN EMPLOI FONCTIONNEL - BÉNÉFICE DES DISPOSITIONS DU II DE L'ARTICLE L. 15 DU CPCMR - DIFFÉRENCE DE TRAITEMENT ENTRE FONCTIONNAIRES EMPLOYÉS PAR DES CATÉGORIES DE COLLECTIVITÉS TERRITORIALES DIFFÉRENTES - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">48-03-04 PENSIONS. RÉGIMES PARTICULIERS DE RETRAITE. PENSIONS DES AGENTS DES COLLECTIVITÉS LOCALES. - FONCTIONNAIRES DÉTACHÉS SUR UN EMPLOI FONCTIONNEL - BÉNÉFICE DES DISPOSITIONS DU II DE L'ARTICLE L. 15 DU CPCMR - DIFFÉRENCE DE TRAITEMENT ENTRE FONCTIONNAIRES EMPLOYÉS PAR DES CATÉGORIES DE COLLECTIVITÉS TERRITORIALES DIFFÉRENTES - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 01-04-03-03-02 S'il résulte des dispositions de l'article R. 27 du code des pensions civiles et militaires de retraite (CPCMR) que le fonctionnaire détaché dans un emploi fonctionnel de directeur général adjoint des services (DGAS) des régions bénéficie, pour la liquidation de sa pension de retraite, des dispositions du II de l'article L. 15 du même code, alors que celui détaché dans un emploi fonctionnel de DGAS des départements n'en bénéficie pas, cette différence de traitement entre des fonctionnaires employés par des catégories de collectivités territoriales différentes ne méconnaît pas le principe d'égalité.</ANA>
<ANA ID="9B"> 48-02-02-03 S'il résulte des dispositions de l'article R. 27 du code des pensions civiles et militaires de retraite (CPCMR) que le fonctionnaire détaché dans un emploi fonctionnel de directeur général adjoint des services (DGAS) des régions bénéficie, pour la liquidation de sa pension de retraite, des dispositions du II de l'article L. 15 du même code, alors que celui détaché dans un emploi fonctionnel de DGAS des départements n'en bénéficie pas, cette différence de traitement entre des fonctionnaires employés par des catégories de collectivités territoriales différentes ne méconnaît pas le principe d'égalité.</ANA>
<ANA ID="9C"> 48-03-04 S'il résulte des dispositions de l'article R. 27 du code des pensions civiles et militaires de retraite (CPCMR) que le fonctionnaire détaché dans un emploi fonctionnel de directeur général adjoint des services (DGAS) des régions bénéficie, pour la liquidation de sa pension de retraite, des dispositions du II de l'article L. 15 du même code, alors que celui détaché dans un emploi fonctionnel de DGAS des départements n'en bénéficie pas, cette différence de traitement entre des fonctionnaires employés par des catégories de collectivités territoriales différentes ne méconnaît pas le principe d'égalité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
