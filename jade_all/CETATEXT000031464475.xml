<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464475</ID>
<ANCIEN_ID>JG_L_2015_11_000000385962</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464475.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 09/11/2015, 385962, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385962</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Françoise Guilhemsans</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:385962.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une ordonnance n° 1424422/5-2 du 20 novembre 2014, enregistrée le 25 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par le syndicat national Solidaires-Justice. <br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Paris le 25 octobre 2014, le syndicat national Solidaires-Justice demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision née du silence gardé par le garde des sceaux, ministre de la justice, sur sa demande tendant à l'abrogation de la note du 6 mai 2013 relative à la mise en oeuvre de la journée de solidarité au sein de l'administration pénitentiaire ; <br/>
<br/>
              2°) d'enjoindre sous astreinte au garde des sceaux, ministre de la justice, d'abroger cette note. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ; <br/>
              - le code du travail, notamment son article L. 3133-7 ; <br/>
              - la loi n° 2004-626 du 30 juin 2004, notamment son article 6 ; <br/>
- l'arrêté du 20 décembre 2005 du garde des Sceaux, ministre de la justice portant application de la loi n° 2004-626 du 30 juin 2004 relative à la solidarité pour l'autonomie des personnes âgées et des personnes handicapées au ministère de la justice ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Françoise Guilhemsans, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la fin de non-recevoir opposée par le garde des sceaux, ministre de la justice : <br/>
<br/>
              1. Considérant que les fonctionnaires et les associations ou syndicats qui défendent leurs intérêts collectifs n'ont pas qualité pour demander l'annulation des dispositions des circulaires ou instructions se rapportant à l'organisation ou à l'exécution du service, sauf dans la mesure où ces dispositions portent atteinte à leurs droits et prérogatives ou affectent leurs conditions d'emploi et de travail ; que la décision attaquée, par laquelle le garde des sceaux, ministre de la justice, a refusé d'abroger une note prévoyant les modalités de mise en oeuvre de la journée de solidarité au sein de l'administration pénitentiaire, affecte les conditions d'emploi des agents de cette administration, dont le syndicat national Solidaires-Justice assure la défense des intérêts collectifs ; que, par suite, le syndicat requérant justifie d'un intérêt lui donnant qualité pour agir contre cette décision ; <br/>
<br/>
              Sur la légalité de la décision attaquée : <br/>
<br/>
              2. Considérant qu'aux termes de l'articles L. 3133-7 du code du travail : " La journée de solidarité instituée en vue d'assurer le financement des actions en faveur de l'autonomie des personnes âgées ou handicapées prend la forme 1° d'une journée supplémentaire de travail non rémunérée pour les salariés (...) " ; qu'aux termes de l'article 6 de la loi du 30 juin 2004 relative à la solidarité pour l'autonomie des personnes âgées et des personnes handicapées : " Pour les fonctionnaires et agents non titulaires relevant de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat (...) la journée de solidarité mentionnée à l'article L. 3133-7 du code du travail est fixée dans les conditions suivantes : / (...) / - dans la fonction publique de l'Etat, par un arrêté du ministre compétent pris après avis du comité technique ministériel concerné. / Dans le respect des procédures énoncées aux alinéas précédents, la journée de solidarité peut être accomplie selon les modalités suivantes : / 1° Le travail d'un jour férié précédemment chômé autre que le 1er mai ; / 2° Le travail d'un jour de réduction du temps de travail tel que prévu par les règles en vigueur ; / 3° Toute autre modalité permettant le travail de sept heures précédemment non travaillées, à l'exclusion des jours de congé annuel. " ; qu'aux termes de l'article 1er de l'arrêté du 20 décembre 2005 du garde des Sceaux, ministre de la justice portant application de la loi du 30 juin 2004 relative à la solidarité pour l'autonomie des personnes âgées et des personnes handicapées au ministère de la justice : " Pour les fonctionnaires et agents non titulaires relevant de la loi du 11 janvier 1984 susvisée placés sous l'autorité du ministre de la justice, la journée de solidarité instituée au titre II de la loi du 30 juin 2004 susvisée prend la forme d'une des deux dispositions suivantes : / 1. Sept heures travaillées, soit continues, soit fractionnées, en jours ou en heures ; / 2. Une journée décomptée au titre de la réduction du temps de travail avec restitution au crédit de l'agent du temps accompli, selon le cycle de travail, au-delà de sept heures. " ; <br/>
<br/>
              3. Considérant que par la note faisant l'objet de la demande d'abrogation, le garde des sceaux, ministre de la justice, a prévu qu'à l'exception des personnels de surveillance en service posté travaillant le lundi de Pentecôte, pour lesquels la journée de solidarité instituée par l'article 6 de la loi du 30 juin 2004 cité ci-dessus se traduit par la réalisation d'un temps de travail supplémentaire, la journée de solidarité pour les autres personnels de l'administration pénitentiaire prend la forme d'une journée décomptée au titre de la réduction du temps de travail ; que ces dispositions ne se bornent pas à réitérer les termes de l'arrêté du 20 décembre 2005 cité ci-dessus, mais précisent, en fonction des catégories de personnels en service au sein de l'administration pénitentiaire, selon quelles modalités, parmi les deux prévues par l'arrêté du 20 décembre 2005, la journée de solidarité doit être mise en oeuvre ; qu'eu égard à son objet et à sa portée, la note litigieuse ne pouvait légalement être adoptée sans consultation préalable du comité technique ministériel, conformément aux dispositions précitées de l'article 6 de la loi du 30 juin 2004 ; qu'il ressort cependant des pièces du dossier que cette consultation a été omise ;<br/>
<br/>
              4. Considérant que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ; qu'en l'espèce, la consultation du comité technique ministériel préalablement à l'adoption de la note litigieuse constitue, pour les personnels de l'Etat concernés, une garantie qui découle du principe de participation des travailleurs à la détermination collective des conditions de travail consacré par le huitième alinéa du Préambule de la Constitution de 1946 ; que, par suite, l'omission de cette consultation, qui a privé les représentants de ces personnels d'une garantie, a constitué une irrégularité de nature à entacher la légalité de la note litigieuse ; qu'il suit de là, sans qu'il soit besoin d'examiner les autres moyens de la requête, que le syndicat national Solidaires-Justice est fondé à demander l'annulation pour excès de pouvoir de la décision née du silence gardé par le garde des sceaux, ministre de la justice, sur sa demande tendant à l'abrogation de cette note ; <br/>
<br/>
              5. Considérant que l'annulation de la décision implicite par laquelle le garde des sceaux, ministre de la justice, a refusé d'abroger la note du 6 mai 2013 implique nécessairement l'abrogation de cette note ; qu'il y a lieu pour le Conseil d'Etat d'ordonner cette mesure dans un délai de deux mois à compter de la notification de la présente décision ; que dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par le syndicat requérant ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le garde des sceaux, ministre de la justice, a rejeté la demande du syndicat national Solidaires-Justice tendant à l'abrogation de la note du 6 mai 2013 du garde des sceaux, ministre de la justice, relative à la mise en oeuvre de la journée de solidarité au sein de l'administration pénitentiaire est annulée. <br/>
Article 2 : Il est enjoint au garde des sceaux, ministre de la justice, d'abroger cette note dans un délai de deux mois à compter de la notification de la présente décision. <br/>
<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée au syndicat national Solidaires-Justice et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
