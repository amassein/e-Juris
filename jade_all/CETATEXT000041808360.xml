<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041808360</ID>
<ANCIEN_ID>JG_L_2020_03_000000439695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/80/83/CETATEXT000041808360.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 24/03/2020, 439695, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439695.20200324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 21 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... A... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution du décret n° 2020-260 du 16 mars 2020 portant réglementation des déplacements dans le cadre de la lutte contre la propagation du virus covid-19.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est remplie dès lors que le décret contesté porte une atteinte grave et immédiate et pour une durée d'au moins quinze jours à la liberté d'aller et venir, la liberté d'entreprendre, la liberté de réunion et la dignité humaine des personnes confinées ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'aller et venir protégée par les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen, compte tenu de l'interdiction, sauf dérogation, de se déplacer, à la liberté d'entreprendre protégée par l'article 4 de cette Déclaration, compte tenu de la difficulté à maintenir une activité professionnelle, à la liberté de réunion et, enfin, à la dignité humaine compte tenu des conséquences psychologiques que pourraient engendrer les mesures contestées qui sont, par ailleurs, disproportionnées et inégalitaires.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n°2020-293 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du code de justice administrative, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
               2. La circonstance qu'une atteinte à une liberté fondamentale, portée par une mesure administrative, serait avérée n'est pas de nature à caractériser l'existence d'une situation d'urgence justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative. Il appartient au juge des référés d'apprécier, au vu des éléments que lui soumet le requérant comme de l'ensemble des circonstances de l'espèce, si la condition d'urgence particulière requise par l'article L. 521-2 est satisfaite, en prenant en compte la situation du requérant et les intérêts qu'il entend défendre mais aussi l'intérêt public qui s'attache à l'exécution des mesures prises par l'administration.<br/>
<br/>
               3. M. et Mme A... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution du décret du 16 mars 2020 portant réglementation des déplacements dans le cadre de la lutte contre la propagation du virus covid-19, qui a été abrogé mais repris par le décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, et qui interdit jusqu'au 31 mars 2020 le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitatives, tenant à diverses nécessités, ainsi que tout regroupement avec la possibilité, pour le représentant de l'Etat dans le département d'adopter des mesures plus strictes si des circonstances locales l'exigent. <br/>
<br/>
               4. Pour justifier de l'urgence à ordonner la suspension demandée, M. et Mme A... se bornent à soutenir que le décret contesté porte une atteinte grave et immédiate pour une durée d'au moins quinze jours à la liberté d'aller et venir, la liberté d'entreprendre, la liberté de réunion et la dignité humaine des personnes confinées.<br/>
<br/>
               5. Eu égard, d'une part, aux circonstances exceptionnelles aux vues desquelles le décret contesté a été pris et qui ont conduit le législateur à déclarer, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, l'état d'urgence sanitaire pour une durée de deux mois, et d'autre part, à l'intérêt public qui s'attache aux mesures de confinement prises, dans le contexte actuel de saturation des structures hospitalières, la condition d'urgence requise par l'article L. 521-2 du code de justice administrative n'est pas remplie. Par suite, il y a lieu de rejeter la requête de M. et Mme A..., selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. et Mme A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M.et Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
