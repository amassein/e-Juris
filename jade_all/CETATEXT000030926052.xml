<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926052</ID>
<ANCIEN_ID>JG_L_2015_07_000000369478</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926052.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 22/07/2015, 369478, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369478</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:369478.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...a demandé au tribunal administratif de Cergy-Pontoise, en premier lieu, d'annuler l'avis du 6 mars 2007 de la commission d'indemnisation des victimes de vaccinations obligatoires sur le fondement duquel le ministre de la santé, de la jeunesse et des sports a, par une décision du 4 mai 2007, rejeté sa demande tendant à l'indemnisation de ses préjudices résultant, selon elle, de la vaccination contre l'hépatite B qu'elle a subie, en deuxième lieu, d'enjoindre à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) de l'indemniser, et enfin d'ordonner une expertise pour évaluer les préjudices qu'elle a subis du fait de cette vaccination. Par un jugement n° 0706984 du 9 décembre 2010, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11VE00506 du 24 janvier 2012, la cour administrative d'appel de Versailles, saisie par MmeA..., a, d'une part, rejeté ses conclusions tendant à l'annulation de l'avis du 6 mars 2007 de cette commission et de la décision du ministre du 4 mai 2007 et, d'autre part, ordonné avant-dire droit une expertise pour déterminer notamment si, en l'état des données acquises de la science, les troubles soufferts par Mme A...sont en relation directe avec la vaccination contre l'hépatite B dont elle a fait  l'objet. Par un second arrêt n° 11VE00506 du 19 février 2013, la cour administrative d'appel a rejeté le surplus des conclusions d'appel de MmeA....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 19 juin et 17 septembre 2013, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 19 février 2013 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme A...et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., manipulatrice en électroradiologie médicale, a reçu plusieurs injections de vaccin contre l'hépatite B les 28 janvier, 3 mars et 6 avril 1992 et les 8 janvier 1993 et 8 janvier 1994 ; qu'elle a déclaré avoir présenté une sciatique paralysante gauche peu après les premières injections de ce vaccin puis, après le rappel administré en 1994, une fatigue et des douleurs diffuses ; qu'une biopsie musculaire, réalisée le 18 septembre 2003, a mis en évidence une lésion histologique de myofasciite à macrophages ; que Mme A...a présenté au ministre chargé de la santé une demande tendant à l'indemnisation des préjudices résultant, selon elle, de la vaccination qu'elle a subie ; que, par une lettre du 4 mai 2007, le ministre, suivant l'avis du 6 mars 2007 de la commission d'indemnisation des victimes de vaccinations obligatoires, a rejeté sa demande ; que, par un jugement du 9 décembre 2010, le tribunal administratif de Cergy-Pontoise a rejeté la demande de Mme A...tendant à ce qu'il annule l'avis rendu par la commission d'indemnisation des victimes de vaccinations obligatoires et condamne l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales à l'indemniser au titre des préjudices résultant de sa vaccination ; que, par l'arrêt du 19 février 2013 contre lequel Mme A...se pourvoit en cassation, la cour administrative d'appel de Versailles a rejeté son appel dirigé contre ce jugement ;<br/>
<br/>
              2. Considérant que, dans le dernier état des connaissances scientifiques, l'existence d'un lien de causalité entre une vaccination contenant un adjuvant aluminique et la combinaison de symptômes constitués notamment par une fatigue chronique, des douleurs articulaires et musculaires et des troubles cognitifs n'est pas exclue et revêt une probabilité suffisante pour que ce lien puisse, sous certaines conditions, être regardé comme établi ; que tel est le cas, lorsque la personne vaccinée, présentant des lésions musculaires de myofasciite à macrophages à l'emplacement des injections, est atteinte de tels symptômes, soit que ces symptômes sont apparus postérieurement à la vaccination, dans un délai normal pour ce type d'affection, soit, si certains de ces symptômes préexistaient, qu'ils se sont aggravés à un rythme et avec une ampleur qui n'étaient pas prévisibles au vu de l'état de santé antérieur à la vaccination, et qu'il ne ressort pas des expertises versées au dossier que les symptômes pourraient résulter d'une autre cause que la vaccination ;<br/>
<br/>
              3. Considérant qu'en écartant, en se fondant sur le rapport d'expertise, l'existence d'un lien de causalité direct et certain, en l'état actuel des connaissances scientifiques, entre les lésions de myofasciite à macrophages apparues à l'emplacement des injections vaccinales et les signes cliniques tels que ceux dont Mme A...est atteinte, la cour administrative d'appel a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à MmeA..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 19 février 2013 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : L'Etat versera à Mme A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
Copie en sera adressée pour information à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la caisse primaire d'assurance maladie de la Seine-Saint-Denis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
