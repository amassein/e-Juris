<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845272</ID>
<ANCIEN_ID>JG_L_2018_04_000000416740</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845272.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 26/04/2018, 416740, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416740</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP WAQUET, FARGE, HAZAN ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416740.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de la décision du 19 octobre 2017 par laquelle le président du Centre national de la recherche scientifique (CNRS) lui a infligé la sanction de révocation. Par une ordonnance n° 1709631 du 15 décembre 2017, le juge des référés a suspendu l'exécution de cette décision et a enjoint au CNRS, à titre provisoire, de réintégrer M. B...dans ses fonctions.<br/>
<br/>
              1° Sous le numéro 416740, par un pourvoi, enregistré le 21 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, le CNRS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M.B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat du Centre national de la recherche scientifique, à la SCP Waquet, Farge, Hazan, avocat de M. B...et à la SCP Thouvenin, Coudray, Grevy, avocat du syndicat national des travailleurs de la recherche scientifique - CGT.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés, que par une décision du 19 octobre 2017, le président du Centre national de la recherche scientifique (CNRS) a infligé à M. B..., directeur de recherche, la sanction de révocation ; que par une ordonnance du 15 décembre 2017, le juge des référés du tribunal administratif de Marseille a, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, suspendu l'exécution de cette décision et enjoint la réintégration de M. B... à titre provisoire ; que le pourvoi par lequel le CNRS demande l'annulation de cette ordonnance et sa requête tendant à ce qu'il soit sursis à son exécution sont dirigés contre la même décision ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que l'Etat, le syndicat national des travailleurs de la recherche scientifique - CGT, la fédération éducation recherche culture - CGT, l'union fédérale des syndicats de l'Etat - CGT et l'union nationale des syndicats CGT des établissements d'enseignement supérieur et de recherche justifient, chacun, d'un intérêt suffisant à l'annulation de l'ordonnance attaquée ; qu'ainsi, leurs interventions sont recevables ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ; que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ;<br/>
<br/>
              4. Considérant qu'il ressort des termes mêmes de l'ordonnance attaquée que, pour juger que l'urgence justifiait la suspension de la décision contestée, le juge des référés du tribunal administratif de Marseille s'est fondé sur la circonstance qu'elle avait pour effet de faire obstacle à ce que M. B...touche son traitement, sans rechercher si, compte tenu de l'argumentation présentée en défense par le CNRS, relative aux troubles que pouvait causer la réintégration de l'intéressé, la suspension demandée était susceptible de porter à des intérêts publics une atteinte de nature à faire regarder la condition d'urgence comme n'étant pas remplie ; qu'il a, ainsi, entaché son ordonnance d'une erreur de droit ; que le CNRS est, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondé à en demander l'annulation ;<br/>
<br/>
              5. Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par M. B... ;<br/>
<br/>
              6. Considérant que pour demander la suspension de l'exécution de la décision du 19 octobre 2017 du président du CNRS, M. B... soutient qu'elle est entachée d'irrégularité en ce que l'avis de la commission administrative paritaire ne lui a pas été communiqué et en ce que cet avis est insuffisamment motivé ; qu'elle est insuffisamment motivée ; qu'elle retient des faits de comportements dénigrants et oppressants, de comportements déplacés vis-à-vis de personnes de sexe féminin ainsi que de harcèlement et d'attouchements sexuels qui sont matériellement inexacts ; que, s'agissant des faits établis, elle est entachée d'inexacte qualification juridique en ce qu'elle les juge fautifs ; qu'enfin, elle prononce une sanction hors de proportion avec les fautes reprochées ;<br/>
<br/>
              7. Considérant qu'aucun de ces moyens ne paraît, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision attaquée ; que la demande de M. B... doit, par suite, être rejetée, y compris, par voie de conséquence, ses conclusions à fin d'injonction ;<br/>
<br/>
              8. Considérant que l'ordonnance du 15 décembre 2017 du juge des référés du tribunal administratif de Marseille étant annulée par la présente décision, les conclusions par lesquelles le CNRS demande qu'il soit sursis à l'exécution de cette ordonnance sont devenues sans objet ; qu'il n'y a pas lieu d'y statuer ;<br/>
<br/>
              9. Considérant que, le CNRS n'étant pas la partie perdante, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à sa charge à ce titre, tant en ce qui concerne la première instance que l'instance de cassation et celle introduite par la requête aux fins de sursis à exécution ; que, par ailleurs, il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le CNRS au titre de ces mêmes dispositions, tant en première instance qu'en cassation ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de l'Etat, du syndicat national des travailleurs de la recherche scientifique - CGT, de la fédération éducation recherche culture - CGT, de l'union fédérale des syndicats de l'Etat - CGT et de l'union nationale des syndicats CGT des établissements d'enseignement supérieur et de recherche sont admises.<br/>
Article 2 : L'ordonnance du 15 décembre 2017 du juge des référés du tribunal administratif de Marseille est annulée.<br/>
Article 3 : La demande présentée par M. B...devant le tribunal administratif de Marseille est rejetée.<br/>
Article 4 : Il n'y a pas lieu de statuer sur les conclusions de la requête du Centre national de la recherche scientifique tendant à ce qu'il soit sursis à l'exécution de l'ordonnance du 15 décembre 2017 du juge des référés du tribunal administratif de Marseille.<br/>
Article 5 : Les conclusions de M. B...et du Centre national de la recherche scientifique, présentées au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 6 : La présente décision sera notifiée au Centre national de la recherche scientifique, à M. A... B..., à la ministre de l'enseignement supérieur, de la recherche et de l'innovation, au syndicat national des travailleurs de la recherche scientifique - CGT, à la fédération éducation recherche culture - CGT, à l'union fédérale des syndicats de l'Etat - CGT et à l'union nationale des syndicats CGT des établissements d'enseignement supérieur et de recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
