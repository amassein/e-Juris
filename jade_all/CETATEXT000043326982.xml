<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043326982</ID>
<ANCIEN_ID>JG_L_2021_03_000000450351</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/32/69/CETATEXT000043326982.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 31/03/2021, 450351, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450351</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450351.20210331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et des mémoires complémentaires, enregistrés les 4, 12 et 24 mars 2021 au secrétariat du contentieux du Conseil d'Etat, l'Union des chirurgiens de France (UCDF) et l'association " Le Bloc Union AAL - SYNGOF - UVDF " dit " Le Bloc " demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2021-97 du 29 janvier 2021 modifiant le décret n° 2019-678 du 28 juin 2019 et portant diverses mesures relatives au retrait d'enregistrement d'organismes ou structures de développement professionnel continu des professions de santé et aux actes des infirmiers diplômés d'Etat ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en raison du nombre insuffisant d'infirmiers et d'infirmières diplômés de bloc opératoire auxquels sont confiés exclusivement certains actes et activités, le décret contesté conduit à la paralysie complète du fonctionnement des blocs opératoires et porte ainsi atteinte au principe de protection de la santé publique, au fonctionnement du service public de santé, à la continuité des soins, au droit au respect de la vie ainsi qu'au droit à la sécurité ;  <br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ; <br/>
              - il est entaché d'un vice de forme en ce qu'il ne comporte pas les signatures manuscrites du Premier ministre et du ministre chargé de son exécution ; <br/>
              - il est entaché d'une erreur manifeste d'appréciation en ce que les mesures transitoires qu'il prévoit ne respectent ni l'impératif de poursuite du fonctionnement du bloc opératoire, ni le principe de réalité ; <br/>
              - il porte atteinte au droit à la protection de la santé ainsi qu'aux garanties d'égal accès au soin et à la continuité des soins en ce qu'il ne prévoit pas les conditions transitoires dans lesquelles la réalisation des actes exclusifs réalisés par les infirmiers de bloc opératoire doit être assurée, y compris en leur absence ; <br/>
              - il porte atteinte au principe de sécurité juridique et place les acteurs du bloc opératoire dans une situation d'insécurité et d'illégalité en ce que ces derniers peuvent voir leur responsabilité personnelle engagée pour non-respect des conditions de fonctionnement au bloc opératoire en l'absence des infirmiers spécialisés de bloc opératoire ;<br/>
              - il porte atteinte aux principes d'égalité et d'impartialité en l'absence de définition précise de la condition d'activé régulière de certains actes en bloc opératoire. <br/>
<br/>
              Par un mémoire en défense et un mémoire complémentaire, enregistrés les 10 et 11 mars 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et que les moyens soulevés ne sont pas de nature à créer un doute sérieux quant à la légalité du décret attaqué. <br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ; <br/>
              - le décret n° 2015-74 du 27 janvier 2015 ; <br/>
              - le décret n° 2018-79 du 9 février 2018 ;<br/>
              - le décret n° 2019-678 du 28 juin 2019 ;<br/>
              - le décret n° 2021-97 du 29 janvier 2021 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'Union des chirurgiens de France et l'association le Bloc et, d'autre part, le premier ministre et le ministre des solidarités et de la santé ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 25 mars 2021, à 10 heures 30 : <br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - les représentants des requérants ; <br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ;  <br/>
<br/>
              à l'issue de cette audience, l'instruction a été close.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              2. D'une part, aux termes de l'article R. 4311-11 du code de la santé publique : " L'infirmier ou l'infirmière titulaire du diplôme d'Etat de bloc opératoire ou en cours de formation préparant à ce diplôme, exerce en priorité les activités suivantes : / 1° Gestion des risques liés à l'activité et à l'environnement opératoire ; / 2° Elaboration et mise en oeuvre d'une démarche de soins individualisée en bloc opératoire et secteurs associés ; / 3° Organisation et coordination des soins infirmiers en salle d'intervention ; /4° Traçabilité des activités au bloc opératoire et en secteurs associés ; / 5° Participation à l'élaboration, à l'application et au contrôle des procédures de désinfection et de stérilisation des dispositifs médicaux réutilisables (...) / En per-opératoire, l'infirmier ou l'infirmière titulaire du diplôme d'Etat de bloc opératoire ou l'infirmier ou l'infirmière en cours de formation préparant à ce diplôme exerce les activités de circulant, d'instrumentiste et d'aide opératoire en présence de l'opérateur (...) ". D'autre part, aux termes de l'article R. 4311-11-1 du même code : " L'infirmier ou l'infirmière de bloc opératoire, titulaire du diplôme d'Etat de bloc opératoire, est seul habilité à accomplir les actes et activités figurant aux 1° et 2° : / 1° Dans les conditions fixées par un protocole préétabli, écrit, daté et signé par le ou les chirurgiens : / a) Sous réserve que le chirurgien puisse intervenir à tout moment : / - l'installation chirurgicale du patient ; / - la mise en place et la fixation des drains susaponévrotiques ; / - la fermeture sous-cutanée et cutanée ; / b) Au cours d'une intervention chirurgicale, en présence du chirurgien, apporter une aide à l'exposition, à l'hémostase et à l'aspiration ; / 2° Au cours d'une intervention chirurgicale, en présence et sur demande expresse du chirurgien, une fonction d'assistance pour des actes d'une particulière technicité déterminés par arrêté du ministre chargé de la santé ". L'arrêté du 27 janvier 2015 relatif aux actes et activités et à la formation complémentaire prévus par le décret n° 2015-74 du 27 janvier 2015 relatif aux actes infirmiers relevant de la compétence exclusive des infirmiers de bloc opératoire a défini à son annexe I le référentiel d'activités pour l'exercice des actes et activités prévus à l'article R. 4311-11-1 du code de la santé publique. Au III de cet annexe, les actes d'une particulière technicité visés au 2° précité recouvrent ceux liés à l'aide aux sutures des organes et des vaisseaux sous la direction de l'opérateur, à l'aide à la réduction d'une fracture et au maintien de la réduction au bloc opératoire, à l'aide à la pose d'un dispositif médical implantable (DMI) ainsi qu'à l'injection d'un produit à visée thérapeutique ou diagnostique dans un viscère, une cavité, une artère.<br/>
<br/>
              3. Les dispositions de l'article R. 4311-11-1 du code de la santé publique, introduites par le décret n° 2015-74 du 27 janvier 2015, ont pour objet de confier les actes et activités figurant aux 1° et 2° de cet article à la compétence exclusive des infirmiers et infirmières de bloc opératoire diplômés d'Etat (IBODE), à la différence des activités mentionnées à l'article R. 4311-11 du même code qui, étant seulement confiées en priorité à ces personnels qualifiés, peuvent être partagés avec, en particulier, les autres infirmiers et infirmières diplômés d'Etat (IDE) exerçant en bloc opératoire. Ces nouvelles dispositions sont entrées en vigueur le 30 janvier 2015, c'est-à-dire le lendemain de leur publication au Journal officiel. Toutefois, par une décision nos 389036, 389589, 390121 du 7 décembre 2016, le Conseil d'Etat, statuant au contentieux, après avoir retenu que le décret du 27 janvier 2015 était entaché d'une erreur manifeste d'appréciation faute de prévoir des mesures transitoires, compte tenu des conséquences d'une telle entrée en vigueur immédiate sur le fonctionnement des services, l'a annulé en tant seulement que, en l'état du dispositif applicable, il ne différait pas au 31 décembre 2017 l'entrée en vigueur des dispositions du b) du 1° de l'article R. 4311-11-1 du code de la santé publique. <br/>
<br/>
              4. En exécution de cette décision, les mesures transitoires qui ont été prises ont consisté, d'une part, à repousser l'entrée en vigueur des dispositions du b) du 1° de l'article R. 4311-11-1 du code de la santé publique relatives aux actes d'aide à l'exposition, à l'hémostase et à l'aspiration au 1er juillet 2019 en vertu du décret du 9 février 2018 portant diverses mesures d'adaptation relatives aux professions de santé puis au 1er janvier 2020 en vertu du décret du 28 juin 2019 relatif aux conditions de réalisation de certains actes professionnels en bloc opératoire par les infirmiers et portant report d'entrée en vigueur de dispositions transitoires sur les infirmiers en bloc opératoire. Elles ont consisté, d'autre part, en application de ce dernier décret, à autoriser, dans un premier temps, à titre temporaire, à compter de l'enregistrement d'un dossier complet et jusqu'au plus tard le 31 décembre 2021, tout infirmier ou infirmière exerçant une fonction d'infirmier de bloc opératoire depuis une durée au moins égale à un an en équivalent temps plein à la date du 30 juin 2019 et apportant de manière régulière une aide à l'exposition, à l'hémostase et à l'aspiration en cours d'intervention chirurgicale, de continuer cette activité, sous réserve de son inscription avant le 31 octobre 2019 à une épreuve de vérification des connaissances puis, dans un second temps, de l'autoriser à titre définitif, en cas de validation des épreuves.<br/>
<br/>
              5. Le décret du 29 janvier 2021, contesté par le présent recours, a modifié le dispositif transitoire mis en place par le décret du 28 juin 2019, d'une part, en repoussant l'échéance de dépôt du dossier d'autorisation au 31 mars 2021, d'autre part, en prévoyant que pourront également s'inscrire les personnels infirmiers exerçant en bloc opératoire sans disposer de la spécialité mais vérifiant les conditions de durée et de pratique rappelées au point précédent appréciées au 31 décembre 2019, et, enfin, en remplaçant l'épreuve de vérification des connaissances par l'obligation de suivre une formation complémentaire, financée par l'employeur, relative à l'aide à l'exposition, à l'hémostase et à l'aspiration, avant le 31 décembre 2025, l'attestation de suivi avec assiduité de cette formation permettant la délivrance d'une autorisation définitive d'exercice dérogatoire. L'arrêté ministériel du 31 juillet 2019 relatif à la formation complémentaire pour la réalisation de certains actes professionnels en bloc opératoire par les infirmiers, modifié, a prévu que la durée de cette formation suivie au sein d'une école autorisée pour la préparation du diplôme d'Etat infirmier de bloc opératoire, est de vingt et une heures.<br/>
<br/>
              Sur la demande des associations requérantes :<br/>
<br/>
              6. L'UCDF, syndicat professionnel représentant les chirurgiens quelle que soit leur spécialité, et l'association Le Bloc qui fédère des syndicats représentatifs d'anesthésistes, chirurgiens et gynécologues obstétriciens, demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution du décret du 29 janvier 2021 modifiant le décret du 28 juin 2019 pour des raisons qui tiennent, en premier lieu, à la régularité formelle de la décision ou à l'imprécision de certains de ses termes et, en second lieu, en tant qu'il n'a pas prévu de nouvelles mesures transitoires pour permettre aux infirmiers et infirmières diplômés d'Etat de pratiquer l'ensemble des actes exclusifs des IBODE mentionnés à l'article R. 4311-11-1 du code de la santé publique.<br/>
<br/>
              Sur les moyens relatifs à l'édiction de l'acte et à la légalité de son article 2 :<br/>
<br/>
              7. Les moyens tirés, d'une part, du vice de forme en ce que le décret contesté ne comporterait pas les signatures manuscrites du Premier ministre et du ministre chargé de son exécution et, d'autre part, de l'atteinte aux principes d'égalité et d'impartialité en l'absence de définition précise de la condition d'activé régulière de certains actes en bloc opératoire prévue à l'article 2 du décret, ne sont pas, en l'état de l'instruction, de nature à faire naître un doute sérieux sur le la légalité du décret contesté.<br/>
<br/>
              Sur les moyens relatifs à l'absence d'adoption de certaines mesures transitoires :<br/>
<br/>
              8. Il résulte de l'instruction, complétée par les échanges à l'audience, que le déficit important d'IBODE en termes d'effectifs au regard des besoins nés du nombre très élevé d'interventions en blocs opératoires, peine à être comblé, et ce, alors même que la réforme issue du décret du 27 janvier 2015, leur a confié l'exclusivité des actes et activités mentionnés à l'article R. 4311-11-1 du code de santé publique. En exécution de l'annulation partielle prononcée, ainsi qu'il a été dit au point 3, par la décision du 7 décembre 2016 du Conseil d'Etat, statuant au contentieux, le gouvernement a pris le décret du 28 juin 2019, modifié sur ce point par celui, actuellement contesté devant le juge des référés, du 29 janvier 2021, qui a prévu des mesures transitoires portant sur les actes du b) du 1° de l'article R. 4311-11-1 du code de la santé publique. Les associations requérantes, qui ne contestent ni le cadre juridique posé par le décret du 27 janvier 2015, ni d'ailleurs les améliorations apportées par le décret du 29 janvier 2021 au dispositif transitoire mis en place pour les actes du b) du 1°, reprochent en revanche à ce dernier décret de ne pas étendre ce régime transitoire aux actes et activités relevant du a) du 1° et à ceux d'une technicité particulière relevant du 2° de l'article R. 4311-11-1 précité. Elles font valoir que les quelque 13 000 IDE exerçant en blocs opératoires et ayant déposé un dossier d'autorisation temporaire, disposent d'un savoir-faire, résultant d'une formation reçue en interne et d'une pratique habituelle, leur permettant d'exercer, en présence du chirurgien, l'ensemble des actes exclusifs et qu'ils sont, dans l'immédiat, indispensables à la poursuite de l'activité des blocs opératoires et à la continuité des soins, aussi longtemps que les IBODE en exercice et en formation ne seront pas en nombre suffisant. Elles font par ailleurs valoir que les établissements de santé privés ne regroupent que 10 % des 8 500 IBODE actuellement recensés en France en dépit du nombre important d'interventions qu'ils assurent.<br/>
<br/>
              9. Dans son avis du 7 décembre 2020, sollicité en vue de l'adoption du décret contesté, l'Académie de médecine a indiqué être " très au fait des tensions dans les blocs opératoires liées, entre autres, aux difficultés démographiques et aux contraintes des activités spécifiques aux interventions opératoires, non seulement pour les IBODES, dont la reconnaissance de leur qualification pour l'ensemble des activités péri et per opératoires devrait être mieux valorisée, mais aussi pour les Infirmiers Diplômés d'Etat travaillant au bloc opératoire qui représentent le plus grand nombre et dont la sécurisation professionnelle doit être préservée. " Elle a également, fait observer le caractère inapproprié d'une distinction trop rigide entre les différentes formes d'actes et d'activités au bloc opératoire mentionnés à l'article R. 4311-11-1 du code de santé publique, et a, compte tenu de la démographie très insuffisante des IBODE, invité le gouvernement à étendre le dispositif transitoire mis en place pour les actes du b) du 1° précités à la totalité des actes dits exclusifs de l'article R. 4311-11-1 du code de la santé publique tant que le nombre des infirmiers et infirmières spécialisés n'aura pas atteint un effectif suffisant. Cette situation préoccupante conduit actuellement le gouvernement à prévoir l'adoption d'un ensemble de mesures destinées, d'une part, à la revalorisation, dans les meilleurs délais, de la situation professionnelle et pécuniaire des IBODE permettant en outre de renforcer l'attractivité de cette spécialisation, d'autre part, à l'aménagement de la formation et de l'accès au diplôme notamment par une validation des acquis de l'expérience plus adaptée permettant de tenir compte des différents parcours professionnels, et, enfin, dans un avenir proche, à l'adoption de nouvelles mesures transitoires portant spécifiquement sur les autres actes dits exclusifs relevant du a) du 1° et du 2° de l'article R. 4311-11-1 du code de la santé publique. Il résulte des indications données au cours de l'audience que le projet de décret relatif à ce dernier point fait actuellement l'objet des consultations requises en vue d'une adoption prochaine. <br/>
<br/>
              10. Il résulte, d'une part, de l'annulation partielle prononcée par la décision du 7 décembre 2016 du Conseil d'Etat, statuant au contentieux, mentionnée au point 3, que son exécution ne faisait obligation au pouvoir réglementaire que de prévoir un dispositif transitoire pour les actes d'aide à l'exposition, à l'hémostase et à l'aspiration prévus au b) du 1° de l'article R. 4311-11-1 du code de la santé publique, lequel a d'ailleurs été mis en place progressivement par l'édiction des décrets des 28 juin 2019 et 29 janvier 2021. Il résulte, d'autre part, des termes du décret du 29 janvier 2021 contesté qu'il a pour objet de modifier le dispositif transitoire résultant du décret du 28 juin 2019 et également de supprimer l'avis du Haut conseil du développement professionnel continu dans le cadre de la procédure de sanction des organismes de développement professionnel continu ainsi que de permettre, par ailleurs, la prise en charge par les infirmiers libéraux de soins postopératoires à domicile, notamment la surveillance ou le retrait de cathéters périnerveux pour analgésie postopératoire. En revanche, il n'a pas pour objet de mettre en place un dispositif transitoire relatif aux actes et activités relevant du a) du 1° ou du 2° de l'article R. 4311-11-1 du code de la santé publique. Il résulte de l'instruction que les mesures actuellement envisagées par le gouvernement qui, dans l'exercice de son pouvoir réglementaire n'était pas tenu d'épuiser sa compétence en adoptant un texte unique, devraient, à l'issue de la phase d'examen en cours, porter en particulier sur la mise en place d'un régime transitoire spécifique relatif à la sécurisation de la pratique des actes exclusifs en l'absence d'IBODE. Compte tenu de l'ensemble de ces considérations, les moyens critiquant le décret du 29 janvier 2021 en tant qu'il ne prévoit pas de dispositions transitoires appropriées au titre des actes relevant du a) du 1° et du 2° de l'article R. 4311-11-1 du code de la santé publique, fondés sur une erreur manifeste d'appréciation, sur une atteinte au droit à la protection de la santé ou aux garanties d'égal accès au soin et à la continuité des soins, ou encore sur une atteinte au principe de sécurité juridique, ne sont pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité du décret contesté.<br/>
<br/>
              11. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête de l'UCDF et de l'association Le Bloc doit être rejetée, y compris leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'UCDF et de l'association Le Bloc est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à l'Union des chirurgiens de France, à l'association Le Bloc, au Premier ministre ainsi qu'au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
