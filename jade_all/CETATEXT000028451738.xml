<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028451738</ID>
<ANCIEN_ID>JG_L_2013_12_000000361156</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/45/17/CETATEXT000028451738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 30/12/2013, 361156, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361156</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361156.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
              Par une requête, enregistrée le 18 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 1200122 du 28 février 2012 par laquelle le tribunal administratif de Montpellier a rejeté sa demande tendant à être autorisée à exercer une action en justice pour le compte de la commune de Quillan (Aude), en vue de porter plainte contre X avec constitution de partie civile, aux fins de condamnation pour destruction d'un bien de la commune et de versement de dommages et intérêts en réparation du préjudice subi par la commune ; <br/>
<br/>
              2°) de l'autoriser à engager cette action au nom de la commune ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Barthélemy, Matuchansky, Vexliard, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
              Par un mémoire en défense, enregistré le 7 décembre 2012, la commune de Quillan conclut au rejet de la requête et à ce que la somme de 2 000 euros soit mise à la charge de Mme B... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de MmeB..., et à la SCP Célice, Blancpain, Soltner, avocat de la commune de Quillan ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes de l'article L. 2132-5 du code général des collectivités territoriales : " Tout contribuable inscrit au rôle de la commune a le droit d'exercer, tant en demande qu'en défense, à ses frais et risques, avec l'autorisation du tribunal administratif, les actions qu'il croit appartenir à la commune, et que celle-ci, préalablement appelée à en délibérer, a refusé ou négligé d'exercer ". Il appartient au tribunal administratif statuant comme autorité administrative et au Conseil d'Etat, saisi d'un recours de pleine juridiction dirigé contre la décision du tribunal administratif, lorsqu'ils examinent une demande présentée par un contribuable sur le fondement de ces dispositions, de vérifier, sans se substituer au juge de l'action, et au vu des éléments qui leur sont fournis, que l'action envisagée présente un intérêt matériel suffisant pour la commune et qu'elle a une chance de succès.<br/>
<br/>
              2. MmeB..., après avoir saisi le conseil municipal de la commune de Quillan, a sollicité du tribunal administratif de Montpellier l'autorisation d'engager une action pénale du chef de destruction d'un immeuble d'habitation et d'un garage situés sur la parcelle cadastrée AX n° 59, appartenant à la commune, avec constitution de partie civile en vue d'obtenir réparation du préjudice subi. Par la décision attaquée, le tribunal a rejeté cette demande au motif qu'une telle action serait dépourvue de chance de succès et d'intérêt matériel suffisant.<br/>
<br/>
              3. Il résulte de l'instruction que l'immeuble d'habitation et sa dépendance situés sur la parcelle cadastrée AX n° 59, objet de la destruction contestée, ont été acquis par la commune de Quillan par voie d'expropriation dans le cadre de la réalisation d'un projet d'aménagement de la " zone de loisirs du lac Saint-Bertrand ", déclaré d'utilité publique par un arrêté préfectoral du 15 février 1995. Les faits de démolition reprochés sont intervenus en avril 2009 sur le fondement d'un permis de démolir du 14 mai 2007, qui était en vigueur au moment des faits et dont il n'est pas allégué qu'il aurait été obtenu par fraude. Ils ne peuvent être poursuivis ni sur le fondement invoqué de l'article 322-1 du code pénal, qui sanctionne la destruction, la dégradation ou la détérioration d'un bien appartenant à autrui ni, en tout état de cause, sur le fondement de l'article L. 480-4 du code de l'urbanisme, qui sanctionne l'exécution de travaux  en infraction, notamment, au régime du permis de démolir. Dès lors, il n'apparaît pas que l'action envisagée par la requérante présente une chance de succès.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de vérifier que l'action envisagée n'est pas prescrite ni qu'elle présente un intérêt suffisant pour la commune, que Mme B... n'est pas fondée à demander l'annulation de la décision attaquée. Par suite, ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent qu'être rejetées. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées, au titre des dispositions du même article L. 761-1, par la commune de Quillan.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
Article 2 : Les conclusions de la commune de Quillan présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B... et à la commune de Quillan.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
