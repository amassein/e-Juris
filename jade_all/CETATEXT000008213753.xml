<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008213753</ID>
<ANCIEN_ID>JG_L_2005_11_000000271982</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/21/37/CETATEXT000008213753.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 07/11/2005, 271982, Publié au recueil Lebon</TITRE>
<DATE_DEC>2005-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>271982</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Hagelsteen</PRESIDENT>
<AVOCATS>SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>M. Edouard  Crépey</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Glaser</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 8 septembre 2004 et 10 janvier 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMPAGNIE GENERALE DES EAUX, représentée par son représentant légal, domicilié en cette qualité au siège de la société, 52 rue d'Anjou, à Paris (75008) ; la COMPAGNIE GENERALE DES EAUX demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2 et 3 de la décision du 11 juillet 2002 par laquelle le conseil de la concurrence a, d'une part, dit qu'il était établi que les sociétés COMPAGNIE GENERALE DES EAUX et Lyonnaise des eaux ont contrevenu aux dispositions de l'article L. 420-2 du code de commerce et, d'autre part, demandé au ministre chargé de l'économie d'enjoindre aux mêmes sociétés de modifier, compléter ou résilier, dans un délai déterminé, tous accords et tous actes qui les ont conduites à associer leurs moyens dans le cadre des filiales communes qu'elles ont créées conjointement dans les secteurs de l'eau potable et de l'assainissement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Crépey, Auditeur,  <br/>
<br/>
              - les observations de la SCP Vier, Barthélemy, Matuchansky, avocat de la COMPAGNIE GENERALE DES EAUX, <br/>
<br/>
              - les conclusions de M. Emmanuel Glaser, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 430-9 du code de commerce : Le conseil de la concurrence peut, en cas d'exploitation abusive d'une position dominante ou d'un état de dépendance économique, demander au ministre chargé de l'économie d'enjoindre, conjointement avec le ministre dont relève le secteur, par arrêté motivé, à l'entreprise ou au groupe d'entreprises en cause de modifier, de compléter ou de résilier, dans un délai déterminé, tous accords et tous actes par lesquels s'est réalisée la concentration de la puissance économique qui a permis les abus même si ces actes ont fait l'objet de la procédure prévue au présent titre ;<br/>
<br/>
              Considérant qu'après s'être saisi d'office, dans le cadre de ses attributions relatives au contrôle des pratiques anti-concurrentielles, de la situation du marché de l'eau potable et de l'assainissement, le conseil de la concurrence a, par une décision du 11 juillet 2002, dans un article 1er, décidé qu'il n'y avait pas lieu de poursuivre la procédure en ce qui concerne le grief tiré d'une méconnaissance des dispositions de l'article L. 420-1 du code de commerce relatif aux ententes, dans un article 2, affirmé qu'il était établi que la COMPAGNIE GENERALE DES EAUX et la société Lyonnaise des eaux avaient contrevenu aux dispositions de l'article L. 420-2 du même code relatif aux abus de position dominante, sans toutefois assortir ce constat d'aucune autre conséquence que celle consistant à demander, dans un article 3, au ministre chargé de l'économie, sur le fondement des dispositions précitées de l'article L. 430-9 du même code, d'enjoindre aux sociétés intéressées de modifier, compléter ou résilier les accords qui les avaient conduites à associer leurs moyens dans le cadre de filiales communes ; qu'après que la cour d'appel de Paris, saisie par la COMPAGNIE GENERALE DES EAUX, eut confirmé cette décision par un arrêt du 18 février 2003, la Cour de cassation a annulé ce dernier sans renvoi, au motif que le seul objet de la décision attaquée était la mise en oeuvre des pouvoirs dévolus au conseil par l'article L. 430-9 du code de commerce et que les décisions prises sur le fondement de cet article n'étaient susceptibles que d'un recours devant le juge administratif ; que la COMPAGNIE GENERALE DES EAUX a alors formé un recours pour excès de pouvoir contre la décision litigieuse ;<br/>
<br/>
              Considérant qu'en vertu d'un principe fondamental reconnu par les lois de la République, à l'exception des matières réservées par nature à l'autorité judiciaire, relève en dernier ressort de la compétence de la juridiction administrative l'annulation ou la réformation des décisions prises par les autorités exerçant le pouvoir exécutif dans l'exercice des prérogatives de puissance publique ; qu'en l'absence de disposition législative expresse dérogeant à ce principe, il appartient à cette juridiction de connaître de la légalité des actes pris pour l'application de l'article L. 430-9 du code de commerce, qui figure au titre III du livre IV, relatif à la concentration économique ; <br/>
<br/>
              Considérant, en premier lieu, que bien qu'il figure dans le dispositif de la décision attaquée, le constat par lequel le conseil de la concurrence a estimé établi que la COMPAGNIE GENERALE DES EAUX et la société Lyonnaise des eaux ont méconnu les dispositions de l'article L. 420-2 du code de commerce prohibant les abus de position dominante constitue non pas une décision faisant grief, mais seulement le fondement de la demande adressée au ministre de prononcer des injonctions visant à mettre un terme à la concentration de puissance économique qui a permis ces abus ; <br/>
<br/>
              Considérant, en second lieu, que la décision par laquelle le conseil de la concurrence saisit le ministre d'une demande tendant à ce qu'il prenne les mesures entrant dans les prévisions de l'article L. 430-9 du code de commerce n'est qu'un élément de la procédure qui permet à celui-ci de prononcer des injonctions à l'encontre des entreprises en cause ; que, contrairement à ce que soutient la société requérante, elle est, par elle-même, dépourvue de tout effet juridique à leur égard ; qu'elle présente, dès lors, le caractère d'un acte préparatoire et n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir ; que, par suite, la requête ne peut qu'être rejetée, sans que la COMPAGNIE GENERALE DES EAUX puisse utilement se prévaloir de la Convention européenne des droits de l'homme et des libertés fondamentales ni du principe de légalité ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que la COMPAGNIE GENERALE DES EAUX demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la COMPAGNIE GENERALE DES EAUX est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la COMPAGNIE GENERALE DES EAUX, au conseil de la concurrence et au ministre de l'économie, des finances et de l'industrie.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-005-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. ACTES. ACTES ADMINISTRATIFS. - DÉCISIONS PRISES PAR LES AUTORITÉS DÉTENANT LE POUVOIR EXÉCUTIF DANS L'EXERCICE DES PRÉROGATIVES DE PUISSANCE PUBLIQUE - ACTES PRIS POUR L'APPLICATION DE L'ARTICLE L. 430-9 DU CODE DE COMMERCE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES PRÉPARATOIRES. - DÉCISION PAR LAQUELLE LE CONSEIL DE LA CONCURRENCE SAISIT LE MINISTRE D'UNE DEMANDE TENDANT À CE QU'IL PRENNE DES MESURES ENTRANT DANS LES PRÉVISIONS DE L'ARTICLE L. 430-9 DU CODE DE COMMERCE.
</SCT>
<ANA ID="9A"> 17-03-02-005-01 En vertu d'un principe fondamental reconnu par les lois de la République, à l'exception des matières réservées par nature à l'autorité judiciaire, relève en dernier ressort de la compétence de la juridiction administrative l'annulation ou la réformation des décisions prises par les autorités détenant le pouvoir exécutif dans l'exercice des prérogatives de puissance publique. En l'absence de disposition législative expresse dérogeant à ce principe, il appartient à cette juridiction de connaître de la légalité des actes pris pour l'application de l'article L. 430-9 du code de commerce.</ANA>
<ANA ID="9B"> 54-01-01-02-02 La décision par laquelle le conseil de la concurrence saisit le ministre d'une demande tendant à ce qu'il prenne des mesures entrant dans les prévisions de l'article L. 430-9 du code de commerce n'est qu'un élément de la procédure qui permet à celui-ci de prononcer des injonctions à l'encontre des entreprises en cause. Elle est, par elle-même, dépourvue de tout effet juridique à leur égard et présente, dès lors, le caractère d'un acte préparatoire qui n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
