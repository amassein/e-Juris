<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037271441</ID>
<ANCIEN_ID>JG_L_2018_07_000000421985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/14/CETATEXT000037271441.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/07/2018, 421985, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:421985.20180719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de la décision du 20 juin 2018 par laquelle la préfète de la Loire-Atlantique a refusé d'aménager son obligation de se présenter au commissariat de Saint-Nazaire durant la mi-temps des rencontres sportives du FC Nantes et de l'équipe de France de football et, d'autre part, d'enjoindre à la préfète de la Loire-Atlantique de faire droit à sa demande d'aménagement de cette obligation ou, à défaut, de procéder au réexamen de sa demande, dans un délai de cinq jours à compter de la notification de l'ordonnance à intervenir, sous astreinte de 50 euros par jour de retard. Par une ordonnance n° 1805882 du 2 juillet 2018, le juge des référés du tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par une requête et trois mémoires complémentaires, enregistrés les 4, 6, 9 et 12 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la décision litigieuse va produire ses effets au mois de juillet 2018 ; <br/>
              - il est porté une atteinte grave et manifestement illégale, d'une part, à sa liberté d'aller et venir dès lors que la décision litigieuse l'empêche de se rendre à son lieu de travail ainsi que de partir en congés et, d'autre part, à sa liberté d'entreprendre dès lors qu'elle l'empêche d'exercer toute activité professionnelle dont les horaires entreraient en conflit avec son obligation de présentation au commissariat et le contraint ainsi notamment à méconnaître son contrat de travail ;<br/>
              - l'absence d'aménagement de son obligation de présentation au commissariat porte également atteinte au respect de sa vie privée en raison de son caractère disproportionné ;<br/>
              - le calendrier précis de l'ensemble des rencontres du FC de Nantes au mois de juillet n'est pas déterminé suffisamment à l'avance pour lui permettre d'en tenir compte dans le cadre de l'organisation de ses horaires de travail ainsi que de ses congés ; <br/>
              - il n'existe aucun impératif d'intérêt général faisant obstacle à la suspension de l'exécution de la décision litigieuse dès lors que, d'une part, il a toujours respecté l'interdiction administrative de stade dont il fait l'objet, d'autre part, le procureur de la République a reconnu que les faits qui ont justifié son interdiction administrative de stade n'étaient pas établis en classant sans suite son dossier et, enfin, la préfète de la Loire-Atlantique n'apporte aucun élément de nature à établir que son comportement serait susceptible de caractériser un trouble à l'ordre public ;<br/>
              - la décision litigieuse a été prise en application de l'arrêté du 25 août 2017, qui est lui-même entaché d'illégalité dès lors qu'il n'est pas établi qu'il aurait méconnu l'arrêté du 12 avril 2017 du préfet du Calvados portant interdiction de déplacement des supporters du FC Nantes à Caen à l'occasion de la rencontre avec le Stade Malherbe Caen (SM Caen) du 22 avril 2017, ainsi que l'a reconnu le procureur de la République en classant son dossier sans suite.<br/>
<br/>
              Par un mémoire en défense, enregistré le 11 juillet 2018, le ministre d'Etat, ministre de l'intérieur, conclut au rejet de la requête. Il fait valoir que la condition d'urgence n'est pas remplie et qu'il n'est porté aucune atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B..., d'autre part, le ministre d'Etat, ministre de l'intérieur ;<br/>
              Vu le procès-verbal de l'audience publique du 12 juillet 2018 à 15 heures au cours de laquelle ont été entendus : <br/>
              - le représentant de M.B... ;<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 juillet 2018, par laquelle le ministre d'Etat, ministre de l'intérieur indique que la préfète de la Loire-Atlantique a abrogé la décision litigieuse et fait droit à la demande d'aménagement de M. B...et conclut ainsi au non-lieu à statuer sur les conclusions de la requête ;<br/>
<br/>
              L'instruction a été rouverte le 13 juillet 2018 et la clôture en a été reportée au 16 juillet 2018 à 17 heures. <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 13 juillet 2018 avant la clôture de l'instruction, par lequel M.B..., d'une part, conclut au non-lieu à statuer sur ses conclusions à fin de suspension de l'exécution de la décision du 20 juin 2018 ainsi que celles à fin d'injonction et, d'autre part, maintient ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
              2. Considérant qu'aux termes de l'article L. 332-16 du code du sport : " Lorsque, par son comportement d'ensemble à l'occasion de manifestations sportives, par la commission d'un acte grave à l'occasion de l'une de ces manifestations, du fait de son appartenance à une association ou un groupement de fait ayant fait l'objet d'une dissolution en application de l'article L. 332-18 ou du fait de sa participation aux activités qu'une association ayant fait l'objet d'une suspension d'activité s'est vue interdire en application du même article, une personne constitue une menace pour l'ordre public, le représentant de l'Etat dans le département et, à Paris, le préfet de police peuvent, par arrêté motivé, prononcer à son encontre une mesure d'interdiction de pénétrer ou de se rendre aux abords des enceintes où de telles manifestations se déroulent ou sont retransmises en public. L'arrêté, valable sur le territoire national, fixe le type de manifestations sportives concernées. (...). Le représentant de l'Etat dans le département et, à Paris, le préfet de police peuvent également imposer, par le même arrêté, à la personne faisant l'objet de cette mesure l'obligation de répondre, au moment des manifestations sportives objet de l'interdiction, aux convocations de toute autorité ou de toute personne qualifiée qu'il désigne. Le même arrêté peut aussi prévoir que l'obligation de répondre à ces convocations s'applique au moment de certaines manifestations sportives, qu'il désigne, se déroulant sur le territoire d'un Etat étranger. Cette obligation doit être proportionnée au regard du comportement de la personne. Le fait, pour la personne, de ne pas se conformer à l'un ou à l'autre des arrêtés pris en application des alinéas précédents est puni d'un an d'emprisonnement et de 3 750 euros d'amende " ;<br/>
              3. Considérant que par un arrêté du 25 août 2017, la préfète de la Loire-Atlantique a interdit à M. B...de pénétrer ou de se rendre aux abords d'une enceinte où se déroule une manifestation sportive de football à laquelle participe une équipe du Football Club de Nantes (FC Nantes) ou l'équipe de France de football du 9 septembre 2017 au 9 septembre 2018 ; que, par ce même arrêté, la préfète de la Loire-Atlantique a fait obligation à M.B..., d'une part, de se présenter au commissariat de Saint-Nazaire à la mi-temps des rencontres de football, toutes compétitions confondues, y compris les matchs amicaux, impliquant l'équipe professionnelle du FC Nantes et, d'autre part, d'informer sans délai l'administration de toute impossibilité de déférer à cette obligation ; que, par un courrier du 30 mai 2018, M. B... a demandé à la préfète de la Loire-Atlantique d'aménager son obligation de présentation au commissariat du 14 juin au 15 juillet 2018 pour les rencontres de l'équipe de France de football lors de la coupe du monde en Russie afin de lui permettre de s'y rendre cinq heures au maximum avant ou après chaque rencontre et non à la mi-temps ; qu'en outre, par ce même courrier, M. B... a informé la préfète de la Loire-Atlantique de son impossibilité de se présenter au commissariat de Saint-Nazaire du 20 au 22 juillet 2018 en raison de sa participation au festival " Les Vieilles Charrues " à Carhaix, du 25 au 28 juillet en raison de sa participation à la feria de Bayonne et du 29 juillet au 5 août 2018 en raison d'un voyage en Croatie ; que, par un courrier du 20 juin 2018, la préfète de la Loire-Atlantique a rejeté sa demande ; que M. B...relève appel de l'ordonnance du 2 juillet 2018 par laquelle le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant, d'une part, à la suspension de l'exécution de la décision du 20 juin 2018 et, d'autre part, à ce qu'il soit enjoint à la préfète de la Loire-Atlantique de faire droit à sa demande d'aménagement ou, à défaut, de procéder au réexamen de sa demande ;<br/>
              4. Considérant qu'ainsi que l'a relevé le juge des référés du tribunal administratif de Nantes, l'arrêté du 25 août 2017 ne fait pas obligation à M. B...de se présenter au commissariat à la mi-temps des rencontres de l'équipe de France ; qu'en outre, par une décision du 12 juillet 2018, la préfète de la Loire-Atlantique a, d'une part, abrogé la décision du 20 juin 2018 et, d'autre part, autorisé M. B...à se présenter, à la mi-temps des rencontres de l'équipe professionnelle du FC Nantes, à la brigade de gendarmerie de Carhaix-Plouger pendant le festival " Les Vieilles Charrues ", au commissariat de Bayonne pendant la feria et par téléphone depuis la ligne fixe de son hôtel lors de son voyage en Croatie ; que tant le ministre de l'intérieur que M. B...s'accordent, dans le dernier état de leurs écritures, pour estimer que les conclusions à fin de suspension de l'exécution de la décision du 20 juin 2018 ainsi que celles à fin d'injonction sont devenues sans objet ; qu'il n'y a, dès lors, pas lieu d'y statuer ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B...sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat la somme de 2 500 euros à ce titre ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de M. B...tendant à la suspension de l'exécution de la décision du 20 juin 2018 par laquelle la préfète de la Loire-Atlantique a refusé d'aménager son obligation de présentation au commissariat de Saint-Nazaire et à enjoindre à la même autorité de faire droit à sa demande d'aménagement de cette obligation ou, à défaut, de procéder au réexamen de sa demande.<br/>
Article 2 : L'Etat versera à M. B...la somme de 2 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
