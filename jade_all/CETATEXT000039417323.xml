<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039417323</ID>
<ANCIEN_ID>JG_L_2019_11_000000417631</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/41/73/CETATEXT000039417323.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 22/11/2019, 417631</TITRE>
<DATE_DEC>2019-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417631</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:417631.20191122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Me C..., agissant en qualité de mandataire judiciaire de la société SMPA Transmar et M. A..., gérant de cette société, ont demandé au tribunal administratif de Paris de condamner l'Etat à leur verser, respectivement, les sommes de 452 443,70 euros et 164 000 euros, avec intérêts et capitalisation des intérêts, en réparation des préjudices qu'ils estiment avoir subis du fait de la décision du préfet de police refusant de leur accorder le concours de la force publique pour procéder à l'expulsion des occupants de locaux sis 44, rue Montcalm à Paris. Par un jugement n° 1613327/3-3 du 28 novembre 2017, le tribunal administratif a condamné l'Etat à verser à la société SMPA Transmar une indemnité de 95 261,65 euros, assortie des intérêts au taux légal à compter du 30 décembre 2014 et a rejeté le surplus des conclusions de la demande. <br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 417631, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 janvier et 20 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la société SMPA Transmar et M. A... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il rejette le surplus de leurs conclusions ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 417743, par un pourvoi, enregistré le 29 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du 28 novembre 2017 du tribunal administratif de Paris en tant qu'il fait droit aux conclusions de la société SMPA Transmar ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société SMPA Transmar.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
	Vu <br/>
	- le code des procédures civiles d'exécution ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gouz-Fitoussi, avocat de la société SMPA Transmar et de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision née le 25 août 2003 du silence gardé sur une demande de la société SMPA Transmar, le préfet de police a refusé d'accorder à cette société le concours de la force publique pour l'exécution d'un jugement du 16 janvier 2003 du tribunal d'instance du dix-huitième arrondissement de Paris ordonnant l'expulsion des occupants d'un local commercial lui appartenant. Sur la demande de la société, le tribunal administratif de Paris a, par un premier jugement du 17 juin 2011 devenu définitif, condamné l'Etat à l'indemniser du préjudice d'indisponibilité ayant résulté pour elle de l'occupation de son local jusqu'à la date du 31 décembre 2010. Ultérieurement saisi d'une demande de la même société et de son gérant, M. A..., portant sur d'autres préjudices nés, selon eux, du même refus du préfet de police et se rapportant notamment à l'indisponibilité du local au-delà du 31 décembre 2010, le tribunal administratif a, par un jugement du 28 novembre 2017, jugé que la responsabilité de l'Etat était engagée pour l'occupation illicite du local jusqu'au 21 mai 2012 et l'a condamné, à ce titre, à verser à la société SMPA Transmar une indemnité de 95 261,65 euros. Le ministre de l'intérieur demande l'annulation de ce jugement en tant qu'il fixe le montant du préjudice locatif, en soutenant que ce préjudice n'existait que jusqu'au 13 juillet 2011. Par un pourvoi qu'il y a lieu de joindre au pourvoi du ministre de l'intérieur pour statuer par une seule décision, la société SMPA Transmar et M. A... demandent l'annulation du même jugement en tant qu'il rejette le surplus de leurs conclusions. <br/>
<br/>
              Sur le jugement en tant qu'il statue sur le préjudice locatif : <br/>
<br/>
              2. Lorsque l'administration a refusé au propriétaire d'un local le concours de la force publique pour procéder à l'expulsion d'occupants sans droit ni titre de ce local et qu'il est établi que ceux-ci ont spontanément quitté les lieux, la responsabilité de l'Etat n'est susceptible d'être engagée à l'égard du propriétaire, au titre des préjudices résultant pour lui de l'indisponibilité du local, que jusqu'à la date du départ des occupants. En cas de réinstallation ultérieure de ceux-ci ou de toute personne n'y ayant pas de titre, la responsabilité de l'Etat ne peut, le cas échéant, être engagée au titre de cette nouvelle occupation qu'en raison d'un nouveau refus de concours de la force publique répondant à une nouvelle demande du propriétaire.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que si, par un courrier du 7 juin 2012, l'huissier chargé de procéder à l'expulsion a informé la société que, le dernier occupant ayant restitué les clés du local au commissariat de police le 21 mai 2012, il avait, à cette date, procédé à la reprise des lieux, le même huissier avait, ainsi que l'établissait un précédent procès-verbal d'expulsion versé au dossier, déjà procédé à la reprise des lieux le 15 juillet 2011, sans émettre de réserves. Le tribunal administratif ne pouvait, par suite, sans dénaturer les pièces du dossier qui lui était soumis, estimer que le local avait été continûment occupé entre le 31 décembre 2010 et le 21 mai 2012.<br/>
<br/>
              4. Dès lors, il résulte de ce qui a été dit au point 2 qu'en jugeant que le préjudice locatif de la société SMPA Transmar devait être réparé pour une période allant du 31 décembre 2010 au 21 mai 2012, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              5. Le ministre de l'intérieur est ainsi fondé à demander l'annulation du jugement qu'il attaque en tant qu'il condamne l'Etat à verser une indemnité réparant un préjudice locatif au-delà du 15 juillet 2011. <br/>
<br/>
              Sur le jugement en tant qu'il statue sur le préjudice résultant de l'impossibilité de vendre le local :<br/>
<br/>
              6. Il ressort des termes mêmes du jugement attaqué que, pour rejeter les conclusions de la société SMPA Transmar relatives au préjudice ayant résulté, pour elle, de l'impossibilité de vendre le local en cause pendant qu'il était occupé, le tribunal administratif s'est fondé sur ce que la requérante n'établissait pas qu'un projet de vente aurait été empêché ou retardé du fait de l'occupation illicite du local. En recherchant ainsi l'existence d'un projet de cession précis, alors que, d'une part, il ressortait des pièces du dossier qui lui était soumis que la société SMPA Transmar, en sa qualité de marchand de biens, avait pour objet social l'achat d'immeubles aux fins de les revendre et que, d'autre part, l'occupation illicite d'un local est, par elle-même, de nature à faire obstacle à une telle transaction, le tribunal a entaché son jugement d'une erreur de droit.<br/>
<br/>
              7. Il résulte toutefois de ce qui a été dit au point 5 que le jugement attaqué, qui a condamné l'Etat à réparer un préjudice locatif, est devenu définitif en tant qu'il condamne l'Etat à verser une indemnité réparant ce préjudice pour la période comprise entre le 1er janvier 2010 et le 15 juillet 2011. Or l'indemnisation du préjudice susceptible d'être né, pour la société SMPA Transmar, de l'impossibilité de vendre son local au cours d'une certaine période, lequel peut notamment résulter de la diminution de sa valeur vénale au cours de cette période ou de l'impossibilité de tirer des revenus, pendant cette période, du placement de la somme attendue en paiement de la vente, ne saurait se cumuler à l'indemnisation d'un préjudice locatif pour cette même période.<br/>
<br/>
              8. Par suite, la société requérante n'est fondée à demander l'annulation du jugement qu'elle attaque qu'en tant qu'il rejette l'indemnisation, par l'Etat, de la part du préjudice lié à l'impossibilité de vendre le local au cours de la période allant du 1er janvier 2010 au 15 juillet 2011 qui excèderait, le cas échéant, la somme due par l'Etat au titre du préjudice locatif sur cette même période.<br/>
<br/>
              Sur le jugement en tant qu'il statue sur les autres préjudices invoqués par la société requérante et M. A... :<br/>
<br/>
              9. En premier lieu, en estimant que la société SMPA Transmar n'établissait pas que l'avis à tiers détenteur du 25 septembre 2008 lui notifiant un rappel d'imposition de 315 000 euros, qu'elle présentait comme la conséquence de l'impossibilité de vendre le local litigieux dans les cinq années suivant son acquisition en 1996, était la conséquence du refus de concours de la force publique opposé en 2003, le tribunal a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation. Le motif par lequel le jugement ajoute que la somme en cause n'a pas été payée par la société étant surabondant, les moyens que la société requérante soulève contre lui sont inopérants.<br/>
<br/>
              10. En deuxième lieu, le tribunal n'a, en tout état de cause, pas dénaturé les pièces du dossier qui lui était soumis en estimant que les frais exposés par la société SMPA Transmar lors de l'instance de cassation engagée par elle, devant le Conseil d'Etat, contre le précédent jugement du 17 juin 2011 mentionné au point 1, étaient, en l'espèce, dépourvus de tout lien avec le refus de concours de la force publique qui lui avait été opposé en août 2003. La société requérante ne saurait, à cet égard, utilement invoquer la circonstance que l'Etat a été condamné, par le jugement attaqué, à l'indemniser d'une partie de ces mêmes frais.<br/>
<br/>
              11. Enfin, en estimant que les préjudices de perte de jouissance et de perte de revenus de M. A..., ainsi que le préjudice moral qu'il invoquait étaient liés, non au refus de concours de la force publique opposé par le préfet de police, mais aux difficultés financières de la société SMPA Transmar, le tribunal, qui n'était pas tenu, eu égard à l'argumentation soulevée devant lui, de se prononcer sur l'éventuelle aggravation de ces difficultés née de ce refus, a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation et n'a pas commis d'erreur de droit.<br/>
<br/>
              12. Il résulte de ce tout ce qui précède que le jugement du 28 novembre 2017 du tribunal administratif de Paris doit être annulé en tant seulement que, d'une part, il procède à l'indemnisation du préjudice locatif de la société SMPA Transmar pour la période postérieure au 15 juillet 2011 et que, d'autre part, il rejette sa demande d'indemnisation de la part du préjudice résultant de l'impossibilité de vendre le local qui excèderait, le cas échéant, son préjudice locatif.<br/>
<br/>
              13. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat les sommes demandées par la société SMPA Transmar et par M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 28 novembre 2017 du tribunal administratif de Paris est annulé en tant qu'il procède à l'indemnisation du préjudice locatif de la société SMPA Transmar pour la période postérieure au 15 juillet 2011 et en tant qu'il rejette sa demande d'indemnisation de la part du préjudice résultant de l'impossibilité de vendre le local qui excèderait, le cas échéant, son préjudice locatif.<br/>
Article 2 : L'affaire est renvoyée dans la limite de la cassation ainsi prononcée au tribunal administratif de Paris.<br/>
Article 3 : Les conclusions présentées par la société SMPA Transmar et par M. A... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : Le surplus des conclusions des parties est rejeté. <br/>
Article 5 : La présente décision sera notifiée au ministre de l'intérieur, à la société SMPA Transmar et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. CONCOURS DE LA FORCE PUBLIQUE. - REFUS DE CONCOURS DE LA FORCE PUBLIQUE POUR PROCÉDER À L'EXPULSION D'OCCUPANTS SANS TITRE D'UN LOCAL - INDEMNISATION - 1) OCCUPANTS AYANT SPONTANÉMENT QUITTÉ LES LIEUX - RESPONSABILITÉ DE L'ETAT NE POUVANT ÊTRE ENGAGÉE QUE JUSQU'À LA DATE DU DÉPART DES OCCUPANTS - CONSÉQUENCE - NÉCESSITÉ D'UNE NOUVELLE DEMANDE DE CONCOURS DE LA FORCE PUBLIQUE POUR ENGAGER LA RESPONSABILITÉ DE L'ETAT EN CAS DE RÉINSTALLATION ULTÉRIEURE DES OCCUPANTS - 2) POSSIBILITÉ DE CUMULER L'INDEMNISATION DU PRÉJUDICE NÉ DE L'IMPOSSIBILITÉ DE VENDRE LE LOCAL AU COURS D'UNE CERTAINE PÉRIODE AVEC L'INDEMNISATION D'UN PRÉJUDICE LOCATIF POUR CETTE MÊME PÉRIODE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-01-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ FONDÉE SUR L'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES. RESPONSABILITÉ DU FAIT DE L'INTERVENTION DE DÉCISIONS ADMINISTRATIVES LÉGALES. - REFUS DE CONCOURS DE LA FORCE PUBLIQUE POUR PROCÉDER À L'EXPULSION D'OCCUPANTS SANS TITRE D'UN LOCAL - INDEMNISATION - 1) OCCUPANTS AYANT SPONTANÉMENT QUITTÉ LES LIEUX - RESPONSABILITÉ DE L'ETAT NE POUVANT ÊTRE ENGAGÉE QUE JUSQU'À LA DATE DU DÉPART DES OCCUPANTS - CONSÉQUENCE - NÉCESSITÉ D'UNE NOUVELLE DEMANDE DE CONCOURS DE LA FORCE PUBLIQUE POUR ENGAGER LA RESPONSABILITÉ DE L'ETAT EN CAS DE RÉINSTALLATION ULTÉRIEURE DES OCCUPANTS - 2) POSSIBILITÉ DE CUMULER L'INDEMNISATION DU PRÉJUDICE NÉ DE L'IMPOSSIBILITÉ DE VENDRE LE LOCAL AU COURS D'UNE CERTAINE PÉRIODE AVEC L'INDEMNISATION D'UN PRÉJUDICE LOCATIF POUR CETTE MÊME PÉRIODE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. - PRÉJUDICE INDEMNISABLE - REFUS DE CONCOURS DE LA FORCE PUBLIQUE POUR PROCÉDER À L'EXPULSION D'OCCUPANTS SANS TITRE D'UN LOCAL - POSSIBILITÉ DE CUMULER L'INDEMNISATION DU PRÉJUDICE NÉ DE L'IMPOSSIBILITÉ DE VENDRE LE LOCAL AU COURS D'UNE CERTAINE PÉRIODE AVEC L'INDEMNISATION D'UN PRÉJUDICE LOCATIF POUR CETTE MÊME PÉRIODE - ABSENCE.
</SCT>
<ANA ID="9A"> 37-05-01 1) Lorsque l'administration a refusé au propriétaire d'un local le concours de la force publique pour procéder à l'expulsion d'occupants sans droits ni titre de ce local et qu'il est établi que ceux-ci ont spontanément quitté les lieux, la responsabilité de l'Etat n'est susceptible d'être engagée à l'égard du propriétaire, au titre des préjudices résultant pour lui de l'indisponibilité du local, que jusqu'à la date du départ des occupants. En cas de réinstallation ultérieure de ceux-ci ou de toute personne n'y ayant pas de titre, la responsabilité de l'Etat ne peut, le cas échéant, être engagée au titre de cette nouvelle occupation qu'en raison d'un nouveau refus de concours de la force publique répondant à une nouvelle demande du propriétaire.,,,2) L'indemnisation du préjudice susceptible d'être né, pour le propriétaire, de l'impossibilité de vendre son local au cours d'une certaine période, lequel peut notamment résulter de la diminution de sa valeur vénale au cours de cette période, ou de l'impossibilité de tirer des revenus, pendant cette période, du placement de la somme attendue en paiement de la vente, ne saurait se cumuler à l'indemnisation d'un préjudice locatif pour cette même période.</ANA>
<ANA ID="9B"> 60-01-02-01-01-03 1) Lorsque l'administration a refusé au propriétaire d'un local le concours de la force publique pour procéder à l'expulsion d'occupants sans droits ni titre de ce local et qu'il est établi que ceux-ci ont spontanément quitté les lieux, la responsabilité de l'Etat n'est susceptible d'être engagée à l'égard du propriétaire, au titre des préjudices résultant pour lui de l'indisponibilité du local, que jusqu'à la date du départ des occupants. En cas de réinstallation ultérieure de ceux-ci ou de toute personne n'y ayant pas de titre, la responsabilité de l'Etat ne peut, le cas échéant, être engagée au titre de cette nouvelle occupation qu'en raison d'un nouveau refus de concours de la force publique répondant à une nouvelle demande du propriétaire.,,,2) L'indemnisation du préjudice susceptible d'être né, pour le propriétaire, de l'impossibilité de vendre son local au cours d'une certaine période, lequel peut notamment résulter de la diminution de sa valeur vénale au cours de cette période, ou de l'impossibilité de tirer des revenus, pendant cette période, du placement de la somme attendue en paiement de la vente, ne saurait se cumuler à l'indemnisation d'un préjudice locatif pour cette même période.</ANA>
<ANA ID="9C"> 60-04-01 L'indemnisation du préjudice susceptible d'être né, pour le propriétaire, de l'impossibilité de vendre son local au cours d'une certaine période, lequel peut notamment résulter de la diminution de sa valeur vénale au cours de cette période, ou de l'impossibilité de tirer des revenus, pendant cette période, du placement de la somme attendue en paiement de la vente, ne saurait se cumuler à l'indemnisation d'un préjudice locatif pour cette même période.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
