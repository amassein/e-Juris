<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034946965</ID>
<ANCIEN_ID>JG_L_2017_06_000000411033</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/94/69/CETATEXT000034946965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/06/2017, 411033, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411033</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:411033.20170607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
         Par une requête, enregistrée le 31 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. E...B...demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
         1°) d'annuler l'ordonnance n° 1700463 du 17 mai 2017 du juge des référés du tribunal administratif de Mayotte en ce qu'elle a rejeté les demandes tendant à la suspension de l'interdiction de retour sur le territoire de trois ans prononcée à son encontre par le préfet de Mayotte le 15 mai 2017 ;<br/>
<br/>
         2°) d'ordonner la suspension de l'exécution de l'arrêté du 15 mai 2017 du préfet de Mayotte en tant qu'il comporte une interdiction de retour sur le territoire français pendant une durée de trois ans ;<br/>
<br/>
         3°) d'enjoindre au préfet de Mayotte de réexaminer sa situation ;<br/>
<br/>
         4°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire et de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
         Il soutient que :<br/>
         - la condition d'urgence est remplie dès lors que, si l'obligation de quitter le territoire français sans délai a été suspendue par l'ordonnance contestée, l'interdiction de retour sur le territoire de trois ans dont il fait l'objet demeure, sans qu'il soit en mesure de demander l'abrogation d'une telle mesure depuis Mayotte, et alors que cette interdiction l'empêche de se voir admettre au séjour dans l'espace Schengen ;<br/>
         - il existe une atteinte grave et manifestement illégale au respect de sa vie privée et familiale protégée par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, à sa liberté d'aller et venir, et à l'intérêt supérieur des ses enfants consacré par l'article 3-1 de la convention internationale des droits de l'enfant, dès lors qu'il vit à Mayotte en concubinage avec MmeD..., que ses enfants y sont scolarisés et qu'il contribue à leur entretien et leur éducation ;<br/>
         - en application du principe de l'exception d'illégalité, l'illégalité et la suspension de la mesure d'éloignement sans délai privent de base légale et entraînent la suspension de l'interdiction de retour sur le territoire français qui l'accompagne ;<br/>
         - l'interdiction de retour sur le territoire prononcée à son encontre est illégale car elle n'a pas été motivée après examen de la particularité de sa situation personnelle.<br/>
<br/>
         Vu les autres pièces du dossier ;<br/>
<br/>
         Vu :<br/>
         - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
- le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
         Considérant ce qui suit :<br/>
<br/>
        1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
         2. Il résulte de l'instruction que M.B..., né aux Comores le 31 décembre 1980, soutient être entré à Mayotte en 2011, vivre en concubinage avec Mme C...A...ae, mère de ses trois filles, à proximité de sa soeur et de sa mère, et contribuer à l'entretien et l'éducation de ses enfants. Par un arrêté du 15 mai 2017, le préfet de Mayotte a obligé M. B... à quitter le territoire français sans délai en fixant les Comores comme pays de renvoi et a assorti cette obligation d'une interdiction de retour sur le territoire français pendant une durée de trois ans. M. B... a saisi le juge des référés du tribunal administratif de Mayotte, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant notamment à la suspension de l'exécution de l'arrêté du 15 mai 2017.  <br/>
<br/>
         3. Le juge des référés du tribunal administratif de Mayotte a suspendu l'exécution de l'arrêté du préfet de Mayotte obligeant M. B...à quitter le territoire français. M. B...fait appel de son ordonnance en tant seulement qu'elle rejette, pour défaut d'urgence caractérisée, les conclusions de l'intéressé dirigées contre la décision du préfet lui interdisant de revenir sur le territoire français durant trois ans. Mais, ainsi que l'a constaté à bon droit le juge des référés du tribunal administratif de Mayotte, dès lors que la mesure obligeant M.B..., qui se trouve à Mayotte, à quitter le territoire est suspendue, il n'existe aucune urgence caractérisée à suspendre également l'arrêté du préfet de Mayotte en tant qu'il interdit de revenir sur le territoire français. Cette dernière mesure ne produit par elle-même aucun effet tant que l'intéressé se trouve sur le territoire.<br/>
<br/>
         4. Il résulte de ce qui précède, et sans qu'il y ait lieu d'admettre l'intéressé au bénéfice de l'aide juridictionnelle provisoire, que la requête de M. B...ne peut être accueillie et doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de la justice administrative, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. E...B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. E...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
