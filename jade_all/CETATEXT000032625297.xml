<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032625297</ID>
<ANCIEN_ID>JG_L_2016_06_000000391065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/52/CETATEXT000032625297.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 01/06/2016, 391065, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:391065.20160601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 juin, 16 septembre 2015 et 1er février 2016 au secrétariat du contentieux du Conseil d'Etat, la société anonyme Veolia Propreté Nord-Normandie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du secrétaire d'Etat chargé du budget du 14 avril 2015 précisant les modalités de remboursement d'une fraction de la taxe intérieure de consommation sur le gazole utilisé par certains véhicules routiers en tant qu'il prévoit, dans son article 2, une obligation pour les personnes présentant une demande de remboursement sur le fondement de l'article 265 septies du code des douanes de justifier de la consommation réelle de carburant par véhicule en produisant des relevés d'approvisionnement en cuve privative ainsi que la possibilité pour l'administration d'exiger immédiatement le reversement du montant de la taxe remboursé à défaut du respect par la société de ses obligations en matière de pièces justificatives ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des douanes ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2014-1395 du 24 novembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société Véolia Propreté Nord-Normandie.<br/>
<br/>
<br/>
<br/>
              1. Considérant que la société Veolia Propreté Nord-Normandie demande l'annulation pour excès de pouvoir de l'arrêté du secrétaire d'Etat chargé du budget du 14 avril 2015 précisant les modalités de remboursement d'une fraction de la taxe intérieure de consommation sur le gazole utilisé par certains véhicules routiers, en tant qu'il prévoit, en son article 2, une obligation pour les personnes présentant une demande de remboursement d'une fraction de la taxe intérieure de consommation sur le gazole utilisé par certains véhicules routiers sur le fondement de l'article 265 septies du code des douanes de justifier de la consommation réelle de gazole par véhicule en conservant pendant trois ans à compter de leur demande des relevés d'approvisionnement en cuve privative ainsi que la possibilité pour l'administration d'exiger immédiatement le versement du montant de la taxe remboursé en cas de présentation de justificatif faux, erroné, incomplet ou inapplicable ou d'absence de justificatif ; <br/>
<br/>
              Sur la légalité externe de l'arrêté : <br/>
<br/>
              2. Considérant, d'une part, que l'arrêté mentionne qu'il a été pris par le secrétaire d'Etat chargé du budget ; que, par suite, le moyen tiré de ce qu'il ne permettrait pas d'identifier la personne au nom de laquelle il a été pris manque en fait ;<br/>
<br/>
              3. Considérant, d'autre part, qu'il résulte du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que la sous-directrice des droits indirects, dont l'arrêté de  nomination du 26 août 2014 a été publié au Journal officiel de la République française le 28 août 2014, avait qualité pour signer l'arrêté attaqué au nom du secrétaire d'Etat chargé du budget ; que le moyen tiré de l'incompétence de la signataire de la décision attaquée doit, par suite, être écarté ;<br/>
<br/>
              Sur la légalité interne de l'arrêté : <br/>
<br/>
              4. Considérant qu'aux termes de l'article 352 du code des douanes : " Les demandes en restitution de droits et taxes perçus par l'administration des douanes, les demandes en paiement de loyers et les demandes en restitution de marchandises (...) sont présentées à l'administration dans les délais et conditions fixées par décret en Conseil d'Etat " ;<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article 1er du décret du 24 novembre 2014 : " I. - Les demandes mentionnées au 1 de l'article 352 du code des douanes sont introduites, au plus tard, le 31 décembre de la deuxième année suivant celle du paiement du droit ou de la taxe, auprès du directeur régional des douanes territorialement compétent en vertu de l'arrêté prévu par le IV de l'article 2 du présent décret./ II. - Par dérogation au I, les demandes sont introduites : / (...) c) Par le bénéficiaire des régimes prévus aux articles 265 septies et 265 octies du code des douanes, à compter du premier jour ouvrable suivant respectivement la fin du premier et du second semestre de chaque année et, au plus tard, le 31 décembre de la deuxième année qui suit ; " qu'aux termes de l'article 2 du même décret : " I. - Les demandes mentionnées à l'article 1er doivent :/ a) Mentionner le droit ou la taxe concerné ;/ b) Contenir l'exposé des moyens et conclusions du demandeur ;/ c) Porter la signature du demandeur ou de son mandataire./ II. - Elles sont accompagnées de toute pièce justifiant le montant réclamé./ III. - Une demande incomplète peut être régularisée à tout moment. / IV. - Pour les régimes de remboursement mentionnés aux b à e du II de l'article 1er, un arrêté du ministre chargé des douanes précise les pièces justificatives à fournir ainsi que les modalités particulières de présentation et d'instruction des demandes " ;  <br/>
<br/>
              6. Considérant que ce décret a fixé de manière suffisamment précise, dans ses articles 1er et 2, les conditions de présentation des demandes de remboursement tant en ce qui concerne les délais que les conditions de forme et les justificatifs ; qu'il pouvait, en conséquence, renvoyer, par le IV de son article 2, à un arrêté le soin de préciser la définition des modalités particulières de présentation et d'instruction de ces demandes ; que, par suite, le moyen tiré de l'illégalité de la subdélégation doit être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes de l'article 265 septies du code des douanes : " Les personnes soumises au droit commercial au titre de leur activité de transport routier de marchandises, propriétaires (...):/ a) De véhicules routiers à moteur destinés au transport de marchandises et dont le poids total autorisé en charge est égal ou supérieur à 7,5 tonnes ; / b) De véhicules tracteurs routiers dont le poids total roulant est égal ou supérieur à 7,5 tonnes, peuvent obtenir, sur demande de leur part, dans les conditions prévues à l'article 352, le remboursement d'une fraction de la taxe intérieure de consommation sur le gazole, identifié à l'indice 22 et mentionné au tableau B du 1 de l'article 265. (...) Ce remboursement est calculé, au choix du demandeur : - soit en appliquant au volume de gazole utilisé comme carburant dans des véhicules définis aux a et b, acquis dans chaque région et dans la collectivité territoriale de Corse, la différence entre 43,19 euros par hectolitre et le tarif qui y est applicable en application des articles 265 et 265 A bis ; - soit en appliquant, au total du volume de gazole utilisé comme carburant dans des véhicules définis aux a et b, acquis dans au moins trois des régions, dont le cas échéant la collectivité territoriale de Corse, un taux moyen de remboursement calculé en pondérant les différents taux régionaux votés dans les conditions précisées au 2 de l'article 265 et à l'article 265 A bis par les volumes de gazole respectivement mis à la consommation dans chaque région et dans la collectivité territoriale de Corse. Le montant de ce taux moyen pondéré est fixé par arrêté " ;<br/>
<br/>
              8. Considérant que l'arrêté attaqué fixe les modalités particulières d'instruction des demandes de remboursement d'une fraction de la taxe intérieure de consommation sur le gazole acquittée notamment par les transporteurs routiers de marchandises présentées sur le fondement de l'article 265 septies pour certains de leurs véhicules ; qu'en imposant à ces entreprises de conserver les relevés d'approvisionnement en cuve privative et de justifier la consommation réelle de gazole pour chacun de ces véhicules, l'arrêté attaqué s'est borné à tirer les conséquences nécessaires du dispositif prévu par le législateur ; que, par suite, le moyen tiré de ce que l'arrêté excéderait l'habilitation prévue par le décret du 24 novembre 2014 et restreindrait la portée de la loi doit être écarté ;<br/>
<br/>
              9. Considérant, enfin, que l'avant-dernier paragraphe de l'article 2 de l'arrêté se borne à indiquer  les conséquences du non-respect par les entreprises de leurs obligations en précisant que l'administration est en droit d'obtenir la restitution de la fraction de la taxe intérieure de consommation sur le gazole indûment remboursée aux transporteurs routiers ; que le retrait de cet avantage fiscal ne constitue pas une sanction ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la société requérante n'est pas fondée à demander l'annulation des deux derniers paragraphes de l'article 2 de l'arrêté qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La requête de la société anonyme Veolia Propreté Nord-Normandie est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société anonyme Veolia Propreté Nord-Normandie et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
