<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044222843</ID>
<ANCIEN_ID>JG_L_2021_10_000000447161</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/22/28/CETATEXT000044222843.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 18/10/2021, 447161, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447161</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447161.20211018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 2 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'Union nationale des associations agréées d'usagers du système de santé (UNAASS) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-1365 du 10 novembre 2020 pris pour l'application de l'article 20 de la loi n° 2020-473 du 25 avril 2020 de finances rectificative pour 2020 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2020-473 du 25 avril 2020 ;<br/>
              - le décret n° 2020-521 du 5 mai 2020 ;<br/>
              - le décret n° 2020-1098 du 29 août 2020 ; <br/>
              - l'ordonnance n° 446873, 446876, 447162 du 15 décembre 2020 du juge des référés du Conseil d'Etat ; <br/>
              - la décision n° 444000, 444665 du 18 décembre 2020 du Conseil d'Etat ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Chonavel, auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Balat, avocat de l'Union nationale des associations agrées d'usagers du système de santé ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique du litige :  <br/>
<br/>
              1. Le I de l'article 20 de la loi du 25 avril 2020 de finances rectificative pour 2020 dispose que : " Sont placés en position d'activité partielle les salariés de droit privé se trouvant dans l'impossibilité de continuer à travailler pour l'un des motifs suivants : / - le salarié est une personne vulnérable présentant un risque de développer une forme grave d'infection au virus SARS-CoV-2, selon des critères définis par voie réglementaire ; / - le salarié partage le même domicile qu'une personne vulnérable au sens du deuxième alinéa du présent I  (...) ", le III de cet article précisant que : " (...) / Pour les salariés mentionnés aux deuxième et troisième alinéas du (...) I, celui-ci s'applique jusqu'à une date fixée par décret et au plus tard le 31 décembre 2020 (...) / Les modalités d'application du présent article sont définies par voie réglementaire ". <br/>
<br/>
              2. Pour l'application de ces dispositions, le décret du 5 mai 2020 a défini les critères permettant d'identifier les salariés vulnérables présentant un risque de développer une forme grave d'infection au virus SARS-CoV-2. Puis, par le décret du 29 août 2020, le Premier ministre a modifié ces critères à compter du 1er septembre 2020, fixé au 31 août 2020 la date jusqu'à laquelle le I de l'article 20 de la loi du 25 avril 2020 s'applique aux salariés partageant le même domicile qu'une personne vulnérable et abrogé en conséquence le décret du 5 mai 2020 à compter du 1er septembre 2020, sous réserve de son application dans les départements de Guyane et de Mayotte tant que l'état d'urgence sanitaire y est en vigueur. L'exécution du décret du 29 août 2020 a été suspendue par une ordonnance du juge des référés du Conseil d'Etat du 15 octobre 2020, à l'exception des dispositions de son article 1er relatives aux salariés partageant le même domicile qu'une personne vulnérable avant que, par une décision du 18 décembre 2020, le Conseil d'Etat n'annule ses articles 2 et 4 en tant qu'ils dressent la liste des pathologies et situations permettant de considérer une personne comme vulnérable et qu'ils ne diffèrent pas au 4 septembre 2020 l'application de l'article 3 du même décret. Enfin, le décret attaqué du 10 novembre 2020, abrogeant le décret du 5 mai 2020 et les articles 2 à 4 du décret du 29 août 2020, fixe de nouveaux critères pour l'application de l'article 20 de la loi du 25 avril 2020. Sont désormais placés à leur demande en position d'activité partielle au titre de ces dispositions, sur présentation d'un certificat établi par un médecin, les salariés répondant à deux critères cumulatifs. Le premier critère se rapporte, soit à leur âge, d'au moins soixante-cinq ans, soit à leur état de grossesse, à partir du troisième trimestre, soit à la pathologie dont ils sont atteints, dont une liste est dressée. Le second critère tient à leur impossibilité à la fois de recourir au télétravail et de bénéficier de mesures de protections renforcées, que le décret énumère, s'agissant de leur poste de travail et de leur trajet entre leur domicile et leur lieu de travail, notamment pour prendre en compte l'utilisation des moyens de transports collectifs. En cas de désaccord du salarié sur la mise en œuvre par l'employeur de ces mesures de protection renforcées, le salarié saisit le médecin du travail et est placé en activité partielle dans l'attente de son avis. <br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 1411-4 du code de la santé publique : " Le Haut Conseil de la santé publique a pour missions : / (...) 2° De fournir aux pouvoirs publics, en liaison avec les agences sanitaires et la Haute Autorité de santé, l'expertise nécessaire à la gestion des risques sanitaires ainsi qu'à la conception et à l'évaluation des politiques et stratégies de prévention et de sécurité sanitaire (...) / Il peut être consulté par les ministres intéressés, par les présidents des commissions compétentes du Parlement et par le président de l'Office parlementaire d'évaluation des politiques de santé sur toute question relative à la prévention, à la sécurité sanitaire ou à la performance du système de santé ". <br/>
<br/>
              4. Il ne résulte ni des dispositions citées aux points 1 et 3, ni d'aucune autre disposition législative ou réglementaire, que le décret litigieux devait être pris après consultation du Haut conseil de la santé publique. Par suite, le moyen tiré de ce qu'il aurait été adopté à l'issue d'une procédure irrégulière ne peut qu'être écarté. <br/>
<br/>
              5. En second lieu, les dispositions de l'article 20 de la loi du 25 avril 2020 laissent au Premier ministre un large pouvoir d'appréciation pour déterminer le terme des mesures dérogatoires qu'elles prévoient et définir les critères selon lesquels un salarié doit être regardé comme une personne vulnérable présentant un risque de développer une forme grave d'infection au virus SARS-CoV-2. En particulier, ces critères peuvent tenir tant à des pathologies ou des situations associées à un risque élevé, en cas d'infection, d'en développer une forme grave, le cas échéant appréciées par un médecin, qu'aux conditions concrètes de transport vers le lieu de travail et d'exercice des fonctions et au risque associé de contamination. Il incombe toutefois au Premier ministre, dans la mise en œuvre de ce pouvoir réglementaire, de justifier de critères pertinents et cohérents au regard de l'objet de la mesure. <br/>
<br/>
              6. Pour justifier les pathologies et situations retenues par le décret attaqué au titre du premier critère, le ministre des solidarités et de la santé indique que le pouvoir réglementaire a choisi de permettre le recours à l'activité partielle aux salariés que le Haut conseil identifie, depuis un avis du 31 mars 2020, comme à risque grave de covid-19 et qu'il recommande, dans son avis 29 octobre 2020, de considérer comme tels. A cette fin, il a repris la liste des pathologies et situations identifiées par le décret du 5 mai 2020, à laquelle il a ajouté les personnes atteintes de la maladie du motoneurone, d'une myasthénie grave, de sclérose en plaques, de la maladie de Parkinson, de paralysie cérébrale, de quadriplégie ou hémiplégie, d'une tumeur maligne primitive cérébrale ou d'une maladie cérébelleuse progressive. En outre, il a choisi d'inclure également les salariés atteints de maladies rares, le Haut conseil de la santé publique considérant, dans son avis du 29 octobre 2020, qu'elles doivent être également considérées, par précaution, comme des facteurs de risques, bien que ceux-ci n'aient pas été évalués. <br/>
<br/>
              7. Si l'union requérante fait valoir que le Haut conseil de la santé publique a, dans son avis du 29 octobre 2020 relatif à l'actualisation de la liste des facteurs de risque de forme grave de covid-19, complété la liste des pathologies et situations déjà identifiées par une autre liste plus large, incluant " toutes les situations comportant un sur-risque significatif identifié " et précisant, au vu de l'ensemble des données récentes qu'il a examinées, " une gradation du risque ", selon qu'il est " significatif ", " significatif élevé " ou " significatif très élevé ", il ne ressort pas des pièces du dossier que le Premier ministre aurait commis une erreur manifeste d'appréciation en ne retenant pas, au titre du premier critère permettant de bénéficier de l'activité partielle, l'ensemble des situations et pathologies ainsi identifiées.<br/>
<br/>
              8. Il résulte de tout ce qui précède que l'union requérante n'est pas fondée à demander l'annulation pour excès de pouvoir du décret du 10 novembre 2020. Par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union nationale des associations agréées d'usagers du système de santé est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'Union nationale des associations agréées d'usagers du système de santé, au Premier ministre, au ministre des solidarités et de la santé et à la ministre du travail, de l'emploi et de l'insertion.<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : M. Damien Botteghi, assesseur, présidant ; M. Damien Pons, maître des requêtes en service extraordinaire et Mme Manon Chonavel, auditrice-rapporteure.<br/>
<br/>
              Rendu le 18 octobre 2021.<br/>
<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Damien Botteghi<br/>
 		La rapporteure : <br/>
      Signé : Mme Manon Chonavel<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
