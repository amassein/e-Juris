<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042828460</ID>
<ANCIEN_ID>JG_L_2020_12_000000426656</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/82/84/CETATEXT000042828460.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 29/12/2020, 426656, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426656</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426656.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Cergy-Pontoise, d'une part, d'annuler pour excès de pouvoir la décision implicite de la ministre chargée de l'environnement refusant de le placer au 10ème échelon du grade des attachés principaux et, d'autre part, de condamner l'Etat à lui verser la somme de 13 964,68 euros en réparation du préjudice subi du fait de son reclassement illégal. Par un jugement n° 1410520 du 28 avril 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°17VE02077 du 8 novembre 2018, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 27 décembre 2018, les 27 mars et 24 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) à titre subsidiaire, de surseoir à statuer et de saisir à titre préjudiciel la Cour de justice de l'Union européenne de savoir si le principe d'égalité entre travailleurs, prévu notamment par les articles 45 et 157 du traité sur le fonctionnement de l'Union européenne et tel qu'il est consacré par le droit dérivé, s'oppose à ce qu'une législation nationale ne tienne pas compte, pour le calcul de l'avancement lors de la réintégration dans un corps de fonctionnaires à la fin de la position de détachement, de l'avancement acquis au sein de la fonction publique européenne ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2009-972 du 3 août 2009 ; <br/>
              - le décret n° 85-986 du 16 septembre 1985 ;<br/>
              - le décret n° 2010-467 du 7 mai 2010 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... D..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., attaché principal d'administration du ministère de l'écologie, du développement durable et de l'énergie, a été reçu à un concours organisé par l'Office européen de sélection du personnel en vue de recruter des administrateurs au sein de la fonction publique de l'Union européenne. Par une décision du Parlement européen du 16 décembre 2010, il a été nommé fonctionnaire stagiaire de l'Union européenne en qualité d'administrateur dans le groupe de fonctions AD, au grade AD 5, échelon 2, à compter du 1er janvier 2011. A compter de cette date, il a été détaché de son corps d'origine, dans lequel il était classé au 2ème échelon du grade d'attaché principal, au sein de la fonction publique de l'Union européenne par arrêté du ministre de l'écologie du 24 décembre 2010. Par une décision du Parlement européen du 7 octobre 2011, il a été titularisé comme fonctionnaire de l'Union européenne avec effet au 1er octobre 2011, puis a été promu en 2013 au grade AD 6, échelon 1. Par arrêté du 22 décembre 2011 du ministre en charge de l'écologie et du développement durable, il a été maintenu en position de détachement pour une période allant du 1er octobre 2011 au 30 septembre 2016. Par un arrêté du 4 août 2013, M. B... a été réintégré, à sa demande, dans son corps d'origine à compter du 1er septembre 2013, au 4ème échelon du grade d'attaché principal d'administration. Par un courrier du 4 août 2014, M. B... a demandé à être reclassé, à compter de la date de sa réintégration, au 10ème échelon de son grade, soit l'échelon le plus proche de celui qu'il détenait dans son grade de détachement au sein du groupe de fonctions AD, et a également sollicité le versement de la somme de 15 000 euros en réparation du préjudice qu'il aurait subi en raison de l'illégalité de son reclassement. La ministre de l'environnement, de l'énergie et de la mer a implicitement refusé de faire droit à ses demandes. Par un jugement du 28 avril 2017, le tribunal administratif de Cergy-Pontoise a rejeté la demande de M. B... tendant à l'annulation de cette décision et à ce que l'Etat soit condamné à réparer le préjudice qu'il allègue avoir subi. M. B... se pourvoit en cassation contre l'arrêt du 8 novembre 2018 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'il avait formé contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes des premier et troisième alinéas de l'article 14 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires, applicable aux fonctionnaires civils des administrations de l'Etat, des régions, des départements, des communes et de leurs établissements publics, dans sa version alors applicable : " L'accès des fonctionnaires de l'Etat, des fonctionnaires territoriaux et des fonctionnaires hospitaliers aux deux autres fonctions publiques, ainsi que leur mobilité au sein de chacune de ces trois fonctions publiques, constituent des garanties fondamentales de leur carrière. / (...) Nonobstant toute disposition contraire prévue dans les statuts particuliers, les agents détachés sont soumis aux mêmes obligations et bénéficient des mêmes droits, notamment à l'avancement et à la promotion, que les membres du corps ou d'emplois dans lequel ils sont détachés ".<br/>
<br/>
              3.  D'autre part, aux termes de l'article 45 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat dans sa rédaction issue de l'article 5 de la loi du 3 août 2009 relative à la mobilité et aux parcours professionnels dans la fonction publique : " Le détachement est la position du fonctionnaire placé hors de son corps d'origine mais continuant à bénéficier, dans ce corps, de ses droits à l'avancement et à la retraite. (...) / A l'expiration de son détachement, le fonctionnaire est, sauf intégration dans le corps ou cadre d'emplois de détachement, réintégré dans son corps d'origine. / Il est tenu compte, lors de sa réintégration, du grade et de l'échelon qu'il a atteints ou auxquels il peut prétendre à la suite de la réussite à un concours ou à un examen professionnel ou de l'inscription sur un tableau d'avancement au titre de la promotion au choix dans le corps ou cadre d'emplois de détachement sous réserve qu'ils lui soient plus favorables(...) ". Selon le premier alinéa de l'article 26-2 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions des fonctionnaires de l'Etat et à certaines modalités de mise à disposition et de cessation définitive de fonctions, inséré par le décret du 7 mai 2010 modifiant le décret du 16 septembre 1985, sous réserve qu'elle lui soit favorable, la réintégration dans son corps d'origine du fonctionnaire de l'Etat détaché auprès d'une administration ou d'un établissement public de l'Etat dans un emploi conduisant à pension du code des pensions civiles et militaires de retraite, auprès d'une collectivité territoriale ou d'un établissement public de santé est prononcée à équivalence de grade et à l'échelon comportant un indice égal ou, à défaut, immédiatement supérieur à celui qu'il détenait dans son grade de détachement. Selon le deuxième alinéa du même article, lorsque le corps d'origine ne dispose pas d'un grade équivalent à celui détenu dans le corps ou cadre d'emplois de détachement, ce fonctionnaire est classé dans le grade dont l'indice sommital est le plus proche de l'indice sommital du grade de détachement et à l'échelon comportant un indice égal ou, à défaut, immédiatement supérieur à celui qu'il détenait dans son grade de détachement. Cette disposition ne s'applique pas aux fonctionnaires de l'Etat détachés pour remplir une mission d'intérêt public à l'étranger ou auprès d'une organisation internationale intergouvernementale.<br/>
<br/>
              4. Il ressort des travaux parlementaires préalables à l'adoption de la loi du 3 août 2009 que le législateur, en adoptant l'article 5 de cette loi dont sont issues les dispositions de l'article 45 de la loi du 11 janvier 1984 citées au point précédent, a entendu favoriser la mobilité des fonctionnaires par la voie du détachement au sein de la fonction publique de l'Etat, de la fonction publique territoriale et de la fonction publique hospitalière et entre ces fonctions publiques, en prévoyant la prise en compte, dans le corps ou cadre d'emplois d'origine du fonctionnaire détaché, des avancements d'échelon et de grade dont il a bénéficié dans le corps ou cadre d'emplois de détachement et qu'il a entendu réserver le bénéfice de ce dispositif aux cas où le détachement intervient dans l'une des trois fonctions publiques auxquelles s'applique la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires. <br/>
<br/>
              5. En premier lieu, il résulte des énonciations de l'arrêt attaqué que la cour administrative d'appel, se fondant sur les dispositions de l'article 45 de la loi du 11 janvier 1984, a relevé que le législateur avait entendu, par ces dispositions telles qu'éclairées par les travaux préparatoires de la loi du 3 août 2009 dont elles sont issues, limiter la prise en compte du grade et de l'échelon obtenus par un fonctionnaire de l'Etat lors d'un détachement dans un autre corps ou cadre d'emplois, pour son reclassement dans son corps d'origine au terme de son détachement, au fonctionnaire ayant été détaché auprès d'une administration ou d'un établissement public de l'Etat dans un emploi conduisant à pension du code des pensions civiles et militaires de retraite, auprès d'une collectivité territoriale ou d'un établissement public de santé. Il résulte de ce qui a été dit précédemment que la cour a pu en déduire, par une motivation suffisante et sans erreur de droit au regard des textes applicables, et sans méconnaître le principe d'égalité entre fonctionnaires appartenant à un même corps non plus, en tout état de cause, que le principe d'égalité de traitement entre travailleurs garanti par les articles 45 et 157 du traité sur le fonctionnement de l'Union européenne, que les dispositions de l'article 45 de la loi du 11 janvier 1984 étaient sans portée sur les conditions de réintégration de M. B... dans son corps d'origine à l'issue de sa période de détachement au sein de la fonction publique de l'Union européenne.<br/>
<br/>
              6. En second lieu, en s'abstenant de saisir la Cour de justice de l'Union européenne d'une question préjudicielle aux fins de dire si les dispositions de l'article 5 de la loi du 3 août 2009 et le refus de l'administration de procéder au reclassement litigieux étaient conformes au droit de l'Union, la cour administrative d'appel n'a fait qu'exercer la faculté qui lui est reconnue par les stipulations de l'article 267 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne à titre préjudiciel, que le pourvoi de M. B... doit être rejeté, y compris en ce qu'il comporte des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ministre de la transition écologique.<br/>
Copie en sera adressée à la ministre de la transformation et de la fonction publiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
