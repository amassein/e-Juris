<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911910</ID>
<ANCIEN_ID>JG_L_2017_10_000000404998</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/19/CETATEXT000035911910.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 25/10/2017, 404998</TITRE>
<DATE_DEC>2017-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404998</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404998.20171025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Bordeaux de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à lui verser la somme de 145 712,30 euros en réparation des préjudices subis du fait de sa contamination transfusionnelle par le virus de l'hépatite C. Par un jugement n° 1302067 du 3 mars 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15BX01202 du 1er juin 2016, la cour administrative d'appel de Bordeaux a, sur appel de MmeA..., annulé ce jugement et rejeté la demande de Mme A... et les conclusions de la caisse primaire d'assurance maladie du Lot-et-Garonne.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 novembre 2016 et 14 février 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'ONIAM la somme de 4 000 euros, à verser à son conseil au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de Mme A...et à la SCP Sevaux, Mathonnet, avocat de l'ONIAM.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a subi, le 25 février 1985, une intervention à l'occasion de laquelle des produits sanguins lui ont été transfusés ; que des examens pratiqués en 1990 ont révélé sa contamination par le virus de l'hépatite C ; qu'imputant cette contamination aux transfusions subies en 1985, Mme A...a, le 7 septembre 2010, demandé à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) de lui verser une indemnité réparant ses préjudices ; que, par une décision du 11 avril 2013, le directeur de l'Office, après avoir diligenté une expertise, a rejeté sa demande, estimant la créance prescrite ; que Mme A...a demandé au tribunal administratif de Bordeaux de condamner l'ONIAM à lui verser la somme de 145 712,30 euros ; que, par un jugement du 3 mars 2015, le tribunal a rejeté sa demande en jugeant que la créance était prescrite en application de l'article 1er de la loi du 31 décembre 1968 ; que, par un arrêt du 1er juin 2016, la cour administrative d'appel de Bordeaux, saisie par MmeA..., a, d'une part, annulé ce jugement pour irrégularité et, d'autre part, évoqué et rejeté la demande de première instance ; que, tout en relevant que la prescription décennale, prévue par l'article L. 1142-28 du code de la santé publique, trouvait en l'espèce à s'appliquer, la cour a estimé que la date de consolidation du dommage devait être fixée au 11 mai 1995, date à laquelle l'état de santé de l'intéressée s'était stabilisé, et que la créance de Mme A...était dès lors prescrite à la date du 7 septembre 2010 à laquelle elle avait saisi l'ONIAM d'une réclamation préalable ; que Mme A...se pourvoit en cassation contre cet arrêt en tant que, par l'article 2 de son dispositif, il rejette sa demande de première instance ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1142-28 du code de la santé publique : " Les actions tendant à mettre en cause la responsabilité des professionnels de santé ou des établissements de santé publics ou privés à l'occasion d'actes de prévention, de diagnostic ou de soins se prescrivent par dix ans à compter de la consolidation du dommage " ;<br/>
<br/>
              3. Considérant qu'en jugeant que la consolidation de l'état de santé de Mme A... avait été acquise le 11 mai 1995, date à laquelle ses troubles s'étaient stabilisés à la suite d'un traitement, alors qu'il ressortait des pièces du dossier qui lui était soumis qu'à cette date, l'intéressée était encore porteuse du virus de l'hépatite C et demeurait par suite atteinte d'une pathologie évolutive et que c'est seulement en 2002 que la disparition du virus avait été constatée, la cour administrative d'appel a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'article 2 de son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant que Mme A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Thouin-Palat et Boucard renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'ONIAM le versement à cette SCP de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Bordeaux du 1er juin 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux dans cette mesure.<br/>
Article 3 : L'ONIAM versera à la SCP Thouin-Palat et Boucard, sous réserve que la SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat, une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. - DEMANDE D'INDEMNISATION ADRESSÉE À L'ONIAM - POINT DE DÉPART DU DÉLAI DE PRESCRIPTION DÉCENNALE - DATE DE LA CONSOLIDATION DU DOMMAGE - APPRÉCIATION DANS L'HYPOTHÈSE D'UNE PATHOLOGIE ÉVOLUTIVE.
</SCT>
<ANA ID="9A"> 60-02-01 Prescription des demandes d'indemnisation formées devant l'Office national d'indemnisation des accidents médicaux (ONIAM) par dix ans à compter de la consolidation du dommage (art. L. 1142-28 du code de la santé publique).,,,La date de consolidation du dommage n'est pas, lorsqu'est en cause une pathologie évolutive, la date de la stabilisation des troubles.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
