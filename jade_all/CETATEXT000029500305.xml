<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029500305</ID>
<ANCIEN_ID>JG_L_2014_09_000000370582</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/50/03/CETATEXT000029500305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 25/09/2014, 370582, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370582</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:370582.20140925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2003 et 2004 et des pénalités correspondantes. Par un jugement n° 0804434 du 30 mai 2012, le tribunal administratif de Grenoble a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 12LY02079 du 28 mai 2013, la cour administrative d'appel de Lyon a, d'une part, déchargé M. et Mme B...des droits et pénalités correspondant à une réduction de leur base d'imposition à hauteur de la somme de 65 113 euros au titre de l'année 2003 et de la somme de 68 215 euros au titre de l'année 2004 et, d'autre part, rejeté le surplus des conclusions de leur requête.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, enregistré le 26 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt n° 12LY02079 du 28 mai 2013, par lesquels la cour administrative d'appel de Lyon a prononcé la décharge partielle des impositions litigieuses ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et MmeB....<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité portant sur la période du 1er octobre 2002 au 30 septembre 2004, l'administration, se fondant notamment sur l'original des factures obtenues dans l'exercice de son droit de communication et adressées par M.B..., qui exerce à titre individuel une activité d'artisan, à deux sociétés clientes, a rejeté la comptabilité de ce dernier et reconstitué ses recettes. Les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles M. et Mme B...ont, par suite, été assujettis au titre des années 2003 et 2004 ont été majorées des pénalités pour manoeuvres frauduleuses. Le ministre délégué, chargé du budget, se pourvoit en cassation contre l'arrêt du 28 mai 2013 par lequel la cour administrative d'appel de Lyon les a partiellement déchargés de ces cotisations supplémentaires et a réformé le jugement du tribunal administratif de Grenoble du 30 mai 2012 en ce qu'il avait de contraire à son arrêt.<br/>
<br/>
              2. Si, eu égard aux garanties dont le livre des procédures fiscales entoure la mise en oeuvre d'une vérification de comptabilité, l'administration est tenue, lorsque, faisant usage de son droit de communication, elle consulte au cours d'une vérification tout ou partie de la comptabilité tenue par l'entreprise vérifiée mais se trouvant chez un tiers, de soumettre l'examen des pièces obtenues à un débat oral et contradictoire avec le contribuable, il n'en est pas de même lorsque lui sont communiqués des documents ne présentant pas le caractère de pièces comptables de l'entreprise vérifiée. Si les doubles des factures originales établies par une entreprise à l'intention de ses clients justifient ses écritures comptables et présentent ainsi le caractère de pièces comptables de l'entreprise qui les a émises, tel n'est pas le cas en revanche des factures originales elles-mêmes, qui n'ont, le cas échéant, le caractère de pièces comptables que pour les clients de cette entreprise.<br/>
<br/>
              3. Pour juger irrégulière la procédure d'imposition suivie à l'égard de M. et Mme B... relative aux cotisations supplémentaires d'impôt sur le revenu et de contributions sociales découlant de l'examen des factures mentionnées au point 1, la cour administrative d'appel de Lyon, après avoir rappelé que l'administration avait rejeté la comptabilité de M. B... et procédé à la reconstitution de ses recettes, a regardé comme des pièces comptables du contribuable vérifié se trouvant chez un tiers ou détenues par l'autorité judiciaire ces factures originales, que M. B...avait établies à l'intention de deux entreprises clientes et qui avaient été obtenues par l'administration dans le cadre de l'exercice de son droit de communication, d'une part, auprès de l'une d'entre elles et, d'autre part, auprès de l'autorité judiciaire. Il résulte de ce qui a été dit ci-dessus qu'elle a, sur ce point, entaché son arrêt d'une erreur de droit. Le ministre est, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, fondé à demander l'annulation des articles 1er et 2 de l'arrêt qu'il attaque.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt du 28 mai 2013 de la cour administrative d'appel de Lyon sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions présentées par M. et Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
