<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375605</ID>
<ANCIEN_ID>JG_L_2020_09_000000423129</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375605.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 28/09/2020, 423129</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423129</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:423129.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... E..., Mme C... E... épouse B..., M. D... F... et Mme G... F... ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir la délibération du 28 mars 2013 par laquelle le conseil municipal du Lavandou a approuvé le plan local d'urbanisme de la commune, en tant que ce dernier crée certains emplacements réservés, classe certaines parcelles ou certaines zones et comporte certaines dispositions. Par un jugement n° 1301313 du 25 juillet 2016, le tribunal administratif de Toulon a annulé cette délibération en tant seulement qu'elle interdit le changement de destination des hôtels existants dans les zones urbaines et qu'elle prévoit des emplacements réservés n° 43 et n° 44 sur l'unité foncière appartenant aux consorts E..., et rejeté le surplus de la demande.<br/>
<br/>
              Par un arrêt n° 16MA03756, 16MA03791 du 12 juin 2018, la cour administrative d'appel de Marseille a, sur appels de M. E... et autres et de la commune du Lavandou, annulé le jugement du tribunal administratif de Toulon en tant qu'il prévoit la création des emplacements réservés n° 43 et n° 44 et rejeté la demande présentée devant le tribunal administratif par les consorts E....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 août et 14 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. E... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Lavandou la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. E... et autres et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune du Lavandou ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 28 mars 2013, le conseil municipal de la commune du Lavandou a approuvé le plan local d'urbanisme (PLU) de la commune. M. et Mme E... et M. et Mme F..., propriétaires de parcelles situées à Pramousquier, grevées par la création de deux emplacements réservés, ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir cette délibération en tant que celle-ci crée certains emplacements réservés, classe certaines parcelles ou certaines zones et comporte certaines dispositions. Par un jugement du 25 juillet 2016, le tribunal administratif de Toulon a annulé cette délibération en tant qu'elle interdit le changement de destination des hôtels existants dans les zones urbaines et qu'elle prévoit des emplacements réservés n° 43 et n° 44 sur l'unité foncière appartenant aux consorts E.... Toutefois, saisie en appel à la fois par les consorts E... et par la commune du Lavandou, la cour administrative d'appel a, par un arrêt du 12 juin 2018 contre lequel M. E... et autres se pourvoient en cassation, annulé ce jugement en tant qu'il avait annulé la délibération du 28 mars 2013 en tant qu'elle prévoit la création des emplacements réservés n° 43 et n° 44.<br/>
<br/>
              2. En premier lieu, d'une part, aux termes de l'article R. 611-1 du code de justice administrative, dans sa version applicable : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". En outre, si l'article R. 431-1 du code de justice administrative, dont les dispositions sont applicables devant la cour administrative d'appel en vertu de l'article R. 431-13 du même code, prévoit que, " lorsqu'une partie est représentée devant le tribunal administratif par un des mandataires mentionnés à l'article R. 431-2, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire ", ces dispositions ne sont  applicables qu'aux procédures dans lesquelles le mandataire en cause a été désigné.<br/>
<br/>
              3. D'autre part, s'il appartient au juge d'apprécier l'opportunité de joindre des requêtes, il ne peut, sans méconnaître le caractère contradictoire de la procédure, fonder sa décision, à l'égard d'une partie, sur des éléments qui n'auraient pas été communiqués à cette dernière.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que M. E... et autres, d'une part, et la commune du Lavandou, d'autre part, ont chacun interjeté appel du jugement du tribunal administratif de Toulon annulant partiellement le PLU litigieux. La cour a joint ces deux requêtes pour statuer par une seule décision. Il ressort de ces mêmes pièces que la requête d'appel de la commune du Lavandou a été régulièrement communiquée à M. et Mme E... ainsi qu'à M. et Mme F.... Par suite, ces derniers ne sont pas fondés à soutenir que la cour aurait méconnu le caractère contradictoire de la procédure en ne communiquant pas cette requête à l'avocat qu'ils avaient mandaté pour les représenter dans la procédure d'appel qu'ils avaient eux-mêmes diligentée devant la cour contre le même jugement, aucune disposition ni aucun principe ne prévoyant une telle communication, laquelle ne saurait relever, le cas échéant, que de l'initiative des intéressés. Il s'en suit que le moyen tiré de l'irrégularité de l'arrêt attaqué ne peut qu'être écarté.<br/>
<br/>
              5. En deuxième lieu, il ne ressort pas de la teneur des écritures d'appel des requérants que la cour en aurait méconnu la portée en jugeant que leurs conclusions tendant à l'annulation de la délibération en litige approuvant le PLU en tant qu'il classe en secteur 1Ne une partie de leur parcelle AK n° 161, qu'il instaure sur l'ensemble du territoire communal les emplacements réservés numérotés 1 à 46, qu'il institue des servitudes de mixité sociale et tendant à l'annulation de la servitude d'espace boisé classé grevant les parcelles BB n° 76 et voisines n'étaient pas assorties des moyens permettant au juge d'en apprécier le bien-fondé. <br/>
<br/>
              6. En troisième lieu, aux termes de l'article L. 300-2 du code de l'urbanisme, dans sa version applicable au litige : " I - Le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale délibère sur les objectifs poursuivis et sur les modalités d'une concertation associant, pendant toute la durée de l'élaboration du projet, les habitants, les associations locales et les autres personnes concernées dont les représentants de la profession agricole, avant : a) Toute élaboration ou révision du schéma de cohérence territoriale ou du plan local d'urbanisme ; (...). Les documents d'urbanisme et les opérations mentionnées aux a, b et c ne sont pas illégaux du seul fait des vices susceptibles d'entacher la concertation, dès lors que les modalités définies par la délibération prévue au premier alinéa ont été respectées (...) ". Il résulte de ces dispositions que l'adoption ou la révision du plan local d'urbanisme doit être précédée d'une concertation associant les habitants, les associations locales et les autres personnes concernées. Le conseil municipal doit, avant que ne soit engagée la concertation, délibérer, d'une part, et au moins dans leurs grandes lignes, sur les objectifs poursuivis par la commune en projetant d'élaborer ou de réviser ce document d'urbanisme, et, d'autre part, sur les modalités de la concertation. Si cette délibération est susceptible de recours devant le juge de l'excès de pouvoir, son illégalité ne peut, en revanche, eu égard à son objet et à sa portée, être utilement invoquée contre la délibération approuvant le plan local d'urbanisme. Ainsi que le prévoit l'article L. 300-2 du code de l'urbanisme précité, les irrégularités ayant affecté le déroulement de la concertation au regard des modalités définies par la délibération prescrivant la révision du document d'urbanisme demeurent par ailleurs invocables à l'occasion d'un recours contre le plan local d'urbanisme approuvé.<br/>
<br/>
              7. Par leur jugement du 25 juillet 2016 dont la cour administrative d'appel s'est appropriée, sur ce point, les motifs, les premiers juges ont jugé, après avoir relevé que l'élaboration du PLU litigieux avait notamment été précédée de plusieurs réunions publiques, d'une exposition permanente faisant état de l'avancée des études en cours et de la mise à la disposition du public d'un registre destiné à recueillir des observations, que les modalités de la concertation étaient conformes aux prescriptions de la délibération du 13 mai 2009 décidant de l'élaboration du PLU litigieux. La cour, après avoir relevé que le moyen tiré de ce que les modalités de la concertation n'avaient pas été respectées ne comportait aucun élément de fait ou de droit nouveau par rapport à l'argumentation développée devant le tribunal administratif, a, par une motivation suffisante et sans dénaturer les faits dont elle était saisie, pu adopter les motifs retenus par celui-ci sans commettre d'erreur de droit au regard des principes rappelés au point précédent. <br/>
<br/>
              8. En quatrième lieu, d'une part, l'article L. 111-1-1 du code de l'urbanisme, dans sa version alors applicable, prévoit, d'une part, que les plans locaux d'urbanisme doivent être compatibles avec les SCOT et les schémas de secteur. En l'absence de SCOT, ils doivent notamment être compatibles, s'il y a lieu, avec les dispositions particulières aux zones de montagne et au littoral prévues aux articles L. 145-1 à L. 146-6. Ce même article prévoit, d'autre part, que les SCOT et les schémas de secteur doivent être compatibles, s'il y a lieu, avec ces mêmes dispositions. <br/>
<br/>
              9. S'il appartient à l'autorité administrative chargée de se prononcer sur une demande d'autorisation d'occupation ou d'utilisation du sol de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de la conformité du projet avec les dispositions du code de l'urbanisme particulières au littoral, il résulte des dispositions citées au point précédent, désormais reprises aux articles L. 131-4 et L. 131-7 du code de l'urbanisme, que, s'agissant d'un plan local d'urbanisme, il appartient à ses auteurs de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de sa compatibilité avec les dispositions du code de l'urbanisme particulières au littoral. Dans le cas où le territoire concerné est couvert par un SCOT, cette compatibilité s'apprécie en tenant compte des dispositions de ce document relatives à l'application des dispositions du code de l'urbanisme particulières au littoral, sans pouvoir en exclure certaines au motif qu'elles seraient insuffisamment précises, sous la seule réserve de leur propre compatibilité avec ces dernières.<br/>
<br/>
              10. D'autre part, aux termes du premier alinéa de l'article L. 146-6 du code de l'urbanisme, dans sa version alors applicable : " Les documents et décisions relatifs à la vocation des zones ou à l'occupation et à l'utilisation des sols préservent les espaces terrestres et marins, sites et paysages remarquables ou caractéristiques du patrimoine naturel et culturel du littoral, et les milieux nécessaires au maintien des équilibres biologiques. Un décret fixe la liste des espaces et milieux à préserver, comportant notamment, en fonction de l'intérêt écologique qu'ils présentent, les dunes et les landes côtières, les plages et lidos, les forêts et zones boisées côtières, les îlots inhabités, les parties naturelles des estuaires, des rias ou abers et des caps, les marais, les vasières, les zones humides et milieux temporairement immergés ainsi que les zones de repos, de nidification et de gagnage de l'avifaune désignée par la directive européenne n° 79-409 du 2 avril 1979 concernant la conservation des oiseaux sauvages et, dans les départements d'outre-mer, les récifs coralliens, les lagons et les mangroves ". L'article R. 146-1 du même code, dans sa version alors applicable, définit une liste d'espaces à préserver en application de ces dispositions.<br/>
<br/>
              11. Enfin, aux termes du II de l'article L. 122-1-5 du code de l'urbanisme, dans sa version alors applicable, le document d'orientations et d'objectifs du SCOT " détermine les espaces et sites naturels, agricoles, forestiers ou urbains à protéger. Il peut en définir la localisation ou la délimitation ".<br/>
<br/>
              12. Après avoir relevé que les éléments figurant au document d'orientations générales du SCOT Provence-Méditerranée relatifs aux espaces remarquables caractéristiques du littoral étaient insuffisamment précis faute notamment de mentionner les différentes catégories d'espaces remarquables énumérées par l'article R. 146-1 du code de l'urbanisme, c'est à tort, au vu des principes énoncés aux points 8 et 9, que la cour en a déduit que la compatibilité du plan local d'urbanisme devait être appréciée sans tenir compte du SCOT.<br/>
<br/>
              13. Toutefois, il résulte des termes mêmes de l'arrêt attaqué que, pour se prononcer sur le moyen tiré de ce que le PLU litigieux, en classant des terrains sur le fondement des articles L. 146-6 et R. 146-1 du code de l'urbanisme précités, alors qu'ils ne sont pas identifiés comme des espaces remarquables par le SCOT, serait incompatible avec ce dernier, la cour a jugé qu'il appartenait aux auteurs d'un plan local d'urbanisme de classer en espace remarquable les espaces correspondant aux caractéristiques prévues à ces articles lorsque les conditions de leur application sont réunies. En statuant ainsi, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              14. En cinquième lieu, aux termes du dernier alinéa de l'article L. 146-6 du code de l'urbanisme, dans sa version alors applicable : " Le plan local d'urbanisme doit classer en espaces boisés, au titre de l'article L. 130-1 du présent code, les parcs et ensembles boisés existants les plus significatifs de la commune ou du groupement de communes, après consultation de la commission départementale compétente en matière de nature, de paysages et de sites ". Contrairement à ce qui est soutenu, ces dispositions ne réservent pas aux seuls espaces boisés d'arbres de haute tige le bénéfice du classement qu'elles prévoient, mais peuvent s'appliquer à des espaces accueillant tout type de végétation.<br/>
<br/>
              15. Par l'arrêt attaqué, la cour administrative d'appel a jugé que la circonstance qu'une grande partie du territoire communal soit couverte par une garrigue ou un maquis ras et que 75% du territoire communal soit classé en espace boisé classé n'était pas de nature à établir l'illégalité du classement opéré par le PLU litigieux sur le fondement des dispositions citées au point précédent. En statuant ainsi, la cour n'a ni commis d'erreur de droit, au regard des principes rappelés ci-dessus, ni dénaturé les pièces du dossier qui lui étaient soumis.<br/>
<br/>
              16. En sixième lieu, comme indiqué précédemment, l'article R. 146-1 du code de l'urbanisme, dans sa version applicable, définit une liste d'espaces à préserver en application du premier alinéa de l'article L. 146-6 du code de l'urbanisme précité, dès lors qu'ils constituent un site ou un paysage remarquable ou caractéristique du patrimoine naturel et culturel du littoral, sont nécessaires au maintien des équilibres biologiques ou présentent un intérêt écologique, parmi lesquels figurent : " b) les forêts et zones boisées proches du rivage de la mer et des plans d'eau intérieurs d'une superficie supérieure à 1 000 hectares (...) ". Ces dispositions instaurent un régime de protection distinct de celui prévu au dernier alinéa de l'article L. 146-6 du code de l'urbanisme précité, ces deux régimes pouvant, le cas échéant, se compléter. <br/>
<br/>
              17. Par l'arrêt attaqué, la cour a relevé que le rapport de présentation du PLU identifiait clairement, d'une part, les " ensembles boisés les plus significatifs " au sens du dernier alinéa de l'article L. 146-6 du code de l'urbanisme, et, d'autre part, les espaces remarquables du territoire communal au sens du premier alinéa du même article, et notamment les forêts et zones boisées au sens du b) de l'article R. 146-1 du code de l'urbanisme précité, classés en zone 1Nr, pour écarter le moyen tiré de ce que les auteurs du PLU auraient classé des secteurs en " boisements les plus significatifs " existants au seul motif erroné qu'ils constitueraient des espaces remarquables au titre de la loi littoral. Il en résulte que le moyen tiré de ce que la cour aurait commis une erreur de droit en refusant de relever la confusion opérée par les auteurs du PLU litigieux entre ces deux régimes de protection distincts manque en fait.<br/>
<br/>
              18. En septième lieu, l'article L. 123-1-5 du code de l'urbanisme, dans sa version applicable, dispose : " Le règlement fixe, en cohérence avec le projet d'aménagement et de développement durables, les règles générales et les servitudes d'utilisation des sols permettant d'atteindre les objectifs mentionnés à l'article L. 121-1, qui peuvent notamment comporter l'interdiction de construire, délimitent les zones urbaines ou à urbaniser et les zones naturelles ou agricoles et forestières à protéger et définissent, en fonction des circonstances locales, les règles concernant l'implantation des constructions. / A ce titre, le règlement peut : / (...) 8° Fixer les emplacements réservés aux voies et ouvrages publics, aux installations d'intérêt général ainsi qu'aux espaces verts ". Ces dispositions ont pour objet de permettre aux auteurs d'un document d'urbanisme de réserver certains emplacements à des voies et ouvrages publics, à des installations d'intérêt général ou à des espaces verts, le propriétaire concerné bénéficiant, en contrepartie de cette servitude, d'un droit de délaissement lui permettant d'exiger de la collectivité publique au bénéfice de laquelle le terrain a été réservé qu'elle procède à son acquisition, faute de quoi les limitations au droit à construire et la réserve ne sont plus opposables.<br/>
<br/>
              19. Par l'arrêt attaqué, la cour a annulé le jugement du tribunal administratif en tant que celui-ci avait jugé que la création des emplacements réservés n° 43 et n° 44 était entachée d'erreur manifeste d'appréciation, après avoir jugé, par une appréciation souveraine exempte de dénaturation, que ces emplacements, destinés respectivement à la création d'un chemin piétonnier d'accès à la plage à partir de la route du littoral et, d'autre part, à la création d'un parc public de stationnement et d'un poste de relevage des eaux, qui avaient reçu un avis favorable du commissaire enquêteur, ne présentaient pas de contradiction l'un avec l'autre et se justifiaient par la volonté des auteurs du PLU litigieux de favoriser les modes de circulation doux, de créer un parking public dans le quartier de Pramousquier, d'assurer la sécurité des piétons et de moderniser les structures de gestion de l'assainissement communal. En statuant ainsi, la cour n'a pas commis d'erreur de droit. Par ailleurs, si les requérants lui reprochent en outre de ne pas avoir tenu compte de leur moyen, développé devant les premiers juges, tiré de ce que la création de ces deux emplacements réservés caractériserait un détournement de pouvoir, il ressort des pièces du dossier que cet argument, au demeurant non assorti de précisions, n'était invoqué qu'au soutien du moyen tiré de ce que la création de ces emplacements réservés serait entachée d'erreur manifeste d'appréciation, auquel la cour a répondu. Par suite, les moyens de dénaturation des pièces du dossier, d'erreur de droit et d'insuffisance de motivation doivent être écartés.<br/>
<br/>
              20. Il résulte de tout ce qui précède que M. E... et autres ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. Par suite, leur pourvoi doit être rejeté, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              21. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. E... et autres une somme au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. E... et autres est rejeté.<br/>
Article 2 : Les conclusions présentées par la commune du Lavandou au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A... E..., premier dénommé pour l'ensemble des requérants, et à la commune du Lavandou.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-03 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - ACTES DE PROCÉDURE EFFECTUÉS À L'ÉGARD DU MANDATAIRE DÉSIGNÉ PAR LES PARTIES (ART. R. 431-1 DU CJA) - 1) CHAMP D'APPLICATION - PROCÉDURES DANS LESQUELLES CE MANDATAIRE A ÉTÉ DÉSIGNÉ [RJ1] - 2) ILLUSTRATION.
</SCT>
<ANA ID="9A"> 54-04-03 1) L'article R. 431-1 du code de justice administrative (CJA), qui prévoit que lorsqu'une partie est représentée par un des mandataires mentionnés à l'article R. 431-2, les actes de procédure, à l'exception de la notification de la décision, ne sont accomplis qu'à l'égard de ce mandataire, n'est applicables qu'aux procédures dans lesquelles le mandataire en cause a été désigné.,,,2) Requérants ayant interjeté appel d'un jugement et mandaté un avocat pour les représenter dans cette procédure. Défendeur en première instance ayant également interjeté appel du même jugement. Cour ayant communiqué cette requête aux requérants, en qualité de défendeurs en appel, et non à leur mandataire.... ,,Le caractère contradictoire n'est pas méconnu par l'absence de communication de cette requête à l'avocat mandaté pour les représenter dans la procédure d'appel qu'ils avaient eux-mêmes diligentée, aucune disposition ni aucun principe ne prévoyant une telle communication, laquelle ne saurait relever, le cas échéant, que de l'initiative des intéressés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de la communication au mandataire désigné pour l'instance ayant conduit au prononcé d'une astreinte y compris pour l'instruction de la demande de liquidation de celle-ci, CE, 24 février 2017, M.,, n° 401656, p. 753.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
