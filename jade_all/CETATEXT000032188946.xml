<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032188946</ID>
<ANCIEN_ID>JG_L_2016_03_000000375818</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/89/CETATEXT000032188946.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 09/03/2016, 375818</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375818</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:375818.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Caisse régionale du Crédit agricole mutuel (CRCAM) de Normandie a demandé au tribunal administratif de Caen de prononcer la décharge de l'amende qui lui a été infligée, sur le fondement de l'article 1739 du code général des impôts, à raison d'opérations effectuées au cours des années 2005 à 2007. Par un jugement n°1101006 du 3 juillet 2012, le tribunal administratif de Caen a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12NT02481 du 26 décembre 2013, la cour administrative d'appel de Nantes a rejeté l'appel formé par la Caisse régionale du Crédit agricole mutuel de Normandie contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 février 2014, 22 mai 2014 et 18 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la Caisse régionale du Crédit agricole mutuel de Normandie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code monétaire et financier ;<br/>
              - le décret n° 2007-996 du 31 mai 2007 relatif aux attributions du ministre de l'économie, des finances et de l'emploi ;<br/>
              - le décret n° 2007-1003 du 31 mai 2007 relatif aux attributions du ministre du budget, des comptes publics et de la fonction publique ;<br/>
              - le décret n° 2008-310 du 3 avril 2008 relatif à la direction générale des finances publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Caisse régionale du Crédit agricole mutuel de Normandie ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la Caisse régionale du Crédit agricole mutuel (CRCAM) de Normandie, établissement de crédit agréé, a fait l'objet d'une vérification de comptabilité portant sur la période du 1er janvier 2005 au 31 décembre 2007, à l'issue de laquelle l'administration lui a infligé une amende de 332 180 euros en application de l'article 1739 du code général des impôts ; que la caisse a sollicité la décharge de cette amende devant le tribunal administratif de Caen qui, par un jugement du 3 juillet 2012, a rejeté sa demande ; qu'elle se pourvoit en cassation contre l'arrêt du 26 décembre 2013 par lequel la cour administrative d'appel de Nantes a confirmé ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article 1739 du code général des impôts dans sa rédaction alors applicable, dont les dispositions reprennent celles qui figuraient, antérieurement au 1er janvier 2006, à l'article 1756 bis du même code : " I. - Nonobstant toutes dispositions contraires, il est interdit à tout établissement de crédit qui reçoit du public des fonds à vue ou à moins de cinq ans, et par quelque moyen que ce soit, d'ouvrir ou de maintenir ouverts dans des conditions irrégulières des comptes bénéficiant d'une aide publique, notamment sous forme d'exonération fiscale, ou d'accepter sur ces comptes des sommes excédant les plafonds autorisés. / Sans préjudice des sanctions disciplinaires qui peuvent être infligées par la commission bancaire, les infractions aux dispositions du présent article sont punies d'une amende fiscale dont le taux est égal au montant des intérêts payés, sans que cette amende puisse être inférieure à 75 euros. (...) " ; que ces dispositions sont reprises, dans des termes analogues, à l'article L. 221-35 du code monétaire et financier ; qu'aux termes de l'article L. 221-36 de ce code, dans sa rédaction applicable à la procédure ayant conduit à l'infliction de l'amende en litige : "Les infractions aux dispositions de l'article L. 221-35 sont constatées comme en matière de timbre : /- par les comptables du Trésor ; /- par les agents des administrations financières./ Les procès-verbaux sont dressés à la requête du ministre chargé de l'économie. " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que l'amende prévue par l'article 1739 du code général des impôts ne peut être infligée que sur le fondement d'un procès-verbal dressé sous l'autorité du ministre chargé de l'économie ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le procès-verbal d'infraction du 26 mai 2009 à la suite duquel la Caisse régionale du Crédit agricole mutuel de Normandie a été soumise à l'amende prévue par l'article 1739 précité du code général des impôts a été établi " à la requête du directeur général des finances publiques " par un agent de la direction des vérifications nationales et internationales ; qu'il résulte  des décrets du 31 mai 2007 relatifs aux attributions du ministre de l'économie, des finances et de l'emploi et du ministre du budget, des comptes publics et de la fonction publique, qu'à la date du 26 mai 2009, la direction générale des finances publiques était placée pour l'ensemble de ses attributions, à la seule exception de celles concernant la législation fiscale, sous l'autorité du ministre du budget, des comptes publics et de la fonction publique et non sous celle du ministre de l'économie, des finances et de l'emploi ;<br/>
<br/>
              5. Considérant que, pour juger que le procès-verbal d'infraction du 26 mai 2009 n'avait pas été dressé par une autorité incompétente, la cour, d'une part, s'est fondée sur la circonstance que l'article 2 de l'arrêté interministériel du 24 juillet 2000 relatif à la direction des vérifications nationales et internationales avait été signé tant du secrétaire d'Etat au budget que du ministre de l'économie, des finances et de l'industrie, d'autre part, a jugé que la disposition selon laquelle les procès-verbaux étaient dressés à la requête du ministre chargé de l'économie avait pour unique objet d'investir ce ministre du pouvoir de statuer sur les demandes formées à l'effet d'obtenir remise gracieuse des amendes encourues ; qu'elle s'est, ainsi, fondée sur une circonstance inopérante et a méconnu la portée, exposée au point 3,  du texte qu'elle appliquait, entachant ainsi son arrêt d'une erreur de droit ;  <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la CRCAM de Normandie est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui a été dit au point 4 ci-dessus que le procès-verbal d'infraction du 26 mai 2009 ne peut être regardé comme ayant été dressé " à la requête du ministre chargé de l'économie " comme l'exigent les dispositions précitées de l'article L. 221-36 du code monétaire et financier ; que la méconnaissance de cette obligation, qui constitue une garantie pour la personne sanctionnée, entache la procédure ayant conduit au prononcé de l'amende litigieuse d'une irrégularité de nature à justifier sa décharge ; que, par suite, la CRCAM de Normandie est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Caen a rejeté sa demande ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, la somme de 5 000 euros à verser à la CRCAM de Normandie, au titre de l'article L. 761-1 du code de justice administrative, pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 26 décembre 2013 et le jugement du tribunal administratif de Caen du 3 juillet 2012 sont annulés.<br/>
Article 2 : La CRCAM de Normandie est déchargée de l'amende qui lui a été infligée, sur le fondement de l'article 1739 du code général des impôts, à raison d'opérations effectuées au cours des années 2005 à 2007.<br/>
Article 3 : L'Etat versera une somme de 5 000 euros à la Caisse régionale du Crédit agricole mutuel de Normandie au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Caisse régionale du Crédit agricole mutuel de Normandie et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-04 CAPITAUX, MONNAIE, BANQUES. BANQUES. - MANQUEMENT À LA RÉGLEMENTATION SUR L'ÉPARGNE RÉGLEMENTÉE - AMENDE PRÉVUE PAR LE I DE L'ARTICLE 1739 DU CGI - NÉCESSITÉ D'UN PROCÈS-VERBAL DRESSÉ SOUS L'AUTORITÉ DU MINISTRE CHARGÉ DE L'ÉCONOMIE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. RÉGULARITÉ. - MANQUEMENT À LA RÉGLEMENTATION SUR L'ÉPARGNE RÉGLEMENTÉE - AMENDE PRÉVUE PAR LE I DE L'ARTICLE 1739 DU CGI - NÉCESSITÉ D'UN PROCÈS-VERBAL DRESSÉ SOUS L'AUTORITÉ DU MINISTRE CHARGÉ DE L'ÉCONOMIE - EXISTENCE.
</SCT>
<ANA ID="9A"> 13-04 L'article 1739 du code général des impôts (CGI), dont les dispositions sont reprises à l'article L. 221-35 du code monétaire et financier, prévoit une amende en cas d'ouverture irrégulière par un établissement de crédit d'un compte bénéficiant d'une aide publique. Les dispositions de l'article L. 221-36 du code monétaire et financier prévoient, d'une part, que les infractions sont constatées par les comptables du trésor et les agents des administrations financières, d'autre part, que les procès-verbaux sont dressés à la requête du ministre chargé de l'économie. Il résulte de ces dispositions que cette amende ne peut être infligée que sur le fondement d'un procès-verbal dressé sous l'autorité du ministre chargé de l'économie.</ANA>
<ANA ID="9B"> 59-02-02-02 L'article 1739 du code général des impôts (CGI), dont les dispositions sont reprises à l'article L. 221-35 du code monétaire et financier, prévoit une amende en cas d'ouverture irrégulière par un établissement de crédit d'un compte bénéficiant d'une aide publique. Les dispositions de l'article L. 221-36 du code monétaire et financier prévoient, d'une part, que les infractions sont constatées par les comptables du trésor et les agents des administrations financières, d'autre part, que les procès-verbaux sont dressés à la requête du ministre chargé de l'économie. Il résulte de ces dispositions que cette amende ne peut être infligée que sur le fondement d'un procès-verbal dressé sous l'autorité du ministre chargé de l'économie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
