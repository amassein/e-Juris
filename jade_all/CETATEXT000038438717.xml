<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038438717</ID>
<ANCIEN_ID>JG_L_2019_04_000000424654</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/43/87/CETATEXT000038438717.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/04/2019, 424654, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424654</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:424654.20190430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a fait opposition devant le tribunal administratif de Strasbourg à la contrainte émise le 12 février 2016 par le directeur régional de Pôle emploi Alsace Champagne-Ardenne Lorraine pour le recouvrement d'une somme de 803,59 euros correspondant à un indu d'allocation de solidarité spécifique au titre de la période comprise entre le 6 juin et le 30 juillet 2014 et a demandé une remise de sa dette. Par une ordonnance n° 1602663 du 4 juin 2018, le président de la sixième chambre du tribunal administratif de Strasbourg a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés le 2 octobre 2018 et les 2 janvier et 22 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de Pôle emploi ou de l'Etat la somme de 3 000 euros à verser à son avocat, la SCP Richard, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2016-1480 du 2 novembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 431-2 du code de justice administrative, applicable à la représentation des parties devant le tribunal administratif : " Les requêtes et les mémoires doivent, à peine d'irrecevabilité, être présentés soit par un avocat, soit par un avocat au Conseil d'Etat et à la Cour de cassation, lorsque les conclusions de la demande tendent au paiement d'une somme d'argent, à la décharge ou à la réduction de sommes dont le paiement est réclamé au requérant ou à la solution d'un litige né de l'exécution d'un contrat ". Toutefois, l'article R. 431-3 du même code, dans sa rédaction issue du 3° de l'article 11 du décret du 2 novembre 2016 portant modification du code de justice administrative (partie réglementaire) prévoit que ces dispositions ne s'appliquent pas : " 4° Aux litiges en matière de pensions, de prestations, allocations ou droits attribués (...) en faveur des travailleurs privés d'emploi (...) ". En vertu de l'article 35 du décret du 2 novembre 2016, ces dispositions sont entrées en vigueur le 1er janvier 2017.<br/>
<br/>
              2. Si le droit de former un recours contre une décision est définitivement fixé au jour où cette décision est rendue, les règles qui régissent les formes dans lesquelles le recours doit être introduit et jugé ne sont pas, à la différence des voies selon lesquelles ce droit peut être exercé ainsi que des délais qui sont impartis à cet effet aux intéressés, des éléments constitutifs de ce droit. Par suite, les dispositions du 4° de l'article R. 431-3 du code de justice administrative dans leur rédaction issue du décret du 2 novembre 2016 font obstacle, depuis le 1er janvier 2017, à ce que soit opposée aux requêtes relatives à des prestations, allocations ou droits attribués en faveur des travailleurs privés d'emploi, même enregistrées avant cette date, une irrecevabilité tirée de la méconnaissance de l'obligation de ministère d'avocat qui leur était antérieurement applicable.<br/>
<br/>
              3. Il suit de là que le président de la sixième chambre du tribunal administratif de Strasbourg a méconnu ces dispositions en rejetant, par une ordonnance du 4 juin 2018, l'opposition formée par M. B...à la contrainte qui lui avait été signifiée pour le recouvrement d'un indu d'allocation de solidarité spécifique au motif que cette opposition, faute d'avoir été présentée par le ministère d'un avocat, était manifestement irrecevable. M.B..., qui au surplus avait été admis au bénéfice de l'aide juridictionnelle par une décision prise le 1er juillet 2016 par le bureau d'aide juridictionnelle près le tribunal de grande instance de Strasbourg, est fondé à demander, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, l'annulation de cette ordonnance.<br/>
<br/>
              4. M. B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Richard, son avocat, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de Pôle emploi une somme de 1 500 euros à verser à cette SCP.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la sixième chambre du tribunal administratif de Strasbourg du 4 juin 2018 est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Strasbourg.<br/>
Article 3 : Pôle emploi versera à la SCP Richard, avocat de M.B..., une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à Pôle emploi.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
