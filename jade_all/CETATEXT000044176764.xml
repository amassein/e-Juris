<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044176764</ID>
<ANCIEN_ID>JG_L_2021_10_000000438374</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/17/67/CETATEXT000044176764.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 07/10/2021, 438374</TITRE>
<DATE_DEC>2021-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438374</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Catherine Fischer-Hirtz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438374.20211007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 février, 22 mai 2020 et 16 février 2021 au secrétariat du contentieux du Conseil d'Etat, la Société mutualiste Tutélaire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2019-02 du 10 décembre 2019 par laquelle la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution (ACPR) lui a infligé un blâme ainsi qu'une sanction pécuniaire de 500 000 euros et a ordonné la publication de cette décision au registre de l'Autorité ; <br/>
              2°) à titre subsidiaire, de réformer la décision attaquée en réduisant les sanctions prononcées ;<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 2007-1775 du 17 décembre 2007, ensemble la loi n° 2014-617 du 13 juin 2014 ;<br/>
              - le code monétaire et financier ;<br/>
              - le code de la mutualité ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Fischer-Hirtz, conseillère d'Etat,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de la Société Mutualiste Tutélaire et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction qu'à la suite d'un contrôle diligenté par l'Autorité de contrôle prudentiel et de résolution (ACPR) du 20 juin 2017 au 7 juillet 2017 portant sur la gestion des garanties décès du contrat TUT'LR commercialisé par la société mutualiste Tutélaire (Tutélaire), qui a révélé des manquements s'agissant de la mise en œuvre du dispositif relatif à la gestion des contrats d'assurance vie en déshérence, une procédure disciplinaire a été ouverte à l'encontre de cette société. Par une décision du 10 décembre 2019, la commission des sanctions de cette autorité a prononcé à l'encontre de Tutélaire, un blâme ainsi qu'une sanction pécuniaire de 500 000 euros et ordonné la publication de cette décision sous une forme nominative pendant cinq ans, puis sous une forme anonyme au registre de l'ACPR. Tutélaire demande, à titre principal, l'annulation de cette décision et, à titre subsidiaire, la réduction du montant de la sanction pécuniaire qui lui a été infligée et l'annulation de la sanction complémentaire de publication.<br/>
<br/>
              Sur le bien-fondé de la décision attaquée : <br/>
<br/>
              En ce qui concerne les obligations pesant sur la mutuelle :<br/>
<br/>
              2. D'une part, aux termes de l'article L. 223-10-2 du code de la mutualité : " I. - Les mutuelles et unions ayant pour objet la réalisation d'opérations d'assurance mentionnées au b du 1° du I de l'article L. 111-1 s'informent, au moins chaque année, dans les conditions prévues au II du présent article, du décès éventuel de l'assuré. / II. - Les organismes professionnels mentionnés à l'article L. 223-10-1 consultent chaque année, dans le respect de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, les données figurant au répertoire national d'identification des personnes physiques et relatives au décès des personnes qui y sont inscrites. Les mutuelles et unions mentionnées au I obtiennent de ces organismes professionnels communication de ces données en vue d'effectuer des traitements de données nominatives. Ces traitements ont pour objet la recherche des membres participants et bénéficiaires décédés des contrats d'assurance sur la vie et des bons ou contrats de capitalisation, à l'exception de ceux au porteur. "<br/>
<br/>
              3. D'autre part, aux termes du dernier alinéa de l'article L. 223-10 du code de la mutualité : " Lorsque la mutuelle ou l'union est informée du décès du membre participant, elle est tenue de rechercher le bénéficiaire et, si cette recherche aboutit, de l'aviser de la stipulation effectuée à son profit. "<br/>
<br/>
              4. Il résulte de ces dispositions, introduites dans le code de la mutualité ainsi que, de manière similaire, dans le code des assurances, par la loi du 17 décembre 2007 permettant la recherche des bénéficiaires des contrats d'assurance sur la vie non réclamés et garantissant les droits des assurés, et modifiées par celle du 13 juin 2014 relative aux comptes bancaires inactifs et aux contrats d'assurance vie en déshérence, qu'elles ont pour objet de prévenir la non-exécution des engagements, pris à l'égard des assurés, dont le fait générateur est le décès. A cet effet, les dispositions précitées, mises en œuvre par le dispositif AGIRA 2, visent, sans restriction aucune, les engagements que les assureurs et les mutuelles peuvent avoir pour objet de réaliser en vertu du b du 1° du I de l'article L. 111-1 du code de la mutualité, parmi lesquels figurent ceux " dont l'exécution dépend de la durée de la vie humaine ".  <br/>
<br/>
              5. Il résulte de l'instruction que Tutélaire, société mutualiste créée en 1907 pour " garantir une couverture prévoyance aux petites catégories de personnel des PTT " commercialise auprès de ses 400 000 adhérents, pour la plupart salariés, fonctionnaires et retraités de La Poste et d'Orange, un contrat de prévoyance dénommé TUT'LR couvrant la dépendance, l'incapacité de travail, l'hospitalisation et le décès. A la suite d'un contrôle sur place réalisé dans les conditions rappelées au point 1, l'ACPR a constaté que cette mutuelle n'avait entrepris aucune recherche visant à s'informer du décès éventuel de ses assurés et à identifier les bénéficiaires de contrats TUT'LR dont le titulaire était décédé dans le but de leur verser les sommes auxquelles ils ont droit. <br/>
<br/>
              6. Tutélaire soutient, en premier lieu, que le contrat TUT'LR est un contrat mixte de prévoyance qui n'est pas au nombre des contrats soumis aux dispositions précitées des articles L. 223-10-2 et L. 223-10, dernier alinéa, du code de la mutualité. Toutefois, il résulte de ces dispositions qu'une mutuelle doit mettre en œuvre les obligations qu'elles prescrivent pour tout contrat d'assurance comportant des engagements dont l'exécution dépend de la durée de la vie, y compris ceux comprenant également d'autres garanties, notamment au titre de la prévoyance, et ceci quelle que soit l'importance respective des différentes garanties offertes au sein du même contrat. En l'espèce, il est constant que le contrat TUT'LR comporte des garanties décès permettant aux ayants droit, en cas de décès de l'assuré pendant la durée de vie du contrat, de bénéficier d'un capital, ce qui constitue un engagement dont l'exécution dépend de la durée de la vie humaine, au sens du b du 1° du I de l'article L. 111-1 du code de la mutualité. Dès lors, et alors même que le contrat TUT'LR est dépourvu de finalité d'épargne, que son souscripteur peut, à chaque échéance annuelle, décider d'y mettre un terme et que les fonds investis sont perdus dans l'hypothèse où le risque garanti ne se réalise pas, ces modalités sont sans incidence sur les obligations auxquelles Tutélaire est soumise en application des articles L. 223-10-2 et L. 223-10 dernier alinéa du code de la mutualité.<br/>
<br/>
              7. Par suite, en estimant que Tutélaire était tenue, au titre de son contrat TUT'LR, de procéder systématiquement à des recherches sur le décès éventuel de ses assurés et, le cas échéant, de rechercher leurs ayants-droit, la commission des sanctions de l'ACPR n'a pas commis d'erreur de droit. <br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance du principe de légalité des délits et des peines :<br/>
<br/>
              8. Le principe de légalité des délits et des peines, lorsqu'il est appliqué à des sanctions qui n'ont pas le caractère de sanctions pénales, ne fait pas obstacle à ce que les infractions soient définies par référence aux obligations auxquelles est soumise une personne en raison de l'activité qu'elle exerce, de la profession à laquelle elle appartient ou de l'institution dont elle relève.<br/>
<br/>
              9. Il résulte de ce qui a été dit au point 4 que les dispositions précitées des articles L. 223-10-2 et L. 223-10 du code de la mutualité, applicables au contrat TUT'LR, sont dépourvues d'ambiguïté. En outre, l'instruction n° 2016-I- 26 du 13 décembre 2016 de l'ACPR sur la remise du rapport annuel obligatoire relatif aux contrats d'assurance sur la vie, ainsi que les précédentes sanctions prononcées par l'ACPR dans des situations similaires, permettaient à la requérante, en sa qualité de professionnel, d'appréhender de manière claire et prévisible la portée de ses obligations en matière de contrats d'assurance sur la vie non réclamés. Par suite, le moyen tiré de ce que la sanction infligée méconnaîtrait le principe de légalité des peines et des délits consacré par l'article 8 de la Déclaration des droits de l'homme et du citoyen et garanti par les stipulations de l'article 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté.<br/>
<br/>
              En ce qui concerne les manquements reprochés à Tutélaire :<br/>
<br/>
              S'agissant du grief tiré de l'absence de recherche exhaustive et systématique du décès potentiel des assurés :<br/>
<br/>
              10. Il résulte de l'instruction que, dès le mois de mars 2009, date à laquelle le dispositif AGIRA 2 est devenu pleinement opérationnel, il appartenait aux mutuelles de mettre en œuvre les moyens nécessaires permettant une recherche générale et systématique du décès éventuel de leurs adhérents et ce, quelle que soit la catégorie de contrats souscrits. En l'espèce, les investigations diligentées par la commission des sanctions ont établi que Tutélaire n'avait adhéré à ce dispositif qu'à compter du 31 janvier 2013 et interrogeait le Répertoire National d'Identification des Personnes Physiques (RNIPP) uniquement lorsqu'elle avait été informée du décès d'un de ses adhérents par l'intermédiaire d'une caisse régionale de pension, dans le seul but de se faire confirmer cette information, alors qu'une note de la direction du contrôle interne de l'organisme datée du 19 août 2015 rappelait la nécessité de " généraliser l'identification de ses adhérents décédés via le dispositif AGIRA. " En outre, le rapport de la mission de contrôle relève qu'à la demande des enquêteurs de l'ACPR, Tutélaire a soumis au RNIPP l'ensemble de son portefeuille d'adhérents constitué entre le 31 décembre 1991 et le 1er mai 2017, leur permettant ainsi d'identifier 758 décès certains et 3 631 décès potentiels parmi ses adhérents. La commission des sanctions en a déduit que le grief résultant d'un défaut de recherche exhaustive et systématique du décès potentiel des assurés ayant souscrit le contrat TUT'LR était caractérisé.<br/>
<br/>
              11. Si Tutélaire fait valoir que pour retenir ce grief, la commission n'a pas pris en compte les erreurs d'identification lors de la consultation du RNIPP résultant notamment d'homonymies, elle ne produit à l'appui de ses allégations aucun élément de nature à établir que l'ACPR, en se fondant sur ces constats, aurait commis une erreur d'appréciation en estimant que la mise en œuvre des recherches entreprises ne respectait pas l'exigence d'exercer avec diligence ses missions dans l'intérêt de ses adhérents et que Tutélaire avait ainsi manqué à ses obligations résultant des articles L. 223-10-2 et L. 223-10 du code de la mutualité. <br/>
<br/>
              S'agissant du grief tiré de l'absence de recherche permettant l'identification des bénéficiaires des contrats TUT'LR :<br/>
<br/>
              12. Il résulte de l'instruction que les moyens et procédures mis en œuvre par Tutélaire pour identifier les bénéficiaires des garanties décès lorsque ce décès était porté à sa connaissance par l'employeur ou la banque de l'adhérent décédé étaient très insuffisants. En l'espèce, le rapport d'inspection relève l'absence totale de toute action permettant cette identification s'agissant des décès signalés par les caisses de retraite antérieurement au 1er mars 2013. Si, à compter de cette date, le signalement d'un décès par un tiers (proche du défunt, notaire ou caisse régionale de paiement des pensions) a déclenché l'ouverture d'une procédure de recherche des bénéficiaires, celle-ci se limitait à l'envoi d'un courrier adressé à ce tiers ou aux ayants droit de l'adhérent qui, s'il restait sans réponse, n'était, le plus souvent, suivi d'aucune action. En outre, le rapport mentionne également que si, à la date du contrôle, Tutélaire avait identifié 5 460 contrats non réglés concernant des adhérents décédés entre 2003 et 2016 et correspondant à un montant total de près de 7 millions d'euros, la société mutualiste n'avait procédé à aucune recherche générale et systématique des assurés décédés. Enfin, au nombre de ces contrats, pour lesquels la mission de contrôle a examiné en détail 82 dossiers sélectionnés de façon aléatoire, 46 dossiers n'avaient fait l'objet d'aucune démarche d'identification des bénéficiaires, 26 dossiers avaient donné lieu à l'envoi d'un courrier adressé aux ayants droit sans comporter d'élément susceptible d'attester l'envoi d'une relance, et 10 dossiers avaient donné lieu à l'envoi d'un courrier et d'une relance aux ayants droit.<br/>
<br/>
              13. Tutélaire, qui ne conteste ni les carences des procédures et moyens mis en œuvre ni l'absence de caractère systématique de la recherche des bénéficiaires des contrats d'assurance TUT'LR, fait valoir que 1 041 dossiers ont été réglés en une année au 31 décembre 2017 sur les 5 460 contrats visés par le grief. Toutefois, ces règlements, intervenus après le début de la mission de contrôle, ne sont pas, en l'espèce, susceptibles d'atténuer la gravité du manquement reproché alors, d'une part, que 4 419 dossiers restaient toujours en déshérence et que l'échantillon examiné par la mission de contrôle démontre, d'autre part, l'ampleur des carences dans le respect par Tutélaire de ses obligations. Ainsi, contrairement à ce qui est soutenu, l'ACPR n'a pas commis d'erreur d'appréciation en estimant que Tutélaire avait manqué à ses obligations résultant des articles L. 223-10-2 et L. 223-10 du code de la mutualité. <br/>
<br/>
              Sur les sanctions prononcées :<br/>
<br/>
              En ce qui concerne le blâme et la sanction pécuniaire de 500 000 euros :<br/>
<br/>
              14. Aux termes de l'article L. 612-39 du code monétaire et financier, dans sa version applicable au litige, si un établissement de crédit " (...) a enfreint une disposition européenne, législative ou réglementaire au respect de laquelle l'Autorité a pour mission de veiller (...), la commission des sanctions peut prononcer l'une ou plusieurs des sanctions disciplinaires suivantes, en fonction de la gravité du manquement : / (...) / 2° Le blâme ; / (...). La commission des sanctions peut prononcer, soit à la place, soit en sus de ces sanctions, une sanction pécuniaire au plus égale à cent millions d'euros ou à 10 % du chiffre d'affaires annuel net ".<br/>
<br/>
              15. Il appartient au Conseil d'Etat, saisi d'une requête dirigée contre une sanction pécuniaire prononcée par la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution, de vérifier que son montant était, à la date à laquelle elle a été infligée, proportionné à la gravité des manquements commis ainsi qu'au comportement et à la situation, notamment financière, de la personne sanctionnée.<br/>
<br/>
              16. En premier lieu, il ressort de la décision attaquée que la commission des sanctions a retenu les deux griefs décrits aux points 11 et 13. Ces manquements au dispositif visant à améliorer la protection des bénéficiaires de contrats d'assurance sur la vie et à garantir le versement de prestations non réglées, revêtent, par leur nature et l'importance des sommes non versées, une gravité certaine. Ils ont, en outre, été commis sur une longue période, alors que le dispositif AGIRA 2 de généralisation des obligations de détection et de recherche était opérationnel depuis mars 2009. Tutélaire a ainsi failli aux engagements majeurs impartis à une société mutualiste dans le traitement et le suivi des dossiers. Enfin, compte tenu de l'objet de la législation visant à faciliter le règlement des prestations décès, à garantir la bonne exécution des contrats d'assurance sur la vie et un meilleur traitement  des contrats en déshérence, Tutélaire ne peut ni soutenir que les manquements constatés sont minimes, ni utilement se prévaloir de ce qu'elle n'aurait tiré aucun profit des dysfonctionnements relevés dès lors qu'elle redistribuait  l'essentiel des sommes en cause sous forme de participation aux bénéfices. <br/>
<br/>
              17. En second lieu, s'il résulte de l'instruction que le montant de la sanction pécuniaire infligée de 500 000 euros représente environ 25 % de son résultat net annuel pour l'année 2018, Tutélaire a réalisé cette même année un chiffre d'affaires de 41 millions et disposait, à la fin de ce même exercice, d'environ 80 millions d'euros de fonds propres. Par ailleurs, la circonstance que la commission des sanctions aurait prononcé des sanctions moindres dans des affaires comparables est, en tout état de cause, sans incidence sur le bien-fondé de la sanction pécuniaire qui n'est, en l'espèce, pas disproportionnée. <br/>
<br/>
              18. Au vu de l'ensemble de ces éléments, et notamment de la gravité des faits invoqués, la commission a pu, sans porter atteinte au principe de proportionnalité des sanctions, prononcer à l'encontre de Tutélaire un blâme et une sanction pécuniaire de 500 000 euros.<br/>
<br/>
              En ce qui concerne la sanction complémentaire de publication :<br/>
<br/>
              19. Aux termes du dernier alinéa de l'article L. 612-39 du code monétaire et financier, dans sa version applicable au litige : " La décision de la commission des sanctions est rendue publique dans les publications, journaux ou supports qu'elle désigne, dans un format proportionné à la faute commise et à la sanction infligée. Les frais sont supportés par les personnes sanctionnées. Toutefois, lorsque la publication risque de perturber gravement les marchés financiers ou de causer un préjudice disproportionné aux parties en cause, la décision de la commission peut prévoir qu'elle ne sera pas publiée. "<br/>
<br/>
              20. Outre sa portée punitive, l'objet de la décision par laquelle la commission des sanctions rend publique, aux frais de l'intéressé, la sanction qu'elle prononce, est de porter à la connaissance de toutes les personnes intéressées tant les irrégularités qui ont été commises que les sanctions que celles-ci ont appelées, afin de satisfaire aux exigences d'intérêt général relatives à la protection des clients des établissements concernés et au bon fonctionnement des marchés financiers.<br/>
<br/>
              21. Pour contester la publication sous forme nominative pendant cinq ans de la sanction qui lui a été infligée, Tutélaire invoque un risque de réputation élevé auprès de ses adhérents. Toutefois, cet élément ne permet pas d'établir que la publication de la décision de sanction lui causerait un préjudice disproportionné au regard de l'intérêt public qui s'attache à une telle publication, alors que les manquements sanctionnés visent précisément à mettre un terme à des pratiques professionnelles contraires aux obligations pesant sur les assureurs et les mutuelles et à faire respecter les exigences de bonnes pratiques et d'amélioration de la gestion des contrats d'assurance sur la vie en déshérence voulues par le législateur. <br/>
<br/>
              22. Dans ces conditions, il ne résulte pas de l'instruction que la publication de la décision de sanction sous forme nominative pendant cinq ans serait susceptible de causer à Tutélaire un préjudice disproportionné. Par suite, la décision de sanction, qui est suffisamment motivée, est en adéquation avec les manquements commis et ne méconnaît pas le principe de proportionnalité. <br/>
<br/>
              23. Il résulte de tout ce qui précède que la requête doit être rejetée, y compris ses conclusions subsidiaires tendant à une réduction du montant de la sanction. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              24. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'APCR qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société requérante la somme de 3 000 euros au titre des frais exposés par elle et non compris dans les dépens.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société mutualiste Tutélaire est rejetée.<br/>
<br/>
Article 2 : La société mutualiste Tutélaire versera la somme de 3 000 euros à l'Autorité de contrôle prudentiel et de résolution au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société mutualiste Tutélaire et à l'Autorité de contrôle prudentiel et de résolution.<br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">12-02 ASSURANCE ET PRÉVOYANCE. - CONTRATS D'ASSURANCE. - DISPOSITIF DE PRÉVENTION DE LA NON-EXÉCUTION DES ENGAGEMENTS DONT LE FAIT GÉNÉRATEUR EST LE DÉCÈS (AGIRA 2) - 1) CHAMP D'APPLICATION - A) INCLUSION - CONTRAT DONT L'EXÉCUTION DÉPEND DE LA DURÉE DE LA VIE HUMAINE - B) CIRCONSTANCE SANS INCIDENCE - CONTRAT COMPORTANT AUSSI D'AUTRES GARANTIES - 2) ILLUSTRATION - CONTRAT DE PRÉVOYANCE COMPORTANT UNE GARANTIE DÉCÈS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">12-04 ASSURANCE ET PRÉVOYANCE. - MUTUELLES (VOIR : MUTUALITÉ ET COOPÉRATION). - DISPOSITIF DE PRÉVENTION DE LA NON-EXÉCUTION DES ENGAGEMENTS DONT LE FAIT GÉNÉRATEUR EST LE DÉCÈS (AGIRA 2) - 1) CHAMP D'APPLICATION - A) INCLUSION - CONTRAT DONT L'EXÉCUTION DÉPEND DE LA DURÉE DE LA VIE HUMAINE - B) CIRCONSTANCE SANS INCIDENCE - CONTRAT COMPORTANT AUSSI D'AUTRES GARANTIES - 2) ILLUSTRATION - CONTRAT DE PRÉVOYANCE COMPORTANT UNE GARANTIE DÉCÈS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">42-01-01 MUTUALITÉ ET COOPÉRATION. - MUTUELLES. - QUESTIONS GÉNÉRALES. - DISPOSITIF DE PRÉVENTION DE LA NON-EXÉCUTION DES ENGAGEMENTS DONT LE FAIT GÉNÉRATEUR EST LE DÉCÈS (AGIRA 2) - 1) CHAMP D'APPLICATION - A) INCLUSION - CONTRAT DONT L'EXÉCUTION DÉPEND DE LA DURÉE DE LA VIE HUMAINE - B) CIRCONSTANCE SANS INCIDENCE - CONTRAT COMPORTANT AUSSI D'AUTRES GARANTIES - 2) ILLUSTRATION - CONTRAT DE PRÉVOYANCE COMPORTANT UNE GARANTIE DÉCÈS.
</SCT>
<ANA ID="9A"> 12-02 Il résulte de l'article L. 223-10-2 et du dernier alinéa de l'article L. 223-10 du code de la mutualité, introduits dans ce code ainsi que, de manière similaire, dans le code des assurances, par la loi n° 2007-1775 du 17 décembre 2007, et modifiés par celle n° 2014-617 du 13 juin 2014, qu'elles ont pour objet de prévenir la non-exécution des engagements, pris à l'égard des assurés, dont le fait générateur est le décès.......1) a) A cet effet, ces dispositions, mises en œuvre par le dispositif AGIRA 2, visent, sans restriction aucune, les engagements que les assureurs et les mutuelles peuvent avoir pour objet de réaliser en vertu du b du 1° du I de l'article L. 111-1 du code de la mutualité, parmi lesquels figurent ceux dont l'exécution dépend de la durée de la vie humaine.......b) Une mutuelle doit mettre en œuvre les obligations qu'elles prescrivent pour tout contrat d'assurance comportant des engagements dont l'exécution dépend de la durée de la vie, y compris ceux comprenant également d'autres garanties, notamment au titre de la prévoyance, et ceci quelle que soit l'importance respective des différentes garanties offertes au sein du même contrat.......2) Société mutualiste commercialisant auprès de ses adhérents un contrat de prévoyance couvrant la dépendance, l'incapacité de travail, l'hospitalisation et le décès.......Garantie décès permettant aux ayants droit, en cas de décès de l'assuré pendant la durée de vie du contrat, de bénéficier d'un capital, ce qui constitue un engagement dont l'exécution dépend de la durée de la vie humaine, au sens du b du 1° du I de l'article L. 111-1 du code de la mutualité.......Dès lors, et alors même que le contrat est dépourvu de finalité d'épargne, que son souscripteur peut, à chaque échéance annuelle, décider d'y mettre un terme et que les fonds investis sont perdus dans l'hypothèse où le risque garanti ne se réalise pas, ces modalités sont sans incidence sur les obligations auxquelles la société mutualiste est soumise en application des articles L. 223-10-2 et L. 223-10 dernier alinéa du code de la mutualité.</ANA>
<ANA ID="9B"> 12-04 Il résulte de l'article L. 223-10-2 et du dernier alinéa de l'article L. 223-10 du code de la mutualité, introduits dans ce code ainsi que, de manière similaire, dans le code des assurances, par la loi n° 2007-1775 du 17 décembre 2007, et modifiés par celle n° 2014-617 du 13 juin 2014, qu'elles ont pour objet de prévenir la non-exécution des engagements, pris à l'égard des assurés, dont le fait générateur est le décès.......1) a) A cet effet, ces dispositions, mises en œuvre par le dispositif AGIRA 2, visent, sans restriction aucune, les engagements que les assureurs et les mutuelles peuvent avoir pour objet de réaliser en vertu du b du 1° du I de l'article L. 111-1 du code de la mutualité, parmi lesquels figurent ceux dont l'exécution dépend de la durée de la vie humaine.......b) Une mutuelle doit mettre en œuvre les obligations qu'elles prescrivent pour tout contrat d'assurance comportant des engagements dont l'exécution dépend de la durée de la vie, y compris ceux comprenant également d'autres garanties, notamment au titre de la prévoyance, et ceci quelle que soit l'importance respective des différentes garanties offertes au sein du même contrat.......2) Société mutualiste commercialisant auprès de ses adhérents un contrat de prévoyance couvrant la dépendance, l'incapacité de travail, l'hospitalisation et le décès.......Garantie décès permettant aux ayants droit, en cas de décès de l'assuré pendant la durée de vie du contrat, de bénéficier d'un capital, ce qui constitue un engagement dont l'exécution dépend de la durée de la vie humaine, au sens du b du 1° du I de l'article L. 111-1 du code de la mutualité.......Dès lors, et alors même que le contrat est dépourvu de finalité d'épargne, que son souscripteur peut, à chaque échéance annuelle, décider d'y mettre un terme et que les fonds investis sont perdus dans l'hypothèse où le risque garanti ne se réalise pas, ces modalités sont sans incidence sur les obligations auxquelles la société mutualiste est soumise en application des articles L. 223-10-2 et L. 223-10 dernier alinéa du code de la mutualité.</ANA>
<ANA ID="9C"> 42-01-01 Il résulte de l'article L. 223-10-2 et du dernier alinéa de l'article L. 223-10 du code de la mutualité, introduits dans ce code ainsi que, de manière similaire, dans le code des assurances, par la loi n° 2007-1775 du 17 décembre 2007, et modifiés par celle n° 2014-617 du 13 juin 2014, qu'elles ont pour objet de prévenir la non-exécution des engagements, pris à l'égard des assurés, dont le fait générateur est le décès.......1) a) A cet effet, ces dispositions, mises en œuvre par le dispositif AGIRA 2, visent, sans restriction aucune, les engagements que les assureurs et les mutuelles peuvent avoir pour objet de réaliser en vertu du b du 1° du I de l'article L. 111-1 du code de la mutualité, parmi lesquels figurent ceux dont l'exécution dépend de la durée de la vie humaine.......b) Une mutuelle doit mettre en œuvre les obligations qu'elles prescrivent pour tout contrat d'assurance comportant des engagements dont l'exécution dépend de la durée de la vie, y compris ceux comprenant également d'autres garanties, notamment au titre de la prévoyance, et ceci quelle que soit l'importance respective des différentes garanties offertes au sein du même contrat.......2) Société mutualiste commercialisant auprès de ses adhérents un contrat de prévoyance couvrant la dépendance, l'incapacité de travail, l'hospitalisation et le décès.......Garantie décès permettant aux ayants droit, en cas de décès de l'assuré pendant la durée de vie du contrat, de bénéficier d'un capital, ce qui constitue un engagement dont l'exécution dépend de la durée de la vie humaine, au sens du b du 1° du I de l'article L. 111-1 du code de la mutualité.......Dès lors, et alors même que le contrat est dépourvu de finalité d'épargne, que son souscripteur peut, à chaque échéance annuelle, décider d'y mettre un terme et que les fonds investis sont perdus dans l'hypothèse où le risque garanti ne se réalise pas, ces modalités sont sans incidence sur les obligations auxquelles la société mutualiste est soumise en application des articles L. 223-10-2 et L. 223-10 dernier alinéa du code de la mutualité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
