<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034698316</ID>
<ANCIEN_ID>JG_L_2017_05_000000391730</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/69/83/CETATEXT000034698316.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/05/2017, 391730</TITRE>
<DATE_DEC>2017-05-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391730</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:391730.20170512</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'organisme gestionnaire de l'école catholique (OGEC) Sainte-Thérèse a demandé au tribunal administratif de Lyon :<br/>
              - d'annuler la décision du 30 juillet 2007 par laquelle le maire de Villeurbanne a rejeté sa demande indemnitaire ainsi que la décision implicite par laquelle le préfet du Rhône a rejeté son recours administratif en date du 31 juillet 2008 ;<br/>
              - à titre principal, de condamner la commune de Villeurbanne à lui verser la somme de 286 618,86 euros, assortie des intérêts au taux légal à compter du 10 juillet 2007 et de leur capitalisation et, à titre subsidiaire, de désigner un expert ayant pour mission de déterminer le montant du forfait communal qu'il aurait dû percevoir au titre des années scolaires 2003-2004 à 2006-2007, après intégration des dépenses regardées comme étant des dépenses de fonctionnement entrant dans la composition du montant du forfait communal au titre de ces années.<br/>
<br/>
              Par un jugement n° 1007968 du 16 mai 2013, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13LY02005 du 12 mai 2015, la cour administrative d'appel de Lyon, avant de statuer sur la requête de l'OGEC de l'Immaculée Conception, Immacgestion, venu aux droits de l'organisme gestionnaire de l'école catholique (OGEC) Sainte-Thérèse, a ordonné une expertise destinée à évaluer le montant du préjudice éventuel de cet organisme, lié au calcul du forfait communal au titre des années scolaires 2003-2004 à 2006-2007.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 juillet et 2 octobre 2015 et 4 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Villeurbanne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'OGEC de l'Immaculée Conception, Immacgestion, une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la commune de Villeurbanne et à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de l'OGEC de l'Immaculée Conception, Immacgestion ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'organisme de gestion de l'école catholique (OGEC) Ecole Sainte-Thérése, sous contrat d'association avec l'Etat, a demandé le 10 juillet 2007 à la commune de Villeurbanne de lui verser une somme de 122 585 euros en réparation du préjudice résultant de l'insuffisance de sa contribution aux dépenses de fonctionnement des classes de son établissement d'enseignement privé pour les années scolaires 2003-2004 à 2006-2007. Cette demande indemnitaire a été rejetée par le maire de Villeurbanne par décision du 30 juillet 2007. Par un jugement du 16 mai 2013, le tribunal administratif de Lyon a rejeté comme irrecevables les conclusions de la requête de l'OGEC tendant à la condamnation de la commune à lui verser la somme de 286 618,86 euros. Par un arrêt du 12 mai 2015, la cour administrative d'appel de Lyon, après avoir annulé ce jugement en tant qu'il a rejeté comme irrecevables les conclusions indemnitaires de l'OGEC de l'Immaculée Conception, Immacgestion, venu aux droits de l'OGEC Ecole Sainte-Thérése, a décidé, avant de statuer sur ces conclusions, de désigner un expert chargé notamment de déterminer les modalités de calcul du coût par élève scolarisé dans une école primaire publique de la commune, ainsi que les dépenses de fonctionnement exposées annuellement par cette commune pour ces élèves, au cours de chacune des années scolaires comprises entre 2003 et 2007. La commune de Villeurbanne se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              Sur le moyen tiré de la méconnaissance de l'article L. 442-5-2 du code de l'éducation :<br/>
<br/>
              2. Aux termes de l'article L. 442-5-2 du code de l'éducation, dans sa rédaction issue de l'article 2 de la loi du 28 octobre 2009 tendant à garantir la parité de financement entre les écoles élémentaires publiques et privées sous contrat d'association lorsqu'elles accueillent des élèves scolarisés hors de leur commune de résidence, publiée au Journal officiel du 29 octobre 2009 : " Lorsqu'elle est obligatoire, la contribution aux dépenses de fonctionnement des classes élémentaires sous contrat d'association des établissements privés du premier degré est, en cas de litige, fixée par le représentant de l'Etat dans le département qui statue dans un délai de trois mois à compter de la date à laquelle il a été saisi par la plus diligente des parties. ". Il résulte de ces dispositions qu'en cas de litige portant sur la contribution obligatoire d'une commune aux dépenses de fonctionnement de classes élémentaires d'un établissement d'enseignement privé du premier degré sous contrat d'association, un recours contentieux ne peut être introduit qu'après que le représentant de l'Etat dans le département a été saisi par la partie la plus diligente, afin qu'il fixe cette contribution. La saisine obligatoire du représentant de l'Etat dans le département prévue par cet article n'est toutefois applicable qu'aux seuls litiges nés à compter du 30 octobre 2009, date de l'entrée en vigueur de la loi du 28 octobre 2009. <br/>
<br/>
              3. Les juges d'appel ont relevé que le litige portant sur la contribution obligatoire aux dépenses de fonctionnement des classes élémentaires de l'établissement d'enseignement privé sous contrat d'association géré par l'OGEC était né le 30 juillet 2007, soit antérieurement à la date d'entrée en vigueur de l'article L. 442-5-2 du code de l'éducation, lequel n'était dès lors pas applicable au litige. Par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en en déduisant que les premiers juges avaient rejeté à tort pour irrecevabilité les conclusions indemnitaires dont ils étaient saisis, faute de recours administratif préalable présenté au préfet du Rhône en application de l'article L. 442-5-2 du code de l'éducation avant l'introduction de cette demande devant le tribunal administratif.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              4. Le motif par lequel la cour administrative d'appel a jugé que la demande indemnitaire présentée par l'OGEC n'était pas irrecevable justifiait nécessairement, à lui seul, le dispositif d'annulation du jugement du tribunal administratif. Si la cour administrative d'appel a également fondé sa décision sur le motif tiré de ce qu'en tout état de cause, et alors même que la contestation se serait prolongée entre la promulgation de l'article L. 442-5-2 du code de l'éducation et l'enregistrement de la demande de première instance, le tribunal ne pouvait, sans compromettre la poursuite de la contestation indemnitaire née le 30 juillet 2007, rejeter la demande de l'OGEC pour irrecevabilité faute de saisine préalable du préfet du Rhône, il résulte de ce qui vient d'être dit qu'un tel motif ne peut qu'être regardé comme surabondant. Dès lors, le moyen tiré de ce que ce motif serait entaché d'insuffisance de motivation ou d'erreur de droit ne saurait, quel qu'en soit le bien-fondé, entraîner l'annulation de l'arrêt attaqué.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Villeurbanne a soutenu, dans son mémoire en défense devant la cour administrative d'appel, que la contribution obligatoire aux dépenses de fonctionnement des classes élémentaires de l'établissement d'enseignement privé sous contrat d'association géré par l'OGEC n'était pas sous-évaluée, en se fondant sur l'existence et le contenu du protocole d'accord fixant les modalités de calcul de cette contribution signé par l'OGEC le 19 juin 2002, sur la validité de ce protocole d'accord au regard des textes applicables et du principe de parité, et sur la circonstance qu'en l'absence de dénonciation par l'OGEC, ce protocole lui était opposable. Par suite, la cour administrative d'appel n'a ni méconnu les écritures de la commune de Villeurbanne, ni entaché sur ce point son arrêt d'insuffisance de motivation en jugeant, avant de statuer sur l'éventuel préjudice résultant d'une sous-évaluation de cette contribution obligatoire, d'une part, que la seule circonstance que la commune et l'OGEC seraient engagés dans des relations contractuelles relatives à la fixation de ce coût moyen ou du montant de cette contribution, ne saurait faire obstacle à leur détermination conformément aux dispositions législatives et réglementaires applicable et, d'autre part, que la commune de Villeurbanne n'était pas fondée à soutenir que l'OGEC, avec lequel elle était liée par une convention du 19 juin 2002 toujours en vigueur, ne pouvait rechercher sa responsabilité au titre du préjudice qu'il estimait avoir subi en raison de l'illégalité commise dans la détermination du coût moyen d'un élève scolarisé dans les classes élémentaires de l'enseignement public de la commune.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la commune de Villeurbanne n'est pas fondée à demander l'annulation de l'arrêt attaqué, qui est suffisamment motivé. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée à ce titre par la commune de Villeurbanne soit mise à la charge de l'OGEC de l'Immaculée Conception, Immacgestion, qui n'est pas la partie perdante dans la présente instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Villeurbanne le versement à l'OGEC de l'Immaculée Conception, Immacgestion, d'une somme de 1 000 euros au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Villeurbanne est rejeté.<br/>
Article 2 : La commune de Villeurbanne versera la somme de 1 000 euros à l'OGEC de Immaculée Conception, Immacgestion au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Villeurbanne et à l'organisme gestionnaire de l'école Immaculée Conception, Immacgestion.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-07-02-03 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS. RELATIONS ENTRE LES COLLECTIVITÉS PUBLIQUES ET LES ÉTABLISSEMENTS PRIVÉS. CONTRIBUTIONS DES COMMUNES AUX DÉPENSES DE FONCTIONNEMENT DES ÉTABLISSEMENTS PRIVÉS SOUS CONTRAT D'ASSOCIATION. - CONTESTATIONS RELATIVES AUX CONTRIBUTIONS OBLIGATOIRES DES COMMUNES - CARACTÈRE OBLIGATOIRE DE LA SAISINE, PRÉALABLE À L'EXERCICE D'UN RECOURS CONTENTIEUX, DU REPRÉSENTANT DE L'ETAT DANS LE DÉPARTEMENT - 1) EXISTENCE - 2) DATE D'ENTRÉE EN VIGUEUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - SAISINE DU REPRÉSENTANT DE L'ETAT DANS LE DÉPARTEMENT POUR QU'IL FIXE LA CONTRIBUTION OBLIGATOIRE D'UNE COMMUNE AUX DÉPENSES DE FONCTIONNEMENT DE CLASSES ÉLÉMENTAIRES D'UN ÉTABLISSEMENT D'ENSEIGNEMENT PRIVÉ DU PREMIER DEGRÉ SOUS CONTRAT D'ASSOCIATION (ART. L. 442-5-2 DU CODE DE L'ÉDUCATION) - CARACTÈRE OBLIGATOIRE PRÉALABLEMENT À L'EXERCICE D'UN RECOURS CONTENTIEUX - 1) EXISTENCE - 2) DATE D'ENTRÉE EN VIGUEUR.
</SCT>
<ANA ID="9A"> 30-02-07-02-03 1) Il résulte de l'article L. 442-5-2 du code de l'éducation qu'en cas de litige portant sur la contribution obligatoire d'une commune aux dépenses de fonctionnement de classes élémentaires d'un établissement d'enseignement privé du premier degré sous contrat d'association, un recours contentieux ne peut être introduit qu'après que le représentant de l'Etat dans le département a été saisi par la partie la plus diligente, afin qu'il fixe cette contribution.... ,,2) La saisine obligatoire du représentant de l'Etat dans le département prévue par cet article n'est toutefois applicable qu'aux seuls litiges nés à compter du 30 octobre 2009, date de l'entrée en vigueur de la loi n° 2009-1312 du 28 octobre 2009.</ANA>
<ANA ID="9B"> 54-01-02-01 1) Il résulte de l'article L. 442-5-2 du code de l'éducation qu'en cas de litige portant sur la contribution obligatoire d'une commune aux dépenses de fonctionnement de classes élémentaires d'un établissement d'enseignement privé du premier degré sous contrat d'association, un recours contentieux ne peut être introduit qu'après que le représentant de l'Etat dans le département a été saisi par la partie la plus diligente, afin qu'il fixe cette contribution.... ,,2) La saisine obligatoire du représentant de l'Etat dans le département prévue par cet article n'est toutefois applicable qu'aux seuls litiges nés à compter du 30 octobre 2009, date de l'entrée en vigueur de la loi n° 2009-1312 du 28 octobre 2009.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
