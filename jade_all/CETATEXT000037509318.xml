<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037509318</ID>
<ANCIEN_ID>JG_L_2018_10_000000422126</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/50/93/CETATEXT000037509318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 19/10/2018, 422126, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422126</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Prada Bordenave</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2018:422126.20181019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 422126, par un mémoire, enregistré le 9 août 2018 et présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B...A...demande, à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 7 mars 2018, du ministre de la défense lui refusant l'accès aux données susceptibles de le concerner figurant dans le traitement automatisé de données de la direction du renseignement militaire (DRM), de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 773-1 à L. 773-8 du code de justice administrative, dans leur rédaction issue de la loi du 24 juillet 2015 relative au renseignement, et des articles 26, 41 et 42 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.<br/>
<br/>
<br/>
              2° Sous le n° 422634, par un mémoire, enregistré le 9 août 2018 et présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B...A...demande, à l'appui de sa requête tendant à l'annulation pour excès de pouvoir de la décision, révélée par le courrier de la présidente de la CNIL du 7 mars 2018, du ministre de la défense lui refusant l'accès aux données susceptibles de le concerner figurant dans le traitement automatisé de données de la direction générale de la sécurité extérieure ( DGSE), de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des mêmes dispositions législatives.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 2015-912 du 24 juillet 2015 ;<br/>
              - la décision n° 2004-499DC du Conseil constitutionnel, du 29 juillet 2004 ;<br/>
              - la décision n° 2015-713 DC du Conseil Constitutionnel du 23 juillet 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Prada Bordenave, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les mémoires présentés par M. A...sous les n° 422126 et 422634 soulèvent la même question prioritaire de constitutionnalité. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. En premier lieu, le Conseil constitutionnel a, dans les motifs et le dispositif de sa décision n° 2004-449DC du 29 juillet 2004, déclaré conformes à la Constitution les dispositions de l'article 26 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Aucun changement dans les circonstances de droit ou de fait survenu depuis cette décision n'est de nature à justifier que la conformité de ces dispositions à la Constitution soit à nouveau examinée par le Conseil constitutionnel. <br/>
<br/>
              4. En deuxième lieu, le Conseil constitutionnel a, dans les motifs et le dispositif de sa décision n° 2015-713 DC du 23 juillet 2015 déclaré conformes à la Constitution les dispositions des articles L. 773-2 à L. 773-7 du code de justice administrative. Aucun changement dans les circonstances de droit ou de fait survenu depuis cette décision n'est de nature à justifier que la conformité de ces dispositions à la Constitution soit à nouveau examinée par le Conseil constitutionnel.<br/>
<br/>
              5. Aux termes de l'article 41 de la loi du 6 janvier 1978 : " Par dérogation aux articles 39 et 40, lorsqu'un traitement intéresse la sûreté de l'Etat, la défense ou la sécurité publique, le droit d'accès s'exerce dans les conditions prévues par le présent article pour l'ensemble des informations qu'il contient. / La demande est adressée à la commission qui désigne l'un de ses membres appartenant ou ayant appartenu au Conseil d'Etat, à la Cour de cassation ou à la Cour des comptes pour mener les investigations utiles et faire procéder aux modifications nécessaires. Celui-ci peut se faire assister d'un agent de la commission. Il est notifié au requérant qu'il a été procédé aux vérifications./ Lorsque la commission constate, en accord avec le responsable du traitement, que la communication des données qui y sont contenues ne met pas en cause ses finalités, la sûreté de l'Etat, la défense ou la sécurité publique, ces données peuvent être communiquées au requérant./ Lorsque le traitement est susceptible de comprendre des informations dont la communication ne mettrait pas en cause les fins qui lui sont assignées, l'acte réglementaire portant création du fichier peut prévoir que ces informations peuvent être communiquées au requérant par le gestionnaire du fichier directement saisi ". Aux termes de l'article 42 de la même loi : " Les dispositions de l'article 41 sont applicables aux traitements mis en oeuvre par les administrations publiques et les personnes privées chargées d'une mission de service public qui ont pour mission de contrôler ou recouvrer des impositions, si un tel droit a été prévu par l'autorisation mentionnée aux articles 26 ou 27 ".<br/>
<br/>
              6. Les dispositions des articles 41 et 42 de la loi du 6 janvier 1978 organisent, par dérogation aux articles 39 et 40 de la même loi, un droit d'accès indirect aux données à caractère personnel contenues dans l'un des fichiers intéressant la sûreté de l'Etat, la défense ou la sécurité publique dont la liste est fixée par un décret en Conseil d'Etat et permettent à l'autorité gestionnaire du fichier de s'opposer à la communication par la Commission nationale de l'informatique et des libertés des informations concernant une personne à celle-ci lorsque cette communication risquerait de porter atteinte à la sûreté de l'Etat, la défense ou la sécurité publique, sous le contrôle du Conseil d'Etat. Dans les conditions précisées aux articles L. 773-1 et suivants du code de justice administrative, il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par le fichier, adéquates et proportionnées. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux, soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité, soit que les données à caractère personnel le concernant soient inexactes, incomplètes, équivoques ou périmées, soit que leur collecte ou leur utilisation, leur communication ou leur conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données litigieuses. En pareil cas, doit être annulée la décision implicite de refusant de procéder à un tel effacement ou une telle rectification. <br/>
<br/>
              7. L'inscription d'une personne dans l'un des fichiers de sûreté figurant à l'article R. 841-2 du code de justice administrative n'affecte pas la liberté individuelle, au sens de l'article 66 de la Constitution. Par suite, contrairement à ce que soutient le requérant, elle n'a pas à être placée sous le contrôle de l'autorité judiciaire. En outre, les dispositions législatives en cause, qui organisent l'accès indirect aux données des fichiers figurant à l'article R. 841-2 du code de justice administrative et le contrôle juridictionnel par la formation spécialisée ne méconnaissent pas les droits et libertés garantis par les articles 1er et 10 de la Déclaration des droits de l'homme et du citoyen, contrairement à ce qui est également soutenu. Les restrictions que ces mêmes dispositions apportent au caractère contradictoire de la procédure, notamment juridictionnelle, et à la protection du secret de la vie privée sont justifiées par la nécessité d'assurer la sauvegarde de la sécurité publique, de la sûreté de l'Etat et de la défense et, par voie de conséquence, de l'ordre public également garantis par la Constitution. Les pouvoirs dont la formation spécialisée est investie pour instruire les requêtes, relever d'office toutes les illégalités qu'elle constate et enjoindre à l'administration de prendre toutes mesures utiles afin de remédier aux illégalités constatées ainsi que l'obligation dans laquelle l'autorité gestionnaire du fichier se trouve, lorsqu'il a été constaté que des données figurent illégalement dans un fichier, de les effacer ou de les rectifier, dans la mesure du nécessaire garantissent l'effectivité du contrôle juridictionnel de l'exercice du droit d'accès indirect aux données personnelles figurant dans des traitements intéressant la sûreté de l'Etat et ainsi le respect du droit à un recours effectif. Par suite, le moyen tiré de ce que les articles 41 et 42  de la loi du 6 janvier 1978 et les articles L. 773-1 et 773-8 du code de justice administrative méconnaîtraient les droits et libertés garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen ne présente pas un caractère sérieux. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.A....<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de la défense. <br/>
Copie en sera adressée au Premier ministre et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
