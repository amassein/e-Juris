<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281214</ID>
<ANCIEN_ID>JG_L_2015_09_000000393089</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281214.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/09/2015, 393089, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393089</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BENABENT, JEHANNIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:393089.20150925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 3 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'amicale laïque Aplemont Le Havre Basket demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision du 3 juillet 2015 par laquelle le bureau fédéral de la Fédération française de basket-ball a décidé de ne pas pourvoir aux places vacantes en Ligue féminine 2.<br/>
<br/>
              Elle soutient que : <br/>
              - le Conseil d'Etat est compétent pour connaître de sa requête en premier et dernier ressort ;<br/>
              - la condition d'urgence est remplie dès lors que la décision litigieuse préjudicie gravement et immédiatement à ses intérêts ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision litigieuse ;<br/>
              - elle a été prise selon une procédure irrégulière ;<br/>
              - elle est constitutive d'une rupture d'égalité entre les clubs de basket-ball et revêt un caractère discriminatoire ;<br/>
              - elle ne lui a pas été régulièrement notifiée.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 16 septembre 2015, la Fédération française de basket-ball conclut au rejet de la requête et à ce que les entiers dépens soient mis à la charge de l'amicale laïque Aplemont Le Havre Basket. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par l'amicale laïque Aplemont Le Havre Basket ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'amicale laïque Aplemont Le Havre Basket, d'autre part, la Fédération française de basket-ball ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 septembre 2015 à 10 heures 30 au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Jehannin, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'amicale laïque Aplemont Le Havre Basket ;<br/>
<br/>
              - le représentant de l'amicale laïque Aplemont Le Havre Basket ;<br/>
<br/>
              - le représentant de la Fédération française de basket-ball ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 23 septembre 2015 à 15 heures ;<br/>
<br/>
              Vu les pièces, enregistrées le 23 septembre 2015, produites par la Fédération française de basket-ball ;<br/>
<br/>
              Vu les observations, enregistrées le 23 septembre 2015, présentées par l'amicale laïque Aplemont Le Havre Basket ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - les règlements sportifs généraux de la Fédération française de basket-ball ;<br/>
              - les règlements généraux de la Fédération française de basket-ball ;<br/>
              - le code du sport ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que l'amicale laïque Aplemont Le Havre Basket évoluait pour la saison 2014-2015 en Ligue féminine 2 (LF2) de basket-ball ; qu'à l'issue de la dernière journée de ce championnat qui s'est tenue le 25 avril 2015, ce club a terminé à la 12ème place sur les 14 équipes de sa division ; qu'en application du règlement sportif particulier de la LF2, il se trouvait ainsi en position relégable ; que par sa décision du 3 juillet 2015, le bureau fédéral de la Fédération française de basket-ball (FFBB) a décidé de ne pas pourvoir aux places laissées vacantes en LF2 du fait de la relégation d'autres équipes et a confirmé la relégation de l'amicale laïque Aplemont Le Havre Basket en division Nationale féminine 1 (NF1) pour la saison 2015-2016 ; que la requérante demande la suspension de l'exécution de cette décision ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article 31 du règlement de la FFBB: " Le vote par correspondance et le vote par procuration sont interdits. Toutefois, en cas d'urgence et pour des questions simples ne donnant pas lieu à scrutin secret obligatoire, le Bureau peut être consulté à distance " ; que le moyen tiré de ce que la décision du 3 juillet 2015 du bureau fédéral, adoptée selon cette procédure, aurait été prise au terme d'une procédure irrégulière, n'est pas de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision litigieuse ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que l'amicale laïque Aplemont Le Havre Basket soutient que la décision du 3 juillet 2015 du bureau fédéral de la Fédération française de basket-ball est constitutive d'une rupture d'égalité entre les clubs de basket-ball et revêt un caractère discriminatoire et arbitraire dès lors, d'une part, que la procédure de remplacement des places vacantes prévue à l'article 19 des règlements sportifs généraux de la Fédération est habituellement mise en oeuvre en LF2 et a par ailleurs été mise en oeuvre dans d'autres divisions pour la saison 2015-2016 et, d'autre part, qu'elle ne repose sur aucune considération objective ; que, toutefois, l'article 19 du règlement précise que le remplacement des places vacantes est une simple faculté à la discrétion du bureau fédéral de la Fédération ; que l'amicale laïque Aplemont Le Havre Basket ne peut ainsi se prévaloir d'aucun droit à la participation à la LF2 pour la saison 2015-2016 ; qu'en outre, il ressort de l'audience que la décision litigieuse est notamment fondée sur le motif que l'organisation d'une saison de la LF2 avec un nombre impair d'équipes, à quelques jours de son démarrage, serait rendue plus difficile pour la fédération et pour l'ensemble des clubs de LF2 ; que le moyen soulevé par l'amicale laïque Aplemont Le Havre Basket tiré du caractère arbitraire et discriminatoire de la décision contestée n'est pas de nature à créer un doute sérieux sur sa légalité ;<br/>
<br/>
              5. Considérant, en troisième lieu, que le moyen tiré de l'absence de communication officielle de la décision du bureau fédéral n'est pas de nature à créer un doute sérieux sur sa légalité ; qu'au demeurant, la décision du 3 juillet 2015 a fait l'objet d'un procès-verbal publié sur le site internet de la Fédération française de basket-ball, conformément à l'article 32 des règlements généraux de la Fédération française de basket-ball ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition tenant à l'existence d'une situation d'urgence, et pour regrettables et préjudiciables que soit la situation d'incertitude résultant, entre deux saisons, des conditions dans lesquelles est mise en oeuvre la procédure de remplacement des places vacantes dans chaque division prévue à l'article 19 du règlement de la Fédération française de basket-ball, que la requête de l'amicale laïque Aplemont Le Havre Basket doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la Fédération française de basket-ball tendant à ce que soient mis à la charge de l'amicale laïque Aplemont Le Havre Basket les entiers dépens ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'amicale laïque Aplemont Le Havre Basket est rejetée.<br/>
<br/>
Article 2 : Les conclusions de la Fédération française de basket-ball tendant à ce que soient mis à la charge de l'amicale laïque Aplemont Le Havre Basket les entiers dépens sont rejetées.<br/>
<br/>
Article 3 : La présente ordonnance sera notifiée à l'amicale laïque Aplemont Le Havre Basket et à la Fédération française de basket-ball <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
