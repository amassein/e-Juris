<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037271434</ID>
<ANCIEN_ID>JG_L_2018_04_000000416172</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/14/CETATEXT000037271434.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 11/04/2018, 416172, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416172</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416172.20180411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société à responsabilité limitée (SARL) Les Jardins de Perce-Neige a demandé au tribunal administratif de Grenoble de prononcer la décharge des rappels de taxe sur les surfaces commerciales auxquelles elle a été assujettie au titre des années 2010 à 2013. Par un jugement n° 1505413 du 29 février 2016, le tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16LY01454 du 30 novembre 2017, enregistré le même jour au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Lyon a transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 28 avril 2016 au greffe de cette cour, formé par la société Les Jardins de Perce-Neige contre ce jugement en ce qui concerne les années 2011 à 2013.<br/>
<br/>
              Par ce pourvoi, un mémoire complémentaire, enregistré au greffe de cette cour le 28 octobre 2016, et deux nouveaux mémoires, enregistrés les 5 février et 9 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Les Jardins de Perce-Neige demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il se prononce sur les rappels dus au titre des années 2011 à 2013 ; <br/>
<br/>
              2°) réglant l'affaire au fond, à titre principal de faire droit à sa demande et subsidiairement, de prononcer la réduction de ces cotisations supplémentaires à concurrence de l'abattement de taux de 30 % prévu par le décret n° 95-85 du 26 janvier 1995 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Les Jardins de Perce-Neige.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 mars 2018, présentée par la société Les Jardins de Perce-Neige.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Les Jardins de Perce-Neige  exploite un commerce de détail de jardinerie spécialisée sous l'enseigne " Botanic ". A la suite de contrôles, l'administration fiscale l'a assujettie, au titre des années 2010 à 2013, à des rappels de taxe sur les surfaces commerciales procédant de la réintégration, dans les surfaces retenues dans l'assiette de la taxe, des serres chaudes abritant les plantes dont elle fait commerce, que l'administration a regardées comme des espaces de vente ouverts à la clientèle. La société Les Jardins de Perce-Neige  se pourvoit en cassation contre le jugement du 29 avril 2016 par lequel le tribunal administratif de Grenoble a, en ce qui concerne les années 2011 à 2013, rejeté sa demande tendant à la décharge de ces impositions. <br/>
<br/>
              En ce qui concerne la loi fiscale : <br/>
<br/>
              2. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse quatre cents mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe (...) s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente (...) ".<br/>
<br/>
              3. En premier lieu, après avoir relevé, par une appréciation souveraine non arguée de dénaturation, qu'il résultait de l'instruction, d'une part, que les serres chaudes en litige étaient affectées à la circulation de la clientèle pour effectuer ses achats et à l'exposition des marchandises proposées à la vente, d'autre part, qu'il n'était pas établi qu'elles seraient en partie utilisées comme un espace de maturation ou de production dans lequel se trouveraient des plantes exclues de la commercialisation, le tribunal n'a ni inexactement qualifié les faits de l'espèce, ni insuffisamment motivé son jugement en jugeant que, nonobstant la circonstance que des bouquets y seraient confectionnés à la demande des clients à partir des plantes achetées sur place, ces serres constituaient des surfaces de vente au sens de l'article 3 de la loi du 13 juillet 1972.<br/>
<br/>
              4. En second lieu, les moyens tirés, d'une part, de ce que la société requérante pourrait prétendre au bénéfice de la réduction de taux de 30 % prévue par l'article 3 de la loi du 13 juillet 1972 en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées dès lors que les plantes qu'elle vend auraient la nature de meubles meublants au sens des dispositions de l'article 3 du décret du 26 janvier 1995 pris pour son application, d'autre part, de ce que ce même décret méconnaîtrait les principes d'égalité devant la loi fiscale et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen en ce qu'il ne mentionnerait pas les activités de vente de fleurs, plantes, graines, engrais, animaux de compagnie et aliments pour ces animaux parmi celles bénéficiant de cette réduction de taux, qui ne sont pas d'ordre public, ne peuvent être utilement soulevés pour la première fois en cassation. <br/>
<br/>
              En ce qui concerne l'interprétation administrative de la loi fiscale :<br/>
<br/>
              5. Aux termes de l'article L. 80 A du livre des procédures fiscales : " Il ne sera procédé à aucun rehaussement d'impositions antérieures si la cause du rehaussement poursuivi par l'administration est un différend sur l'interprétation par le redevable de bonne foi du texte fiscal et s'il est démontré que l'interprétation sur laquelle est fondée la première décision a été, à l'époque, formellement admise par l'administration. / Lorsque le redevable a appliqué un texte fiscal selon l'interprétation que l'administration avait fait connaître par ses instructions ou circulaires publiées et qu'elle n'avait pas rapportée à la date des opérations en cause, elle ne peut poursuivre aucun rehaussement en soutenant une interprétation différente. Sont également opposables à l'administration, dans les mêmes conditions, les instructions ou circulaires publiées relatives au recouvrement de l'impôt et aux pénalités fiscales. ". La garantie prévue au premier alinéa de l'article L. 80 A s'applique également, aux termes du 1° de l'article L. 80 B du même livre, lorsque l'administration a formellement pris position sur l'appréciation d'une situation de fait au regard d'un texte fiscal. <br/>
<br/>
              6. En premier lieu, il résulte de la lettre même de ces dispositions que les contribuables ne peuvent, sur leur fondement, invoquer le bénéfice que des seules interprétations que donne l'administration de la loi fiscale, à l'exclusion des interprétations qu'elle donne de dispositions de nature non fiscale. Il s'en déduit que le tribunal administratif, dont le jugement est suffisamment motivé sur ce point, n'a pas commis d'erreur de droit en jugeant que la lettre du 13 avril 1995 par laquelle M. A..., alors directeur du commerce intérieur du ministère des entreprises et du développement économique, précisait au directeur de la fédération nationale des distributeurs spécialistes jardin que les serres chaudes installées dans les établissements de vente au détail de végétaux d'ornement pouvaient être exclues de l'assiette imposable à la taxe d'aide au commerce et à l'artisanat, ne pouvait être opposée à l'administration fiscale sur le fondement des dispositions des articles L. 80 A et L. 80 B du livre des procédures fiscales pour contester l'assujettissement à la taxe sur les surfaces commerciales des serres aux plantes de la société, dès lors que cette lettre concernait un prélèvement qui, à la date à laquelle elle a écrite, avait la nature d'une taxe parafiscale et non d'une imposition. <br/>
<br/>
              7. En deuxième lieu, en jugeant, après avoir relevé que la lettre du 13 avril 1995 ne constituait pas une prise de position formelle quant au traitement des serres chaudes au regard de l'assujettissement à la taxe sur les surfaces commerciales entrant dans le champ de la garantie contre les changements de doctrine de l'administration, que la requérante n'était pas, en tout état de cause, fondée à soutenir que la prise en compte de ses serres dans l'assiette de cette taxe porterait atteinte aux principes de sécurité juridique et de confiance légitime, le tribunal administratif n'a pas commis d'erreur de droit. Le moyen tiré de ce que la prise en compte des serres dans l'assiette de la taxe sur les surfaces commerciales méconnaîtrait, en outre, les stipulations de l'article premier du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui n'est pas d'ordre public, ne peut être utilement soulevé pour la première fois en cassation. <br/>
<br/>
              8. En troisième lieu, si la notice explicative 3350 Not SD relative à la taxe sur les surfaces commerciales disponible sur le portail internet " impots.gouv.fr " mentionne que " la surface à déclarer sur la ligne 02 s'entend des espaces clos et couverts affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement et ceux affectés à la circulation du personnel pour présenter les marchandises à la vente. Les surfaces des locaux de production, les serres chaudes et celles où sont réalisées les prestations de services, n'ont pas à être déclarées ", il ressort de la lettre même de ce paragraphe, dont la seconde phrase ne saurait être lue isolément de la première, que cette notice indique que seules les serres chaudes constituant des espaces de production et non des espaces clos et couverts affectés à la circulation de la clientèle pour effectuer ses achats ne doivent pas être prises en compte pour l'assujettissement à la taxe sur les surfaces commerciales. Cette notice ne contient, par suite et en tout état de cause, aucune interprétation de la loi fiscale dont le bénéfice est susceptible d'être invoqué par les contribuables sur le fondement des dispositions de l'article L. 80 A du livre des procédures fiscales. Il y a lieu de substituer ce motif à celui, tiré de ce que la notice explicative 3350 Not SD n'a fait l'objet d'aucune publication, retenu par le jugement attaqué, lequel est suffisamment motivé et dont il justifie légalement le dispositif. <br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi de la société Les Jardins de Perce-Neige ne peut qu'être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Les Jardins de Perce-Neige est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société à responsabilité limitée Les Jardins de Perce-Neige et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
