<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036927153</ID>
<ANCIEN_ID>JG_L_2018_05_000000411925</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/92/71/CETATEXT000036927153.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre jugeant seule, 18/05/2018, 411925, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411925</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP FOUSSARD, FROGER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411925.20180518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              La société L'immobilière groupe Casino et la société Distribution Casino France ont demandé au tribunal administratif de Toulouse d'annuler pour excès de pouvoir l'arrêté du 26 novembre 2013 par lequel le maire de Decazeville a délivré à la société civile de construction vente Decazeville développement un permis de construire en vue de la création d'un pôle commercial au sein de la zone d'aménagement concerté dite du " Centre ". Par un jugement n°s 1400393, 1402620 du 16 novembre 2016, le tribunal d'administratif de Toulouse a rejeté leur demande.<br/>
<br/>
              Par une ordonnance n° 17BX00107 du 27 avril 2017, le président de la 1ère chambre de la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société L'immobilière groupe Casino et la société Distribution Casino France contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 juin et 28 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société L'immobilière groupe Casino et la société Distribution Casino France demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
              2°) de renvoyer l'affaire à la cour administrative d'appel de Bordeaux ou, à titre subsidiaire, réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Decazeville et de la SCCV Decazeville développement la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2016-1480 du 2 novembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public. ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société L'immobilière groupe Casino et de la société Distribution Casino France, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la commune de Decazeville, et à la SCP Odent, Poulet, avocat de la SCCV Decazeville développement.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 26 novembre 2013, le maire de Decazeville a délivré à la société civile de construction vente Decazeville développement un permis de construire en vue de la création d'un pôle commercial d'une surface de plancher de 9 781 mètres carrés au sein de la zone d'aménagement concerté dite du " Centre ". Par un jugement du 16 novembre 2016, le tribunal administratif de Toulouse a rejeté comme irrecevable, pour défaut d'intérêt à agir, la demande des sociétés L'immobilière groupe Casino et Distribution Casino France tendant à l'annulation pour excès de pouvoir de ce permis de construire. Par une ordonnance du 27 avril 2017, prise sur le fondement du dernier alinéa de l'article R. 222-1 du code de justice administrative, le président de la 1ère chambre de la cour administrative d'appel de Bordeaux a rejeté comme manifestement dépourvu de fondement l'appel formé par ces sociétés contre le jugement du tribunal administratif de Toulouse.<br/>
<br/>
              2. Aux termes du dernier alinéa de l'article R. 222-1 du code de justice administrative, dans sa rédaction issue du décret du 2 novembre 2016 portant modification du code de justice administrative : " (...) les présidents des formations de jugement des cours peuvent (...) par ordonnance, rejeter (...), après l'expiration du délai de recours ou, lorsqu'un mémoire complémentaire a été annoncé, après la production de ce mémoire les requêtes d'appel manifestement dépourvues de fondement (...) ". Ces dispositions, qui ne modifient pas des éléments constitutifs du droit de former un appel contre une décision d'un tribunal administratif, étaient d'application immédiate aux instances en cours au 1er janvier 2017, date fixée pour leur entrée en vigueur par le I de l'article 35 du décret du 2 novembre 2016, y compris en cas d'appel formé contre un jugement ou une ordonnance intervenu avant cette date.<br/>
<br/>
              3. Il ressort toutefois des pièces de la procédure devant la cour que les mémoires en défense produits, respectivement, les 6 et 24 mars 2017 par la commune de Decazeville et la SCCV Decazeville développement ont été communiqués aux sociétés L'immobilière groupe Casino et Distribution Casino France, appelantes, avec l'indication selon laquelle " afin de ne pas retarder la mise en état d'être jugé de [leur] dossier, [elles avaient] tout intérêt, si [elles l'estimaient] utile, à produire [leurs] observations aussi rapidement que possible ". Ainsi, dès lors que, d'une part, une telle indication ne permettait pas à ces sociétés, en l'absence de date déterminée, de connaître le délai dans lequel elles étaient autorisées à produire leurs observations en réplique, et que, d'autre part, en l'absence d'audience, elles n'ont pas été mises en mesure d'exposer éventuellement celles-ci avant que le juge ne statue, les exigences du caractère contradictoire de la procédure ont été méconnues. Il suit de là que l'ordonnance attaquée, rendue à l'issue d'une procédure irrégulière, doit être annulée.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge tant de la commune de Decazeville que de la SCCV Decazeville développement le versement à la société L'immobilière groupe Casino et à la société Distribution Casino France d'une somme de 750 euros chacune au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'il soit fait droit aux conclusions de la commune de Decazeville et de la SCCV Decazeville développement présentées au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la 1ère chambre de la cour administrative d'appel de Bordeaux du 27 avril 2017 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La commune de Decazeville et la société civile de construction vente Decazeville développement verseront chacune une somme de 750 euros tant à la société L'immobilière groupe Casino qu'à la société Distribution Casino France au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Decazeville et de la société civile de construction vente Decazeville développement présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée, pour les sociétés requérantes, à la société L'immobilière groupe Casino, première dénommée, à la commune de Decazeville et à la société civile de construction vente Decazeville développement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
