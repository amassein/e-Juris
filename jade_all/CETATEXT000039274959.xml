<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274959</ID>
<ANCIEN_ID>JG_L_2019_10_000000430319</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 24/10/2019, 430319, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430319</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430319.20191024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, d'une part, d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 13 février 2018 par lequel le recteur de l'académie de Versailles l'a admise à la retraite pour invalidité à compter du 6 septembre 2017, ainsi que de la décision implicite de rejet née du silence gardé sur son recours gracieux du 23 avril 2018, et, d'autre part, d'enjoindre au recteur de l'académie de Versailles, à titre principal, de l'autoriser à reprendre ses fonctions ou de la reclasser dans un emploi équivalent à celui qu'elle occupait, dans un délai de deux mois à compter de la notification de l'ordonnance à intervenir, sous astreinte de 500 euros par jour de retard ou, à titre subsidiaire, de procéder à un nouvel examen de sa situation, dans un délai d'un mois à compter de la notification de l'ordonnance à intervenir, sous astreinte de 500 euros par jour de retard. <br/>
<br/>
              Par une ordonnance n° 1903792 du 15 avril 2019, le juge des référés du tribunal administratif de Cergy-Pontoise a fait droit à sa demande de suspension et a enjoint au ministre de l'éducation nationale de procéder à un nouvel examen de la situation de Mme A... et, le cas échéant, de prendre une nouvelle décision, dans un délai de deux mois à compter de la notification de son ordonnance.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 30 avril et 3 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale et de la jeunesse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de Mme A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision / (...) ". <br/>
<br/>
              2. Lorsque le juge des référés, saisi de conclusions tendant à la suspension d'une décision administrative, recherche si la condition d'urgence est remplie, il lui appartient de rapprocher, d'une part, les motifs invoqués par le requérant pour soutenir qu'il est satisfait à cette condition et, d'autre part, la diligence avec laquelle il a, par ailleurs, introduit ces conclusions. En l'absence de circonstances particulières tenant, notamment, à l'évolution de la situation de droit ou de fait postérieurement à l'introduction des conclusions d'annulation, ce rapprochement peut conduire le juge des référés à estimer que la demande de suspension ne satisfait pas à la condition d'urgence. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés que Mme A... n'a saisi le tribunal administratif d'une demande d'annulation de l'arrêté du 13 février 2018 décidant de sa mise à la retraite pour invalidité que le 15 octobre suivant et qu'elle n'en a demandé la suspension que le 25 mars 2019. En jugeant que la condition d'urgence posée par les dispositions précitées de l'article L. 521-1 du code de justice administrative était satisfaite en dépit du délai écoulé depuis l'édiction de l'arrêté attaqué sans rechercher si des circonstances particulières étaient susceptibles de le justifier, le juge des référés a commis une erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que le ministre de l'éducation nationale et de la jeunesse est fondé à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise qu'il attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1903792 du juge des référés du tribunal administratif de Cergy-Pontoise du 15 avril 2019 est annulée. <br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif de Cergy-Pontoise. <br/>
Article 3 : Les conclusions de Mme A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'éducation nationale et de la jeunesse et à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
