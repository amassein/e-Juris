<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032188954</ID>
<ANCIEN_ID>JG_L_2016_03_000000378129</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/89/CETATEXT000032188954.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème SSR, 09/03/2016, 378129</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378129</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:378129.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 26 décembre 2011 par laquelle le ministre du travail, de l'emploi et de la santé a autorisé son licenciement par la Fédération française d'éducation physique et de gymnastique volontaire (FFEPGV). Par un jugement n° 1201462 du 20 novembre 2012, le tribunal administratif a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 13VE00112-13VE00186 du 3 décembre 2013, la cour administrative d'appel de Versailles a, sur les appels du ministre du travail, de l'emploi et de la santé et de la FFEPGV, annulé ce jugement et rejeté la demande de MmeA.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 avril et 18 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les appels du ministre chargé du travail et de la FFEPGV ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à Me Occhipinti au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de Mme A...et à la SCP de Nervo, Poupet, avocat de la Fédération française d'éducation physique et de gymnastique volontaire ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., qui exerçait des fonctions d'assistante de communication de la Fédération française d'éducation physique et de gymnastique volontaire et avait la qualité de déléguée du personnel, a fait l'objet d'une demande d'autorisation de licenciement par son employeur en raison de ses absences répétées pour maladie ; que cette autorisation a été accordée par une décision du ministre chargé du travail du 26 décembre 2011 ; que Mme A...se pourvoit en cassation contre l'arrêt du 3 décembre 2013 par lequel la cour administrative d'appel de Versailles, après avoir annulé le jugement du 20 novembre 2012 du tribunal administratif de Montreuil, a rejeté sa demande d'annulation de cette décision ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; que lorsque le licenciement de l'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou avec l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est fondée sur des absences prolongées ou répétées, pour maladie, du salarié, il incombe à l'inspecteur du travail et, le cas échéant, au ministre compétent de rechercher, sous le contrôle du juge de l'excès de pouvoir, si, eu égard à la nature des fonctions de l'intéressé et aux règles applicables à son contrat, ses absences apportent au fonctionnement de l'entreprise des perturbations suffisamment graves que l'employeur ne peut pallier par des mesures provisoires et qui sont dès lors de nature à justifier le licenciement en vue de son remplacement définitif par le recrutement d'un autre salarié ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui a été dit ci-dessus que, après avoir relevé que la Fédération française d'éducation physique et de gymnastique volontaire avait demandé l'autorisation de licencier Mme A...en raison des conséquences de ses absences répétées sur le fonctionnement de l'entreprise, la cour administrative d'appel de Versailles a pu, sans méconnaître la portée des obligations définies au point 2 ci-dessus ni commettre d'erreur de droit, juger, par un arrêt suffisamment motivé, que la fédération n'était pas tenue de rechercher un poste permettant le reclassement de Mme A...;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que les conclusions de son pourvoi doivent par suite être rejetées, y compris, l'Etat n'étant pas la partie perdante dans la présente instance, celles qu'elle présente au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la Fédération française d'éducation physique et de gymnastique volontaire.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-035-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. MOTIFS AUTRES QUE LA FAUTE OU LA SITUATION ÉCONOMIQUE. INAPTITUDE ; MALADIE. - DEMANDE DE LICENCIEMENT FONDÉE SUR DES ABSENCES PROLONGÉES OU RÉPÉTÉES POUR MALADIE DU SALARIÉ - OFFICE DE L'AUTORITÉ ADMINISTRATIVE [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-04-035-02 Dans le cas où la demande de licenciement est fondée sur des absences prolongées ou répétées, pour maladie, du salarié, il incombe à l'inspecteur du travail et, le cas échéant, au ministre compétent de rechercher, sous le contrôle du juge de l'excès de pouvoir, si, eu égard à la nature des fonctions de l'intéressé et aux règles applicables à son contrat, ses absences apportent au fonctionnement de l'entreprise des perturbations suffisamment graves que l'employeur ne peut pallier par des mesures provisoires et qui sont dès lors de nature à justifier le licenciement en vue de son remplacement définitif  par le recrutement d'un autre salarié.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., en précisant, CE, 21 octobre 1996, Maube, n° 111961, T. p. 1192.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
