<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018003339</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/33/CETATEXT000018003339.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour administrative d'appel de Douai, 1re chambre - formation à 3 (bis), 08/11/2006, 05DA01494, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2006-11-08</DATE_DEC>
<JURIDICTION>Cour administrative d'appel de Douai</JURIDICTION>
<NUMERO>05DA01494</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1re chambre - formation à 3 (bis)</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Tricot</PRESIDENT>
<AVOCATS>DANNAY</AVOCATS>
<RAPPORTEUR>M. Olivier (AC)  Yeznikian</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lepers</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 décembre 2005, présentée pour M. Clément X, demeurant ..., par Me Donnay ; M. X demande à la Cour : 
<br/>
<br/>

      11) d'annuler le jugement n° 0401380 en date du 13 octobre 2005 par lequel le Tribunal administratif d'Amiens a rejeté sa demande tendant à l'annulation, d'une part, de la décision en date du 11 mai 2004 par laquelle le ministre de l'intérieur lui a retiré deux points de son titre de conduite et lui a notifié la perte de validité de ce dernier pour solde de points nuls et, d'autre part, de la décision en date du 11 juin 2004 par laquelle le Préfet de la Somme lui a enjoint de restituer son permis de conduire ;
<br/>
<br/>

      2°) d'annuler lesdites décisions ;
<br/>
<br/>
<br/>

      Il soutient que les dispositions de l'article L. 123-1 du code de la route n'ont pas été respectées en l'espèce ; que le Tribunal administratif d'Amiens a dénaturé l'argumentation de sa demande et qu'il est en totale contradiction avec la position du ministre de l'intérieur, de la sécurité intérieure et des libertés locales, qui rejoignait la sienne ; que le ministre l'informait en effet que son permis avait recouvré sa validité à la suite du défaut d'information préalable lors de la constatation des infractions commises les 31 mai 1994, 8 septembre 1994, 19 juin 1997, 27 janvier 1998 et 5 octobre 1999 ; que la décision ministérielle ne rapporte pas la preuve de la matérialité de la perte de l'intégralité des points affectée à son permis de conduire ;
<br/>
<br/>

      Vu le jugement et la décision attaqués ;
<br/>

      Vu l'ordonnance en date du 9 janvier 2006 du président de la 1ère chambre portant clôture de l'instruction au 31 mars 2006 ;
<br/>
<br/>

      Vu le mémoire en défense, enregistré le 27 mars 2006, présenté par le ministre de l'intérieur et de l'aménagement du territoire qui conclut à ce que la Cour prononce un non-lieu à statuer sur la requête ; le ministre fait valoir que le permis de conduire de l'intéressé a retrouvé sa validité du fait de la restitution de points et qu'il en a informé M. X par lettre en date du 29 avril 2005 jointe à la requête ;
<br/>
<br/>

      Vu les éléments attestant de la transmission, le 28 mars 2006, du mémoire du ministre pour production d'une réplique au conseil de M. X ;
<br/>
<br/>

      Vu les autres pièces du dossier ;
<br/>
<br/>

      Vu le code de la route ;
<br/>
<br/>

      Vu le code de justice administrative ;
<br/>
<br/>
<br/>

      Les parties ayant été régulièrement averties du jour de l'audience ;
<br/>
<br/>

      Après avoir entendu au cours de l'audience publique du 19 octobre 2006 à laquelle siégeaient 
<br/>

Mme Christiane Tricot, président de chambre, M. Olivier Yeznikian, président-assesseur et  ;
<br/>
<br/>

      - le rapport de M. Olivier Yeznikian, président-assesseur,
<br/>
<br/>

      - et les conclusions de M. Jacques Lepers, commissaire du gouvernement ;
<br/>
<br/>
<br/>
<br/>

      Considérant que, par lettre recommandée en date du 11 mai 2004, le ministre de l'intérieur, de la sécurité intérieure et des libertés locales a prononcé le retrait de deux points correspondant à l'infraction commise par M. X le 6 février 2004 et a informé ce dernier de la perte de validité de son permis de conduire compte tenu des infractions précédemment commises les 31 mai 1994, 8 septembre 1994, 19 juin 1997, 27 janvier 1998, 5 octobre 1999, 18 avril 2001 et 3 avril 2002, ayant donné lieu à des retraits d'un total de dix-huit points ; que, par décision en date du 11 juin 2004, le Préfet de la Somme a enjoint à M. X de restituer son permis de conduire ; 
<br/>
<br/>
<br/>

      Sur la régularité du jugement : 
<br/>
<br/>

      Considérant qu'il ressort des pièces du dossier qu'au cours de l'instruction devant le Tribunal administratif d'Amiens, le ministre de l'intérieur a prononcé le retrait de celles de ses décisions retirant plusieurs points au titre de conduite de M. X à la suite des infractions commises par ce dernier les 31 mai 1994, 8 septembre 1994, 19 juin 1997, 27 janvier 1998 et 5 octobre 1999 ; que, par suite, les conclusions tendant à l'annulation des décisions ministérielles de retrait de points correspondant à ces infractions, récapitulées le 11 mai 2004, étant dans cette mesure devenues sans objet, le tribunal administratif, en rejetant, dans la même mesure, la demande de M. X, a entaché son jugement d'irrégularité ; qu'il y a lieu, pour la Cour, d'annuler, dans la mesure précitée, le jugement attaqué, d'évoquer les conclusions devenues sans objet au cours de la procédure de première instance et de décider qu'il n'y a pas lieu d'y statuer ;
<br/>
<br/>
<br/>

      Sur le surplus des conclusions de la requête :
<br/>
<br/>

      Considérant qu'en vertu des dispositions de l'article L. 223-1 du code de la route, le permis de conduire est affecté d'un nombre de points, celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue ; que les mêmes dispositions précisent que la réalité d'une infraction entraînant un retrait de points est établie par le paiement d'une amende forfaitaire, l'exécution d'une composition pénale, par une condamnation définitive ou également, dans leur version résultant de la loi n° 2003-495 du 12 juin 2003, par l'émission du titre exécutoire de l'amende forfaitaire majorée ;
<br/>
<br/>

      Considérant qu'il ressort des pièces du dossier, et notamment de la lettre récapitulative des infractions, en date du 11 mai 2004, que l'intéressé a réglé l'amende forfaitaire pour les infractions encore en litige commises les 3 avril 2002 et 6 février 2004 ;  que l'intéressé, qui ne conteste pas, par ailleurs, la réalité de l'infraction commise le 18 avril 2001, n'apporte, au cours de la procédure juridictionnelle aucun élément permettant de constater qu'il aurait, par la suite et dans le délai légal, contesté les autres infractions en litige et renoncé à payer l'amende forfaitaire correspondante ; que, par suite, M. X n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif d'Amiens a rejeté les conclusions tendant à l'annulation des décisions ministérielles de retrait de points correspondant aux infractions commises les 18 avril 2001, 3 avril 2002 et 6 février 2004 ; que, toutefois, le nombre de points retirés à cette occasion s'élevant à un total de neuf points, le permis de conduire de M. X n'avait pas perdu  sa validité lorsque le préfet de la Somme lui a, par sa décision du 11 juin 2004, enjoint de le restituer ; que, par suite, M. X est fondé à soutenir que c'est à tort que, par le même jugement, le Tribunal administratif d'Amiens a rejeté sa demande tendant à l'annulation de cette décision préfectorale ; 
<br/>
<br/>
<br/>

      DÉCIDE :
<br/>
<br/>
<br/>

      Article 1er : Le jugement n° 0401380 du Tribunal administratif d'Amiens du 13 octobre 2005 est annulé, en tant, d'une part, qu'il a omis de prononcer un non-lieu sur les conclusions de la demande de M. X tendant à l'annulation des décisions ministérielles de retrait de points prononcées à la suite des infractions des 31 mai 1994, 8 septembre 1994, 19 juin 1997, 27 janvier 1998, et 5 octobre 1999, et en tant, d'autre part, qu'il a rejeté les conclusions de M. X tendant à l'annulation de la décision du 11 juin 2004 du préfet de l'Oise lui enjoignant de restituer son permis  de conduire. 
<br/>
<br/>

      Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la demande M. X relative aux décisions ministérielles de retrait de points correspondant aux infractions mentionnées à l'article 1er.
<br/>

      Article 3 : La décision du préfet de la Somme en date du 11 juin 2004 est annulée.
<br/>
<br/>

      Article 4 : Le surplus des conclusions de la requête de M. X est rejeté.
<br/>
<br/>

      Article 5 : Le présent arrêt sera notifié à M. Clément X ainsi qu'au ministre d'Etat, ministre de l'intérieur et de l'aménagement du territoire.
<br/>
<br/>
<br/>

      Copie sera transmise au préfet de la Somme. 
<br/>
<br/>
<br/>
<br/>

N°05DA01494	2
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
