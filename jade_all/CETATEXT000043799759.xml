<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799759</ID>
<ANCIEN_ID>JG_L_2021_07_000000440885</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799759.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 09/07/2021, 440885, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440885</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:440885.20210709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Chemin du Genêt a demandé au tribunal administratif de Montpellier de prononcer la décharge des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie au titre de la période du 1er janvier au 31 décembre 2014 et des pénalités correspondantes. Par un jugement n° 1703859 du 25 juin 2018, ce tribunal a prononcé la décharge de ces impositions.<br/>
<br/>
              Par un arrêt n° 18MA04544 du 17 mars 2020, la cour administrative d'appel de Marseille a rejeté l'appel formé par le ministre de l'action et des comptes publics contre ce jugement.  <br/>
<br/>
              Par un pourvoi, enregistré le 27 mai 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2006/112/CE du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament - Robillot, avocat de la société Chemin du Genêt ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Chemin du Genêt, qui exerce une activité de marchand de biens, a procédé à la cession, comme terrains à bâtir, de trois parcelles, situées sur le territoire de la commune de Sète (Hérault), résultant de la division d'une parcelle sur laquelle était édifiée, à la date de l'acquisition, un immeuble d'habitation que la société a fait démolir préalablement à la division et à la vente. Elle a, dans les déclarations qu'elle a souscrites au titre de la taxe sur la valeur ajoutée, estimé pouvoir faire application à ces opérations du régime de la taxe sur la valeur ajoutée sur la marge. La société a fait l'objet d'une vérification de comptabilité à l'issue de laquelle des rappels de taxe sur la valeur ajoutée ont été mis à sa charge au titre de la période du 1er janvier au 31 décembre 2014, procédant de la remise en cause du régime de la taxe sur la valeur ajoutée sur la marge qu'elle avait appliqué. Par un jugement du 25 juin 2018, le tribunal administratif de Montpellier a prononcé la décharge de ces rappels de taxe sur la valeur ajoutée ainsi que des pénalités correspondantes. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 17 mars 2020 par laquelle la cour administrative d'appel de Marseille a rejeté l'appel qu'il avait formé contre ce jugement.<br/>
<br/>
              2. Le I de l'article 257 du code général des impôts dans sa rédaction applicable, issue de l'article 16 de la loi du 9 mars 2010 de finances rectificative pour 2010, prévoit que les opérations concourant à la production ou à la livraison d'immeubles, lesquelles comprennent les livraisons à titre onéreux de terrains à bâtir, sont soumises à la taxe sur la valeur ajoutée. En vertu du 2 du b de l'article 266 du même code, l'assiette de la taxe est en principe constituée par le prix de cession.<br/>
<br/>
              3. L'article 392 de la directive du Conseil du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée dispose toutefois que : " Les États membres peuvent prévoir que, pour les livraisons de bâtiments et de terrains à bâtir achetés en vue de la revente par un assujetti qui n'a pas eu droit à déduction à l'occasion de l'acquisition, la base d'imposition est constituée par la différence entre le prix de vente et le prix d'achat ". L'article 268 du code général des impôts, pris pour la transposition de ces dispositions, prévoit, dans sa rédaction également issue de l'article 16 de la loi du 9 mars 2010 de finances rectificative pour 2010, que : " S'agissant de la livraison d'un terrain à bâtir (...), si l'acquisition par le cédant n'a pas ouvert droit à déduction de la taxe sur la valeur ajoutée, la base d'imposition est constituée par la différence entre : / 1° D'une part, le prix exprimé et les charges qui s'y ajoutent ; / 2° D'autre part, selon le cas : / - soit les sommes que le cédant a versées, à quelque titre que ce soit, pour l'acquisition du terrain(...); / - soit la valeur nominale des actions ou parts reçues en contrepartie des apports en nature qu'il a effectués ".<br/>
<br/>
              4. Il résulte de ces dernières dispositions, lues à la lumière de celles de la directive dont elles ont pour objet d'assurer la transposition, que les règles de calcul dérogatoires de la taxe sur la valeur ajoutée qu'elles prévoient s'appliquent aux opérations de cession de terrains à bâtir qui ont été acquis en vue de leur revente et ne s'appliquent donc pas à une cession de terrains à bâtir qui, lors de leur acquisition, avaient le caractère d'un terrain bâti, notamment quand le bâtiment qui y était édifié a fait l'objet d'une démolition de la part de l'acheteur-revendeur ou quand le bien acquis a fait l'objet d'une division parcellaire en vue d'en céder séparément des parties ne constituant pas le terrain d'assiette du bâtiment.<br/>
<br/>
              5. Il suit de là que la cour administrative d'appel a commis une erreur de droit en jugeant qu'il résultait des dispositions des articles 268 du code général des impôts et 392 de la directive du 28 novembre 2006 que le bénéfice du régime de la taxe sur la valeur ajoutée sur la marge était subordonné à la seule condition que l'acquisition du bien cédé n'ait pas ouvert droit à déduction de la taxe et en jugeant sans incidence sur sa mise en oeuvre la circonstance que les caractéristiques physiques et la qualification du bien en cause aient été modifiées entre son acquisition et sa vente.<br/>
<br/>
              6. Par suite, le ministre de l'action et des comptes publics est fondé à demander, pour ce motif, l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 17 mars 2020 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société à responsabilité limitée Chemin du Genêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
