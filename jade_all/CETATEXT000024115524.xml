<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024115524</ID>
<ANCIEN_ID>JG_L_2011_05_000000336838</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/11/55/CETATEXT000024115524.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 30/05/2011, 336838</TITRE>
<DATE_DEC>2011-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336838</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Christophe Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:336838.20110530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 février et 23 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant ... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NT00365 du 18 décembre 2009 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement n°s 08-4165 et 08-4204 du 29 janvier 2009 du tribunal administratif de Rennes qui avait rejeté ses demandes tendant à l'annulation pour excès de pouvoir de l'arrêté du 12 septembre 2008 du préfet d'Ille-et-Vilaine décidant sa suspension du droit d'exercer la médecine pendant une durée maximale de cinq mois, ainsi que de ce cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 2 400 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Eoche-Duval, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Le Bret-Desaché, avocat de M.A..., <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Le Bret-Desaché, avocat de M. A..., <br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 4113-14 du code de la santé publique, dans sa version alors en vigueur : " En cas d'urgence, lorsque la poursuite de son exercice par un médecin (...) expose ses patients à un danger grave, le représentant de l'Etat dans le département prononce la suspension immédiate du droit d'exercer pour une durée maximale de cinq mois. (...) Le représentant de l'Etat dans le département informe immédiatement de sa décision le président du conseil départemental compétent et saisit sans délai le conseil régional ou interrégional lorsque le danger est lié à une infirmité ou un état pathologique du professionnel, ou la chambre disciplinaire de première instance dans les autres cas. Le conseil régional ou interrégional ou la chambre disciplinaire de première instance statue dans un délai de deux mois à compter de sa saisine. En l'absence de décision dans ce délai, l'affaire est portée devant le Conseil national ou la Chambre disciplinaire nationale, qui statue dans un délai de deux mois. A défaut de décision dans ce délai, la mesure de suspension prend fin automatiquement. (...) Le médecin, le chirurgien-dentiste ou la sage-femme dont le droit d'exercer a été suspendu selon la procédure prévue au présent article peut exercer un recours contre la décision du représentant de l'Etat dans le département devant le tribunal administratif, qui statue en référé dans un délai de quarante-huit heures. / Les modalités d'application du présent article sont définies par décret en Conseil d'Etat (...) " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet d'Ille-et-Vilaine a pris le 23 juin 2008, sur le fondement de ces dispositions, une mesure de suspension du droit d'exercer la médecine pendant " une durée maximale de cinq mois " à l'encontre de M.A..., médecin effectuant des remplacements chez des confrères radiologues ; que ce premier arrêté a été annulé pour vice de forme par un arrêt de la cour administrative d'appel de Nantes du 29 août 2008 ; que, le 12 septembre 2008, le préfet a pris à l'encontre de ce praticien un nouvel arrêté de suspension pour la même durée maximale de cinq mois ; que, par l'arrêt attaqué, la cour administrative d'appel de Nantes a confirmé le jugement du 29 janvier 2009 par lequel le tribunal administratif de Rennes avait rejeté sa demande tendant à l'annulation de cette dernière mesure ; <br/>
<br/>
              Considérant, en premier lieu, qu'à défaut de toute précision dans la loi en ce qui concerne, notamment, la nature des pouvoirs du juge des référés dont l'intervention est prévue par les dispositions citées ci-dessus de l'article L. 4113-14 du code de la santé publique, ces dispositions n'ont pu entrer en vigueur, en l'absence de définition de leurs modalités d'application par le décret en Conseil d'Etat prévu à l'avant-dernier alinéa de cet article ; qu'ainsi et en tout état de cause, la cour administrative d'appel de Nantes était compétente pour connaître du jugement du tribunal administratif de Rennes se prononçant sur le recours exercé par M. A... contre la mesure de suspension prise à son encontre ;<br/>
<br/>
              Considérant, en deuxième lieu, que l'allégation du requérant selon laquelle les témoignages de ses confrères étaient uniquement animés par l'intention de lui nuire ne peut être regardée comme un fait, au sens des dispositions de l'article R. 612-6 du code de justice administrative, auquel l'administration, qui n'avait pas produit de mémoire malgré la mise en demeure qui lui avait été adressée par la cour, aurait acquiescé en application de ces dispositions ; que par suite, en jugeant que cette allégation n'était pas établie, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, en troisième lieu, qu'en énonçant que la première mesure de suspension du droit, pour M.A..., d'exercer la médecine pour une durée maximale de cinq mois, annulée en raison de l'insuffisance de sa motivation, ne faisait pas obstacle " par elle-même " à ce que le préfet prenne à l'encontre de l'intéressé une nouvelle mesure de suspension ayant la même durée, la cour administrative d'appel n'a pas commis d'erreur de droit ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A...et au ministre du travail, de l'emploi et de la santé.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 336838- 2 -<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. ENTRÉE EN VIGUEUR SUBORDONNÉE À L'INTERVENTION DE MESURES D'APPLICATION. - ARTICLE L. 4113-14 DU CODE DE LA SANTÉ PUBLIQUE PRÉVOYANT UNE POSSIBILITÉ DE SAISINE DU JUGE DES RÉFÉRÉS PAR LES MÉDECINS, CHIRURGIENS-DENTISTES OU SAGES FEMMES POUR CONTESTER LA DÉCISION DE SUSPENSION DU DROIT D'EXERCER PRISE À LEUR ENCONTRE - ENTRÉE EN VIGUEUR, EN L'ABSENCE DE DÉCRET D'APPLICATION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-03 PROCÉDURE. PROCÉDURES D'URGENCE. - ARTICLE L. 4113-14 DU CODE DE LA SANTÉ PUBLIQUE PRÉVOYANT UNE POSSIBILITÉ DE SAISINE DU JUGE DES RÉFÉRÉS PAR LES MÉDECINS, CHIRURGIENS-DENTISTES OU SAGES FEMMES POUR CONTESTER LA DÉCISION DE SUSPENSION DU DROIT D'EXERCER PRISE À LEUR ENCONTRE - ENTRÉE EN VIGUEUR, EN L'ABSENCE DE DÉCRET D'APPLICATION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-04-04 PROCÉDURE. INSTRUCTION. PREUVE. - ACQUIESCEMENT AUX FAITS - NOTION DE FAIT - EXCLUSION - EXISTENCE D'UNE INTENTION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">55-01-02 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. - ARTICLE L. 4113-14 DU CODE DE LA SANTÉ PUBLIQUE PRÉVOYANT UNE POSSIBILITÉ DE SAISINE DU JUGE DES RÉFÉRÉS PAR LES MÉDECINS, CHIRURGIENS-DENTISTES OU SAGES FEMMES POUR CONTESTER LA DÉCISION DE SUSPENSION DU DROIT D'EXERCER PRISE À LEUR ENCONTRE - ENTRÉE EN VIGUEUR, EN L'ABSENCE DE DÉCRET D'APPLICATION - ABSENCE.
</SCT>
<ANA ID="9A"> 01-08-01-02 Article L. 4113-14 du code de la santé publique dont l'avant-dernier alinéa prévoit que le médecin, le chirurgien-dentiste ou la sage-femme dont le droit d'exercer a été suspendu selon la procédure prévue à cet article peut exercer un recours contre la décision du représentant de l'Etat dans le département devant le tribunal administratif, qui statue en référé dans un délai de quarante-huit heures, la définition des modalités d'application de cet article étant renvoyée à un décret en Conseil d'Etat. A défaut de toute précision dans la loi en ce qui concerne, notamment, la nature des pouvoirs du juge des référés, ces dispositions n'ont pu entrer en vigueur, en l'absence de définition de leurs modalités d'application par le décret en Conseil d'Etat prévu.</ANA>
<ANA ID="9B"> 54-03 Article L. 4113-14 du code de la santé publique dont l'avant-dernier alinéa prévoit que le médecin, le chirurgien-dentiste ou la sage-femme dont le droit d'exercer a été suspendu selon la procédure prévue à cet article peut exercer un recours contre la décision du représentant de l'Etat dans le département devant le tribunal administratif, qui statue en référé dans un délai de quarante-huit heures, la définition des modalités d'application de cet article étant renvoyée à un décret en Conseil d'Etat. A défaut de toute précision dans la loi en ce qui concerne, notamment, la nature des pouvoirs du juge des référés, ces dispositions n'ont pu entrer en vigueur, en l'absence de définition de leurs modalités d'application par le décret en Conseil d'Etat prévu.</ANA>
<ANA ID="9C"> 54-04-04 Les allégations d'une partie prêtant aux auteurs de témoignages produits au dossier une intention de lui nuire ne peuvent être regardées comme un fait, au sens des dispositions de l'article R. 612-6 du code de justice administrative, auquel l'administration, qui n'avait pas produit de mémoire malgré la mise en demeure qui lui avait été adressée par la cour, aurait acquiescé en application de ces dispositions.</ANA>
<ANA ID="9D"> 55-01-02 Article L. 4113-14 du code de la santé publique dont l'avant-dernier alinéa prévoit que le médecin, le chirurgien-dentiste ou la sage-femme dont le droit d'exercer a été suspendu selon la procédure prévue à cet article peut exercer un recours contre la décision du représentant de l'Etat dans le département devant le tribunal administratif, qui statue en référé dans un délai de quarante-huit heures, la définition des modalités d'application de cet article étant renvoyée à un décret en Conseil d'Etat. A défaut de toute précision dans la loi en ce qui concerne, notamment, la nature des pouvoirs du juge des référés, ces dispositions n'ont pu entrer en vigueur, en l'absence de définition de leurs modalités d'application par le décret en Conseil d'Etat prévu.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
