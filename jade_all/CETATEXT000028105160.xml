<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028105160</ID>
<ANCIEN_ID>JG_L_2013_10_000000372519</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/10/51/CETATEXT000028105160.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/10/2013, 372519, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372519</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:372519.20131015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 1er octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Pierre Fabre Médicament, dont le siège social est situé 45, place Abel Gance à Boulogne-Billancourt (92100) ; la société requérante demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté interministériel du 30 septembre 2011 portant radiation de la spécialité Structum 500 mg de la liste des médicaments remboursables aux assurés sociaux prévue au premier alinéa de l'article L. 162-17 du code de la sécurité sociale ;  <br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de l'arrêté interministériel du 30 septembre 2011 portant radiation à compter du 1er décembre 2011 de la spécialité Structum 500 mg de la liste des médicaments agréés à l'usage des collectivités publiques mentionnée à l'article L. 5123-2 du code de la santé publique ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              elle soutient que :<br/>
              - constituent de nouvelles circonstances de fait, d'une part, les avis de la commission de la transparence du 9 janvier 2013 dont il résulte qu'il ne demeure aucune différence entre les spécialités Structum et Chondrosulf qui se trouvent dans une situation identique et, d'autre part, l'adoption des arrêtés interministériels du 31 mai 2013 portant radiation de spécialités pharmaceutiques de la liste des médicaments agréés à l'usage des collectivités publiques prévue à l'article L. 5123-2 du code de la santé publique ainsi que la suspension de leur exécution concernant trois spécialités par l'effet d'ordonnances du juge des référés du Conseil d'Etat ; <br/>
              - la condition d'urgence est remplie dès lors que, en premier lieu, l'exécution de l'arrêté interministériel a entraîné une perte de chiffre d'affaires pour la société requérante évaluée à 35 670 286 euros sur la période d'octobre 2011 à juillet 2013 ; en deuxième lieu, l'exécution des arrêtés contestés a provoqué une altération significative de la concurrence sur le marché des anti-arthrosiques symptomatiques d'action lente (AASAL, hors glucosamines) ; en dernier lieu, l'intérêt public d'une bonne administration de la justice consiste à ce qu'il soit statué dans le même sens dans des situations strictement comparables ; <br/>
              - il existe un doute quant à la légalité des arrêtés contestés ; <br/>
              - ils méconnaissent les dispositions du IV de l'article R. 163-6 du code de la sécurité sociale en ce que la radiation de la spécialité Structum est intervenue avant que la commission de la transparence ne rende son avis sur les autres spécialités de la même classe et sur les AASAL à base de glucosamine ;  <br/>
              - la procédure de radiation est irrégulière dès lors qu'elle n'a pas respecté le droit de la société requérante à une procédure contradictoire ;  <br/>
              - ils méconnaissent le principe d'égalité dès lors qu'ils instituent une différence de traitement entre Structum et les autres AASAL ;<br/>
              - ils portent atteinte au principe de libre concurrence sans justification au regard de l'intérêt général ; <br/>
<br/>
<br/>
      Vu les arrêtés dont la suspension de l'exécution est demandée ; <br/>
<br/>
      Vu la copie de la requête à fin d'annulation des arrêtés contestés ;<br/>
      Vu les autres pièces du dossier ; <br/>
<br/>
      Vu le code de la santé publique ; <br/>
<br/>
      Vu le code de la sécurité sociale ; <br/>
      Vu le code de justice administrative ;		<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du code de justice administrative, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative ;<br/>
<br/>
              2. Considérant que le ministre du travail, de l'emploi et de la santé et le ministre du budget, des comptes publics et de la réforme de l'État, porte-parole du gouvernement, ont, par deux arrêtés du 30 septembre 2011, radié la spécialité Structum, anti-arthrosique symptomatique d'action lente, à compter du 1er décembre 2011, respectivement de la liste des médicaments remboursables aux assurés sociaux et de la liste des médicaments agréés à l'usage des collectivités publiques et divers services publics ; que, par une ordonnance du 30 novembre 2011, le juge des référés du Conseil d'Etat a rejeté une première demande de suspension de ces arrêtés par la société requérante, faute que soit remplie la condition d'urgence exigée par les dispositions citées ci-dessus, en raison notamment de la part relativement faible de cette spécialité dans son chiffre d'affaires ; <br/>
<br/>
              3. Considérant qu'en raison, d'une part, de l'absence d'éléments nouveaux relatifs à la condition d'urgence, tel n'étant le cas ni de la suspension par le juge des référés du Conseil d'Etat de mesures de radiation de spécialités de la même classe thérapeutique, ni de l'identité alléguée entre le Structum et une autre de ces spécialités, et, d'autre part, de l'intervention prochaine de la décision du Conseil d'Etat sur la requête en annulation présentée par la société requérante, cette condition ne peut être regardée comme remplie ; que, par suite, la nouvelle demande de suspension des arrêtés du 30 septembre 2011 doit être rejetée selon la procédure prévue à l'article L. 522-3 du code de justice administrative ainsi que les conclusions présentées sur le fondement de l'article L. 761-1 du même code ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Pierre Fabre médicament est rejetée. <br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à la société Pierre Fabre médicament.<br/>
Copie pour information sera adressée au ministre des affaires sociales et à la Haute autorité de santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
