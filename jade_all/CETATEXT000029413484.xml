<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029413484</ID>
<ANCIEN_ID>JG_L_2014_08_000000372022</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/41/34/CETATEXT000029413484.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 27/08/2014, 372022, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372022</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:372022.20140827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir l'arrêté du 22 février 2010 par lequel le centre d'action sociale de la ville de Paris (CASVP) l'a placée en position de disponibilité d'office pour inaptitude physique à compter du 15 décembre 2009 et, d'autre part, d'ordonner sa réintégration. Par un jugement n° 1004787 du 29 février 2012, le tribunal administratif de Paris a annulé cet arrêté et a enjoint au directeur général du CASVP de procéder à la réintégration de Mme B...dans ses fonctions. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un arrêt n° 12PA01829 du  3 septembre 2013, enregistré le 9 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 25 avril 2012 au greffe de cette cour, présenté par le centre d'action sociale de la ville de Paris. Par ce pourvoi, le centre d'action sociale de la ville de Paris demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement n° 1004787 du tribunal administratif de Paris du 29 février 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de MmeB... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
              - le décret n° 86-68 du 13 janvier 1986 ; <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat du centre d'action sociale de la ville de Paris.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que MmeB..., agent social du centre d'action sociale de la ville de Paris, a bénéficié d'un congé parental à compter du 18 janvier 2006, renouvelé jusqu'au 21 octobre 2006, puis du 16 mai 2007 jusqu'au 15 novembre 2009, prolongé, sur sa demande, jusqu'au 14 décembre 2009. Par un arrêté du 22 février 2010, le centre d'action sociale de la ville de Paris l'a placée en position de disponibilité d'office pour inaptitude physique à compter du 15 décembre 2009. Le centre d'action sociale de la ville de Paris se pourvoit en cassation contre le jugement du 29 février 2012 par lequel le tribunal administratif de Paris a annulé cet arrêté et ordonné la réintégration de MmeB....<br/>
<br/>
              2. Les écritures de MmeB..., qui ont été présentées sans le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, bien que l'intéressée ait été informée de l'obligation de recourir à ce ministère, doivent être écartées des débats.<br/>
<br/>
              3. En premier lieu, le moyen tiré de ce que les visas du jugement du tribunal administratif de Paris ne comporteraient pas l'analyse des moyens soulevés par les parties manque en fait. <br/>
<br/>
              4. En deuxième lieu, il ressort des écritures de Mme B...devant le tribunal que celle-ci demandait aux premiers juges d'annuler l'arrêté du 22 février 2010 et d'enjoindre au centre d'action sociale de la ville de Paris, sur le fondement de l'article L. 911-1 du code de justice administrative, de la réintégrer. Par suite, le centre d'action sociale de la ville de Paris n'est pas fondé à soutenir que le tribunal aurait statué au-delà des conclusions dont il était saisi.<br/>
<br/>
              5. En troisième lieu, aux termes des dispositions de l'article 72 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " La disponibilité est la position du fonctionnaire qui, placé hors de son administration ou service d'origine, cesse de bénéficier, dans cette position, de ses droits à l'avancement et à la retraite. / La disponibilité est prononcée, soit à la demande de l'intéressé, soit d'office à l'expiration des congés prévus aux 2°, 3° et 4° de l'article 57 (...) ". Aux termes du premier alinéa de l'article 19 du décret du 13 janvier 1986 relatif aux positions de détachement, hors cadres, de disponibilité et de congé parental des fonctionnaires territoriaux, dans sa rédaction applicable à la date de la décision litigieuse : " La mise en disponibilité peut être prononcée d'office à l'expiration des droits statutaires à congés de maladie prévus à l'article 57 (2°, 3° et 4°) de la loi du 26 janvier 1984 et s'il ne peut, dans l'immédiat, être procédé au reclassement du fonctionnaire dans les conditions prévues aux articles 81 à 86 de la loi du 26 janvier 1984 ".<br/>
<br/>
              6. Le tribunal administratif, qui était saisi d'un moyen tiré par Mme B...de ce qu'elle avait droit à sa réintégration à l'issue de son congé parental, auquel le centre d'action sociale de la ville de Paris opposait son inaptitude à reprendre ses fonctions, n'a pas relevé un moyen d'office en vérifiant si l'intéressée entrait dans l'un des cas limitativement énumérés permettant à l'administration, pour des raisons liées à son état de santé, de la placer d'office en disponibilité. Ayant relevé que Mme B...n'avait pas épuisé ses droits aux congés de maladie, de longue maladie ou de longue durée mentionnés aux 2°, 3° et 4° de l'article 57 de la loi du 26 janvier 1984, déterminant les seuls cas dans lesquels l'administration aurait pu légalement la mettre en disponibilité d'office pour des raisons liées à son état de santé, il en a déduit sans erreur de droit que le centre d'action sociale de la ville de Paris n'avait pu légalement prendre une telle mesure à son encontre. <br/>
<br/>
              7. En quatrième lieu, aux termes de l'article 75 de la loi du 26 janvier 1984, dans sa rédaction applicable à la décision litigieuse : " Le congé parental est la position du fonctionnaire qui est placé hors de son administration ou service d'origine pour élever son enfant. (...) A l'expiration de son congé, il est réintégré de plein droit, au besoin en surnombre, dans sa collectivité ou établissement d'origine, sur sa demande et à son choix, dans son ancien emploi ou dans un emploi le plus proche de son dernier lieu de travail ou de son domicile lors de sa réintégration lorsque celui-ci a changé pour assurer l'unité de la famille ". Aux termes de l'article 34 du décret du 13 janvier 1986, dans sa rédaction applicable à la décision litigieuse : " Le fonctionnaire doit, deux mois au moins avant sa réintégration à l'issue d'une période de congé parental, faire connaître si pour assurer l'unité de la famille, il demande à être réintégré dans son ancien emploi, dans l'emploi le plus proche de son dernier lieu de travail ou dans l'emploi le plus proche de son domicile lorsque celui-ci a changé ".<br/>
<br/>
              8. Il résulte de ces dispositions qu'à l'expiration d'un congé parental, le fonctionnaire est réintégré de plein droit dans sa collectivité ou son établissement d'origine. La circonstance qu'il n'ait pas fait connaître, deux mois au moins avant ce terme, son choix quant à l'emploi dans lequel il demande à être réintégré n'a d'incidence que sur son droit à faire usage de cette faculté mais ne dispense pas l'administration de l'obligation de le réintégrer. Il suit de là que le tribunal, qui s'est borné, au terme d'une appréciation souveraine exempte de dénaturation, à relever que Mme B...avait demandé sa réintégration, n'a pas commis d'erreur de droit en jugeant qu'en l'absence d'autre souhait exprimé par l'intéressée, le centre d'action sociale de la ville de Paris était tenu de procéder à sa réintégration à l'expiration de son congé parental.     <br/>
<br/>
              9. En cinquième lieu, il résulte des dispositions des articles 57 de la loi du 26 janvier 1984 et 14 du décret du 30 juillet 1987 qu'un fonctionnaire en activité peut être mis d'office en congé de maladie en cas de maladie dûment constatée et le mettant dans l'impossibilité d'exercer ses fonctions. En jugeant qu'à la date du 15 décembre 2009, Mme B...ne pouvait être légalement placée d'office en disponibilité par son administration mais devait être réintégrée, tout en relevant que le comité médical avait rendu un avis défavorable à sa reprise de fonctions en raison de son inaptitude aux fonctions d'agent social, le tribunal n'a pas entaché son jugement de contradiction de motifs et n'a pas commis d'erreur de droit. <br/>
<br/>
              10. En sixième lieu, aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public (...) prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure (...) ". En jugeant qu'eu égard au motif de l'annulation de l'arrêté plaçant Mme B...en disponibilité d'office, tiré du droit à réintégration à l'issue d'un congé parental, sa décision impliquait nécessairement qu'il fût enjoint au centre d'action sociale de la ville de Paris de procéder à cette réintégration, le tribunal n'a pas entaché son jugement de contradiction de motifs et n'a pas commis d'erreur de droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que le centre d'action sociale de la ville de Paris n'est pas fondé à demander l'annulation du jugement qu'il attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du centre d'action sociale de la ville de Paris est rejeté.<br/>
Article 2 : La présente décision sera notifiée au centre d'action sociale de la ville de Paris et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
