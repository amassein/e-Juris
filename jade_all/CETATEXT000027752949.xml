<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027752949</ID>
<ANCIEN_ID>JG_L_2013_07_000000350674</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/29/CETATEXT000027752949.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 25/07/2013, 350674, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350674</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:350674.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 juillet et 12 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE STAR VOYAGES ANTILLES, dont le siège est à la Marina de la Pointe du Bout aux Trois Ilets (97229) ; la SOCIETE STAR VOYAGES ANTILLES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09BX02214 du 7 avril 2011 par lequel la cour administrative d'appel de Bordeaux, après avoir réduit le rappel de taxe sur la valeur ajoutée au titre de la période comprise entre le 1er janvier et le 31 décembre 1996 et  l'avoir déchargée des pénalités correspondantes, a rejeté le surplus des conclusions de sa requête tendant à l'annulation des jugements n° 0300215 et 0300216 du 30 juin 2009 par lesquels le tribunal administratif de Fort-de-France a rejeté sa demande tendant, d'une part, à la décharge des compléments d'impôt sur les sociétés et de contribution additionnelle sur cet impôt auxquels elle a été assujettie au titre des exercices clos en 1995 et 1996 et, d'autre part, à la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période correspondant à l'exercice clos en 1996 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de la SOCIETE STAR VOYAGES ANTILLES, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet, avocat de la SOCIETE STAR VOYAGES ANTILLES ; <br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              Considérant que, pour demander l'annulation de l'arrêt qu'elle attaque, la SOCIETE STAR VOYAGES ANTILLES soutient que la cour administrative d'appel de Bordeaux a omis de répondre au moyen soulevé devant elle, tiré de ce que l'administration fiscale ne pouvait, pour l'application des dispositions de l'article 217 bis du code général des impôts, réduire la part de déficit déductible provenant de l'activité éligible au dispositif que cet article prévoit ; qu'elle a commis une erreur de droit et dénaturé les pièces du dossier en jugeant que les activités de location et d'achat-revente de bateaux devaient être appréciées distinctement pour l'application de cet article au motif que, si elle tirait un avantage économique de l'exercice de ces deux activités, elles étaient cependant distinctes et que seule l'activité de location relevait du secteur du tourisme ; qu'elle a commis une erreur de droit en jugeant que l'activité de location de bateaux devait s'analyser comme une prestation, non de transport maritime de personnes, mais de location de moyens de transport au motif que la mise à disposition d'un skipper constituait une simple option ; qu'elle a dénaturé les pièces du dossier et commis une erreur de droit au regard de la charge de la preuve en validant le passif regardé comme injustifié à la clôture de l'exercice 1996 au motif que la symétrie des écritures dans sa comptabilité et celle de sa filiale ne constituait pas, en l'absence de pièces justificatives, un élément suffisant pour justifier de leur exactitude ; qu'elle a dénaturé les pièces du dossier en estimant  qu'elle n'avait pas établi que le solde de taxe sur la valeur ajoutée déductible comptabilisé au 31 décembre 1996 concernait uniquement les factures se rattachant aux prestations de services délivrées aux propriétaires des bateaux ; qu'elle a commis une erreur de droit en jugeant qu'elle ne pouvait pas se prévaloir, sur le fondement de l'article L. 80 A du livre des procédures fiscales, de la documentation administrative de base 3 G-242 du 1er septembre 1998 au motif que celle-ci était postérieure à la période d'imposition en cause ; qu'elle a commis une erreur de droit en jugeant qu'elle n'établissait pas que les voiliers loués n'étaient utilisés qu'en dehors des eaux territoriales françaises ou d'un autre Etat membre de la Communauté européenne et qu'ainsi ces moyens de transport avaient été utilisés dans des conditions n'entrant pas dans le champ d'application de la taxe sur la valeur ajoutée ;<br/>
<br/>
              Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les compléments d'impôt sur les sociétés et de contribution additionnelle sur l'impôt sur les sociétés auxquels la SOCIETE STAR VOYAGES ANTILLES a été assujettie au titre des exercices clos en 1995 et 1996 en application des dispositions de l'article 217 bis du code général des impôts ; qu'en revanche, s'agissant des conclusions dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur le passif regardé comme injustifié au titre de l'exercice clos en 1996 et sur le rappel de taxe sur la valeur ajoutée de l'année 1996, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la SOCIETE STAR VOYAGES ANTILLES qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les compléments d'impôt sur les sociétés et de contribution additionnelle sur l'impôt sur les sociétés auxquels elle a été assujettie au titre des exercices clos en 1995 et 1996 en application des dispositions de l'article 217 bis du code général des impôts au titre des exercices clos en 1995 et 1996 sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la SOCIETE STAR VOYAGES ANTILLES n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE STAR VOYAGES ANTILLES. Copie en sera adressée pour information à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du gouvernement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
