<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044205268</ID>
<ANCIEN_ID>JG_L_2021_10_000000433525</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/20/52/CETATEXT000044205268.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/10/2021, 433525, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433525</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:433525.20211013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée 12 août 2019 au secrétariat du contentieux du Conseil d'État, M. A... B... demande au Conseil d'État : <br/>
<br/>
              l°) d'annuler pour excès de pouvoir l'arrêté du 17 juin 2019 par lequel le président de l'université d'Aix-Marseille l'a suspendu de ses fonctions pour une durée de quatre mois, avec maintien du traitement ; <br/>
<br/>
              2°) de mettre à la charge de l'université d'Aix-Marseille la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation, notamment son article L. 951-4 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - l'arrêté du ministre de l'enseignement supérieur et de la recherche du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. B..., professeur des universités en fonction à l'université d'Aix-Marseille, demande l'annulation pour excès de pouvoir de l'arrêté du 17 juin 2019 par lequel le président de l'université l'a suspendu de ses fonctions pour une durée de quatre mois, avec maintien du traitement.<br/>
<br/>
              2. Aux termes de l'article L. 951-4 du code de l'éducation : " Le ministre chargé de l'enseignement supérieur peut prononcer la suspension d'un membre du personnel de l'enseignement supérieur pour un temps qui n'excède pas un an, sans privation de traitement ". La suspension d'un professeur des universités sur le fondement de ces dispositions revêt un caractère conservatoire et vise à préserver l'intérêt du service public universitaire. Elle ne peut être prononcée que lorsque les faits imputés à l'intéressé présentent un caractère suffisant de vraisemblance et de gravité et que la poursuite des activités de l'intéressé au sein de l'établissement présente des inconvénients suffisamment sérieux pour le service ou pour le déroulement des procédures en cours.<br/>
<br/>
              3. En premier lieu, il résulte des dispositions précitées de l'article L. 951-4 du code de l'éducation que l'article 30 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires, relatif à la suspension des fonctionnaires, n'est pas applicable aux professeurs d'université. Par suite, M. B... est fondé à soutenir que la décision de suspension du 17 juin 2019, prise au visa de ces dernières dispositions, est fondée sur une base légale erronée.<br/>
<br/>
              4. Toutefois, lorsqu'il constate que la décision contestée devant lui aurait pu être prise, en vertu du même pouvoir d'appréciation, sur le fondement d'un autre texte que celui dont la méconnaissance est invoquée, le juge de l'excès de pouvoir peut substituer ce fondement à celui qui a servi de base légale à la décision attaquée, sous réserve que l'intéressé ait disposé des garanties dont est assortie l'application du texte sur le fondement duquel la décision aurait dû être prononcée. En l'espèce, la décision attaquée, motivée par la suspicion de faute grave qu'aurait commise M. B..., trouve son fondement légal dans les dispositions de l'article L. 951-4 du code de l'éducation, qui peuvent être substituées, comme le demande l'université d'Aix-Marseille, à celles de l'article 30 de la loi du 13 juillet 1983 dès lors, en premier lieu, que la situation de M. B... relève du champ d'application de l'article L. 951-4 du code de l'éducation, en deuxième lieu, que cette substitution de base légale n'a pour effet de priver l'intéressé d'aucune garantie et, en troisième lieu, que l'administration dispose du même pouvoir d'appréciation pour appliquer l'une ou l'autre de ces deux dispositions. <br/>
<br/>
              5. En deuxième lieu, l'article L. 951-3 du code de l'éducation autorise le ministre chargé de l'enseignement supérieur à déléguer aux présidents des universités tout ou partie de ses pouvoirs en matière de recrutement et de gestion des personnels relevant de son autorité. En application de ces dernières dispositions, l'article 2 de l'arrêté du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche prévoit que : " Les présidents et les directeurs des établissements publics d'enseignement supérieur dont la liste est fixée à l'article 3 du présent arrêté reçoivent délégation des pouvoirs du ministre chargé de l'enseignement supérieur pour le recrutement et la gestion des personnels enseignants mentionnés à l'article 1er du présent arrêté en ce qui concerne : / [...] 24. La suspension ". L'article 3 du même arrêté vise notamment les établissements publics à caractère scientifique, culturel et professionnel mentionnés à l'article L. 711-2 du code de l'éducation, dont font partie les universités. La suspension est donc au nombre des mesures de gestion pour lesquelles l'article L. 951-3 du code de l'éducation autorise le ministre chargé de l'enseignement supérieur à déléguer ses pouvoirs aux présidents d'université. Par suite, le moyen tiré de l'incompétence du président de l'université d'Aix-Marseille pour prendre la décision litigieuse doit être écarté. <br/>
<br/>
              6. En troisième lieu, les décisions de suspension prononcées sur le fondement de l'article L. 951-4 du code de l'éducation sont des mesures conservatoires, prises dans le souci de préserver le bon fonctionnement du service public universitaire. Il ne ressort pas des pièces du dossier que la décision litigieuse aurait revêtu le caractère d'une sanction disciplinaire déguisée. Par suite, M. B... n'est pas fondé à soutenir que la mesure qu'il attaque aurait dû être motivée en raison de ce qu'elle lui inflige une sanction. Pour le même motif, le moyen tiré de ce que sa motivation serait insuffisante est inopérant.<br/>
<br/>
              7. En quatrième lieu, eu égard à la nature de l'acte de suspension prévu par les dispositions de l'article L. 951-4 du code de l'éducation et à la nécessité d'apprécier, à la date à laquelle cet acte a été pris, la condition de légalité tenant aux caractères grave et vraisemblable de certains faits, il appartient au juge de l'excès de pouvoir de statuer au vu des informations dont disposait effectivement l'autorité administrative au jour de sa décision. Les éléments nouveaux qui seraient, le cas échéant, portés à la connaissance de l'administration postérieurement à sa décision, ne peuvent ainsi, alors même qu'ils seraient relatifs à la situation de fait prévalant à la date de l'acte litigieux, être utilement invoqués au soutien d'un recours en excès de pouvoir contre cet acte. L'administration est en revanche tenue d'abroger la décision en cause si de tels éléments font apparaître que la condition tenant à la gravité et à la vraisemblance des faits à l'origine de la mesure n'est plus satisfaite. <br/>
<br/>
              8. Il ressort des pièces du dossier qu'à la date de l'arrêté attaqué, plusieurs témoignages concordants imputaient à M. B... un comportement violent et imprévisible responsable d'une dégradation profonde du climat de travail, qui affectait les activités universitaires et le déroulement des enseignements. Alors même que ces faits étaient contestés par M. B..., ils étaient, en l'état des informations portées à la connaissance du président de l'université, suffisamment graves et vraisemblables pour justifier l'éloignement de l'intéressé à titre conservatoire. Par suite, le président de l'université d'Aix-Marseille n'a pas fait une inexacte application des dispositions de l'article L. 951-4 du code de l'éducation en prenant l'arrêté attaqué.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêté du 17 juin 2019 par lequel le président de l'université d'Aix-Marseille l'a suspendu de ses fonctions pour une durée de quatre mois.<br/>
<br/>
              10. La décision en litige ayant été prise au nom de l'Etat, l'université d'Aix-Marseille n'a pas la qualité de partie en défense dans la présente instance. Sa présence en qualité d'observateur ne lui confère pas davantage la qualité de partie, dès lors qu'elle n'aurait pas eu, à défaut d'être présente, qualité pour faire tierce-opposition du présent arrêt. Par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. B... la somme qu'elle demande à ce titre. Pour les mêmes motifs, ces dispositions font obstacle à ce que soit mise à la charge de l'université la somme que M. B... demande au même titre. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : Les conclusions de l'université d'Aix-Marseille présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.  <br/>
Copie en sera adressée à l'université d'Aix-Marseille. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
