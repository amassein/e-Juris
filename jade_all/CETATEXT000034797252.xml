<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034797252</ID>
<ANCIEN_ID>JG_L_2017_05_000000410404</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/79/72/CETATEXT000034797252.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/05/2017, 410404, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410404</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:410404.20170515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...C...et Mme A...C...ont demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre à la préfète de la Loire-Atlantique d'enregistrer leurs demandes d'asile dans un délai de 48 heures à compter de la notification de l'ordonnance et, à titre subsidiaire, d'enjoindre au préfet de la Vendée de leur fournir un hébergement d'urgence susceptible de les accueillir sans délai. Par une ordonnance n° 1703873 du 5 mai 2017, le juge des référés a rejeté leur demande. <br/>
<br/>
              Par une requête, enregistrée 10 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :  <br/>
              1°) d'annuler cette ordonnance ;<br/>
              2°) de faire droit à leur demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 1 500 euros au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, sous réserve du renoncement au bénéfice de l'aide juridictionnelle.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, le refus du préfet d'enregistrer leur demande d'asile dans le délai légal de trois jours ouvrables prévu à l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile les empêche de bénéficier des conditions matérielles d'accueil inhérentes à leur statut de demandeur d'asile, et ce malgré leur situation de particulière vulnérabilité dans la mesure où la mère est malade et les enfants sont mineurs et, d'autre part, il reste encore dix jours avant l'enregistrement de leur demande en préfecture, où le rendez-vous est fixé au 18 mai 2017 ;<br/>
              - l'ordonnance contestée est entachée d'une erreur de droit en ce qu'elle ne se prononce pas sur l'urgence à faire droit, à titre subsidiaire, à un hébergement d'urgence et à faire cesser l'atteinte à la dignité humaine ;<br/>
              - le non-respect du délai légal imparti au préfet pour enregistrer une demande d'asile après la présentation de cette dernière a porté une atteinte illégale et manifestement grave à leur droit d'asile ; <br/>
              - le défaut d'enregistrement de leur demande d'asile porte une atteinte grave et manifestement illégale à leur droit d'asile dès lors que, d'une part, ils sont privés des conditions matérielles d'accueil auxquelles ont droit les demandeurs d'asile, d'autre part, leur demande n'a pas été examinée en fonction de leurs besoins particuliers alors qu'ils présentent plusieurs facteurs de vulnérabilité tels que la présence d'enfants mineurs et la fragilité psychiatrique de MmeC... ; <br/>
              - il est porté une atteinte grave et manifestement illégale au principe de dignité humaine tel que protégé par la Constitution, la convention européenne de sauvegarde  des droits de l'homme et des libertés fondamentales ainsi que la charte des droits fondamentaux de l'Union européenne dès lors qu'ils sont privés de leur droit à l'accueil ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit à l'hébergement d'urgence au regard de la vulnérabilité de leur famille ;<br/>
              - le juge des référés a commis une erreur de droit en considérant que la seule saturation locale du réseau d'hébergement suffisait à conclure à une impossibilité de la prise en charge de leur famille alors que le dispositif d'hébergement des demandeurs d'asile est national.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la charte des droits fondamentaux de l'Union européenne ; <br/>
              - la directive 2013/33/UE du 26 juin 2013 du Parlement européen et du Conseil ; <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. D'une part, le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. L'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que l'enregistrement de la demande d'asile " a lieu au plus tard trois jours ouvrés après la présentation de la demande à l'autorité administrative compétente, sans condition préalable de domiciliation. Toutefois, ce délai peut être porté à dix jours ouvrés lorsqu'un nombre élevés d'étrangers demandent l'asile simultanément ".<br/>
<br/>
              3. D'autre part, il appartient aux autorités de l'Etat, sur le fondement du code de l'action sociale et des familles, de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique ou sociale. Une carence caractérisée dans l'accomplissement de cette mission peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée. <br/>
              4. Il résulte de l'instruction que M. et MmeC..., ressortissants albanais, accompagnés de deux enfants âgés de 6 et 12 ans, entrés sur le territoire français le 6 avril 2017, ont obtenu le 7 avril 2017 un rendez-vous à la préfecture de la Loire-Atlantique pour le 18 mai 2017 afin que soient enregistrées leurs demandes d'asile. D'une part, l'instruction ne fait apparaître aucune méconnaissance manifeste des obligations qui découlent du respect du droit d'asile, ni, ainsi que l'a relevé le juge des référés du tribunal administratif de Nantes, d'urgence caractérisée eu égard à la date du 18 mai 2017 fixée pour l'enregistrement de la demande d'asile des intéressés à la préfecture. D'autre part, ainsi que l'a constaté à bon droit le juge des référés du tribunal administratif de Nantes, et pour les motifs qu'il a retenus, aucune carence caractérisée des autorités préfectorales dans l'exercice du droit à l'hébergement d'urgence ne peut être retenue.<br/>
<br/>
              5. Il s'ensuit que M. et Mme C...ne sont pas fondés à soutenir que c'est à tort que, par l'ordonnance contestée, qui est suffisamment motivée, le juge des référés du tribunal administratif de Nantes a rejeté leur demande. Leur requête doit, par suite, être rejetée, y compris, par voie de conséquence, leurs conclusions présentées au titre des articles L. 761-1 du code de justice administrative et de l'article 37 alinéa 2 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. et Mme C...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...C..., à Mme A...C...et à la préfète de la Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
