<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031466673</ID>
<ANCIEN_ID>JG_L_2015_11_000000382122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/66/CETATEXT000031466673.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 09/11/2015, 382122, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2015:382122.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser les sommes de 72 893,45 euros et 30 000 euros en réparation respectivement du préjudice financier et du préjudice moral résultant de la régularisation tardive de sa situation par la signature d'un contrat à durée indéterminée et d'enjoindre au ministre de l'économie et des finances de lui verser l'indemnité de résidence due depuis la date de son recrutement. Par un jugement n° 1215654/5-4 du 25 juin 2013, le tribunal administratif a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 13PA03374 du 22 avril 2014, la cour administrative d'appel de Paris, sur appel de Mme B..., a annulé ce jugement en tant qu'il statue sur ses conclusions tendant à l'indemnisation de son préjudice moral, statuant après évocation a rejeté ces conclusions, puis a rejeté le surplus de sa requête d'appel.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 juillet et 24 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu ;<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a exercé, à partir du 15 juillet 1987, les fonctions de médecin de prévention des services déconcentrés des ministères économiques et financiers ; qu'elle est restée sous le régime de simples vacations hebdomadaires rémunérées selon un taux horaire jusqu'à la signature, le 11 février 2010, d'un contrat qui a régularisé sa situation statutaire ; qu'elle demande l'annulation de l'arrêt de la cour administrative d'appel de Paris du 22 avril 2014 qui a rejeté ses conclusions tendant à l'indemnisation par l'Etat des préjudices qu'elle estime avoir subis du fait de la régularisation tardive de sa situation par la signature de ce contrat ;<br/>
<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur le préjudice moral :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis " ;<br/>
<br/>
              3. Considérant que, pour l'application de ces dispositions, le délai de prescription de la créance dont se prévaut un agent du fait du retard mis par l'administration à le placer dans une situation statutaire régulière court à compter du premier jour de l'année suivant celle au cours de laquelle est intervenu l'acte ayant régularisé sa situation ; qu'il en résulte que le délai de prescription de la créance liée au préjudice moral dont se prévaut Mme B...du fait de l'intervention tardive du contrat à durée indéterminée signé le 11 février 2010 a commencé à courir le 1er janvier 2011 ; que, par suite, Mme B... est fondée à soutenir que la cour administrative d'appel de Paris a commis une erreur de droit en jugeant qu'à la date d'introduction de sa réclamation préalable, intervenue en 2012, la créance relative à son préjudice moral était prescrite ; que, dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi dirigé contre cette partie de l'arrêt attaqué, celui-ci doit être annulé dans cette mesure ;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la perte du supplément familial de traitement :<br/>
<br/>
              4. Considérant que, pour établir qu'elle pouvait prétendre au bénéfice du supplément familial de traitement depuis la date de son recrutement, Mme B...se bornait à soutenir devant les juges du fond que la rémunération des médecins de prévention vacataires était réévaluée depuis le 1er janvier 1991 en fonction de la valeur du point d'indice dans la fonction publique ; que, par suite, le moyen tiré de ce que la cour aurait commis une erreur de droit en ne recherchant pas si l'administration n'aurait pas dû, dès l'origine, lui proposer un contrat lui ouvrant le bénéfice du supplément familial de traitement est nouveau en cassation et doit, dès lors, être écarté comme inopérant ;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la perte des droits à congés annuels :<br/>
<br/>
              5. Considérant que la cour ne s'est pas méprise sur la portée des écritures de Mme B...en estimant que le moyen tiré de ce qu'elle n'avait pas pu, en qualité de vacataire, bénéficier de congés annuels équivalents à ceux des agents contractuels, était dépourvu des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que l'arrêt attaqué doit être annulé en tant qu'il statue sur les conclusions présentées par Mme B...relatives à son préjudice moral ; qu'il doit également être annulé, par voie de conséquence, en tant qu'il statue sur ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat une somme de 1 000 euros au titre des frais exposés par Mme B...et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				---------------<br/>
<br/>
Article 1er : L'arrêt du 22 avril 2014 de la cour administrative d'appel de Paris est annulé en tant qu'il statue sur les conclusions de Mme B...relatives à son préjudice moral et sur ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris dans cette mesure.<br/>
Article 3 : L'Etat versera à Mme B...une somme de 1 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de Mme B...est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
