<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035775006</ID>
<ANCIEN_ID>JG_L_2017_10_000000403855</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/50/CETATEXT000035775006.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 11/10/2017, 403855</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403855</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403855.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 septembre et 20 décembre 2016 et le 13 juin 2017 au secrétariat du contentieux du Conseil d'Etat, le syndicat éducation populaire - UNSA demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-1051 du 1er août 2016 relatif au projet éducatif territorial et à l'encadrement des enfants scolarisés bénéficiant d'activités périscolaires dans ce cadre ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 37-1 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2013-595 du 8 juillet 2013 ;<br/>
              - le décret n° 2013-707 du 2 août 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat du syndicat éducation populaire - UNSA ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, que l'article 37-1 de la Constitution dispose : " La loi et le règlement peuvent comporter, pour un objet et une durée limités, des dispositions à caractère expérimental " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article L. 551-1 du code de l'éducation, dans sa rédaction issue de la loi du 8 juillet 2013 d'orientation et de programmation pour la refondation de la République : " Des activités périscolaires prolongeant le service public de l'éducation, et en complémentarité avec lui, peuvent être organisées dans le cadre d'un projet éducatif territorial associant notamment aux services et établissements relevant du ministre chargé de l'éducation nationale d'autres administrations, des collectivités territoriales, des associations et des fondations, sans toutefois se substituer aux activités d'enseignement et de formation fixées par l'Etat. L'élaboration et la mise en application de ce projet sont suivies par un comité de pilotage (...) " ; <br/>
<br/>
              3. Considérant, enfin, qu'aux termes de l'article L. 227-4 du code de l'action sociale et des familles : " La protection des mineurs, dès leur inscription dans un établissement scolaire en application de l'article L. 113-1 du code de l'éducation, qui bénéficient hors du domicile parental, à l'occasion des vacances scolaires, des congés professionnels ou des loisirs, d'un mode d'accueil collectif à caractère éducatif (...) est confiée au représentant de l'Etat dans le département(...) " ; que l'article L. 227-5 du même code renvoie à un décret en Conseil d'Etat le soin de prévoir les modalités d'application de ces dispositions, notamment en vue de préciser " les conditions particulières d'encadrement " des mineurs bénéficiant de ces modes d'accueil ; qu'en application de ces dispositions, l'article R. 227-16 du code de l'action sociale et des familles fixe, pour l'encadrement des enfants en accueil de loisirs périscolaire, l'effectif minimum des personnes exerçant des fonctions d'animation selon l'âge et le nombre de mineurs pris en charge ;<br/>
<br/>
              4. Considérant que, pour la mise en oeuvre des dispositions citées aux points 2 et 3, le décret du 2 août 2013 relatif au projet éducatif territorial et portant expérimentation relative à l'encadrement des enfants scolarisés bénéficiant d'activités périscolaires dans ce cadre a, en premier lieu, précisé le régime juridique des projets éducatifs territoriaux et, en second lieu, prévu, à titre expérimental et pour une durée de trois ans, des taux d'encadrement réduits par rapport aux normes fixées à l'article R. 227-16 du code de l'action sociale et des familles pour l'accueil de loisirs périscolaire organisé dans le cadre des projets éducatifs territoriaux ; qu'à l'issue de cette expérimentation, le décret attaqué, du 1er août 2016, qui avait notamment pour objet de pérenniser cette dernière, a inséré au code de l'action sociale et des familles les dispositions relatives aux projets éducatifs territoriaux, pérennisé les taux d'encadrement dérogatoires dont les projets éducatifs territoriaux bénéficiaient et abrogé le décret du 2 août 2013 ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article 3 du décret du 2 août 2013 : " L'évaluation de l'expérimentation prévue au I de l'article 2 fait l'objet, six mois avant son terme, d'un rapport réalisé par le comité de pilotage mentionné à l'article L. 551-1 du code de l'éducation réunissant l'ensemble des partenaires du projet éducatif territorial signataires de la convention mentionnée au I de l'article 1er et transmis au préfet du département et au recteur d'académie. Ces autorités adressent aux ministres chargés de l'éducation nationale et de la jeunesse, au plus tard quatre mois avant la fin de l'expérimentation, une synthèse de ces rapports d'évaluation. Au vu de ces rapports, le Gouvernement décide soit de mettre fin à l'expérimentation, soit de pérenniser tout ou partie des mesures prises à titre expérimental " ; qu'il ressort des pièces du dossier que, préalablement à l'édiction du décret du 1er août 2016, des " fiches de synthèse " évaluant l'impact des mesures prévues dans le décret du 2 août 2013 ont été réalisées par les services déconcentrés de l'Etat, s'appuyant sur les rapports des comités de pilotage des projets éducatifs territoriaux, sur des contrôles d'accueils effectués par ces services et sur des entretiens avec les publics et les collectivités concernés, et transmises aux ministres concernés ; qu'en outre, les résultats de ces évaluations ont été synthétisés dans un document du ministère de la jeunesse intitulé " Rapport - Evaluation de l'impact des mesures expérimentales prévues par le décret n° 2013-707 du 2 août 2013 " ; que, dès lors, le syndicat requérant n'est pas fondé à soutenir que le décret attaqué aurait été pris au terme d'une procédure irrégulière, faute d'avoir été précédé de l'évaluation prévue par les dispositions de l'article 3 du décret du 2 août 2013 ; que, par suite, ses conclusions tendant à l'annulation du décret attaqué ne peuvent qu'être rejetées ainsi que ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête du syndicat éducation populaire - UNSA est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat éducation populaire - UNSA, au Premier ministre, au ministre de l'éducation nationale et à la ministre des sports.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. - DÉCRET INSTITUANT UNE RÈGLE DE PROCÉDURE RELATIVE À L'ÉLABORATION D'UN DÉCRET POSTÉRIEUR - MOYEN TIRÉ DE CE QUE LE DÉCRET POSTÉRIEUR, QUI ABROGE LE PRÉCÉDENT DÉCRET, A ÉTÉ PRIS EN MÉCONNAISSANCE DE LA RÈGLE FIXÉE PAR CELUI-CI - MOYEN OPÉRANT (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - DÉCRET INSTITUANT UNE RÈGLE DE PROCÉDURE RELATIVE À L'ÉLABORATION D'UN DÉCRET POSTÉRIEUR - MOYEN TIRÉ DE CE QUE LE DÉCRET POSTÉRIEUR, QUI ABROGE LE PRÉCÉDENT DÉCRET, A ÉTÉ PRIS EN MÉCONNAISSANCE DE LA RÈGLE FIXÉE PAR CELUI-CI - ABSENCE (SOL. IMPL.) [RJ1].
</SCT>
<ANA ID="9A"> 01-03-01 Décret organisant une expérimentation et prévoyant un rapport d'évaluation préalablement à la pérennisation du dispositif par un décret postérieur.,,,Une autorité administrative étant tenue de se conformer aux dispositions réglementaires légalement édictées qui fixent les règles de forme et de procédure selon lesquelles elle doit exercer ses compétences, le moyen tiré de la méconnaissance de l'obligation d'évaluation, soulevé contre le décret pérennisant le dispositif expérimenté par le premier décret, est opérant.</ANA>
<ANA ID="9B"> 54-07-01-04-03 Décret organisant une expérimentation et prévoyant un rapport d'évaluation préalablement à la pérennisation du dispositif par un décret postérieur.,,,Une autorité administrative étant tenue de se conformer aux dispositions réglementaires légalement édictées qui fixent les règles de forme et de procédure selon lesquelles elle doit exercer ses compétences, le moyen tiré de la méconnaissance de l'obligation d'évaluation, soulevé contre le décret pérennisant le dispositif expérimenté par le premier décret, est opérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le respect par une autorité administrative d'une règle qu'elle a édictée, Section, 28 novembre 1930, Sieur,p. 995 ; CE, Assemblée, 19 mai 1983, Club sportif et familial de la Fève et autre, n°s 23127, 23181, 23182, p. 204. Rappr., dans l'hypothèse d'un décret simple instituant une règle de procédure relative à l'élaboration d'un décret en Conseil d'Etat, CE, 16 mai 2008, Département du Val-de-Marne, n°s 290416, 290723, 290766, 294677, T. pp. 579-581-619-883.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
