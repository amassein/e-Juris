<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042737155</ID>
<ANCIEN_ID>JG_L_2020_12_000000431505</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/73/71/CETATEXT000042737155.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 23/12/2020, 431505, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431505</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431505.20201223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure <br/>
<br/>
              M. D... A... a demandé au tribunal départemental des pensions militaires de la Haute-Savoie d'annuler les décisions du ministre de la défense des 30 avril 2007 et 2 juillet 2007 rejetant sa demande de revalorisation de sa pension militaire d'invalidité pour l'aligner sur l'indice du grade équivalent dans la marine nationale. Par un jugement du 1er février 2010, le tribunal a accordé à M. A... la revalorisation demandée au taux de l'indice du grade équivalent dans la marine nationale à celui de sergent-major de l'armée de terre. <br/>
<br/>
              Par un arrêt n° 10/00004 du 17 décembre 2010, la cour régionale des pensions de Chambéry a, sur appel du ministre de la défense, annulé ce jugement et déclaré irrecevable la demande de M. A... devant le tribunal départemental des pensions militaires de la Haute-Savoie.<br/>
<br/>
              Par un arrêt n° 345941 du 1er février 2012, le Conseil d'Etat a annulé l'arrêt de la cour régionale des pensions de Chambéry, puis rejeté l'appel présenté par les ministres de la défense et des anciens combattants devant cette cour. <br/>
<br/>
              M. A... a demandé au tribunal départemental des pensions militaires de la Savoie d'annuler la décision du 14 octobre 2014 par laquelle le ministre de la défense a rejeté sa demande de revalorisation de sa pension à compter du 28 janvier 1968, date à laquelle il a été radié des cadres. Par un jugement n° 15/00002 du 1er avril 2016, le tribunal a rejeté cette demande comme étant irrecevable. <br/>
<br/>
              Par un arrêt n°16/00003 du 1er décembre 2017, la cour régionale des pensions de Chambéry a, sur appel de M. A..., annulé le jugement du tribunal départemental des pensions en tant qu'il a déclaré sa demande irrecevable, jugé cette demande recevable et que l'intéressé était en droit de prétendre aux avantages prévus par l'article 37 du code des pensions militaires d'invalidité et des victimes de guerre.<br/>
<br/>
              Par un arrêt n° RG 18/00001 du 22 novembre 2018, la cour régionale des pensions de Chambéry, saisie par M. A... d'une requête tendant à la rectification pour erreur matérielle de l'arrêt rendu le 1er décembre 2017, a complété le dispositif de cet arrêt en rejetant sa demande tendant à bénéficier de la revalorisation de sa pensions militaire d'invalidité à compter du 28 janvier 1968 et de l'octroi des arrérages depuis le 22 mai 2014, ainsi que ses conclusions tendant à l'application  des dispositions de l'article 37 de la loi n° 91-647 du 10 juillet 1991.  <br/>
<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 juin et 10 septembre 2019, au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête devant la cour régionale des pensions de Chambéry ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions militaires d'invalidité et des victimes de guerre ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... B..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 décembre 2020, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., titulaire d'une pension militaire d'invalidité concédée par un arrêté du 3 juin 1997, a obtenu la revalorisation de sa pension sur la base de l'indice applicable au grade des personnels de la marine nationale équivalent au sien, à compter du 1er janvier 2004, par un arrêté du 16 août 2010 du ministre de la défense, pris en exécution d'un jugement du 1er février 2010 du tribunal départemental des pensions militaires de la Haute-Savoie, confirmé par une décision du Conseil d'Etat n° 345941 du 1er février 2012. Par un jugement du 1er avril 2016, le tribunal départemental des pensions militaires de la Savoie a rejeté comme irrecevable sa requête dirigée contre le refus du ministre de faire droit à sa demande tendant à ce que la revalorisation de sa pension soit calculée à compter du 28 janvier 1968, date de sa radiation des cadres. Par un arrêt du 1er décembre 2017, la cour régionale des pensions de Chambéry a, à la demande de M. A..., annulé le jugement du tribunal départemental et jugé que sa requête était recevable. L'intéressé a ensuite demandé à la cour régionale de rectifier son arrêt du 1er décembre 2017 en faisant valoir qu'elle avait omis de statuer sur ses conclusions tendant à la revalorisation de sa pension à compter du 28 janvier 1968, à l'octroi des arrérages depuis le 22 mai 2014 et à l'application de l'article 37 de la loi de la loi du 10 juillet 1991 relative à l'aide juridictionnelle. M. A... se pourvoit en cassation contre l'arrêt du 22 novembre 2018 par lequel la cour régionale des pensions de Chambéry a complété le dispositif de son arrêt du 1er décembre 2017 en rejetant ses conclusions. <br/>
<br/>
              Sur le pourvoi incident de la ministre des armées : <br/>
<br/>
              2. Si la ministre des armées demande l'annulation de l'arrêt attaqué en tant qu'il a jugé recevable la demande de M. A... tendant à la revalorisation de sa pension, ces conclusions, qui ne sont pas dirigées contre le dispositif de l'arrêt, ne sont, en tout état de cause, pas recevables. <br/>
<br/>
              Sur le pourvoi de M. A... : <br/>
<br/>
              3. Les exigences qui découlent du principe d'impartialité s'opposent à ce que participe au jugement d'un recours en rectification d'erreur matérielle un juge qui a participé à la décision qui en est l'objet. Par conséquent, la présidente et les conseillers qui composaient la cour régionale des pensions de Chambéry ayant statué sur l'arrêt du 1er décembre 2017 ne pouvaient, comme ils l'ont fait, statuer sur un tel recours. L'arrêt du 22 novembre 2018 doit, pour ce motif et, sans qu'il soit besoin d'examiner les autres moyens du pourvoi de M. A..., être annulé. <br/>
<br/>
              4. En application des dispositions de l'article L. 821-2 du code de justice administrative, il y a lieu de régler l'affaire au titre du recours en rectification d'erreur matérielle. <br/>
<br/>
              Sur le recours en rectification d'erreur matérielle : <br/>
<br/>
              5. Par son arrêt du 1er décembre 2017, la cour régionale des pensions de Chambéry a omis de statuer sur les conclusions de M. A... tendant à ce que l'alignement du calcul de sa pension militaire d'invalidité sur l'indice applicable au grade équivalent au sien des personnels de la marine nationale soit fixé à compter du 28 janvier 1968, à l'octroi des arrérages de sa pension depuis la date de sa demande adressée au ministre, le 22 mai 2014, et à l'application des dispositions de l'article 37 de la loi n° 91-647 du 10 juillet 1991. Par conséquent, la requête présentée par M. A... tendant à la rectification de l'erreur matérielle résultant de cette omission est recevable et il y a lieu de statuer sur ces conclusions. <br/>
<br/>
              6. Aux termes de l'article L 108 du code des pensions militaires d'invalidité et des victimes de guerre : " Lorsque, par suite du fait personnel du pensionné, la demande de liquidation ou de révision de la pension est déposée postérieurement à l'expiration de la troisième année qui suit celle de l'entrée en jouissance normale de la pension, le titulaire ne peut prétendre qu'aux arrérages afférents à l'année au cours de laquelle la demande a été déposée et aux trois années antérieures. "<br/>
<br/>
              7. Il résulte de l'instruction que la demande de M. A..., tendant à l'alignement de l'indice de sa pension militaire d'invalidité sur celui appliqué, à grade équivalent, aux pensions des personnels de la marine nationale, entre dans le champ d'application de l'article L. 108 du code des pensions militaires d'invalidité et des victimes de guerre. Dès lors que l'intéressé ne justifie pas qu'une circonstance particulière l'aurait empêché de se prévaloir, avant l'expiration de la troisième année suivant celle de l'entrée en jouissance normale de sa pension d'invalidité, de ce que cette différence de traitement était contraire au principe d'égalité, il ne peut prétendre, en application de ces dispositions, qu'aux arrérages afférents à l'année au cours de laquelle il a présenté sa demande de revalorisation ainsi qu'aux trois années antérieures. Dès lors, la demande de Monsieur A... tendant à bénéficier de la revalorisation de sa pension militaire d'invalidité à compter du 28 janvier 1968 doit être rejetée. Ses conclusions tendant à l'octroi des arrérages depuis le 22 mai 2014 ainsi que celles tendant à l'application des dispositions de l'article 37 de la loi du 10 juillet 1991 ne peuvent, par voie de conséquence, qu'être rejetées.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Chambéry du 22 novembre 2018 est annulé.  <br/>
Article 2 : Le dispositif de l'arrêt du 1er décembre 2017 de la cour régionale des pensions de Chambéry est modifié et complété comme suit : <br/>
Rejette la demande de Monsieur A... tendant à l'alignement du calcul de sa pension militaire d'invalidité sur l'indice applicable au grade équivalent au sien des personnels de la marine nationale à compter du 28 janvier 1968 et l'octroi des arrérages depuis le 22 mai 2014 ;<br/>
Dit n'avoir pas lieu à application des dispositions de l'article 37 de la loi n°91-647 du 10 juillet 1991.<br/>
Article 3 : L'Etat versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. D... A..., à la cour régionale des pensions de Chambéry et à la ministre des armées.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
