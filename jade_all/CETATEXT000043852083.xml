<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852083</ID>
<ANCIEN_ID>JG_L_2021_07_000000440683</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 22/07/2021, 440683, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440683</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:440683.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 mai et 1er juillet 2020 et 9 février 2021 au secrétariat du contentieux du Conseil d'Etat, l'Union régionale des médecins de l'Océan indien demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-148 du 21 février 2020 relatif au fonctionnement du comité national des coopérations interprofessionnelles et des protocoles nationaux prévus à l'article L. 4011-3 du code de la santé publique et leur application au service de santé des armées ; <br/>
<br/>
              2°) de mettre la somme de 4 500 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de l'Union régionale des médecins libéraux de l'Océan Indien ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 4011-3 du code de la santé publique dispose, dans sa rédaction applicable à la date du décret attaqué, que : " I.- Un comité national des coopérations interprofessionnelles est chargé de la stratégie, de la promotion et du déploiement des coopérations interprofessionnelles. Il propose la liste des protocoles nationaux à élaborer et à déployer sur l'ensemble du territoire, appuie les professionnels de santé dans l'élaboration de ces protocoles et de leur modèle économique et émet un avis sur leur financement par l'assurance maladie. Il assure le suivi annuel et l'évaluation des protocoles autorisés. / (...) Le comité est composé, selon des modalités précisées par décret, de représentants de l'Union nationale des caisses de l'assurance maladie, de la Haute Autorité de santé, des ministres chargés de la sécurité sociale et de la santé ainsi que des agences régionales de santé. Les conseils nationaux professionnels et les ordres des professions concernées sont associés aux travaux de ce comité. (...) / III.- Le protocole national et son modèle économique sont rédigés par une équipe de rédaction, sélectionnée dans le cadre d'un appel national à manifestation d'intérêt, avec l'appui des conseils nationaux professionnels et des ordres des professions concernées. (...) / V.- Les conditions d'application du présent article sont précisées par voie réglementaire ". Aux termes de l'article D. 4011-2 du même code, dans sa rédaction issue du décret attaqué : " Le comité national des coopérations interprofessionnelles mentionné à l'article L. 4011-3 est placé auprès des ministres chargés de la santé et de la sécurité sociale./ (...) Les conseils nationaux professionnels et les ordres professionnels sont associés sans voix délibérative aux travaux du comité national sur invitation de ses présidents. / (...) " L'article D. 4011-3 du même code, dans sa rédaction issue du même décret, prévoit que : " (...) En vue de l'élaboration d'un protocole national, le comité publie un appel à manifestation d'intérêt qui précise les éléments utiles à la rédaction et au modèle économique de celui-ci. (...). / Le comité national sélectionne une ou plusieurs équipes candidates. Il apporte son appui à l'équipe de rédaction mentionnée au III de l'article L. 4011-3 pour l'élaboration collégiale du protocole national et de son modèle économique (...) ".<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Il ressort des pièces du dossier, notamment de l'ampliation de la minute du décret attaqué communiquée par le ministre des solidarités et de la santé, que ce décret a, contrairement à ce qui est soutenu par la requérante, été signé par le Premier ministre et par les ministres chargés de son exécution. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. D'une part, en prévoyant, à l'article D. 4011-2 du code de la santé publique, que les conseils nationaux professionnels et les ordres professionnels sont associés sans voix délibérative aux travaux du comité national sur invitation de ses présidents, le décret attaqué n'a pas méconnu les exigences de l'article L. 4011-3 du code de la santé publique, qui, s'il prévoit une association des conseils et ordres intéressés, n'impose pas ce faisant qu'ils aient une voix délibérative. En subordonnant la participation des intéressés à une invitation des présidents de ce comité national, le pouvoir réglementaire n'a pas davantage méconnu l'article L. 4011-3, dès lors que cette invitation est destinée, comme le fait valoir le ministre des solidarités et de la santé en défense, à adapter le format des réunions à leur ordre du jour, eu égard à l'importance du nombre de professions concernées. <br/>
<br/>
              4. D'autre part, les dispositions de l'article D. 4011-3 du code de la santé publique créées par le décret attaqué ont pour objet de définir la procédure de sélection de la ou des équipes chargées de la rédaction des projets nationaux de coopération interprofessionnelle. Si elles mentionnent que le comité national des coopérations interprofessionnelles apporte son appui à l'équipe de rédaction, sans rappeler dans le même temps que les conseils nationaux professionnels et les ordres professionnels sont également investis par l'article L. 4011-3 d'une mission d'appui à cette rédaction, elles n'ont ni pour objet ni pour effet de remettre en cause les dispositions de ce dernier article, que la procédure d'élaboration du protocole national par le comité national des coopérations interprofessionnelles doit respecter.<br/>
<br/>
              5. Il résulte de tout ce qui précède que l'Union régionale des médecins libéraux de l'Océan indien n'est pas fondée à soutenir que les dispositions du décret qu'elle attaque sont entachées d'illégalité. Ses conclusions tendant à leur annulation pour excès de pouvoir, ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative, ne peuvent, dès lors, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union régionale des médecins libéraux de l'Océan Indien est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'Union régionale des médecins libéraux de l'Océan indien et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
