<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081849</ID>
<ANCIEN_ID>JG_L_2017_02_000000394116</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081849.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 23/02/2017, 394116, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394116</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394116.20170223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête sommaire et un mémoire complémentaire, enregistrés les 19 octobre 2015 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 20 novembre 2014 par laquelle la directrice des ressources humaines du ministère des finances et des comptes publics et du ministère de l'économie, de l'industrie et du numérique a rejeté sa demande tendant à l'exonération du paiement de l'indemnité liée à la rupture de l'engagement de servir l'Etat pendant une durée de dix années à sa sortie de l'Ecole nationale d'administration ainsi que le décret du 19 mars 2015 le concernant portant réintégration et radiation des cadres du corps des administrateurs civils et l'astreignant à verser cette indemnité et la décision implicite de rejet du recours gracieux qu'il a formé le 17 juin 2015 à l'encontre de ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83- 634 du 13 juillet 1983 ;<br/>
              - le décret n° 45-2291 du 9 octobre 1945 ;<br/>
              - le décret n° 85-986 du 16 septembre 1985 ;<br/>
              - le décret n° 2014-1370 du 14 novembre 2014 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier que M. A..., administrateur civil, a été affecté à la direction du budget à compter du 1er avril 2003 à l'issue de sa scolarité à l'Ecole nationale d'administration ; qu'inscrit sur la liste de réserve établie à la suite du concours général d'administrateur communautaire, il a été placé en position de disponibilité pour convenances personnelles à compter du 2 juin 2003 afin d'exercer des fonctions de fonctionnaire européen au secrétariat général du Conseil de l'Union européenne, position dans laquelle il est demeuré pendant une période de dix ans jusqu'au 1er juin 2013 ; que, par lettre du 26 avril 2013, la directrice des ressources humaines du ministère des finances et des comptes publics et du ministère de l'économie, de l'industrie et du numérique l'a informé qu'il parvenait au terme de ses droits à disponibilité et qu'il pouvait solliciter soit sa réintégration dans les cadres de l'administration centrale, soit sa démission en conséquence de laquelle il serait astreint, faute d'avoir satisfait à l'engagement des élèves de l'Ecole nationale d'administration de rester au service de l'Etat pendant une durée minimum de dix années, à verser au trésor public une indemnité égale à deux fois son dernier traitement annuel ; que, par lettre du 23 juillet 2013, M. A...a présenté sa démission du corps des administrateurs civils à compter du 2 juin 2013 et a sollicité d'être exonéré du paiement de l'indemnité due en raison de la rupture de l'engagement de rester au service de l'Etat pendant une durée minimum de dix ans ; que, par lettre du 20 novembre 2014, la directrice des ressources humaines du ministère des finances et des comptes publics et du ministère de l'économie, de l'industrie et du numérique l'a informé de ce que les ministres n'avaient pas réservé de suite favorable à sa demande d'exonération et de ce que la radiation des cadres de l'administration allait leur être proposée ; que, par un décret du 19 mars 2015, le Président de la République a radié M. A...du corps des administrateurs civils et l'a astreint à verser au trésor public une indemnité calculée conformément aux dispositions du I de l'article 1er du décret du 14 novembre 2014 relatif à la rupture de l'engagement de servir des anciens élèves de l'Ecole nationale d'administration ; que M. A...a formé un recours gracieux contre cette décision auprès du ministre des finances et des comptes publics le 17 juin 2015 ; que M. A...demande l'annulation pour excès de pouvoir de la décision du 20 novembre 2014, du décret du 19 mars 2015 prononçant sa radiation et de la décision implicite de rejet du recours administratif formé le 17 juin 2015 ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de la lettre du 20 novembre 2014 :<br/>
<br/>
              2.	Considérant que la lettre du 20 novembre 2014 par laquelle la directrice des ressources humaines du ministère des finances et des comptes publics et du ministère de l'économie, de l'industrie et du numérique a informé M. A...de ce que les ministres n'avaient pas réservé de suite favorable à sa demande et que sa radiation des cadres allait être proposée aux ministres n'a qu'un caractère préparatoire et ne constitue pas une décision susceptible de faire l'objet d'un recours pour excès de pouvoir ; que les conclusions de M. A...tendant à son annulation sont, par suite, irrecevables ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation du décret du 19 mars 2015 : <br/>
<br/>
              Sur les fins de non-recevoir opposées par le ministre de l'économie et des finances :<br/>
<br/>
              3.	Considérant qu'aux termes de l'article R. 421-1 du code de justice administrative, dans sa version alors applicable : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. / La publication, sous forme électronique, au Journal officiel de la République fait courir le délai du recours contentieux ouvert aux tiers contre les décisions individuelles : 1° Relatives au recrutement et à la situation des fonctionnaires et agents publics, des magistrats ou des militaires (...) " ; qu'en vertu de l'article R. 421-7 du même code, ce délai est augmenté de deux mois pour les personnes qui demeurent ...à l'étranger; <br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier que si le décret du Président de la République portant radiation de M. A...du corps des administrateurs civils et décidant de l'astreindre au paiement de l'indemnité litigieuse a été publié au Journal officiel, le délai de recours ne pouvait courir à son égard qu'à compter de la notification de cette décision individuelle ; que si un courriel a été adressé à M. A...pour lui communiquer l'extrait du Journal officiel, la lettre recommandée avec accusé de réception assurant la notification lui a été présentée le 7 avril 2015 à Bruxelles, où il réside ; que, faute pour M. A...d'avoir retiré ce pli, la décision individuelle doit être regardée comme ayant été régulièrement notifiée le 7 avril 2015 ; que, compte tenu de l'augmentation du délai de deux mois prévue à l'article R. 421-7 du code de justice administrative, contrairement à ce que soutient le ministre de l'économie et des finances, le délai de recours contentieux n'était pas expiré lorsque M. A...a présenté son recours gracieux au ministre des finances et des comptes publics, le 17 juin 2015 ;<br/>
<br/>
              5.	Considérant que, contrairement à ce que soutient le ministre, M. A...a intérêt à demander l'annulation du décret litigieux ;<br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              6.	Considérant qu'aux termes de l'article 24 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " La cessation définitive de fonctions qui entraîne radiation des cadres et perte de la qualité de fonctionnaire résulte : / (...) 2° De la démission régulièrement acceptée (...) " ; qu'aux termes de l'article 58 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions des fonctionnaires de l'État et à certaines modalités de cessation définitive de fonctions : " La démission ne peut résulter que d'une demande écrite de l'intéressé marquant sa volonté expresse de quitter son administration ou son service. Elle n'a d'effet qu'autant qu'elle est acceptée par l'autorité investie du pouvoir de nomination et prend effet à la date fixée par cette autorité. La décision de l'autorité compétente doit intervenir dans le délai de quatre mois à compter de la réception de la demande de démission " ;<br/>
<br/>
              7.	Considérant, d'une part, que, eu égard à la portée d'une démission et à l'exigence, posée par la loi du 13 juillet 1983, qu'elle soit régulièrement acceptée, il résulte des dispositions précitées du décret du 16 septembre 1985 que, si l'autorité investie du pouvoir de nomination dispose d'un délai de quatre mois pour notifier une décision expresse d'acceptation ou de refus, sans que puisse naître, à l'intérieur de ce délai, une décision implicite de rejet, elle se trouve dessaisie de l'offre de démission à l'expiration de ce délai, dont le respect constitue une garantie pour le fonctionnaire, et ne peut alors se prononcer légalement que si elle est à nouveau saisie dans les conditions prévues par l'article 58 du décret précité ;<br/>
<br/>
              8.	Considérant, d'autre part, que, dans l'hypothèse où l'autorité compétente ne s'est pas prononcée dans le délai de quatre mois, elle doit être regardée comme ayant refusé de statuer sur l'offre de démission du fonctionnaire ;<br/>
<br/>
              9.	Considérant qu'il ressort des pièces du dossier que M. A... a régulièrement présenté sa démission du corps des administrateurs civils, ainsi qu'il a été dit, par une lettre du 23 juillet 2013 ; que l'administration disposait d'un délai de quatre mois, à compter de cette date, pour lui notifier une décision d'acceptation ou de refus ; que le décret contesté, qui vise la lettre de démission de M.A..., et doit donc être regardé dans l'ensemble des circonstances de l'espèce comme ayant entendu tirer les conséquences de l'offre de l'intéressé, est intervenu le 19 mars 2015, soit après l'expiration du délai de quatre mois prévu par les dispositions précitées du décret du 16 septembre 1985, et est, pour ce seul motif, illégal ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête , M. A...est fondé à demander l'annulation du décret du 19 mars 2015 ainsi que, par voie de conséquence, de la décision implicite de rejet de son recours administratif formé le 17 juin 2015 ;<br/>
<br/>
              10.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le décret du 19 mars 2015 et la décision implicite de rejet du recours gracieux formé par M. A...le 17 juin 2015 sont annulés.<br/>
Article 2 : L'Etat versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
