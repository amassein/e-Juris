<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021630654</ID>
<ANCIEN_ID>JG_L_2009_12_000000306173</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/63/06/CETATEXT000021630654.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 30/12/2009, 306173, Publié au recueil Lebon</TITRE>
<DATE_DEC>2009-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>306173</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Mireille  Imbert-Quaretta</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boucher Julien</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2009:306173.20091230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 4 juin et 4 septembre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE EXPERIAN, dont le siège est immeuble Le Triangle de l'Arche 8, cours du Triangle à La Défense (92937) ; la SOCIETE EXPERIAN demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération n° 2007-044 du 8 mars 2007 par laquelle la Commission nationale de l'informatique et des libertés a refusé la création par la société d'un traitement automatisé ayant pour finalité la mise en place d'une centrale de crédit ;<br/>
<br/>
              2°) d'enjoindre à la Commission nationale de l'informatique et des libertés de délivrer à la SOCIETE EXPERIAN l'autorisation demandée dans un délai de deux mois à compter de la notification de la décision à intervenir ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention n° 108 du Conseil de l'Europe pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel signée à Strasbourg le 28 janvier 1981 ;<br/>
<br/>
              Vu la directive 95/46/CE du Parlement européen et du Conseil du 24 octobre 1995 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et la libre circulation de ces données ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978 ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Imbert-Quaretta, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la SOCIETE EXPERIAN WESTERN EUROPE,<br/>
<br/>
              - les conclusions de M. Julien Boucher, Rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la SOCIETE EXPERIAN WESTERN EUROPE.<br/>
<br/>
<br/>
<br/>
<br/>Considérant que la SOCIETE EXPERIAN, société de services, a présenté le 12 décembre 2006 à la Commission nationale de l'informatique et des libertés une demande d'autorisation portant sur un traitement automatisé de données à caractère personnel, consistant dans la mise en oeuvre d'une " centrale de crédit " ayant pour objectif, selon les écritures de la société, de favoriser le développement du crédit notamment auprès des populations qui en sont traditionnellement exclues, sans risquer le surendettement ; que ce traitement se caractérise par le partage, entre des établissements de crédit, d'informations sur l'état des encours de crédit d'une personne physique ; que par délibération du 8 mars 2007, la Commission nationale de l'informatique et des libertés a refusé la création de ce traitement ; que la SOCIETE EXPERIAN WESTERN EUROPE, venant aux droits de la SOCIETE EXPERIAN, demande l'annulation de cette délibération ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              Considérant qu'aux termes de l'article 25 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " I. Sont mis en oeuvre après autorisation de la Commission nationale de l'informatique et des libertés, à l'exclusion de ceux qui sont mentionnés aux articles 26 et 27 : (...) / 4° Les traitements automatisés susceptibles, du fait de leur nature, de leur portée ou de leurs finalités, d'exclure des personnes du bénéfice d'un droit, d'une prestation ou d'un contrat en l'absence de toute disposition législative ou réglementaire (...) " ; que le traitement présenté par la SOCIETE EXPERIAN WESTERN EUROPE permet aux établissements bancaires qui ont adhéré à cette " centrale " de disposer, avant tout octroi de prêt ou de crédit à un client, des renseignements sur les crédits attribués à celui-ci par l'ensemble des autres établissements bancaires et, donc, de refuser sur la base des informations ainsi collectées ce crédit ou ce prêt ; qu'ainsi, ce traitement, dès lors qu'il est susceptible d'avoir ce résultat, est au nombre de ceux qui, au sens du 4° du I de l'article 25 de la loi du 6 janvier 1978, sont susceptibles par leur nature d'exclure des personnes du bénéfice d'un droit, d'une prestation ou d'un contrat ; que dès lors, le traitement en cause ne pouvait être mis en oeuvre qu'après autorisation de la Commission nationale de l'informatique et des libertés ;<br/>
<br/>
              Considérant qu'en application des articles 2 et 3 du décret du 20 octobre 2005 pris pour l'application de la loi du 6 janvier 1978, d'une part, la commission délibère valablement lorsque la majorité de ses membres en exercice sont présents et, d'autre part, ses délibérations sont prises à la majorité des membres présents ou, pour les autorisations portant sur la création de traitements mentionnés à l'article 25 de la loi du 6 janvier 1978, à la majorité des membres composant la commission ; que, par suite, le moyen tiré de ce que les textes applicables imposeraient pour la validité des délibérations la présence de la totalité des membres de la commission doit être écarté ;<br/>
<br/>
              Considérant que la délibération en date du 8 mars 2007, qui énonce les éléments de fait et de droit sur lesquels elle se fonde, est suffisamment motivée au regard des exigences des articles 1er et 3 de la loi du 11 juillet 1979 ;<br/>
<br/>
              Considérant que, contrairement à ce qui est soutenu, la mission de conseil et d'information exercée par la Commission nationale de l'informatique et des libertés en application du 1° et du d) du 2° de l'article 11 de la loi du 6 janvier 1978, qui est distincte de l'exercice de la mission définie au a) du 2° du même article selon laquelle elle autorise les traitements mentionnés à l'article 25, est une mission générale dont les termes ne posent pas une règle de procédure applicable à l'instruction des dossiers individuels ; que, par suite, la société requérante ne saurait utilement invoquer à l'encontre de la décision contestée la circonstance que celle-ci serait fondée sur des motifs qui n'auraient pas été étudiés au cours des contacts intervenus entre la SOCIETE EXPERIAN WESTERN EUROPE et des membres de la commission lors de l'instruction de la demande ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              Considérant, en premier lieu, que si la société requérante soutient que la commission aurait commis une erreur de droit en estimant que le transfert d'informations couvertes par le secret bancaire ne peut être autorisé que par le législateur et que le traitement était illicite au sens de la loi du 6 janvier 1978 comme portant sur des données ne pouvant être collectées, il ressort des termes mêmes de la décision attaquée que la commission ne s'est pas fondée sur de tels motifs pour refuser la délivrance de l'autorisation sollicitée ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : / 1° Les données sont collectées et traitées de manière loyale et licite ; / 2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités (...) " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que la " centrale de crédit " exploitée par la société requérante est alimentée par les données fournies par les établissements bancaires adhérents ; que les informations sont recueillies par ces derniers auprès de leurs clients à l'occasion d'une demande de prêt ou d'ouverture de crédit ;<br/>
<br/>
              Considérant que le client d'une banque peut toujours renoncer à la protection du secret bancaire à condition d'y consentir librement et de manière expresse au vu d'une information complète lui permettant de prendre sa décision de manière éclairée ; qu'en l'espèce, d'une part, le client est invité à donner son accord au transfert d'informations le concernant, qui sont couvertes par le secret bancaire, avant qu'il lui soit répondu à sa demande de prêt ou de crédit ; que, d'autre part, la notice d'information qui lui est remise ne précise ni les données exactes qui seront transmises à la " centrale de crédit ", ni les utilisations de ces données par les établissements qui en seront destinataires, ni le délai de conservation de ces données dans la " centrale de crédit " ; qu'ainsi, la Commission nationale de l'informatique et des libertés, en estimant que les conditions qui viennent d'être rappelées pour que le consentement de l'emprunteur soit libre et éclairé n'étaient pas réunies et, qu'en conséquence, les informations sur la situation financière de ce dernier n'étaient pas recueillies de manière licite, n'a pas fait une inexacte application des dispositions précitées de la loi du 6 janvier 1978 ;<br/>
<br/>
              Considérant, en troisième lieu, que selon le 3° de l'article 6 de la loi du 6 janvier 1978, les données à caractère personnel pouvant faire l'objet d'un traitement doivent être " adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier qu'il n'existe aucun engagement de la part des établissements bancaires adhérents de la "  centrale de crédit " de ne pas utiliser les données qui leur seront communiquées pour une autre finalité que l'appréciation financière des demandeurs de crédit ; que cette incertitude sur l'utilisation ultérieure des données est aggravée par les caractéristiques du projet qui maintient, sans justification évidente, les données sur les emprunteurs dans les fichiers de la centrale trois ans après la fin du remboursement du crédit alors, au surplus, que le projet prend en compte les crédits immobiliers dont la durée est très longue ; qu'en conséquence, la commission n'a pas fait une inexacte application des dispositions de l'article 6 de la loi du 6 janvier 1978 en estimant que les données ainsi recueillies qui pouvaient être utilisées à d'autres fins que celle pour laquelle la demande d'autorisation a été présentée, et notamment à des fins commerciales, n'étaient ni adéquates, ni pertinentes et avaient un caractère excessif par rapport au but en vue duquel la collecte des données est envisagée ;<br/>
<br/>
              Considérant que la société requérante ne critique pas utilement la délibération de la Commission nationale de l'informatique et des libertés au regard du principe de libre concurrence, de celui de libre prestation de service et des objectifs de la directive 95/46/CE du 24 octobre 1995 ;<br/>
<br/>
              Considérant que la circonstance, qui n'est d'ailleurs étayée par aucun élément et aucune argumentation, que la Commission nationale de l'informatique et des libertés aurait délivré des autorisations pour des projets de traitements semblables ne constituerait pas, par elle-même, une violation du principe d'égalité ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE EXPERIAN WESTERN EUROPE n'est pas fondée à demander l'annulation de la délibération de la Commission nationale de l'informatique et libertés en date du 8 mars 2007 ; que, par voie de conséquence, ses conclusions à fin d'injonction ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la SOCIETE EXPERIAN WESTERN EUROPE est rejetée.<br/>
<br/>
Article  2 : La présente décision sera notifiée à la SOCIETE EXPERIAN WESTERN EUROPE, à la Commission nationale de l'informatique et des libertés et à la ministre de l'économie, de l'industrie et de l'emploi.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26 CONTRIBUTIONS ET TAXES. IMPÔTS SUR LES REVENUS ET BÉNÉFICES. RÈGLES GÉNÉRALES. IMPÔT SUR LES BÉNÉFICES DES SOCIÉTÉS ET AUTRES PERSONNES MORALES. DÉTERMINATION DU BÉNÉFICE IMPOSABLE. - PROTECTION DES DONNÉES À CARACTÈRE PERSONNEL (LOI DU 6 JANVIER 1978) - DEMANDE D'AUTORISATION D'UN TRAITEMENT DE DONNÉES PRÉSENTÉE À LA CNIL - 1) MISSION DE CONSEIL ET D'INFORMATION DE LA CNIL (ART. 11, 1° ET 2°, D) - EXIGENCE PROCÉDURALE APPLICABLE AUX DEMANDES INDIVIDUELLES D'AUTORISATION - ABSENCE - 2) MODALITÉS DE COLLECTE ET DE TRAITEMENT DES DONNÉES - OBLIGATIONS DE LOYAUTÉ ET DE LICÉITÉ ET CARACTÈRE DÉTERMINÉ, EXPLICITE ET LÉGITIME DES FINALITÉS (ART. 6, 1° ET 2°) - MÉCONNAISSANCE - EXISTENCE - 3) NATURE DES DONNÉES COLLECTÉES - OBLIGATION DE STRICTE PROPORTIONNALITÉ AU REGARD DES FINALITÉS (ART. 6, 3°) - MÉCONNAISSANCE - EXISTENCE - CONSÉQUENCE - LÉGALITÉ DU REFUS D'AUTORISATION OPPOSÉ PAR LA CNIL.
</SCT>
<ANA ID="9A"> 26 Demande d'autorisation présentée à la Commission nationale de l'informatique et des libertés (CNIL) en vue de la création d'un traitement automatisé de données à caractère personnel ayant pour objet le partage, entre des établissements de crédit, d'informations sur l'état des encours de crédit d'une personne physique.... ...1) La mission de conseil et d'information exercée par la CNIL en application du 1° et du d) du 2° de l'article 11 de la loi du 6 janvier 1978, qui est distincte de l'exercice de la mission définie au a) du 2° du même article selon laquelle elle autorise certains traitements de données, est une mission générale dont les termes ne posent pas une règle de procédure applicable à l'instruction des dossiers individuels.... ...2) En vertu des 1° et 2° de l'article 6 de la loi du 6 janvier 1978, un traitement ne peut porter que sur des données à caractère personnel collectées et traitées de manière loyale et licite et pour des finalités déterminées, explicites et légitimes. Si le client d'une banque peut toujours renoncer à la protection du secret bancaire, c'est à la condition d'y consentir librement et de manière éclairée. En l'espèce, d'une part, le client est invité à donner son accord au transfert d'informations le concernant avant qu'il lui soit répondu à sa demande de prêt ou de crédit et, d'autre part, l'information qui lui est remise ne précise ni les données exactes faisant l'objet du transfert vers le traitement, ni les utilisations de ces données, ni leur délai de conservation. Dans ces conditions, les données en question ne peuvent pas être regardées comme recueillies de manière licite.,,3) En vertu du 3° de l'article 6 de la loi du 6 janvier 1978, les données à caractère personnel pouvant faire l'objet d'un traitement doivent être adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs. En l'espèce, il n'existe aucun engagement de la part des établissements bancaires de ne pas utiliser les données pour une autre finalité que l'appréciation de la situation financière des demandeurs de crédit, circonstance aggravée par la durée de conservation des données, qui s'étend jusqu'à trois ans après la fin du remboursement du crédit. Dans ces conditions, les données recueillies n'apparaissent ni adéquates, ni pertinentes et présentent un caractère excessif par rapport au but en vue duquel leur collecte est envisagée.,,En conséquence, la CNIL a pu légalement refuser d'autoriser le traitement dont la création était projetée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
