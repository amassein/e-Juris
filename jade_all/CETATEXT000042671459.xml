<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042671459</ID>
<ANCIEN_ID>JG_L_2020_12_000000438328</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/14/CETATEXT000042671459.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 08/12/2020, 438328</TITRE>
<DATE_DEC>2020-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438328</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:438328.20201208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
Procédures contentieuses antérieures<br/>
<br/>
              1° Le syndicat intercommunal d'adduction d'eau et d'assainissement (SIAEA) de Saint-Jean-d'Illac et de Martignas-sur-Jalle et la commune de Saint-Jean-d'Illac ont demandé au juge des référés du tribunal administratif de Bordeaux de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 20 décembre 2019 par lequel la préfète de la Gironde a prononcé la fin de l'exercice des compétences de ce syndicat. <br/>
<br/>
              Par une ordonnance n° 1906284 du 21 janvier 2020, le juge des référés a rejeté leur demande.<br/>
<br/>
              2° Le SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle a demandé au juge des référés du tribunal administratif de Bordeaux de suspendre, sur le fondement des mêmes dispositions, l'exécution de l'arrêté du 27 juin 2019 par lequel la préfète de la Gironde a autorisé le retrait de Bordeaux Métropole, en représentation-substitution de la commune de Martignas-sur-Jalle, du syndicat. <br/>
<br/>
              Par une ordonnance n° 1906285 du 21 janvier 2020, le juge des référés a rejeté leur demande.<br/>
<br/>
              3° Le SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle a demandé au juge des référés du tribunal administratif de Bordeaux de suspendre, sur le fondement des mêmes dispositions, l'exécution de la délibération du 22 mars 2019 par laquelle le conseil de l'établissement public Bordeaux Métropole a décidé de demander à l'autorité préfectorale d'engager la procédure de retrait du syndicat, aux fins d'en exercer lui-même les compétences à compter du 1er janvier 2020, et de la décision du 21 juin 2019 rejetant son recours gracieux contre cette délibération. <br/>
<br/>
              Par une ordonnance n° 1906286 du 21 janvier 2020, le juge des référés a rejeté leur demande.<br/>
<br/>
Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 438328, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 et 24 février 2020 au secrétariat du contentieux du Conseil d'Etat, le SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle et la commune de Saint-Jean-d'Illac demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1906284 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de Bordeaux Métropole la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              2° Sous le n° 438329, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 et 24 février 2020 au secrétariat du contentieux du Conseil d'Etat, le SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1906285 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de Bordeaux Métropole la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 438332, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 et 24 février 2020 au secrétariat du contentieux du Conseil d'Etat, le SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1906286 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de Bordeaux Métropole la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2015-991 du 7 août 2015 ;<br/>
              - la loi n° 2018-702 du 3 août 2018 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament, Robillot, avocat du syndicat intercommunal d'adduction d'eau et d'assainissement de Saint-Jean-d'Illac et de Martignas-sur-Jalle et de la commune de Saint-Jean-d'Illac et à la SCP Foussard, Froger, avocat de la Bordeaux Métropole ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois visés ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision... ".<br/>
<br/>
              3. Il ressort des pièces des dossiers soumis au juge des référés que, consécutivement à l'adhésion de la commune de Martignas-sur-Jalle à la communauté urbaine de Bordeaux, devenue Bordeaux Métropole, cette dernière s'est substituée à cette commune au sein du syndicat intercommunal d'adduction d'eau et d'assainissement (SIAEA) qu'elle formait avec la commune de Saint-Jean-d'Illac. Par une délibération du 22 mars 2019, le conseil métropolitain de Bordeaux Métropole a autorisé son président à demander au préfet de département, sur le fondement de l'article L. 5711-5 du code général des collectivités territoriales, d'autoriser le retrait de la métropole du SIAEA. La commune de Saint-Jean-d'Illac a formé un recours gracieux contre cette délibération, rejeté le 21 juin 2019. Par deux arrêtés des 27 juin et 20 décembre 2019, la préfète de la Gironde a respectivement autorisé le retrait de Bordeaux Métropole du SIAEA et mis fin à l'exercice des compétences du syndicat le 31 décembre 2019. Le SIAEA et la commune de Saint-Jean-d'Illac se pourvoient en cassation contre les ordonnances par lesquelles le juge des référés du tribunal administratif de Bordeaux a rejeté leurs demandes tendant à ce que l'exécution de ces décisions soit suspendue sur le fondement de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              En ce qui concerne la délibération du 22 mars 2019 et la décision de rejet du recours gracieux du 21 juin 2019 :<br/>
<br/>
              4. Il ressort des pièces des dossiers soumis au juge des référés que la délibération attaquée du conseil de Bordeaux Métropole n'a pas d'autre objet que de permettre l'engagement de la procédure d'autorisation préfectorale en application de l'article L. 5711-5 du code général des collectivités territoriales. Elle revêt, comme telle, le caractère d'une mesure préparatoire insusceptible de recours pour excès de pouvoir. La requête aux fins de suspension de cette délibération ne pouvait, dès lors, qu'être rejetée. Ce motif, qui est d'ordre public et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué aux motifs retenus par l'ordonnance attaquée, dont il justifie le dispositif. Par suite, le pourvoi du SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle dirigé contre l'ordonnance n° 1906286 du 21 janvier 2020 du juge des référés du tribunal administratif de Bordeaux doit être rejeté.<br/>
<br/>
              En ce qui concerne les arrêtés préfectoraux des 27 juin et 20 décembre 2019 :<br/>
<br/>
              5. Il ressort des pièces des dossiers soumis au juge des référés que le retrait de Bordeaux Métropole a été sollicité et autorisé au motif que sa participation au syndicat était devenue sans objet du fait du changement de réglementation opéré par la loi du 3 août 2018 relative à la mise en oeuvre du transfert des compétences eau et assainissement aux communautés de communes et que, par suite, les conditions fixées par l'article L. 5711-5 du code général des collectivités territoriales pour le retrait unilatéral d'un syndicat mixte étaient réunies. <br/>
<br/>
              6. Aux termes de l'article L. 5711-5 du code général des collectivités territoriales : " Une commune ou un établissement public de coopération intercommunale peut être autorisé par le représentant de l'Etat dans le département à se retirer d'un syndicat mixte si, à la suite d'une modification de la réglementation, de la situation de cette personne morale de droit public au regard de cette réglementation ou des compétences de cette personne morale, sa participation au syndicat mixte est devenue sans objet./ Le retrait est prononcé par arrêté du représentant de l'Etat dans le département dans un délai de deux mois à compter de la demande de la commune ou de l'établissement public ". Il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé leur adoption, que la participation d'une commune ou d'un établissement public de coopération intercommunale à un syndicat mixte devient sans objet dès lors que cette commune ou cet établissement ne dispose plus de la compétence au titre de laquelle il ou elle participait à ce groupement.<br/>
<br/>
              7. Il résulte par ailleurs des articles 64 et 66 de la loi du 7 août 2015 portant nouvelle organisation territoriale de la République, qui a renforcé le degré d'intégration des communautés de communes et des communautés d'agglomération, qu'à compter du 1er janvier 2020, l'eau et l'assainissement devaient devenir des compétences obligatoires des communautés de communes et d'agglomération. La loi a fixé des règles de représentation-substitution et de retrait des syndicats intercommunaux et des syndicats mixtes consécutifs à ce transfert de compétences : les syndicats d'eau potable et d'assainissement comprenant dans leur périmètre des communes appartenant à au moins trois établissements publics de coopération intercommunale à fiscalité propre pouvaient subsister, ces établissements se substituant alors aux communes membres au sein du syndicat ; pour les autres syndicats, les communes qui les composaient devaient voir leur compétence en matière d'eau transférée de plein droit à une communauté de communes, ce qui, en application du dernier alinéa du II de l'article L. 5214-21 du code général des collectivités territoriales dans sa version alors applicable, valait retrait de la commune du syndicat. <br/>
<br/>
              8. Toutefois l'article 1er de la loi du 3 août 2018 relative à la mise en oeuvre du transfert des compétences eau et assainissement aux communautés de communes a permis aux communes membres d'une communauté de communes qui n'exerce pas les compétences en matière d'eau et d'assainissement de s'opposer, sous certaines conditions et jusqu'au 1er janvier 2026, à leur transfert obligatoire prévu le 1er janvier 2020. La commune de Saint-Jean-d'Illac, qui appartient à la communauté de communes Jalle-Eau-Bourde, qui n'exerçait pas la compétence en matière d'eau et d'assainissement, s'est opposée à ce transfert, ce qu'elle a pu valablement faire, les conditions de délai et de pourcentage du nombre des communes membres et de population intercommunale requises par l'article 1er de la loi du 3 août 2018 étant remplies. Par suite, la commune de Saint-Jean-d'Illac ne s'est pas retirée du syndicat intercommunal d'eau et d'assainissement de Saint-Jean-d'Illac et de Martignas-sur-Jalle, comme cela aurait été le cas, en l'absence d'intervention de la loi de 2018, en application du II de l'article L. 5214-21 du code général des collectivités territoriales.<br/>
<br/>
              9. Il résulte de ce qui précède que la loi du 3 août 2018 ne saurait être regardée comme un changement de réglementation rendant sans objet la participation de Bordeaux Métropole à ce syndicat intercommunal puisque, d'une part, elle a permis au second membre du syndicat d'en rester membre et d'autre part, elle n' pas davantage eu pour objet ou pour effet de faire perdre à Bordeaux Métropole la compétence en matière d'eau et d'assainissement qu'elle tient de l'article L. 5215-20 du code général des collectivités territoriales et qu'elle exerce, au titre de la commune de Martignas-sur-Jalle, à travers ce syndicat. La participation de Bordeaux Métropole au SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle au titre de la commune de Martignas-sur-Jalle n'a en conséquence pas perdu son objet au sens de l'article L. 5711-5 du même code à raison de l'intervention de cette loi, motif des arrêtés contestés. <br/>
<br/>
              10. La commune et le syndicat requérants sont par suite fondés à soutenir qu'en jugeant que le moyen tiré de l'inapplicabilité de l'article L. 5711-5 du code général des collectivités territoriales n'était pas, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité des décisions contestées, le juge des référés du tribunal administratif de Bordeaux a commis une erreur de droit.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              12. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés, saisi de conclusions tendant à la suspension d'un acte administratif, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              13. Les arrêtés préfectoraux dont le SIAEA de Saint-Jean-d'Illac et de Martignas sur Jalle et la commune de Saint-Jean-d'Illac ont pour effet d'autoriser le retrait de Bordeaux Métropole du syndicat mixte et de mettre fin aux compétences du syndicat au 31 décembre 2019, préalablement à la dissolution du syndicat. La dissolution d'un établissement public de coopération intercommunale crée, par elle-même, une situation d'urgence à l'égard de l'établissement. Dans ces conditions, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie.<br/>
<br/>
              14. Ainsi qu'il a été dit au point 10, le moyen tiré de l'inapplicabilité au cas d'espèce de l'article L. 5711-5 du code général des collectivités territoriales est de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des décisions attaquées. <br/>
<br/>
              15. Il résulte de ce qui précède que le SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle et la commune de Saint-Jean-d'Illac sont fondés à demander la suspension des arrêtés de la préfète de la Gironde des 27 juin et 20 décembre 2019.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de Bordeaux Métropole la somme de 2 500 euros chacun à verser au SIAEA de Saint-Jean-d'Illac et de Martignas-sur-Jalle et la somme de 1 000 euros chacun à la commune de Saint-Jean-d'Illac au titre de l'ensemble de la procédure en application de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge du SIAEA de Saint Jean d'Illac et de Martignas-sur-Jalle et de la commune de Saint-Jean-d'Illac qui ne sont pas, dans les présentes instances, les parties perdantes. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi n° 438332 du syndicat d'adduction d'eau et d'assainissement de Saint-Jean-d'Illac et de Martignas-sur-Jalle est rejeté.<br/>
Article 2 : Les ordonnances n°s 1906284 et 1906285 du 21 janvier 2020 du juge des référés du tribunal administratif de Bordeaux sont annulées.<br/>
Article 3 : L'exécution des arrêtés des 27 juin et 20 décembre 2020 de la préfète de Gironde est suspendue.<br/>
Article 4 : L'Etat et Bordeaux Métropole verseront au syndicat d'adduction d'eau et d'assainissement de Saint-Jean-d'Illac et de Martignas-sur-Jalle la somme de 2 500 euros chacun et à la commune de Saint-Jean-d'Illac la somme de 1 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par Bordeaux Métropole au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée au syndicat d'adduction d'eau et d'assainissement de Saint-Jean-d'Illac et de Martignas-sur-Jalle, à la commune de Saint-Jean-d'Illac, à Bordeaux Métropole et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée à la préfète de Gironde.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. - PARTICIPATION D'UNE COMMUNE OU D'UN EPCI À UN SYNDICAT MIXTE FERMÉ (ART. L. 5711-1 DU CGT) - PERTE D'OBJET (ART. L. 5711-5 DU CGCT) - NOTION - PERTE DE LA COMPÉTENCE JUSTIFIANT CETTE PARTICIPATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-05-05 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. SYNDICATS MIXTES. - SYNDICAT MIXTE FERMÉ (ART. L. 5711-1 DU CGT) - PARTICIPATION D'UNE COMMUNE OU D'UN EPCI - PERTE D'OBJET (ART. L. 5711-5 DU CGCT) - NOTION - PERTE DE LA COMPÉTENCE JUSTIFIANT CETTE PARTICIPATION [RJ1].
</SCT>
<ANA ID="9A"> 135-01 Il résulte de l'article L. 5711-5 du code général des collectivités territoriales (CGCT), éclairé par les travaux parlementaires qui ont précédé son adoption, que la participation d'une commune ou d'un établissement public de coopération intercommunale (EPCI) à un syndicat mixte devient sans objet, ce qui lui ouvre la possibilité d'être autorisée à s'en retirer, dès lors que cette commune ou cet établissement ne dispose plus de la compétence au titre de laquelle il ou elle participait à ce groupement.</ANA>
<ANA ID="9B"> 135-05-05 Il résulte de l'article L. 5711-5 du code général des collectivités territoriales (CGCT), éclairé par les travaux parlementaires qui ont précédé son adoption, que la participation d'une commune ou d'un établissement public de coopération intercommunale (EPCI) à un syndicat mixte devient sans objet, ce qui lui ouvre la possibilité d'être autorisée à s'en retirer, dès lors que cette commune ou cet établissement ne dispose plus de la compétence au titre de laquelle il ou elle participait à ce groupement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'obligation de retrait d'un syndicat mixte ouvert, CE, 13 décembre 2017, Assemblée des départements de France, n° 406563, T. pp. 484-492.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
