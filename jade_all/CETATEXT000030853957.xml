<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853957</ID>
<ANCIEN_ID>JG_L_2015_07_000000378064</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853957.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 03/07/2015, 378064, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378064</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:378064.20150703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...ont demandé au tribunal administratif de Nantes la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles ils ont été assujettis au titre des années 2004 à 2007, ainsi que des pénalités correspondantes. Par un jugement n° 0903863-0905912 du 7 février 2013, le tribunal administratif de Nantes a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13NT00973 du 13 février 2014, la cour administrative d'appel de Nantes a rejeté l'appel que M. et Mme B...ont formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 18 avril 2014, 18 juillet 2014 et 10 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. et Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B... ont été assujettis à des compléments d'impôt sur le revenu au titre des années 2004 et 2005, en application des dispositions de l'article 8 du code général des impôts, à raison du rehaussement du résultat de la société civile d'exploitation agricole (SCEA) VignoblesB..., qui exploite un domaine viticole à Rochefort-sur-Loire (Maine-et-Loire), dont M. B... est l'associé majoritaire et le gérant. Les contribuables ont en outre été assujettis, à la suite d'un contrôle sur pièces, à des cotisations supplémentaires d'impôt sur le revenu au titre des années 2006 et 2007 résultant de l'annulation des reports de déficits constatés au titre des années 2004 et 2005. Ils se pourvoient en cassation contre l'arrêt du 13 février 2014 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'ils ont formé contre le jugement du 7 février 2013 du tribunal administratif de Nantes rejetant leur demande tendant à la décharge de ces compléments d'impôt ainsi que des pénalités correspondantes.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur la régularité de la procédure d'imposition :<br/>
<br/>
              2. En jugeant, après avoir relevé que la vérification de comptabilité de la société Vignobles B...avait eu lieu dans les locaux de celle-ci et qu'il incombait dès lors à M. et MmeB..., qui soutenaient que la société n'avait pas eu de débat oral et contradictoire avec le vérificateur, d'établir que ce dernier se serait refusé à tout échange de vues avec elle, que les requérants n'apportaient pas cette preuve en se bornant à reprocher au vérificateur, qui n'y était pas tenu par la loi fiscale, de ne pas avoir évoqué, lors de la réunion de synthèse du 19 juillet 2007, les motifs l'ayant conduit à écarter comme non probante la comptabilité, alors qu'une quinzaine de rencontres avaient été organisées durant le contrôle, la cour, qui a suffisamment motivé son arrêt sur ce point, ne l'a entaché d'aucune dénaturation et n'a pas commis d'erreur de droit.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur le bien-fondé de l'imposition :<br/>
<br/>
              En ce qui concerne les compléments d'impôt sur le revenu assignés à M. et MmeB... :<br/>
<br/>
              3. En premier lieu, la cour n'a pas dénaturé les pièces du dossier soumis aux juges du fond en relevant, pour juger que le vérificateur était fondé à ne pas admettre le caractère régulier et probant de la comptabilité de la société VignoblesB..., d'une part, que la société n'avait pas conservé sur un support informatique l'enregistrement des stocks et de certaines pièces justificatives de recettes et qu'elle avait enregistré certaines factures avec une numérotation non chronologique et, d'autre part, qu'elle avait volontairement et artificiellement majoré l'état d'inventaire établi au titre de l'année 2004.<br/>
<br/>
              4. En deuxième lieu, si la cour n'a mentionné dans son arrêt que le prix moyen de 15,41 euros hors taxes, retenu pour l'année 2004, en omettant de faire référence au prix moyen de 13,82 euros hors taxes, retenu pour l'année 2005, elle a suffisamment motivé son arrêt en relevant que les requérants ne contestaient pas que les tarifs de vente de vin au litre dont ils demandaient la prise en compte tant pour la reconstitution des recettes de la société que pour la taxation des cadeaux d'affaires non justifiés avaient été calculés à partir de données issues d'une comptabilité écartée comme dénuée de valeur probante, et en en déduisant que les contribuables ne justifiaient pas que l'administration fiscale aurait retenu à tort le prix moyen résultant de l'examen des factures délivrées aux clients particuliers et professionnels.<br/>
<br/>
              5. Il ressort toutefois des pièces du dossier soumis aux juges du fond et des énonciations de l'arrêt attaqué que le vérificateur a reconstitué l'état de stock de vin de la société Vignobles B...au 31 décembre 2004 à partir du stock physique constaté par le service des douanes dans un procès-verbal établi le 16 novembre 2004, duquel il a retiré les quantités de vin vendues entre le 17 novembre et le 31 décembre 2004, telle qu'elles ressortaient de l'exploitation d'un fichier informatique des ventes, et auquel il a ajouté la production de vin mentionnée par la société dans la déclaration de récolte souscrite au titre de l'année 2004. Ayant estimé que la différence de 277,30 hectolitres constatée entre le stock au 31 décembre 2004 déclaré par la société et le stock reconstitué correspondait nécessairement à des ventes de vin non retranscrites en comptabilité, il a donc rehaussé le résultat de la société à concurrence du montant de ces recettes et, par voie de conséquence, rehaussé les bénéfices agricoles de M. et Mme B...à proportion des droits de M. B...dans la société.<br/>
<br/>
              6. En s'abstenant de viser et de répondre au moyen soulevé dans le mémoire en réplique enregistré le 18 décembre 2013 et tiré de ce que la reconstitution du chiffre d'affaires de la société Vignobles B...procédait d'une méthode radicalement viciée dans son principe, la cour a entaché son arrêt d'insuffisance de motivation.<br/>
<br/>
              En ce qui concerne les pénalités :<br/>
<br/>
              7. Il résulte de ce qui a été dit au point 6 que M. et Mme B...sont fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant que la cour a statué sur l'application de l'ensemble des pénalités pour manquement délibéré aux compléments d'impôt sur le revenu auxquels ils ont été assujettis.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. et Mme B...sont seulement fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant que, par cet arrêt, la cour a statué, d'une part, sur les compléments d'impôt sur le revenu auxquels ils ont été assujettis au titre de l'année 2004, résultant du rehaussement du résultat de la société Vignobles B...en conséquence de la prise en compte des recettes correspondant à la différence entre le stock de vin déclaré au 31 décembre 2004 et le stock reconstitué par l'administration ainsi que, s'il y a lieu et par voie de conséquence, des compléments d'impôt sur le revenus au titre des années 2005, 2006 et 2007 résultant de la remise en cause de reports de déficits,  d'autre part, l'ensemble des pénalités mises à la charge de M. et MmeB.... <br/>
<br/>
              Sur les conclusions de M. et Mme B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la l'Etat la somme de 1 000 euros à verser à M. et Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 13 février 2014 de la cour administrative d'appel de Nantes est annulé en tant qu'il a statué sur les compléments d'impôt sur le revenu auxquels M. et Mme B...ont été assujettis au titre de l'année 2004, résultant du rehaussement du résultat de la société Vignobles B...en conséquence de la prise en compte des recettes correspondant à la différence entre le stock de vin déclaré au 31 décembre 2004 et le stock reconstitué par l'administration, sur l'ensemble des pénalités et, le cas échéant, sur les compléments d'impôt sur le revenu qui leur ont été assignés au titre des années 2005, 2006 et 2007, dans la mesure où la présente décision l'impliquerait, eu égard aux reports de déficits remis en cause par l'administration.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée à l'article 1er, à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : L'Etat versera à M. et Mme B...une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente ordonnance sera notifiée à M. et Mme A...B...et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
