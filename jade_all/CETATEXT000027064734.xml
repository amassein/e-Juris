<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064734</ID>
<ANCIEN_ID>JG_L_2013_02_000000350445</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064734.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 13/02/2013, 350445, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350445</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:350445.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 juin et 27 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA Oséo Financement, venant aux droits de la société Oséo BDPME, dont le siège est 27/31 avenue du Général Leclerc, à Maisons-Alfort (94170) ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n°s 1002642-1002643 du 28 avril 2011 par lequel le tribunal administratif de Montpellier a rejeté ses demandes tendant à la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles la société Oséo BDPME a été assujettie au titre des années 2007, 2008 et 2009 pour des locaux situés à Castelnaudary ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la SA Oséo Financement,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la SA Oséo Financement ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumises au juge du fond que la société Oséo BDPME, devenue la société Oséo Financement, a été assujettie à la taxe foncière sur les propriétés bâties au titre des années 2007 à 2009 à raison d'un ensemble immobilier situé à Castelnaudary (Aude) qu'elle a acquis de la société Spanghero par acte du 26 juillet 2006, compte tenu d'une valeur locative établie sur le fondement de l'article 1518 B du code général des impôts ; que la société Oséo Financement se pourvoit en cassation contre le jugement du 28 avril 2011 par lequel le tribunal administratif de Montpellier a rejeté ses demandes tendant à la réduction de ces impositions ; <br/>
<br/>
               2. Considérant qu'au terme de l'article 1518 B du code général des impôts, dans sa rédaction applicable à l'imposition en litige :  " A compter du 1er janvier 1980, la valeur locative des immobilisations corporelles acquises à la suite d'apports, de scissions, de fusions de sociétés ou de cessions d'établissements réalisés à partir du 1er janvier 1976 ne peut être inférieure aux deux tiers de la valeur locative retenue l'année précédant l'apport, la scission, la fusion ou la cession . / Les dispositions du premier alinéa s'appliquent aux seules immobilisations corporelles directement concernées par l'opération d'apport, de scission, de fusion ou de cession, dont la valeur locative a été retenue au titre de l'année précédant l'opération (...) / Pour les opérations mentionnées au premier alinéa réalisées à compter du 1er janvier 1992, la valeur locative des immobilisations corporelles ne peut être inférieure aux quatre cinquièmes de son montant avant l'opération (...) / Les dispositions du présent article s'appliquent distinctement aux trois catégories d'immobilisations suivantes : terrains, constructions, équipements et biens mobiliers (...) " ; que, pour l'application de ces dispositions, un établissement doit être regardé comme ayant fait l'objet d'une cession lorsque le même redevable a acquis l'ensemble des éléments mobiliers et immobiliers, qui étaient nécessaires à l'activité exercée par le cédant, en vue d'y exercer avec ces moyens sa propre activité  ; <br/>
<br/>
              3. Considérant que, pour soutenir que la vente ne portait que sur des terrains et des bâtiments et ne pouvait être assimilée à une cession d'établissement au sens de l'article 1518 B du code général des impôts, la société a fait valoir, devant le tribunal administratif, que l'acte du 26 juillet 2006 se bornait à décrire les locaux en fonction de leur usage sans mentionner qu'étaient également cédés l'outillage et les installations qu'ils abritaient, que les biens cédés étaient exhaustivement et limitativement énumérés et que la valeur des installations techniques, matériels et outillages industriels inscrits au bilan, dont un tableau était fourni, était supérieure après la vente à la situation antérieure ; qu'en se bornant à mentionner la liste des locaux cédés figurant dans l'acte pour en déduire que la cession avait porté sur un établissement au sens de l'article 1518 B, sans répondre à l'ensemble de l'argumentation dont il était saisi, le tribunal administratif a insuffisamment motivé son jugement ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société requérante est fondée à en demander l'annulation ;<br/>
<br/>
              4. Considérant  qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Oséo Financement de la somme de 3 000 euros, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le jugement du 28 avril 2011 du tribunal administratif de Montpellier est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montpellier.<br/>
<br/>
Article 3 : L'Etat versera à la SA Oséo Financement la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SA Oséo Financement et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
