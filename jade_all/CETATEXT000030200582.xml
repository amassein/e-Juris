<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200582</ID>
<ANCIEN_ID>JG_L_2015_01_000000381838</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200582.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 16/01/2015, 381838, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381838</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Méar</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:381838.20150116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une protestation, enregistrée le 6 juin 2014 au consulat général de France à Pondichéry et des mémoires enregistrés les 17 et 30 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'élection de M. B... et de Mme D..., candidats de la liste " Ensemble pour faire entendre la voix des Français de l'étranger ", qui a été prononcée à l'issue des opérations électorales qui ont eu lieu le 25 mai 2014 dans la circonscription de Pondichéry pour la désignation des conseillers consulaires, puis de répartir les deux sièges obtenus par cette liste sur la base des voix obtenues par les autres listes de candidats ;<br/>
<br/>
              2°) de déclarer M. B... et Mme D...inéligibles en application de l'article L. 118-4 du code électoral ;<br/>
<br/>
              3°) à titre subsidiaire, d'attribuer à la liste " Union des Français indépendants " le deuxième siège obtenu par la liste " Ensemble pour faire entendre la voix des Français de l'étranger " ;<br/>
<br/>
              4°) de prononcer contre la liste " Ensemble pour faire entendre la voix des Français de l'étranger " les sanctions par l'article L. 113-1 du code électoral.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2013-659 du 22 juillet 2013 ;<br/>
              - le décret n° 2014-290 du 4 mars 2014 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Méar, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales en vue de l'élection des conseillers consulaires qui se sont déroulées le 25 mai 2014 dans la circonscription de Pondichéry, deux des trois sièges à pourvoir ont été attribués à la liste " Ensemble pour faire entendre la voix des Français de l'étranger " qui a recueilli 760 voix. Le troisième siège a été attribué la liste " Solidarité pour le progrès " qui a recueilli 670 voix. Les cinq autres listes en présence, qui ont recueilli, chacune, entre 73 et 335 voix, n'ont obtenu aucun siège de conseiller consulaire.<br/>
<br/>
              2. M. A..., tête de la liste " Union des Français indépendants " demande, à titre principal, l'annulation de l'élection de M. B... et de MmeD..., candidats de la liste " Ensemble pour faire entendre la voix des Français de l'étranger ", que ces deux personnes soient déclarés inéligibles en application de l'article L. 118-4 du code électoral et que les deux sièges qu'ils ont obtenus soient attribués sur la base des voix obtenues par les autres listes en présence. A titre subsidiaire, il demande l'attribution à la liste électorale " Union des Français indépendants " du deuxième siège obtenu par la liste " Ensemble pour faire entendre la voix des Français de l'étranger ". Il demande enfin que soient prononcées les sanctions prévues par l'article L. 113-1 du code électoral contre les membres de liste " Ensemble pour faire entendre la voix des Français de l'étranger ".<br/>
<br/>
              Sur les conclusions tendant à la réformation des résultats du scrutin :<br/>
<br/>
              3. En premier lieu, si M. A... soutient que la liste " Ensemble pour faire entendre la voix des Français de l'étranger " a eu recours, à plusieurs reprises, à des messages publicitaires dans les chaînes de télévision locale, il n'apporte aucun élément probant au soutien de cette allégation.<br/>
<br/>
              4. En deuxième lieu, M. A...soutient que M. B..., tête de la liste " Ensemble pour faire entendre la voix des Français de l'étranger ", a fait insérer le 20 février 2014, en violation des dispositions de l'article L. 52-1 du code électoral applicable à l'élection des conseillers consulaires en vertu de l'article 15 de la loi du 22 juillet 2013 relative à la représentation des Français établis hors de France, un encart publicitaire dans le supplément d'un journal quotidien de Pondichéry. Il résulte toutefois de l'instruction que cet encart ne contenait, pour l'essentiel, que l'annonce de la candidature de la liste " Ensemble pour faire entendre la voix des Français de l'étranger " et l'exposé de considérations de politique générale. Il ne contenait aucun élément de polémique électorale. Dans ces conditions, cette insertion ne saurait en tout état de cause être regardée comme ayant porté atteinte à la sincérité du scrutin.<br/>
<br/>
              5. En troisième lieu, M. A... soutient que les listes " Solidarité pour le progrès " et " Ensemble pour faire entendre la voix des Français de l'étranger " ont eu recours eu recours à des pressions pour recueillir des procurations de vote de personnes en situation de faiblesse. Le requérant n'apporte au soutien de cette allégation aucune précision permettant d'en apprécier le bien-fondé.<br/>
<br/>
              6. En quatrième lieu, M. A... n'établit pas, par les documents qu'il produit à l'appui de sa protestation, que des membres ou des partisans des listes " Solidarité pour le progrès " et " Ensemble pour faire entendre la voix des Français de l'étranger " ont exercé sur les électeurs se présentant dans les bureaux de vote des pressions qui auraient été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 118-4 du code électoral :<br/>
<br/>
              7. Aux termes de l'article L. 118-4 du code électoral, applicable à l'élection des conseillers consulaires en vertu de l'article 15 de la loi du 22 juillet 2013 relative à la représentation des Français établis hors de France : " Saisi d'une contestation formée contre l'élection, le juge de l'élection peut déclarer inéligible, pour une durée maximale de trois ans, le candidat qui a accompli des manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin (...) ". En l'absence de manoeuvres frauduleuses ayant eu pour objet ou pour effet de porter atteinte à la sincérité du scrutin imputables à M. B... ou à MmeD..., il n'y a pas lieu de les déclarer inéligibles en application des dispositions précitées de l'article L. 118-4 du code électoral.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 113 du code électoral :<br/>
<br/>
              8. La juridiction administrative est incompétente pour connaître des conclusions de la protestation de M. A... tendant à ce qu'il soit fait application au membres de la liste " Ensemble pour faire entendre la voix des Français de l'étranger " des dispositions pénales de l'article L. 113 du code électoral.<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que l'ensemble des conclusions de la protestation de M. A... devant le Conseil d'Etat doit être rejeté.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B... au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La protestation de M. A... est rejetée. Ses conclusions tendant à l'application des dispositions de l'article L. 113 du code électoral sont rejetées comme portées devant une juridiction incompétente pour en connaître.<br/>
Article 2 : Les conclusions présentées par M. B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A..., à M. B..., à M. C... et au ministre des affaires étrangères et du développement international.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
