<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220701</ID>
<ANCIEN_ID>JG_L_2018_07_000000410964</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220701.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/07/2018, 410964, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410964</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410964.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 410964, par une requête sommaire, un mémoire additionnel, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 mai, 4 et 30 août 2017 et 20 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société Europe 2 Entreprises demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la mise en garde du président du Conseil supérieur de l'audiovisuel (CSA) du 5 décembre 2016 concernant des manquements à ses obligations conventionnelles de diffusion de titres et de nouveaux talents francophones, ensemble la décision du 9 juin 2017 par laquelle le CSA a rejeté son recours gracieux formé contre cette mise en garde ;<br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              2° Sous le n° 411140, par une requête sommaire et un mémoire complémentaire, enregistrés les 2 juin et 1er septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société SERC Fun Radio demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la mise en garde du président du Conseil supérieur de l'audiovisuel (CSA) du 5 décembre 2016 concernant des manquements à ses obligations conventionnelles de diffusion de titres et de nouveaux talents francophones, ensemble la décision implicite de rejet de son recours gracieux formé contre cette mise en garde ;<br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              3° Sous le n° 412866, par une requête sommaire et un mémoire complémentaire, enregistrés les 28 juillet et 27 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Wit FM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la mise en garde du président du Conseil supérieur de l'audiovisuel (CSA) du 5 décembre 2016 concernant les conséquences de manquements à ses obligations conventionnelles de diffusion de titres et de nouveaux talents francophones, ensemble la décision du 31 mai 2017 par laquelle le CSA a rejeté son recours gracieux formé contre cette mise en garde ;<br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              4° Sous le n° 412867, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 juillet 2017, 27 octobre 2017 et 2 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Ado FM puis la société Swigg FM, venue aux droits de la société Ado FM, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la mise en garde du président du Conseil supérieur de l'audiovisuel (CSA) du 5 décembre 2016 concernant les conséquences de manquements à ses obligations conventionnelles de diffusion de titres et de nouveaux talents francophones, ensemble la décision du 31 mai 2017 par laquelle le CSA a rejeté son recours gracieux formé contre cette mise en garde ;<br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Europe 2 Entreprises, à la SCP Lyon-Caen, Thiriez, avocat de la société SERC Fun Radio, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Wit Fm et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Ado FM.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 juin 2018, présentée par la société SERC Fun Radio.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article 28 de la loi du 30 septembre 1986 relative à la liberté de communication subordonne la délivrance de l'autorisation d'usage de la ressource radioélectrique à tout service diffusé par voie hertzienne terrestre, autre que ceux exploités par les sociétés nationales de programme, à la conclusion d'une convention passée entre le Conseil supérieur de l'audiovisuel (CSA) au nom de l'Etat et la personne qui demande l'autorisation ; qu'aux termes du 2° bis de cet article, cette convention porte notamment sur " la proportion substantielle d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France, qui doit atteindre un minimum de 40 % de chansons d'expression française, dont la moitié au moins provenant de nouveaux talents ou de nouvelles productions, diffusées aux heures d'écoute significative par chacun des services de radio autorisés par le Conseil supérieur de l'audiovisuel, pour la part de ses programmes composée de musique de variétés " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces des dossiers que, le 5 décembre 2016, le président du CSA a adressé aux sociétés requérantes des courriers aux termes desquels le conseil les " met en garde contre les conséquences que pourrait entraîner le renouvellement de (...) manquements " à leurs obligations de diffusion de chansons d'expression française et leur " demande expressément de (se) conformer aux obligations de la convention " qui définit ces obligations ; que les sociétés demandent l'annulation de ces mises en garde, ainsi que de décisions rejetant des recours gracieux tendant à leur retrait ; qu'il y a lieu de joindre  leurs requêtes, qui présentent à juger la même question, pour statuer par une seule décision ;<br/>
<br/>
              3. Considérant que les courriers litigieux, qui se bornent à rappeler à leurs destinataires la nécessité de se conformer à leurs obligations, n'ont pas le caractère de décisions faisant grief, susceptibles de faire l'objet de recours pour excès de pouvoir ; que les conclusions présentées par les sociétés SERC Fun radio, Europe 2 Entreprises, Wit FM et Ado FM et tendant à l'annulation des mises en garde dont elles ont fait l'objet sont, par suite, irrecevables et doivent être rejetées pour ce motif ; qu'il en va de même des conclusions à fin d'annulation des décisions par lesquelles le CSA a rejeté les recours gracieux formés par les sociétés SERC Fun radio, Europe 2 Entreprises, Wit FM et Ado FM contre ces mises en garde ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du CSA qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes des sociétés Europe 2 Entreprises, SERC Fun radio, Wit FM et Ado FM sont rejetées.<br/>
Article 2 : La présente décision sera notifiée aux sociétés Europe 2 Entreprises, SERC Fun radio, Wit FM et Swigg FM, venue aux droits de la société Ado FM, et au Conseil supérieur de l'audiovisuel.<br/>
Copie en sera adressée  à la ministre de la culture.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
