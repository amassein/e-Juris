<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030133974</ID>
<ANCIEN_ID>JG_L_2015_01_000000374659</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/13/39/CETATEXT000030133974.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/01/2015, 374659</TITRE>
<DATE_DEC>2015-01-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374659</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP VINCENT, OHL</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374659.20150119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 janvier et 8 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Châteauneuf, représentée par son maire ; la commune de Châteauneuf demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY02470 du 14 novembre 2013 de la cour administrative d'appel de Lyon en tant qu'il a, sur la requête de la société Tenesol, en premier lieu, annulé le jugement n°s 0902289, 1100066 du tribunal administratif de Grenoble du 13 juillet 2012, en deuxième lieu, annulé le titre exécutoire n° 24 émis le 17 mars 2009 par la commune de Châteauneuf à l'encontre de la société Tenesol et déchargé cette dernière de l'obligation de payer la somme de 11 973,36 euros, en troisième lieu, condamné la commune de Châteauneuf à verser à la société Tenesol la somme de 46 331,64 euros, toutes taxes comprises, assortie des intérêts capitalisés ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Tenesol ; <br/>
<br/>
              3°) de mettre à la charge de la société Tenesol le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Vincent, Ohl, avocat de la commune de Châteauneuf, et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Tenesol ;<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par marché conclu le 30 juillet 2007, la commune de Châteauneuf a confié à la société Tenesol l'installation d'un générateur photovoltaïque sur le groupe scolaire de la commune, raccordé au réseau de distribution d'électricité ; que le maître d'ouvrage a notifié à cette société un décompte général, retenant des pénalités de retard, par lettre recommandée avec accusé de réception ; que la commune de Châteauneuf a émis, le 17 mars 2009, un titre exécutoire à l'encontre de la société Tenesol au titre du solde du marché ; que par jugement du 13 juillet 2012, le tribunal administratif de Grenoble a rejeté les deux demandes de la société Tenesol tendant, d'une part, à l'annulation du titre exécutoire et, d'autre part, à la condamnation de la commune à l'indemniser pour le règlement du marché ; que, par l'arrêt attaqué du 14 novembre 2013, la cour administrative d'appel de Lyon a annulé ce jugement et le titre exécutoire et condamné la commune de Châteauneuf à indemniser la société Tenesol ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 13.42 du cahier des clauses administratives générales applicable aux marchés publics de travaux, approuvé par le décret du 21 janvier 1976 et alors en vigueur : " Le décompte général signé par la personne responsable du marché doit être notifié à l'entrepreneur par ordre de service avant la plus tardive des deux dates ci-après : quarante-cinq jours après la date de remise du projet de décompte final ; trente jours après la publication de l'index de référence permettant la révision du solde (...) " ; que ces dispositions n'imposent pas que le décompte général soit notifié par le maître d'oeuvre ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la société Tensesol a reçu notification du décompte général de son marché, signé par le maître d'oeuvre, par lettre recommandée avec accusé de réception adressée par le maître d'ouvrage ; qu'en se fondant sur la seule circonstance que le décompte général n'avait pas été notifié par le maître d'oeuvre pour en déduire que cette notification était irrégulière et que, de ce fait, le décompte reçu par la société Tenesol ne pouvait être regardé comme définitif, la cour administrative d'appel de Lyon, a commis une erreur de droit ; que, par suite, la commune de Châteauneuf est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de Châteauneuf, qui n'est pas, dans la présente instance, la partie perdante, le versement des sommes que demandent la société Tenesol ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Tenesol le versement de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 14 novembre 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La société Tenevol versera la somme de 3 000 euros à la commune de Châteauneuf en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de Châteauneuf et à la société Tenesol.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÈGLEMENT DES MARCHÉS. DÉCOMPTE GÉNÉRAL ET DÉFINITIF. - MODALITÉS - MARCHÉS PUBLICS DE TRAVAUX - CCAG APPROUVÉ PAR LE DÉCRET DU 21 JANVIER 1976 - OBLIGATION D'UNE SIGNATURE PAR LE MAÎTRE D'OEUVRE - EXISTENCE - OBLIGATION D'UNE NOTIFICATION PAR LE MAÎTRE D'OEUVRE - ABSENCE.
</SCT>
<ANA ID="9A"> 39-05-02-01 Si, aux termes de l'article 13.42 du cahier des clauses administratives générales (CCAG) applicable aux marchés publics de travaux, approuvé par le décret n°76-87 du 21 janvier 1976 :  Le décompte général signé par la personne responsable du marché doit être notifié à l'entrepreneur par ordre de service (...) , ces dispositions n'imposent pas que le décompte général soit notifié par le maître d'oeuvre. Par suite, en l'espèce, erreur de droit à juger irrégulière une notification du décompte général, signé par le maître d'oeuvre, mais notifiée par le maître d'ouvrage.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
