<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029476911</ID>
<ANCIEN_ID>JG_L_2014_09_000000361344</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/47/69/CETATEXT000029476911.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 19/09/2014, 361344, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361344</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361344.20140919</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 25 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le syndicat national des enseignements de second degré, dont le siège est 46, avenue d'Ivry (75647) Paris Cedex 13, représenté par sa secrétaire générale ; le syndicat national des enseignements de second degré demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note de service n° 2012-085 du 9 mai 2012 du ministre de l'éducation nationale par laquelle il indique les modalités de dépôt et de traitement des candidatures des personnels enseignants du second degré, d'éducation et d'orientation à une mise à disposition de la Nouvelle-Calédonie pour la rentrée scolaire de février 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu la note de service attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 21 mars 2013, présenté par le ministre de l'éducation nationale qui conclut au rejet de la requête ; il soutient que les moyens de la requête ne sont pas fondés ; <br/>
<br/>
              Vu les pièces dont il résulte que, par application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de ce que le ministre de l'éducation nationale n'était pas compétent pour déléguer au vice-recteur de Nouvelle-Calédonie sa compétence pour déterminer ceux de ses agents qu'il retient en vue d'une mise à disposition de la Nouvelle-Calédonie ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 23 décembre 2013, présenté par le syndicat national des enseignements de second degré, qui soutient que le ministre de l'éducation nationale ne pouvait légalement déléguer au vice-recteur de Nouvelle-Calédonie sa compétence pour déterminer ceux de ses agents qu'il retient en vue d'une mise à disposition de la Nouvelle-Calédonie ;<br/>
<br/>
              Vu le nouveau mémoire en défense, enregistré le 7 janvier 2014, présenté par le ministre de l'éducation nationale, qui persiste dans ses conclusions et soutient que le ministre n'a pas délégué au vice-recteur agissant au nom de l'Etat la compétence d'établir la liste des personnels appelés à exercer en Nouvelle-Calédonie puisque cette compétence appartient en propre à la Nouvelle-Calédonie ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi organique n° 99-209 du 19 mars 1999 ;<br/>
<br/>
              Vu la loi de pays n° 2009-09 du 28 décembre 2009 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la convention du 18 octobre 2011 passée entre l'Etat et la Nouvelle-Calédonie portant sur la mise à disposition globale et gratuite des personnels rémunérés sur le budget de l'Etat au titre de l'exercice des compétences en matière d'enseignement du second degré public et privé, d'enseignement primaire privé et de santé scolaire ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de l'éducation nationale : <br/>
<br/>
              1. Considérant que si l'interprétation que, par voie de circulaires ou d'instructions, l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre, n'est pas susceptible d'être déférée au juge de l'excès de pouvoir, il en va autrement lorsqu'une telle instruction contient des dispositions impératives ; que tel est le cas de la note de service attaquée, qui a pour objet de préciser les modalités de dépôt et de traitement des candidatures des enseignants du second degré et des personnels d'éducation et d'orientation rémunérés sur le budget de l'Etat à une mise à disposition de la Nouvelle-Calédonie pour la rentrée scolaire de février 2013 ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              2. Considérant que la note de service attaquée, relative à la procédure à suivre pour la sélection et l'affectation des agents relevant du ministre chargé de l'éducation nationale en vue de la rentrée scolaire de février 2013 en Nouvelle-Calédonie et qui abroge la note de service n° 2011-065 du 18 avril 2011, prévoit que " la procédure de mise à disposition de la Nouvelle-Calédonie se déroule en deux phases. Une première phase extra-territoriale à l'issue de laquelle le vice-recteur de Nouvelle-Calédonie établit une liste des personnels admis à participer au mouvement intra-territorial et susceptibles d'être retenus pour une mise à disposition en Nouvelle-Calédonie. Une seconde phase intra-territoriale visant à affecter les personnels sur poste. / Le vice-recteur de Nouvelle-Calédonie procède à l'examen des candidatures et établit la liste des candidats susceptibles d'être mis à disposition de la Nouvelle-Calédonie sur le fondement d'éléments d'appréciation conformes à l'intérêt du service public de l'éducation en Nouvelle-Calédonie. La liste des candidats admis à participer au mouvement intra-territorial sera mise en ligne (...) sur le site du vice-rectorat de Nouvelle-Calédonie (...). / A l'issue de la phase intra-territoriale, les candidats retenus recevront directement une proposition d'affectation du vice-recteur de Nouvelle-Calédonie, directeur général des enseignements. Après acceptation de cette proposition d'affectation en Nouvelle-Calédonie, les intéressés devront adresser au bureau des affectations et mutations des personnels du second degré l'accusé de réception confirmant leur accord (...). Au terme de cette procédure, ils recevront [de ce] bureau, un arrêté ministériel de mise à disposition auprès de la Nouvelle-Calédonie. " ; qu'il résulte de ces dispositions que la note de service distingue deux étapes, la première, dite extra-territoriale, qui a pour objet d'établir la liste des agents susceptibles de changer d'académie d'affectation pour servir en Nouvelle-Calédonie, la seconde, dite intra-territoriale, qui a pour objet d'affecter les agents retenus pour servir en Nouvelle-Calédonie dans les différents postes vacants dans les établissements d'enseignement de cette collectivité ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 264-2 du code de l'éducation : " Le vice-recteur exerce en matière d'enseignement scolaire : / 1° les attributions qui relèvent de la compétence de l'Etat conférée en métropole aux recteurs et aux directeurs académiques des services de l'éducation nationale agissant sur délégation du recteur d'académie ; / 2° Les pouvoirs que le ministre chargé de l'éducation lui délègue par arrêté, dans la limite de ceux qu'il est habilité à déléguer aux recteurs et aux directeurs académiques des services de l'éducation nationale agissant sur délégation du recteur d'académie. " ; que si le ministre de l'éducation nationale soutient que sa compétence, s'agissant des mouvements en vue de l'affectation en Nouvelle-Calédonie, a été transférée au vice-recteur en application des dispositions de la convention du 18 octobre 2011 entre l'Etat et la Nouvelle-Calédonie sur la mise à disposition globale et gratuite des personnels rémunérés sur le budget de l'Etat en matière d'enseignement, il ressort des termes mêmes des  3°) et 4°) de l'article 9 de cette convention que " Le mouvement extra-territorial des agents de l'Etat est géré par le ministère de l'éducation nationale (...). A l'issue les personnels sont nommés en Nouvelle-Calédonie par le ministre de l'Education nationale (...). / Le mouvement intra-territorial est géré par le vice-recteur, directeur général des enseignements (...) selon les modalités définies dans l'article 6 de la présente convention " ; que ces dispositions n'ont en tout état de cause eu ni pour objet ni pour effet de transférer au vice-recteur de la Nouvelle-Calédonie agissant au nom de l'Etat la compétence du ministre de l'éducation nationale pour prendre les décisions relatives au changement d'académie d'affectation des agents concernés ; qu'en l'absence de délégation régulièrement publiée au Journal officiel de la République française, la note de service attaquée n'a pu légalement attribuer une telle compétence au vice-recteur de la Nouvelle-Calédonie ; que, par suite, et sans qu'il soit besoin d'examiner les moyens de la requête, le syndicat national des enseignements de second degré est fondé à demander l'annulation de cette note de service ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de condamner l'Etat à verser au syndicat requérant la somme qu'il réclame au titre des frais exposés par lui et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La note de service n° 2012-085 du 9 mai 2012 du ministre de l'éducation nationale relative à la mise à disposition de la Nouvelle-Calédonie des personnels enseignants du second degré, d'éducation et d'orientation - rentrée de février 2013 est annulée.<br/>
Article 2 : Les conclusions présentées par le syndicat national des enseignements de second degré au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au syndicat national des enseignements de second degré et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
