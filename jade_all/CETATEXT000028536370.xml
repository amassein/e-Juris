<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028536370</ID>
<ANCIEN_ID>JG_L_2014_01_000000356196</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/53/63/CETATEXT000028536370.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 29/01/2014, 356196</TITRE>
<DATE_DEC>2014-01-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356196</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU ; BERTRAND</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356196.20140129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 27 janvier et 23 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) André Virondeau, représenté par son directeur, dont le siège est Le Peu de Chaudade à Nantiat (87140) ; l'EHPAD André Virondeau demande au Conseil d'Etat :<br/>
<br/>
              1° d'annuler l'arrêt n° 11BX01198 du 29 novembre 2011 par lequel la cour administrative d'appel de Bordeaux, réformant le jugement n° 1000735 du 17 mars 2011 du tribunal administratif de Limoges, a porté l'indemnité de licenciement due à M. B...A...à 25 407,45 euros et rejeté son appel incident dirigé contre ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel incident et de rejeter l'appel de M.A... ; <br/>
<br/>
              3°) de mettre à la charge de M. A...le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              Vu l'arrêté du 19 décembre 1983 relatif à l'indemnisation des agents des établissements d'hospitalisation publics et de certains établissements à caractère social licenciés pour insuffisance professionnelle ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bertrand, avocat de l'établissement d'hébergement pour personnes âgées dépendantes André Virondeau et à la SCP Blanc, Rousseau, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., infirmier titulaire recruté à compter du 1er avril 2005 par l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) André Virondeau, a été licencié pour insuffisance professionnelle par une décision du 29 janvier 2008 du directeur de cet établissement ; que, par un jugement du 17 mars 2011, le tribunal administratif de Limoges, saisi par M.A..., a condamné l'établissement à lui verser une somme de 3 387,66 euros à titre d'indemnité de licenciement ; que, par un arrêt du 29 novembre 2011, la cour administrative d'appel de Bordeaux, accueillant l'appel de M. A... et rejetant l'appel incident de l'EHPAD André Virondeau, a réformé ce jugement et porté le montant de l'indemnité allouée à M. A...à 25 407,45 euros ; que l'EHPAD André Virondeau se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que l'article 88 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, qui reprend les termes de l'ancien article L. 888 du code de la santé publique, dispose que : " Hormis le cas d'abandon de poste et les cas prévus aux articles 62 et 93, les fonctionnaires ne peuvent être licenciés que pour insuffisance professionnelle. Le fonctionnaire qui fait preuve d'insuffisance professionnelle peut soit être admis à faire valoir ses droits à la retraite, soit être licencié. La décision est prise par l'autorité investie du pouvoir de nomination après observation de la procédure prévue en matière disciplinaire. / Le fonctionnaire licencié pour insuffisance professionnelle peut recevoir une indemnité dans les conditions qui sont fixées par décret " ; que, si le décret auquel renvoie ces dispositions n'est pas intervenu, les conditions d'application des dispositions dont elles reprennent les termes ont été définies par l'arrêté du 19 décembre 1983 relatif à l'indemnisation des agents des établissements d'hospitalisation publics et de certains établissements à caractère social licenciés pour insuffisance professionnelle, qui n'est pas incompatible avec elles et qui demeure ainsi applicable ; que le versement de l'indemnité constitue un droit pour le fonctionnaire hospitalier qui remplit les conditions prévues par cet arrêté ; <br/>
<br/>
              3. Considérant qu'en jugeant que les dispositions de l'arrêté du 19 décembre 1983 demeuraient applicables en l'absence du décret prévu à l'article 88 de la loi du 9 janvier 1986, la cour, devant laquelle cette question était discutée, n'a pas soulevé d'office un moyen dont elle aurait dû informer les parties en application des dispositions de l'article R. 611-7 du code de justice administrative et n'a pas méconnu le caractère  contradictoire de la procédure ; qu'il résulte de ce qui a été dit ci-dessus qu'elle n'a pas commis d'erreur de droit en jugeant que ce texte était applicable au licenciement pour insuffisance professionnelle de M. A...et que le fait de remplir les conditions prévues par l'arrêté ouvrait à ce dernier un droit à indemnité ; que l'arrêt attaqué est suffisamment motivé sur ce point ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article 1er de l'arrêté du 19 décembre 1983 mentionné ci-dessus : " Les agents titulaires des établissements mentionnés à l'article L. 792 du code de la santé publique qui, ne satisfaisant pas aux conditions requises pour être admis à la retraite avec jouissance immédiate d'une pension, sont licenciés par application des dispositions de l'article L. 888 du même code, peuvent percevoir, dans la limite des versements prévus aux troisième et quatrième alinéas du présent article, une indemnité égale aux trois quarts des émoluments afférents au dernier mois d'activité multipliés par le nombre d'années de services validées pour la retraite sans que le nombre des années retenues pour ce calcul puisse être supérieur à quinze. / Le calcul est opéré sur les échelles de traitement en vigueur au moment du licenciement majoré du supplément familial de traitement et de l'indemnité de résidence. / L'indemnité est versée par mensualités qui ne peuvent dépasser le montant des derniers émoluments perçus par l'agent licencié. / Dans le cas d'un agent ayant acquis des droits à pension de retraite, les versements cessent à la date à laquelle l'intéressé atteint ou aurait atteint l'âge requis pour jouir de sa pension " ; qu'en l'absence de dispositions prévoyant un partage de la charge de l'indemnité de licenciement pour insuffisance professionnelle, celle-ci doit être assumée par le seul établissement qui a prononcé le licenciement ; qu'ainsi, la cour a pu, sans erreur de droit, juger qu'il incombait à l'EHPAD André Virondeau de prendre intégralement à sa charge l'indemnité due à M.A..., sans distinguer entre la part liée aux années de service effectuées par celui-ci au sein de cet établissement et celle qui est liée aux services effectués antérieurement au sein d'autres établissements ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que l'EHPAD André Virondeau n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux qu'il attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'EHPAD André Virondeau  la somme de 3 000 euros à verser à M.A..., au titre des dispositions du même article ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de l'EHPAD André Virondeau est rejeté.<br/>
<br/>
Article 2 : L'EHPAD André Virondeau versera une somme de 3 000 euros à M. A...au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'établissement d'hébergement pour personnes âgées dépendantes André Virondeau et à M. B...A.... <br/>
Copie en sera adressée, pour information, à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-06-03 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. INSUFFISANCE PROFESSIONNELLE. - INDEMNITÉ DUE AUX AGENTS TITULAIRES DE LA FONCTION PUBLIQUE HOSPITALIÈRE - CHARGE INCOMBANT AU SEUL ÉTABLISSEMENT QUI A PRONONCÉ LE LICENCIEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-11 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. - INDEMNITÉ DUE AUX AGENTS TITULAIRES DE LA FONCTION PUBLIQUE HOSPITALIÈRE EN CAS DE LICENCIEMENT POUR INSUFFISANCE PROFESSIONNELLE - CHARGE INCOMBANT AU SEUL ÉTABLISSEMENT QUI A PRONONCÉ LE LICENCIEMENT.
</SCT>
<ANA ID="9A"> 36-10-06-03 En l'absence de disposition prévoyant un partage de la charge de l'indemnité à laquelle les fonctionnaires mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 ont droit en cas de licenciement pour insuffisance professionnelle, dans les conditions prévues par l'arrêté du 19 décembre 1983, cette charge doit être assumée par le seul établissement qui a prononcé le licenciement.</ANA>
<ANA ID="9B"> 36-11 En l'absence de disposition prévoyant un partage de la charge de l'indemnité à laquelle les fonctionnaires mentionnés à l'article 2 de la loi n° 86-33 du 9 janvier 1986 ont droit en cas de licenciement pour insuffisance professionnelle, dans les conditions prévues par l'arrêté du 19 décembre 1983, cette charge doit être assumée par le seul établissement qui a prononcé le licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
