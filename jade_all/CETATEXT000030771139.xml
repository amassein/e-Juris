<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030771139</ID>
<ANCIEN_ID>JG_L_2015_06_000000374963</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/77/11/CETATEXT000030771139.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 19/06/2015, 374963, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374963</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374963.20150619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...a demandé au tribunal administratif de Montpellier, d'une part, d'annuler la décision du 22 août 2011 par laquelle le président de la communauté d'agglomération Béziers Méditerranée (CABEME) a rejeté sa demande de voir prise en compte, pour son reclassement au grade d'éducateur des activités physiques et sportives de 2ème classe, l'activité professionnelle de maître nageur sauveteur qu'il a exercée de novembre 1989 à novembre 1996 dans un cadre associatif ainsi que  l'arrêté du 3 octobre 2011 le reclassant au 6ème échelon de son grade, d'autre part d'enjoindre au président de la CABEME de procéder en conséquence à son reclassement et à la reconstitution de sa carrière. <br/>
<br/>
              Par un jugement n° 1105713 du 29 novembre 2013, le tribunal administratif de Montpellier a annulé la décision du 22 août 2011, enjoint à la CABEME de procéder au reclassement de M. B...en tenant compte d'une reprise d'ancienneté  supplémentaire de 8 ans et 1 mois de services antérieurs à sa titularisation, et rejeté le surplus des conclusions de l'intéressé.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 janvier 2014, 28 avril 2014 et 13 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération Béziers Méditerranée demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement, en tant qu'il lui est défavorable ; <br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu  le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la communauté d'agglomération Béziers Méditerranee et à la SCP Waquet, Farge, Hazan, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1 En jugeant recevables les conclusions de M. B...dirigées contre la décision du 22 août 2011, par laquelle le président de la CABEME a rejeté sa demande tendant à ce que soit prise en compte, pour son reclassement, son activité professionnelle antérieure au mois de décembre 1996, alors qu'il résulte de l'instruction que M. B...n'a demandé à la CABEME la prise en compte de ces services qu'après l'expiration du délai de recours contentieux contre l'arrêté du 27 décembre 2010 procédant à son reclassement et qu'en l'absence de changement de circonstances de fait ou de droit de nature à modifier l'appréciation des droits en litige, la décision du 22 août 2011 n'a fait que confirmer sur ce point les termes de cet arrêté, devenu définitif, le tribunal administratif de Montpellier a entaché son jugement d'irrégularité. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le jugement attaqué doit être annulé.<br/>
<br/>
              2. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              3. Il résulte ce qui a été dit au point 1 que la demande présentée par M. B... tendant à l'annulation de la décision du 22 août 2011 et de l'arrêté du 3 octobre 2011 du président de la CABEME est tardive, et par suite, irrecevable. Par voie de conséquence, M. B... n'est pas fondé à demander qu'il soit enjoint au président de la CABEME de procéder à son reclassement et à la reconstitution de sa carrière. <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              4.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la CABEME, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la CABEME au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 29 novembre 2013 du tribunal administratif de Montpellier est annulé.<br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif de Montpellier et ses conclusions présentées au titre de l'article L.761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : Les conclusions de la communauté d'agglomération Béziers Méditerranée présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la communauté d'agglomération Béziers Méditerranée et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
