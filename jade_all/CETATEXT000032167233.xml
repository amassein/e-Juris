<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032167233</ID>
<ANCIEN_ID>JG_L_2016_03_000000392035</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/16/72/CETATEXT000032167233.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 07/03/2016, 392035, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392035</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:392035.20160307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Toulouse d'annuler les trois notifications d'avis à tiers détenteur en date du 24 septembre 2014 émises par le pôle de recouvrement spécialisé de l'Ariège, lui réclamant le paiement d'une somme totale de 17 816,07 euros correspondant à des cotisations de taxe foncière sur les propriétés bâties au titre des années 2009 à 2013, de taxe d'habitation au titre de l'année 2013 et d'impôt sur le revenu au titre de l'année 2012, assorties de majorations pour paiement tardif et frais de poursuite, de lui accorder un sursis de paiement et de condamner la direction départementale des finances publiques de l'Ariège à lui verser les sommes de 10 414 et 400 euros au titre du remboursement des sommes indûment saisies et des frais de saisies correspondants, augmentées des intérêts légaux capitalisés à compter des dates de saisie. <br/>
<br/>
              Par une ordonnance n° 1405693 du 1er juin 2015, la présidente de la première chambre de ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juillet et 22 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par deux mémoires enregistrés les 22 décembre 2015 et 21 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de son pourvoi, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du deuxième alinéa du I de l'article 1384 C du code général des impôts, dans sa rédaction issue de l'article 92 de la loi n° 2005-32 du 18 janvier 2005 de programmation pour la cohésion sociale en ce qu'elles réservent le bénéfice de l'exonération de taxe foncière sur les propriétés bâties aux seuls organismes ne se livrant pas à une exploitation ou à des opérations de caractère lucratif et agréés par le représentant de l'Etat dans le département. <br/>
<br/>
              Par un mémoire, enregistré le 5 janvier 2016, le ministre des finances et des comptes publics soutient que les conditions posées par l'article 23-5 de l'ordonnance du 7 novembre 1958 ne sont pas remplies. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>Sur les conclusions du pourvoi dirigées contre l'ordonnance attaquée en tant qu'elle porte sur le recouvrement des cotisations d'impôt sur le revenu :<br/>
<br/>
              1. Considérant qu'il résulte de l'article R. 811-1 du code de justice administrative que les litiges relatifs à l'impôt sur le revenu ne relèvent pas des litiges pour lesquels le tribunal administratif statue en premier et dernier ressort ; que, par suite, il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'attribuer à la cour administrative d'appel de Bordeaux, compétente pour en connaître en vertu de l'article R. 322-1 du code de justice administrative, le jugement des conclusions par lesquelles Mme A...conteste l'ordonnance attaquée en tant qu'elle statue sur le recouvrement des cotisations d'impôt sur le revenu qui lui sont réclamées ;<br/>
<br/>
              Sur les conclusions du pourvoi dirigées contre l'ordonnance attaquée en tant qu'elle porte sur le recouvrement des cotisations de taxe foncière sur les propriétés bâties et de taxe d'habitation:<br/>
<br/>
              En ce qui concerne la question prioritaire de constitutionnalité :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 281 du livre des procédures fiscales : " Les contestations relatives au recouvrement des impôts, taxes, redevances et sommes quelconques dont la perception incombe aux comptables publics compétents mentionnés à l'article L. 252 (...) ne peuvent porter que : / 1° Soit sur la régularité en la forme de l'acte ; / 2° Soit sur l'existence de l'obligation de payer, sur le montant de la dette compte tenu des paiements effectués, sur l'exigibilité de la somme réclamée, ou sur tout autre motif ne remettant pas en cause l'assiette et le calcul de l'impôt (...) " ; qu'il résulte de ces dispositions que dans une instance relative au recouvrement de l'impôt, la contestation ne peut pas porter sur un motif remettant en cause l'assiette ou le calcul de cet impôt ; que, par suite, une disposition relative à l'assiette ou au calcul d'une imposition ne peut être regardée comme étant applicable au litige ou à la procédure, au sens et pour l'application de l'article 23-5 précité de l'ordonnance du 7 novembre 1958, dans un litige relatif au recouvrement de cette imposition ;<br/>
<br/>
              4. Considérant que les dispositions de l'article 1384 C du code général des impôts régissent l'assiette et le calcul de la taxe foncière sur les propriétés bâties ; qu'elles ne sont pas applicables au présent litige, relatif au recouvrement de cotisations de taxe foncière sur les propriétés bâties ; que, dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, contestant la conformité aux principes d'égalité devant la loi et d'égalité devant les charges publiques du deuxième alinéa du I de cet article, dans sa rédaction en vigueur avant le 1er janvier 2015 ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              En ce qui concerne le pourvoi :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission (...) " ;<br/>
<br/>
              6. Considérant que pour demander l'annulation de l'ordonnance qu'elle attaque, Mme A... soutient que le tribunal administratif de Toulouse a méconnu son droit à bénéficier effectivement de l'assistance d'un avocat, reconnu par l'article 25 de la loi du 10 juillet 1991 relative à l'aide juridique, en statuant alors que l'avocat qui avait été désigné au titre de cette loi n'avait pas produit de mémoire, sans avoir préalablement mis en demeure cet avocat de le faire et sans l'avoir préalablement informée de cette carence ; qu'il a méconnu le principe d'impartialité dès lors que la présidente de chambre, qui a pris l'ordonnance attaquée, avait déjà rejeté par ordonnance d'autres demandes portant sur le même litige et que, faute d'en avoir été préalablement informée, elle n'a pu demander sa récusation ; qu'il a dénaturé les pièces du dossier et commis une erreur de droit au regard des articles L. 281 et R. 281-1 du livre des procédures fiscales en jugeant qu'elle n'avait pas produit de réclamation préalable dirigée contre les avis à tiers détenteur en litige ; qu'il a dénaturé les pièces du dossier et commis une erreur de droit au regard de l'article L. 277 du livre des procédures fiscales en regardant comme irrecevable sa demande de sursis de paiement faute qu'elle ait saisi l'administration fiscale d'une réclamation contentieuse assortie expressément d'une telle demande ;<br/>
<br/>
              7. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigées contre l'ordonnance attaquée en tant qu'elle porte sur le recouvrement des cotisations de taxe foncière sur les propriétés bâties et de taxe d'habitation ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
Article 2 : Les conclusions du pourvoi de Mme A...relatives au recouvrement de l'impôt sur le revenu sont attribuées à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Le surplus des conclusions du pourvoi est admis.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la présidente de la cour administrative d'appel de Bordeaux. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
Copie en sera adressée, pour information, au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
