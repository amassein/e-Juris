<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041757059</ID>
<ANCIEN_ID>JG_L_2020_03_000000418640</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/75/70/CETATEXT000041757059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 02/03/2020, 418640</TITRE>
<DATE_DEC>2020-03-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418640</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:418640.20200302</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 21 novembre 2013 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a annulé la décision du 23 mai 2013 de l'inspectrice du travail de la 7ème section de l'unité territoriale du Bas-Rhin refusant à la Fédération du Crédit mutuel centre est Europe l'autorisation de le licencier. Par un jugement n° 1400312 du 21 avril 2016, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 16NC01022 du 28 décembre 2017, la cour administrative d'appel de Nancy a rejeté l'appel formé par la Fédération du Crédit mutuel centre est Europe contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 février, 28 mai et 13 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération du Crédit mutuel centre est Europe demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge M. B... la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la Fédération du Crédit mutuel centre est Europe et à la SCP Meier-Bourdeau, Lecuyer, avocat de M. B... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 février 2020, présentée par la Fédération du Crédit mutuel centre est Europe ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 23 mai 2013, l'inspectrice du travail de la 7ème section de l'unité territoriale du Bas-Rhin a refusé d'autoriser la Fédération Crédit mutuel centre est Europe à licencier M. B..., salarié protégé, exerçant les fonctions d'inspecteur fédéral. Par une décision du 21 novembre 2013, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a annulé cette décision. La Fédération Crédit mutuel centre est Europe se pourvoit en cassation contre l'arrêt du 28 décembre 2017 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'elle a formé contre le jugement du tribunal administratif de Strasbourg du 21 avril 2016 ayant annulé la décision du ministre chargé du travail.<br/>
<br/>
              2. En vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail. Lorsque leur licenciement est envisagé, celui-ci ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou avec leur appartenance syndicale. Dans le cas où la demande de licenciement est motivée par un acte ou un comportement du salarié qui, ne méconnaissant pas les obligations découlant pour lui de son contrat de travail, ne constitue pas une faute, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits en cause sont établis et de nature, compte tenu de leur répercussion sur le fonctionnement de l'entreprise, à rendre impossible le maintien du salarié dans l'entreprise, eu égard à la nature de ses fonctions et à l'ensemble des règles applicables au contrat de travail de l'intéressé. <br/>
<br/>
              3. Il ressort des constatations de fait, non arguées de dénaturation, effectuées par la cour administrative d'appel de Nancy dans l'arrêt attaqué que la Fédération Crédit mutuel centre est Europe a diligenté une enquête interne visant M. B..., sans en informer ce dernier, après qu'un client eut signalé au Crédit mutuel avoir fait l'objet, à l'occasion d'un différend d'ordre privé, de menaces de la part de M. B..., fondées sur de prétendus mouvements suspects sur ses comptes bancaires. Les investigations réalisées par le service chargé du contrôle dans le cadre de cette enquête interne ont porté non seulement sur le point de savoir si M. B... avait consulté les comptes bancaires du client à l'origine du signalement, mais également sur les comptes personnels détenus par le salarié au sein de la banque et sur ceux du syndicat dont il était trésorier. Il est résulté de ces dernières investigations que M. B... avait commis des détournements de fonds au détriment du syndicat. <br/>
<br/>
              4. Lorsqu'un employeur diligente une enquête interne visant un salarié à propos de faits, venus à sa connaissance, mettant en cause ce salarié, les investigations menées dans ce cadre doivent être justifiées et proportionnées par rapport aux faits qui sont à l'origine de l'enquête et ne sauraient porter d'atteinte excessive au droit du salarié au respect de sa vie privée. <br/>
<br/>
              5. En premier lieu, la cour administrative d'appel de Nancy, qui n'était pas tenue de répondre à l'ensemble des arguments en défense invoqués devant elle, a suffisamment motivé son arrêt quant au champ des investigations conduites et à leur justification au regard des fonctions exercées par M. B.... <br/>
<br/>
              6. En second lieu, il ressort des pièces du dossier soumis aux juges du fond que l'employeur de M. B... a procédé, sans l'en informer, à la consultation des comptes bancaires personnels de ce salarié, auquel il n'a pu avoir accès qu'à raison de sa qualité de fédération d'établissements bancaires, alors que cette consultation n'était pas nécessaire pour établir la matérialité des allégations qui avaient été portées à sa connaissance par un tiers. En l'espèce, le trouble causé par le salarié rendant impossible son maintien dans l'entreprise, invoqué par l'employeur dans sa demande d'autorisation de licenciement, ne résultant que des éléments ainsi obtenus, la cour administrative d'appel n'a pas inexactement qualifié les faits de l'espèce en jugeant que la Fédération Crédit mutuel centre est Europe avait porté une atteinte excessive au respect de la vie privée de M. B..., dans des conditions insusceptibles d'être justifiées par les intérêts qu'elle poursuivait. La cour a pu en déduire, sans commettre d'erreur de droit ni méconnaître l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, que le ministre du travail n'avait pu légalement, pour annuler la décision de l'inspectrice du travail, se fonder sur le motif tiré de ce que le détournement de fonds commis par le salarié constituait un trouble manifeste dans le fonctionnement de l'entreprise.<br/>
<br/>
              7. Il résulte de ce qui précède que le pourvoi de la Fédération du Crédit mutuel centre est Europe doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B... au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la Fédération du Crédit mutuel centre est Europe est rejeté.<br/>
Article 2 : Les conclusions présentées par M. B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la Fédération du Crédit mutuel centre est Europe et à M. A... B.... <br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-01-08-02 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT AU RESPECT DE LA VIE PRIVÉE ET FAMILIALE (ART. 8). VIOLATION. - ENQUÊTE INTERNE VISANT UN SALARIÉ À PROPOS DE FAITS VENUS À LA CONNAISSANCE DE L'EMPLOYEUR - INVESTIGATIONS AYANT DÉPASSÉ LE CHAMP NÉCESSAIRE À LA VÉRIFICATION DES ALLÉGATIONS PORTÉES CONTRE LE SALARIÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66 TRAVAIL ET EMPLOI. - ENQUÊTE INTERNE VISANT UN SALARIÉ - CONDITIONS DE LÉGALITÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-07-01-04-035-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. MOTIFS AUTRES QUE LA FAUTE OU LA SITUATION ÉCONOMIQUE. COMPORTEMENT DU SALARIÉ EN DEHORS DU TRAVAIL. - DEMANDE DE LICENCIEMENT MOTIVÉE PAR DES FAITS, NON LIÉS À L'EXÉCUTION DU CONTRAT DE TRAVAIL, DE NATURE À RENDRE IMPOSSIBLE LE MAINTIEN DU SALARIÉ DANS L'ENTREPRISE [RJ1] - ESPÈCE - ILLÉGALITÉ, LE TROUBLE POUR L'ENTREPRISE RÉSULTANT D'ÉLÉMENTS OBTENUS À L'OCCASION D'UNE ENQUÊTE INTERNE AYANT PORTÉ UNE ATTEINTE EXCESSIVE AU DROIT DU SALARIÉ AU RESPECT DE SA VIE PRIVÉE.
</SCT>
<ANA ID="9A"> 26-055-01-08-02 Lorsqu'un employeur diligente une enquête interne visant un salarié à propos de faits, venus à sa connaissance, mettant en cause ce salarié, les investigations menées dans ce cadre doivent être justifiées et proportionnées par rapport aux faits qui sont à l'origine de l'enquête et ne sauraient porter d'atteinte excessive au droit du salarié au respect de sa vie privée.,,,Salarié protégé, employé d'une banque, ayant fait l'objet d'un signalement par un client qu'il aurait menacé après avoir consulté ses comptes bancaires. Enquête interne diligentée par la banque ayant porté non seulement sur la consultation des comptes de ce client par le salarié, mais également sur les comptes du salarié et ceux du syndicat dont il était trésorier. Investigations ayant révélé des détournements de fonds commis par le salarié au détriment du syndicat.... ,,L'employeur du salarié protégé a procédé, sans l'en informer, à la consultation des comptes bancaires personnels de ce salarié, auquel il n'a pu avoir accès qu'à raison de sa qualité de fédération d'établissements bancaires, alors que cette consultation n'était pas nécessaire pour établir la matérialité des allégations qui avaient été portées à sa connaissance par un tiers. En l'espèce, le trouble causé par le salarié rendant impossible son maintien dans l'entreprise, invoqué par l'employeur dans sa demande d'autorisation de licenciement, ne résultant que des éléments ainsi obtenus, l'employeur a porté une atteinte excessive au droit au respect de la vie privée du salarié protégé, dans des conditions insusceptibles d'être justifiées par les intérêts qu'il poursuivait.</ANA>
<ANA ID="9B"> 66 Lorsqu'un employeur diligente une enquête interne visant un salarié à propos de faits, venus à sa connaissance, mettant en cause ce salarié, les investigations menées dans ce cadre doivent être justifiées et proportionnées par rapport aux faits qui sont à l'origine de l'enquête et ne sauraient porter d'atteinte excessive au droit du salarié au respect de sa vie privée.</ANA>
<ANA ID="9C"> 66-07-01-04-035-03 Lorsqu'un employeur diligente une enquête interne visant un salarié à propos de faits, venus à sa connaissance, mettant en cause ce salarié, les investigations menées dans ce cadre doivent être justifiées et proportionnées par rapport aux faits qui sont à l'origine de l'enquête et ne sauraient porter d'atteinte excessive au droit du salarié au respect de sa vie privée.,,,Salarié protégé, employé d'une banque, ayant fait l'objet d'un signalement par un client qu'il aurait menacé après avoir consulté ses comptes bancaires. Enquête interne diligentée par la banque ayant porté non seulement sur la consultation des comptes de ce client par le salarié, mais également sur les comptes du salarié et ceux du syndicat dont il était trésorier. Investigations ayant révélé des détournements de fonds commis par le salarié au détriment du syndicat.... ,,L'employeur du salarié protégé a procédé, sans l'en informer, à la consultation des comptes bancaires personnels de ce salarié, auquel il n'a pu avoir accès qu'à raison de sa qualité de fédération d'établissements bancaires, alors que cette consultation n'était pas nécessaire pour établir la matérialité des allégations qui avaient été portées à sa connaissance par un tiers. En l'espèce, le trouble causé par le salarié rendant impossible son maintien dans l'entreprise, invoqué par l'employeur dans sa demande d'autorisation de licenciement, ne résultant que des éléments ainsi obtenus, l'employeur a porté une atteinte excessive au droit au respect de la vie privée du salarié protégé, dans des conditions insusceptibles d'être justifiées par les intérêts qu'il poursuivait. Le ministre du travail ne pouvait donc légalement se fonder, pour annuler le refus de l'inspecteur du travail d'autoriser le licenciement, sur le motif tiré de ce que le détournement de fonds commis par le salarié constituait un trouble manifeste dans le fonctionnement de l'entreprise.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les conditions de légalité d'une telle demande, CE, 4 juillet 2005,,, n° 272193, p. 306 ; CE, 15 décembre 2010,,, n° 316856, p. 508.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
