<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844921</ID>
<ANCIEN_ID>JG_L_2020_12_000000445050</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/49/CETATEXT000042844921.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/12/2020, 445050, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445050</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Vaullerin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:445050.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. H... C... a demandé au tribunal administratif de Rennes d'annuler l'élection du maire et des adjoints au maire de la commune de Theix-Noyalo et du maire délégué de la commune déléguée de Noyalo, qui s'est déroulée le 3 juillet 2020. <br/>
<br/>
              Par un jugement n° 2002692 du 17 septembre 2020, le tribunal administratif de Rennes a annulé ces opérations électorales. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 2 et 28 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. F... E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la protestation de M. C... contre ces opérations électorales.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... G..., auditrice,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 décembre 2020, présentée par M. E... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction que, lors des opérations électorales organisées le 28 juin 2020 en vue de désigner les conseillers municipaux de la commune de Theix-Noyalo, la liste " Osez citoyens ! " a obtenu un siège. Les trois premiers candidats de cette liste ont décidé de présenter leur démission le 3 juillet 2020 à 15h30, avant l'installation du conseil municipal qui a eu lieu le même jour à 18h30. Le quatrième candidat, M. D..., qui s'est présenté pour assister à la séance d'installation, n'a pas été autorisé, par le maire encore en exercice, à participer aux opérations électorales pour l'élection du maire, au motif qu'il n'avait pas pu être régulièrement convoqué à cette séance en raison de la démission tardive de ses colistiers.  M. C..., candidat de la liste " Osez citoyens ! " a demandé l'annulation de l'élection du maire, de ses adjoints et du maire délégué de la commune de Theix-Noyalo. Par un jugement du 14 septembre 2020, le tribunal administratif de Rennes a fait droit à sa protestation et annulé les opérations électorales du 3 juillet 2020. M. E..., dont l'élection en tant que maire de Theix-Noyalo a été annulée, fait régulièrement appel de ce jugement. <br/>
<br/>
              Sur la recevabilité du mémoire en intervention de la commune de Theix-Noyalo :<br/>
<br/>
              2. Aux termes de l'article L. 248 du code électoral : " Tout électeur et tout éligible a le droit d'arguer de nullité les opérations électorales de la commune devant le tribunal administratif. / Le préfet, s'il estime que les conditions et les formes légalement prescrites n'ont pas été remplies, peut également déférer les opérations électorales au tribunal administratif ". Aux termes de l'article L. 250 du même code : " Le recours au Conseil d'Etat contre la décision du tribunal administratif est ouvert soit au préfet, soit aux parties intéressées ". Il résulte de ces dispositions que, dans un contentieux électoral, faute de justifier d'un intérêt propre, une commune ne peut avoir, quand bien même elle aurait été mise en cause dans l'instance, ni la qualité de partie, ni celle d'intervenant. Par suite, l'intervention de la commune n'est pas recevable.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              3. Pour demander l'annulation du jugement attaqué, M. E... soutient que les premiers juges n'auraient pas répondu au moyen en défense présenté devant eux, et tiré de ce que la commune n'était pas en mesure de convoquer les nouveaux élus à la séance d'installation du conseil municipal en assurant le respect du délai minimum d'un jour franc en cas d'urgence prévu par l'article L. 2121-7 du code général des collectivités territoriales, s'exposant ainsi à l'annulation pour irrégularité de la délibération adoptée. Pour écarter ce moyen, le tribunal administratif a estimé que M. D... étant pleinement informé de la tenue du premier conseil municipal, l'absence de convocation dans le délai prévu ne faisait pas obstacle, dans les circonstances de l'espèce, à sa participation régulière à la séance. Par suite, le tribunal n'a pas omis de répondre au moyen en défense présenté par M. E... et n'a pas entaché son jugement d'insuffisance de motivation. <br/>
<br/>
              Au fond :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article L. 270 du code électoral : " Le candidat venant sur une liste immédiatement après le dernier élu est appelé à remplacer le conseiller municipal élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit ". L'article L. 2121-4 du code général des collectivités territoriales dispose que : " Les démissions des membres du conseil municipal sont adressées au maire. La démission est définitive dès sa réception par le maire, qui en informe immédiatement le représentant de l'État dans le département. " En vertu de l'article L. 2121-7 du même code : " Lors du renouvellement général des conseils municipaux, la première réunion se tient de plein droit au plus tôt le vendredi et au plus tard le dimanche suivant le tour de scrutin à l'issue duquel le conseil a été élu au complet.  Par dérogation aux dispositions de l'article L. 2121-12, dans les communes de 3 500 habitants et plus, la convocation est adressée aux membres du conseil municipal trois jours francs au moins avant celui de cette première réunion. " L'article L. 2121-12 du même code dispose que : " Dans les communes de 3 500 habitants et plus, (...) Le délai de convocation est fixé à cinq jours francs. En cas d'urgence, le délai peut être abrégé par le maire sans pouvoir être toutefois inférieur à un jour franc. "<br/>
<br/>
              5. La démission des trois premiers candidats de la liste " Osez Citoyens ! " est devenue définitive dès la réception de leurs lettres, le 3 juillet 2020 à 15h30, par le maire demeuré en fonction après le renouvellement général, jusqu'à l'installation du nouveau conseil municipal. En conséquence, le siège devenu vacant revenait immédiatement à M. D..., quatrième candidat de cette liste. Si la convocation pour la séance d'installation du conseil municipal doit être adressée à l'ensemble des nouveaux conseillers municipaux de la commune dans un délai minimum de trois jours, pouvant être ramené à un délai d'un jour franc en cas d'urgence, conformément aux articles L. 2121-7 et L. 2121-12 cités au point précédent, l'absence de convocation de M. D... dans ces délais, compte tenu des démissions tardives de ses colistiers et alors qu'il était informé de la tenue du conseil municipal auquel il s'était d'ailleurs présenté, n'était pas susceptible d'entacher d'irrégularité les délibérations adoptées. Par conséquent, il ne pouvait être refusé à M. D... de participer à la séance d'installation du conseil municipal. Par suite, M E... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rennes a annulé les opérations électorales qui ont eu lieu le 3 juillet 2020 dans la commune de Theix-Noyalo en vue de la désignation du maire, de ses adjoints et du maire délégué. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la commune de Theix-Noyalo n'est pas admise. <br/>
Article 2 : La requête de M. E... est rejetée.<br/>
Article 3: La présente décision sera notifiée à M. F... E..., à M. H... C... et à M. B... D....<br/>
Copie en sera adressée au ministre de l'intérieur, au préfet du Morbihan et à la commune de Theix-Noyalo. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
