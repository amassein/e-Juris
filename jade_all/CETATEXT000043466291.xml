<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043466291</ID>
<ANCIEN_ID>JG_L_2021_04_000000446608</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/46/62/CETATEXT000043466291.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/04/2021, 446608, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446608</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446608.20210422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. V... Y... a demandé au tribunal administratif de Lille d'annuler le second tour des élections municipales qui se sont déroulées le 28 juin 2020 dans la commune d'Eringhem. Par un jugement n° 2004478 du 19 octobre 2020, le tribunal administratif de Lille a fait droit à sa protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 18 novembre 2020 et le 15 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme J... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la protestation de M. Y... ;<br/>
<br/>
              3°) de mettre à la charge de M. Y... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ; <br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction qu'à l'issue du premier tour des opérations électorales qui se sont tenues dans la commune d'Eringhem (Nord) pour la désignation des conseillers municipaux, seul M. N..., maire sortant qui se présentait en candidat individuel, a été élu. A l'issue du second tour qui s'est déroulé le 28 juin 2020, ont été élus quatre candidats de la liste " Unis pour Eringhem " conduite par Mme C..., cinq candidats de la liste " Vivre et construire ensemble à Eringhem " conduite par M. Y... ainsi qu'un candidat individuel. M. Y..., qui n'a pas été élu, a demandé l'annulation du second tour au tribunal administratif de Lille, qui a fait droit à sa protestation par un jugement du 19 octobre 2020 dont Mme C... relève appel. <br/>
<br/>
              2. En premier lieu, l'erreur de plume qui a entaché l'article 2 du jugement attaqué en ce qu'il a décidé l'annulation des élections organisées le 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires d'Eringhem, alors que ces élections ont seulement visé à la désignation de conseillers municipaux de la commune, est sans incidence sur la régularité du jugement attaqué.  <br/>
<br/>
              3. En deuxième lieu, le jugement, qui souligne le caractère diffamatoire du tract mettant en cause l'honorabilité d'un candidat, sa diffusion pendant la campagne électorale et son impact sur la sincérité du scrutin au regard du très faible écart de voix entre le dernier candidat élu et le premier candidat non élu, est suffisamment motivé.<br/>
<br/>
              4. En troisième lieu, aux termes du premier alinéa de l'article L. 48-2 du code électoral : " Il est interdit à tout candidat de porter à la connaissance du public un élément nouveau de polémique électorale à un moment tel que ses adversaires n'aient pas la possibilité d'y répondre utilement avant la fin de la campagne électorale ".<br/>
<br/>
              5. Il résulte de l'instruction qu'entre le 19 et le 26 juin 2020, un tract mettant en cause l'intégrité et l'honorabilité de M. Y..., tête de la liste " Vivre et construire ensemble à Eringhem ", en des termes excédant les limites de ce qui peut être admis dans le cadre de la polémique électorale, a été diffusé auprès des habitants de la commune d'Eringhem à l'approche du second tour des élections municipales qui se sont tenues le 28 juin 2020. La teneur des propos tenus dans le tract en litige excluait toute défense utile de la part de l'intéressé, quand bien même celui-ci aurait disposé du temps nécessaire pour y répondre et que des articles parus dans la presse locale ainsi qu'un message public du maire sortant, ont dénoncé cette pratique. La diffusion de ce tract a ainsi constitué une manoeuvre qui, compte tenu de l'écart de seulement une voix entre le dernier élu et les deux premiers candidats non élus dans une commune qui compte 349 électeurs inscrits et alors même que le nombre d'exemplaires distribués n'est pas connu avec précision, a été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              6. Il résulte de ce tout ce qui précède que Mme C... n'est pas fondée à soutenir que c'est à tort que par le jugement attaqué, le tribunal administratif de Lille a annulé les opérations électorales qui se sont déroulées le 28 juin 2020 à Eringhem.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de M. Y..., qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande Mme C.... Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de M. Y... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme C... est rejetée.<br/>
Article 2 : Les conclusions présentées par M. Y... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme U... C..., à M. V... Y..., à Mme E... S..., à M. P... G..., à M. B... N..., à M. X... K..., à Mme A... R..., à M. H... W..., à M. F... L..., à M. Q... O..., à M. D... T..., à M. M... I... et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
