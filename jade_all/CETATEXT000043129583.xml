<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043129583</ID>
<ANCIEN_ID>JG_L_2021_02_000000441037</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/12/95/CETATEXT000043129583.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 11/02/2021, 441037, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441037</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441037.20210211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 4 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision du 8 janvier 2020 par laquelle la commission des sanctions de l'Agence française de lutte contre le dopage a prononcé à son encontre, d'une part, une sanction d'interdiction de participer pendant une durée de quatre ans à toute manifestation sportive donnant lieu à une remise de prix en argent ou en nature, de même qu'aux manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un de ses membres, et de participer directement ou indirectement à l'organisation et au déroulement des manifestations sportives et des entrainements mentionnés ci-dessus, d'autre part, d'exercer les fonctions de personnel encadrant au sein d'une fédération agréée ou d'un groupement ou d'une association affiliés à la fédération et d'exercer les fonctions définies à l'article L. 212-1 du code du sport ; <br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 000 euros à verser à la SCP Ricard, Bendel-Vasseur, Ghnassia, son avocat au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du sport ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - l'ordonnance n° 2018-1178 du 19 décembre 2018 ;<br/>
              - le décret n° 2019-322 du 12 avril 2019 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du <br/>
18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme C... D..., rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Ricard, <br/>
Bendel-Vasseur, Ghnassia, avocat de M. B..., et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il résulte de l'instruction que M. A... B... a été désigné pour être soumis, le 28 novembre 2018 à Essert-Blay (Savoie) à un contrôle antidopage diligenté par l'Agence française de lutte contre le dopage (AFLD). Par deux décisions des 6 mars et 15 mai 2019, la présidente de l'Agence a prononcé à son encontre une suspension provisoire à titre conservatoire, en application de l'article L. 232-23-4 du code du sport, les analyses des prélèvements effectués à l'occasion du contrôle ayant révélé l'existence d'une substance interdite. Par une décision du 8 janvier 2020, dont M. B... demande l'annulation, la commission des sanctions de l'Agence a prononcé à l'encontre de l'intéressé, à raison de ces faits, une sanction comportant notamment l'interdiction de pratiquer diverses activités dans le domaine sportif pendant une durée de quatre ans.<br/>
<br/>
              Sur la procédure de contrôle :<br/>
<br/>
              2. Aux termes de l'article L. 230-3 du code du sport, dans sa rédaction en vigueur à la date du contrôle contesté : " Est un sportif au sens du présent titre toute personne qui participe ou se prépare : / 1° Soit à une manifestation sportive organisée par une fédération agréée ou autorisée par une fédération délégataire ; / 2° Soit à une manifestation sportive donnant lieu à une remise de prix en argent ou en nature, alors même qu'elle n'est pas organisée par une fédération agréée ou autorisée par une fédération délégataire ; / 3° Soit à une manifestation sportive internationale. " Le 2° du I de l'article L. 232-5 du même code, dans sa rédaction applicable à l'espèce, donne compétence à l'AFLD pour effectuer des contrôles antidopage " dans les conditions prévues au présent chapitre : / a) Pendant les manifestations sportives organisées par les fédérations agréées ou autorisées par les fédérations délégataires ; / b) Pendant les manifestations sportives donnant lieu à une remise de prix en argent ou en nature, alors même qu'elles ne sont pas organisées par une fédération agréée ou autorisées par une fédération délégataire ; / c) Pendant les manifestations sportives internationales mentionnées à l'article L. 230-2 ; d) Pendant les périodes d'entraînement préparant aux manifestations sportives mentionnées aux a à c ; ". L'article L. 232-13-1 du même code, dans sa rédaction alors en vigueur, dispose : " Les contrôles peuvent être réalisés : (...) / 3° Dans tout lieu, y compris le domicile du sportif, permettant de réaliser le contrôle dans le respect de la vie privée du sportif et de son intimité ; (...). " En vertu de l'article R. 232-46 du même code : " La décision prescrivant un contrôle mentionné à l'article R. 232-45 est prise par le directeur du département des contrôles de l'Agence française de lutte contre le dopage et désigne, parmi les personnes agréées dans les conditions prévues à l'article R. 232 68 et dans le respect de la règle énoncée à l'article R. 232-53, celle qui est chargée du contrôle. L'ordre de mission que le directeur du département des contrôles établit précise : / 1° Le type de prélèvement ou de dépistage auquel il sera procédé ; / 2° Les modalités de choix des sportifs contrôlés, telles que le fait de figurer dans le groupe cible mentionné à l'article L. 232-15, le tirage au sort, le classement, l'établissement d'un nouveau record à l'occasion d'une manifestation sportive ; la personne chargée du contrôle peut également effectuer un contrôle sur tout sportif inscrit ou participant à une manifestation sportive ou encore se trouvant sur les lieux de celle-ci dès lors qu'il est licencié de la fédération qui organise ou autorise la manifestation ainsi qu'à l'occasion des entraînements y préparant ; (...). " <br/>
<br/>
              3. Il résulte de la combinaison des textes cités au point précédent qu'un sportif, au sens de l'article L. 230-3 du code du sport précité, peut faire l'objet, y compris à son domicile, d'un contrôle diligenté par l'Agence à l'occasion de manifestations sportives ou d'entraînements y préparant, et que le directeur du département des contrôles de l'agence peut délivrer à la personne chargée du contrôle un ordre de mission l'autorisant à y procéder dans ce cadre.<br/>
              4. Il résulte de l'instruction que le directeur du département des contrôles de l'agence a délivré à un agent habilité à procéder à des contrôles antidopage un ordre de mission en vue de prélèvements sanguins et urinaires sur M. B... à son domicile d'Essert-Blay (Savoie) le 8 novembre 2018. Il est constant que si M. B... n'était pas à cette date licencié de la fédération française d'athlétisme, s'étant inscrit au cross international d'Allonnes (Sarthe) auquel il devait participer le 18 novembre 2018, il devait être considéré comme sportif au sens des dispositions des articles L. 230-3 et R. 232-46 du code du sport, Par suite, le moyen tiré de ce que la procédure de contrôle aurait été irrégulière au motif que l'AFLD n'était pas compétente pour effectuer un contrôle à son domicile le 8 novembre 2018 doivent être écartés.<br/>
<br/>
              Sur la proposition d'entrée en voie de composition administrative :<br/>
<br/>
              5. Aux termes de l'article L. 232-21-1 du code du sport, issu de l'article 21 de l'ordonnance n° 2018-1178 du 19 décembre 2018 entrée en vigueur le 1er mars 2019 : " Lorsque l'agence dispose d'éléments permettant de présumer une infraction aux dispositions des articles L. 232-9, L. 232-9-1, L. 232-9-2, L. 232-9-3, L. 232-10, L. 232-14-5, L. 232-15-1 ou L. 232-17, le secrétaire général en informe l'intéressé. / Le secrétaire général de l'agence adresse à l'intéressé une proposition d'entrée en voie de composition administrative. / Toute personne qui accepte d'entrer en voie de composition administrative s'engage, dans le cadre d'un accord arrêté avec le secrétaire général de l'Agence française de lutte contre le dopage, à reconnaître l'infraction et à en accepter les conséquences prévues aux articles L. 232-21 à L. 232 23-3-11 ainsi qu'aux I et II de l'article L. 232-23-5. / L'accord est soumis au collège puis, s'il est validé par celui-ci, à la commission des sanctions, qui peut décider de l'homologuer. " L'article R. 232-89 du code du sport, dans sa rédaction issue de l'article 46 du décret n° 2019-322 du 12 avril 2019 pris pour l'application de l'ordonnance du 19 décembre 2018 précitée et entré en vigueur le 15 avril 2019, dispose : " Au plus tard un mois après la réception par l'agence de la preuve de la notification prévue au premier alinéa de l'article L. 232-21-1, le secrétaire général de l'agence adresse à l'intéressé une proposition d'entrée en voie de composition administrative, selon les modalités prévues au premier alinéa de l'article R. 232-88. / Le destinataire dispose d'un délai de quinze jours à compter de la réception de cette proposition pour se prononcer par écrit sur celle-ci. / A compter de la réception par l'Agence française de lutte contre le dopage de l'acceptation de la proposition d'entrée en voie de composition administrative, l'accord mentionné au troisième alinéa de l'article L. 232-21-1 est conclu dans un délai de deux mois. "<br/>
<br/>
              6. Les dispositions citées ci-dessus qui prévoient que le secrétaire général de l'Agence adresse au sportif concerné une proposition d'entrée en voie de composition administrative dans un délai d'un mois après réception des éléments permettant de présumer une infraction aux dispositions du code du sport ne sont pas prescrites à peine de nullité de la procédure de sanction. Par suite le moyen tiré de ce que la sanction infligée à M. B... serait irrégulière faute pour le secrétaire général d'avoir adressé sa proposition d'entrée en voie de composition dans ce délai d'un mois ne peut, en tout état de cause, qu'être écarté.<br/>
<br/>
              Sur les griefs retenus par la commission des sanctions :<br/>
<br/>
              7. En premier lieu, les stipulations du code mondial antidopage, qui constitue le premier appendice de la convention internationale contre le dopage dans le sport, ne produisent pas d'effets entre les Etats ni, par voie de conséquence, à l'égard des particuliers et ne peuvent donc pas être utilement invoquées, à défaut de tout renvoi du code du sport, à l'appui de conclusions tendant à l'annulation d'une décision individuelle ou réglementaire. Ainsi, M. B... ne peut utilement se prévaloir des dispositions des articles 10.4 et 10.5 du code mondial antidopage, relatives à l'élimination ou à la réduction de la période de suspension en l'absence de faute ou de négligence.<br/>
<br/>
              8. En second lieu, aux termes de l'article L. 232-9 du code du sport, dans sa rédaction applicable à l'espèce : " Il est interdit à tout sportif : / (...) ; / 2° D'utiliser ou tenter d'utiliser une ou des substances ou méthodes interdites figurant sur la liste mentionnée au dernier alinéa du présent article. / L'interdiction prévue au 2° ne s'applique pas aux substances et méthodes pour lesquelles le sportif : / a) Dispose d'une autorisation pour usage à des fins thérapeutiques ; (...) / c) Dispose d'une raison médicale dûment justifiée. / La liste des substances et méthodes mentionnées au présent article est celle qui est élaborée en application de la convention internationale mentionnée à l'article L. 230-2 ou de tout autre accord ultérieur qui aurait le même objet et qui s'y substituerait. Elle est publiée au Journal officiel de la République française. "<br/>
<br/>
              9. En dehors du cas où est apportée la preuve d'une prescription médicale à des fins thérapeutiques justifiées, l'existence d'une violation des dispositions législatives et réglementaires relatives au dopage est établie par la présence, dans un prélèvement urinaire ou sanguin, d'une substance interdite, sans qu'il y ait lieu de rechercher si l'usage de cette substance a revêtu un caractère intentionnel.<br/>
<br/>
              10. Si M. B... conteste avoir commis une violation des dispositions de lutte contre le dopage, il se borne à énumérer succinctement les éléments qu'il a fait valoir devant la commission des sanctions, selon lesquels la présence d'érythropoïétine dans les échantillons prélevés serait le fruit d'une malveillance, et ne produit aucun élément au soutien de ses allégations de nature à établir qu'il n'a pas voulu utiliser la substance interdite, au sens de l'article L. 232-9 citée au point 11, détectée dans les prélèvements urinaires et sanguins effectués le 8 novembre 2018.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Agence française de lutte contre le dopage qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à l'Agence française de lutte contre le dopage et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
