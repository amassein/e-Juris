<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041853439</ID>
<ANCIEN_ID>JG_L_2020_04_000000439955</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/85/34/CETATEXT000041853439.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 30/04/2020, 439955, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439955</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439955.20200430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 4 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) d'enjoindre au Premier ministre de procéder au réexamen dans un délai bref de l'article 3 du décret n° 2020-293 du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire sous peine que, dans le cas où cette injonction ne serait pas suivie d'effet, soient suspendus à la date d'expiration du délai précité :<br/>
<br/>
              - à titre principal, l'ensemble de l'article 3 du décret attaqué ;<br/>
<br/>
              - à titre subsidiaire :<br/>
o au 2° de son I, les mots " achats de première nécessité " ;<br/>
o le 3° de son I ; <br/>
o au 4° de son I, les mots " motif familial impérieux " ;<br/>
o au 5° de son I, les mots " dans un rayon maximal d'un kilomètre ", " domicile " ;<br/>
o le 5° de son I en tant qu'il ne permet pas de se rendre avec son animal de compagnie chez le vétérinaire ;<br/>
o à son II, les mots " domicile " ;<br/>
o son II en tant qu'il ne précise pas les mentions que doit comporter le document exigé, la forme qu'il doit prendre et ce qu'il en est de cette obligation en ce qui concerne les enfants mineurs, selon qu'ils sont ou non accompagnés dans l'un des déplacements autorisés, et/ou ne renvoie pas à un arrêté ministériel le soin d'apporter ces précisions ;<br/>
o son II en tant qu'il oblige à se munir, lors d'un déplacement hors du domicile, d'un document pour les motifs dérogatoires 2° à 5° du I ;<br/>
o l'article 3 du décret en tant qu'il n'est pas rendu inapplicable aux personnes sans domicile fixe ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 500 001 francs Pacifique au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - il dispose d'un intérêt pour agir lui donnant qualité pour agir ;<br/>
              - la condition d'urgence est remplie dès lors, d'une part, que les dispositions attaquées, actuellement en vigueur, restreignent la liberté d'aller et venir et, d'autre part, que l'article L. 3131-18 du code de la santé publique, dans sa rédaction résultant de la loi n° 2020-290 du 23 mars 2020, crée nécessairement une présomption d'urgence à contester les mesures attaquées ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
              - l'article 3 dudit décret méconnaît les dispositions de l'article 22 de la Constitution, faute d'être revêtu des contreseings des ministres de l'intérieur et de la justice, pourtant chargés de son exécution ;<br/>
              - compte tenu de leur caractère vague et imprécis, et donc sujettes à interprétations variées, certaines dispositions dudit article, édictées sous peine d'amendes voire d'autres peines correctionnelles prévues par l'article L. 3136-1 du code de la santé publique, méconnaissent le principe de légalité des délits et des peines, le principe de clarté et de précision de la loi pénale et l'objectif d'accessibilité et d'intelligibilité de la loi ;<br/>
              - il est porté une atteinte au principe constitutionnel de nécessité et de proportionnalité des peines compte tenu des difficultés des services de police et des contraintes excessives pesant sur les citoyens quant à la véracité des motifs à l'origine de déplacements dérogatoires ;<br/>
              - le décret est entaché d'une erreur manifeste d'appréciation et méconnaît les principes constitutionnels d'égalité, de fraternité et de sauvegarde de la dignité de la personne humaine dès lors que les mesures prescrites s'appliquent aux personnes sans domicile fixe ;<br/>
              - l'emploi du terme " domicile " au dépend des termes " résidence actuelle " est entaché d'une erreur manifeste d'appréciation et porte atteinte aux droits au respect de la vie privée et de mener une vie familiale normale pour les personnes éloignées de leur domicile lors de la mise en place du confinement ;<br/>
              - la limite d'une heure quotidienne pour les déplacements brefs autorisés autour du domicile est contraire au droit de propriété compte tenu de l'obstacle qu'elle représente pour les personnes en possession d'un animal de compagnie dans l'accès aux services vétérinaires.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable, ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée, sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              Sur les circonstances :<br/>
<br/>
              2. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux, et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants dans les établissements les recevant et des élèves et étudiants dans les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par plusieurs arrêtés successifs. <br/>
<br/>
              3. Par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a été déclaré l'état d'urgence sanitaire pour une durée de deux mois sur l'ensemble du territoire national. Par un nouveau décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, le Premier ministre a réitéré les mesures qu'il avait précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Leurs effets ont été prolongés en dernier lieu par décret du 14 avril 2020.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              4. M. A... demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, d'enjoindre au Premier Ministre de réexaminer l'article 3 du décret du 23 mars 2020 et, à défaut, d'ordonner la suspension de son exécution.<br/>
<br/>
              5. D'une part, il n'appartient au Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'enjoindre au Premier Ministre de réexaminer les dispositions d'un décret. Par suite, les conclusions de M. A... tendant à ce que soit ordonné au Premier Ministre de réexaminer l'article 3 du décret du 20 mars 2020 ne peuvent qu'être rejetées.<br/>
<br/>
              6. D'autre part, pour demander au juge des référés du Conseil d'Etat d'ordonner la suspension de l'exécution de l'article 3 du décret du 20 mars 2020, M. A... soutient que les dispositions de l'article 22 de la Constitution ont été méconnues, faute que le décret soit revêtu des contreseings des ministres de l'intérieur et de la justice. Il soutient ensuite que, compte tenu de leur caractère vague et imprécis, certaines dispositions de l'article 3, édictées sous peine d'amendes voire d'autres peines correctionnelles prévues par l'article L. 3136-1 du code de la santé publique, méconnaissent le principe de légalité des délits et des peines, le principe de clarté et de précision de la loi pénale et l'objectif d'accessibilité et d'intelligibilité de la loi ainsi qu'au principe constitutionnel de nécessité et de proportionnalité des peines. Il soutient également que le décret est entaché d'une erreur manifeste d'appréciation et méconnaît les principes constitutionnels d'égalité, de fraternité et de sauvegarde de la dignité de la personne humaine dès lors que les mesures prescrites s'appliquent aux personnes sans domicile fixe. Il soutient en outre que l'emploi du terme " domicile " et non des termes " résidence actuelle " est entaché d'une erreur manifeste d'appréciation et porte atteinte aux droits au respect de la vie privée et de mener une vie familiale normale pour les personnes éloignées de leur domicile lors de la mise en place du confinement. Il soutient enfin que la limite d'une heure quotidienne pour les déplacements brefs autorisés autour du domicile est contraire au droit de propriété compte tenu de l'obstacle qu'elle représente pour les personnes en possession d'un animal de compagnie dans l'accès aux services vétérinaires.<br/>
<br/>
              7. Ces moyens ne sont manifestement pas propres à créer un doute sérieux quant à la légalité des dispositions contestées.<br/>
<br/>
              8. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, la requête doit être rejetée par application des dispositions de l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre de l'article L. 761-1 de ce code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée au Premier Ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
