<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031649145</ID>
<ANCIEN_ID>JG_L_2015_12_000000382907</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/91/CETATEXT000031649145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 18/12/2015, 382907, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382907</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382907.20151218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Bordeaux d'annuler la décision du 26 avril 2013 par laquelle le ministre de l'intérieur a retiré quatre points de son permis de conduire à la suite d'une infraction commise le 7 mars 2012 et a constaté la perte de validité de ce permis pour solde de points nul. Par un jugement n°1302368 du 20 mai 2014, le tribunal a fait droit à sa demande. <br/>
<br/>
              Par un pourvoi enregistré le 21 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article L. 223-1 du code de la route : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. A la date d'obtention du permis de conduire, celui-ci est affecté de la moitié du nombre maximal de points. Il est fixé un délai probatoire de trois ans. Au terme de chaque année de ce délai probatoire, le permis est majoré d'un sixième du nombre maximal de points si aucune infraction ayant donné lieu à un retrait de points n'a été commise... " ; qu'aux termes de l'article R. 223-1 du même code : " ... II. - A la date d'obtention du permis de conduire, celui-ci est affecté d'un nombre initial de six points. Au terme de chaque année du délai probatoire défini à l'article L. 223-1, si aucune infraction ayant donné lieu à retrait de points n'a été commise depuis le début de la période probatoire, ce permis de conduire est majoré de deux points. Cette majoration est portée à trois points si le titulaire du permis a suivi un apprentissage anticipé de la conduite. / III. - Pendant le délai probatoire, le permis de conduire ne peut être affecté d'un nombre de points supérieur à six. Ce nombre est augmenté de la majoration résultant de l'application du II du présent article. / IV. - A l'issue de ce délai probatoire, si aucune infraction ayant donné lieu à retrait de points n'a été commise, le permis de conduire est affecté du nombre maximal de douze points. En cas de commission d'infraction ayant donné lieu à retrait de points au cours du délai probatoire, l'affectation du nombre maximal de points intervient dans les conditions définies à l'article L. 223-6 " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article L. 223-6 du même code : " Si le titulaire du permis de conduire n'a pas commis, dans le délai de deux ans à compter de la date du paiement de la dernière amende forfaitaire, de l'émission du titre exécutoire de la dernière amende forfaitaire majorée, de l'exécution de la dernière composition pénale ou de la dernière condamnation définitive, une nouvelle infraction ayant donné lieu au retrait de points, son permis est affecté du nombre maximal de points. /  Le délai de deux ans mentionné au premier alinéa est porté à trois ans si l'une des infractions ayant entraîné un retrait de points est un délit ou une contravention de la quatrième ou de la cinquième classe. / Toutefois, en cas de commission d'une infraction ayant entraîné le retrait d'un point, ce point est réattribué au terme du délai de six mois à compter de la date mentionnée au premier alinéa, si le titulaire du permis de conduire n'a pas commis, dans cet intervalle, une infraction ayant donné lieu à un nouveau retrait de points. / Le titulaire du permis de conduire qui a commis une infraction ayant donné lieu à retrait de points peut obtenir une récupération de points s'il suit un stage de sensibilisation à la sécurité routière qui peut être effectué dans la limite d'une fois par an. / (...) " ; qu'aux termes du II de l'article R. 223-8 : " L'attestation délivrée à l'issue du stage effectué en application des dispositions du quatrième alinéa de l'article L. 223-6 donne droit à la récupération de quatre points dans la limite du plafond affecté au permis de conduire de son titulaire " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, notamment du relevé intégral d'information relatif au permis de conduire de M. A... qui y avait été versé par le ministre de l'intérieur, que l'intéressé a obtenu le 5 novembre 2009 un permis de conduire dont le capital de points, initialement fixé à six en vertu des dispositions précitées du II de l'article R. 223-1 du code de la route, a été porté à huit le 5 novembre 2010 en l'absence, pendant la première année du délai probatoire, de toute infraction ayant entraîné un retrait de points ; qu'une infraction commise le 16 mai 2011 a entraîné le retrait d'un point et fait obstacle à ce que le capital de points soit majoré à l'issue des deuxième et troisième années du délai probatoire ; que si le point ainsi retiré a été réattribué au bout de six mois en l'absence de nouvelle infraction entraînant retrait de points, M. A...a commis les 17 janvier, 7 mars, 14 octobre, 27 novembre et 18 décembre 2012 cinq infractions au code de la route ayant entraîné au total le retrait de quatorze points ; qu'ainsi, alors même qu'il avait effectué les 7 et 8 février 2013 un stage de sensibilisation à la sécurité routière ayant entraîné, en application des dispositions du II de l'article R. 223-8 du code de la route, la récupération de quatre points, le solde de points était nul à la date de la décision du 26 avril 2013 du ministre de l'intérieur constatant qu'il avait perdu sa validité ; qu'il suit de là qu'en annulant cette décision au motif que, compte tenu de la récupération de quatre points consécutive au stage de sensibilisation à la sécurité routière, le permis n'avait pas perdu sa validité, le tribunal administratif a commis une erreur de fait ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant que si M. A...soutient qu'il n'a pas bénéficié, lors de la constatation de l'infraction commise le 16 mai 2011, des informations exigées par les articles L. 223-3 et R. 223-3 du code de la route, et qu'ainsi la décision retirant un point de son permis de conduire à la suite de cette infraction est intervenue à l'issue d'une procédure irrégulière, il ressort des pièces du dossier, ainsi qu'il a été dit ci-dessus, que le point ainsi retiré a été ultérieurement rétabli en application des dispositions du troisième alinéa de l'article L. 223-6 du même code ; que, dans ces conditions, le moyen tiré de l'illégalité de ce retrait de point est inopérant à l'encontre de la décision attaquée, qui ne l'a pas pris en compte pour constater la perte de validité du permis ; <br/>
<br/>
              6. Considérant qu'ainsi qu'il a été dit ci-dessus le moyen tiré de ce que, compte tenu du stage réalisé les 7 et 8 février 2013, le solde de points du permis de conduire de M. A...n'était pas nul à la date de la décision constatant la perte de validité de ce permis manque en fait ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision du 26 avril 2013 du ministre de l'intérieur ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne sauraient, par suite, être accueillies ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le jugement du tribunal administratif de Bordeaux est annulé.<br/>
<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Bordeaux est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
