<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031350136</ID>
<ANCIEN_ID>JG_L_2015_10_000000380481</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/01/CETATEXT000031350136.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 21/10/2015, 380481, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380481</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:380481.20151021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. D...B...a demandé au tribunal administratif de Poitiers, qui a transmis sa demande au tribunal administratif de Paris, d'annuler la décision implicite par laquelle le Premier ministre a rejeté sa demande du 18 avril 2002 de revaloriser sa pension militaire de retraite et sa retraite du combattant en le faisant bénéficier des dispositions de droit commun du code des pensions civiles et militaires de retraite, d'ordonner le rappel des arrérages dus et de condamner l'Etat à lui verser la somme de 50 000 euros à titre de dommages et intérêts.<br/>
<br/>
              Par une ordonnance n° 0301233 du 31 décembre 2008, le vice-président de la 5ème section du tribunal administratif de Paris a annulé la décision implicite rejetant la demande de M. B...de révision de sa pension militaire de retraite, a enjoint à l'Etat de lui verser, pour la période postérieure au 1er janvier 1998, les arrérages correspondant à la différence entre le montant de sa pension militaire de retraite revalorisée selon les modalités précisées dans les motifs du jugement et celui déjà versé à l'intéressé, ainsi que les intérêts capitalisés, et a rejeté le surplus des conclusions de la demande de M.B....<br/>
<br/>
              Par un arrêt n° 11PA02362 du 18 mars 2014, la cour administrative d'appel de Paris, à la demande de M.B..., a partiellement annulé l'ordonnance du 31 décembre 2008, a annulé la décision du 17 décembre 1992 du ministre de la défense, la décision du 30 juin 1993 du ministre du budget et la décision implicite de rejet résultant du silence gardé par le Premier ministre sur la demande de M. B...du 18 avril 2002, a enjoint à l'Etat de verser à M. B... une somme égale à la différence entre le montant des arrérages de la pension qu'il aurait dû percevoir, à compter du 30 novembre 1990, en application des dispositions de droit commun du code des pensions civiles et militaires de retraite, et celui qui lui a été effectivement versé, assortie des intérêts, et a rejeté le surplus des conclusions de M.B.... <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et par un mémoire en réplique, enregistrés les 20 mai et 6 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Paris du 18 mars 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de fixer le point de départ du rappel d'arrérages en fonction de la demande de décristallisation de sa pension formée par M. B...en 2002.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2009-1053 du 26 août 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par son pourvoi, le ministre des finances et des comptes publics doit être regardé comme demandant l'annulation de l'arrêt de la cour administrative d'appel de Paris du 18 mars 2014 en tant seulement que, par son article 4, il détermine la somme qu'il enjoint à l'Etat de verser à M. B...au titre du rappel des arrérages de la pension qu'il aurait dû percevoir.<br/>
<br/>
              Sur la recevabilité du pourvoi :<br/>
<br/>
              2. Par un arrêté du 9 avril 2014 publié au Journal officiel de la République française du 11 avril suivant, pris sur le fondement de l'article 3 du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement, Mme C...A..., fonctionnaire de catégorie A et chef du bureau des affaires juridiques du service des retraites de l'Etat, chargée à ce titre de traiter le contentieux administratif des pensions de l'Etat en vertu du décret du 26 août 2009 portant organisation du service des retraites de l'Etat, a reçu délégation à l'effet de signer, au nom du ministre des finances et des comptes publics et dans la limite de ses attributions, tous actes, arrêtés, décisions ou conventions. Dès lors, elle avait compétence pour signer le pourvoi au nom du ministre. Par suite, M. B...n'est pas fondé à soutenir que le pourvoi serait irrecevable faute pour son auteur de bénéficier d'une délégation de signature régulière.  <br/>
<br/>
              Sur le bien-fondé du pourvoi :<br/>
<br/>
              3. Les dispositions applicables en matière de rappel d'arrérages de pensions civiles ou militaires de retraite sont celles de la législation dont relève la pension, déterminée en fonction de la date d'ouverture des droits du pensionné, dans leur rédaction en vigueur à la date de la demande de pension ou de révision. Aux termes de l'article L. 74 du code des pensions civiles et militaires de retraite issu de la loi du 20 septembre 1948, dans sa rédaction résultant de la loi du 31 juillet 1962, applicable à M. B...eu égard à la date de l'ouverture de ses droits à pension et à celle de sa demande de révision de sa pension : " Sauf l'hypothèse où la production tardive de la demande de liquidation ne serait pas imputable au fait  personnel du pensionné, il ne pourra y avoir lieu en aucun cas au rappel de plus de deux années d'arrérages antérieurs à la date du dépôt de la demande de pension ". Les demandes tendant à la revalorisation d'une pension cristallisée s'analysent comme des demandes de liquidation de pension au sens de ces dispositions.<br/>
<br/>
              4. Pour juger que M. B...était fondé à demander le versement des arrérages de sa pension revalorisée à compter du 30 novembre 1990, et non à compter du 2 mai 2000 ainsi que le soutenait le ministre en invoquant la règle de prescription prévue à l'article L. 74 cité ci-dessus, la cour administrative d'appel a relevé que l'intéressé avait présenté au service des pensions des armées une première demande de revalorisation de sa pension le 30 novembre 1992. En statuant ainsi, sans rechercher si la demande de M. B...du 30 novembre 1992 tendait à la décristallisation de sa pension au motif du caractère discriminatoire des règles applicables au calcul de sa pension de retraite, alors qu'aucune circonstance ne l'en empêchait, la cour a commis une erreur de droit. <br/>
<br/>
              5. Par suite, le ministre de l'économie et des finances, dont le moyen n'est pas nouveau en cassation, est fondé à demander, pour ce motif, l'annulation de l'arrêt attaqué en tant qu'il reconnaît à M. B...le bénéfice d'un rappel d'arrérages à compter du 30 novembre 1990, avec intérêts au taux légal et capitalisation des intérêts.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              6. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 4 de l'arrêt de la cour administrative d'appel de Paris du 18 mars 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions présentées par la SCP Lévis, avocat de M.B..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M.B....<br/>
Copie en sera adressée au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
