<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926989</ID>
<ANCIEN_ID>JG_L_2021_08_000000439715</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 06/08/2021, 439715, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439715</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439715.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A... et F... D..., M. et Mme C... et G... E... et H... B... ont demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir l'arrêté du 11 juin 2018 par lequel le maire de Vert-Saint-Denis a délivré, au nom de l'Etat, un permis de construire à la société Promocéan aux fins d'édification d'un immeuble collectif de 21 logements. Par un jugement n° 1810300 du 13 décembre 2019, le tribunal administratif a fait droit à leur demande et a annulé cet arrêté.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 mars et 23 juin 2020 et 5 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la société Promocéan demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M. D... et autres ;<br/>
<br/>
              3°) de mettre à la charge de M. D... et autres la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Promocéan et à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de M. et Mme D... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que par un arrêté du 11 juin 2018, le maire de la commune de Vert-Saint-Denis (Seine-et-Marne) a délivré, au nom de l'État, à la société Promocéan, un permis de construire valant division parcellaire aux fins d'édification d'un immeuble collectif de 21 logements. A la demande de M. D... et autres, le tribunal administratif de Melun a annulé ce permis de construire. La société Promocéan se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. En premier lieu, c'est par une appréciation souveraine exempte de dénaturation que le tribunal administratif a relevé que si la société bénéficiaire du permis attaqué soutenait, à l'appui de la fin de non-recevoir pour tardiveté qu'elle opposait à la requête, que l'affichage du permis en cause, comportant les voies et délais de recours, avait commencé à courir à compter du 3 août 2018, il ressortait des pièces du dossier qui lui était soumis que des recours gracieux formés par les requérants avaient été enregistrés à la mairie entre le 8 et le 13 août et, qu'ainsi, le délai de recours contentieux n'ayant commencé à courir qu'à compter du rejet tacite de ces derniers les 8, 9 et 13 octobre 2018, la requête dont il a été saisi, le lundi 10 décembre 2018, n'était pas tardive. En statuant ainsi, le tribunal administratif n'a pas non plus entaché son jugement d'erreur de droit ni d'erreur de qualification juridique.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article UC 6 du règlement du plan local d'urbanisme : " Implantation des constructions par rapport aux voies et emprises publiques. / Pour le calcul des marges de reculement imposées aux constructions : / - les chemins piétons ou agricoles et les pistes cyclables ne sont pas considérées comme des voies ; / - sont considérées comme des voies privées (ou cour commune), les voies librement accessibles (sans barrière, ni clôture depuis la voie publique) dont les caractéristiques sont conformes à l'article 3. / Dans l'ensemble de la zone UC / Les constructions nouvelles devront être implantées en respectant une marge de reculement au moins égale à 4 m par rapport à l'emprise de la voie de desserte du terrain. / (...) / Quand il ne s'agit pas d'annexes (surface inférieure à 30 m² et hauteur inférieure à 4 mètres) ou d'extension de bâtiments existants, la distance maximale des nouvelles constructions à l'alignement des voies publiques ou privées n'excédera pas 25 mètres. / (...) ". <br/>
<br/>
              4. Pour accueillir le moyen tiré de la méconnaissance de ces dispositions par le projet litigieux, le tribunal administratif, après avoir jugé que ces dernières, qui ont notamment pour objet de préserver " l'identité pavillonnaire de la zone considérée et l'urbanisation des coeurs d'ilot ", s'appliquent à toutes les constructions nouvelles, y compris celles implantées sur les parcelles en retrait des voies publiques ou privées, a relevé qu'il ressortait des pièces du dossier que la façade de la construction la plus proche de la rue serait située à plus de 40 mètres en retrait de la rue assurant la desserte du terrain à la date de l'autorisation considérée et qu'en outre, la profondeur de cette construction décomptée à partir de cette même façade s'étendrait sur plus de 56 mètres. En statuant ainsi, le tribunal administratif n'a ni commis d'erreur de droit, ni d'erreur de qualification juridique, et n'a pas dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article UC 10 du règlement du plan local d'urbanisme : " Hauteur maximale des constructions. / La hauteur totale des constructions nouvelles n'excèdera pas 11 mètres comptés à partir du niveau de la rue. Le nombre de niveaux habitables ne doit pas excéder 3, soit R + 1 + combles aménageables ".<br/>
<br/>
              6. Pour juger que l'arrêté litigieux avait été délivré en méconnaissance de ces dispositions, le tribunal administratif a, par une appréciation souveraine exempte de dénaturation, relevé que le dernier niveau du projet devait être regardé comme un étage à part entière et non comme des combles aménagés, conférant ainsi à la construction un type R + 2 + combles non aménageables non autorisé par les dispositions de l'article UC 10 du règlement du plan local d'urbanisme précité. <br/>
<br/>
              7. En quatrième lieu, l'article UC 11 du règlement du plan local d'urbanisme prévoit : " (...) / Toitures / (...) / a) Pentes / Les constructions nouvelles devront être couvertes par une toiture dont la pente sera comprise entre 35 et 45 °. Les toitures terrasses sont interdites. / (...) ".<br/>
<br/>
              8. C'est par une appréciation souveraine exempte de dénaturation que le tribunal a relevé que la construction projetée comportait, en sa partie centrale, deux terrasses de 48 et 50 mètres carrés chacune, accessibles depuis deux des logements aménagés au dernier niveau et qualifiées par la notice explicative du projet architectural de toitures terrasses, et en a déduit que, dès lors que ces terrasses couvraient des espaces habitables des logements situés aux étages inférieurs, elles devaient être regardées, quand bien même elles seraient situées en contrebas des éléments principaux de la toiture, comme des toitures terrasses prohibées par les dispositions de l'article UC 11 du règlement du plan local d'urbanisme. En statuant ainsi, le tribunal n'a pas commis d'erreur de droit.<br/>
<br/>
              9. En cinquième lieu, aux termes de l'article UC 13 du règlement du plan local d'urbanisme de la commune : " Obligations imposées aux constructeurs en matière de réalisation d'espaces libres, d'aires de jeux et de loisirs, et de plantations. / Les plantations existantes seront maintenues ou remplacées par des plantations équivalentes. / (...) ".<br/>
<br/>
              10. Pour juger que le projet litigieux méconnaissait ces dispositions, le tribunal administratif a relevé, par une appréciation souveraine exempte de dénaturation, que sept des arbres préexistant sur le terrain d'assiette du projet seraient conservés tandis que 23 autres seraient abattus contre seulement 12 plantés, pour en déduire que ni la plantation de haies vives, ni celle de plusieurs arbustes ne permettaient de satisfaire à l'objectif de remplacement des plantations existantes et d'équivalence des nouvelles plantations posé par l'article UC 13 précité. En statuant ainsi, le tribunal administratif n'a pas commis d'erreur de droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que la société Promocéan n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Son pourvoi doit, par suite, être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Promocéan la somme de 3 000 euros à verser à M. D... et autres au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Promocéan est rejeté. <br/>
Article 2 : La société Promocéan versera à M D... et autres une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Promocéan et à M. A... D..., premier dénommé pour l'ensemble des défendeurs.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la commune de Vert-Saint-Denis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
