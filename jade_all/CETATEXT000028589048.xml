<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028589048</ID>
<ANCIEN_ID>JG_L_2014_02_000000357215</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/90/CETATEXT000028589048.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 12/02/2014, 357215</TITRE>
<DATE_DEC>2014-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357215</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; LE PRADO ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357215.20140212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 février et 29 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune d'Epinay-Champlâtreux (95270), représentée par son maire ; la commune d'Epinay-Champlâtreux demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10VE00269 du 29 décembre 2011 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement nos 0806287-0805981-0806953 du 27 novembre 2009 par lequel le tribunal administratif de Cergy-Pontoise a annulé, à la demande du syndicat mixte d'aménagement et de gestion du parc naturel régional Oise-Pays de France, des associations Les amis de la terre du val d'Ysieux et Val-d'Oise Environnement et de l'association luzarchoise pour la sauvegarde de l'environnement, la délibération du 28 mars 2008 par laquelle son conseil municipal a approuvé la révision simplifiée du plan d'occupation des sols de la commune ; <br/>
<br/>
              2°) de mettre à la charge du syndicat mixte d'aménagement et de gestion du parc naturel régional Oise-Pays de France la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la commune d'Epinay-Champlâtreux, à la SCP Boulloche, avocat du syndicat mixte d'aménagement et de gestion du parc naturel région Oise-Pays de France et à la SCP Didier, Pinet, avocat des associations Les amis de la terre du val d'Ysieux et Val-d'Oise Environnement et de l'association luzarchoise pour la sauvegarde de l'environnement ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond  que, par une délibération du 28 mars 2008, le conseil municipal de la commune d'Epinay-Champlâtreux a approuvé, en application des articles L. 123-13 et L. 123-19 du code de l'urbanisme, une révision simplifiée du plan d'occupation des sols de la commune ; que cette révision a eu pour objet, d'une part, de classer un ensemble de terrains, antérieurement classés en zone agricole, en zone NCc, autorisant les bâtiments et aménagements agricoles, les carrières, le stockage et l'enfouissement des déchets issus de carrières et les déchetteries ainsi que les centres de tri des déchets et, d'autre part, de classer en zone ND des terrains situés en bordure de la nouvelle zone NCc ; que ces diverses modifications ont été opérées afin de permettre la réalisation d'un projet comportant une carrière à ciel ouvert, une activité de stockage de déchets industriels banals, une déchetterie publique et un centre de tri de déchets non dangereux ; qu'à la demande du syndicat mixte d'aménagement et de gestion du parc naturel régional Oise-Pays de France et autres, le tribunal administratif de Cergy-Pontoise a, par un jugement du 27 novembre 2009, annulé la délibération du 28 mars 2008 ; que, par un arrêt du 29 décembre 2011, contre lequel la commune d'Epinay-Champlâtreux se pourvoit en cassation, la cour administrative d'appel de Versailles a confirmé le jugement du tribunal administratif ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 331-3 du code de l'environnement, dans sa rédaction applicable à la date de la délibération litigieuse : " Les parcs naturels régionaux concourent à la politique de protection de l'environnement, d'aménagement du territoire, de développement économique et social et d'éducation et de formation du public. Ils constituent un cadre privilégié des actions menées par les collectivités publiques en faveur de la préservation des paysages et du patrimoine naturel et culturel. / La charte du parc détermine pour le territoire du parc naturel régional les orientations de protection, de mise en valeur et de développement et les mesures permettant de les mettre en oeuvre. Elle comporte un plan élaboré à partir d'un inventaire du patrimoine indiquant les différentes zones du parc et leur vocation. La charte détermine les orientations et les principes fondamentaux de protection des structures paysagères sur le territoire du parc. / (...) / L'Etat et les collectivités territoriales adhérant à la charte appliquent les orientations et les mesures de la charte dans l'exercice de leurs compétences sur le territoire du parc. Ils assurent, en conséquence, la cohérence de leurs actions et des moyens qu'ils y consacrent. (...) Les documents d'urbanisme doivent être compatibles avec les orientations et les mesures de la charte. " ; qu'en vertu de l'article            R. 244-1, devenu l'article R. 333-3 du même code, la charte comprend un rapport déterminant les orientations de protection, de mise en valeur et de développement envisagées pour la durée du classement et définissant les mesures qui seront mises en oeuvre sur le territoire, un plan du périmètre d'étude sur lequel sont délimitées, en fonction du patrimoine, les différentes zones où s'appliquent les orientations et les mesures définies dans le rapport et caractérisant toutes les zones du territoire selon leur nature et leur vocation dominante, enfin des annexes limitativement énumérées ;<br/>
<br/>
              3. Considérant, d'autre part, qu'il ressort des pièces du dossier soumis aux juges du fond qu'aux termes de la " notice " annexée au plan de référence de la charte du parc naturel régional Oise-Pays de France, les zones d'intérêt et de sensibilité paysagère sont " des espaces du territoire, le plus souvent à vocation agricole, jouant un rôle primordial dans l'identité, la lecture et la qualité paysagère du territoire " ; que selon la page 15 de cette même notice : " Aucune extension urbaine ne peut entamer l'intégrité de ces espaces. Les seules constructions envisageables sont : / - les constructions nécessaires à l'activité agricole, respectant la sensibilité et la qualité paysagère du site. (....) / - les équipements d'utilité publique (tels que routes, voies ferrées, stations d'épuration, châteaux d'eau, etc.), dès lors que les contraintes techniques le justifient et à condition de prendre toutes les précautions utiles pour minimiser les atteintes à l'environnement et au paysage ; / - les extensions limitées, la réparation, l'aménagement, la reconstruction après sinistre à égalité de surface de plancher hors oeuvre brut des constructions existantes, dès lors qu'elles font l'objet d'une reconnaissance réglementaire dans les documents d'urbanisme ; / - les installations légères et/ou les équipements d'intérêt public dès lors qu'ils permettent la mise en oeuvre des politiques menées pour répondre aux objectifs de la charte, notamment en matière écologique et d'accueil du public ; / (...) Concernant les zones d'intérêt et de sensibilité paysagère situées hors sites classés, l'activité d'extraction s'inscrit en cohérence avec les enjeux et les orientations définis dans la charte paysagère et les plans de paysage. Dans ces espaces, les installations et les constructions nécessaires à l'exploitation des ressources minérales sont permises pendant la durée de l'exploitation. Les projets sont exemplaires autant en matière d'intégration paysagère qu'en matière de remise en état. Cette dernière doit être cohérente avec la vocation (agricole ou forestière) et la sensibilité de l'espace concerné. (...) " ; que ces dispositions, qui doivent être interprétées à la lumière des orientations générales du rapport de la charte, limitent les possibilités de construction dans les zones d'intérêt et de sensibilité paysagère, en laissant toutefois une marge d'appréciation aux autorités compétentes pour autoriser notamment des équipements d'utilité publique, dès lors que les contraintes techniques le justifient et à condition de prendre des précautions pour minimiser les atteintes à l'environnement et au paysage ; qu'elles n'interdisent pas les activités extractives situées en dehors des sites classés, sous réserve notamment qu'elles soient cohérentes avec la vocation agricole des espaces et qu'elles soient en particulier assorties de mesures en matière d'intégration paysagère et de remise en état du site ; <br/>
<br/>
              4. Considérant qu'en se fondant, pour juger que la notice  mentionnée au point précédent devait être regardée comme faisant partie intégrante du rapport de la charte du parc et comme ayant la même portée juridique, sur le fait qu'elle définit des mesures qui constituent la déclinaison particulière, au plan local, des orientations définies par le rapport, lequel y fait référence à de multiples reprises, et ce alors même que ni l'article L. 333-1 cité au point 2 ci-dessus ni ses dispositions réglementaires d'application ne mentionnent un tel document, la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, toutefois, que, pour annuler la délibération litigieuse, au motif qu'elle était incompatible avec les dispositions de la notice rappelées au point 3 relatives aux zones d'intérêt et de sensibilité paysagère, la cour s'est fondée notamment sur l'ampleur du projet, la durée prévue de l'exploitation, ses incidences sur l'agriculture et l'environnement et sur l'absence de justification technique du choix du site retenu ; qu'il ressort cependant des pièces du dossier soumis aux juges du fond, en particulier de l'article 8 du rapport de présentation de la charte du parc, que la gestion durable des ressources naturelles est au nombre des objectifs du parc ; que cet objectif comprend notamment " la conciliation de l'industrie extractive et de la préservation de l'environnement ", ainsi que " la contribution du parc à la politique de gestion des déchets ", en particulier par " des efforts sur la résorption des dépôts sauvages et la réhabilitation des sites pollués et sur la valorisation " matière " et la valorisation " organique " des déchets " ; qu'en outre, les installations projetées, qui répondent à l'objectif d'intérêt général de traitement des déchets ménagers, font l'objet de mesures d'intégration paysagère et comportent un volet agricole, le projet prévoyant une serre de 5 000 m² chauffée par le centre de valorisation des déchets ; que le site retrouvera sa vocation agricole à l'issue de l'exploitation, conformément aux dispositions de l'article NCc1 du règlement du plan d'occupation des sols de la commune ; qu'enfin, le site retenu pour l'implantation du projet a été sélectionné  parmi sept sites, eux-mêmes choisis parmi trente-cinq sites initialement envisagés comme répondant le mieux aux contraintes géographiques, géologiques, juridiques et démographiques pesant sur le projet ;  qu'au demeurant, pour écarter le moyen tiré de ce que le classement contesté n'était entaché d'aucune erreur manifeste d'appréciation, la cour a souverainement relevé, par des motifs qui ne sont pas discutés en cassation, que les terrains concernés par la révision en cause, bien que classés par la charte du parc naturel régional en " zone d'intérêt et de sensibilité paysagère ", ne présentaient aucun caractère remarquable et que l'impact visuel du projet litigieux serait extrêmement faible ; que, dès lors, en jugeant que ce projet était incompatible avec la charte, la cour a inexactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune d'Epinay-Champlâtreux est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat mixte du parc naturel régional Oise-Pays de France la somme de 3 000 euros à verser à la commune d'Epinay-Champlâtreux, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune d'Epinay-Champlâtreux qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
      Article 1er : L'arrêt de la cour administrative d'appel de Versailles est annulé. <br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
<br/>
Article 3 : Le syndicat mixe d'aménagement du parc naturel régional Oise-Pays de France versera à la commune d'Epinay-Champlâtreux une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées, d'une part, par le syndicat mixe d'aménagement du parc naturel régional Oise-Pays de France, d'autre part, par les associations Les amis de la terre du val d'Ysieux et Val-d'Oise Environnement et par l'association luzarchoise pour la sauvegarde de l'environnement au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la commune d'Epinay-Champlâtreux, au syndicat mixte d'aménagement du parc naturel régional Oise-Pays de France, aux associations Les amis de la terre du val d'Ysieux et Val-d'Oise Environnement et à l'association luzarchoise pour la sauvegarde de l'environnement. <br/>
Copie en sera adressée pour information au ministre de l'écologie, du développement durable et de l'énergie et à la ministre du logement et de l'égalité des territoires.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-04-02 NATURE ET ENVIRONNEMENT. PARCS NATURELS. PARCS RÉGIONAUX. - CHARTE - COMPATIBILITÉ AVEC SES DISPOSITIONS D'UN PROJET DE CONSTRUCTION - CONTRÔLE DU JUGE DE CASSATION - CONTRÔLE DE LA QUALIFICATION JURIDIQUE DES FAITS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - CONTRÔLE DE LA COMPATIBILITÉ D'UN PROJET DE CONSTRUCTION AVEC LA CHARTE D'UN PARC NATUREL RÉGIONAL.
</SCT>
<ANA ID="9A"> 44-04-02 Le juge de cassation contrôle la qualification juridique des faits opérée par les juges du fond pour apprécier la compatibilité d'un projet de construction avec la charte d'un parc naturel régional.</ANA>
<ANA ID="9B"> 54-08-02-02-01-02 Le juge de cassation contrôle la qualification juridique des faits opérée par les juges du fond pour apprécier la compatibilité d'un projet de construction avec la charte d'un parc naturel régional.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
