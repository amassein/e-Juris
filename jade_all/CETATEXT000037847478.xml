<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037847478</ID>
<ANCIEN_ID>JG_L_2018_12_000000411832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/74/CETATEXT000037847478.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 21/12/2018, 411832, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411832.20181221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le Bureau européen d'assurance hospitalière (BEAH) a demandé au tribunal administratif de Montpellier, d'une part, d'annuler ou de résilier avec effet différé le marché public de prestation d'assurance en responsabilité civile passé entre le centre hospitalier de Perpignan et la société hospitalière d'assurances mutuelles (SHAM) et, d'autre part, de condamner le centre hospitalier de Perpignan à lui verser la somme de 247 270 euros en réparation du préjudice né de son éviction irrégulière de ce marché. Par un jugement n° 1301730 du 1er avril 2015, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15MA002295 du 24 avril 2017, la cour administrative d'appel de Marseille a, sur appel du BEAH, annulé ce jugement puis rejeté sa demande formée devant le tribunal administratif. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 juin et 26 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, le Bureau européen d'assurance hospitalière demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Perpignan la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Bureau européen d'assurance hospitalière, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du centre hospitalier de Perpignan, et à Me Le Prado, avocat de la société hospitalière d'assurances mutuelles.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le centre hospitalier de Perpignan a lancé, le 29 mars 2012, un appel d'offres ouvert en vue de l'attribution d'un marché public d'assurance en responsabilité civile ; que le Bureau européen d'assurance hospitalière (BEAH) a présenté une offre conjointe avec deux autres compagnies d'assurances, Amtrust International Inderwriters et Areas Assurance ; que cette offre a été rejetée, le marché litigieux ayant été attribué, pour une durée de douze mois renouvelable trois fois, à la société hospitalière d'assurances mutuelles (SHAM) ; que le BEAH a formé, le 11 avril 2013, un recours tendant à l'annulation ou à la résiliation avec effet différé du marché et à la condamnation du centre hospitalier de Perpignan à lui verser la somme de 247 270 euros en réparation du préjudice né de son éviction irrégulière ; que, par un jugement du 1er avril 2015, le tribunal administratif de Montpellier a rejeté comme tardives ses conclusions dirigées contre le marché et a rejeté comme infondées ses demandes indemnitaires ; que le BEAH se pourvoit en cassation contre l'arrêt du 24 avril 2017 par lequel la cour administrative d'appel de Marseille a, après avoir annulé ce jugement pour un motif de régularité, rejeté au fond tant ses conclusions relatives à la validité du marché que ses conclusions indemnitaires ; que, par la voie du pourvoi incident, la société SHAM conclut à l'annulation de l'arrêt en tant qu'il a jugé recevable le recours en contestation de validité du marché soulevé par le BEAH ;<br/>
<br/>
              2. Considérant, en premier lieu, que lorsque le juge d'appel statue par la voie de l'évocation, il est tenu d'examiner l'ensemble des moyens soulevés en première instance même lorsqu'ils n'ont pas été repris devant lui, à la seule exception des moyens qui ont été expressément abandonnés en appel ; qu'il ressort des écritures devant les juges du fond que le BEAH avait soulevé un moyen, qui n'était pas inopérant, tiré de ce que le centre hospitalier avait méconnu l'article 5.2. du règlement de la consultation ; que l'arrêt attaqué n'a pas répondu à ce moyen, qui n'a pas été expressément abandonné en appel ; que, par suite, le demandeur est fondé à en demander l'annulation ;<br/>
<br/>
              3. Considérant, en second lieu, qu'il ressort des énonciations mêmes de l'arrêt attaqué que les juges d'appel se sont appuyés, pour écarter le moyen tiré devant eux de ce qu'un sous-critère technique de sélection des offres aurait été modifié en cours de procédure, sur l'annexe 5 de l'offre présentée par la société retenue, dont il n'est pas contesté qu'elle n'a pas été communiquée au BEAH au motif qu'elle était protégée par le secret des affaires ; que, par suite, et alors même que ce secret lui aurait été effectivement opposable, la société demanderesse est également fondée à soutenir que l'arrêt est intervenu en méconnaissance du caractère contradictoire de la procédure ;<br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que l'arrêt du 24 avril 2017 de la cour administrative d'appel de Marseille doit être annulé ;<br/>
<br/>
              5. Considérant que l'annulation de l'arrêt de la cour administrative d'appel de Marseille prive d'objet le pourvoi incident de la société SHAM ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Perpignan et de la société SHAM la somme de 1 500 euros à verser chacune au BEAH, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du BEAH qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 24 avril 2017 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
Article 3 : Il n'y a pas lieu de statuer sur le pourvoi incident de la société hospitalière d'assurances mutuelles.<br/>
Article 4 : Le centre hospitalier de Perpignan et la société hospitalière d'assurances mutuelles verseront chacune au BEAH une somme de  1 500 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par le centre hospitalier de Perpignan et la société hospitalière d'assurances mutuelles sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée au Bureau européen d'assurance hospitalière, au centre hospitalier de Perpignan et à la société hospitalière d'assurances mutuelles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
