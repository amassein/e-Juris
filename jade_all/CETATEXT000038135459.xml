<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038135459</ID>
<ANCIEN_ID>JG_L_2019_02_000000422283</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/13/54/CETATEXT000038135459.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 13/02/2019, 422283</TITRE>
<DATE_DEC>2019-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422283</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2019:422283.20190213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêt n° 16PA02914 du 10 juillet 2018, enregistré le 11 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Paris, avant de statuer sur l'appel de M. B...A...et autres tendant à l'annulation du jugement n°1500343-1500376-1600045 du 10 juin 2016 par lequel le tribunal administratif de Nouvelle-Calédonie a rejeté leur demande d'annulation pour excès de pouvoir de l'arrêté du 3 avril 2014 du maire de Nouméa autorisant la SCI Les Bambous II à construire une villa au 2, Henry Wetta, de l'arrêté du 7 juillet 2015 transférant ce permis de construire à la SCI Bambou-Wetta et de l'arrêté du 7 décembre 2015 modifiant l'arrêté du 3 avril 2014, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Des dispositions de la nature de celles prévues aux articles R. 424-15 et A. 424-17 du code de l'urbanisme, prescrivant, s'agissant de l'affichage sur le terrain d'un permis de construire, les mentions relatives aux voies et délais de recours et à l'obligation, prévue à peine d'irrecevabilité par l'article R. 600-1 du même code, de notifier tout recours administratif ou tout recours contentieux à l'auteur de la décision et au bénéficiaire du permis, constituent-elles, soit des règles relevant de la procédure administrative contentieuse, des droits des citoyens dans leurs relations avec les administrations des communes ou du régime des actes des autorités communales, ressortissant, comme telles, à la compétence des autorités de l'État et applicables, le cas échéant, de plein droit en Nouvelle-Calédonie, soit comme des règles de forme ou de procédure intervenant dans une matière ressortissant à la compétence respective de la Nouvelle-Calédonie et de ses provinces ' <br/>
<br/>
              2°) Si les règles d'affichage de ces mentions relèvent de la compétence de l'État, doit-on regarder comme en constituant des accessoires indissociables celles qui déterminent la durée de l'affichage et les conditions destinées à assurer sa visibilité effective ' <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
              - le code de l'urbanisme de la Nouvelle-Calédonie ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Rend l'avis suivant :<br/>
<br/>
              1. En premier lieu, la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie dispose à l'article 21 que : "  I- L'Etat est compétent dans les matières suivantes: (...) 2° (...) procédure administrative contentieuse  (...) ". La loi organique du 3 août 2009 a inséré dans cette loi organique un article 6-2 aux termes duquel : " (...) sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions les adaptant à son organisation particulière, les dispositions législatives et réglementaires qui sont relatives : (lisibles de la voie publique ou des espaces ouverts au public pendant toute la durée du chantier) 6° A la procédure administrative contentieuse ". <br/>
<br/>
              2. En deuxième lieu, aux termes de l'article R. 600-1 du code de l'urbanisme : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un certificat d'urbanisme, d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir, le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. (...) L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif. (...) ". L'article R. 600-2 du même code prévoit que : " Le délai de recours contentieux à l'encontre d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir court à l'égard des tiers à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain des pièces mentionnées à l'article R. 424-15 ".<br/>
<br/>
              3. En troisième lieu, aux termes de l'article R. 424-15 du code de l'urbanisme : " Mention du permis explicite ou tacite ou de la déclaration préalable doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté ou dès la date à laquelle le permis tacite ou la décision de non-opposition à la déclaration préalable est acquis et pendant toute la durée du chantier (...) / Cet affichage mentionne également l'obligation, prévue à peine d'irrecevabilité par l'article R. 600-1, de notifier tout recours administratif ou tout recours contentieux à l'auteur de la décision et au bénéficiaire du permis ou de la décision prise sur la déclaration préalable./ (...) Un arrêté du ministre chargé de l'urbanisme règle le contenu et les formes de l'affichage ". L'article A. 424-15 dispose que : " L'affichage sur le terrain du permis de construire, d'aménager ou de démolir explicite ou tacite ou l'affichage de la déclaration préalable, prévu par l'article R. 424-15, est assuré par les soins du bénéficiaire du permis ou du déclarant sur un panneau rectangulaire dont les dimensions sont supérieures à 80 centimètres ". L'article A. 424-16 précise que : " Le panneau prévu à l'article A. 424-15 indique le nom, la raison sociale ou la dénomination sociale du bénéficiaire, le nom de l'architecte auteur du projet architectural, la date de délivrance, le numéro du permis, la nature du projet et la superficie du terrain ainsi que l'adresse de la mairie où le dossier peut être consulté./ Il indique également, en fonction de la nature du projet : / a) Si le projet prévoit des constructions, la surface de plancher autorisée ainsi que la hauteur de la ou des constructions, exprimée en mètres par rapport au sol naturel ; / b) Si le projet porte sur un lotissement, le nombre maximum de lots prévus ;/ c) Si le projet porte sur un terrain de camping ou un parc résidentiel de loisirs, le nombre total d'emplacements et, s'il y a lieu, le nombre d'emplacements réservés à des habitations légères de loisirs ; / d) Si le projet prévoit des démolitions, la surface du ou des bâtiments à démolir ". L'article A. 424-17 ajoute que : " Le panneau d'affichage comprend la mention suivante : / " Droit de recours : / " Le délai de recours contentieux est de deux mois à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain du présent panneau (art. R. 600-2 du code de l'urbanisme). / " Tout recours administratif ou tout recours contentieux doit, à peine d'irrecevabilité, être notifié à l'auteur de la décision et au bénéficiaire du permis ou de la décision prise sur la déclaration préalable. Cette notification doit être adressée par lettre recommandée avec accusé de réception dans un délai de quinze jours francs à compter du dépôt du recours (art. R. 600-1 du code de l'urbanisme) " ". Enfin, aux termes de l'article A. 424-18 : " Le panneau d'affichage doit être installé de telle sorte que les renseignements qu'il contient demeurent... ".<br/>
<br/>
              4. L'obligation d'affichage sur le terrain de mentions relatives à la consistance du projet et de l'indication des voies et délais de recours contentieux a pour objet de permettre aux tiers de préserver leurs droits et constitue une condition au déclenchement du délai de recours contentieux. Elle revêt dès lors le caractère d'une règle de procédure administrative contentieuse. <br/>
<br/>
              5. Par ailleurs, si le rappel à titre d'information des tiers, sur le panneau d'affichage, de l'obligation de notification à peine d'irrecevabilité du recours contentieux résultant de l'article R. 600-1 du code de l'urbanisme n'est pas au nombre des éléments dont la présence est une condition au déclenchement du délai de recours, son omission fait obstacle à ce que soit opposée à l'auteur du recours l'irrecevabilité qu'il prévoit. Dès lors, eu égard à son objet et à ses effets, l'obligation de mentionner sur le panneau d'affichage l'obligation de notification résultant de l'article R. 600-1 du code de l'urbanisme revêt également le caractère d'une règle de procédure administrative contentieuse. <br/>
<br/>
              6. En application du 2° de l'article 21 de la loi organique du 19 mars 1999 cité au point 1, la procédure administrative contentieuse relève de la compétence de l'Etat en Nouvelle-Calédonie. L'article 6-2 de la même loi organique précise que les dispositions législatives et réglementaires qui y sont relatives sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.<br/>
<br/>
              7. Il s'ensuit que, d'une part, l'obligation d'affichage sur le terrain des mentions relatives à la consistance du projet, aux voies et délais de recours et à l'obligation de notification prévue à peine d'irrecevabilité par l'article R. 600-1 du code de l'urbanisme relève de la compétence de l'Etat et, d'autre part, les dispositions correspondantes des articles R. 424-15, A. 424-16 et A. 424-17 du même code sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière. <br/>
<br/>
              8. L'Etat étant également compétent pour déterminer les règles accessoires se rattachant aux domaines relevant de sa compétence, il lui revient d'arrêter les règles relatives à la durée et aux modalités de l'affichage, notamment les conditions destinées à assurer sa visibilité effective. Il s'ensuit que les dispositions correspondantes des articles R. 424-15, A. 424-15 et A. 424-18 du code de l'urbanisme sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.<br/>
<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié à la cour administrative d'appel de Paris, à M. B...A..., premier dénommé, au Gouvernement de la Nouvelle-Calédonie, à la Province Sud de Nouvelle-Calédonie, au ministre des outre-mer et au haut-commissaire de la République en Nouvelle-Calédonie. <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. DROIT APPLICABLE. STATUTS. NOUVELLE-CALÉDONIE. - 1) COMPÉTENCES NORMATIVES DE LA COLLECTIVITÉ - RÈGLES DE PROCÉDURE ADMINISTRATIVE CONTENTIEUSE - EXCLUSION - CONSÉQUENCE - APPLICATION DE PLEIN DROIT DES RÈGLES ÉDICTÉES PAR L'ETAT (ART. 6-2 DE LA LOI ORGANIQUE DU 19 MARS 1999, ISSU DE LA LOI ORGANIQUE DU 3 AOÛT 2009) [RJ1] - 2) APPLICATION - OBLIGATION D'AFFICHAGE SUR LE TERRAIN DES MENTIONS RELATIVES, D'UNE PART, À LA CONSISTANCE DU PROJET ET AUX VOIES ET DÉLAIS DE RECOURS, D'AUTRE PART, À L'OBLIGATION DE NOTIFICATION PRÉVUE À PEINE D'IRRECEVABILITÉ PAR L'ARTICLE R. 600-1 DU CODE DE L'URBANISME (ART. R. 424-15, A. 424-16 ET A. 424-17 DU CODE DE L'URBANISME) - APPLICATION DE PLEIN DROIT EN NOUVELLE-CALÉDONIE [RJ2], Y COMPRIS DES RÈGLES ACCESSOIRES [RJ3] RELATIVES À LA DURÉE ET AUX MODALITÉS DE CET AFFICHAGE (ART. R. 424-15, A. 424-15 ET A. 424-18 DE CE CODE).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-03-02-02-01 OUTRE-MER. DROIT APPLICABLE. LOIS ET RÈGLEMENTS (HORS STATUTS DES COLLECTIVITÉS). COLLECTIVITÉS D'OUTRE-MER ET NOUVELLE-CALÉDONIE. NOUVELLE-CALÉDONIE. RÉPARTITIONS DES COMPÉTENCES ENTRE L'ETAT ET LES AUTRES AUTORITÉS. - COMPÉTENCE DE L'ETAT - 1) RÈGLES DE PROCÉDURE ADMINISTRATIVE CONTENTIEUSE (2° DU I DE L'ART. 21 DE LA LOI ORGANIQUE DU 19 MARS 1999) [RJ1] - NOTION - OBLIGATION D'AFFICHAGE SUR LE TERRAIN DES MENTIONS RELATIVES, D'UNE PART, À LA CONSISTANCE DU PROJET ET AUX VOIES ET DÉLAIS DE RECOURS, D'AUTRE PART, À L'OBLIGATION DE NOTIFICATION PRÉVUE À PEINE D'IRRECEVABILITÉ PAR L'ARTICLE R. 600-1 DU CODE DE L'URBANISME (ART. R. 424-15, A. 424-16 ET A. 424-17 DU CODE DE L'URBANISME) - INCLUSION [RJ2] - 2) RÈGLES ACCESSOIRES SE RATTACHANT AUX DOMAINES RELEVANT DE SA COMPÉTENCE [RJ3] - APPLICATION - RÈGLES RELATIVES À LA DURÉE ET AUX MODALITÉS DE CET AFFICHAGE (ART. R. 424-15, A. 424-15 ET A. 424-18 DE CE CODE).
</SCT>
<ANA ID="9A"> 46-01-02-01 1) En application du 2° de l'article 21 de la loi organique n° 99-209 du 19 mars 1999, la procédure administrative contentieuse relève de la compétence de l'Etat en Nouvelle-Calédonie. L'article 6-2 de la même loi organique précise que les dispositions législatives et réglementaires qui y sont relatives sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.,,2) L'obligation d'affichage sur le terrain de mentions relatives à la consistance du projet et de l'indication des voies et délais de recours contentieux a pour objet de permettre aux tiers de préserver leurs droits et constitue une condition au déclenchement du délai de recours contentieux. Elle revêt dès lors le caractère d'une règle de procédure administrative contentieuse.... ...Par ailleurs, si le rappel à titre d'information des tiers, sur le panneau d'affichage, de l'obligation de notification à peine d'irrecevabilité du recours contentieux résultant de l'article R. 600-1 du code de l'urbanisme n'est pas au nombre des éléments dont la présence est une condition au déclenchement du délai de recours, son omission fait obstacle à ce que soit opposée à l'auteur du recours l'irrecevabilité qu'il prévoit. Dès lors, eu égard à son objet et à ses effets, l'obligation de mentionner sur le panneau d'affichage l'obligation de notification résultant de l'article R. 600-1 du code de l'urbanisme revêt également le caractère d'une règle de procédure administrative contentieuse.... ...Il s'ensuit que, d'une part, l'obligation d'affichage sur le terrain des mentions relatives à la consistance du projet, aux voies et délais de recours et à l'obligation de notification prévue à peine d'irrecevabilité par l'article R. 600-1 du code de l'urbanisme relève de la compétence de l'Etat et, d'autre part, les dispositions correspondantes des articles R. 424-15, A. 424-16 et A. 424-17 du même code sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.... ...L'Etat étant également compétent pour déterminer les règles accessoires se rattachant aux domaines relevant de sa compétence, il lui revient d'arrêter les règles relatives à la durée et aux modalités de l'affichage, notamment les conditions destinées à assurer sa visibilité effective. Il s'ensuit que les dispositions correspondantes des articles R. 424-15, A. 424-15 et A. 424-18 du code de l'urbanisme sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.</ANA>
<ANA ID="9B"> 46-01-03-02-02-01 1) L'obligation d'affichage sur le terrain de mentions relatives à la consistance du projet et de l'indication des voies et délais de recours contentieux a pour objet de permettre aux tiers de préserver leurs droits et constitue une condition au déclenchement du délai de recours contentieux. Elle revêt dès lors le caractère d'une règle de procédure administrative contentieuse.... ...Par ailleurs, si le rappel à titre d'information des tiers, sur le panneau d'affichage, de l'obligation de notification à peine d'irrecevabilité du recours contentieux résultant de l'article R. 600-1 du code de l'urbanisme n'est pas au nombre des éléments dont la présence est une condition au déclenchement du délai de recours, son omission fait obstacle à ce que soit opposée à l'auteur du recours l'irrecevabilité qu'il prévoit. Dès lors, eu égard à son objet et à ses effets, l'obligation de mentionner sur le panneau d'affichage l'obligation de notification résultant de l'article R. 600-1 du code de l'urbanisme revêt également le caractère d'une règle de procédure administrative contentieuse.... ...En application du 2° de l'article 21 de la loi organique n° 99-209 du 19 mars 1999, la procédure administrative contentieuse relève de la compétence de l'Etat en Nouvelle-Calédonie. L'article 6-2 de la même loi organique précise que les dispositions législatives et réglementaires qui y sont relatives sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.,,Il s'ensuit que, d'une part, l'obligation d'affichage sur le terrain des mentions relatives à la consistance du projet, aux voies et délais de recours et à l'obligation de notification prévue à peine d'irrecevabilité par l'article R. 600-1 du code de l'urbanisme relève de la compétence de l'Etat et, d'autre part, les dispositions correspondantes des articles R. 424-15, A. 424-16 et A. 424-17 du même code sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.... ...2) L'Etat étant également compétent pour déterminer les règles accessoires se rattachant aux domaines relevant de sa compétence, il lui revient d'arrêter les règles relatives à la durée et aux modalités de l'affichage, notamment les conditions destinées à assurer sa visibilité effective. Il s'ensuit que les dispositions correspondantes des articles R. 424-15, A. 424-15 et A. 424-18 du code de l'urbanisme sont applicables de plein droit en Nouvelle-Calédonie, sans préjudice des dispositions prises par l'Etat les adaptant à son organisation particulière.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de Saint-Barthélemy, CE, 8 avril 2015,,, n° 368349, T. p. 770-926.,,[RJ2] Comp., sous l'empire de la législation antérieure à la loi organique du 19 mars 1999, CE, 24 octobre 1997,,, n° 168863, T. pp. 953-993-1133., ,[RJ3] Rappr., s'agissant de l'extension de la compétence aux règles accessoires, CE, 13 juin 2013,,, n° 361767 361768 361912 361913 361990 361991 362028, T. p. 719.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
