<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022273</ID>
<ANCIEN_ID>JG_L_2018_06_000000403303</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022273.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème chambres réunies, 06/06/2018, 403303, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403303</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:403303.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SA Carrefour a demandé au tribunal administratif de Montreuil de prononcer la restitution des cotisations d'impôt sur les sociétés et de contribution sociale sur cet impôt dont elle estime s'être acquittée à tort au titre de l'exercice clos en 2006.<br/>
<br/>
              Par un jugement n° 1309291 du 1er juillet 2014, le tribunal administratif de Montreuil a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14VE02647 du 5 juillet 2016, la cour administrative d'appel de Versailles a, sur appel de la société Carrefour, annulé ce jugement et fait droit aux conclusions de la société.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 7 septembre 2016 et le 30 mai 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de rejeter l'appel de la société Carrefour.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Carrefour ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société anonyme Promodès, aux droits de laquelle est venue la société Carrefour, a souscrit le 12 mars 1991 un contrat de prêt subordonné à durée indéterminée (PSDI) d'un montant de 1,375 milliard de francs (209,6 millions d'euros) auprès de l'établissement bancaire Barclays. Cette convention de prêt prévoyait qu'une partie de la somme prêtée, représentative des futurs intérêts précomptés, était immédiatement investie en obligations zéro-coupon pour être confiée à des trusts situés dans des pays à fiscalité privilégiée, contrôlés par la société Promodès, à charge pour ces trusts de rembourser ce prêt à sa valeur nominale, à l'échéance principale de quinze ans, au moyen de cette somme augmentée des intérêts capitalisés en franchise d'impôt. Au titre de l'exercice clos en 2006, la société Carrefour a inclus dans ses résultats déclarés pour l'établissement de l'impôt sur les sociétés et de la contribution sociale additionnelle à cet impôt, les sommes représentatives des produits financiers perçus par les trusts mentionnés ci-dessus en application des dispositions de l'article 238 bis-0 I bis du code général des impôts. La société Carrefour a vainement demandé à l'administration de prononcer la restitution des cotisations d'impôt sur les sociétés et de contribution sociale sur cet impôt dont elle estimait s'être ainsi acquittée à tort au titre de l'exercice clos en 2006. Par un jugement du 5 mai 2014, le tribunal administratif de Montreuil a rejeté cette demande de restitution. Le ministre de l'économie se pourvoit en cassation contre l'arrêt du 5 juillet 2016 par lequel la cour administrative d'appel de Versailles a, sur appel de la société Carrefour, annulé ce jugement et fait droit à sa demande.<br/>
<br/>
              2. D'une part, aux termes de l'article 238 bis-0 I bis du code général des impôts : " I. Les produits provenant du placement de la fraction des sommes reçues lors de l'émission de valeurs mobilières relevant des dispositions de l'article L. 228-97 du code de commerce transférée hors de France à une personne ou une entité, directement ou indirectement, par l'entreprise émettrice ou par l'intermédiaire d'un tiers, sont compris dans le résultat imposable de cette entreprise au titre du premier exercice clos à compter du 31 décembre 2005 ou, s'il est postérieur, de l'exercice clos au cours de la quinzième année qui suit la date d'émission, sous déduction des intérêts déjà imposés sur cette même fraction postérieurement à la date du douzième anniversaire de l'émission. Pour l'application des dispositions de la phrase précédente, le montant de ces produits est réputé égal à la différence entre le montant nominal de l'émission et la fraction transférée hors de France majorée des intérêts capitalisés, jusqu'à ce douzième anniversaire, calculés au taux actuariel (...) à la date du transfert (...) / II. Les dispositions du I s'appliquent aux émissions de valeurs mobilières réalisées entre le 1er janvier 1988 et le 31 décembre 1991 ainsi qu'aux émissions réalisées en 1992 sous réserve que les produits mentionnés au I n'aient pas été imposés sur le fondement de l'article 238 bis-0 I, et dont les dettes corrélatives sont inscrites au bilan d'ouverture du premier exercice clos à compter du 31 décembre 2005 de l'entreprise émettrice ". D'autre part, aux termes de l'article L. 228-97 du code de commerce : " Lors de l'émission de valeurs mobilières représentatives de créances sur la société émettrice, y compris celles donnant le droit de souscrire ou d'acquérir une valeur mobilière, il peut être stipulé que ces valeurs mobilières ne seront remboursées qu'après désintéressement des autres créanciers (...) ".<br/>
<br/>
              3. Il résulte des dispositions de l'article L. 238 bis-0 I bis du code général des impôts, combinées avec celles de l'article L. 228-97 du code de commerce, qu'elles ne visent que l'imposition des produits des placements hors de France de la fraction des fonds reçus à l'occasion de l'émission de valeurs mobilières.<br/>
<br/>
              4. Dès lors, en se fondant sur la circonstance que si la convention de prêt qu'avait conclue la société Promodès avec la banque Barclays, qualifiée par les parties de prêt subordonné à durée indéterminée reconditionné, avait les mêmes caractéristiques que les opérations comportant l'émission de titres subordonnés à durée indéterminée, elle n'avait pas donné lieu à l'émission de valeurs mobilières, pour en déduire que les produits tirés d'une fraction des sommes reçues lors de la conclusion de ce prêt n'entraient pas dans le champ d'application de l'article 238 bis-0 I bis du code général des impôts, la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi du ministre de l'économie et des finances doit être rejeté. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Carrefour au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à la société Carrefour au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société Carrefour.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
