<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028217624</ID>
<ANCIEN_ID>JG_L_2013_11_000000353691</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/21/76/CETATEXT000028217624.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 19/11/2013, 353691</TITRE>
<DATE_DEC>2013-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353691</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Agnès Martinel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:353691.20131119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 11NC01599 du 11 octobre 2011, enregistrée le 27 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par Mme B...A... ; <br/>
<br/>
              Vu le pourvoi, enregistré le 30 septembre 2011 au greffe de la cour administrative d'appel de Nancy et le mémoire complémentaire, enregistré le 15 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n°1100288 du 2 août 2011 par lequel le tribunal administratif de Besançon a rejeté sa demande tendant à l'annulation des décisions en date des 22 et 30 décembre 2010 par lesquelles la directrice de l'agence régionale de santé de <br/>
Franche-Comté l'a placée en situation d'astreinte du vendredi 31 décembre 2010 à 17 heures au lundi 3 janvier 2011 à 8 h 30 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Vu le décret n° 2000-815 du 25 août 2000 ; <br/>
<br/>
              Vu le décret n° 2009-924 du 27 juillet 2009 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Martinel, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de Mme B...A...et à la SCP Bouzidi, Bouhanna, avocat de l'agence régionale de santé de Franche-Comté ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...exerce les fonctions d'ingénieur du génie sanitaire à l'agence régionale de santé (ARS) de Franche-Comté ; que par une lettre du 22 décembre 2010, la directrice générale de l'ARS de Franche-Comté l'a placée en situation d'astreinte du vendredi 31 décembre 2010 à 17 heures au lundi 3 janvier 2011 à 8 h 30 ; que sa réclamation ayant été rejetée par la directrice générale le 30 décembre 2010, Mme A...a demandé au tribunal administratif de Besançon d'annuler pour excès de pouvoir ces décisions ; que par le jugement attaqué du 2 août 2011, le tribunal administratif de Besançon a rejeté sa demande ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du I de l'article L.1432-11 du code de la santé publique : " Il est institué dans chaque agence régionale de santé un comité d'agence et un comité d'hygiène, de sécurité et des conditions de travail, compétents pour l'ensemble du personnel de l'agence. / 1. Le comité d'agence exerce les compétences prévues au II de l'article 15 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat et celles prévues au chapitre III du titre II du livre III de la deuxième partie du code du travail, sous réserve des adaptations prévues par décret en Conseil d'Etat en application de l'article L. 2321-1 du même code. (...) " ; qu'aux termes du deuxième alinéa de l'article 5 du décret du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat : " Des arrêtés du ministre intéressé, du ministre chargé de la fonction publique et du ministre chargé du budget, pris après consultation des comités techniques paritaires ministériels, déterminent les cas dans lesquels il est possible de recourir à des astreintes. Les modalités de leur rémunération ou de leur compensation sont fixées par décret. La liste des emplois concernés et les modalités d'organisation des astreintes sont fixées après consultation des comités techniques paritaires. " ; <br/>
<br/>
              4. Considérant que, s'il résulte de la combinaison de ces dispositions que le directeur général d'une agence régionale de santé ne peut en principe fixer les modalités d'organisation des astreintes qu'après consultation du comité d'agence, elles ne font pas obstacle à ce qu'en cas d'urgence il prenne, dans le cadre de son pouvoir d'organisation du service, toute mesure destinée à garantir la continuité du service public ; que, par suite, le tribunal administratif n'a pas commis d'erreur de droit en jugeant, après avoir constaté, par un motif non contesté en cassation, l'existence d'une situation d'urgence et relevé que la décision d'organisation des astreintes n'avait pas été précédée de la consultation de l'instance provisoire compétente avant l'installation du comité d'agence, que, compte tenu des missions confiées aux agences régionales de santé par la loi du 21 juillet 2009 et par les dispositions des articles L. 1435-1 et L. 1435-2 du code de la santé publique en matière de veille et de sécurité sanitaire, il appartenait à la directrice générale de l'agence régionale de santé de Franche-Comté, dans le cadre de son pouvoir d'organisation du service, de garantir l'effectivité du principe de continuité du service public et de prendre toutes les mesures nécessaires à cette fin, dès lors que le nombre de volontaires s'avérait insuffisant pour assurer la continuité du dispositif d'astreintes ; <br/>
<br/>
              5. Considérant, en second lieu, qu'il résulte de ce qui vient d'être dit que les décisions attaquées n'ont pas pour fondement le décret du 27 juillet 2009 relatif aux modalités de rémunération ou de compensation des astreintes et des interventions de certains personnels relevant des ministères chargés des affaires sociales ; qu'il suit de là, d'une part, que le tribunal administratif n'a pas commis d'erreur de droit en écartant comme inopérant le moyen tiré, par voie d'exception, de l'illégalité de ce décret et, d'autre part, que la requérante ne peut utilement critiquer en cassation le motif par lequel il s'est prononcé sur l'applicabilité des dispositions de ce décret aux agents des agences régionales de santé ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation du jugement qu'elle attaque, qui est suffisamment motivé et n'est pas entaché de contradiction de motifs ; <br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'ARS de Franche-Comté au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : Les conclusions de l'agence régionale de santé de Franche-Comté présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B... A...et à l'agence régionale de santé de Franche-Comté.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DIVERSES DÉTENTRICES D`UN POUVOIR RÉGLEMENTAIRE. - DIRECTEUR GÉNÉRAL D'AGENCE RÉGIONALE DE SANTÉ - TEXTE SUBORDONNANT LA FIXATION DES MODALITÉS D'ORGANISATION DES ASTREINTES À LA CONSULTATION PRÉALABLE DU COMITÉ D'AGENCE - POSSIBILITÉ POUR LE DIRECTEUR GÉNÉRAL DE PRENDRE UNE MESURE DE MÊME PORTÉE AU TITRE DE SES POUVOIRS PROPRES D'ORGANISATION DU SERVICE [RJ1] - EXISTENCE - CONDITION - URGENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-09-02-03 SANTÉ PUBLIQUE. ADMINISTRATION DE LA SANTÉ. - ORGANISATION DES ASTREINTES - TEXTE SUBORDONNANT LA FIXATION DES MODALITÉS D'ORGANISATION DES ASTREINTES À LA CONSULTATION PRÉALABLE DU COMITÉ D'AGENCE - POSSIBILITÉ POUR LE DIRECTEUR GÉNÉRAL DE PRENDRE UNE MESURE DE MÊME PORTÉE AU TITRE DE SES POUVOIRS PROPRES D'ORGANISATION DU SERVICE [RJ1] - EXISTENCE - CONDITION - URGENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-02-02-01-07 S'il résulte du I de l'article L. 1432-11 du code de la santé publique et du deuxième alinéa de l'article 5 du décret n° 2000-815 du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat que le directeur général d'une agence régionale de santé ne peut en principe fixer les modalités d'organisation des astreintes qu'après consultation du comité d'agence, ces dispositions ne font pas obstacle à ce qu'en cas d'urgence il prenne, dans le cadre de son pouvoir d'organisation du service, toute mesure destinée à garantir la continuité du service public, y compris afin d'organiser les astreintes.</ANA>
<ANA ID="9B"> 61-09-02-03 S'il résulte du I de l'article L. 1432-11 du code de la santé publique et du deuxième alinéa de l'article 5 du décret n° 2000-815 du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat que le directeur général d'une agence régionale de santé ne peut en principe fixer les modalités d'organisation des astreintes qu'après consultation du comité d'agence, ces dispositions ne font pas obstacle à ce qu'en cas d'urgence il prenne, dans le cadre de son pouvoir d'organisation du service, toute mesure destinée à garantir la continuité du service public, y compris afin d'organiser les astreintes.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 7 février 1936, Jamart, n° 43321, p. 172.,,[RJ2] Comp., pour le cas où le pouvoir Jamart ne s'exerce pas dans l'urgence, CE, Section, 8 janvier 1982, S.A.R.L. Chocolat de régime Dardenne, n° 17270, p. 1.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
