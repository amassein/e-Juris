<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629963</ID>
<ANCIEN_ID>JG_L_2015_10_000000393508</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 14/10/2015, 393508, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393508</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP LESOURD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:393508.20151014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire de production, enregistrés les                                    14 septembre et 8 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Vaillance Courtage demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 20 juillet 2015 par laquelle l'Autorité de contrôle prudentiel et de résolution a, d'une part, prononcé à son encontre un blâme et une sanction pécuniaire de 20 000 euros, d'autre part, décidé que celle-ci sera publiée au registre de l'Autorité de contrôle prudentiel et de résolution et pourra être consultée au secrétariat de la commission ; <br/>
<br/>
              2°) de mettre à la charge de l'Autorité de contrôle prudentiel et de résolution le paiement des dépens ;<br/>
<br/>
              3°) de mettre à la charge de l'Autorité de contrôle prudentiel et de résolution la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est caractérisée dès lors que la décision de la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution fait l'objet d'une publicité qui nuit à l'image de la société ; <br/>
              - il existe un doute sérieux sur la légalité de la décision contestée ;<br/>
              - elle a été prise selon une procédure irrégulière, en méconnaissance du droit au procès équitable garanti par les articles 6 et 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que, d'une part, la société n'a pas été informée de la possibilité de se faire assister durant la procédure devant la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution et que, d'autre part, il n'y a pas de prescription applicable aux poursuites disciplinaires de la commission ;  <br/>
              - elle méconnaît le principe de personnalité des délits et des peines garanti par les articles 8 et 9 de la Déclaration des droits de l'homme et du citoyen dès lors que la société est poursuivie pour des faits commis par des mandataires d'assurance et par la société Groupe Vaillance Conseil ; <br/>
              - elle méconnaît les dispositions des articles L. 132-27-1 et R. 132-5-1-1 du code des assurances, dès lors que la société a satisfait à son devoir de conseil et d'information ; <br/>
              - elle est entachée d'une erreur d'appréciation des faits et d'une erreur de droit,   dès lors qu'elle a estimé que la souscription multiple ou successive de contrats d'assurance avait un impact défavorable sur les souscripteurs ; <br/>
              - elle est entachée d'une erreur de droit, dès lors que la commercialisation de produits en unités de compte n'est pas prohibée et n'a pas eu pour effet de fournir une information inexacte et trompeuse à ses clients. <br/>
<br/>
              Par un mémoire en défense, enregistré le 5 octobre 2015, l'Autorité de contrôle prudentiel et de résolution conclut au rejet de la requête et à ce qu'une somme de 6 000 euros soit mise à la charge de la société Vaillance Courtage au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la société Vaillance Courtage ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code des assurances ;<br/>
              - le code monétaire et financier ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Vaillance Courtage et, d'autre part, l'Autorité de contrôle prudentiel et de résolution ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 9 octobre 2015 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Lesourd, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Vaillance Courtage ;<br/>
<br/>
              - les représentants de la société Vaillance Courtage ;<br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Autorité de contrôle prudentiel  et de résolution ;<br/>
              - les représentants de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 612-1 du code monétaire et financier : " I.-L'Autorité de contrôle prudentiel et de résolution, autorité administrative indépendante, veille à la préservation de la stabilité du système financier et à la protection des clients, assurés, adhérents et bénéficiaires des personnes soumises à son contrôle. L'Autorité contrôle le respect par ces personnes des dispositions européennes qui leur sont directement applicables, des dispositions du code monétaire et financier ainsi que des dispositions réglementaires prévues pour son application, du code des assurances, du livre IX du code de la sécurité sociale, du code de la mutualité, du livre III du code de la consommation, des articles 26-4 à 26-8 de la loi n° 65-557 du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis, des codes de conduite homologués ainsi que de toute autre disposition législative et réglementaire dont la méconnaissance entraîne celle des dispositions précitées. / IV.-Pour l'accomplissement de ses missions, l'Autorité de contrôle prudentiel et de résolution dispose, à l'égard des personnes mentionnées à l'article L. 612-2 et au I et, le cas échéant, au II de l'article L. 613-34, d'un pouvoir de contrôle, du pouvoir de prendre des mesures de police administrative et d'un pouvoir de sanction. Elle peut en outre porter à la connaissance du public toute information qu'elle estime nécessaire à l'accomplissement de ses missions, sans que lui soit opposable le secret professionnel mentionné à l'article L. 612-17. " ;<br/>
<br/>
              3. Considérant qu'à la suite d'un contrôle de la société Vaillance Courtage diligenté par l'Autorité de contrôle prudentiel et de résolution, effectué sur place du 28 mars 2013 au 17 septembre 2013, un rapport définitif a été établi le 28 juillet 2014, au vu duquel le collège de supervision de l'Autorité, statuant en sous-collège sectoriel de l'assurance, a décidé, lors de sa séance du 17 octobre 2014, d'ouvrir une procédure disciplinaire à l'encontre de la société ; que le président de ce collège a, par une lettre du 3 décembre 2014, notifié à la société Vaillance Courtage les griefs retenus à son encontre dans ce cadre ; que la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution a, par une décision rendue le 20 juillet et après avoir entendu les représentants de la société au cours de l'audience du 6 juillet 2015, prononcé à son encontre un blâme ainsi qu'une sanction pécuniaire de 20 000 euros et ordonné la publication de cette décision sous forme nominative au registre de l'Autorité de contrôle prudentiel et de résolution, pour des manquements aux dispositions des articles L. 132-27-1 et L. 520-1 du code des assurances ; que la société Vaillance Courtage demande, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cette décision ;<br/>
<br/>
              Sans qu'il soit besoin de se prononcer sur l'urgence :<br/>
<br/>
              Sur la régularité de la procédure :<br/>
<br/>
              4. Considérant que si, lorsqu'elle est saisie d'agissements pouvant donner lieu aux sanctions prévues par le code monétaire et financier, la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution doit être regardée comme décidant du bien-fondé d'accusations en matière pénale au sens des stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, le principe des droits de la défense, rappelé tant par l'article 6 de cette convention que par l'article L. 612-38 du code monétaire et financier, s'applique seulement à la procédure de sanction ouverte par la notification de griefs par le collège de l'Autorité et par la saisine de la commission des sanctions, et non à la phase préalable de contrôle prévue par l'article L. 612-23 de ce code ; que la société requérante n'est pas non plus fondée à se prévaloir d'une violation des dispositions de l'article R.  612-27 du code monétaire et financier découlant de ce que, contrairement à ce qu'elles prévoient, ni la société Vaillance Courtage, ni la société Groupe Vaillance Courtage n'ont été informées de ce qu'elles étaient en droit de se faire assister des personnes de leur choix lors du contrôle sur place, alors que, d'une part, ces dispositions ne sont entrées en vigueur qu'à compter du 3 novembre 2014, soit postérieurement au contrôle diligenté par l'Autorité, et que, d'autre part, elles ne s'appliquent, dans la phase de contrôle, que lors de l'audition de la personne contrôlée par le secrétaire général de l'Autorité, et non lors du déroulement des opérations d'enquête sur site ; qu'il suit de là que ces moyens tirés de la méconnaissance des droits de la défense lors de la réalisation du contrôle préalable à la procédure de sanction ne sont pas de nature à faire naître un doute sérieux quant à la légalité de la décision contestée  ;<br/>
<br/>
              5. Considérant que la société Vaillance Courtage soutient en outre que l'absence de prescription applicable aux poursuites disciplinaires engagées par l'Autorité de contrôle prudentiel et de résolution est contraire aux dispositions des articles 6 et 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que toutefois, l'absence de prescription applicable aux poursuites susceptibles d'être engagées par le collège de l'Autorité de contrôle prudentiel et de résolution n'est contraire à aucun principe à valeur constitutionnelle ni à aucun autre principe ; qu'il suit de là que ce moyen n'est pas non plus de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              Sur le bien fondé de la décision :<br/>
<br/>
              6. Considérant que la société Vaillance Courtage soutient, en premier lieu, que le principe de personnalité des peines s'oppose à ce que l'Autorité de contrôle prudentiel et de résolution prononce une sanction à son encontre pour les manquements au code des assurances commis par ses mandataires et par la société Groupe Vaillance Conseil ; que toutefois, il résulte notamment du contrat fixant les relations de la société Vaillance Courtage avec ses mandataires que les mandataires de la société Vaillance Courtage exercent leur activité selon les modalités qu'elle fixe, sous son contrôle et sans réelle autonomie ; qu'il résulte également de l'instruction que les sociétés Vaillance Courtage et Groupe Vaillance Conseil ont des relations très imbriquées qui entraînent une confusion du rôle de ces deux intermédiaires ; qu'il suit de là que le moyen tiré de ce que la commission de sanction a méconnu le principe de personnalité des peines en imputant à la société Vaillance Courtage les manquements au code des assurances commis par ses mandataires et la société Groupe Vaillance Conseil  n'est pas de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ; <br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes de l'article L. 132-27-1 du code des assurances : " I. - Avant la conclusion d'un contrat d'assurance individuel comportant des valeurs de rachat, d'un contrat de capitalisation, ou avant l'adhésion à un contrat mentionné à l'article  L. 132-5-3 ou à l'article L. 441-1, l'entreprise d'assurance ou de capitalisation précise les exigences et les besoins exprimés par le souscripteur ou l'adhérent ainsi que les raisons qui motivent le conseil fourni quant à un contrat déterminé. Ces précisions, qui reposent en particulier sur les éléments d'information communiqués par le souscripteur ou l'adhérent concernant sa situation financière et ses objectifs de souscription, sont adaptées à la complexité du contrat d'assurance ou de capitalisation proposé. / " ; qu'aux termes de l'article R. 132-5-1-1 du même code : " I.-Les précisions ainsi que, le cas échéant, la mise en garde prévues à l'article L. 132-27-1 sont communiquées au souscripteur par écrit, avec clarté et exactitude, sur support papier ou tout autre support durable à sa disposition et auquel il a facilement accès. II.-Lorsque le souscripteur le demande ou lorsqu'une couverture immédiate est nécessaire, les informations peuvent être fournies oralement. Dans ce cas, sitôt le contrat conclu, les informations sont communiquées au souscripteur sur support papier ou tout autre support durable à sa disposition et auquel il a facilement accès. / " ; <br/>
<br/>
              8. Considérant qu'il résulte de l'instruction et n'est pas contesté par la société Vaillance Courtage que diverses insuffisances dans les devoirs d'information et de conseil de l'intermédiaire ont été commises à l'occasion de la distribution de contrats d'assurance ; que si la société fait valoir que la mission de contrôle n'a pas recherché si une information verbale complémentaire n'avait pas été fournie aux souscripteurs en complément des documents remplis, ainsi que le permettent les dispositions du II de l'article R. 132-5-1 du code des assurances, il ne résulte pas de l'instruction que, ainsi que le prévoient également les dispositions de cet article, les informations données oralement aient ensuite été communiquées aux souscripteurs sur support papier ou tout autre support durable ; que l'intermédiaire en assurance ne peut s'acquitter de son obligation d'information uniquement de manière orale ; que la circonstance que plusieurs souscripteurs aient déclaré être satisfaits de l'information délivrée par la requérante est sans influence sur le manquement constaté ; qu'il suit de là que le moyen tiré de la méconnaissance des articles L. 132-27-1 et R. 132-5-1-1 du code des assurances n'est pas susceptible de faire naître un doute sérieux quant à la légalité de la décision litigieuse ; <br/>
<br/>
              9. Considérant, enfin, que contrairement à ce qui est soutenu, la décision contestée ne fait grief à la société Vaillance Courtage ni d'avoir proposé des souscriptions multiples ou successives à ses clients ni d'avoir proposé la souscription de contrats en unités de compte ; qu'il suit de là que les moyens tirés de ce que la commission des sanctions aurait considéré que de telles souscriptions avaient un impact défavorable pour les clients ou étaient illicites ne sont pas susceptibles de faire naître un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la demande de suspension de l'exécution de la décision du 20 juillet 2015 présentée par la société Vaillance Courtage ne peut qu'être rejetée ; <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Autorité de contrôle prudentiel et de résolution, qui n'est pas la partie perdante dans la présente instance, les sommes que demande la société requérante au titre des frais exposés et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre une somme de 3 000 euros à la charge de la société Vaillance Courtage, sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Vaillance Courtage est rejetée. <br/>
Article 2 : La société Vaillance Courtage versera la somme de 3 000 euros à l'Autorité de contrôle prudentiel et de résolution au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la société Vaillance Courtage et à l'Autorité de contrôle prudentiel et de résolution.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
