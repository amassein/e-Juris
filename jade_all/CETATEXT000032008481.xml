<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032008481</ID>
<ANCIEN_ID>JG_L_2016_02_000000380779</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/00/84/CETATEXT000032008481.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 10/02/2016, 380779, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380779</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:380779.20160210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 28 mai et 15 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...B..., veuveF..., M. C...F...et Mme D...F..., venant aux droits de M. E...F..., demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur leur demande du 24 janvier 2014 tendant à ce que soit pris le décret en Conseil d'Etat prévu par l'article L. 172-1 du code de la sécurité sociale dans sa rédaction issue de l'article 94 de la loi du 20 décembre 2010 de financement de la sécurité sociale pour 2011 ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre ce décret ;<br/>
<br/>
              3°) de condamner l'Etat à leur verser une indemnité de 4 566 euros, assortie des intérêts, en réparation des préjudices causés, d'une part, par le retard de l'Etat à prendre ce décret et, d'autre part, par l'illégalité de la décision implicite de rejet résultant du silence gardé sur leur demande ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le loi n° 2010-1594 du 20 décembre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de ConsortsF... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur les conclusions aux fins d'annulation pour excès de pouvoir et d'injonction :<br/>
<br/>
              1. Considérant qu'en vertu de l'article 21 de la Constitution, le Premier ministre " assure l'exécution des lois " et, sous réserve de la compétence conférée au Président de la République pour les décrets délibérés en Conseil des ministres par l'article 13 de la Constitution, " exerce le pouvoir réglementaire " ; que l'exercice du pouvoir réglementaire comporte non seulement le droit, mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect des engagements internationaux de la France y ferait obstacle ;<br/>
<br/>
              2. Considérant qu'aux termes des deux premiers alinéas de l'article L. 172-1 du code de la sécurité sociale, dans leur rédaction issue de l'article 94 de la loi du 20 décembre 2010 de financement de la sécurité sociale pour 2011 : " Il est institué une coordination entre régimes d'assurance invalidité pour les personnes ayant relevé successivement ou alternativement soit de régimes de salariés, soit d'un régime de salariés et d'un régime de non salariés, soit de plusieurs régimes de travailleurs non salariés. / Un décret en Conseil d'Etat fixe les conditions dans lesquelles sont ouverts et maintenus les droits à pension d'invalidité dans les régimes en cause, ainsi que les conditions dans lesquelles sont calculés ces droits, lorsque le montant de la pension servie par le régime représente une fraction annuelle des revenus moyens correspondant aux cotisations versées au cours des dix années civiles d'assurance les plus avantageuses " ;<br/>
<br/>
              3. Considérant que les dispositions de l'article 94 de la loi du 20 décembre 2010 de financement de la sécurité sociale pour 2011 ont eu pour effet d'étendre, en matière d'assurance invalidité, la coordination entre les régimes de sécurité sociale, jusqu'alors limitée aux conditions dans lesquelles sont ouverts et maintenus les droits à pension d'invalidité, aux conditions dans lesquelles sont calculés ces droits, lorsque le montant de la pension servie par le régime représente une fraction annuelle des revenus moyens correspondant aux cotisations versées au cours des dix années civiles d'assurance les plus avantageuses ; que, toutefois, les dispositions ainsi ajoutées au deuxième alinéa de l'article L. 172-1 du code de la sécurité sociale ne peuvent, en raison de leur généralité, recevoir application sans l'intervention du décret en Conseil d'Etat qu'elles prévoient ; que, dans ces conditions, les dispositions législatives mentionnées au point 2 ne laissent pas la décision de prendre ce décret à la libre appréciation du Premier ministre ; qu'en dépit des difficultés rencontrées par l'administration dans l'élaboration de ce texte, le délai raisonnable au terme duquel le décret aurait dû être adopté était dépassé à la date à laquelle le Premier ministre a rejeté la demande présentée par les requérants à cette fin ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen de la requête, que la décision implicite par laquelle le Premier ministre a refusé de prendre le décret en Conseil d'Etat prévu par l'article L. 172-1 du code de la sécurité sociale, dans sa rédaction issue de l'article 94 de la loi précitée du 20 décembre 2010, doit être annulée ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public (...) prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ;<br/>
<br/>
              6. Considérant que l'annulation de la décision implicite par laquelle le Premier ministre a refusé de prendre le décret en Conseil d'Etat prévu par l'article L. 172-1 du code de la sécurité sociale, dans sa rédaction issue de l'article 94 de la loi précitée du 20 décembre 2010, implique nécessairement que ce décret soit pris ; qu'il y a lieu pour le Conseil d'Etat d'ordonner au Premier ministre de prendre ce décret dans un délai de six mois ;<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              7. Considérant que l'abstention de l'Etat à prendre dans un délai raisonnable le décret en Conseil d'Etat auquel était subordonnée, en matière d'assurance invalidité, l'extension de la coordination entre les régimes de sécurité sociale aux conditions dans lesquelles sont calculés les droits à pension d'invalidité est constitutive d'une faute de nature à engager sa responsabilité ;<br/>
<br/>
              8. Considérant qu'eu égard à la nature des mesures à prendre, le délai raisonnable dont le Gouvernement disposait pour prendre ce décret était dépassé lorsque le Régime social des indépendants Provence-Alpes a notifié à M. E...F..., aux droits duquel viennent les requérants, une pension d'incapacité au métier à compter du 1er juin 2013, sur la base des cotisations versées à ce seul régime au cours des dix années civiles d'assurance les plus avantageuses ; que l'absence d'intervention du décret à cette date a entrainé un préjudice direct et certain pour M. E...F..., dont la pension d'incapacité versée du 1er juin au 21 novembre 2013, date de son décès, s'est trouvée minorée par l'absence de prise en compte des cotisations versées au régime général, alors qu'il résulte de l'instruction qu'il a relevé de ce régime durant la majeure partie de sa carrière, notamment durant les années au cours desquelles ses revenus ont été les plus importants ; que les consorts F...sont fondés à demander l'indemnisation de ce préjudice ;<br/>
<br/>
              9. Considérant que le législateur a entendu prescrire la prise en compte, pour le calcul des droits à pension d'invalidité, de tout ou partie des cotisations versées aux régimes autres que celui servant la pension, lorsque le montant de cette pension représente une fraction annuelle des revenus moyens correspondant aux cotisations versées au cours des dix années civiles d'assurance les plus avantageuses ; qu'il résulte de l'instruction que, compte tenu des éléments fournis par les requérants, notamment du relevé de carrière de M. E...F...établi par la caisse d'assurance retraite et de la santé au travail Sud-Est, il sera fait une juste appréciation du préjudice en condamnant l'Etat à leur verser une somme de 4 500 euros ;<br/>
<br/>
              10. Considérant que les requérants ont droit aux intérêts de la somme de 4 500 euros qui leur est due à compter du 28 mai 2014, date de saisine du juge administratif ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser aux consorts F...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le Premier ministre a refusé de prendre le décret en Conseil d'Etat prévu par l'article L. 172-1 du code de la sécurité sociale, dans sa rédaction issue de l'article 94 de la loi du 20 décembre 2010 de financement de la sécurité sociale pour 2011, est annulée.<br/>
Article 2 : Il est enjoint au Premier ministre de prendre le décret en Conseil d'Etat prévu par l'article L. 172-1 du code de la sécurité sociale, dans sa rédaction issue de l'article 94 de la loi du 20 décembre 2010 de financement de la sécurité sociale pour 2011, dans un délai de six mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat est condamné à verser aux consorts F...une indemnité d'un montant de 4 500 euros assortie des intérêts au taux légal à compter du 28 mai 2014.<br/>
Article 4 : L'Etat versera aux consorts F...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de la requête des consorts F...est rejeté. <br/>
Article 6 : La présente décision sera notifiée à Mme A...B..., veuveF..., à M. C...F..., à Mme D...F..., au Premier ministre et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
Copie en sera adressée au Régime social des indépendants Provence-Alpes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
