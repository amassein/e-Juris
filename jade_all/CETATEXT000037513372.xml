<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037513372</ID>
<ANCIEN_ID>JG_L_2018_10_000000415941</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/51/33/CETATEXT000037513372.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 22/10/2018, 415941</TITRE>
<DATE_DEC>2018-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415941</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:415941.20181022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1709686 du 22 novembre 2017, enregistrée le 23 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Montreuil a transmis, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 31 octobre 2017 au greffe de ce tribunal, présentée  par la Fédération des syndicats des travailleurs du rail - SUD Rail.<br/>
<br/>
              Par cette requête et par un mémoire en réplique, enregistré le 4 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération des syndicats des travailleurs du rail - SUD Rail demande au Conseil d'Etat, dans le dernier état de ses écritures :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 21 septembre 2017 par laquelle le directeur de la cohésion et des ressources humaines ferroviaires modifie le référentiel des ressources humaines RH0924 relatif à la mise en oeuvre des dispositions de la loi du 21 août 2007 sur le dialogue social et la continuité du service public dans les transports terrestres réguliers de voyageurs ; <br/>
<br/>
              2°) de mettre à la charge de la SNCF, de SNCF Mobilités et de SNCF Réseau la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des transports ;<br/>
              - loi n° 2007-1224 du 21 août 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Fédération des syndicats des travailleurs du Rail - SUD Rail et à la SCP Spinosi, Sureau, avocat de la SNCF Mobilités, de la SNCF Réseau et de la SNCF.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 1222-2 du code des transports : " Après consultation des usagers lorsqu'existe une structure les représentant, l'autorité organisatrice de transport définit les dessertes prioritaires en cas de perturbation prévisible du trafic. (...) ". Aux termes de l'article L. 1222-3 de ce code : " Pour assurer les dessertes prioritaires, l'autorité organisatrice de transports détermine différents niveaux de service en fonction de l'importance de la perturbation. (...) ". Aux termes de l'article L. 1222-4 du même code : " L'entreprise de transports élabore : / 1° Un plan de transports adapté aux priorités de desserte et aux niveaux de service définis par l'autorité organisatrice de transports, qui précise, pour chaque niveau de service, les plages horaires et les fréquences à assurer (...) ". Aux termes de l'article L. 1222-7 de ce code : " Dans les entreprises de transports, l'employeur et les organisations syndicales représentatives concluent un accord collectif de prévisibilité du service applicable en cas de perturbation prévisible du trafic. / L'accord collectif de prévisibilité du service recense, par métier, fonction et niveau de compétence ou de qualification, les catégories d'agents et leurs effectifs, ainsi que les moyens matériels, indispensables à l'exécution, conformément aux règles de sécurité en vigueur applicables à l'entreprise, de chacun des niveaux de service prévus dans le plan de transports adapté. / Il fixe les conditions dans lesquelles, en cas de perturbation prévisible, l'organisation du travail est révisée et les personnels disponibles réaffectés afin de permettre la mise en oeuvre du plan de transports adapté. En cas de grève, les personnels disponibles sont les personnels de l'entreprise non grévistes. / A défaut d'accord applicable, un plan de prévisibilité est défini par l'employeur. Un accord collectif de prévisibilité du service qui entre en vigueur s'applique en lieu et place du plan de prévisibilité. (...) ". Aux termes de l'article L. 1324-7 du même code : " En cas de grève, les salariés relevant des catégories d'agents mentionnées dans l'accord collectif ou le plan de prévisibilité prévus à l'article L. 1222-7 informent, au plus tard quarante-huit heures avant de participer à la grève, le chef d'entreprise ou la personne désignée par lui de leur intention d'y participer. (...) ". Enfin, l'article L. 1324-8 de ce code prévoit qu' " Est passible d'une sanction disciplinaire le salarié qui n'a pas informé son employeur de son intention de participer à la grève dans les conditions prévues à l'article L. 1324-7. Cette sanction disciplinaire peut également être prise à l'encontre du salarié qui, de façon répétée, n'a pas informé son employeur de son intention de renoncer à participer à la grève ou de reprendre son service ".<br/>
<br/>
              2. La Fédération des syndicats des travailleurs du rail - SUD Rail demande l'annulation pour excès de pouvoir de la décision du 21 septembre 2017 par laquelle le directeur de la cohésion et des ressources humaines ferroviaires de la SNCF a modifié, en l'étendant de trois à quatorze catégories d'agents, la liste des salariés qui sont tenus de faire connaître à leur employeur, la SNCF, SNCF Réseau ou SNCF Mobilités, leur intention de participer à une grève, telle qu'elle résultait du plan de prévisibilité prévu à l'article L. 1222-7 du code des transports, initialement défini par l'employeur le 30 mai 2008. <br/>
<br/>
              3. Il résulte des dispositions de l'article L. 1222-7 du code des transports qu'un plan de prévisibilité du service applicable en cas de perturbation prévisible du trafic ne peut être élaboré par l'employeur qu'à défaut de conclusion d'un accord avec les organisations syndicales représentatives. Par suite, l'employeur ne saurait, contrairement à ce que soutiennent la SNCF, SNCF Réseau et SNCF Mobilités, modifier significativement le plan de prévisibilité élaboré par lui en l'absence d'accord collectif de prévisibilité du service qu'après avoir, de nouveau, engagé avec les organisations syndicales représentatives des négociations en vue de la signature d'un accord, sans qu'elles aient pu aboutir à la conclusion d'un tel accord. Il suit de là que la circonstance que les négociations, entre l'employeur et les organisations syndicales représentatives, prévues par l'article 5 de la loi du 21 août 2007 sur le dialogue social et la continuité du service public dans les transports terrestres réguliers de voyageurs, en vue de la signature, avant le 1er janvier 2008, d'un accord collectif de prévisibilité du service applicable en cas de perturbation prévisible du trafic ou de grève n'avaient pas permis de parvenir à la conclusion d'un tel accord et qu'ainsi le plan de prévisibilité désormais prévu à l'article L. 1222-7 du code des transports avait été initialement défini par l'employeur, le 30 mai 2008, ne dispensait pas celui-ci d'engager des négociations en vue de la conclusion d'un accord collectif avant de modifier significativement le plan de prévisibilité existant par voie de décision unilatérale.  <br/>
<br/>
              4. Il ressort des pièces du dossier que l'employeur a invité les organisations syndicales, y compris la requérante, à des rencontres bilatérales, organisées à la fin du mois d'août 2017, portant sur l'extension à de nouveaux agents de l'obligation de procéder à une déclaration individuelle d'intention. A cette occasion, il leur a remis un document intitulé " Extension des catégories d'agents soumis à déclaration individuelle d'intention ", qui faisait état de ce que les trois catégories mentionnées par le plan de prévisibilité établi par la décision du 30 mai 2008 ne permettaient pas au groupe public ferroviaire de respecter pleinement son obligation légale de garantir un service minimum et qui mentionnait les évolutions envisagées. Ces rencontres bilatérales, qui n'avaient d'autre objet que d'informer les organisations syndicales de la modification à laquelle il serait unilatéralement procédé, et qui a porté de trois à quatorze catégories d'agents la liste des salariés tenus de faire connaître à leur employeur leur intention de participer à une grève, ne sauraient être regardées comme ayant pu tenir lieu de la négociation préalable imposée par les dispositions de l'article L. 1222-7 du code des transports avant toute modification significative du plan de prévisibilité. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens soulevés par la requérante, que la Fédération des syndicats des travailleurs du rail - SUD Rail est fondée à demander l'annulation de la décision du 21 septembre 2017 qu'elle attaque. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la fédération des syndicats des travailleurs du rail - SUD Rail, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de chacun des établissements publics, SNCF, SNCF Mobilités et SNCF Réseau, au titre des mêmes dispositions, le versement d'une somme de 1 000 euros au profit de la Fédération des syndicats des travailleurs du rail - SUD Rail au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                     --------------<br/>
<br/>
              Article 1er : La décision du 21 septembre 2017 du directeur de la cohésion et des ressources humaines ferroviaires modifiant le référentiel des ressources humaines RH0924 relatif à la mise en oeuvre des dispositions de la loi du 21 août 2007 sur le dialogue social et la continuité du service public dans les transports terrestres réguliers de voyageurs est annulée.<br/>
<br/>
              Article 2 : La SNCF, SNCF Mobilités et SNCF Réseau verseront chacune à la Fédération des syndicats des travailleurs du rail - SUD Rail une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Article 3 : Les conclusions de la SNCF, de SNCF Mobilités et de SNCF Réseau présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
              Article 4 : La présente décision sera notifiée à la Fédération des syndicats des travailleurs du rail - SUD Rail et, pour l'ensemble des défendeurs, à la SNCF, première dénommée. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">65-01-02-05-01 TRANSPORTS. TRANSPORTS FERROVIAIRES. OPÉRATEURS DE TRANSPORTS FERROVIAIRES. - PLAN DE PRÉVISIBILITÉ DU SERVICE APPLICABLE EN CAS DE PERTURBATION PRÉVISIBLE DU TRAFIC OU DE GRÈVE (ART. L. 1222-7 DU CODE DES TRANSPORTS) - PLAN ÉLABORÉ UNILATÉRALEMENT PAR L'EMPLOYEUR - 1) PRINCIPES - POSSIBILITÉ SUBORDONNÉE AU DÉFAUT DE CONCLUSION D'UN ACCORD AVEC LES ORGANISATIONS SYNDICALES REPRÉSENTATIVES (OSR) - CONSÉQUENCE - MODIFICATION UNILATÉRALE SIGNIFICATIVE DU PLAN PAR L'EMPLOYEUR - POSSIBILITÉ SUBORDONNÉE À L'ENGAGEMENT DE NÉGOCIATIONS ET AU DÉFAUT DE CONCLUSION D'UN ACCORD AVEC LES OSR PORTANT SUR CETTE MODIFICATION - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 65-01-02-05-01 1) Il résulte de l'article L. 1222-7 du code des transports qu'un plan de prévisibilité du service applicable en cas de perturbation prévisible du trafic ne peut être élaboré par l'employeur qu'à défaut de conclusion d'un accord avec les organisations syndicales représentatives (OSR). Par suite, l'employeur ne saurait modifier significativement le plan de prévisibilité élaboré par lui en l'absence d'accord collectif de prévisibilité du service qu'après avoir, de nouveau, engagé avec les OSR des négociations en vue de la signature d'un accord, sans qu'elles aient pu aboutir à la conclusion d'un tel accord.,,Il suit de là que la circonstance que les négociations, entre l'employeur et les OSR, prévues par l'article 5 de la loi du 21 août 2007 sur le dialogue social et la continuité du service public dans les transports terrestres réguliers de voyageurs, en vue de la signature, avant le 1er janvier 2008, d'un accord collectif de prévisibilité du service applicable en cas de perturbation prévisible du trafic ou de grève n'avaient pas permis de parvenir à la conclusion d'un tel accord et qu'ainsi le plan de prévisibilité désormais prévu à l'article L. 1222-7 du code des transports avait été initialement défini par l'employeur, le 30 mai 2008, ne dispensait pas celui-ci d'engager des négociations en vue de la conclusion d'un accord collectif avant de modifier significativement le plan de prévisibilité existant par voie de décision unilatérale....  ,,2) Employeur ayant invité les organisations syndicales à des rencontres bilatérales portant sur l'extension à de nouveaux agents de l'obligation de procéder à une déclaration individuelle d'intention et leur ayant remis à cette occasion un document intitulé Extension des catégories d'agents soumis à déclaration individuelle d'intention, qui faisait état de ce que les trois catégories mentionnées par le plan de prévisibilité établi par la décision du 30 mai 2008 ne permettaient pas au groupe public ferroviaire de respecter pleinement son obligation légale de garantir un service minimum et qui mentionnait les évolutions envisagées.... ...Ces rencontres bilatérales, qui n'avaient d'autre objet que d'informer les organisations syndicales de la modification à laquelle il serait unilatéralement procédé, et qui a porté de trois à quatorze catégories d'agents la liste des salariés tenus de faire connaître à leur employeur leur intention de participer à une grève, ne sauraient être regardées comme ayant pu tenir lieu de la négociation préalable imposée par les dispositions de l'article L. 1222-7 du code des transports avant toute modification significative du plan de prévisibilité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
