<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043890812</ID>
<ANCIEN_ID>JG_L_2021_07_000000441926</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/89/08/CETATEXT000043890812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/07/2021, 441926, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441926</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441926.20210730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :	<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Nîmes, d'une part, d'annuler pour excès de pouvoir les décisions du 24 octobre  2016 par lesquelles le recteur de l'académie de Corse a refusé de reconnaître l'imputabilité au service des congés maladie accordés à la suite d'incidents survenus les 7 décembre 2012 et 19 janvier 2015 et d'enjoindre à l'administration de reconnaître cette imputabilité au service, d'autre part, de condamner l'Etat à lui verser la somme de 15 000 euros en réparation du préjudice qu'elle estime avoir subi. Par un jugement n° 1700049 du 22 octobre 2018, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 19MA00025 du 16 mars 2020, la cour administrative d'appel de Marseille, sur appel de Mme B... a, d'une part, annulé partiellement le jugement du tribunal administratif de Nîmes et la décision du recteur de l'académie de Corse du 24 octobre 2016 en tant qu'elle refuse de reconnaître l'imputabilité au service de l'incident survenu le 7 décembre 2012, et enjoint au recteur de reconstituer la carrière et les droits à rémunération et congés de Mme B... dans la mesure correspondante, d'autre part, condamné l'Etat à verser à Mme B... une indemnité de 1 500 euros en réparation de son préjudice moral, et rejeté le surplus des conclusions de Mme B.... <br/>
<br/>
              Par un pourvoi, enregistré le 17 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale, de la jeunesse et des sports demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a fait droit aux conclusions de Mme B... ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions de Mme B.... <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... D..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rousseau et Tapie, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., professeure des écoles affectée au réseau d'aide spécialisée aux élèves en difficulté de Porto-Vecchio, a sollicité la reconnaissance de l'imputabilité au service de la maladie consécutive à des incidents survenus le 7 décembre 2012 et le 19 janvier 2015. Par une décision du 24 octobre 2016, le recteur de l'académie de Corse a refusé de faire droit à cette demande. Par un jugement du 22 octobre 2018, le tribunal administratif de Nîmes a rejeté la demande de Mme B... tendant à l'annulation de ces décisions et à ce que l'Etat soit condamné à lui verser la somme de 15 000 euros en réparation du préjudice qu'elle estime avoir subi. Par un arrêt du 16 mars 2020, la cour administrative d'appel de Marseille a, d'une part, annulé le jugement du tribunal administratif de Nîmes et la décision du recteur de l'académie de Corse du 24 octobre 2016 en tant qu'elle refuse de reconnaître l'imputabilité au service de l'incident survenu le 7 décembre 2012 et enjoint au recteur de l'académie de Corse de reconstituer la carrière et les droits à rémunération et congés de Mme B... dans la mesure correspondante, d'autre part condamné l'Etat à verser à Mme B... une indemnité de 1 500 euros en réparation de son préjudice moral, et rejeté le surplus des conclusions de Mme B.... Le ministre de l'éducation nationale, de la jeunesse et des sports se pourvoit en cassation contre cet arrêt en tant qu'il a partiellement fait droit aux conclusions de Mme B.... <br/>
<br/>
              2. Aux termes de l'article 34 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction alors applicable : " Le fonctionnaire en activité a droit : / (...) / 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. Le fonctionnaire conserve, en outre, ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence. / Toutefois, si la maladie provient (...) d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident (...) ".<br/>
<br/>
              3. Une maladie contractée par un fonctionnaire, ou son aggravation, doit être regardée comme imputable au service si elle présente un lien direct avec l'exercice des fonctions ou avec des conditions de travail de nature à susciter le développement de la maladie en cause, sauf à ce qu'un fait personnel de l'agent ou toute autre circonstance particulière conduisent à détacher la survenance ou l'aggravation de la maladie du service.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond et des énonciations mêmes de l'arrêt attaqué que Mme B... bénéficiait d'un logement de service dans un bâtiment voisin de l'école où elle était affectée et qu'un conflit de voisinage particulièrement aigu l'opposait depuis plusieurs années à des personnes occupant également des logements de service dans le même immeuble. En jugeant que l'incident verbal survenu le 7 décembre 2012 entre Mme B... et l'une de ces personnes devait être regardé comme un accident de service pour l'application des dispositions citées au point 2 aux motifs qu'il était soudain, qu'il avait eu lieu pendant l'exécution du service et que Mme B... avait été immédiatement placée en congé de maladie après sa survenance, alors que le contexte avéré de conflit de voisinage dans lequel il s'inscrivait constituait une circonstance particulière détachant sa survenance de l'exécution du service, la cour administrative d'appel de Marseille a commis une erreur de droit et inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que le ministre de l'éducation nationale, de la jeunesse et des sports est fondé à demander l'annulation des articles 1er à 5 de l'arrêt qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er à 5 de l'arrêt du 16 mars 2020 de la cour administrative d'appel de Marseille sont annulés.<br/>
Article 2 :  L'affaire est renvoyée, dans la mesure de l'annulation prononcée à l'article 1er, à la cour administrative d'appel de Marseille. <br/>
Article 3 : Les conclusions de Mme B... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au ministre de l'éducation nationale, de la jeunesse et des sports et à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
