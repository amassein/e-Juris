<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032491623</ID>
<ANCIEN_ID>JG_L_2016_05_000000394869</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/16/CETATEXT000032491623.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 04/05/2016, 394869, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394869</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394869.20160504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Lille d'ordonner la suspension de l'exécution de la décision du 31 août 2015 de la commission de discipline du baccalauréat de Lille lui infligeant la sanction du blâme assorti d'une inscription au livret scolaire, sanction entraînant la nullité des épreuves anticipées écrite et orale de français qu'il repassait au cours de la session 2015, jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision. Par une ordonnance n° 1509150 du 12 novembre 2015 le juge des référés a rejeté cette demande.  <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 27 novembre 2015 et le 3 février 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 31 août 2015, la commission de discipline du baccalauréat de l'académie de Lille a infligé à M.B..., du fait des fraudes qu'il aurait commises lors des épreuves écrites de français qu'il repassait au cours de la session 2015, en même temps qu'il passait les épreuves du baccalauréat, la sanction du blâme assorti d'une inscription au livret scolaire, entraînant la nullité des épreuves de français et, par voie de conséquence, la non délivrance du diplôme du baccalauréat ; que M. B...se pourvoit en cassation contre l'ordonnance du 12 novembre 2015 par laquelle le juge des référés du tribunal administratif de Lille a refusé de faire droit à sa demande de suspension de l'exécution de cette décision jusqu'à ce qu'il soit statué au fond sur sa légalité ; <br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais " ; qu'aux termes de l'article L. 911-1 de ce code : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; <br/>
<br/>
              3. Considérant que, dans le cas où les conditions posées par l'article L. 521-1 du code de justice administrative sont remplies, le juge des référés peut non seulement suspendre l'exécution d'une décision administrative, même de rejet, mais aussi assortir cette suspension d'une injonction, s'il est saisi de conclusions en ce sens, ou de l'indication des obligations qui en découleront pour l'administration ; que, toutefois, les mesures qu'il prescrit ainsi, alors qu'il se borne à relever l'existence d'un doute sérieux quant à la légalité de la décision en litige, doivent présenter un caractère provisoire ; qu'il suit de là que le juge des référés, saisi sur le fondement de l'article L. 521-1 du code de justice administrative, ne peut, sans excéder sa compétence, ordonner une mesure qui aurait des effets en tous points identiques à ceux qui résulteraient de l'exécution par l'autorité administrative d'un jugement annulant la décision administrative contestée ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la demande de suspension de la délibération du 31 août 2015 sanctionnant M. B...n'impliquait pas nécessairement que lui soit délivré le diplôme du baccalauréat à titre définitif, le juge des référés pouvant enjoindre que soit délivré à l'intéressé un relevé de notes à titre provisoire ; que, par suite, le juge des référés du tribunal administratif de Lille, en jugeant que la mesure sollicitée aurait eu les mêmes effets qu'une décision exécutant un jugement d'annulation de la délibération litigieuse, a commis une erreur de droit ; que dès lors, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son ordonnance doit être annulée ;<br/>
<br/>
              Sur la demande de suspension présentée par M. B...devant le juge des référés du tribunal administratif de Lille :<br/>
<br/>
              5. Considérant que pour justifier l'urgence d'une suspension de l'exécution de la délibération du 31 août 2015, M. B...se bornait à faire valoir, devant le juge des référés du tribunal administratif de Lille, que cette délibération le privait de la possibilité de s'inscrire dans une université et d'y suivre les cours dans l'attente de la décision statuant sur la légalité de la délibération litigieuse ; que toutefois, ainsi que le soutient, sans être contesté, le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, les inscriptions à l'université sont définitivement closes pour l'année 2015 ; que dès lors, les circonstances ainsi invoquées ne sont pas de nature à justifier de l'urgence qui s'attacherait à la suspension des effets de la délibération du 31 août 2015 ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander la suspension de la délibération du 31 août 2015 ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 12 novembre 2015 du juge des référés du tribunal administratif de Lille est annulée.<br/>
<br/>
Article 2 : La demande de suspension présentée par M. B...devant le juge des référés du tribunal administratif de Lille est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
