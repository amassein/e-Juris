<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861426</ID>
<ANCIEN_ID>JG_L_2016_01_000000378321</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/14/CETATEXT000031861426.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 07/01/2016, 378321, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378321</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:378321.20160107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 22 avril 2014 et 3 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 17 février 2014 du Président de la République prononçant son déplacement d'office par mesure disciplinaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure pénale ;<br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              - le décret n° 84-961 du 25 octobre 1984 ;<br/>
<br/>
              - le décret n° 95-654 du 9 mai 1995 ;<br/>
<br/>
- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que M. A..., chef de la circonscription de sécurité publique du 4ème arrondissement de Paris, a comparu le 16 janvier 2013 devant la 14ème chambre correctionnelle du tribunal de grande instance de Paris à raison d'un accident survenu alors qu'il conduisait un véhicule de fonction ; qu'ayant relevé au cours de l'audience publique la présence d'un individu prenant des notes, lequel a alors indiqué à l'avocat de M. A... n'être pas journaliste mais s'est abstenu de communiquer toute autre information le concernant, M. A... a, au sortir de l'audience, fait procéder sur la voie publique au contrôle d'identité dudit individu, lequel s'est révélé être un commandant de l'inspection générale des services mandaté par sa hiérarchie pour suivre la procédure judiciaire concernant M. A... ; que, considération étant notamment prise des responsabilités et prérogatives s'attachant au corps supérieur de conception et de direction de la police nationale auquel appartient M. A..., de l'absence en l'espèce de circonstance particulière caractérisant un risque sérieux d'atteinte à l'ordre public ou de considération d'une infraction et du caractère abusif de la mesure ainsi diligentée, le Président de la République a prononcé à l'encontre de M. A... une sanction de déplacement d'office par un décret du 17 février 2014 contre lequel l'intéressé forme un recours pour excès de pouvoir ;<br/>
<br/>
              2. Considérant que si M. A... allègue que l'administration aurait envisagé, antérieurement à la réunion du conseil de discipline, de prononcer sa mutation dans l'intérêt du service sur le fondement des dispositions de l'article 25 du décret du 9 mai 1995, cette circonstance, à la supposer établie, est en tout état de cause sans incidence sur la régularité de la procédure disciplinaire qui seule a été suivie ; qu'il en va de même du fait que des organes de presse aient pu, avant la réunion le 29 novembre 2013 de ce conseil de discipline, évoquer la possibilité d'une sanction à l'encontre du requérant ;<br/>
<br/>
              3. Considérant que la procédure au terme de laquelle l'autorité administrative compétente exerce son pouvoir disciplinaire n'entre pas dans le champ d'application de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'ainsi, le moyen tiré de la méconnaissance des stipulations de cet article est inopérant ;<br/>
<br/>
              4. Considérant que si M. A... soutient que l'administration aurait de longue date agi à son encontre en préméditant l'infliction d'une sanction, l'intéressé n'invoque aucune circonstance de nature à révéler par elle-même un manquement au principe d'impartialité ou un détournement de la procédure disciplinaire ;<br/>
<br/>
              5. Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; <br/>
<br/>
              6. Considérant, d'une part, que, dès lors notamment que l'autorité concernée doit justifier dans tous les cas des circonstances particulières motivant la réalisation d'un contrôle d'identité en application de l'article 78-2 du code de procédure pénale, et qu'un tel contrôle ne saurait être diligenté au bénéfice des seuls intérêts personnels d'un fonctionnaire de police, l'autorité investie du pouvoir disciplinaire n'a pas inexactement qualifié ces faits en estimant qu'ils revêtaient un caractère abusif et étaient de nature à justifier une sanction ;<br/>
<br/>
              7. Considérant, d'autre part, que, compte tenu de la gravité des faits reprochés à M. A... consistant à ordonner un contrôle d'un tiers au seul motif qu'il prenait des notes lors d'une audience du tribunal correctionnel le concernant et à la méconnaissance qu'ils traduisent, de sa part, des responsabilités qui étaient les siennes, l'autorité disciplinaire n'a pas, en l'espèce, pris une sanction disproportionnée en infligeant à l'intéressé la sanction du déplacement d'office ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation du décret attaqué ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
