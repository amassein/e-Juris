<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375672</ID>
<ANCIEN_ID>JG_L_2020_09_000000440954</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375672.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/09/2020, 440954, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440954</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440954.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 6 avril 2020, M. A... B... a demandé au tribunal administratif de Melun, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa demande tendant à la réduction de la cotisation de contribution sociale généralisée à laquelle il a été assujetti au titre de l'année 2017 à raison de plus-values de cession de droits sociaux réalisées en 2017, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 3° du V.-A de l'article 8 de la loi n° 2017-1836 du 30 décembre 2017 de financement de la sécurité sociale pour 2018. <br/>
<br/>
              Par une ordonnance n° 1905634 du 28 mai 2020, enregistrée le 2 juin 2020 au secrétariat du contentieux du Conseil d'Etat, le président de la 3ème chambre du tribunal administratif de Melun a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre cette question au Conseil d'Etat. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ; <br/>
              - le code de la sécurité sociale ; <br/>
              - la loi n° 2017-1836 du 30 décembre 2017 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. B... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 septembre 2020, présentée par M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              2. Aux termes de l'article 8 de la loi du 30 décembre 2017 de financement de la sécurité sociale pour 2018 : " I. - Le code de la sécurité sociale est ainsi modifié : (...) / 6° L'article L. 136-8 est ainsi modifié : (...) / b) Au 2° du même I, le taux : " 8,2 % " est remplacé par le taux " 9,9 % " ; (...) / V.-A. - Les I et II du présent article s'appliquent : (...) / 3° A compter de l'imposition des revenus de l'année 2017, en ce qu'ils concernent la contribution mentionnée à l'article L. 136-6 du code de la sécurité sociale, sous réserve du II de l'article 34 de la loi n° 2016-1918 du 29 décembre 2016 de finances rectificative pour 2016 ; / (...) ". Aux termes de l'article L. 136-6 du code de la sécurité sociale : " I. Les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du code général des impôts sont assujetties à une contribution sur les revenus du patrimoine assise sur le montant net retenu pour l'établissement de l'impôt sur le revenu, à l'exception de ceux ayant déjà supporté la contribution au titre des articles L. 136-3, L. 136-4 et L. 136-7 : / (...) e. Des plus-values, gains en capital et profits soumis à l'impôt sur le revenu (...) ".<br/>
<br/>
              3. Aux termes de l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 : " Toute société dans laquelle la garantie des droits n'est pas assurée, ni la séparation des pouvoirs déterminée, n'a point de Constitution ". Il est à tout moment loisible au législateur, statuant dans le domaine de sa compétence, de modifier des textes antérieurs ou de les abroger en leur substituant, le cas échéant, d'autres dispositions. Ce faisant, il ne saurait toutefois priver de garanties légales des exigences constitutionnelles. En particulier, il ne saurait, sans motif d'intérêt général suffisant, ni porter atteinte aux situations légalement acquises ni remettre en cause les effets qui pouvaient légitimement être attendus de situations nées sous l'empire de textes antérieurs. <br/>
<br/>
              4. A l'appui de sa demande tendant à la réduction de la contribution sociale généralisée à laquelle il a été assujetti à raison de plus-values provenant de la cession de droits sociaux réalisées en 2017, M. B... soutient que les dispositions du 3° du V-A de l'article 8 de la loi du 30 décembre 2017 portent atteinte aux exigences de l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 dès lors qu'elles rendent applicable à des cessions réalisées au cours de l'année 2017, antérieurement à leur entrée en vigueur, la hausse de contribution sociale généralisée de 1,7 point prévue par le b du 6° du I de l'article 8 de cette loi.<br/>
<br/>
              5. Les dispositions contestées, qui sont applicables à la contribution sociale généralisée due en 2018 au titre de l'année 2017, en modifient le taux. Aucune règle constitutionnelle n'en imposait le maintien et le requérant ne pouvait légitimement s'attendre à ce que lui soit appliqué le taux en vigueur à la date de la cession, alors que si le transfert de propriété constitue le fait générateur de la plus-value, le fait générateur de l'imposition de la plus-value intervient le 31 décembre de l'année de réalisation du revenu. Par suite, le moyen tiré de ce que les dispositions contestées seraient contraires à la garantie des droits résultant de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 est dépourvu de caractère sérieux.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B....<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'économie, des finances et de la relance. <br/>
Copie en sera adressée au Premier ministre, au tribunal administratif de Melun et au Conseil constitutionnel. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
