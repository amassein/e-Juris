<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036685881</ID>
<ANCIEN_ID>JG_L_2018_03_000000418125</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/58/CETATEXT000036685881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 06/03/2018, 418125, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418125</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:418125.20180306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 13 et 26 février 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la délibération de la Commission de régulation de l'énergie (CRE) n° 2018-005 du 11 janvier 2018 portant décision sur le maintien de la certification de la société Réseau de Transport d'Electricité (RTE) en tant qu'en son point 10, elle demande à la société Electricité de France (EDF) " de proposer à l'assemblée générale des actionnaires de RTE, dans les meilleurs délais et au plus tard dans les deux mois suivant la publication de la présente délibération au Journal officiel de la République française, la nomination d'un autre représentant d'EDF au sein du conseil de surveillance de RTE en remplacement de M. A...B... " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              - sa requête est recevable ;<br/>
              - la condition d'urgence est remplie dès lors, d'une part, que l'exécution de la délibération mettrait fin à des fonctions rémunérées sans possibilité de prise en charge au titre de l'assurance chômage, et d'autre part, qu'aucun motif d'intérêt général tenant à une violation des règles d'indépendance ne justifie l'urgence à ne pas prononcer la suspension demandée ;<br/>
              - il existe un doute sérieux quant à la légalité de la délibération contestée, dès lors, en premier lieu, que la CRE ne dispose pas, au titre de l'article L. 111-3 du code de l'énergie, du pouvoir de prononcer des injonctions à l'encontre d'EDF, en deuxième lieu, que sa nomination en tant que représentant de cette société au sein du conseil de surveillance de RTE, et non plus de la minorité, est intervenue non pas " après la cessation " de son mandat, comme le prévoit l'article L. 111-27 du même code, mais au cours de ce mandat, en troisième lieu, qu'en tout état de cause, cette nomination ne caractérise pas l'exercice d'activités ou la détention de responsabilités professionnelles dans une société composant l'entreprise verticalement intégrée à laquelle appartient RTE autre que cette dernière, et, en quatrième lieu, que la CRE ne peut s'opposer, en vertu de l'article L. 111-25 du même code, qu'aux nominations au sein du conseil de surveillance au titre de la minorité.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 21 février 2018, la CRE conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par M. B...ne sont pas propres à créer un doute sérieux quant à la légalité de la délibération contestée.<br/>
              Par des observations, enregistrées le 26 février 2018, la société RTE s'en remet à la sagesse du Conseil d'Etat.<br/>
<br/>
              La requête a été transmise au ministre d'Etat, ministre de la transition écologique et solidaire, au ministre de l'économie et des finances ainsi qu'à la société EDF qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B...et, d'autre part, le président de la Commission de régulation de l'énergie ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du mardi 27 février 2018 à 10 heures au cours de laquelle ont été entendus :<br/>
              - le représentant de M.B... ;<br/>
<br/>
              - les représentants de la Commission de régulation de l'énergie  ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au vendredi 2 mars 2018 à 16 heures ;<br/>
<br/>
              Vu le mémoire, enregistré le 2 mars 2018 avant la clôture de l'instruction, présenté pour M. B... qui reprend les conclusions et moyens de sa requête ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 mars 2018 après la clôture de l'instruction, présentée par la Commission de régulation de l'énergie ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Il ressort des pièces du dossier que, par courrier du 15 mars 2017, RTE, qui est une société anonyme à directoire et conseil de surveillance, a, d'une part, informé la CRE de la finalisation de l'opération au terme de laquelle elle serait détenue à 100 % non plus par EDF mais par la société holding Coentreprise de Transport d'Electricité, elle-même détenue à 50,1 % par EDF et à 49,9 % par la Caisse des dépôts et consignations ainsi que par la société CNP Assurances, et d'autre part, demandé le réexamen, prévu à l'article L. 111-4 du code de l'énergie, de la certification qui lui avait été accordée, en vertu de l'article L. 111-3 du même code, en tant que gestionnaire d'un réseau de transport d'électricité. Par une délibération du 11 janvier 2018, la CRE a décidé le maintien de cette certification sous diverses réserves formulées aux points 9 et 10. M. B...a demandé l'annulation de cette décision en tant qu'elle a demandé à EDF, en son point 10, de proposer à l'assemblée générale des actionnaires de RTE, dans les meilleurs délais et au plus tard dans les deux mois suivant sa publication au Journal officiel de la République française, la nomination d'un autre représentant d'EDF que lui au sein du conseil de surveillance de RTE. Par la présente requête, il demande la suspension de l'exécution de cette réserve, en application de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              3. Aux termes, d'une part, des deux premiers alinéas de l'article L. 111-3 du code de l'énergie : " Seule une société dont la Commission de régulation de l'énergie a préalablement certifié qu'elle respectait les obligations découlant des règles d'indépendance énoncées à la présente sous-section peut être agréée en tant que gestionnaire d'un réseau de transport d'électricité ou de gaz./ L'octroi de la certification peut être assorti de l'obligation faite à la société gestionnaire de réseau de transport de prendre, dans un délai fixé, diverses mesures organisationnelles destinées à garantir son indépendance. ".<br/>
<br/>
              4. En vertu, d'autre part, de l'article L. 111-24 du même code, l'exercice du mandat de membre du conseil de surveillance de la société gestionnaire de réseau de transport est, par dérogation au titre II du livre II du code de commerce, régi par les règles fixées aux articles L. 111-25 à L. 111-28. Aux termes de l'article L. 111-25 du même code : " Pour la moitié moins un, dénommée aux articles L. 111-26 à L. 111-28 la " minorité ", des membres composant ... son conseil de surveillance, l'autorité investie du pouvoir de nomination au sein de la société gestionnaire d'un réseau de transport notifie à la Commission de régulation de l'énergie, préalablement à leur nomination ou à la reconduction de leur mandat, l'identité des personnes et les conditions régissant leurs mandats, y compris leur durée et les conditions de leur cessation. / Si la Commission de régulation de l'énergie estime que les conditions régissant l'exercice du mandat ne répondent pas aux exigences fixés à l'article L. 111-26, elle peut s'opposer à la nomination ou à la reconduction, dans un délai et des conditions fixées par décret en Conseil d'Etat /... ". Aux termes de l'article L. 111-27 du même code : " Après la cessation de leur mandat, les personnes appartenant à la minorité des membres ... du conseil de surveillance de la société gestionnaire d'un réseau de transport ne peuvent exercer d'activités, ni avoir de responsabilités professionnelles dans les autres sociétés composant l'entreprise verticalement intégrée d'électricité ou de gaz définie à l'article L. 111-10, ni détenir d'intérêt dans ces sociétés, ni exercer de responsabilités dans une société dont l'essentiel des relations contractuelles s'effectue avec ces sociétés, pendant une période de quatre ans. ".<br/>
<br/>
              5. En premier lieu, il résulte des dispositions précitées qu'indépendamment des pouvoirs qu'elle tient de l'article L. 111-25 du code de l'énergie et en vertu desquels elle peut s'opposer à la nomination de membres de la minorité du conseil de surveillance de la société gestionnaire de réseau de transport qui ne respectent pas les exigences fixées à l'article L. 111-26, la CRE peut, sur le fondement de l'article L. 111-3, assortir toute décision accordant ou maintenant la certification d'une telle société, de l'obligation faite à celle-ci que soit prise, par elle-même ou par l'un de ses actionnaires, dans un délai fixé, les mesures organisationnelles destinées à garantir son indépendance, telle celle consistant à remplacer un membre du conseil de surveillance qui ne respecte pas l'exigence posée par l'article L. 111-27. <br/>
<br/>
              6. Par suite, les moyens tirés, d'une part, de ce que la CRE ne disposait pas, au titre de l'article L. 111-3 du code de l'énergie, du pouvoir d'adresser une demande à EDF, et d'autre part, de ce qu'elle ne pouvait s'opposer, au regard de l'article L. 111-25 du même code, qu'aux nominations au sein du conseil de surveillance au titre de la minorité, ne sont pas propres à créer, en l'état de l'instruction, un doute sérieux quant à la légalité du point 10 de la délibération contestée.<br/>
<br/>
              7. En deuxième lieu, il résulte des motifs non contestés du point 2.2.1.5 de la délibération en litige, d'une part, qu'alors qu'il était président du conseil de surveillance de RTE, en qualité de membre de la minorité depuis août 2015, M. B...a indiqué dans un courrier adressé le 21 mars 2017 à RTE qu'EDF avait proposé qu'il soit désormais son représentant au sein de ce conseil de surveillance, et d'autre part, que cette circonstance faisait obstacle à ce que l'intéressé puisse continuer à être regardé comme membre de la minorité. <br/>
<br/>
              8. Par suite, et dès lors qu'un membre du conseil de surveillance ne saurait continuer à y siéger sans que son mandat soit renouvelé lorsqu'il quitte ou rejoint la minorité, le moyen tiré de ce que la nomination de M. B...en tant que représentant d'EDF ne serait pas intervenue après la cessation de son mandat, comme le prévoit l'article L. 111-27 du code de l'énergie, n'est pas davantage propre à créer un doute sérieux.<br/>
<br/>
              9. En troisième et dernier lieu, il ressort des pièces du dossier qu'en vertu de l'article 2.1.6 du pacte d'actionnaire conclu le 31 mars 2017 entre EDF, la Caisse des dépôts et consignations ainsi que la société CNP Assurances, chaque partie devra faire en sorte que les représentants qu'elles désigneront au sein du conseil de surveillance de RTE, soient liés à elle ou à défaut à une autre partie par un contrat de travail, un mandat social ou un mandat dont la nomination au conseil de surveillance de RTE serait l'objet et aux termes duquel le représentant s'engage à se concerter étroitement avec la Partie l'ayant désigné et à démissionner en cas de divergences de vue ou d'analyse avec celle-ci. <br/>
<br/>
              10. Par suite, et alors même que M. B...n'est pas rémunéré par EDF, le moyen tiré de ce que sa nomination en qualité de représentant de cette société au sein du conseil de surveillance de RTE ne caractériserait pas l'exercice de responsabilités professionnelles dans une autre société de l'entreprise verticalement intégrée à laquelle celle-ci appartient, n'est pas non plus propre à créer un doute sérieux quant à la légalité de la délibération contestée.<br/>
<br/>
              11. Il résulte de tout ce qui précède qu'il y a lieu, sans qu'il soit besoin de se prononcer sur la condition d'urgence, de rejeter la demande de suspension présentée par M. B... ainsi, par voie de conséquence, que les conclusions présentées par celui-ci au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B...et au président de la Commission de régulation de l'énergie.<br/>
Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire, au ministre de l'économie et des finances, au président du directoire de la société Réseau Transport Electricité et au président-directeur général de la société Electricité de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
