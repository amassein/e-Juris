<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039112477</ID>
<ANCIEN_ID>JG_L_2019_09_000000434104</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/11/24/CETATEXT000039112477.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/09/2019, 434104, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434104</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:434104.20190913</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Office français de l'immigration et de l'intégration, ou subsidiairement au préfet du Bas-Rhin, de lui indiquer un lieu d'hébergement dans le délai de cinq jours, sous astreinte de 100 euros par jour de retard à compter de la notification de l'ordonnance. Par une ordonnance n° 1906085 du 19 août 2019, le tribunal administratif de Strasbourg a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 2 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ; <br/>
<br/>
              3°) de faire droit à ses conclusions de première instance ; <br/>
<br/>
              4°) de mettre à la charge de l'Office français de l'immigration et de l'intégration, ou à titre subsidiaire du préfet du Bas-Rhin, la somme de 1 000 euros à verser à Me C..., son conseil, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que ses conditions d'existence sont actuellement indécentes ;<br/>
              - sa situation de vulnérabilité, qui s'est dégradée depuis quatorze mois, implique qu'un hébergement lui soit proposé au titre de l'asile ou, à défaut, un hébergement d'urgence ;<br/>
              - l'ordonnance est entachée d'une autre erreur d'appréciation en considérant que le montant de l'allocation pour demandeur d'asile dont il bénéficie est suffisant pour accéder à un logement ;<br/>
              - elle est entachée d'une erreur de droit et d'appréciation en écartant l'existence d'une atteinte grave et manifestement illégale à son droit d'asile, à son droit à un hébergement d'urgence ainsi qu'à son droit à l'instruction alors que l'absence d'hébergement le prive de la possibilité d'accéder à un enseignement de niveau supérieur à celui déjà acquis.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive n° 2013/33/UE du 26 juin 2013 ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;  <br/>
              - le code de l'action sociale et des familles ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. D'une part, si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés, qui apprécie si les conditions prévues par l'article L. 521-2 du code de justice administrative sont remplies à la date à laquelle il se prononce, ne peut faire usage des pouvoirs qu'il tient de cet article en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de famille.<br/>
<br/>
              3. D'autre part, il appartient aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une carence caractérisée dans l'accomplissement de cette tâche peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              4. Il résulte de l'instruction diligentée par le juge des référés du tribunal administratif de Strasbourg que M. B... A..., ressortissant guinéen né en 1998, a sollicité le statut de réfugié le 31 mai 2018. Une attestation de demande d'asile en procédure Dublin lui a été remise le même jour, puis l'intéressé a été placé en procédure normale à compter du 15 mai 2019. M. A..., qui indique n'avoir eu accès ni au dispositif d'hébergement des demandeurs d'asile, ni au dispositif d'hébergement d'urgence depuis le 31 mai 2018, relève appel de l'ordonnance du 19 août 2019 par laquelle le juge des référés du tribunal administratif de Strasbourg, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint à l'Office français de l'immigration et de l'intégration, ou subsidiairement au préfet du Bas-Rhin, de lui indiquer un lieu d'hébergement dans le délai de cinq jours, sous astreinte de 100 euros par jour de retard. Pour rejeter cette demande, le juge des référés s'est fondé sur la triple circonstance que M. A..., étant jeune, célibataire et en bonne santé, ne présente aucune vulnérabilité particulière, que le dispositif d'accueil des demandeurs d'asile est saturé dans le Bas-Rhin et qu'enfin, n'étant pas hébergé, M. A... perçoit le montant majoré de l'allocation pour demandeur d'asile. Estimant que ces éléments, d'une part, ne permettent pas de regarder l'intéressé comme se trouvant dans une situation particulière de vulnérabilité et, d'autre part, manifestent que l'administration a accompli les diligences nécessaires, le juge des référés du tribunal administratif de Strasbourg en a déduit qu'une carence de l'administration dans l'accomplissement de ses obligations n'était, en l'état de l'instruction, pas suffisamment caractérisée pour être regardée comme constitutive d'une atteinte grave et manifestement illégale au droit d'asile, au droit à un hébergement ou au droit à l'instruction de M. A.... <br/>
              5. A l'appui de son appel, M. A..., qui allègue à nouveau sa situation de vulnérabilité faute de disposer depuis quatorze mois d'un hébergement et fait en outre valoir le caractère très insuffisant du montant de l'allocation pour demandeur d'asile pour subvenir à ses besoins essentiels d'hébergement et de nourriture, n'apporte aucun élément nouveau susceptible d'infirmer l'appréciation portée par le juge des référés du tribunal administratif de Strasbourg selon laquelle une carence de l'administration dans l'accomplissement de ses obligations n'était, en l'état de l'instruction, pas suffisamment caractérisée pour être regardée comme constitutive d'une atteinte grave et manifestement illégale au droit d'asile, au droit à un hébergement ou au droit à l'instruction de M. A....<br/>
<br/>
              6. Il résulte de tout ce qui précède qu'il est manifeste que l'appel de M. A... ne peut être accueilli. Sa requête, y compris les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, ne peut dès lors qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative et sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle provisoire.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
