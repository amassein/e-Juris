<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027353545</ID>
<ANCIEN_ID>JG_L_2013_04_000000354695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/35/35/CETATEXT000027353545.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 24/04/2013, 354695, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP VINCENT, OHL ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:354695.20130424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 7 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour la commune de Bettancourt-la-Ferrée, représentée par son maire ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0901042 du 6 octobre 2011 par lequel le tribunal administratif de Châlons-en-Champagne, statuant sur la demande de Mme B...A..., a annulé la décision du maire de Bettancourt-la-Ferrée prise en mars 2009 mettant fin au versement de l'indemnité d'exercice des missions de cette dernière à compter du mois de mars 2009 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 91-875 du 6 septembre 1991 ;<br/>
<br/>
              Vu le décret n° 97-1223 du 26 décembre 1997 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Vincent, Ohl, avocat de la commune de Bettancourt-la-Ferrée et de la SCP Didier, Pinet, avocat de Mme A...,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Vincent, Ohl, avocat de la commune de Bettancourt-la-Ferrée et à la SCP Didier, Pinet, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 88 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " L'assemblée délibérante de chaque collectivité territoriale ou le conseil d'administration d'un établissement public local fixe, par ailleurs, les régimes indemnitaires dans la limite de ceux dont bénéficient les différents services de l'Etat. (...) " ; qu'aux termes de l'article 1er du décret du 6 septembre 1991 pris pour l'application du premier alinéa de l'article 88 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le régime indemnitaire fixé par les assemblées délibérantes des collectivités territoriales et les conseils d'administration des établissements publics locaux pour les différentes catégories de fonctionnaires territoriaux ne doit pas être plus favorable que celui dont bénéficient les fonctionnaires de l'Etat exerçant des fonctions équivalentes. (...) " ; qu'aux termes de l'article 2 du même décret : " L'assemblée délibérante de la collectivité ou le conseil d'administration de l'établissement fixe, dans les limites prévues à l'article 1er, la nature, les conditions d'attribution et le taux moyen des indemnités applicables aux fonctionnaires de ces collectivités ou établissements. (...) " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er du décret du 26 décembre 1997 portant création d'une indemnité d'exercice de missions des préfectures : " Une indemnité d'exercice est attribuée aux fonctionnaires de la filière administrative et de service du cadre national des préfectures, de la filière technique (corps des ouvriers professionnels et maîtres ouvriers, corps des services techniques du matériel) et de la filière médico-sociale (infirmiers, assistants et conseillers techniques des services sociaux) qui participent aux missions des préfectures dans lesquelles ils sont affectés. " ; qu'aux termes de l'article 2 du même décret : " Le montant de l'indemnité mentionnée à l'article 1er du présent décret est calculé par application à un montant de référence fixé par arrêté conjoint du ministre de l'intérieur, du ministre chargé de la fonction publique, du ministre chargé de l'outre-mer et du ministre chargé du budget d'un coefficient multiplicateur d'ajustement compris entre 0,8 et 3 " ; <br/>
<br/>
              3. Considérant qu'il résulte de l'ensemble de ces dispositions que, si une commune ne peut attribuer aux agents répondant aux conditions légales pour en bénéficier une indemnité d'exercice des missions d'un montant supérieur au triple du montant annuel de référence, il lui est loisible de fixer la limite basse du coefficient multiplicateur d'ajustement du montant de référence en deçà du seuil de 0,8 prévu par l'article 2 du décret du 26 décembre 1997, et, le cas échéant, de prévoir un coefficient nul ;<br/>
<br/>
              4. Considérant que, pour annuler la décision du maire de la commune de Bettancourt-la-Ferrée mettant fin à compter du mois de mars 2009 au versement de l'indemnité d'exercice des fonctions dont bénéficiait Mme A..., agent territorial spécialisé des écoles maternelles, le tribunal administratif de Châlons-en-Champagne a jugé qu'il ne résultait pas des termes de la délibération du conseil municipal du 26 novembre 2003 fixant le régime indemnitaire des agents qu'il ait entendu instituer une règle de modulation différente de celle fixée par l'article 2 du décret du 26 décembre 1997 et qu'en conséquence, Mme A... avait droit au minimum à une indemnité modulée par un coefficient d'ajustement de 0,8, alors que la délibération prévoyait que cette indemnité serait calculée par application d'un coefficient d'ajustement compris entre 0 et 3 et que la commune avait soutenu qu'elle était en droit, en vertu des règles qui régissent les indemnités des agents des collectivités territoriales, de retenir une telle modulation variant entre 0 et 3 ; qu'en statuant ainsi, sans préciser les éléments sur lesquels il se fondait pour porter cette appréciation, le tribunal administratif a insuffisamment motivé son jugement et, par suite, commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Bettancourt-la-Ferrée est fondée à demander l'annulation du jugement attaqué ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Bettancourt-la-Ferrée qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Châlons-en-Champagne du 6 octobre 2011 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Châlons-en-Champagne.<br/>
<br/>
Article 3 : Les conclusions de la commune de Bettancourt-la-Ferrée et de Mme A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présence décision sera notifiée à la commune de Bettancourt-la-Ferrée et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
