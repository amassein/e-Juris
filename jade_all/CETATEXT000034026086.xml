<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026086</ID>
<ANCIEN_ID>JG_L_2017_02_000000394756</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/60/CETATEXT000034026086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 10/02/2017, 394756</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394756</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP LYON-CAEN, THIRIEZ ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394756.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 23 novembre 2015 et 23 février 2016 au secrétariat du contentieux du Conseil d'Etat, le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 23 septembre 2015 par laquelle le conseil national de l'ordre des masseurs-kinésithérapeutes a dit qu'il n'y avait pas lieu de statuer sur son recours contre la décision du 15 mai 2012 par laquelle le conseil régional de l'ordre des masseurs-kinésithérapeutes de Languedoc-Roussillon a, d'une part, annulé sa décision du 6 mars 2012 refusant l'inscription de M. A...B...au tableau de l'ordre des masseurs-kinésithérapeutes du Gard et, d'autre part, inscrit M. B...à ce tableau ;<br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du conseil départemental de l'Ordre des masseurs-kinesithérapeutes du Gard, à la SCP Lyon-Caen, Thiriez, avocat de M. B... et à la SCP Hémery, Thomas-Raquin, avocat du conseil national de l'ordre des masseurs-kinésithérapeuthes.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que, par une décision du 6 mars 2012, le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard a refusé d'inscrire M. A...B...au tableau de l'ordre des masseurs-kinésithérapeutes de ce département ; que, sur recours de M.B..., le conseil régional de l'ordre des masseurs-kinésithérapeutes de Languedoc-Roussillon a, par une décision du 15 mai 2012, annulé la décision du 6 mars 2012 du conseil départemental et prononcé l'inscription de l'intéressé au tableau de l'ordre des masseurs-kinésithérapeutes du Gard ; que, par une décision du 23 septembre 2015, le conseil national de l'ordre des masseurs-kinésithérapeutes a estimé qu'il n'y avait pas lieu de statuer sur le recours introduit par le conseil départemental contre la décision du 15 mai 2012 du conseil régional dès lors que, postérieurement à cette décision, M. B... avait été inscrit au tableau de l'ordre du département de la Seine-Saint-Denis ; que le conseil départemental présente un recours pour excès de pouvoir contre la décision du conseil national ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article R. 4321-129 du code de la santé publique : " Le lieu habituel d'exercice du masseur-kinésithérapeute est celui de la résidence professionnelle au titre de laquelle, conformément à l'article L. 4321-10, il est inscrit sur le tableau du conseil départemental de l'ordre " ; qu'aux termes de l'article R. 4112-3, rendu applicable aux masseurs-kinésithérapeutes par l'article R. 4323-1 du même code : " En cas de transfert de sa résidence professionnelle hors du département, le praticien est tenu de demander, par lettre recommandée avec demande d'avis de réception, sa radiation du tableau de l'ordre du département où il exerçait " ; qu'il résulte de ces dispositions qu'un masseur-kinésithérapeute ne peut être inscrit qu'à un seul tableau, qui est celui du département où il a sa résidence professionnelle ; <br/>
<br/>
              3. Considérant, d'autre part, qu'il résulte des dispositions de l'article L. 4112-4 du code de la santé publique, rendues applicables aux masseurs-kinésithérapeutes par l'article L. 4321-19 du même code, que les recours formés devant un conseil régional de l'ordre des masseurs-kinésithérapeutes et devant le conseil national constituent des préalables obligatoires à la saisine du juge ; qu'il en résulte que le conseil national, saisi d'un recours contre une décision d'un conseil régional, doit se placer, pour apprécier le droit du masseur-kinésithérapeute d'être inscrit au tableau, à la date à laquelle il statue ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier qu'à la suite d'un changement de résidence professionnelle, M.B..., qui avait été inscrit au tableau de l'ordre des masseurs-kinésithérapeutes du département du Gard par la décision du 15 mai 2012 du conseil régional de Languedoc-Roussillon, a demandé son inscription au tableau du département de la Seine-Saint-Denis, à laquelle il a été procédé le 12 juillet 2012 ; qu'il résulte des dispositions citées au point 2 que son inscription au tableau de l'ordre du département du Gard devait être regardée comme abrogée depuis cette même date ; que, dès lors, en estimant que le recours du conseil départemental du Gard contre la décision du conseil régional était, à la date à laquelle il statuait, devenu sans objet, le conseil national a fait une exacte application des dispositions citées ci-dessus du code de la santé publique ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B..., qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard la somme de 3 000 euros à verser à M.B..., au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard est rejetée.<br/>
Article 2 : Le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard versera à M. B...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard, au conseil national de l'ordre des masseurs-kinésithérapeutes et à M. A... B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-018 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. - 1) POSSIBILITÉ D'ÊTRE INSCRIT SUR PLUSIEURS TABLEAUX DÉPARTEMENTAUX - ABSENCE - CONSÉQUENCES - 2) RECOURS DEVANT UN CONSEIL RÉGIONAL  DE L'ORDRE OU DEVANT LE CONSEIL NATIONAL CONTRE UNE INSCRIPTION AU TABLEAU - DATE À LAQUELLE LE CONSEIL DOIT APPRÉCIER LE DROIT D'ÊTRE INSCRIT AU TABLEAU - DATE À LAQUELLE IL STATUE - 3) CAS OÙ L'INTÉRESSÉ A ÉTÉ INSCRIT SUR UN AUTRE TABLEAU LORSQUE LE RECOURS CONTRE SON INSCRIPTION SUR LE TABLEAU PRÉCÉDENT EST EXAMINÉ PAR LE CONSEIL NATIONAL - RECOURS SANS OBJET.
</SCT>
<ANA ID="9A"> 55-01-02-018 1) Un masseur-kinésithérapeute ne peut être inscrit qu'à un seul tableau, qui est celui du département où il a sa résidence professionnelle. Par suite, en cas d'inscription sur un nouveau tableau départemental, l'inscription précédente sur le tableau d'un autre département doit être regardée comme abrogée à la date de la nouvelle inscription.,,,2) Les recours formés devant un conseil régional de l'ordre des masseurs-kinésithérapeutes et devant le conseil national constituent des préalables obligatoires à la saisine du juge. Il en résulte que le conseil national, saisi d'un tel recours contre une décision d'un conseil régional, doit se placer, pour apprécier le droit du masseur-kinésithérapeute d'être inscrit au tableau, à la date à laquelle il statue.,,,3) Masseur-kinésithérapeute qui avait été inscrit au tableau de l'ordre du département du Gard puis a été inscrit au tableau de l'ordre du département de la Seine-Saint-Denis. Son inscription au tableau de l'ordre du département du Gard devait être regardée comme abrogée à la date de cette nouvelle inscription. Le recours devant le conseil national de l'ordre contre l'inscription de l'intéressé au tableau de l'ordre du département du Gard avait donc, à cette date, perdu son objet.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
