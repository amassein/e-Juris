<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018007707</ID>
<ANCIEN_ID>JG_L_2007_11_000000296114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/77/CETATEXT000018007707.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 09/11/2007, 296114, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2007-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>296114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP ROGER, SEVAUX ; SCP BACHELLIER, POTIER DE LA VARDE</AVOCATS>
<RAPPORTEUR>M. Alexandre  Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 2 août et 4 décembre 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE MIREMONT, représentée par son maire ; la COMMUNE DE MIREMONT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 1er juin 2006 par lequel la cour administrative d'appel de Bordeaux, à la demande de Mme Suzanne A, a, d'une part, annulé le jugement du 7 novembre 2002 du tribunal administratif de Toulouse ayant rejeté sa demande d'annulation de la décision du 19 avril 2000 du maire de la commune requérante lui refusant un permis de construire une maison à usage d'habitation et, d'autre part, annulé cette décision ;<br/>
<br/>
              2°) statuant au fond, de rejeter la requête d'appel de Mme A ;<br/>
<br/>
              3°) de mettre à la charge de Mme A le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Lallet, Auditeur,  <br/>
<br/>
              - les observations de la SCP Bachellier, Potier de la Varde, avocat de la COMMUNE DE MIREMONT et de la SCP Roger, Sevaux, avocat de Mme A, <br/>
<br/>
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que selon l'article R. 123-18 du code de l'urbanisme alors en vigueur, les zones de richesses naturelles dites « zones NC » doivent être protégées en raison notamment de la valeur agricole des terres ou de la richesse du sol et du sous-sol ; qu'en vertu du règlement du plan d'occupation des sols de la COMMUNE DE MIREMONT alors applicable, la zone NC « constitue un espace naturel qu'il convient de protéger » ; que l'article NC 1 de ce règlement dispose que : « sont notamment admises les occupations et utilisations du sol ci-après : / Dans les secteurs NC et NCb : / 1. Les constructions nécessaires aux exploitations agricoles et au logement des exploitants./ 2. L'aménagement, la restauration et l'extension justifiée des constructions existantes ainsi que la création d'annexes à l'habitat sans création d'une nouvelle unité d'habitation (...) » ; que l'article NC 2 du même règlement prévoit que « les occupations et utilisations du sol non mentionnées à l'article NC 1 sont interdites et notamment : / 1. Les constructions à usage : / d'habitat (...) » ; que si les dispositions de l'article NC 1 permettent la réalisation de travaux d'aménagement de constructions existantes, il résulte des termes mêmes employés par le règlement du plan d'occupation des sols, ainsi d'ailleurs que de la vocation assignée par celui-ci à la zone NC, qu'elles s'opposent à tout changement de destination des bâtiments affectés à un usage agricole, notamment en vue de leur transformation en locaux d'habitation, ainsi qu'à la construction de tels locaux, sauf lorsque ceux-ci sont nécessaires au logement des exploitants ;<br/>
<br/>
              Considérant que, pour annuler le jugement du tribunal administratif de Toulouse rejetant la requête de Mme A tendant à l'annulation de l'arrêté en date du 19 avril 2000 par lequel le maire de Miremont a rejeté la demande de permis de construire un local d'habitation en lieu et place d'un hangar agricole sur une parcelle lui appartenant, située en zone NC, la cour administrative d'appel de Bordeaux a estimé que les dispositions des articles NC 1 et NC 2 du règlement du plan d'occupation des sols de la COMMUNE DE MIREMONT autorisaient de manière générale les travaux d'aménagement ou de restauration d'un hangar agricole s'accompagnant d'un changement de destination en maison d'habitation ; qu'il résulte de ce qui a été dit ci-dessus qu'elle a, ce faisant, commis une erreur de droit ; que, dès lors, l'arrêt attaqué doit, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que la construction projetée consistait à transformer un hangar agricole existant en maison d'habitation ; qu'il est constant que Mme A, qui précisait dans sa demande de permis qu'elle désirait occuper personnellement les lieux à titre de résidence principale, n'avait pas la qualité d'exploitante agricole à la date de la décision litigieuse ; que, dès lors que le maire était tenu, en application des dispositions du règlement du plan d'occupation des sols de la commune, de refuser le permis sollicité, les moyens invoqués par Mme A à l'encontre de cette décision de refus sont inopérants ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la COMMUNE DE MIREMONT, Mme A n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif a rejeté sa demande ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la COMMUNE DE MIREMONT, qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre la charge de Mme A le versement à cette commune d'une somme de 2 000 euros à ce titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux en date du 1er juin 2006 est annulé.<br/>
<br/>
Article 2 : La requête présentée par Mme A devant la cour administrative d'appel de Bordeaux est rejetée.<br/>
<br/>
Article 3 : Mme A versera à la COMMUNE DE MIREMONT une somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la COMMUNE DE MIREMONT et à Mme Suzanne A.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
