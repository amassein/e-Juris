<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143123</ID>
<ANCIEN_ID>JG_L_2020_07_000000434709</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143123.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 22/07/2020, 434709, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434709</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434709.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre de l'intérieur a rejeté sa demande du 29 juin 2017 tendant à ce qu'il retire sa décision de ramener à 1 % le taux de liquidation de l'indemnité de résidence dont il bénéficiait et d'enjoindre au ministre de l'intérieur de rétablir ce taux à 3 % et de procéder au versement des sommes dues à ce titre depuis le mois d'avril 2017. Par une ordonnance n° 1707949 du 30 août 2019, la présidente de la 6ème chambre du tribunal administratif a fait droit à ses demandes. <br/>
<br/>
              Par un pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 19 septembre 2019, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B....<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 85-1148 du 24 octobre 1985 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. B..., brigadier de police, s'est vu allouer, à raison de son affectation le 1er septembre 2016 à l'antenne de Meaux de la direction régionale de la police judiciaire de Versailles, l'indemnité de résidence prévue par le décret du 24 octobre 1985 relatif à la rémunération des personnels civils et militaires de l'Etat, des personnels des collectivités territoriales et des personnels des établissements publics d'hospitalisation, liquidée au taux de 3 %. Par un courrier du 14 avril 2017, l'administration l'a informé que ce taux, qui ne correspondait pas à sa situation administrative, lui avait été attribué par erreur et qu'il allait, pour cette raison, être abaissé à 1 % à compter du mois d'avril 2017. Le ministre de l'intérieur se pourvoit en cassation contre l'ordonnance du 30 août 2019 par laquelle la présidente de la 6ème chambre du tribunal administratif de Melun a, à la demande de M. B..., annulé la décision du 14 avril 2017 et le rejet, par le ministre, du recours formé par M. B... contre cette décision.<br/>
<br/>
              2. Il résulte des termes mêmes de l'ordonnance attaquée que, pour juger que la décision du 14 avril 2017 était illégale, la présidente de la 6ème chambre du tribunal administratif de Melun s'est fondée sur ce qu'elle avait pour effet, contrairement aux dispositions de l'article L. 242-1 du code des relations entre le public et l'administration, d'abroger, plus de quatre mois après qu'elle a été prise, la décision par laquelle l'administration a accordé à M. B..., à compter de son affectation à Meaux en 2016, une indemnité de résidence au taux de 3 %.<br/>
<br/>
              3. Il ressort toutefois des pièces du dossier soumis au juge du fond que la décision du 14 avril 2017 ne procède pas expressément à l'abrogation d'une décision de 2016 ayant attribué à M. B... une indemnité de résidence au taux de 3 % et que cette décision doit dès lors être regardée, ainsi que le soutient le ministre de l'intérieur, comme manifestant seulement l'intention de l'administration de cesser, pour l'avenir, le versement à M. B... des sommes dues en application de cette décision de 2016, dont l'administration considère qu'elle est illégale en tant qu'elle lui attribue une indemnité de résidence à un taux supérieur à 1 %.<br/>
<br/>
              4. Le ministre de l'intérieur est fondé à soutenir, par un moyen qui, même si l'application de ces dispositions n'a pas été discutée devant le juge du fond, n'est pas nouveau en cassation, que l'article 37-1 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, aux termes duquel  : " Les créances résultant de paiements indus effectués par les personnes publiques en matière de rémunération de leurs agents peuvent être répétées dans un délai de deux années à compter du premier jour du mois suivant celui de la date de mise en paiement du versement erroné, y compris lorsque ces créances ont pour origine une décision créatrice de droits irrégulière devenue définitive ", lui permet, contrairement à ce que juge l'ordonnance attaquée, de cesser de verser à l'un de ses agents des sommes dues en application d'une décision attribuant illégalement un avantage financier, alors même que, ayant le caractère d'une décision créatrice de droits, elle ne pourrait plus être ni retirée ni abrogée. <br/>
<br/>
              5. Il résulte de ce qui précède que le ministre de l'intérieur est fondé à soutenir que l'ordonnance qu'il attaque est entachée d'erreur de droit et à en demander, par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'annulation.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que demande, à ce titre, M. B....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 30 août 2019 de la présidente de la 6ème chambre du tribunal administratif de Melun est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Melun.<br/>
Article 3 : Les conclusions présentées par M. B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
