<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411753</ID>
<ANCIEN_ID>JG_L_2019_04_000000414325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411753.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 24/04/2019, 414325, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:414325.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Aspirline a demandé au tribunal administratif de Strasbourg, d'une part, de condamner la communauté de communes de Maizières-les-Metz, devenue communauté de communes Rives de Moselle, à lui verser la somme de 131 878,42 euros au titre des prestations exécutées dans le cadre du marché d'acquisition de conteneurs enterrés pour la collecte d'ordures ménagères et, d'autre part, d'annuler le titre de recettes d'un montant de 31 154,22 euros émis à son encontre le 28 janvier 2015 par le président de cette communauté de communes. Par un jugement n°s 1301276, 1501596 du 9 mars 2016, le tribunal administratif de Strasbourg a annulé ce titre de recettes en tant qu'il excédait la somme de 3 541,96 euros et rejeté le surplus des demandes de la société Aspirline.<br/>
<br/>
              Par un arrêt n° 16NC00830 du 13 juillet 2017, la cour administrative d'appel de Nancy a, sur appel de la société Aspirline, réformé ce jugement en condamnant la communauté de communes Rives de Moselle à payer à la société Aspirline la somme de 97 407,30 euros correspondant au solde du marché et rejeté le surplus des conclusions de la requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 septembre et 15 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes Rives de Moselle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a partiellement fait droit à l'appel de la société Aspirline ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter cet appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Aspirline la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - l'arrêté du 19 janvier 2009 portant approbation du cahier des clauses administratives générales des marchés publics de fournitures courantes et services ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. A...Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme B...Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la communaute de communes Rives de Moselle et à la SCP Didier, Pinet, avocat de la société Aspirline.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la communauté de communes de Maizières-les-Metz, à laquelle s'est substituée la communauté de communes Rives de Moselle, a, en 2012, conclu avec la société Kollect'in, devenue Aspirline, un marché portant sur l'acquisition de conteneurs enterrés pour la collecte des ordures ménagères à Maizières-les-Metz. Ce marché a été résilié pour faute du titulaire le 26 juin 2012. Le décompte de résiliation, faisant apparaître un solde débiteur de 25 131,96 euros, a été notifié à la société Aspirline le 30 octobre 2012. Cette dernière a alors transmis à la communauté de communes Rives de Moselle un mémoire en réclamation avant de saisir le tribunal administratif de Strasbourg d'une demande tendant à la condamnation de la communauté de communes à lui verser le montant des prestations déjà réalisées dans le cadre du marché et d'une demande tendant à l'annulation du titre exécutoire émis à son encontre par la communauté de communes le 28 janvier 2015 en vue de recouvrer une somme de 31 154,22 euros correspondant aux pénalités de retard figurant au décompte. La société Aspirline, qui a obtenu l'annulation de ce titre exécutoire en tant qu'il excède un montant de 3 541,96 euros, a relevé appel du jugement du 9 mars 2016 du tribunal administratif de Strasbourg en tant qu'il a rejeté sa demande tendant à la condamnation de la communauté de communes à lui payer les prestations exécutées dans le cadre de son marché. Par un arrêt du 13 juillet 2017, la cour administrative d'appel de Nancy a réformé ce jugement en condamnant la communauté de communes Rives de Moselle à payer à la société Aspirline la somme de 97 407,30 euros correspondant au solde du marché. La communauté de communes Rives de Moselle se pourvoit en cassation contre cet arrêt en tant qu'il a partiellement fait droit à l'appel de la société Aspirline.<br/>
<br/>
              2. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Nancy a estimé qu'il n'était pas contesté que les prestations correspondant au montant du marché ont été exécutées par la société Aspirline alors que c'est précisément en raison de leur inexécution partielle que la communauté de communes Rives de Moselle a mis en demeure la société Aspirline de les exécuter et qu'elle a résilié le marché en litige. En statuant ainsi, la cour a dénaturé les pièces du dossier. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé en tant qu'il a partiellement fait droit à l'appel de la société Aspirline.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Aspirline la somme de 3 000 euros à verser à la communauté de communes Rives de Moselle, au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la communauté de communes qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er à 3 de l'arrêt du 13 juillet 2017 de la cour administrative d'appel de Nancy sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Nancy.<br/>
Article 3 : La société Aspirline versera à la communauté de communes Rives de Moselle une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Aspirline présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la communauté de communes Rives de Moselle et à la société Aspirline.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
