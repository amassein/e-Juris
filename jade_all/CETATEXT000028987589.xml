<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028987589</ID>
<ANCIEN_ID>JG_L_2014_05_000000375100</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/98/75/CETATEXT000028987589.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 26/05/2014, 375100, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375100</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SPINOSI</AVOCATS>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:375100.20140526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 31 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour la commune de Longueville, représentée par son maire ; la commune de Longueville demande au Conseil d'Etat :<br/>
<br/>
              1°) de prononcer le sursis à l'exécution de l'arrêt n° 11NT02135 du 14 juin 2013 par lequel la cour administrative d'appel de Nantes, d'une part, a annulé, à la demande de la société Les trois coteaux, le jugement n° 0902666 du 10 juin 2011 par lequel le tribunal administratif de Caen a rejeté sa demande tendant à la condamnation de la commune de Longueville à lui verser la somme de 213 600 euros, assortie des intérêts au taux légal, en réparation des préjudices résultant de l'illégalité de la décision du 18 décembre 2007 du maire de Longueville refusant de lui délivrer le permis de construire qu'elle sollicitait, d'autre part, l'a condamnée à verser à la société Les trois coteaux la somme de 209 900 avec intérêts au taux légal à compter du 1er septembre 2009 ;<br/>
<br/>
              2°) de mettre à la charge de la société Les trois coteaux la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative, ainsi que les dépens ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la commune de Longueville et à Me Spinosi, avocat de la société Les trois coteaux ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle rendue en dernier ressort, l'infirmation de la solution retenue par les juges du fond. " ; <br/>
<br/>
              2. Considérant, en premier lieu, que, la décision contestée a condamné la commune de Longueville à verser à la société Les trois coteaux la somme de 209 900 euros avec intérêts au taux légal à compter du 1er septembre 2009 ; que le paiement de cette indemnité risque d'entraîner, pour la commune, des conséquences difficilement réparables eu égard à sa situation financière ; <br/>
<br/>
              3. Considérant, en second lieu, que les moyens tirés, d'une part, de ce que la cour administrative d'appel de Nantes a commis une erreur de droit et entaché son arrêt de dénaturation en accordant une indemnité à la société au titre d'un préjudice tiré du manque à gagner correspondant aux bénéfices attendus de l'opération immobilière projetée, d'autre part, de ce que la cour a inexactement qualifié les faits de l'espèce en jugeant qu'il existait un lien de causalité directe entre la faute de la commune résultant de l'illégalité du refus de permis de construire opposé par le maire le 18 décembre 2007 et le préjudice subi par la société, paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de l'arrêt attaqué, l'infirmation de la solution retenue par les juges du fond ;<br/>
<br/>
              4. Considérant que, dans ces conditions, il y a lieu d'ordonner le sursis à l'exécution de l'arrêt de la cour administrative d'appel de Nantes ;<br/>
<br/>
              5. Considérant enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à ce titre à la charge de la commune de Longueville qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par la société Les trois coteaux ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce même titre par la commune de Longueville ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : Jusqu'à ce qu'il ait été statué sur le pourvoi de la commune de Longueville contre l'arrêt du 14 juin 2013 de la cour administrative d'appel de Nantes, il sera sursis à l'exécution de cet arrêt. <br/>
<br/>
 Article 2 : Les conclusions de la société Les trois coteaux et de la commune de Longueville tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
 Article 3 : La présente décision sera notifiée à la commune de Longueville et à la société Les trois coteaux.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
