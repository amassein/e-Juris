<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022024106</ID>
<ANCIEN_ID>JG_L_2010_03_000000327129</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/02/41/CETATEXT000022024106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 26/03/2010, 327129, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2010-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>327129</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Chantepy</PRESIDENT>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Alain  Boulanger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 16 avril 2009 au secrétariat du contentieux du Conseil d'Etat, présenté par le MINISTRE DE LA DEFENSE ; le ministre requérant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 10 février 2009 par lequel la cour régionale des pensions de Montpellier a confirmé le jugement du 10 juin 2008 du tribunal départemental des pensions de l'Hérault reconnaissant que M. Tinh A remplissait les conditions légales pour bénéficier d'une pension de victime civile de la guerre d'Indochine au titre des infirmités de surdité gauche et de séquelles de brûlures du membre supérieur gauche ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Boulanger, chargé des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Boré et Salve de Bruneton, avocat de M. A, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré et Salve de Bruneton, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que M. A a demandé une pension de victime civile de la guerre au titre des infirmités de surdité gauche et de séquelles de brûlures du membre supérieur gauche, qui résulteraient d'un bombardement de l'armée française à Tion Xiao en 1952 lors de la guerre d'Indochine ; que, par un arrêt du 10 février 2009, déféré en cassation par le MINISTRE DE LA DEFENSE, la cour régionale des pensions de Montpellier a confirmé le jugement du 10 juin 2008 par lequel le tribunal départemental des pensions de l'Hérault a estimé que M. A remplissait les conditions légales pour bénéficier d'une pension pour victime civile de la guerre au titre de ces deux infirmités ;<br/>
<br/>
              Considérant qu'en se bornant à relever, d'une part, que l'expertise médicale du docteur B établissait que M. A présentait un taux d'invalidité de 15 % au titre de la surdité gauche et de 45 % au titre des séquelles de brûlures du membre supérieur gauche sans se prononcer sur l'existence d'un lien de causalité direct et certain entre ces infirmités et le fait de guerre invoqué, pourtant contesté par le ministre en appel, et en omettant, d'autre part, de répondre au moyen tiré du caractère erroné du motif du jugement par lequel le tribunal départemental des pensions énonçait que l'administration admettait le lien entre la perte d'audition et ce fait de guerre, la cour régionale des pensions n'a pas mis le juge de cassation à même d'exercer son contrôle ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'en application du 1° de l'article L. 4 du code des pensions militaires d'invalidité et des victimes de la guerre,  il est concédé une pension : 1° Au titre des infirmités résultant de blessures, si le degré d'invalidité qu'elles entraînent atteint ou dépasse 10 %  ; qu'il résulte des dispositions combinées des articles L. 195 et L. 213 de ce code que sont réputées causées par des faits de guerre les blessures ou lésions provoquées, même après la fin des opérations militaires, par des explosions de projectiles, des éboulements ou tous autres accidents pouvant se rattacher aux événements de guerre ; qu'il appartient au postulant victime civile de guerre, de faire la preuve de ses droits à pension en établissant notamment que les infirmités qu'il invoque ont leur origine dans une blessure ou une maladie causée par l'un des faits de guerre énoncés aux articles L. 195 et suivants de ce code ; que cette preuve, qui implique l'existence d'un lien de causalité direct et déterminant, ne saurait résulter d'une probabilité même forte, d'une vraisemblance ou d'une hypothèse médicale ;<br/>
<br/>
              Considérant, d'une part, qu'il est constant que M. A n'a demandé de pension qu'au titre de sa surdité gauche, et non de ses troubles auditifs bilatéraux ; qu'il résulte de l'instruction que son taux d'invalidité pour cette seule affection est inférieur au taux de 10 % requis en application de l'article L. 4 du code des pensions militaires d'invalidité et des victimes de la guerre ; que, dès lors, la lésion invoquée par M. A n'est pas susceptible d'ouvrir droit à pension ; <br/>
<br/>
              Considérant, d'autre part, que si l'armée française a été impliquée dans des combats dans le secteur de Lai Chau en 1952, aucun document contemporain des faits n'atteste que M. A aurait été blessé et soigné à la suite d'un bombardement de l'armée française dans ce secteur ; que les attestations produites par M. A ne sont pas de nature à pallier, à elles seules, l'absence de tout constat médical contemporain des faits ; que l'attestation du Dr C, relative à ses seuls troubles auditifs, n'établit pas de lien direct et déterminant de cause à effet entre les séquelles de brûlures du membre supérieur gauche de M. A et le fait de guerre invoqué ; que, dès lors, les infirmités en résultant ne peuvent ouvrir droit à pension ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le MINISTRE DE LA DEFENSE est fondé à demander l'annulation du jugement du tribunal départemental des pensions de l'Hérault ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 10 février 2009 de la cour régionale des pensions de Montpellier et le jugement du 10 juin 2008 du tribunal départemental des pensions de l'Hérault sont annulés.<br/>
Article 2 : La demande présentée par M. A devant le tribunal départemental des pensions de l'Hérault est rejetée. <br/>
Article 3 : La présente décision sera notifiée au MINISTRE DE LA DEFENSE et à M. Tinh A.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
