<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983409</ID>
<ANCIEN_ID>JG_L_2015_07_000000383383</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/34/CETATEXT000030983409.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 31/07/2015, 383383, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383383</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383383.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Pakers Mussy a demandé au tribunal administratif de Châlons-en-Champagne d'annuler pour excès de pouvoir la décision du 29 juin 2011 du ministre du travail, de l'emploi et de la santé en tant qu'elle a déclaré M. B...A...inapte à tout poste de travail dans l'entreprise. Par un jugement n° 1100827 du 18 juillet 2013, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 13NC01784 du 2 juin 2014, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société Pakers Mussy contre l'article 2 de ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 août et 4 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Pakers Mussy demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de M. A...le versement d'une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Pakers Mussy ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond qu'un médecin du travail a émis le 1er février 2011 un avis déclarant M. A...inapte à tout poste de travail chez son employeur, la société Pakers Mussy ; que l'employeur a contesté cet avis devant l'inspecteur du travail compétent qui a rejeté son recours par une décision du 14 avril 2011 ; que, saisi par l'employeur d'un recours hiérarchique, le ministre du travail, de l'emploi et de la santé a, par une décision du 29 juin 2011, annulé la décision de l'inspecteur du travail mais l'a confirmée sur le fond, en déclarant M. A...inapte à tout poste de travail dans l'entreprise ; que la société Pakers Mussy se pourvoit en cassation contre l'arrêt du 2 juin 2014 par lequel la cour administrative de Nancy a rejeté sa requête tendant à l'annulation du jugement du 18 juillet 2013 du tribunal administratif de Châlons-en-Champagne en tant qu'il a rejeté son recours contre cette décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4624-1 du code du travail : " Le médecin du travail est habilité à proposer des mesures individuelles telles que mutations ou transformations de postes, justifiées par des considérations relatives notamment à l'âge, à la résistance physique ou à l'état de santé physique et mentale des travailleurs. L'employeur est tenu de prendre en considération ces propositions et, en cas de refus, de faire connaître les motifs qui s'opposent à ce qu'il y soit donné suite. En cas de difficulté ou de désaccord, l'employeur ou le salarié peut exercer un recours devant l'inspecteur du travail. Ce dernier prend sa décision après avis du médecin inspecteur du travail " ; qu'aux termes de l'article R. 4624-31 du même code dans sa rédaction alors applicable : " Sauf dans le cas où le maintien du salarié à son poste de travail entraîne un danger immédiat pour sa santé ou sa sécurité ou celles des tiers, le médecin du travail ne peut constater l'inaptitude médicale du salarié à son poste de travail qu'après avoir réalisé : 1° Une étude de ce poste ; / 2° Une étude des conditions de travail dans l'entreprise ; / 3° Deux examens médicaux de l'intéressé espacés de deux semaines, accompagnés, le cas échéant, des examens complémentaires " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'en cas de difficulté ou de désaccord sur les propositions formulées par le médecin du travail concernant l'aptitude d'un salarié à occuper son poste de travail, il appartient à l'inspecteur du travail, saisi par l'une des parties, ou le cas échéant au ministre en cas de recours hiérarchique, de se prononcer définitivement sur cette aptitude ; que cette appréciation, qu'elle soit confirmative ou infirmative de l'avis du médecin du travail, se substitue à cet avis ; que seule la décision rendue par l'inspecteur du travail et, le cas échéant, par le ministre, est susceptible de faire l'objet d'un recours devant le juge de l'excès de pouvoir ; que, par suite, en jugeant que les vices invoqués contre l'avis du 1er février 2011 du médecin du travail, et notamment celui tiré de ce que ce dernier aurait été incompétent pour prendre cet avis, étaient sans incidence sur la légalité de la décision prise le 29 juin 2011 par le ministre chargé du travail, la cour administrative d'appel de Nancy, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Pakers Mussy doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Pakers Mussy est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Pakers Mussy. <br/>
Copie en sera adressée pour information à M. B...A...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
