<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033237411</ID>
<ANCIEN_ID>JG_L_2016_10_000000396170</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/23/74/CETATEXT000033237411.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 12/10/2016, 396170</TITRE>
<DATE_DEC>2016-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396170</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:396170.20161012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 18 janvier et 18 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des entreprises des loisirs marchands, l'Union des navigants et armateurs prestataires professionnels - Syndicat national des armateurs de voiliers et vedettes professionnels et le Syndicat national des entreprises commerciales des conducteurs de chiens attelés pour le loisir demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 16 novembre 2015 par laquelle le Premier ministre a rejeté leur demande tendant à l'abrogation des articles L. 212-1 et L. 212-8 du code du sport issus de l'ordonnance du 23 mai 2006 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment ses articles 34 et 38 ; <br/>
              - le code du sport, notamment ses articles L. 212-1 et L. 212-8 ;<br/>
              - la loi n° 2004-1343 du 9 décembre 2004, notamment ses articles 84 et 92 ;<br/>
              - l'ordonnance n° 2006-596 du 23 mai 2006 ;<br/>
              - la décision du 13 juillet 2016 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le Syndicat national des entreprises des loisirs marchands (SNELM) et autres ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat du Syndicat national des entreprises des loisirs marchands et autres.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que l'article 38 de la Constitution dispose, dans son premier alinéa, que le Gouvernement peut, pour l'exécution de son programme, demander au Parlement l'autorisation de prendre par ordonnances, pendant un délai limité, des mesures qui sont normalement du domaine de la loi ; que, selon le deuxième alinéa de l'article 38, les ordonnances sont prises en Conseil des ministres après avis du Conseil d'Etat ; que le même alinéa précise qu'elles entrent en vigueur dès leur publication mais deviennent caduques si le projet de loi de ratification n'est pas déposé devant le Parlement avant la date fixée par la loi d'habilitation ; qu'en vertu du troisième alinéa de l'article 38, à l'expiration du délai consenti par la loi d'habilitation, " les ordonnances ne peuvent plus être modifiées que par la loi dans les matières qui sont du domaine législatif " ; <br/>
<br/>
              2.	Considérant qu'il résulte de ces dispositions que si une ordonnance prise sur le fondement de l'article 38 de la Constitution conserve, aussi longtemps qu'elle n'a pas été ratifiée, le caractère d'un acte administratif, celles de ses dispositions qui relèvent du domaine de la loi, si elles peuvent être contestées par voie d'exception à l'occasion de leur application, ne peuvent plus, après l'expiration du délai de l'habilitation conféré au Gouvernement, être modifiées ou abrogées que par le législateur ou sur le fondement d'une nouvelle habilitation qui serait donnée au Gouvernement ; que l'expiration du délai fixé par la loi d'habilitation fait ainsi obstacle à ce que l'autorité investie du pouvoir réglementaire fasse droit à une demande d'abrogation portant sur les dispositions d'une ordonnance relevant du domaine de la loi, quand bien même seraient-elles entachées d'illégalité, sans qu'ait d'incidence à cet égard la circonstance qu'elles ne sont pas, en raison de leur caractère réglementaire, au nombre des dispositions législatives, visées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance du 7 novembre 1958, qui peuvent faire l'objet d'une question prioritaire de constitutionalité ; <br/>
<br/>
              3.	Considérant que les articles L. 212-1 et L. 212-8 du code du sport résultent de l'ordonnance du 23 mai 2006 relative à la partie législative du code du sport, prise en vertu de l'article 84 de la loi du 9 décembre 2004 relative à la simplification du droit ; que cette ordonnance n'a pas été ratifiée ; que les dispositions de ces articles, qui déterminent les titres et diplômes exigés pour pouvoir enseigner, animer ou encadrer contre rémunération une activité physique et sportive ainsi que les sanctions pénales applicables, relèvent du domaine de la loi ; que l'habilitation donnée au pouvoir réglementaire ayant cessé de produire effet, conformément à ce que prévoyait l'article 92 de la loi du 9 décembre 2004, à l'expiration d'un délai de dix-huit mois suivant la publication de cette loi, la demande par laquelle le Syndicat national des entreprises des loisirs marchands et autres ont, le 23 octobre 2015, sollicité l'abrogation des dispositions de ces deux articles ne pouvait, quels qu'en fussent les motifs, être accueillie ; <br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède que le Syndicat national des entreprises des loisirs marchands et autres ne sont pas fondés à demander l'annulation de la décision qu'ils attaquent ; que les conclusions qu'ils ont présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du Syndicat national des entreprises des loisirs marchands et autres est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée au Syndicat national des entreprises des loisirs marchands, à l'Union des navigants et armateurs prestataires professionnels - Syndicat national des armateurs de voiliers et vedettes professionnels, au Syndicat national des entreprises commerciales des conducteurs de chiens attelés pour le loisir, au ministre de la ville, de la jeunesse et des sports et au Premier ministre. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-04-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES LÉGISLATIFS. LOIS D'HABILITATION. - EFFETS DE L'EXPIRATION DU DÉLAI FIXÉ PAR UNE LOI D'HABILITATION (ART. 38 DE LA CONSTITUTION) - IMPOSSIBILITÉ POUR LE POUVOIR RÉGLEMENTAIRE D'ABROGER DES DISPOSITIONS RELEVANT DU DOMAINE DE LA LOI CONTENUE DANS UNE ORDONNANCE - CIRCONSTANCES SANS INCIDENCE - ILLÉGALITÉ DES DISPOSITIONS EN CAUSE [RJ1] - IMPOSSIBILITÉ DE CONTESTER LES DISPOSITIONS PAR LA VOIE DE LA QPC.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. HABILITATIONS LÉGISLATIVES. - EFFETS DE L'EXPIRATION DU DÉLAI FIXÉ PAR UNE LOI D'HABILITATION (ART. 38 DE LA CONSTITUTION) - IMPOSSIBILITÉ POUR LE POUVOIR RÉGLEMENTAIRE D'ABROGER DES DISPOSITIONS RELEVANT DU DOMAINE DE LA LOI CONTENUE DANS UNE ORDONNANCE - CIRCONSTANCES SANS INCIDENCE - ILLÉGALITÉ DES DISPOSITIONS EN CAUSE [RJ1] - IMPOSSIBILITÉ DE CONTESTER LES DISPOSITIONS PAR LA VOIE DE LA QPC.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-04-03-07-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. OBLIGATION D'ABROGER UN RÈGLEMENT ILLÉGAL. - ABSENCE - IMPOSSIBILITÉ POUR LE POUVOIR RÉGLEMENTAIRE, APRÈS L'EXPIRATION DU DÉLAI FIXÉ PAR LA LOI D'HABILITATION, D'ABROGER DES DISPOSITIONS RELEVANT DU DOMAINE DE LA LOI CONTENUES DANS UNE ORDONNANCE PRISE EN APPLICATION DE L'ARTICLE 38 DE LA CONSTITUTION - CIRCONSTANCES SANS INCIDENCE - ILLÉGALITÉ DES DISPOSITIONS EN CAUSE [RJ1] - IMPOSSIBILITÉ DE CONTESTER LES DISPOSITIONS PAR LA VOIE DE LA QPC.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">01-09-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES RÉGLEMENTAIRES. - IMPOSSIBILITÉ POUR LE POUVOIR RÉGLEMENTAIRE, APRÈS L'EXPIRATION DU DÉLAI FIXÉ PAR LA LOI D'HABILITATION, D'ABROGER DES DISPOSITIONS RELEVANT DU DOMAINE DE LA LOI CONTENUES DANS UNE ORDONNANCE PRISE EN APPLICATION DE L'ARTICLE 38 DE LA CONSTITUTION - CIRCONSTANCES SANS INCIDENCE - ILLÉGALITÉ DES DISPOSITIONS EN CAUSE [RJ1] - IMPOSSIBILITÉ DE CONTESTER LES DISPOSITIONS PAR LA VOIE DE LA QPC.
</SCT>
<ANA ID="9A"> 01-01-04-04 Si une ordonnance prise sur le fondement de l'article 38 de la Constitution conserve, aussi longtemps qu'elle n'a pas été ratifiée, le caractère d'un acte administratif, celles de ses dispositions qui relèvent du domaine de la loi, si elles peuvent être contestées par voie d'exception à l'occasion de leur application, ne peuvent plus, après l'expiration du délai de l'habilitation conféré au Gouvernement, être modifiées ou abrogées que par le législateur ou sur le fondement d'une nouvelle habilitation qui serait donnée au Gouvernement. L'expiration du délai fixé par la loi d'habilitation fait ainsi obstacle à ce que l'autorité investie du pouvoir réglementaire fasse droit à une demande d'abrogation portant sur les dispositions d'une ordonnance relevant du domaine de la loi, quand bien même elles seraient entachées d'illégalité, sans qu'ait d'incidence à cet égard la circonstance qu'elles ne sont pas, en raison de leur caractère réglementaire, au nombre des dispositions législatives, visées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, qui peuvent faire l'objet d'une question prioritaire de constitutionalité (QPC).</ANA>
<ANA ID="9B"> 01-02-01-04 Si une ordonnance prise sur le fondement de l'article 38 de la Constitution conserve, aussi longtemps qu'elle n'a pas été ratifiée, le caractère d'un acte administratif, celles de ses dispositions qui relèvent du domaine de la loi, si elles peuvent être contestées par voie d'exception à l'occasion de leur application, ne peuvent plus, après l'expiration du délai de l'habilitation conféré au Gouvernement, être modifiées ou abrogées que par le législateur ou sur le fondement d'une nouvelle habilitation qui serait donnée au Gouvernement. L'expiration du délai fixé par la loi d'habilitation fait ainsi obstacle à ce que l'autorité investie du pouvoir réglementaire fasse droit à une demande d'abrogation portant sur les dispositions d'une ordonnance relevant du domaine de la loi, quand bien même elles seraient entachées d'illégalité, sans qu'ait d'incidence à cet égard la circonstance qu'elles ne sont pas, en raison de leur caractère réglementaire, au nombre des dispositions législatives, visées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, qui peuvent faire l'objet d'une question prioritaire de constitutionalité (QPC).</ANA>
<ANA ID="9C"> 01-04-03-07-06 Si une ordonnance prise sur le fondement de l'article 38 de la Constitution conserve, aussi longtemps qu'elle n'a pas été ratifiée, le caractère d'un acte administratif, celles de ses dispositions qui relèvent du domaine de la loi, si elles peuvent être contestées par voie d'exception à l'occasion de leur application, ne peuvent plus, après l'expiration du délai de l'habilitation conféré au Gouvernement, être modifiées ou abrogées que par le législateur ou sur le fondement d'une nouvelle habilitation qui serait donnée au Gouvernement. L'expiration du délai fixé par la loi d'habilitation fait ainsi obstacle à ce que l'autorité investie du pouvoir réglementaire fasse droit à une demande d'abrogation portant sur les dispositions d'une ordonnance relevant du domaine de la loi, quand bien même elles seraient entachées d'illégalité, sans qu'ait d'incidence à cet égard la circonstance qu'elles ne sont pas, en raison de leur caractère réglementaire, au nombre des dispositions législatives, visées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, qui peuvent faire l'objet d'une question prioritaire de constitutionalité (QPC).</ANA>
<ANA ID="9D"> 01-09-02-01 Si une ordonnance prise sur le fondement de l'article 38 de la Constitution conserve, aussi longtemps qu'elle n'a pas été ratifiée, le caractère d'un acte administratif, celles de ses dispositions qui relèvent du domaine de la loi, si elles peuvent être contestées par voie d'exception à l'occasion de leur application, ne peuvent plus, après l'expiration du délai de l'habilitation conféré au Gouvernement, être modifiées ou abrogées que par le législateur ou sur le fondement d'une nouvelle habilitation qui serait donnée au Gouvernement. L'expiration du délai fixé par la loi d'habilitation fait ainsi obstacle à ce que l'autorité investie du pouvoir réglementaire fasse droit à une demande d'abrogation portant sur les dispositions d'une ordonnance relevant du domaine de la loi, quand bien même elles seraient entachées d'illégalité, sans qu'ait d'incidence à cet égard la circonstance qu'elles ne sont pas, en raison de leur caractère réglementaire, au nombre des dispositions législatives, visées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, qui peuvent faire l'objet d'une question prioritaire de constitutionalité (QPC).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Assemblée, 11 décembre 2006, Conseil national de l'Ordre des médecins, n°s 279517 283983, p. 510.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
