<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065836</ID>
<ANCIEN_ID>JG_L_2020_06_000000440790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/58/CETATEXT000042065836.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/06/2020, 440790, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440790.20200609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la société Sinema demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des articles 2, 3, 7, 10 et 27 du décret n° 2020-548 du 11 mai 2020 ;<br/>
<br/>
              2°) d'enjoindre à l'Etat, sans délai et sous astreinte de 150 euros par jour de retard à compter de l'ordonnance à intervenir, de prendre les mesures suivantes : <br/>
              - autoriser la réouverture administrative de son établissement ;<br/>
              - si la réouverture n'est pas possible, couvrir la marge bénéficiaire réalisée par l'établissement à la même époque les années précédentes ;<br/>
              - si la réouverture est possible mais insuffisamment rentable, couvrir la marge bénéficiaire manquante due aux conditions restrictives imposées par la pandémie et prendre en charge les frais exposés en vue d'instaurer les mesures barrières ;<br/>
              - si le restaurant souhaite diversifier son offre en se lançant dans la restauration à emporter en période de crise, prendre en charge les frais engendrés par le développement de cette activité ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle justifie d'un intérêt à agir dès lors qu'il lui est interdit d'exercer son activité depuis le 14 mars 2020 et que cette interdiction est maintenue depuis le 11 mai 2020 sans qu'aucune date de réouverture ne soit juridiquement prévue ;<br/>
              - la condition d'urgence est satisfaite eu égard, en premier lieu, à l'existence avérée de la pandémie, en deuxième lieu, aux conséquences économiques et patrimoniales de la fermeture de l'établissement et de l'absence de date de réouverture, en troisième lieu, à la nécessité de préserver la vie des entreprises afin qu'elles contribuent au financement du système de santé et, en dernier lieu, à l'absence de motifs de nature à justifier le maintien de la fermeture de son établissement alors que le risque pandémique n'est pas le même pour l'ensemble des personnes infectées ;<br/>
              - il est porté une atteinte grave et manifestement illégale au principe de sécurité juridique, au principe d'égalité devant la loi, au droit de propriété, à la liberté d'entreprendre, au principe de non-discrimination, au droit à la vie et à la liberté d'aller et venir ;<br/>
              - le décret attaqué méconnaît l'objectif de valeur constitutionnelle de prévisibilité et de sécurité juridique en ce qu'il instaure des dispositions imprécises, confuses, évolutives et susceptibles de conduire à une méconnaissance du principe d'égalité devant la loi ;<br/>
              - il porte une atteinte disproportionnée à deux des composantes du droit de propriété dès lors qu'il rend la valeur patrimoniale de son établissement incertaine et la prive de l'usage de cet établissement, et ce bien au-delà de la période de confinement ;<br/>
              - il méconnaît les stipulations de l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors qu'il ne permet pas de préserver la vie biologique et la vie économique de son établissement et de sa clientèle ;<br/>
              - il restreint sa liberté d'aller et venir pour les besoins de son activité ainsi que celle de ses clients depuis le 14 mars 2020 ;<br/>
              - il porte une atteinte manifestement excessive à la liberté d'entreprendre dès lors que, en premier lieu, la décision d'interdiction des activités économiques est disproportionnée au regard de l'objectif de sécurité sanitaire poursuivi, en deuxième lieu, elle a été adoptée sans qu'aucune étude d'impact n'ait mesuré ses conséquences financières et, en dernier lieu, les compensations économiques prévues sont insuffisantes au regard des préjudices subis par le secteur de la restauration ;<br/>
              - il est discriminatoire dans la jouissance de la liberté d'entreprendre et du droit de propriété en ce qu'il maintient la fermeture de son restaurant jusqu'à nouvel ordre, alors que, d'une part, d'autres secteurs d'activité accueillant du public n'ont fait l'objet d'aucune interdiction ou ont rouvert et, d'autre part, dans son propre secteur d'activité, certains établissements, notamment de restauration collective, ne se sont pas vu interdire l'accès ou vont pouvoir accueillir du public pour l'organisation d'épreuves de concours ou d'examens ;<br/>
              - il méconnaît l'article 4 du pacte international relatif aux droits civils et politiques et l'article 30 de la Déclaration universelle des droits de l'homme dès lors que la France n'a pas signalé au secrétaire général de l'Organisation des Nations-Unies la promulgation de l'état d'urgence sanitaire et que les mesures édictées ne respectent pas le principe d'intangibilité des droits fondamentaux, le principe de temporalité et le principe de proportionnalité ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation eu égard, en premier lieu, à l'existence de moyens plus efficaces pour lutter contre la pandémie, en deuxième lieu, à la possibilité d'une régionalisation et d'une spécialisation du confinement, en troisième lieu, aux dommages collatéraux causés par le confinement et les fermetures administratives et, en dernier lieu, à l'interdiction générale et absolue pour les établissements relevant de la catégorie N d'accueillir du public ainsi qu'à la durée excessive de ces interdictions.<br/>
              Par un mémoire en défense, enregistré le 3 juin 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucune atteinte manifestement grave et illégale aux libertés fondamentales invoquées par la requérante n'est caractérisée.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
             - la Constitution ;<br/>
             - la charte des droits fondamentaux de l'Union européenne ;<br/>
             - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
             - le pacte international relatif aux droits civils et politiques ;<br/>
             - le code de la santé publique ;<br/>
             - la loi n° 2020-290 du 23 mars 2020 ;<br/>
             - la loi n° 2020-546 du 11 mai 2020 ;<br/>
             - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
             - le décret n°2020-548 du 11 mai 2020 ;<br/>
             - le décret n° 2020-663 du 31 mai 2020 ;<br/>
             - le code de justice administrative ; <br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 4 juin 2020 à 18 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. La société Sinema demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution des articles 2, 3, 7, 10 et 27 du décret n° 2020-548 du 11 mai 2020, d'autre part, d'enjoindre à l'Etat, sans délai et sous astreinte, d'autoriser la réouverture administrative de son établissement ou, à défaut de réouverture dans des conditions normales, de l'indemniser des frais exposés en vue d'instaurer les mesures barrières, des frais nécessaires à la réorientation de son activité dans la restauration à emporter et des pertes de bénéfices subies par comparaison avec la période équivalente de l'année précédente.<br/>
<br/>
              Sur les circonstances :<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants, élèves et étudiants dans les établissements les recevant et les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par plusieurs arrêtés successifs. <br/>
<br/>
              4. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020 puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Par un décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, plusieurs fois modifié et complété depuis lors, le Premier ministre a réitéré les mesures précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Par un premier décret du 11 mai 2020, applicable les 11 et 12 mai 2020, le Premier ministre a abrogé l'essentiel des mesures précédemment ordonnées par le décret du 23 mars 2020 et en a pris de nouvelles. Par un second décret du 11 mai 2020, pris sur le fondement de la loi du 11 mai 2020 et abrogeant le précédent décret, le Premier ministre a prescrit les nouvelles mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. Enfin, par un décret du 31 mai 2020, abrogeant celui du 11 mai 2020, le Premier ministre a mis fin, à compter du 2 juin 2020, à une partie des mesures restrictives jusqu'alors en vigueur.  <br/>
<br/>
              Sur les demandes adressées au juge des référés :<br/>
<br/>
              5. Par son article 2, le décret du 11 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire classe le territoire des départements et des collectivités de l'article 73 de la Constitution en zone " verte " ou  " rouge " au regard de leur situation sanitaire, déterminée notamment en fonction du nombre de passages aux urgences pour suspicion d'affection au covid-19, du taux d'occupation des lits de réanimation par des patients atteints par le covid-19 et de la capacité de réalisation des tests virologiques sur leur territoire. Le classement de ces collectivités dans l'une ou l'autre de ces zones est annexé au décret. Par son article 3, ce même décret interdit, sauf pour un nombre limité de motifs qu'il précise, tout déplacement de personne la conduisant à la fois à sortir d'un périmètre défini par un rayon de 100 kilomètres de son lieu de résidence et à sortir du département dans lequel ce dernier est situé. Par son article 7, ce décret interdit, sur l'ensemble du territoire de la République, tout rassemblement, réunion ou activité à un titre autre que professionnel sur la voie publique ou dans un lieu ouvert au public, mettant en présence de manière simultanée plus de dix personnes. L'article 10 du même décret fait interdiction à certaines catégories d'établissements recevant du public d'accueillir du public. Il en va notamment ainsi des restaurants et débits de boissons, sauf pour leurs activités de livraison et de vente à emporter, le room service des restaurants et bars d'hôtels et la restauration collective sous contrat. Enfin, l'article 27 donne pouvoir aux préfets de département, lorsque l'évolution de la situation sanitaire le justifie et aux seules fins de lutter contre la propagation du virus, de prendre les mesures plus restrictives que celles énoncées ci-dessus.<br/>
<br/>
              6. La société requérante soutient que ces différentes mesures combinées, en ce qu'elles font obstacle à tout fonctionnement économiquement viable de l'établissement de restauration qu'elle a pour objet d'exploiter, portent une atteinte grave à la liberté d'entreprendre et à la liberté du commerce et de l'industrie, ainsi qu'au droit de propriété, au principe de sécurité juridique, au principe d'égalité devant la loi, au principe de non-discrimination, à la liberté d'aller et venir et au droit à la vie. La société soutient que ces mesures ne sont pas justifiées par la situation sanitaire, ou à tout le moins ne le sont plus à ce jour, de sorte que l'atteinte qu'elles portent à ces libertés fondamentales est disproportionnée et revêt un caractère manifestement illégal. <br/>
<br/>
              7. Il résulte toutefois de l'instruction que par un décret du 31 mai 2020, abrogeant le décret du 11 mai 2020, les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire ont été modifiées. Il a été mis fin, à compter du 2 juin 2020, à la distinction entre départements classés en zone " rouge " et départements classés en zone " verte ". L'article 4 du décret du 31 mai 2020 lui a substitué un classement des départements en zone " verte " ou en zone " orange " au regard de leur situation sanitaire, déterminée notamment en fonction du taux d'incidence de nouveaux cas quotidiens cumulés sur sept jours, du facteur de reproduction du virus, du taux d'occupation des lits de réanimation par des patients atteints par le covid-19, du taux de positivité des tests recueillis trois jours auparavant et du nombre de tests réalisés, ainsi que de la vulnérabilité particulière des territoires concernés. Ce classement, annexé au décret, fait figurer en zone " orange " les départements de la région Ile-de-France ainsi que ceux de la Guyane et de Mayotte, l'ensemble des autres départements étant classés en zone " verte ". Le décret du 31 mai 2020 a également mis fin à la règle limitant les déplacements dans un rayon de 100 kilomètres autour du lieu de résidence. <br/>
<br/>
              8. S'agissant plus particulièrement des cafés et restaurants, il découle de l'article 40 du décret du 31 mai 2020 que les établissements situés en zone " verte " peuvent désormais accueillir du public et reprendre leur activité sous réserve de l'observation de " mesures barrières " telles que le respect d'un maximum de dix clients à une même table, d'une distance minimale d'un mètre entre deux tables, du port du masque pour le personnel et pour les clients lors de leurs déplacements au sein de l'établissement. Seule demeure, pour les établissements situés dans les départements classés en zone " orange ", notamment ceux de la région Ile-de-France, une restriction supplémentaire tenant à ce que l'accueil des clients ne pourra, à tout le moins jusqu'à la date du 22 juin selon les déclarations publiques du Premier ministre, se faire qu'en terrasse et non à l'intérieur de l'établissement.<br/>
              9. Il n'apparaît pas que les règles nouvelles auxquelles sont soumis, à compter du 2 juin, les cafés et restaurants situés dans les départements classés en zone " verte ", qui se bornent à imposer à ces établissement des restrictions découlant du respect des mesures d'hygiène et de distanciation sociale qui doivent être observées, aux termes de l'article 1er du décret du 31 mai 2020, " en tout lieu et en toute circonstance " en vue de lutter contre la propagation du virus, dont la circulation n'a pas à ce jour cessé dans cette zone, portent aux entreprises concernées une atteinte grave et manifestement illégale à une liberté fondamentale. <br/>
<br/>
              10. S'agissant des établissements situés dans les départements désormais classés en zone " orange ", au sens de l'article 4 du décret du 31 mai 2020, notamment ceux de la région Ile-de-France, il résulte des éléments produits par le ministre en défense que le maintien de restrictions plus importantes dans certaines parties du territoire national est justifié par des facteurs objectifs tels que le taux d'incidence - nombre de nouveau cas apparus en une semaine pour 100 000 habitants - constaté et le taux d'occupation des services de réanimation. Ces indicateurs traduisent l'existence d'un niveau de circulation du virus, dans les départements concernés, supérieur à ce qui est observé sur le reste du territoire. Compte tenu de la densité de population dans les départements d'Ile-de-France et de l'existence d'un réseau de transports publics particulièrement dense permettant une circulation aisée des personnes dans toute la région, il apparaît nécessaire d'adopter des mesures cohérentes à l'échelle de la région. Il ressort par ailleurs des recommandations du Haut conseil de la santé publique que le risque de transmission du virus est moindre en milieu ouvert que dans les espaces fermés. Dans ces conditions, alors même que cette mesure aboutit, pour les entreprises dont les établissements ne disposent pas d'une terrasse ou de la possibilité de bénéficier d'une autorisation temporaire d'occupation du domaine public, au maintien d'une restriction de leur activité à la vente à emporter et alors même que les mesures de soutien économique mises en place ne permettraient pas de compenser le préjudice subi par ces entreprises, il n'apparaît pas en l'état de l'instruction que l'atteinte ainsi portée à la liberté du commerce et de l'industrie, à la liberté d'entreprendre ou, en tout état de cause, aux autres droits et libertés invoqués revêtirait un caractère manifestement illégal.<br/>
<br/>
              11. Il résulte de tout ce qui précède qu'il n'y a plus lieu, compte tenu de son abrogation, de statuer sur les conclusions tendant à la suspension de l'exécution du décret du 11 mai 2020. S'agissant des autres conclusions, qui doivent être lues comme tendant à ce qu'il soit enjoint à l'Etat de prendre, pour ce qui concerne les cafés et restaurants, des mesures moins restrictives que celles qui découlent de l'article 40 du décret du 31 mai 2020, elles ne peuvent, à la date de la présente ordonnance, qu'être rejetées. <br/>
<br/>
              12. Enfin, il n'entre pas dans l'office du juge des référés, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de statuer sur les demandes tendant à l'indemnisation du préjudice que la requérante estime avoir subi du fait des mesures de police administrative dont elle a fait l'objet.   <br/>
<br/>
              13. Les demandes de la société Sinema ne peuvent ainsi qu'être rejetées, y compris celles tendant à la mise en oeuvre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions tendant à la suspension du décret du 11 mai 2020.<br/>
Article 2 : Le surplus de la requête de la société Sinema est rejeté.<br/>
Article 3 : La présente ordonnance sera notifiée à la société Sinema et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
