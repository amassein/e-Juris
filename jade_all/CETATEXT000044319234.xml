<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044319234</ID>
<ANCIEN_ID>JG_L_2021_11_000000453907</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/92/CETATEXT000044319234.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/11/2021, 453907, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453907</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA-PRIGENT-DRUSCH ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Frédéric Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:453907.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              La société Méridionale de Coordination (Somerco) a demandé au tribunal administratif de Nice de condamner le centre hospitalier de Cannes à lui verser, à titre principal, les sommes de 925 243,88 euros toutes taxes comprises (TTC) et de 20 606,23 euros TTC en règlement respectivement des prestations prévues par le marché initial et le marché complémentaire conclus entre les parties, relatifs à la mission d'ordonnancement, de pilotage et de coordination des travaux de restructuration d'un ensemble immobilier, résiliés le 2 mars 2009, et, à titre subsidiaire, de condamner cet établissement public à lui verser les sommes de, respectivement, 413 068,48 euros TTC et 20 606,23 euros TTC en règlement des mêmes prestations. Par un jugement n° 1102407 du 10 juin 2016, le tribunal a condamné le centre hospitalier de Cannes à verser à la société Somerco la somme de 27 134,24 euros TTC et rejeté le surplus de sa demande indemnitaire.<br/>
<br/>
              Par un arrêt avant dire droit n° 16MA03553 du 28 janvier 2019, la cour administrative d'appel de Marseille a, sur appel de la société Somerco, jugé que la décision de résiliation contestée était infondée, ordonné qu'il soit procédé à une expertise en vue d'obtenir les éléments lui permettant de statuer sur cinq des chefs de préjudice invoqués par la société au titre de travaux supplémentaires, rejeté les conclusions de celle-ci tendant à l'indemnisation du préjudice subi, d'une part, au titre des surcoûts correspondant au personnel supplémentaire et, d'autre part, au titre de l'exécution du marché complémentaire conclu entre elle et le centre hospitalier de Cannes le 4 août 2006, et réformé le jugement en ce qu'il avait de contraire à cet arrêt.<br/>
<br/>
              Par un second arrêt n° 16MA03553 du 26 avril 2021, la cour administrative d'appel de Marseille a réformé le jugement du tribunal administratif de Nice en portant le montant des condamnations prononcées à l'encontre du centre hospitalier de Cannes de la somme de 27 134,24 euros TTC à celle de 416 760,48 euros TTC, assortie des intérêts moratoires au taux légal à compter du 9 novembre 2009 et mis les frais d'expertise à la charge du centre hospitalier de Cannes. <br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 453907, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juin et 6 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Cannes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt avant dire droit du 28 janvier 2019 ;<br/>
<br/>
              2°) de mettre à la charge de la société Somerco la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 453914, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juin et 6 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Cannes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 26 avril 2021 ;<br/>
<br/>
              2°) de mettre à la charge de la société Somerco la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              3° Sous le n° 456330, par une requête, enregistrée le 6 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Cannes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner le sursis à exécution de l'arrêt du 26 avril 2021 ;<br/>
<br/>
              2°) de mettre à la charge de M. B..., en sa qualité de liquidateur amiable de la société Somerco, la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le décret n° 78-1306 du 26 décembre 1978 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Gueudar Delahaye, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Melka-Prigent-Drusch, avocat du centre hospitalier de Cannes ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois et la requête visés ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              Sur le pourvoi enregistré sous le n° 453907 : <br/>
<br/>
              3. Pour demander l'annulation de l'arrêt du 28 janvier 2019 qu'il attaque, le centre hospitalier de Cannes soutient que la cour administrative d'appel de Marseille a :<br/>
              - inexactement qualifié les faits en jugeant que la résiliation du contrat, prononcée aux torts exclusifs de la société Somerco, n'était pas justifiée ;<br/>
              - commis une erreur de droit en écartant la possibilité de procéder, devant le juge, à une substitution de motif justifiant la décision de résiliation ;<br/>
              - commis une erreur de droit, d'une part, en statuant ultra petita en accordant une indemnisation au titre des fiches de travaux modificatifs (FTM) qui n'était pas demandée par la société Somerco et, d'autre part, dès lors qu'une telle demande aurait été, en tout état de cause, nouvelle en appel. <br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur le pourvoi enregistré sous le n° 453914 :<br/>
<br/>
              5. Pour demander l'annulation de l'arrêt du 26 avril 2021 qu'il attaque, le centre hospitalier de Cannes soutient que la cour administrative d'appel de Marseille a :<br/>
              - statué ultra petita en accordant à la société Somerco une somme de 61 979,19 euros HT, correspondant au solde du marché initial, que cette société n'avait pas sollicitée ;<br/>
              - statué à tort sur les conclusions relatives à l'indemnisation de l'intégration des FTM qui étaient irrecevables faute d'avoir été chiffrées dans le mémoire de réclamation, d'une part, et pour avoir été présentées pour la première fois en appel, d'autre part ;<br/>
              - statué à tort sur les demandes d'indemnités de résiliation du contrat qui étaient irrecevables dès lors qu'elles n'ont pas été présentées dans le recours devant les premiers juges et qu'elles sont fondées sur un chef de préjudice distinct des autres demandes. <br/>
<br/>
              6. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué du 26 avril 2021 en tant qu'il s'est prononcé sur la condamnation du centre hospitalier de Cannes au versement d'une somme de 61 979,19 euros HT correspondant au solde du marché initial. En revanche, s'agissant des autres conclusions dirigées contre cet arrêt, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions.<br/>
<br/>
              Sur la requête enregistrée sous le n° 456330 :<br/>
<br/>
              7. Aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond. ".<br/>
<br/>
              8. D'une part, le moyen tiré de ce que la cour administrative d'appel de Marseille aurait statué ultra petita en accordant à la société Somerco une somme de 61 979,19 euros HT, correspondant au solde du marché initial, que cette société n'avait pas sollicitée paraît, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de l'arrêt attaqué, l'infirmation de la solution retenue par les juges du fond. Compte tenu de ce que cette société est en liquidation amiable, l'exécution de l'arrêt attaqué, en tant qu'il condamne le centre hospitalier à payer cette somme, exposerait cet établissement à la perte définitive de sommes qui ne devraient pas rester à sa charge au cas où ses conclusions tendant à l'annulation de l'arrêt seraient reconnues fondées par le Conseil d'Etat. L'exécution de cet arrêt risque ainsi, dans cette mesure, d'entraîner des conséquences difficilement réparables pour le requérant. <br/>
<br/>
              9. Dans ces conditions, il y a lieu d'ordonner le sursis à exécution de l'arrêt de la cour administrative d'appel de Marseille du 26 avril 2021 en tant qu'il a condamné le centre hospitalier de Cannes à verser à la société Somerco une somme de 61 979,19 euros HT correspondant au solde du marché initial. <br/>
<br/>
              10. D'autre part, il résulte de ce qui a été au point 6 que le surplus des conclusions du pourvoi formé par le centre hospitalier de Cannes contre l'arrêt du 26 avril 2021 de la cour administrative d'appel de Marseille n'est pas admis. Par suite, le surplus des conclusions à fin de sursis de cet arrêt est devenu sans objet. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le centre hospitalier de Cannes sous le n° 456330 au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du centre hospitalier de Cannes enregistré sous le n° 453907 n'est pas admis.<br/>
Article 2 : Les conclusions du pourvoi du centre hospitalier de Cannes enregistré sous le n° 453914 qui sont dirigées contre l'arrêt de la cour administrative d'appel de Marseille du 26 avril 2021 en tant qu'il a condamné le centre hospitalier à verser à la société Somerco une somme de 61 979,19 euros HT correspondant au solde du marché initial sont admises.<br/>
Article 3 : Le surplus des conclusions du pourvoi du centre hospitalier de Cannes enregistré sous le n° 453914 n'est pas admis.<br/>
Article 4 : Jusqu'à ce qu'il ait été statué sur les conclusions admises du pourvoi n° 453914 du centre hospitalier de Cannes contre l'arrêt de la cour administrative d'appel de Marseille du 26 avril 2021, il sera sursis à l'exécution de cet arrêt en tant qu'il a condamné le centre hospitalier à verser à la société Somerco une somme de 61 979,19 euros HT correspondant au solde du marché initial.<br/>
Article 5 : Les conclusions présentées par le centre hospitalier de Cannes sous le n° 456330 au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : Il n'y a pas lieu de statuer sur le surplus des conclusions de la requête du centre hospitalier de Cannes enregistrée sous le n° 456330.<br/>
Article 7 : La présente décision sera notifiée au centre hospitalier de Cannes et à la société méridionale de coordination.<br/>
              Délibéré à l'issue de la séance du 21 octobre 2021 où siégeaient : M. Olivier Japiot, président de chambre, présidant ; M. Gilles Pellissier, conseiller d'Etat et M. Frédéric Gueudar Delahaye, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 10 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Olivier Japiot<br/>
 		Le rapporteur : <br/>
      Signé : M. Frédéric Gueudar Delahaye<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
