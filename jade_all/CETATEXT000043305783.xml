<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043305783</ID>
<ANCIEN_ID>JG_L_2021_03_000000450656</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/30/57/CETATEXT000043305783.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/03/2021, 450656, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450656</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450656.20210313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mmes A... B... F..., C... D... et E... G... ont demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre au préfet des Alpes-Maritimes de produire l'avis de l'agence régionale de santé (ARS) sur lequel se fonde l'arrêté n° 2021-329 du 11 mars 2021 portant restriction de déplacement dans certaines communes du département des Alpes-Maritimes et à l'ARS de produire divers éléments, notamment les données et les méthodes de calcul relatives aux indicateurs publiés, d'autre part, d'ordonner la suspension de cet arrêté. Par une ordonnance n° 2101389 du 12 mars 2021, le juge des référés du tribunal administratif de Nice a rejeté leur demande.<br/>
<br/>
              Par une requête enregistrée le 13 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mmes B... F..., D... et G... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leur demande ;<br/>
<br/>
              3°) mettre à la charge de l'Etat la somme de 1 euro à verser à chacune d'elles au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - le juge des référés du tribunal administratif de Nice a entaché son ordonnance d'une insuffisance de motivation et d'une erreur de droit en jugeant qu'il ne lui appartenait pas de contrôler l'existence et le bien-fondé des méthodes statistiques mises en oeuvre par l'administration ;<br/>
              - seule une mesure d'instruction leur aurait permis de contester sérieusement les chiffres avancés par l'administration ;<br/>
              - la décision du juge des référés porte atteinte à leur droit à un recours effectif ;<br/>
              - l'arrêté du préfet méconnaît les principes de sécurité juridique et de prévisibilité de la norme ; <br/>
              - il n'est pas établi que les mesures contestées sont adaptées, nécessaires et proportionnées.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1262 du 16 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisis en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure qu'il a diligentée. <br/>
<br/>
              2. Une nouvelle progression de l'épidémie de covid-19 sur le territoire national a conduit le Président de la République à prendre, le 14 octobre 2020, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre 2020 sur l'ensemble du territoire de la République. L'état d'urgence sanitaire a été prorogé jusqu'au 1er juin 2021 par la loi n° 2021-160 du 15 février 2021. Les 16 et 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, deux décrets prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19. <br/>
<br/>
              3. En raison de la dégradation particulièrement préoccupante de la situation sanitaire dans certains départements, l'article 4 du décret du 29 octobre 2020 a été modifié par le décret n° 2021-217 du 25 février 2021 puis par le décret n° 2021-248 du 4 mars 2021. Aux termes du II de cet article 4 dans sa rédaction en vigueur : " II. Dans les départements mentionnés à l'annexe 2, le préfet de département interdit, dans les zones qu'il définit, aux seules fins de lutter contre la propagation du virus, tout déplacement de personne hors de son lieu de résidence les samedi et dimanche entre 6 heures et 18 heures à l'exception des déplacements pour les motifs mentionnés au I et les motifs suivants, en évitant tout regroupement de personnes : / 1° Déplacements pour effectuer des achats de fournitures nécessaires à l'activité professionnelle, des achats de première nécessité, des retraits de commandes, des livraisons à domicile, ainsi que pour les déménagements ; / 2° Déplacements brefs, dans la limite d'une heure quotidienne et dans un rayon maximal de cinq kilomètres autour du domicile, liés soit à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective et de toute proximité avec d'autres personnes, soit à la promenade avec les seules personnes regroupées dans un même domicile ; / 3° Déplacements pour se rendre dans un service public, pour un acte ou une démarche qui ne peuvent être réalisés à distance ; / 4° Déplacements à destination ou en provenance d'un lieu de culte ; / 5° Participation à des rassemblements, réunions ou activités sur la voie publique ou dans un lieu ouvert au public qui ne sont pas interdits en application de l'article 3 ". Les départements mentionnés à l'annexe 2 sont les Alpes-Maritimes, le Nord et le Pas-de-Calais.<br/>
<br/>
              4. Par l'arrêté n° 2021-329 du 11 mars 2021 portant restriction de déplacement dans certaines communes du département des Alpes-Maritimes, pris sur le fondement des dispositions citées au point 4 ci-dessus, le préfet des Alpes-Maritimes a interdit, dans les communes mentionnées à l'annexe 1 de cet arrêté, tout déplacement de personnes hors de leur lieu de résidence les samedi 13 et dimanche 14 mars entre 6 heures et 18 heures, à l'exception des déplacements pour les motifs prévus par les mêmes dispositions. Mmes A... B... F..., C... D... et E... G... font appel de l'ordonnance du 12 mars 2021 par laquelle le juge des référés du tribunal administratif de Nice a rejeté leur demande, présentée sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à la production de divers documents et données chiffrées, ainsi qu'à la suspension de l'exécution de cet arrêté.<br/>
<br/>
              5. Par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nice a relevé qu'il résulte de l'instruction et des données discutées lors de l'audience qu'il a organisée le 12 mars 2021 que le département des Alpes-Maritimes a l'un des taux d'incidence les plus élevés de France métropolitaine, avec un taux de 484 pour 100 000 habitants alors que le moyenne nationale d'élève à 217, que le taux de positivité constaté dans ce département s'établit à 10,2 % pour une moyenne nationale de 7,3 %, que la part du variant anglais du coronavirus représente 82 % des nouvelles contaminations dans le département et enfin que le taux d'occupation des lits de réanimation est de 132,2 %, 12 patients ayant dû être évacués vers des établissements d'autres départements.<br/>
<br/>
              6. En premier lieu, les requérantes ne contestent pas la circonstance, relevée par le juge des référés du tribunal administratif de Nice, que les données relevées quotidiennement par l'agence régionale de santé (ARS), notamment les taux d'incidence, de positivité et d'occupation des équipements de réanimation sont rendues publiques sur plusieurs sites internet, dont celui de l'ARS et celui de Santé Publique France. En l'absence de tout doute raisonnable sur l'exactitude des données ainsi rendues publiques, les requérantes ne sont pas fondées à soutenir que le premier juge aurait méconnu leur droit à un recours effectif en refusant d'enjoindre à l'ARS de produire, comme elles le demandaient, " l'ensemble des données collectées dans le mois écoulé, la méthode de remontée des données tant pour les hospitalisations que pour les tests, la méthode de vérification interne des chiffres remontés et de correction des éventuelles erreurs, la méthode de ventilation entre le cas retenu comme covid et de ceux des cas relevant d'autres comorbidités (...), la méthode de compilation des chiffres définitifs déclarés à la presse et l'éventuelle péréquation au regard (...) du surnombre ou du sous-nombre de personnes testées sur la zone retenue (...) et la méthode de différenciation ou de comparaison entre les départements de la région, mais aussi au plan national ".<br/>
<br/>
              7. En deuxième lieu, si l'arrêté du 26 février 2021 du préfet des Alpes-Maritimes limitant les déplacements au cours des week-ends précédents était motivé par le fait que les déplacements étaient importants dans la zone littorale du département au cours de la période des vacances scolaires, cette circonstance n'implique pas que des mesures analogues ne pouvaient, sans méconnaître les principes de sécurité juridique et de prévisibilité de la norme, être adoptées pour la période suivante.<br/>
<br/>
              8. En dernier lieu, les requérantes n'apportent aucun élément susceptible de remettre en cause l'appréciation portée par le juge des référés du tribunal administratif de Nice qui, par une ordonnance suffisamment motivée, a estimé que les mesures prises par le préfet des Alpes-Maritimes aux seules fins de lutter contre la propagation du virus sont nécessaires, proportionnées et adaptées aux circonstances locales et en a déduit, sans se prononcer sur la condition d'urgence, que l'arrêté contesté ne portait pas une atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
<br/>
              9. Il résulte de ce qui précède qu'il est manifeste que l'appel de Mme B... F... et autres ne peut être accueilli. La requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit par suite être rejetée, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme B... F... et autres est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme A... B... F..., première requérante dénommée.<br/>
Copie en sera adressée au préfet des Alpes-Maritimes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
