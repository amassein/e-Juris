<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029069578</ID>
<ANCIEN_ID>JG_L_2014_06_000000362903</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/06/95/CETATEXT000029069578.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 11/06/2014, 362903, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362903</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:362903.20140611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 362903, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 septembre et 19 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 827 du 3 avril 2012, telle qu'elle lui a été notifiée le 18 juillet 2012, par laquelle le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en matière disciplinaire, l'a reconnu coupable des faits qui lui étaient reprochés et a prononcé à son encontre l'interdiction d'exercer toute fonction d'enseignement et de recherche au sein de l'université de Reims pour une durée de deux ans, avec suppression de la totalité du traitement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 363028, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 septembre et 19 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 827 du 3 avril 2012, telle qu'elle lui a été notifiée le 26 juillet 2012, par laquelle le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en matière disciplinaire, l'a reconnu coupable des faits qui lui étaient reprochés et a prononcé l'interdiction d'exercer toute fonction d'enseignement et de recherche au sein de tout établissement d'enseignement supérieur pour une durée de deux ans, avec suppression de la totalité du traitement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois n° 362903 et 363028 visés ci-dessus sont dirigés contre la même décision du Conseil national de l'enseignement supérieur et de la recherche (CNESER) en date du 3 avril 2012 prise dans les versions qui ont été notifiées au requérant successivement les 18 et 26 juillet 2012 ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que, par une décision du 15 avril 2011, la section disciplinaire de l'université de Reims a infligé à M.B..., professeur des universités, la sanction de l'interdiction de toutes fonctions d'enseignement et de recherche dans tout établissement public d'enseignement supérieur pendant deux ans ; que, saisi en appel par l'intéressé, le CNESER, statuant en matière disciplinaire, a, par une décision du 3 avril 2012, notifiée au requérant le 2 juillet 2012, ramené la sanction à l'interdiction d'exercer toute fonction d'enseignement et de recherche au sein de l'université de Reims pendant deux ans avec sursis, avec suppression de la totalité du traitement ; que, toutefois, le conseil national, invoquant des " erreurs de frappe " affectant cette décision, en a notifié au requérant, le 18 juillet 2012, une nouvelle version présentée comme " remplaçant et annulant la précédente ", dont le dispositif reprend la même sanction en supprimant le sursis ; que le conseil national, toujours au motif d'erreurs de frappe, a notifié à ce dernier, le 26 juillet 2012, une troisième version de la décision initiale, prononçant à son encontre l'interdiction d'exercer toute fonction d'enseignement et de recherche pour une durée de deux ans dans tout établissement public d'enseignement supérieur, et non plus seulement au sein de l'université de Reims, avec suppression de la totalité du traitement, non assortie du sursis ; que M. B...demande l'annulation de la décision du 3 avril 2012 dans les versions qui lui ont été notifiées les 18 et 26 juillet 2012 ;<br/>
<br/>
              3. Considérant que le CNESER n'a pu, sans méconnaître l'autorité de la chose jugée par lui-même lors de sa délibération du 3 avril 2012, modifier à deux reprises successives le dispositif de sa décision initiale ; que, par suite, le requérant est fondé à demander l'annulation de la décision du 3 avril 2012 telle qu'elle lui a été notifiée les 18 et 26 juillet 2012 ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du Conseil national de l'enseignement supérieur et de la recherche du 3 avril 2012, telle qu'elle a été notifiée à M. B...les 18 et 26 juillet 2012, est annulée.<br/>
<br/>
Article 2 : L'Etat versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
