<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040534</ID>
<ANCIEN_ID>JG_L_2020_06_000000425965</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040534.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/06/2020, 425965, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425965</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:425965.20200622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 10 mai 2019, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la société Sport Player Kart, dirigées contre l'arrêt n° 16MA03178 du 4 octobre 2018 de la cour administrative d'appel de Marseille en tant seulement que cet arrêt s'est prononcé sur les pénalités pour manquement délibéré liées aux rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie pour la période du 1er janvier 2009 au 30 septembre 2012 et les conséquences de l'application de ces pénalités sur le délai de reprise de l'administration. <br/>
<br/>
              Par un mémoire en défense, enregistré le 19 juillet 2019, le ministre de l'action et des comptes publics conclut au rejet du pourvoi. Il soutient que les moyens soulevés par la société requérante ne sont pas fondés.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 22 mai 2020, la société Sport Player Kart persiste dans les conclusions de son pourvoi. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Sport Player Kart ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Sport Player Kart, qui exerce une activité de parc de loisirs et d'attractions dans la commune de Hyères (Var), a fait l'objet d'une vérification de comptabilité relative à ses déclarations de taxe sur la valeur ajoutée (TVA) correspondant à la période du 1er janvier 2009 au 30 septembre 2012. A l'issue de cette vérification, l'administration fiscale a considéré que les recettes " parc enfant " de la société et ses recettes liées à son forfait " Speedoland " ne pouvaient être soumises au taux réduit de TVA de 5,5 % prévu à l'article 279 du code général des impôts dans sa version applicable au litige et que le taux normal de TVA aurait dû leur être appliqué. Par deux propositions de rectification du 21 décembre 2012 et du 15 février 2013, l'administration a notifié à la société des rappels de taxe sur la valeur ajoutée assortis d'intérêts de retard et de la majoration de 40 % pour manquement délibéré prévue à l'article 1729 du code général des impôts. Après avoir vainement contesté ces redressements, la société Sport Player Kart a porté le litige devant le tribunal administratif de Toulon qui, par un jugement du 9 juin 2016, a rejeté sa demande de décharge des rappels de taxe sur la valeur ajoutée et des pénalités correspondantes. Sur appel de la société, la cour administrative d'appel de Marseille a, par un arrêt du 4 octobre 2018 contre lequel la société s'est pourvue en cassation, confirmé ce jugement. Les conclusions de ce pourvoi ont fait l'objet d'une admission par une décision du Conseil d'Etat, statuant au contentieux du 10 mai 2019, en tant seulement qu'elles portent sur les pénalités de 40 % pour manquement délibéré de la part de la société et les conséquences de l'application de ces pénalités sur le délai de reprise de l'administration.<br/>
<br/>
              2. Aux termes de l'article 1729 du code général des impôts : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'Etat entraînent l'application d'une majoration de : / a. 40 % en cas de manquement délibéré ; (...) ". Aux termes de l'article L. 195 A du livre des procédures fiscales : " En cas de contestation des pénalités fiscales appliquées à un contribuable au titre des impôts directs, de la taxe sur la valeur ajoutée et des autres taxes sur le chiffre d'affaires, des droits d'enregistrement, de la taxe de publicité foncière et du droit de timbre, la preuve de la mauvaise foi et des manoeuvres frauduleuses incombe à l'administration ". Enfin, il résulte du deuxième alinéa de l'article L. 176 du même livre, dans sa rédaction alors applicable, que les contribuables vérifiés qui ont été assujettis à des suppléments d'imposition majorés de pénalités pour manquement délibéré ne peuvent bénéficier du délai de reprise réduit à deux ans accordé notamment aux adhérents d'un centre de gestion agréé.<br/>
<br/>
              3. Il résulte des dispositions citées ci-dessus au point 2 qu'en jugeant que l'administration fiscale avait suffisamment établi l'intention de la société Sport Player Kart d'éluder l'impôt au seul motif qu'elle avait, au titre des années 2000 et 2001, déjà effectué des rectifications en matière de taux réduit de taxe sur la valeur ajoutée concernant l'exploitation individuelle par M. A..., aujourd'hui gérant et unique associé de la société Sport Player Kart, de la même activité, et que la légalité de ces premiers redressements avait été confirmée par une décision du Conseil d'Etat, statuant au contentieux du 23 juin 2014, alors que cette dernière décision est postérieure aux déclarations de TVA de la société pour la période litigieuse, allant du 1er janvier 2009 au 30 septembre 2012, la cour administrative d'appel de Marseille a commis une erreur de droit. Par suite, la société Sport Player Kart est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il s'est prononcé sur les pénalités pour manquement délibéré qui ont été mises à sa charge et, par suite, sur les conséquences de l'application de ces pénalités sur le délai de reprise de l'administration prévu à l'article L. 176 du livre des procédures fiscales. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1500 euros à verser à la société Sport Player Kart au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 4 octobre 2018 est annulé en tant qu'il a statué sur les conclusions tendant à la décharge des pénalités pour manquement délibéré dont ont été assortis les rappels de taxe sur la valeur ajoutée mis à la charge de la société Sport Player Kart au titre de la période du 1er janvier 2009 au 30 septembre 2012 et sur les conséquences de l'application de ces pénalités sur le délai de reprise de l'administration. <br/>
<br/>
Article 2 : L'affaire est, dans cette mesure, renvoyée à la cour administrative d'appel de Marseille. <br/>
<br/>
Article 3 : L'Etat versera à la société Sport Player Kart une somme de 1500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Sport Player Kart et au ministre de l'action et des comptes publics. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
