<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030296249</ID>
<ANCIEN_ID>JG_L_2015_02_000000361073</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/29/62/CETATEXT000030296249.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 27/02/2015, 361073, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361073</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:361073.20150227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n°361073 du 24 avril 2013, le Conseil d'État, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. A...B...dirigées contre l'arrêt n° 10BX00585 du 15 mai 2012 de la cour administrative d'appel de Bordeaux en tant qu'il s'est prononcé sur les cotisations supplémentaires d'impôt sur le revenu mises à sa charge et résultant, au titre de l'année 1998, de la réintégration d'une dette injustifiée inscrite au passif du bilan au nom de Renault Agriculture et, au titre de l'année 1999, de la réintégration des sommes correspondant à la renonciation à recettes consentie au profit de la commune de Saint-Geniès, ainsi que du rappel de taxe sur la valeur ajoutée ayant grevé les recettes auxquelles il a été renoncé ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. En vertu de l'article 38 du code général des impôts, le bénéfice imposable est celui qui provient des opérations de toute nature faites par l'entreprise, à l'exception de celles qui, en raison de leur objet ou de leurs modalités, sont étrangères à une gestion commerciale normale. Les renonciations à recettes et abandons de créances consentis par une entreprise au profit d'un tiers ne relèvent pas, en règle générale, d'une gestion commerciale normale, sauf s'il apparaît qu'en consentant de tels avantages, l'entreprise a agi dans son propre intérêt. S'il appartient à l'administration d'apporter la preuve des faits sur lesquels elle se fonde pour estimer que les avantages octroyés par une entreprise à un tiers constituent un acte anormal de gestion, elle est réputée apporter cette preuve dès lors que cette entreprise n'est pas en mesure de justifier qu'elle a bénéficié en retour de contreparties. Dans l'hypothèse où l'entreprise s'acquitte de cette obligation, il incombe ensuite à l'administration d'apporter la preuve que cet avantage est, contrairement à ce que soutient l'entreprise, dépourvu de contrepartie, qu'il a une contrepartie dépourvue d'intérêt pour l'entreprise ou que la rémunération de cette contrepartie est excessive.<br/>
<br/>
              Sur le redressement en matière d'impôt sur le revenu consécutif à la réintégration d'une dette injustifiée inscrite au passif du bilan au nom de Renault Agriculture :<br/>
<br/>
              2. M.B..., exploitant, à titre individuel, d'une entreprise de travaux agricoles et de travaux publics, fait valoir qu'il a payé par chèque sur un compte personnel l'achat d'une moissonneuse à Renault Agriculture et que c'est en raison d'une erreur comptable que ce paiement n'a pas été porté dans les écritures comptables de l'exploitation au 31 décembre 1998. En jugeant que ce paiement sur un compte personnel était sans incidence sur le bien-fondé du redressement, dès lors que le requérant n'apportait pas d'autre justification à l'absence d'inscription de ce paiement dans les écritures comptables de l'exploitation au 31 décembre 1998, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis. Elle n'a pas commis d'erreur de droit en jugeant que l'administration était fondée à réintégrer dans les résultats de l'entreprise de M. B... cette dette injustifiée à l'égard de Renault Agriculture inscrite au passif du bilan.<br/>
<br/>
              Sur le redressement en matière d'impôt sur le revenu et le rappel de taxe sur la valeur ajoutée consécutifs à la réintégration d'une renonciation à recettes consentie au profit de la commune de Saint-Geniès :<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que l'entreprise de M. B... a facturé à la commune de Saint-Geniès une somme de 144 000 francs correspondant au goudronnage de deux routes, pour une surface totale de 9 000 m², alors que la société Eurovia, à qui ces travaux ont été sous-traités, a facturé à l'entreprise un montant de 160 000 francs correspondant à une surface de 10 000 m². L'administration fiscale en a déduit qu'en ne refacturant pas l'intégralité des travaux dont elle a supporté la charge, l'entreprise de M. B...s'était privée, sans contrepartie de la part de la commune, de la recette correspondant au goudronnage d'une superficie de 1 000 m².<br/>
<br/>
              4. Si M. B...soutient que la contrepartie de la renonciation à recettes litigieuse résidait dans le maintien de bonnes relations avec le client important que constituait la commune de Saint-Geniès, il n'est pas fondé à soutenir, par les moyens qu'il invoque, que la cour administrative d'appel de Bordeaux, qui a suffisamment motivé son arrêt sur ce point, aurait dénaturé les faits et commis une erreur de droit en jugeant qu'il n'apportait pas la preuve de l'existence d'une contrepartie à cette renonciation à recettes et que l'administration avait, dès lors, pu à bon droit regarder cet avantage comme un acte anormal de gestion et, en conséquence, en réintégrer le montant dans ses revenus, dans la catégorie des bénéfices industriels et commerciaux. Par suite, ces conclusions du pourvoi ne peuvent qu'être rejetées. <br/>
<br/>
              5. Il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt de la cour en tant qu'il statue, d'une part, sur les cotisations supplémentaires d'impôt sur le revenu résultant, au titre de l'année 1998, de la réintégration d'une dette injustifiée inscrite au passif du bilan au nom de Renault Agriculture, et, au titre de l'année 1999, de la réintégration d'une renonciation à recettes consentie au profit de la commune de Saint-Geniès et, d'autre part, sur le rappel de taxe sur la valeur ajoutée ayant grevé les recettes auxquelles l'entreprise de M. B...a renoncé.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
