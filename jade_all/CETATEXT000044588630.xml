<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044588630</ID>
<ANCIEN_ID>JG_L_2021_12_000000459222</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/58/86/CETATEXT000044588630.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/12/2021, 459222, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459222</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459222.20211216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 7 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la levée du secret défense sur la décision annoncée le 7 décembre 2021 prévoyant dans les écoles primaires l'obligation du port du masque en extérieur, l'obligation du port du masque en extérieur et en intérieur pour les activités sportives et la distanciation pour la prise des repas en évitant le croisement des classes ;<br/>
<br/>
              2°) d'enjoindre la levée de l'obligation du port du masque en extérieur pour les élèves des établissements scolaires de maternelle et d'élémentaire sans délai, y compris durant les temps de récréation et d'activités sportives ;<br/>
<br/>
              3°) d'enjoindre la levée de l'obligation de distanciation sociale pour les élèves des établissements scolaires de maternelle et d'élémentaire lors des périodes de restauration.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de sa requête ;<br/>
              - elle justifie d'un intérêt à agir ;<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, la décision contestée s'applique à compter du 9 décembre 2021, en deuxième lieu, elle méconnaît plusieurs des libertés fondamentales des enfants scolarisés en école maternelle et élémentaire et, en troisième lieu, l'augmentation du taux d'incidence des enfants de 0 à 9 ans est liée à l'augmentation du nombre de tests effectués ;<br/>
              - il est porté une atteinte grave et manifestement illégale aux libertés fondamentale ; <br/>
              - la décision contestée porte une atteinte grave à l'intérêt supérieur de l'enfant et à la liberté d'aller et venir dès lors que, en premier lieu, les enfants ont peu de risque de développer des formes graves de la Covid-19, en deuxième lieu, le port du masque provoque des souffrances physiques et psychologiques chez les enfants et retarde l'acquisition des apprentissages et, en dernier lieu, il existe des mesures plus efficaces et moins contraignantes que le port du masque en extérieur pour lutter contre la propagation du virus ; <br/>
              - cette décision est illégale dès lors qu'elle a été prise par le conseil de défense sanitaire et est soumise au secret défense, qu'elle n'a pas été publiée mais a seulement fait l'objet d'une annonce publique reprise sur le site internet de la Présidence de la République alors qu'elle prolonge des mesures censées être temporaires.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule ;<br/>
              - la convention internationale des droits de l'enfant ;<br/>
              - le code de la défense ; <br/>
              - la loi n°2021-689 du 31 mai 2021 ;<br/>
              - le décret n°2021-699 du1er juin 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Face à l'aggravation de la situation sanitaire liée au covid-19, le Premier ministre a annoncé, lors d'une conférence de presse, le 6 décembre 2021, un rehaussement du protocole sanitaire dans les écoles maternelles et élémentaires à partir du 9 décembre 2021 avec le port du masque en extérieur pour les élèves en primaire, la limitation du brassage pendant la restauration et la pratique d'activités physiques et sportives compatibles avec le port du masque et les règles de distanciation. Un décret n° 2021-1585 du 7 décembre 2021 a modifié l'article 36 du décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire pour prévoir que les élèves des écoles élémentaires doivent porter un masque de protection dans les espaces extérieurs de ces établissements. Mme A... demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la levée de ces mesures.<br/>
<br/>
              3. Il résulte des données disponibles que le taux d'incidence a fortement augmenté chez les enfants de 0 à 9 ans passant de 66 à 578 cas pour 100 000 en cinq semaines, ce taux atteignant même 988 nouveaux cas pour 100 000 dans la classe d'âge des enfants de 6 à 10 ans. Une telle augmentation, si elle est liée à une augmentation des dépistages dans cette classe d'âge, traduit une circulation importante du virus en milieu scolaire. Si Mme A... soutient que les mesures prises méconnaîtraient l'intérêt supérieur de l'enfant et la liberté d'aller et venir, elle se borne à énoncer des allégations générales sans apporter d'éléments précis sur la nocivité du port du masque pour les enfants, sur son incidence sur les apprentissages et sur l'existence de mesures plus efficaces. Dans ces conditions, les mesures contestées ne peuvent être regardées comme des mesures n'étant pas manifestement nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de demander la déclassification d'informations qui seraient protégées au titre du secret de la défense nationale, la requête de Mme A... ne peut qu'être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A....<br/>
Fait à Paris, le 16 décembre 2021<br/>
Signé : Mathieu Hérondart<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
