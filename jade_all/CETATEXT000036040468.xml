<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036040468</ID>
<ANCIEN_ID>JG_L_2017_11_000000397796</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/04/04/CETATEXT000036040468.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 17/11/2017, 397796, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397796</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Vincent Ploquin-Duchefdelaville</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:397796.20171117</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal régional des pensions de Saint-Denis de La Réunion d'annuler la décision du 9 mai 2012 par laquelle le ministre de la défense a rejeté sa demande tendant au bénéfice d'une pension militaire d'invalidité au taux de 20 % à raison de " douleurs abdominales à type de brûlures épigastriques post prandiales avec troubles de transit intestinal et sensibilité abdominale ". Par un jugement n° 12/00006 du 10 février 2015, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16/02 du 24 février 2016, la cour régionale des pensions de Saint-Denis de la Réunion a rejeté l'appel formé par M. A...contre ce jugement.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 8 mars 2016 et 12 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à son avocat au titre des articles L. 761-1 du code de justice administrative et 37, alinéa 2, de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions militaires d'invalidité et des victimes de la guerre ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Ploquin-Duchefdelaville, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a notamment demandé le 16 février 2009 le bénéfice d'une pension militaire d'invalidité au taux de 20 % à raison de douleurs abdominales et de troubles du transit intestinal. Par une décision du 9 mai 2012, le ministre de la défense a rejeté sa demande au motif que cette infirmité n'était pas de nature à justifier un taux d'invalidité permettant d'atteindre le seuil minimum requis par les articles L. 4 et L. 5 du code des pensions militaires d'invalidité et des victimes de la guerre pour l'octroi d'une pension. Par un jugement du 10 février 2015, le tribunal régional des pensions de Saint-Denis de La Réunion a rejeté sa demande tendant à l'annulation de cette décision et à l'octroi d'une pension. Par un arrêt du 24 février 2016, la cour régionale des pensions de Saint-Denis de la Réunion a rejeté l'appel formé par M. A...contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article L. 4 du code des pensions militaires d'invalidité et des victimes de la guerre, dans sa version alors applicable : " Les pensions sont établies d'après le degré d'invalidité. / Sont prises en considération les infirmités entraînant une invalidité égale ou supérieure à 10 %. / Il est concédé une pension : / 1° Au titre des infirmités résultant de blessures, si le degré d'invalidité qu'elles entraînent atteint ou dépasse 10 % (...) /3° Au titre d'infirmité résultant exclusivement de maladie, si le degré d'invalidité qu'elles entraînent atteint ou dépasse : / 30 % en cas d'infirmité unique ; / 40 % en cas d'infirmités multiples.". L'article L. 9 de ce code renvoie à un décret le soin de fixer " les règles et barèmes pour la classification des infirmités d'après leur gravité ". Aux termes de l'article L.10 du même code : " Les degrés de pourcentage d'invalidité figurant aux barèmes prévus par le quatrième alinéa de l'article L. 9 sont : a) Impératifs, en ce qui concerne les amputations et les exérèses d'organe ; / b) Indicatifs dans les autres cas. / Ils correspondent à l'ensemble des troubles fonctionnels et tiennent compte, quand il y a lieu, de l'atteinte de l'état général ". Aux termes de l'article L. 26 de ce code : " Toute décision administrative ou judiciaire relative à l'évaluation de l'invalidité doit être motivée par des raisons médicales et comporter, avec le diagnostic de l'infirmité, une description complète faisant ressortir la gêne fonctionnelle et, s'il y a lieu, l'atteinte de l'état général qui justifient le pourcentage attribué ".<br/>
<br/>
              3. Pour estimer que le ministre de la défense avait à bon droit retenu que les infirmités invoquées par M. A...n'étaient pas de nature à justifier un taux d'invalidité permettant d'atteindre le seuil minimum prévu par les articles L. 4 et L. 5 du code des pensions militaires d'invalidité et des victimes de la guerre, la cour régionale des pensions s'est bornée, après avoir cité l'expertise médicale du 7 août 2009 qui décrivait notamment des " douleurs abdominales à type de brûlure épigastrique ", " un transit intestinal accéléré, fait de cinq à sept selles par jour ", des " diarrhées ", " une symptomatologie gênante dans la vie quotidienne avec obligation parfois de restreindre ses activités ", à relever que le guide barème prévoyait, d'une part, s'agissant des infirmités affectant l'estomac, l'indemnisation des seuls ulcères chroniques et fistules stomacales et, d'autre part, s'agissant des diarrhées chroniques, que le degré d'invalidité devait être fixé selon l'état de dépérissement du malade, puis que le rapport d'expertise ne mentionnait ni ulcères chroniques, ni fistules stomacales et qu'il ne faisait état d'aucun dépérissement de M. A... mais concluait au contraire au bon état général de celui-ci. En écartant ainsi les prétentions de M. A...en se référant aux seules recommandations du guide-barème, qui ne sont pas impératives hors le cas des amputations et exérèses d'organe, sans rechercher quelle était la gêne fonctionnelle subie par l'intéressé, la cour a méconnu les prescriptions précitées de l'article L. 26 du code des pensions militaires d'invalidité et des victimes de la guerre.  <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que M. A...est fondé à demander l'annulation de l'arrêt qu'il attaque. Il y a lieu, compte tenu de l'impossibilité pour la cour régionale des pensions de Saint-Denis de La Réunion de juger sur renvoi de la présente affaire dans une composition différente, d'en attribuer le jugement à la cour régionale des pensions de Paris. <br/>
<br/>
              5. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que Maître Delamarre, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à celui-ci.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 24 février 2016 de la cour régionale des pensions de Saint-Denis de La Réunion est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour régionale des pensions de Paris.<br/>
Article 3 : L'Etat versera à maître Delamarre, avocat de M.A..., une somme de 1 500 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : La présente décision sera notifiée à M. A...et à la ministre des armées.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
