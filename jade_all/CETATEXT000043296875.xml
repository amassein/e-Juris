<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043296875</ID>
<ANCIEN_ID>JG_L_2021_03_000000445716</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/29/68/CETATEXT000043296875.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 19/03/2021, 445716, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445716</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445716.20210319</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... D... a demandé au tribunal administratif de Rennes d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 en vue de la désignation des conseillers municipaux et communautaires d'Arzon. Par un jugement n° 2001365 du 28 septembre 2020, le tribunal administratif a rejeté sa protestation. <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 27 octobre 2020 et 7 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les résultats du premier tour des élections municipales de la commune d'Arzon et de déclarer M. B... C... inéligible ;<br/>
<br/>
              3°) de mettre à la charge de M. C... et des candidats élus de la liste " Arzon avec passion " la somme d'un euro au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 1er mars 2021, présentée par M. D... ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 en vue de la désignation des conseillers municipaux et des conseillers communautaires de la commune d'Arzon (Morbihan), la liste conduite par M. C..., maire sortant, est arrivée en tête avec 666 voix, soit 51,70 % des suffrages exprimés, la liste conduite par M. F... a obtenu 370 voix, soit 28,72 % des suffrages exprimés et celle conduite par M. E... 252 voix, soit 19,56 % des suffrages exprimés. M. D..., qui figurait sur la liste menée par M. F..., fait appel du jugement du tribunal administratif de Rennes du 28 septembre 2020 rejetant sa protestation formée contre ces opérations électorales.<br/>
<br/>
              2. En premier lieu, en portant à la connaissance des parties, avant la tenue de l'audience du tribunal, le sens des conclusions qu'il envisageait de prononcer par la mention " rejet au fond ", le rapporteur public a mis ces dernières en mesure de connaître pleinement sa position et a satisfait, par suite, aux prescriptions de l'article R. 711-3 du code de justice administrative. <br/>
<br/>
              3. En deuxième lieu, en écartant aux points 6 à 8 de son jugement, les griefs tirés de ce que l'usage de la salle de cinéma municipale pendant la campagne électorale aurait été réservé à la liste de M. C..., que les données personnelles des usagers du complexe sportif municipal auraient été utilisées par le service des sports de la commune à des fins de propagande électorale et que le film " Regards croisés " aurait été produit par la commune pour favoriser la réélection du maire sortant, le tribunal administratif de Rennes, qui n'était pas tenu de répondre à l'ensemble des arguments invoqués par le protestataire au soutien de ses griefs, doit, en tout état de cause, être regardé comme s'étant prononcé sur la méconnaissance invoquée, à ce titre, des dispositions de l'article L. 52-8 du code électoral.    <br/>
<br/>
              4. En troisième lieu, aux termes de l'article L. 51 du code électoral : " Pendant la durée de la période électorale, dans chaque commune, des emplacements spéciaux sont réservés par l'autorité municipale pour l'apposition des affiches électorales. (...) / Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, tout affichage relatif à l'élection, même par affiches timbrées, est interdit en dehors de cet emplacement ou sur l'emplacement réservé aux autres candidats, ainsi qu'en dehors des panneaux d'affichage d'expression libre lorsqu'il en existe ".<br/>
<br/>
              5. Il résulte de l'instruction que, lors de la période précédant le tour de scrutin, quatre affiches électorales en faveur de la liste de M. C... ont été apposées pendant deux jours en dehors des emplacements prévus à cet effet, en méconnaissance des prescriptions de l'article L. 51 du code électoral précédemment cité. Toutefois, cet abus de propagande, du fait même de la brièveté de sa durée et de son caractère limité, ne saurait, eu égard à l'écart des voix observé, être regardé comme ayant été, dans les circonstances de l'espèce, de nature à altérer la sincérité du scrutin. <br/>
<br/>
              6. En quatrième lieu, aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. / Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. (...) ". Aux termes de l'article L. 52-8 du même code : " (...) Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués (...) ". <br/>
<br/>
              7. Il résulte de l'instruction que la projection publique dans la salle de cinéma communale, le 12 mars 2020, des films " Regards croisés " et " Vivre à Arzon " ne peut être regardée, eu égard au contenu de ces deux films qui se bornent à assurer la promotion générale de la commune sans mettre en avant le bilan de l'équipe sortante ni présenter le maire dans des termes particulièrement élogieux, comme ayant le caractère d'une campagne de promotion publicitaire prohibée par l'article L. 52-1 du code électoral cité ci-dessus. Dès lors, la circonstance, à la supposer établie, que des invitations à cette projection aient été diffusées, par courrier électronique, par le service des sports de la commune auprès de ses usagers est sans incidence sur la sincérité du scrutin et cette projection ne peut, en tout état de cause, avoir emporté méconnaissance des dispositions précitées de l'article L. 52-8 du code électoral.       <br/>
<br/>
              8. Il résulte de tout ce qui précède que M. D... n'est pas fondé à demander l'annulation du jugement qu'il attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être, en tout état de cause, également rejetées.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D..., à M. B... C... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
