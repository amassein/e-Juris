<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728716</ID>
<ANCIEN_ID>JG_L_2019_12_000000425061</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/87/CETATEXT000039728716.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 31/12/2019, 425061, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425061</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:425061.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... et Mme C... B... ont demandé au tribunal administratif de Clermont-Ferrand d'annuler la décision du 19 mars 2014 par laquelle le préfet de la Haute-Loire a refusé de reconnaître la persistance du droit de prise d'eau fondé en titre attaché au Moulin du Rocher, sur le territoire de la commune de Saint-Martin-de-Fugères, de reconnaître le maintien de ce droit, de fixer la consistance légale de ce droit fondé en titre à 103 kilowatts et la valeur du débit à restituer au cours d'eau à l'aval du barrage de prise à 39,6  litres par seconde. Par un jugement n° 1401691 du 14 juin 2016, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16LY02894 du 22 août 2018, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. et Mme B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 25 octobre 2018 et le 28 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. et Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un courrier du 11 décembre 2013, M. et Mme B... ont saisi le préfet de la Haute-Loire d'une demande de reconnaissance du droit de prise d'eau fondé en titre attaché au Moulin du Rocher, installé sur L'Holme, dont ils sont propriétaires, sur le territoire de la commune de Saint-Martin-de-Fugères. Par une lettre du 19 mars 2014, le préfet de la Haute-Loire a refusé de reconnaître la persistance de ce droit. Par un jugement du 14 juin 2016, le tribunal administratif de Clermont-Ferrand a rejeté leur demande tendant à l'annulation de cette décision. Par un arrêt du 22 août 2018, contre lequel M. et Mme B... se pourvoient en cassation, la cour administrative d'appel de Lyon a rejeté leur appel formé contre ce jugement.<br/>
<br/>
              2. La force motrice produite par l'écoulement d'eaux courantes ne peut faire l'objet que d'un droit d'usage et en aucun cas d'un droit de propriété. Il en résulte qu'un droit fondé en titre ne se perd que lorsque la force motrice du cours d'eau n'est plus susceptible d'être utilisée par son détenteur, du fait de la ruine ou du changement d'affectation des ouvrages essentiels destinés à utiliser la pente et le volume de ce cours d'eau. Ni la circonstance que ces ouvrages n'aient pas été utilisés en tant que tels au cours d'une longue période de temps, ni le délabrement du bâtiment auquel le droit d'eau fondé en titre est attaché, ne sont de nature, à eux seuls, à remettre en cause la pérennité de ce droit. L'état de ruine, qui conduit en revanche à la perte du droit, est établi lorsque les éléments essentiels de l'ouvrage permettant l'utilisation de la force motrice du cours d'eau ont disparu ou qu'il n'en reste que de simples vestiges, de sorte qu'elle ne peut plus être utilisée sans leur reconstruction complète.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond, et en particulier des constatations effectuées tant par la direction départementale des territoires de Haute-Loire en juin 2012 que par l'huissier de justice mandaté sur les lieux par les requérants en février 2017, que si le seuil de prise d'eau de l'installation sur L'Holme est dans un état très dégradé, les pierres qui le constituent persistent à assurer au moins en partie leur fonction de retenue de l'eau et que des travaux limités permettraient aisément de rétablir leur fonction de dérivation en vue de l'utilisation de la force motrice du cours d'eau, les canaux d'amenée et de fuite ainsi que le bâtiment étant en revanche toujours présents, eu égard à la configuration des lieux. Par suite, en jugeant que la persistance de seuls quelques blocs de pierre non agencés, s'agissant du seuil de prise d'eau, impliquait la reconstruction complète de l'ouvrage et caractérisait un état de ruine de l'installation permettant de justifier la perte du droit fondé en titre des requérants, la cour administrative d'appel a inexactement qualifié les faits qui lui étaient soumis. <br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. et Mme B... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser aux requérants au titres des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 22 août 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera à M. et Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... et Mme C... B... et à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
