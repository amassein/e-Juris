<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288274</ID>
<ANCIEN_ID>JG_L_2014_07_000000368856</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288274.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 23/07/2014, 368856, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368856</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368856.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 mai et 21 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la communauté de communes de Lacq, dont le siège est Hôtel de Communauté, Rond-point des Chênes, BP 73 à Mourenx (64150) ; la communauté de communes de Lacq demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1100759 du 26 mars 2013 par lequel le tribunal administratif de Pau a, à la demande M. B...A..., annulé pour excès de pouvoir la décision du 7 février 2011 du président de la communauté de communes de Lacq, en tant qu'elle a refusé à M. A...le retrait des arrêtés le plaçant en congé de maladie ordinaire du 2 avril au 21 novembre 2010 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M. A...devant le tribunal administratif de Pau ;<br/>
<br/>
              3°) de mettre à la charge de M. A...le versement de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, avocat de la communauté de communes de Lacq ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., agent de service au sein de la communauté de communes de Lacq, a été victime, le 25 janvier 2008, d'un accident reconnu imputable au service et mis, par suite, en congé de maladie à ce titre ; qu'une première expertise médicale effectuée le 2 avril 2010 a conclu à la consolidation de son état de santé et à son inaptitude définitive à reprendre ses fonctions ; que le président de la communauté de communes de Lacq a, par un arrêté du 8 octobre 2010, placé rétroactivement M. A...en congé de maladie ordinaire à compter du 2 avril 2010, congé prolongé par un arrêté du 16 novembre 2010 ; qu'une seconde expertise médicale effectuée le 22 novembre 2010 a déclaré M. A...apte à reprendre ses fonctions sur un poste aménagé ; que les congés de maladie ordinaire de l'intéressé ont été prolongés par arrêtés du 3 décembre 2010 et du 6 janvier 2011 jusqu'au 1er avril 2011, date à laquelle M. A...a pu reprendre son travail sur un nouveau poste ; que, sur demande de M.A..., le tribunal administratif de Pau a, par un jugement du 26 mars 2013, annulé pour excès de pouvoir la décision du 7 février 2011 par laquelle le président de la communauté de communes de Lacq a refusé de retirer les arrêtés du 8 octobre et du 16 novembre 2010, en tant que ceux-ci plaçaient l'intéressé en congé de maladie ordinaire du 2 avril au 21 novembre 2010 ; que la communauté de communes de Lacq se pourvoit en cassation contre ce jugement ; <br/>
<br/>
              2.	Considérant qu'aux termes des dispositions du 2° de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le fonctionnaire en activité a droit : (...) / 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants (...). / Toutefois, si la maladie provient... d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à la mise à la retraite (...) " ; <br/>
<br/>
              3.	Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...demandait au tribunal administratif de Pau d'annuler pour excès de pouvoir la décision du 7 février 2011 par laquelle le président de la communauté de communes de Lacq a refusé de retirer ses arrêtés du 8 octobre et du 16 novembre 2010 ; que, dès lors, contrairement à ce que soutient la communauté requérante, le tribunal administratif n'a pas statué au-delà des conclusions dont il était saisi  en annulant cette décision en tant qu'elle portait refus de retirer les arrêtés le plaçant en congé de maladie ordinaire du 2 avril au 21 novembre 2010 ;<br/>
<br/>
              4.	Considérant, en deuxième lieu, qu'il résulte des dispositions précitées du 2° de l'article 57 de la loi du 26 janvier 1984 qu'un agent de la fonction publique territoriale qui n'est plus apte à reprendre son service à la suite d'un accident de service et auquel aucune offre de poste adapté ou de reclassement n'a été faite a le droit d'être maintenu en congé de maladie ordinaire avec le bénéfice de son plein traitement sans autre limitation que celles tenant à sa mise à la retraite ou au rétablissement de son aptitude au service ; qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de l'accident du 25 janvier 2008, reconnu imputable au service, M. A...a été déclaré, au terme d'une première expertise médicale effectuée le 2 avril 2010, inapte à l'exercice de toutes fonctions à compter de cette date ; que, par suite, et en dépit de la circonstance tenant à ce que la commission de réforme qui a, le 23 septembre 2010, confirmé l'inaptitude définitive de M.A..., a été d'avis que les arrêts de travail postérieurs au 2 avril 2010 devaient être considérés comme n'étant pas imputables à l'accident du 25 janvier 2008, le tribunal administratif, qui a suffisamment motivé son jugement sur ce point, n'a pas commis d'erreur de droit en estimant, au vu des conclusions de l'expert, que M. A...tenait des dispositions précitées de la loi du 26 janvier 1984 le droit d'être maintenu en congé de maladie ordinaire, avec bénéfice de son plein traitement, sans autre limitation que celle tenant à sa mise à la retraite ou au rétablissement de son aptitude au service, pour la période du 2 avril au 21 novembre 2010, date à laquelle une seconde expertise médicale l'a jugé apte à reprendre son service sur un poste aménagé ; <br/>
<br/>
              5.	Considérant qu'il résulte de tout ce qui précède que la communauté de communes de Lacq n'est pas fondée à demander l'annulation du jugement attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la communauté de communes de Lacq la somme de 3 000 euros à verser à M. A...au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la communauté de communes de Lacq est rejeté.<br/>
<br/>
Article 2 : La communauté de communes de Lacq versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la communauté de communes de Lacq et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
