<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036576213</ID>
<ANCIEN_ID>JG_L_2018_02_000000401325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/57/62/CETATEXT000036576213.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/02/2018, 401325</TITRE>
<DATE_DEC>2018-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401325.20180205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Châlons-en-Champagne l'annulation de la décision du 4 septembre 2012 prise par l'Agence de services et de paiement et sa condamnation à lui verser la somme de 2 700 000 euros en réparation des préjudices qu'il estime avoir subis du fait de la suppression des aides communautaires auxquelles la société Euromat était éligible. Par un jugement n° 1201997, 1202178 du 14 octobre 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14NC02249, 14NC02250 du 10 mai 2016, la cour administrative d'appel de Nancy a rejeté l'appel qu'il a formé contre ce jugement en tant qu'il le concerne.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 juillet et 10 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il le concerne ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Agence de services et de paiement le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. B...et à la SCP Boutet-Hourdeaux, avocat de l'agence de services et de paiement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Euromat, qui était dirigée par M.B..., exerçait une activité de fabrication de granulés et de fourrage séché qui la rendait éligible au bénéfice d'aides communautaires prévues par le règlement (CE) n° 603/95 du Conseil du 21 février 1995. A la suite d'un contrôle effectué par la société interprofessionnelle des oléagineux, protéagineux et cultures textiles (SIDO), l'agrément dont la société Euromat bénéficiait pour l'attribution de ces aides lui a été retiré par une décision de la SIDO du 16 juin 1997. Ce retrait a été confirmé à la société par une lettre du 23 mars 1998 lui demandant également de reverser les aides déjà perçues au titre de la campagne précédente. La société Euromat a été placée en redressement judiciaire le 27 avril 1998 et la SIDO a déclaré une créance de 1 216 187,99 euros correspondant à la demande de restitution des aides déjà allouées. La liquidation judiciaire a été prononcée par jugement en date du 7 juillet 1998, et la créance de la SIDO, contestée par le liquidateur devant le juge-commissaire, a été rejetée par ordonnance du 21 novembre 2005, confirmée par la cour d'appel de Reims le 14 mai 2007 et par la Cour de cassation le 10 juillet 2008. M. B...a été relaxé du chef de déclarations mensongères en vue d'obtenir d'une administration publique ou d'un organisme chargé d'une mission de service public une allocation, un paiement ou un avantage indu par un jugement du tribunal correctionnel de Troyes en date du 8 février 2005 devenu définitif. Par un jugement du 14 octobre 2014, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande tendant à la condamnation de l'Agence de services et de paiement (ASP) venant aux droits de la SIDO à lui verser une indemnité de 6 525 000 euros en réparation du préjudice qu'il estime avoir subi du fait de la suppression des aides communautaires. M. B...demande l'annulation de l'arrêt du 10 mai 2016, par lequel la cour administrative d'appel de Nancy a rejeté son appel.  <br/>
<br/>
              2. Aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis. Sont prescrites, dans le même délai et sous la même réserve, les créances sur les établissements publics dotés d'un comptable public ". Aux termes de l'article 2 de cette loi : " La prescription est interrompue par : (...) Tout recours formé devant une juridiction, relatif au fait générateur, à l'existence, au montant ou au paiement de la créance, quel que soit l'auteur du recours et même si la juridiction saisie est incompétente pour en connaître, et si l'administration qui aura finalement la charge du règlement n'est pas partie à l'instance (...) ". Aux termes de l'article 3 de cette loi : " La prescription ne court ni contre le créancier qui ne peut agir, soit par lui-même ou par l'intermédiaire de son représentant légal, soit pour une cause de force majeure, ni contre celui qui peut être légitimement regardé comme ignorant l'existence de sa créance ou de la créance de celui qu'il représente légalement ". <br/>
<br/>
              3. Lorsqu'est demandée l'indemnisation du préjudice résultant de l'illégalité d'une décision administrative, le fait générateur de la créance doit être rattaché non à l'exercice au cours duquel la décision a été prise mais à celui au cours duquel elle a été valablement notifiée à son destinataire ou portée à la connaissance du tiers qui se prévaut de cette illégalité. <br/>
<br/>
              4. En premier lieu, la cour a relevé, par un motif non contesté, que la créance dont se prévalait M. B...trouvait sa source dans la décision du 16 juin 1997 retirant à la société Euromat l'agrément dont elle bénéficiait et que M. B...avait eu connaissance de la décision de retrait de l'agrément du 16 juin 1997 au plus tard le 15 juillet 1997, date à laquelle il avait adressé à la SIDO un courrier " accusant réception " de cette décision. La cour n'a pas commis d'erreur de droit en déduisant de ces faits, alors même qu'il n'était pas établi que la décision avait été régulièrement notifiée et qu'elle ne portait pas la mention des voies et délais de recours, que la prescription avait commencé à courir le 1er janvier 1998, premier jour de l'année suivant celle au cours de laquelle la décision du 16 juin 1997 devait être regardée comme ayant été portée à la connaissance de M. B...à travers la notification valablement faite à la société Euromat.<br/>
<br/>
              5. En second lieu, la cour a relevé que la constitution de partie civile de la SIDO dans le cadre de la procédure pénale engagée contre M. B...et la procédure civile résultant de la déclaration de créance par la SIDO au mandataire judiciaire de la société Euromat mentionnées au point 1 tendaient au reversement des aides déjà allouées à la société Euromat pour la campagne 1996/1997, d'un montant de 1 216 188 euros, dont l'établissement public lui avait demandé le remboursement par lettre du 23 mars 1998. Elle n'a pas inexactement qualifié les faits en jugeant que ces actions concernaient une créance et un fait générateur distinct de celui auquel se rattachait la demande indemnitaire de M. B...qui, ainsi qu'il a été dit au point 4, trouvait son origine dans la décision du 16 juin 1997 de suppression, pour le futur, des aides communautaires et qu'en conséquence, elles n'avaient pas interrompu le cours de la prescription quadriennale. <br/>
<br/>
              6. Il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. En conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'ASP au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : Les conclusions de l'Agence de services et de paiement présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à l'Agence de services et de paiement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-04 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. POINT DE DÉPART DU DÉLAI. - PRÉJUDICE RÉSULTANT DE L'ILLÉGALITÉ D'UNE DÉCISION ADMINISTRATIVE - EXERCICE AUQUEL RATTACHER LA CRÉANCE POUR DÉTERMINER LE POINT DE DÉPART DE LA PRESCRIPTION - DATE DE NOTIFICATION DE LA DÉCISION [RJ1] OU, POUR UN TIERS, DATE À LAQUELLE ELLE A ÉTÉ PORTÉE À SA CONNAISSANCE.
</SCT>
<ANA ID="9A"> 18-04-02-04 Lorsqu'est demandée l'indemnisation du préjudice résultant de l'illégalité d'une décision administrative, le fait générateur de la créance doit être rattaché non à l'exercice au cours duquel la décision a été prise mais à celui au cours duquel elle a été valablement notifiée à son destinataire ou portée à la connaissance du tiers qui se prévaut de cette illégalité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 5 décembre 2014, Commune de Scionzier, n° 359769, p. 60.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
