<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027042749</ID>
<ANCIEN_ID>JG_L_2013_02_000000353559</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/04/27/CETATEXT000027042749.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 06/02/2013, 353559, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353559</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:353559.20130206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 21 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B... A..., demeurant..., ; M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 006707 du 24 août 2011 par laquelle le Conseil national de l'ordre des chirurgiens-dentistes a refusé de reconnaître le diplôme universitaire d'occlusodontie et d'ostéopathie délivré par l'Ecole supérieure d'ostéopathie et par la faculté de chirurgie dentaire de Lille II en 2007 et décidé que la mention de ce diplôme ne pouvait figurer sur les plaques et imprimés professionnels des chirurgiens-dentistes ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes, en application de l'article L. 911-2 du code de justice administrative, de prendre une nouvelle décision sur la reconnaissance du diplôme d'université d'occlusodontie et d'ostéopathie dans le délai de deux mois à compter de la décision ;<br/>
<br/>
              3°) d'assortir cette injonction d'une astreinte de 100 euros par jour de retard sur le fondement de l'article L. 911-3 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 2002-303 du 4 mars 2002 ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 4127-216 du code de la santé publique, qui est au nombre des dispositions formant le code de déontologie des chirurgiens-dentistes : " Les seules indications que le chirurgien-dentiste est autorisé à mentionner sur ses imprimés professionnels, notamment ses feuilles d'ordonnances, notes d'honoraires et cartes professionnelles, sont : (...) / 3° Les diplômes, titres et fonctions reconnus par le Conseil national de l'ordre ; (...) " ; et qu'aux termes de l'article R. 4127-218 du même code : " Les seules indications qu'un chirurgien-dentiste est autorisé à faire figurer sur une plaque professionnelle à la porte de son immeuble ou de son cabinet sont (...)  les diplômes, titres ou fonctions reconnus par le Conseil national de l'ordre. (...) " ; que, pour l'application de ces dispositions, le conseil national de l'ordre a, par une décision du 13 avril 2007, modifiée le 26 septembre 2009, fixé les critères au regard desquels il se prononce sur les demandes de reconnaissance des diplômes, titres et fonctions, en prenant en compte, notamment, l'information du patient et l'objectif d'éviter les risques de confusion de nom avec d'autres diplômes ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, contrairement à ce que soutient le requérant, la décision du 24 août 2011 est signée du président du Conseil national de l'ordre des chirurgiens-dentistes ; <br/>
<br/>
              3. Considérant que, par la décision attaquée, le Conseil national de l'ordre des chirurgiens-dentistes a refusé de reconnaître  le diplôme d'université " Occlusodontie et ostéopathie " délivré par l'Ecole supérieure d'ostéopathie et la faculté de chirurgie dentaire de Lille II, ce qui interdit à M. A...de le mentionner sur sa plaque et ses imprimés professionnels ; que cette décision est fondée sur le motif que l'information du patient ne doit pas conduire à une confusion quant à l'étendue de la capacité professionnelle du praticien alors même que l'ostéopathie n'entre pas dans la capacité professionnelle du chirurgien-dentiste ; <br/>
<br/>
              4. Considérant que, contrairement à ce que soutient le requérant, il résulte des dispositions de la décision du 13 avril 2007 que la condition d'information des patients posée pour qu'un diplôme puisse être reconnu conformément aux dispositions du 3° de l'article R. 4127-216 du code de la santé publique a pour objet d'éviter tout risque d'erreur ou de confusion dans l'interprétation des indications qui sont données par un chirurgien-dentiste ; qu'il ressort des pièces du dossier que le diplôme d'université " occlusodontie et ostéopathie " délivré par l'Ecole supérieure d'ostéopathie et l'université Lille II n'a pas pour objet de permettre d'exercer la profession d'ostéopathe, laquelle est réservée, aux termes de l'article 75 de la loi du 4 mars 2002 relative aux droits des patients et à la qualité du système de santé, aux personnes titulaires d'un diplôme sanctionnant une formation spécifique délivrée par un établissement de formation agréé ; que, dès lors, le Conseil national de l'ordre des chirurgiens-dentistes a pu, sans commettre d'erreur d'appréciation, se fonder sur ce que la mention de ce diplôme est de nature à créer une confusion auprès des patients sur les capacités professionnelles du praticien pour refuser qu'un tel diplôme soit reconnu comme pouvant figurer sur les imprimés professionnels de chirurgien-dentiste ;<br/>
<br/>
              5. Considérant que si le requérant soutient que, pour éviter ce risque de confusion, il appartenait au Conseil national de l'ordre des chirurgiens-dentistes d'autoriser la mention du diplôme dont il est titulaire en le limitant à la mention " occlusodontie ", il ne ressort d'aucune disposition du code de la santé publique ou d'un autre texte ni d'aucun principe que le conseil national de l'ordre puisse modifier l'intitulé d'un titre universitaire ; qu'ainsi ce moyen ne peut qu'être rejeté ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision du Conseil national de l'ordre des chirurgiens-dentistes du 24 août 2011, qui est suffisamment motivée ; que doivent être rejetées, par voie de conséquence, ses conclusions à fin d'injonction ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A...et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
