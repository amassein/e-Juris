<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027311366</ID>
<ANCIEN_ID>JG_L_2013_04_000000364029</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/31/13/CETATEXT000027311366.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 12/04/2013, 364029</TITRE>
<DATE_DEC>2013-04-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364029</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Bereyziat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364029.20130412</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 21 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le garde des sceaux, ministre de la justice ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1202331 du 12 novembre 2012 par laquelle le juge des référés du tribunal administratif de Dijon, statuant sur le fondement de l'article L. 521-1 du code de justice administrative a suspendu l'exécution de la décision du 11 octobre 2012 par laquelle le directeur du centre de détention de Joux-la-Ville (Yonne) avait ouvert puis retenu au...;<br/>
<br/>
              2°) statuant en référé, de rejeter les demandes de M. Ait-Hammi;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de procédure pénale, notamment ses articles R. 57-6-2, R. 57-8-19 et D. 262 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 2009-1436 du 24 novembre 2009, notamment ses articles 40 et 42 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Bereyziat, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le directeur du centre de détention de Joux-la-Ville a saisi, le 11 octobre 2012, un courrier adressé au détenu Ait-Hammi-, à la demande de ce dernier, par le greffier de la cour d'appel de Dijon ; que ce courrier comportait copie de l'arrêt du 28 avril 2011 par lequel la chambre correctionnelle de cette cour avait condamné ce détenu à...; que pour soutenir, devant le juge des référés, qu'il existait un doute sérieux, en l'état de l'instruction, sur la légalité de cette saisie, M. Hait-Hammifaisait notamment valoir que le courrier litigieux était au nombre des correspondances sous pli fermé prévues au troisième alinéa de l'article 40 de la loi pénitentiaire du 24 novembre 2009, que l'arrêt du 28 avril 2011 ne constituait pas un document mentionnant le motif d'écrou, au sens de l'article 42 de la même loi, et qu'entraient seuls dans le champ des documents devant être conservés au greffe de l'établissement pénitentiaire les documents mentionnant le motif d'écrou que le détenu aurait eus en sa possession au moment de l'entrée dans l'établissement ou qu'il aurait choisi de confier au greffe ; <br/>
<br/>
              3. Considérant toutefois, d'une part, qu'aux termes qu'aux termes du deuxième alinéa de l'article 40 de la loi du 24 novembre 2009 : " Le courrier adressé ou reçu par les personnes détenues peut être contrôlé et retenu par l'administration pénitentiaire lorsque cette correspondance paraît compromettre gravement leur réinsertion ou le maintien du bon ordre et la sécurité (...) " et qu'aux termes de l'article 42 de la même loi : " Toute personne détenue a droit à la confidentialité de ses documents personnels. Ces documents peuvent être confiés au greffe de l'établissement qui les met à la disposition de la personne concernée. Les documents mentionnant le motif d'écrou de la personne détenue sont, dès son arrivée, obligatoirement confiés au greffe " ; <br/>
<br/>
              4. Considérant, d'autre part, que le quatrième alinéa de l'article 40 prévoit que : " Lorsque l'administration pénitentiaire décide de retenir le courrier d'une personne détenue, elle lui notifie sa décision " ; qu'en application de l'article R. 57-8-19 du code de procédure pénale, la correspondance ainsi retenue est déposée dans le dossier individuel de la personne détenue ; que l'article R. 57-6-2 de ce code organise les modalités de consultation du document mentionnant le motif de l'écrou retenu qu'il ait, aux termes de ses dispositions, " été déposé dès son arrivée ou en cours de détention " ; que le troisième alinéa de l'article 40 de la loi pénitentiaire prévoit toutefois que : " Ne peuvent être ni contrôlées ni retenues les correspondances échangées entre les personnes détenues et leur défenseur, les autorités administratives et judiciaires françaises et internationales, dont la liste est fixée par décret, et les aumôniers agréés auprès de l'établissement " ; que l'article D. 262 du code de procédure pénale dresse la liste limitative des autorités administratives et judiciaires françaises mentionnées par ces dernières dispositions ; que le greffier d'une cour d'appel ne figure pas au nombre de ces autorités ; que l'article R. 57-8-20 de ce code dispose que les correspondances adressées aux détenus par ces mêmes autorités sont expédiées sous pli fermé comportant sur les enveloppes toutes les mentions utiles pour indiquer les qualité et adresse professionnelle de leurs expéditeurs ;<br/>
<br/>
              5. Considérant qu'il résulte de la combinaison des dispositions législatives et réglementaires citées plus haut que peut être légalement contrôlée par l'administration pénitentiaire, sur le fondement du deuxième alinéa de l'article 40 de la loi du 24 novembre 2009, la correspondance adressée à un détenu, émanant du greffe d'une cour d'appel et ne comportant pas, sur son enveloppe, les mentions utiles pour faire connaître qu'elle est expédiée par l'une des autorités administratives ou judiciaires limitativement énumérées à l'article D. 262 du code de procédure pénale ; que lorsque l'administration pénitentiaire contrôle une telle correspondance et constate, d'une part, que celle-ci n'est pas expédiée par l'une des ces autorités, d'autre part, que cette correspondance comporte un document mentionnant le motif d'écrou du destinataire, elle a alors obligation de retenir ce document, de le conserver au greffe de l'établissement pénitentiaire et de notifier cette saisie au détenu intéressé afin que ce dernier soit en mesure de consulter ce document dans les conditions fixées à l'article R. 57-6-2 du code de procédure pénale ; que constitue un document mentionnant le motif d'écrou, au sens de l'article 42 de cette loi, l'arrêt par lequel une cour d'appel condamne une personne à la peine de prison ferme en exécution de laquelle cette personne est détenue ; qu'enfin la rétention obligatoire au greffe de l'établissement pénitentiaire, prévue par ce même article s'étend à l'ensemble des documents mentionnant le motif d'écrou du détenu et dont l'administration pénitentiaire viendrait à prendre légalement connaissance, non seulement au moment de l'incarcération, mais aussi tout au long de celle-ci ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'en jugeant que les moyens soulevés devant lui par M. Ait-Hammiet tirés de la méconnaissance des dispositions combinées des articles 40 et 42 de la loi pénitentiaire étaient de nature à faire naître un doute sérieux, en l'état de l'instruction, sur la légalité de la saisie de courrier opérée le 11 octobre 2002 par le directeur du centre de détention de Joux-la-Ville, alors que ces moyens se méprenaient sur la portée de ces dispositions, le juge des référés a commis une erreur de droit ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, le garde des sceaux, ministre de la justice est fondé à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure engagée devant le juge des référés, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier que, pour soutenir que l'urgence s'attache à ce que soit suspendue l'exécution de la saisie de courrier opérée le 11 octobre 2012, M. Ait-Hammifait valoir qu'il lui est nécessaire de disposer de la copie de l'arrêt de la cour d'appel de Dijon du 28 avril 2011 contenue dans le pli saisi, afin de pouvoir préparer convenablement sa défense lors de l'audience de confusion des peines organisée par le tribunal correctionnel d'Auxerre le 13 décembre 2012 ; que toutefois, l'intéressé ne conteste pas avoir pu prendre connaissance de cet arrêt, postérieurement à l'introduction de sa demande en référé, lors d'une consultation organisée par le greffe du centre de détention le 26 octobre 2012, dans les conditions fixées à l'article R. 57-6-2 du code de procédure pénale et en temps utile pour préparer sa défense à l'audience de confusion des peines ; qu'ainsi et sans qu'il soit besoin de rechercher si cette dernière audience s'est effectivement tenue, M. Ait-Hammine justifie pas de l'urgence qui s'attacherait à la suspension de l'exécution de la décision du 11 octobre 2012 ; que, par suite, ses conclusions présentées sur le fondement de l'article L. 521-1 du code de justice administrative et tendant à la suspension de l'exécution de cette décision, doivent être rejetées ; que doivent être également rejetées, par voie de conséquence, ses conclusions tendant au prononcé de mesures d'injonction ; <br/>
<br/>
              9. Considérant, enfin, que les dispositions combinées de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 12 novembre 2012 du juge des référés du tribunal administratif de Dijon est annulée.<br/>
<br/>
Article 2 : La demande présentée par M. KalideHait-Hammidevant le juge des référés du tribunal administratif de Dijon est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice et à M. Kalide B.greffe de l'établissement le courrier du 9 octobre 2012 adressé à M. Kalide Ait-Hammi par la cour d'appel de Dijon enjoint au directeur de remettre ce courrier à l'intéressé dans un délai de trois jours à compter de la notification de son ordonnance et mis à la charge de l'Etat une somme de 700 euros au titre des dispositions combinées de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - DOCUMENTS PERSONNELS DES DÉTENUS - RÉTENTION AU GREFFE DES DOCUMENTS MENTIONNANT LE MOTIF D'ÉCROU (ART. 42 DE LA LOI PÉNITENTIAIRE) - PORTÉE DE L'OBLIGATION - DOCUMENTS DONT L'ADMINISTRATION PREND CONNAISSANCE PENDANT LA DÉTENTION - INCLUSION.
</SCT>
<ANA ID="9A"> 37-05-02-01 La rétention obligatoire au greffe de l'établissement pénitentiaire des documents mentionnant le motif d'écrou de la personne détenue, prévue par l'article 42 de la loi n° 2009-1436 du 24 novembre 2009, s'étend à l'ensemble des documents mentionnant le motif d'écrou du détenu et dont l'administration pénitentiaire viendrait à prendre légalement connaissance, non seulement au moment de l'incarcération, mais aussi tout au long de celle-ci.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
