<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043688516</ID>
<ANCIEN_ID>JG_L_2021_06_000000445346</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/68/85/CETATEXT000043688516.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 21/06/2021, 445346</TITRE>
<DATE_DEC>2021-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445346</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445346.20210621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... D... a demandé au tribunal administratif de Toulon d'annuler l'élection de M. J... au conseil municipal de la commune de Grimaud. Par un jugement n° 2001364 du 15 septembre 2020, le tribunal administratif de Toulon a, d'une part, refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 231-6 du code électoral soulevée en défense par M. J... et, d'autre part, rejeté la protestation de M. D.... <br/>
<br/>
              Par une requête, deux mémoires en réplique et un nouveau mémoire, enregistrés le 14 octobre 2020 et les 7 janvier, 18 février et 26 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) d'annuler l'élection de M. J... ; <br/>
<br/>
              3°) de mettre à la charge de M. J... la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code électoral ;<br/>
              - la loi n°2004-809 du 13 août 2004 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 à Grimaud (Var), commune de plus de 1 000 habitants, en vue de l'élection des conseillers municipaux et des conseillers communautaires, la liste " Servir Grimaud " conduite par M. I... a obtenu 1122 voix, soit 69,60 % des suffrages exprimés, et la liste concurrente menée par Mme G... C..., " Grimaud autrement ", a obtenu 490 voix soit 30,39 % des suffrages exprimés. M. J..., figurant en quatrième position sur la liste conduite par Mme C..., a été élu au conseil municipal. Saisi par M. D... d'une protestation tendant à l'annulation de l'élection au conseil municipal de M. J..., le tribunal administratif de Toulon a, par un jugement du 15 septembre 2020, d'une part décidé qu'il n'y avait pas lieu de transmettre au Conseil d'Etat la question, soulevée en défense par M. J..., de la conformité aux droits et libertés garantis par la Constitution des dispositions du 6° de l'article L. 231 du code électoral et, d'autre part, rejeté la protestation de M. D.... Ce dernier fait appel du jugement du 15 septembre 2020 en tant qu'il a rejeté sa protestation. Eu égard aux moyens invoqués dans ses mémoires en défense, M. J... doit être regardé comme contestant le refus qui lui a été opposé par le tribunal administratif de Toulon de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée. <br/>
<br/>
              Sur la contestation du refus de transmission de la question prioritaire de constitutionnalité opposé à M. J... : <br/>
<br/>
              2. Aux termes de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Devant les juridictions relevant du Conseil d'Etat ou de la Cour de cassation, le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est, à peine d'irrecevabilité, présenté dans un écrit distinct et motivé ". L'article 23-2 de la même ordonnance dispose que : " (...) Le refus de transmettre la question ne peut être contesté qu'à l'occasion d'un recours contre la décision réglant tout ou partie du litige ". Selon l'article 23-5 de cette ordonnance : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat ou la Cour de cassation. Le moyen est présenté, à peine d'irrecevabilité, dans un mémoire distinct et motivé (...) ". Aux termes de l'article R. 771-16 du code de justice administrative : " Lorsque l'une des parties entend contester devant le Conseil d'Etat, à l'appui d'un appel ou d'un pourvoi en cassation formé contre la décision qui règle tout ou partie du litige, le refus de transmission d'une question prioritaire de constitutionnalité précédemment opposé, il lui appartient, à peine d'irrecevabilité, de présenter cette contestation avant l'expiration du délai de recours dans un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission. / La contestation du refus de transmission par la voie du recours incident doit, de même, faire l'objet d'un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission ".<br/>
<br/>
              3. Il résulte de ces dispositions que, lorsqu'un tribunal administratif a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité qui lui a été soumise, il appartient à l'auteur de cette question de contester ce refus, à l'occasion de l'appel formé contre le jugement qui statue sur le litige, dans le délai de recours contentieux et par un mémoire distinct et motivé, que le refus de transmission précédemment opposé l'ait été par une décision distincte du jugement, dont il joint alors une copie, ou directement par ce jugement. Une telle contestation peut être formée sans condition de délai par le défendeur à l'appel, par la voie du recours incident. Les dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 n'ont ni pour objet ni pour effet de permettre à celui qui a déjà présenté une question prioritaire de constitutionnalité devant une juridiction statuant en première instance de s'affranchir des conditions, définies par les dispositions citées plus haut de la loi organique et du code de justice administrative, selon lesquelles le refus de transmission peut être contesté devant le juge d'appel puis, le cas échéant, devant le juge de cassation. <br/>
<br/>
              4. Si, alors que la voie du recours incident n'est pas ouverte en matière électorale, la contestation du refus de transmission d'une question prioritaire de constitutionnalité opposé par le tribunal administratif peut être formée par le défendeur à l'appel, auteur de cette question, sans condition de délai, cette contestation reste soumise aux conditions définies par les dispositions citées au point 2. Par suite, faute d'avoir été présentée dans un mémoire distinct, la contestation par M. J... du refus opposé par le tribunal administratif de Toulon de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 6° de l'article L. 231 du code électoral est irrecevable. <br/>
<br/>
              Sur la régularité de la procédure : <br/>
<br/>
              5. Il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral que, par dérogation aux dispositions de l'article R. 611-1 du code de justice administrative, les tribunaux administratifs ne sont pas tenus de communiquer aux auteurs des protestations les mémoires en défense des conseillers municipaux dont l'élection est contestée, non plus que les autres mémoires ultérieurement enregistrés, et qu'il appartient seulement aux parties, si elles le jugent utile, de prendre connaissance de ces défenses et mémoires ultérieurs au greffe du tribunal administratif. <br/>
<br/>
              6. Il résulte de l'instruction que par un courrier daté du 8 août 2020, M. D... a demandé au greffe du tribunal administratif de Toulon de lui communiquer le mémoire en défense présenté par M. J... et enregistré le 16 juillet 2020. Si M. D... soutient, par ailleurs, qu'il n'a pas pu en prendre connaissance alors qu'un confrère de son avocate s'est présenté à cette fin au greffe du tribunal administratif le lundi 31 août 2020, il n'apporte aucun élément susceptible d'étayer cette allégation, et ne fait en particulier pas état d'une demande écrite qui aurait été adressée en ce sens auprès du greffe du tribunal, la mention de cette impossibilité n'ayant été formulée que dans une note en délibéré datée du 7 septembre 2020. Dans ces conditions, M. D... n'est pas fondé à soutenir que le tribunal administratif de Toulon aurait méconnu le principe du contradictoire en s'abstenant de lui communiquer le mémoire en défense de M. J... ou en ne le mettant pas à sa disposition. <br/>
<br/>
              Sur le grief tiré de l'inéligibilité de M. J... : <br/>
<br/>
              7. Aux termes de l'article L. 231 du code électoral : " (...) Ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : (...) 6° (...) les entrepreneurs de services municipaux ".<br/>
<br/>
              8. Il résulte de l'instruction, d'une part, que si, par une convention du 23 octobre 1981, l'Etat a concédé l'établissement et l'exploitation du port de plaisance Port-Grimaud II pour une durée de quarante-trois ans à la SCI La Baie de Saint-Tropez et à l'association syndicale libre Port-Grimaud II, cette association assure désormais la gestion du service public portuaire pour le compte de la commune de Grimaud, dès lors que la compétence de l'Etat en matière de gestion de ce port de plaisance a été transférée à la commune à compter du 1er janvier 1984 en application des dispositions de la loi du 22 juillet 1983, cette compétence de la commune résultant désormais des dispositions de l'article 30 de la loi du 13 août 2004 relative aux libertés et responsabilités locales en vertu desquelles la collectivité bénéficiaire du transfert succède à l'Etat dans l'ensemble de ses droits et obligations à l'égard des tiers. D'autre part, en qualité de président de l'association syndicale libre Port-Grimaud II, M. J... représente l'association, dirige et anime le comité de gestion et fait exécuter les décisions prises par les assemblées générales ou le syndicat. Il assure ainsi, au sein de l'association, un rôle prédominant. Dans ces conditions, les circonstances que, d'une part, cette association soit sans but lucratif et que, d'autre part, M. J... y exerce ses fonctions à titre bénévole étant indifférentes à cet égard, M. J... doit être regardé comme un entrepreneur de services municipaux au sens des dispositions précitées du 6° de l'article L. 231 du code électoral, qui sont, contrairement à ce que fait valoir l'intéressé, suffisamment précises. <br/>
<br/>
              9. Il résulte de ce qui précède que M. D... est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulon a écarté le grief tiré de la méconnaissance de ces dispositions. <br/>
<br/>
              10. Il appartient toutefois au Conseil d'Etat, saisi par l'effet dévolutif de l'appel, de se prononcer sur les fins de non-recevoir opposées en défense par M. J.... Il résulte de l'instruction, en premier lieu, que M. D... justifie de sa qualité d'électeur inscrit sur les listes de la commune de Grimaud, en deuxième lieu, que sa protestation ayant été présentée par le biais de l'application Télérecours, elle doit être regardée, en vertu des dispositions de l'article R. 611-8-4 du code de justice administrative, comme comportant la signature de l'avocate le représentant et, en troisième lieu, que la circonstance que la commune n'ait pas été mise en cause dans l'instance alors que M. D... demanderait l'annulation de l'ensemble des opérations électorales, ce qui, au demeurant, n'est pas le cas, sa protestation tendant seulement à l'annulation de l'élection de M. J..., est en tout état de cause sans incidence sur la recevabilité de sa protestation. <br/>
<br/>
              11. Ainsi qu'il a été dit au point 8, M. J... doit être regardé comme un entrepreneur de services municipaux. Par suite, M. D... est fondé à demander l'annulation du jugement attaqué en tant qu'il a rejeté ses conclusions tendant à l'annulation de l'élection de M. J... en qualité de conseiller municipal de la commune de Grimaud. <br/>
<br/>
              12. En application des dispositions du premier alinéa de l'article L. 270 du code électoral, il y a lieu pour le Conseil d'Etat de proclamer élue Mme F... K..., inscrite sur la liste où figurait M. B... J..., immédiatement après lui.<br/>
<br/>
              Sur les conclusions du recours incident présentées par M. J... : <br/>
<br/>
              13. Les conclusions présentées en appel par M. J... tendant à l'annulation de l'élection de M. E... H... ont le caractère d'un recours incident qui, ainsi qu'il a été rappelé au point 4, n'est pas ouvert en matière électorale. Elles sont par suite et en tout état de cause irrecevables.<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              14. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. D... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de M. D... qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du 15 septembre 2020 du tribunal administratif de Toulon est annulé.<br/>
Article 2 : L'élection de M. B... J... en qualité de conseiller municipal de la commune de Grimaud est annulée. <br/>
Article 3 : Mme F... K... est proclamée élue en qualité de conseillère municipale de la commune de Grimaud.<br/>
Article 4 : Les conclusions présentées par M. D... et M. J... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. A... D..., à M. B... J..., à Mme F... K... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-02-02-05 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. ÉLIGIBILITÉ. INÉLIGIBILITÉS. ENTREPRENEURS DE SERVICES MUNICIPAUX. - NOTION - 1) INCLUSION - PERSONNE EXERÇANT UN RÔLE PRÉDOMINANT [RJ2] DANS L'ASSOCIATION GESTIONNAIRE, POUR LE COMPTE DE LA COMMUNE, DU SERVICE PUBLIC PORTUAIRE [RJ3] - 2) CIRCONSTANCES SANS INCIDENCE - ASSOCIATION À BUT NON LUCRATIF - CARACTÈRE BÉNÉVOLE DES FONCTIONS [RJ4].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-06 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. VOIES DE RECOURS. - RECEVABILITÉ EN APPEL DE LA CONTESTATION PAR L'INTIMÉ, AUTEUR D'UNE QPC EN PREMIÈRE INSTANCE, DU REFUS DE TRANSMETTRE CELLE-CI - 1) EXISTENCE [RJ1] - 2) CONDITIONS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10-10 PROCÉDURE. - CONTENTIEUX ÉLECTORAL - RECEVABILITÉ EN APPEL D'UNE CONTESTATION ÉMANANT DE L'INTIMÉ, AUTEUR DE LA QPC - 1) EXISTENCE [RJ1] - 2) CONDITIONS.
</SCT>
<ANA ID="9A"> 28-04-02-02-05 1) Association assurant la gestion du service public portuaire pour le compte de la commune. Président de l'association, qui la représente, dirige et anime le comité de gestion et fait exécuter les décisions prises par les assemblées générales ou le syndicat, y assurant ainsi un rôle prédominant.,,,Président devant dans ces conditions être regardé comme un entrepreneur des services municipaux au sens du 6° de l'article L. 231 du code électoral.... ,,2) Les circonstances que, d'une part, cette association soit sans but lucratif et que, d'autre part, le président y exerce ses fonctions à titre bénévole, sont indifférentes à cet égard.</ANA>
<ANA ID="9B"> 28-08-06 1) Alors que la voie du recours incident n'est pas ouverte en matière électorale, la contestation du refus de transmission d'une question prioritaire de constitutionnalité opposé par le tribunal administratif peut être formée par le défendeur à l'appel, auteur de cette question, sans condition de délai.,,,2) Cette contestation reste soumise aux conditions définies par les articles 23-1, 23-2 et 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et par l'article R. 771-16 du code de justice administrative (CJA), et notamment celle d'être présentée dans un mémoire distinct.</ANA>
<ANA ID="9C"> 54-10-10 1) Alors que la voie du recours incident n'est pas ouverte en matière électorale, la contestation du refus de transmission d'une question prioritaire de constitutionnalité opposé par le tribunal administratif peut être formée par le défendeur à l'appel, auteur de cette question, sans condition de délai.,,,2) Cette contestation reste soumise aux conditions définies par les articles 23-1, 23-2 et 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et par l'article R. 771-16 du code de justice administrative (CJA), et notamment celle d'être présentée dans un mémoire distinct.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la contestation d'un refus de transmission par la voie de l'appel incident en contentieux général, CE, 30 novembre 2020, SAS Société de gestion La Rotonde Montparnasse, n° 443970, T. pp. 961-963.,,[RJ2] Cf., CE, 18 décembre 1996, Elections municipales de Gérardmer (Vosges), élection du maire et d'un adjoint au maire de Gérardmer, n° 174907, p. 506.,,[RJ3] Cf., CE, 29 novembre 1996, Elections municipales d'Antibes, n° 176974, T. p. 898.,,[RJ4] Cf. CE, 20 mars 1996, Elections municipales de la Bollène-Vésubie, n° 173673, T. p. 899.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
