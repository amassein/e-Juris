<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038451682</ID>
<ANCIEN_ID>JG_L_2019_04_000000429668</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/45/16/CETATEXT000038451682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/04/2019, 429668, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429668</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429668.20190423</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C..., agissant en son nom personnel et au nom de ses enfants mineurs, et E...A...D..., ont demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au ministre de l'Europe et des affaires étrangères d'organiser le rapatriement en France de Mme C...et de ses enfants dans un délai de huit jours ou, à défaut, d'enjoindre au ministre de l'Europe et des affaires étrangères de réexaminer leur situation dans un délai de deux mois. Par une ordonnance n° 1906076/9 du 9 avril 2019 dont Mme C...et Mme D...ont fait appel, le juge des référés du tribunal administratif de Paris, statuant dans les conditions prévues au dernier alinéa de l'article L. 511-2 du code de justice administrative, a rejeté leur demande. <br/>
              Par une requête et un mémoire complémentaire, enregistrés les 11 et 15 avril 2019, au secrétariat du contentieux du Conseil d'Etat, Mme C...et Mme D... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'annuler l'ordonnance contestée ;<br/>
<br/>
              2°) d'enjoindre à l'Etat d'organiser le rapatriement en France de Mme C...et de ses enfants dans un délai de huit jours ou, à défaut, de réexaminer leur situation dans un délai de deux mois ; <br/>
              3°) de saisir la Cour européenne des droits de l'homme, sur le fondement de l'article 1er du protocole n° 16 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, d'une demande d'avis portant sur la conformité, d'une part, de la théorie des actes de gouvernement par rapport aux articles 6 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et, d'autre part, du refus de rapatrier une mère et ses enfants eu égard aux articles 2, 3 et 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales combinés avec l'article 1 de cette même convention ;<br/>
<br/>
              4°) de poser une question préjudicielle à la Cour de justice de l'Union européenne portant sur la conformité de la théorie des actes de gouvernement par rapport aux droits protégés par les Charte des droits fondamentaux ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens. <br/>
<br/>
<br/>
              Elles soutiennent que : <br/>
              - le juge des référés a entaché son ordonnance d'une erreur de droit dès lors qu'il a considéré que le litige dont il était saisi échappait à la compétence de la juridiction administrative française en application de la théorie de l'acte de gouvernement alors que, face à la violation d'un droit fondamental, le droit à un recours effectif prévaut sur la théorie des actes de gouvernement ;<br/>
              - le juge administratif est compétent pour connaître d'une décision de refus de rapatriement dès lors que cette décision est un acte détachable de la conduite des relations extérieures de la France ;<br/>
              - la condition de l'urgence est remplie dès lors que Mme C...et ses trois enfants mineurs sont retenus dans le camp de Roj où ils se trouvent exposés à des traitements inhumains et dégradants et à un danger imminent pour leur vie ; <br/>
              - il est porté une atteinte grave et manifestement illégale à leur droit à la vie et à leur droit de ne pas subir des traitements inhumains et dégradants ;<br/>
              - il est porté une atteinte grave et manifestement illégale à leur droit à la liberté dès lors qu'elle et ses enfants sont privés de leur liberté de mouvement sans qu'aucune condamnation n'ait été prononcée à leur encontre par une autorité légitime ;<br/>
              - il est porté une atteinte grave et manifestement illégale à leur droit au retour sur le territoire national ;<br/>
              - la France peut mettre un terme aux violations des droits fondamentaux des ressortissants français détenus dans le nord-est de la Syrie et obtenir leur rapatriement dès lors que, d'une part, elle dispose de moyens opérationnels sur place et, d'autre part, elle est susceptible d'exercer une influence sur les forces kurdes ;<br/>
              - l'intérêt supérieur de l'enfant commande que Mme C...ne soit pas séparée de ses enfants et, partant, qu'ils ne puissent être rapatriés sans elle. <br/>
              Par un mémoire en défense, enregistré le 18 avril 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'Europe et des affaires étrangères conclut au rejet de la requête.  <br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme C...et Mme D...et, d'autre part, le ministre de l'Europe et des affaires étrangères ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 19 avril 2019 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Mathonnet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mmes C...et D...;<br/>
<br/>
              - le représentant de Mmes C...etD... ;<br/>
<br/>
              - les représentants du ministre de l'Europe et des affaires étrangères ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Mme C...et Mme D...ont demandé au juge des référés du tribunal administratif de Paris d'enjoindre à l'Etat, à titre principal, de procéder au rapatriement de MmeC..., retenue avec ses trois enfants mineurs dans le camp de Roj situé dans le nord-est de la Syrie. Par une ordonnance du 9 avril 2019 dont Mme C...et Mme D...ont fait appel, le juge des référés du tribunal administratif de Paris, statuant dans les conditions prévues au dernier alinéa de l'article L. 511-2 du code de justice administrative, a rejeté leur demande.<br/>
<br/>
              3. La requête de Mme C...et de Mme D...a pour objet soit que l'Etat intervienne auprès d'autorités étrangères sur un territoire étranger afin d'organiser le rapatriement en France de ressortissants, soit qu'il s'efforce de prendre lui-même des mesures pour assurer leur retour à partir d'un territoire hors sa souveraineté. Les mesures ainsi demandées en vue d'un rapatriement, qui ne peut être rendu possible par la seule délivrance d'un titre leur permettant de franchir les frontières françaises, ainsi que cela a été demandé à l'audience, nécessiteraient l'engagement de négociations avec des autorités étrangères ou une intervention sur un territoire étranger. Elles ne sont pas détachables de la conduite des relations internationales de la France. En conséquence, une juridiction n'est pas compétente pour en connaître.<br/>
<br/>
              4. Les conclusions de Mme C...et de Mme D...tendant à ce qu'une somme soit mise à la charge de l'Etat en application des dispositions de l'article L 761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme C...et de Mme D...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...C..., à Mme A...D...et au ministre de l'Europe et des affaires étrangères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
