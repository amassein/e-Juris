<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033657442</ID>
<ANCIEN_ID>JG_L_2016_12_000000399925</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/65/74/CETATEXT000033657442.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 16/12/2016, 399925, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399925</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:399925.20161216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement du 11 janvier 2016, le juge de proximité d'Ivry-sur-Seine a sursis à statuer sur la demande du Conseil national de l'ordre des masseurs-kinésithérapeutes tendant à la condamnation de Mme B...A...pour défaut d'acquittement de ses cotisations et a invité les parties à saisir le juge administratif de la question de la légalité des délibérations du Conseil national de l'ordre des masseurs-kinésithérapeutes relatives à la fixation des cotisations des personnes inscrites au tableau de l'ordre. <br/>
<br/>
              Par deux mémoires, enregistrés les 19 mai et 11 août 2016 au secrétariat du contentieux, agissant en exécution de ce jugement, le Conseil national de l'ordre des masseurs-kinésithérapeutes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'apprécier la légalité des délibérations relatives à la fixation, pour les années 2009 à 2014, des cotisations des personnes inscrites au tableau de l'ordre et de déclarer que ces délibérations ne sont pas entachées d'illégalité ;<br/>
<br/>
              2°) de mettre à la charge de Mme A...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2006-270 du 7 mars 2006 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que le Conseil national de l'ordre des masseurs-kinésithérapeutes, reprochant à MmeA..., masseur-kinésithérapeute inscrite au tableau de l'ordre depuis 2008, de n'avoir pas versé ses cotisations annuelles de 2009 à 2014, a saisi le juge de proximité d'Ivry-sur-Seine pour obtenir la condamnation de l'intéressée au paiement de ces arriérés. Le juge de proximité a décidé de surseoir à statuer pour que les parties posent à la juridiction administrative la question de la légalité des délibérations par lesquelles le Conseil national de l'ordre des masseurs-kinésithérapeutes a fixé le montant de ces cotisations. En exécution de ce jugement, le Conseil national de l'ordre des masseurs-kinésithérapeutes a saisi le Conseil d'Etat de conclusions tendant à ce qu'il déclare légales ces délibérations.<br/>
<br/>
              Sur l'étendue de la question préjudicielle :<br/>
<br/>
              2. En vertu des principes généraux relatifs à la répartition des compétences entre les deux ordres de juridiction, il n'appartient pas à la juridiction administrative, lorsqu'elle est saisie d'une question préjudicielle en appréciation de validité d'un acte administratif, de trancher d'autres questions que celle qui lui a été renvoyée par l'autorité judiciaire. Il suit de là que, lorsque la juridiction de l'ordre judiciaire a énoncé dans son jugement le ou les moyens invoqués devant elle qui lui paraissent justifier ce renvoi, la juridiction administrative doit limiter son examen à ce ou ces moyens et ne peut connaître d'aucun autre, fût-il d'ordre public, que les parties viendraient à présenter devant elle à l'encontre de cet acte. Ce n'est que dans le cas où, ni dans ses motifs ni dans son dispositif, la juridiction de l'ordre judiciaire n'a limité la portée de la question qu'elle entend soumettre à la juridiction administrative, que cette dernière doit examiner tous les moyens présentés devant elle, sans qu'il y ait lieu alors de rechercher si ces moyens avaient été invoqués dans l'instance judiciaire.<br/>
<br/>
              3. Avant de surseoir à statuer, le juge de proximité d'Ivry-sur-Seine a relevé, dans les motifs de son jugement, qu'il appartenait à la juridiction administrative d'apprécier la légalité des décisions prises par le Conseil national de l'ordre des masseurs-kinésithérapeutes sur le fondement de l'article L. 4321-16 du code de la santé publique " en l'absence du décret d'application prévu par l'article L. 4321-20 de ce code ". En mentionnant ce seul moyen, il a défini et limité l'étendue de la question qu'il entendait soumettre à la juridiction administrative. <br/>
<br/>
              4. Il en résulte que Mme A...n'est recevable à soumettre au Conseil d'Etat ni, comme le relève le Conseil national de l'ordre des masseurs-kinésithérapeutes, l'appréciation de la légalité et l'opposabilité de son règlement de trésorerie, ni l'examen des moyens tirés de l'irrégularité de la composition du Conseil national et de l'illégalité de la fixation d'un montant de cotisation pour des professionnels qui ont cessé d'exercer. <br/>
<br/>
              Sur la légalité des délibérations du Conseil national de l'ordre des masseurs-kinésithérapeutes :<br/>
<br/>
              5. Aux termes de l'article L. 4321-16 du code de la santé publique : " Le conseil national fixe le montant de la cotisation qui doit être versée à l'ordre des masseurs-kinésithérapeutes par chaque personne physique ou morale inscrite au tableau. Il détermine également les quotités de cette cotisation qui seront attribuées à l'échelon départemental, régional et national (...) ". Aux termes de l'article L. 4321-20 du même code : " Un décret en Conseil d'Etat détermine les modalités d'application des dispositions des articles L. 4321-15 à L. 4321-19, notamment la représentation des professionnels dans les instances ordinales en fonction du mode d'exercice et des usagers dans les chambres disciplinaires ainsi que l'organisation de la procédure disciplinaire préalable à la saisine des chambres disciplinaires ".<br/>
<br/>
              6. L'article L. 4321-16 du code de la santé publique, cité ci-dessus, confie au Conseil national de l'ordre des masseurs-kinésithérapeutes le pouvoir de fixer, par une délibération, le montant des cotisations annuelles dues par les masseurs-kinésithérapeutes. L'application de ces dispositions n'est pas manifestement impossible en l'absence de mesures règlementaires d'application. Dès lors, contrairement à ce que soutenait Mme A...devant le juge de proximité d'Ivry-sur-Seine, le Conseil national de l'ordre des masseurs-kinésithérapeutes pouvait légalement, sur le fondement de l'article L. 4321-16 du code de la santé publique, fixer par les délibérations contestées le montant des cotisations annuelles dues par les masseurs-kinésithérapeutes. Par suite, le Conseil national de l'ordre des masseurs-kinésithérapeutes est fondé à soutenir que l'exception d'illégalité des délibérations par lesquelles il a fixé le montant des cotisations annuelles dues par les masseurs-kinésithérapeutes, soulevée par Mme A...devant le juge de proximité d'Ivry-sur-Seine, n'est pas fondée.<br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par ce Conseil au titre du même article.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est déclaré que l'exception d'illégalité des délibérations du Conseil national de l'ordre des masseurs-kinésithérapeutes soulevée par Mme A...devant le juge de proximité d'Ivry-sur-Seine n'est pas fondée.<br/>
Article 2 : Le surplus des conclusions de Mme A...est rejeté.<br/>
Article 3 : Les conclusions du Conseil national de l'ordre des masseurs-kinésithérapeutes présentées au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au Conseil national de l'ordre des masseurs-kinésithérapeutes et à Mme B...A....<br/>
Copie en sera adressé au juge de proximité d'Ivry-sur-Seine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
