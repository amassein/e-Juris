<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032790104</ID>
<ANCIEN_ID>JG_L_2016_06_000000386165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/79/01/CETATEXT000032790104.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 27/06/2016, 386165, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP SEVAUX, MATHONNET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2016:386165.20160627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... C...et Mme A... D...ont demandé au juge des référés du tribunal administratif de Poitiers de condamner le centre hospitalier universitaire (CHU) de Poitiers à verser à eux-mêmes et à leurs fils une provision au titre de la réparation des préjudices ayant résulté des conditions de prise en charge de l'accouchement de Mme D...le 13 janvier 2010 et de l'absence d'information sur les risques et bénéfices d'une éventuelle tentative d'accouchement par voie basse ou d'une césarienne. Par une ordonnance n° 1301569 du 5 novembre 2013, le juge des référés a rejeté leur demande.<br/>
<br/>
              Par une ordonnance n° 13BX03148 du 14 novembre 2014, le juge des référés de la cour administrative d'appel de Bordeaux a, à la demande de M. B... C...et Mme A...D..., annulé cette ordonnance et condamné le CHU de Poitiers à leur verser une provision de 25 000 euros.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 2 décembre et 17 décembre 2014, le 29 décembre 2015 et le 29 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le CHU de Poitiers demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du centre hospitalier de Poitiers, à la SCP Lyon-Caen, Thiriez, avocat de M. C... et de Mme D...et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que Mme D...a accouché de son deuxième enfant le 14 janvier 2010 au centre hospitalier universitaire (CHU) de Poitiers ; qu'alors que l'accouchement avait été entrepris par voie basse, l'apparition d'anomalies du rythme cardiaque foetal en lien avec une rupture utérine a rendu nécessaire la réalisation en urgence d'une césarienne ; que l'enfant Nathan présente de graves lésions cérébrales consécutives à une encéphalopathie anoxo-ischémique en rapport direct avec la rupture utérine ; que M. C...et Mme D...ont demandé au juge des référés du tribunal administratif de Poitiers de mettre à la charge du CHU le versement d'indemnités provisionnelles ; que cette demande a été rejetée par une ordonnance du 5 novembre 2013 ; que, par l'ordonnance du 14 novembre 2014 contre laquelle le CHU de Poitiers se pourvoit en cassation, le juge des référés de la cour administrative d'appel de Bordeaux, sur appel formé par M. C...et MmeD..., a estimé qu'en s'abstenant d'informer Mme D...du risque de rupture utérine inhérent à un accouchement par voie basse quand un précédent accouchement avait donné lieu à une césarienne, les médecins avaient commis une faute ayant fait perdre à l'intéressée une chance d'éviter cette rupture en demandant qu'une césarienne soit programmée et a condamné l'établissement à leur verser une provision de 25 000 euros au titre des préjudices subis par l'enfant, ses parents et son frère ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1111-2 du code de la santé publique : " Toute personne a le droit d'être informée sur son état de santé. Cette information porte sur les différentes investigations, traitements ou actions de prévention qui sont proposés, leur utilité, leur urgence éventuelle, leurs conséquences, les risques fréquents ou graves normalement prévisibles qu'ils comportent ainsi que sur les autres solutions possibles et sur les conséquences prévisibles en cas de refus (...) / Cette information incombe à tout professionnel de santé dans le cadre de ses compétences et dans le respect des règles professionnelles qui lui sont applicables. Seules l'urgence ou l'impossibilité d'informer peuvent l'en dispenser. / Cette information est délivrée au cours d'un entretien individuel. (...) /En cas de litige, il appartient au professionnel ou à l'établissement de santé d'apporter la preuve que l'information a été délivrée à l'intéressé dans les conditions prévues au présent article. Cette preuve peut être apportée par tout moyen " ;<br/>
<br/>
              3. Considérant que la circonstance que l'accouchement par voie basse constitue un événement naturel et non un acte médical ne dispense pas les médecins de l'obligation de porter, le cas échéant, à la connaissance de la femme enceinte les risques qu'il est susceptible de présenter eu égard notamment à son état de santé, à celui du foetus ou à ses antécédents médicaux,  et les moyens de les prévenir ; qu'en particulier, en présence d'une pathologie de la mère ou de l'enfant à naître ou d'antécédents médicaux entraînant un risque connu en cas d'accouchement par voie basse, l'intéressée doit être informée de ce risque ainsi que de la possibilité de procéder à une césarienne et des risques inhérents à une telle intervention ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le premier accouchement de Mme D...s'était fait par césarienne ; que l'expert indiquait qu'en pareil cas, l'accouchement par voie basse comporte un risque connu de rupture utérine, évalué à 1 %, et qu'un tel accident, s'il survient, peut avoir de très graves conséquences pour l'enfant si une césarienne réalisée en urgence ne permet pas son extraction dans les plus brefs délais ; que l'expert mentionnait également que le risque de rupture utérine est moindre en cas d'accouchement par césarienne ; que la cour a constaté que le CHU de Poitiers n'établissait pas avoir dispensé à Mme D...une information sur ce risque en cas d'accouchement par voie basse et de césarienne ; qu'en retenant que s'il était probable que l'intéressée, informée des risques inhérents à chacune des voies, aurait opté pour un accouchement par voie basse, le défaut d'information avait néanmoins été à l'origine d'une perte de chance d'éviter le dommage et qu'il en était résulté pour l'ensemble des demandeurs une créance non sérieusement contestable d'un montant de 25 000 euros, le juge des référés de la cour administrative d'appel de Bordeaux, qui a suffisamment motivé son ordonnance, n'a pas commis d'erreur de droit et a porté sur les faits de l'espèce une appréciation souveraine, exempte de dénaturation ; qu'il n'a pas non plus commis d'erreur de droit en se fondant sur la méconnaissance de l'obligation d'information sur le risque général de rupture utérine, consécutif à une précédente césarienne, alors même que la rupture utérine survenue en l'espèce ne correspondait pas au cas plus le plus fréquent de survenance de ce risque ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le CHU de Poitiers n'est pas fondé à demander l'annulation de l'ordonnance du 14 novembre 2014 ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier universitaire de Poitiers, au titre de l'article L. 761-1 du code de justice administrative, le versement d'une somme globale de 5 000 euros à M. C...et Mme D...;  qu'en revanche, doivent être rejetées les conclusions présentées au titre des mêmes dispositions par l'ONIAM, qui, appelé en la cause pour présenter des observations, n'est pas partie à la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi du centre hospitalier universitaire de Poitiers est rejeté.<br/>
<br/>
Article 2 : Le centre hospitalier universitaire de Poitiers versera la somme globale de 5 000 euros à M. C...et Mme D...au titre de l'article L.  761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions présentées par l'ONIAM au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier universitaire de Poitiers, à M. B... C..., à Mme A...D..., à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la caisse primaire d'assurances maladie de la Vienne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - OBLIGATION D'INFORMATION DU PATIENT (ART. L. 1111-2 DU CSP) - CHAMP D'APPLICATION - ACCOUCHEMENT PAR VOIE BASSE - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. EXISTENCE D'UNE FAUTE. MANQUEMENTS À UNE OBLIGATION D'INFORMATION ET DÉFAUTS DE CONSENTEMENT. - OBLIGATION D'INFORMATION DU PATIENT (ART. L. 1111-2 DU CSP) - CHAMP D'APPLICATION - ACCOUCHEMENT PAR VOIE BASSE - INCLUSION.
</SCT>
<ANA ID="9A"> 55-03-01-02 La circonstance que l'accouchement par voie basse constitue un événement naturel et non un acte médical ne dispense pas les médecins, en application de l'article L. 1111-2 du code de la santé publique (CSP) de l'obligation de porter, le cas échéant, à la connaissance de la femme enceinte les risques qu'il est susceptible de présenter eu égard notamment à son état de santé, à celui du foetus ou à ses antécédents médicaux, et les moyens de les prévenir. En particulier, en présence d'une pathologie de la mère ou de l'enfant à naître ou d'antécédents médicaux entraînant un risque connu en cas d'accouchement par voie basse, l'intéressée doit être informée de ce risque ainsi que de la possibilité de procéder à une césarienne et des risques inhérents à une telle intervention.</ANA>
<ANA ID="9B"> 60-02-01-01-01-01-04 La circonstance que l'accouchement par voie basse constitue un événement naturel et non un acte médical ne dispense pas les médecins, en application de l'article L. 1111-2 du code de la santé publique (CSP) de l'obligation de porter, le cas échéant, à la connaissance de la femme enceinte les risques qu'il est susceptible de présenter eu égard notamment à son état de santé, à celui du foetus ou à ses antécédents médicaux, et les moyens de les prévenir. En particulier, en présence d'une pathologie de la mère ou de l'enfant à naître ou d'antécédents médicaux entraînant un risque connu en cas d'accouchement par voie basse, l'intéressée doit être informée de ce risque ainsi que de la possibilité de procéder à une césarienne et des risques inhérents à une telle intervention.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
