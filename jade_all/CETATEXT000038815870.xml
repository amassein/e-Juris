<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815870</ID>
<ANCIEN_ID>JG_L_2019_07_000000428852</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815870.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 24/07/2019, 428852</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428852</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:428852.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1813376 du 28 février 2019, le président du tribunal administratif de Cergy-Pontoise a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par la société par actions simplifiée (SAS) Total Réunion.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Cergy-Pontoise le 20 décembre 2018, la SAS Total Réunion demande :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 28 juin 2018 du ministre d'Etat, ministre de la transition écologique et solidaire procédant au retrait de la décision de délivrance du certificat d'économies d'énergie référencé sous le numéro PR021116STAS788723203A0 et au retrait, sur son compte dans le registre national des certificats d'économies d'énergie, du volume de certificats d'économies d'énergie correspondant, ainsi que de la décision implicite de rejet de son recours gracieux du 21 août 2018 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Total Réunion ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que la société Total Réunion était soumise, en sa qualité de fournisseur d'énergie, à des obligations d'économies d'énergie, au titre de la troisième période comprise entre le 1er janvier 2015 et le 31 décembre 2017, dont elle s'est notamment acquittée en achetant le 5 décembre 2016 à la société BHC, filiale à 100% du groupe Total, une fraction du certificat d'économies d'énergie référencé sous le numéro PR021116STAS788723203A0, que cette société avait elle-même acheté le 15 novembre 2016 auprès du premier détenteur auquel il avait été délivré. Par un courrier du 28 mai 2018, le ministre d'Etat, ministre de la transition écologique et solidaire a informé la société Total Réunion que la décision de délivrance de ce certificat avait été obtenue de manière frauduleuse et qu'il envisageait par conséquent, en application de l'article L. 241-2 du code des relations entre le public et l'administration, le retrait de cette décision ainsi que le retrait, sur son compte dans le registre national des certificats d'économies d'énergie, du volume de certificats correspondant puis, par un courrier du 28 juin 2018, lui a notifié une décision en ce sens. La société Total Réunion demande l'annulation de cette décision et de la décision implicite rejetant le recours gracieux qu'elle avait formé contre celle-ci.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 221-5 du code de l'énergie : " Les certificats d'économies d'énergie sont des biens meubles négociables, dont l'unité de compte est le kilowattheure d'énergie finale économisé. Ils peuvent être détenus, acquis ou cédés par toute personne mentionnée aux 1° à 6° de l'article L. 221-7 ou par toute autre personne morale (...) ". Selon l'article L. 221-7 du même code : " Le ministre chargé de l'énergie ou, en son nom, un organisme habilité à cet effet peut délivrer des certificats d'économies d'énergie aux personnes éligibles lorsque leur action, additionnelle par rapport à leur activité habituelle, permet la réalisation d'économies d'énergie sur le territoire national d'un volume supérieur à un seuil fixé par arrêté du ministre chargé de l'énergie (...) ". Selon l'article L. 222-1 du même code : " Dans les conditions définies aux articles suivants, le ministre chargé de l'énergie peut sanctionner les manquements aux dispositions du chapitre Ier du présent titre ou aux dispositions réglementaires prises pour leur application ". L'article L. 222-2 du même code dispose que : " Le ministre met l'intéressé en demeure de se conformer à ses obligations dans un délai déterminé. Il peut rendre publique cette mise en demeure. / Lorsque l'intéressé ne se conforme pas dans les délais fixés à cette mise en demeure, le ministre chargé de l'énergie peut : 1° Prononcer à son encontre une sanction pécuniaire (...) ; / 2° Le priver de la possibilité d'obtenir des certificats d'économies d'énergie (...) ; / 3° Annuler des certificats d'économies d'énergie de l'intéressé, d'un volume égal à celui concerné par le manquement ; / 4° Suspendre ou rejeter les demandes de certificats d'économies d'énergie faites par l'intéressé. (...) ". Enfin, selon l'article R. 222-12 du même code : " Les décisions du ministre chargé de l'énergie prononçant les sanctions prévues à l'article L. 222-2 peuvent faire l'objet d'un recours de pleine juridiction et d'une demande de référé tendant à la suspension de leur exécution devant le Conseil d'Etat. Cette demande a un caractère suspensif ".<br/>
<br/>
              3. D'autre part, selon l'article L. 241-2 du code des relations entre le public et l'administration : " Par dérogation aux dispositions du présent titre, un acte administratif unilatéral obtenu par fraude peut être à tout moment abrogé ou retiré ".<br/>
<br/>
              4. En prenant la décision attaquée, qui prononce le retrait de la décision délivrant les certificats d'économies d'énergie litigieux au premier détenteur ainsi que le " retrait ", sur le compte de la société requérante, des certificats correspondants, le ministre n'a pas infligé une sanction en faisant application des dispositions des articles L. 222-1 et suivants du code de l'énergie, permettant de sanctionner les manquements aux dispositions du chapitre Ier du titre II du livre II du même code, mais a entendu retirer une décision obtenue par fraude et tirer les conséquences de ce retrait, en se fondant sur les dispositions de l'article L. 241-2 du code des relations entre le public et l'administration. Dès lors, la décision attaquée n'est pas au nombre des décisions de sanction qui, en vertu de l'article R. 222-12 du même code, peuvent être contestées devant le Conseil d'Etat statuant en premier ressort. Le recours pour excès de pouvoir dirigé contre cette décision relève, en premier ressort, de la compétence des tribunaux administratifs, juges de droit commun du contentieux administratif en vertu de l'article L. 211-1 du code de justice administrative.<br/>
<br/>
              5. Par suite, il y a lieu d'attribuer le jugement de la requête de la société Total Réunion, qui a son siège au Port (La Réunion), au tribunal administratif de La Réunion, compétent pour en connaître en vertu de l'article R. 312-10 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la société Total Réunion est attribué au tribunal administratif de La Réunion. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la SAS Total Réunion, au ministre d'Etat, ministre de la transition écologique et solidaire et au président du tribunal administratif de La Réunion.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. - INCLUSION - REP CONTRE UNE DÉCISION MINISTÉRIELLE RETIRANT DES CERTIFICATS D'ÉCONOMIES D'ÉNERGIE À UNE SOCIÉTÉ AU MOTIF QUE LEUR PREMIER DÉTENTEUR LES A OBTENUS PAR FRAUDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - REP CONTRE UNE DÉCISION MINISTÉRIELLE RETIRANT DES CERTIFICATS D'ÉCONOMIES D'ÉNERGIE À UNE SOCIÉTÉ AU MOTIF QUE LEUR PREMIER DÉTENTEUR LES A OBTENUS PAR FRAUDE - TA DU SIÈGE DE LA SOCIÉTÉ REQUÉRANTE (ART. R 312-10 DU CJA).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">17-05-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. - EXCLUSION - REP CONTRE UNE DÉCISION MINISTÉRIELLE RETIRANT DES CERTIFICATS D'ÉCONOMIES D'ÉNERGIE À UNE SOCIÉTÉ AU MOTIF QUE LEUR PREMIER DÉTENTEUR LES A OBTENUS PAR FRAUDE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">29 ENERGIE. - REP CONTRE UNE DÉCISION MINISTÉRIELLE RETIRANT DES CERTIFICATS D'ÉCONOMIES D'ÉNERGIE À UNE SOCIÉTÉ AU MOTIF QUE LEUR PREMIER DÉTENTEUR LES A OBTENUS PAR FRAUDE - COMPÉTENCE EN PREMIER RESSORT DU TA DU SIÈGE DE LA SOCIÉTÉ REQUÉRANTE.
</SCT>
<ANA ID="9A"> 17-05-01-01 Société requérante soumise, en sa qualité de fournisseur d'énergie, à des obligations d'économies d'énergie et s'en acquittant en rachetant à une société tierce des certificats d'économie d'énergie, acquis par celle-ci auprès du premier détenteur auquel ils avaient été délivrés.... ,,En prenant une décision prononçant le retrait de la décision délivrant les certificats d'économies d'énergie litigieux au premier détenteur ainsi que le retrait, sur le compte de la société requérante, des certificats correspondants, le ministre n'a pas infligé une sanction en faisant application des articles L. 222-1 et suivants du code de l'énergie, permettant de sanctionner les manquements aux dispositions du chapitre Ier du titre II du livre II du même code, mais a entendu retirer une décision obtenue par fraude et tirer les conséquences de ce retrait, en se fondant sur l'article L. 241-2 du code des relations entre le public et l'administration (CRPA). Dès lors, la décision attaquée n'est pas au nombre des décisions de sanction qui, en vertu de l'article R. 222-12 du code de l'énergie, peuvent être contestées devant le Conseil d'Etat statuant en premier ressort. Le recours pour excès de pouvoir dirigé contre cette décision relève, en premier ressort, de la compétence des tribunaux administratifs, juges de droit commun du contentieux administratif en vertu de l'article L. 211-1 du code de justice administrative (CJA).</ANA>
<ANA ID="9B"> 17-05-01-02 Le recours pour excès de pouvoir dirigé contre la décision ministérielle retirant des certificats d'économies d'énergie obtenus par fraude relève, en premier ressort, de la compétence des tribunaux administratifs, juges de droit commun du contentieux administratif en vertu de l'article L. 211-1 du code de justice administrative (CJA). Le jugement de cette requête est attribué, en vertu de l'article R. 312-10 du CJA, au tribunal administratif dans le ressort duquel se trouve le siège de la société requérante.</ANA>
<ANA ID="9C"> 17-05-02 Société requérante soumise, en sa qualité de fournisseur d'énergie, à des obligations d'économies d'énergie et s'en acquittant en rachetant à une société tierce des certificats d'économie d'énergie, acquis par celle-ci auprès du premier détenteur auquel ils avaient été délivrés.... ,,En prenant une décision prononçant le retrait de la décision délivrant les certificats d'économies d'énergie litigieux au premier détenteur ainsi que le retrait, sur le compte de la société requérante, des certificats correspondants, le ministre n'a pas infligé une sanction en faisant application des articles L. 222-1 et suivants du code de l'énergie, permettant de sanctionner les manquements aux dispositions du chapitre Ier du titre II du livre II du même code, mais a entendu retirer une décision obtenue par fraude et tirer les conséquences de ce retrait, en se fondant sur l'article L. 241-2 du code des relations entre le public et l'administration (CRPA). Dès lors, la décision attaquée n'est pas au nombre des décisions de sanction qui, en vertu de l'article R. 222-12 du code de l'énergie, peuvent être contestées devant le Conseil d'Etat statuant en premier ressort. Le recours pour excès de pouvoir dirigé contre cette décision relève, en premier ressort, de la compétence des tribunaux administratifs, juges de droit commun du contentieux administratif en vertu de l'article L. 211-1 du code de justice administrative (CJA).</ANA>
<ANA ID="9D"> 29 Société requérante soumise, en sa qualité de fournisseur d'énergie, à des obligations d'économies d'énergie et s'en acquittant en rachetant à une société tierce des certificats d'économie d'énergie, acquis par celle-ci auprès du premier détenteur auquel ils avaient été délivrés.... ,,En prenant une décision prononçant le retrait de la décision délivrant les certificats d'économies d'énergie litigieux au premier détenteur ainsi que le retrait, sur le compte de la société requérante, des certificats correspondants, le ministre n'a pas infligé une sanction en faisant application des articles L. 222-1 et suivants du code de l'énergie, permettant de sanctionner les manquements aux dispositions du chapitre Ier du titre II du livre II du même code, mais a entendu retirer une décision obtenue par fraude et tirer les conséquences de ce retrait, en se fondant sur l'article L. 241-2 du code des relations entre le public et l'administration (CRPA). Dès lors, la décision attaquée n'est pas au nombre des décisions de sanction qui, en vertu de l'article R. 222-12 du code de l'énergie, peuvent être contestées devant le Conseil d'Etat statuant en premier ressort. Le recours pour excès de pouvoir dirigé contre cette décision relève, en premier ressort, de la compétence des tribunaux administratifs, juges de droit commun du contentieux administratif en vertu de l'article L. 211-1 du code de justice administrative (CJA).,,,Le jugement de cette requête est attribué, en vertu de l'article R. 312-10 du CJA, au tribunal administratif dans le ressort duquel se trouve le siège de la société requérante.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
