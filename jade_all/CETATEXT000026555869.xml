<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555869</ID>
<ANCIEN_ID>JG_L_2012_10_000000354689</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555869.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 29/10/2012, 354689, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354689</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Marc Dandelot</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:354689.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 7 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Bouche Distribution, dont le siège est situé ZI, boulevard de la Marne à Coulommiers (77120) ; la société Bouche Distribution demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite résultant du silence gardé par la Commission nationale d'aménagement commercial sur son recours dirigé contre la décision du 13 mai 2011 de la commission départementale d'aménagement commercial de Seine-et-Marne autorisant la SCI Du Fourneau à créer un ensemble commercial d'enseigne " Intermarché " et de surface de vente totale de 3 521 m², à Coulommiers (Seine-et-Marne) ; <br/>
<br/>
              2°) d'enjoindre à la commission nationale de prendre une nouvelle décision dans un délai de 3 mois après la décision du Conseil d'Etat, en application de l'article L. 911-2 du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public,<br/>
<br/>
<br/>
<br/>1. Considérant que par la décision implicite attaquée, la Commission nationale d'aménagement commercial a accordé à la SCI Du Fourneau l'autorisation préalable requise en vue de la création d'un ensemble commercial d'enseigne " Intermarché " et de surface de vente totale de 3 521 m², à Coulommiers (Seine-et-Marne) ; que la société Bouche Distribution demande l'annulation pour excès de pouvoir de cette décision ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par la SCI Du Fourneau : <br/>
<br/>
              2. Considérant qu'en tant que société par actions simplifiées assimilable à une société anonyme, la société requérante est dotée, par les dispositions combinées des articles L. 225-51-1 et L. 225-56 du code de commerce, de représentants légaux ayant de plein droit qualité pour agir en justice en son nom, parmi lesquels figure son président, M. Bouche, lequel a donné pouvoir à Me Mailhe pour former la présente requête ; que, par suite, la fin de non-recevoir opposée par la SCI Du Fourneau doit être écartée ;<br/>
<br/>
              Sur la légalité de la décision attaquée : <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs : " (...) doivent être motivées les décisions qui (...) rejettent un recours administratif dont la présentation est obligatoire préalablement à tout recours contentieux (...) " ; qu'aux termes de l'article 5 de la même loi : " Une décision implicite intervenue dans les cas où la décision explicite aurait dû être motivée n'est pas illégale du seul fait qu'elle n'est pas assortie de cette motivation. Toutefois, à la demande de l'intéressé, formulée dans les délais du recours contentieux, les motifs de toute décision implicite de rejet devront lui être communiqués dans le mois suivant cette demande (...) " ; <br/>
<br/>
              4. Considérant, d'une part, que la saisine par un tiers, en vertu de l'article L. 752-17 du code de commerce, de la Commission nationale d'aménagement commercial d'un recours contre une décision de la commission départementale d'aménagement commercial constitue un recours administratif préalable obligatoire au sens des dispositions mentionnées ci-dessus de l'article 1er de la loi du 11 juillet 1979 ; qu'il résulte, d'autre part, de ce même article L. 752-17 que le silence gardé pendant quatre mois par la commission nationale sur un recours dont elle est saisie fait naître une décision implicite rejetant ce recours ; qu'ainsi, en cas de demande régulière en ce sens de la part de l'auteur du recours, il appartient à la commission nationale, conformément aux dispositions de l'article 5 de la loi du 11 juillet 1979, de lui communiquer, dans un délai d'un mois, les motifs de la décision implicite née de son silence, à peine d'illégalité de cette décision ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que la société Bouche Distribution a présenté à la commission nationale, le 8 juin 2011, une demande de communication des motifs de sa décision implicite rejetant son recours dirigé contre l'autorisation délivrée le 13 mai 2011 par la commission départementale d'aménagement commercial de Seine-et-Marne ; qu'en s'abstenant de faire droit à cette demande dans le délai d'un mois qui lui était imparti par l'article 5 de la loi du 11 juillet 1979,  la commission nationale a entaché sa décision implicite d'illégalité ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la société Bouche Distribution est fondée à demander l'annulation de la décision attaquée ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 911-2 du code de justice administrative :<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 911-2 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé " ; <br/>
<br/>
              8. Considérant que la présente décision d'annulation implique nécessairement que la Commission nationale d'aménagement commercial se prononce sur la demande initiale d'autorisation d'aménagement commercial présentée par la SCI Du Fourneau ; qu'il y a lieu, par suite, d'enjoindre à la commission nationale de procéder à cet examen dans un délai de quatre mois à compter de la notification de la présente décision ;<br/>
<br/>
              Sur les conclusions de la société Bouche Distribution présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la société requérante au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite, par laquelle la Commission nationale d'aménagement commercial a, d'une part, rejeté le recours formé par la société Bouche Distribution contre la décision du 13 mai 2011 de la commission départementale d'aménagement commercial de Seine-et-Marne autorisant la SCI Du Fourneau à créer un ensemble commercial d'enseigne " Intermarché " à Coulommiers,  et, d'autre part, autorisé la SCI Du Fourneau à créer cet ensemble commercial, est annulée.<br/>
Article 2 : Il est enjoint à la Commission nationale d'aménagement commercial de se prononcer à nouveau sur la demande d'autorisation d'aménagement commercial présentée par la SCI Du Fourneau dans un délai de quatre mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à la société Bouche Distribution la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Bouche Distribution, à la SCI Du Fourneau et à la Commission nationale d'aménagement commercial.<br/>
Copie en sera adressée pour information à la ministre chargée de l'artisanat, du commerce et du tourisme.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
