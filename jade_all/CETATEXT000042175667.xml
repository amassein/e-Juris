<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175667</ID>
<ANCIEN_ID>JG_L_2020_07_000000427713</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175667.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 29/07/2020, 427713, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427713</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:427713.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société en nom collectif (SNC) Veuve A... B... a demandé au tribunal administratif de Paris de prononcer la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés pour la période du 1er janvier 2010 au 31 mai 2013 et de la majoration de 80 % prévue par l'article 1729 du code général des impôts dont ces rappels ont été assortis. Par un jugement nos 1601236, 1609928 du 6 décembre 2017, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18PA00438 du 5 décembre 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par la SNC Veuve A... B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 février et 6 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la SNC Veuve A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Veuve A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du II de l'article L. 47 A du livre des procédures fiscales, dans sa rédaction alors applicable : " En présence d'une comptabilité tenue au moyen de systèmes informatisés et lorsqu'ils envisagent des traitements informatiques, les agents de l'administration fiscale indiquent par écrit au contribuable la nature des investigations souhaitées. Le contribuable formalise par écrit son choix parmi l'une des options suivantes : / a) Les agents de l'administration peuvent effectuer la vérification sur le matériel utilisé par le contribuable ; / b) Celui-ci peut effectuer lui-même tout ou partie des traitements informatiques nécessaires à la vérification. Dans ce cas, l'administration précise par écrit au contribuable, ou à un mandataire désigné à cet effet, les travaux à réaliser ainsi que le délai accordé pour les effectuer. Les résultats des traitements sont alors remis sous forme dématérialisée répondant à des normes fixées par arrêté du ministre chargé du budget ; / c) Le contribuable peut également demander que le contrôle ne soit pas effectué sur le matériel de l'entreprise. Il met alors à la disposition de l'administration les copies des documents, données et traitements soumis à contrôle. (...) ".<br/>
<br/>
              2. Il résulte de ces dispositions que le vérificateur qui envisage un traitement informatique sur une comptabilité tenue au moyen de systèmes informatisés est tenu d'indiquer au contribuable, au plus tard au moment où il décide de procéder au traitement, par écrit et de manière suffisamment précise, la nature des investigations qu'il souhaite effectuer, c'est-à-dire les données sur lesquelles il entend faire porter ses recherches ainsi que l'objet de ces investigations, afin de permettre au contribuable de choisir en toute connaissance de cause entre les trois options offertes par ces dispositions.<br/>
<br/>
              3. Il ressort des pièces de la procédure que, dans son mémoire enregistré au greffe de la cour le 29 juin 2018, la SNC Veuve A... B... soutenait à l'appui de ses conclusions d'appel que la procédure d'imposition avait été menée en méconnaissance du II de l'article L. 47 A du livre des procédures fiscales dès lors, d'une part, que l'administration ne l'avait pas informée de manière suffisamment précise de la nature des investigations qu'elle souhaitait effectuer, en s'abstenant de lui indiquer que les traitements informatiques qu'elle envisageait de réaliser sur sa comptabilité avaient pour objet de reconstituer ses recettes et, d'autre part, que la société n'avait pas disposé du temps nécessaire pour formaliser son choix entre les trois options définies par ces mêmes dispositions. La cour a rejeté les conclusions de la société requérante en omettant de répondre à ce moyen, qui n'était pas inopérant.<br/>
<br/>
              4. Par suite, la SNC Veuve A... B... est fondée à soutenir que la cour a insuffisamment motivé son arrêt et à en demander pour ce motif l'annulation, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 750 euros à verser à la SNC Veuve A... B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 5 décembre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera une somme de 750 euros à la SNC Veuve A... B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société en nom collectif Veuve A... B... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
