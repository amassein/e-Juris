<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027531316</ID>
<ANCIEN_ID>JG_L_2013_06_000000363657</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/53/13/CETATEXT000027531316.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/06/2013, 363657, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363657</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:363657.20130610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 31 octobre et 26 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...; M. A...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 29 juin 2012 accordant son extradition aux autorités polonaises ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 mai 2013, présentée pour M. A... ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ; <br/>
<br/>
              Vu la convention établie sur la base de l'article K3 du traité sur l'Union européenne, relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996 ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par le décret attaqué, le Premier ministre a accordé aux autorités polonaises l'extradition de M. A...sur le fondement d'un mandat d'arrêt décerné le 21 août 2007 par le substitut du procureur régional de Raciborz, pour l'exécution d'une ordonnance de placement en détention provisoire du 12 juin 2007 du tribunal d'instance de Raciborz, confirmée par décision du tribunal de grande instance de Gliwice du 17 juillet 2007, aux fins de poursuites de faits de meurtre, vols avec effraction et recel de biens ;<br/>
<br/>
              2.	Considérant, en premier lieu, qu'il ressort des mentions de l'ampliation du décret attaqué figurant au dossier et certifiée conforme par le secrétaire général du Gouvernement que, contrairement à ce que soutient M.A..., le décret a été signé par le Premier ministre et contresigné par la garde des sceaux, ministre de la justice ; <br/>
<br/>
              3.	Considérant, en deuxième lieu, qu'il n'appartient pas au Conseil d'Etat, saisi d'un recours contre un décret d'extradition, d'examiner les moyens de forme ou de procédure mettant en cause la régularité de l'avis émis par la chambre de l'instruction ; <br/>
<br/>
              4.	Considérant, en troisième lieu, que les conventions d'extradition sont des lois de procédure qui, sauf stipulation contraire, sont applicables immédiatement aux faits survenus avant leur entrée en vigueur ; qu'il en est ainsi, notamment, des conditions qu'elles fixent quant à la prescription de l'action publique ou de la peine ; que la convention signée à Dublin le 27 septembre 1996, relative à l'extradition entre les Etats membres de l'Union européenne, dont les stipulations sont applicables, en vertu du paragraphe 5 de son article 18, aux demandes d'extradition présentées après son entrée en vigueur, s'est substituée à la convention européenne d'extradition du 13 décembre 1957 à compter du 18 juillet 2006 pour ce qui concerne les rapports entre la France et la Pologne ; qu'ainsi, sont applicables à la demande d'extradition de M.A..., présentée par les autorités polonaises le 21 mai 2010, les stipulations du paragraphe 1 de l'article 8 de la convention de Dublin aux termes desquelles : " L'extradition ne peut être refusée au motif qu'il y a prescription de l'action ou de la peine selon la législation de l'Etat membre requis " ; que ces stipulations se sont substituées à celles de l'article 10 de la convention du 13 décembre 1957 selon lesquelles la prescription ne devait pas être acquise selon la législation de l'Etat requérant comme celle de l'Etat requis, sans que puisse être utilement invoqué le principe de la non rétroactivité de la loi pénale, dès lors que les effets défavorables d'une modification d'un régime de prescription ne peuvent s'analyser, s'agissant de la mise en oeuvre d'une procédure d'extradition, comme une peine au sens de l'article 7 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, par suite, le moyen tiré de ce que la prescription des infractions de vols et de recel reprochées à M. A...était acquise au regard du droit français avant l'entrée en vigueur de la convention de Dublin ne peut qu'être écarté ; <br/>
<br/>
              5.	Considérant, en quatrième lieu, que si les dispositions du paragraphe 5 a de l'article 156 du code de procédure pénale polonais, qui aménage, pendant l'enquête, un accès aux pièces du dossier au cours de la procédure préliminaire susceptible de conduire à un placement en détention provisoire, prévoit que l'accès peut être refusé par décision du procureur, les restrictions qui sont ainsi susceptibles d'être mises à cet accès répondent à des motifs fondés sur la protection des personnes et les exigences de l'enquête, sont soumises à des conditions précises et sont susceptibles d'être contestées ; que, par suite, sans que les autorités françaises aient eu à solliciter des autorités polonaises des garanties particulières sur ce point préalablement à l'intervention du décret d'extradition, le moyen tiré de ce que l'extradition aurait été accordée en méconnaissance de l'ordre public français, de l'article 1er des réserves émises par la France à la convention européenne d'extradition et du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales  doit être écarté ; <br/>
<br/>
              6.	Considérant, en cinquième lieu, que si M. A...soutient, en se référant à des documents généraux, que les conditions de détention dans les prisons polonaises feraient courir des risques, notamment en raison de la surpopulation carcérale, de traitements inhumains ou dégradants à l'ensemble des détenus, les pièces du dossier ne permettent pas d'établir des risques personnels pour ce qui le concerne ; que, par suite, le moyen tiré de la méconnaissance des stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté ; <br/>
<br/>
              7.	Considérant, en sixième lieu, qu'il résulte des principes généraux du droit applicables à l'extradition qu'il n'appartient pas aux autorités françaises, sauf en cas d'erreur évidente, de statuer sur le bien-fondé des charges retenues contre la personne dont l'extradition est demandée ; que si M. A...fait valoir que deux des coauteurs des infractions à l'origine de son extradition, auditionnés en février 2012 par les autorités judiciaires polonaises, se sont rétractés quant à sa participation au meurtre qui lui est reproché, il ne ressort pas des pièces versées au dossier qu'une erreur évidente ait été commise en ce qui concerne les faits reprochés à M. A...à l'origine de la demande de son extradition ;<br/>
<br/>
              8.	Considérant, enfin, que la circonstance que M. A...aurait, postérieurement à l'intervention du décret d'extradition, bénéficié en Pologne d'une décision de non-lieu pour les faits de recel visés dans le décret est sans effet sur l'appréciation de sa légalité ; <br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du décret du 20 septembre 2012 accordant son extradition aux autorités polonaises ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
