<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037525321</ID>
<ANCIEN_ID>JG_L_2018_10_000000402480</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/52/53/CETATEXT000037525321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 22/10/2018, 402480</TITRE>
<DATE_DEC>2018-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402480</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:402480.20181022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL Saint-Léon a demandé au tribunal administratif de Strasbourg d'annuler l'arrêté du préfet du Bas-Rhin du 26 mars 2013 portant prescriptions complémentaires relatives à la réalisation d'une passe à poissons au barrage situé en lit mineur de la Bruche, dans la commune de Heiligenberg, lieu-dit Weschmatt, associé à l'usine hydroélectrique lui appartenant. Par un jugement n° 1302362 du 21 janvier 2015, le tribunal administratif de Strasbourg a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 15NC00542 du 9 juin 2016, la cour administrative d'appel de Nancy a rejeté son appel formé contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 août et 16 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la SARL Saint-Léon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2006-1772 du 30 décembre 2006 ;<br/>
              - le décret n° 89-804 du 27 octobre 1989 ;<br/>
              - le décret n° 99-1101 du 15 décembre 1999 ;<br/>
              - l'arrêté du 15 décembre 1999 fixant par bassin ou sous-bassin, dans certains cours d'eau classés au titre de l'article L. 232-6 du code rural, la liste des espèces migratrices de poissons ;<br/>
              - l'arrêté du 28 décembre 2012 établissant la liste des cours d'eau mentionnée au 2° du I de l'article L. 214-17 du code de l'environnement sur le bassin Rhin-Meuse ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la SARL Saint-Léon.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL Saint-Léon a acquis en 2005 une centrale hydroélectrique d'une puissance de moins de 150 kW, située sur la commune de Heiligenberg (Bas-Rhin), sur le cours d'eau de la Bruche, installation initialement autorisée par un arrêté du préfet du Bas-Rhin du 26 mars 1860. Par un arrêté du 17 avril 2012, qui fait l'objet d'un pourvoi distinct, le préfet du Bas-Rhin a imposé à la SARL Saint-Léon plusieurs prescriptions relatives au débit réservé, à la continuité écologique et à la mise à jour du règlement d'eau de la centrale, notamment la réalisation d'une passe à poissons. Le préfet a imposé des prescriptions complémentaires, relatives aux caractéristiques, au délai de réalisation, aux conditions d'exécution des travaux et au récolement de cette passe à poissons, exigeant notamment que cet ouvrage soit opérationnel avant le 1er octobre 2013, par un arrêté du 26 mars 2013, dont la SARL Saint-Léon a demandé l'annulation au tribunal administratif de Strasbourg, qui a rejeté sa demande par un jugement du 21 janvier 2015. La société se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Nancy du 9 juin 2016 qui a rejeté son appel formé contre ce jugement.<br/>
<br/>
              Sur la régularité de la procédure d'adoption de l'arrêté litigieux :<br/>
<br/>
              2. Aux termes de l'article R. 214-17 du code de l'environnement, applicable à la date de l'arrêté litigieux : " A la demande du bénéficiaire de l'autorisation ou à sa propre initiative, le préfet peut prendre des arrêtés complémentaires après avis du conseil départemental de l'environnement et des risques sanitaires et technologiques. Ces arrêtés peuvent fixer toutes les prescriptions additionnelles que la protection des éléments mentionnés à l'article L. 211-1 rend nécessaires, ou atténuer celles des prescriptions primitives dont le maintien n'est plus justifié. (applicables jusqu'à ce que ces obligations y soient substituées, dans le délai prévu à l'alinéa précédent). / Le bénéficiaire de l'autorisation peut se faire entendre et présenter ses observations dans les conditions prévues au second alinéa de l'article R. 214-11 et au premier alinéa de l'article R. 214-12. / Le silence gardé sur la demande du bénéficiaire de l'autorisation plus de trois mois à compter de la réception de cette demande vaut décision de rejet ". Aux termes de l'article R. 214-11 du même code, applicable en l'espèce : " (...) / Le pétitionnaire a la faculté de se faire entendre par ce conseil ou de désigner à cet effet un mandataire. Il est informé, par le préfet, au moins huit jours à l'avance, de la date et du lieu de la réunion du conseil et reçoit simultanément un exemplaire des propositions mentionnées à l'alinéa précédent ". Par ailleurs, aux termes de l'article R. 214-12 du même code, dans sa rédaction applicable en l'espèce : " Le projet d'arrêté statuant sur la demande est porté, par le préfet, à la connaissance du pétitionnaire, auquel un délai de quinze jours est accordé pour présenter éventuellement ses observations, par écrit, au préfet, directement ou par mandataire. / (...) ". Il résulte de ces dispositions que seules les prescriptions additionnelles envisagées par le préfet doivent être communiquées à l'exploitant préalablement à leur examen par le conseil départemental de l'environnement et des risques sanitaires et technologiques, le projet d'arrêté portant prescriptions complémentaires en tant que tel n'ayant à être porté à sa connaissance qu'ultérieurement, dans les conditions prévues à l'article R. 214-12 du code de l'environnement, alors applicable.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond qu'en application des dispositions des articles R. 214-17 et R. 214-11 du code de l'environnement précitées, la SARL Saint-Léon a été rendue destinataire des propositions de prescriptions complémentaires du préfet du Bas-Rhin concernant la centrale de Heiligenberg le 28 novembre 2012, sous la forme d'un projet d'arrêté, en vue de la réunion du conseil départemental de l'environnement et des risques sanitaires et technologiques prévue le 9 janvier 2013. Si, compte tenu de la publication au Journal officiel le 1er janvier 2013 de l'arrêté du 28 décembre 2012 établissant la liste des cours d'eau mentionnée au 2° du I de l'article L. 214-17 du code de l'environnement sur le bassin Rhin-Meuse, qui a classé certaines partie de la Bruche et ses affluents en application des nouvelles dispositions de l'article L. 214-17 du code de l'environnement introduites par la loi du 30 décembre 2006 sur l'eau et les milieux aquatiques, le projet finalement soumis au conseil départemental comportait des visas et deux motifs supplémentaires par rapport au projet communiqué à la SARL Saint-Léon, les prescriptions additionnelles envisagées par le préfet étaient, en revanche, identiques dans les deux projets. Par ailleurs, à la suite de la réunion du conseil départemental, un projet d'arrêté a été notifié le 11 janvier 2013 à la SARL Saint-Léon conformément aux dispositions de l'article R. 214-12 du code de l'environnement, alors applicable, cette dernière ayant d'ailleurs formulé des observations par deux courriers des 25 janvier et 4 février 2013.<br/>
<br/>
              4. Par suite, en retenant, pour juger que les dispositions de l'article R. 214-11 du code de l'environnement n'avaient pas été méconnues, que seules les prescriptions envisagées par le préfet avaient à être communiquées à la SARL Saint-Léon préalablement à la réunion du conseil départemental de l'environnement et des risques sanitaires et technologiques et que, en l'espèce, les prescriptions communiquées et celles examinées par le conseil départemental étaient identiques, indépendamment de la modification de certains visas et de motifs du projet d'arrêté, la cour, qui n'a pas insuffisamment motivé son arrêt, n'a pas commis d'erreur de droit. <br/>
<br/>
              Sur l'applicabilité des dispositions de l'article L. 214-4 du code de l'environnement :<br/>
<br/>
              5. En vertu du II de l'article L. 211-1 du code de l'environnement, dans sa rédaction applicable en l'espèce, la gestion équilibrée de la ressource en eau " doit permettre en priorité de satisfaire les exigences de la santé, de la salubrité publique, de la sécurité civile et de l'alimentation en eau potable de la population. Elle doit également permettre de satisfaire ou concilier, lors des différents usages, activités ou travaux les exigences : / 1° De la vie biologique du milieu récepteur, et spécialement de la faune piscicole et conchylicole (...) ". Aux termes de l'article L. 214-3 du même code, également dans sa rédaction applicable : " I. Sont soumis à autorisation de l'autorité administrative les installations, ouvrages, travaux et activités susceptibles de présenter des dangers pour la santé et la sécurité publique, de nuire au libre écoulement des eaux, de réduire la ressource en eau, d'accroître notablement le risque d'inondation, de porter gravement atteinte à la qualité ou à la diversité du milieu aquatique, notamment aux peuplements piscicoles. / Les prescriptions nécessaires à la protection des intérêts mentionnés à l'article L. 211-1, les moyens de surveillance, les modalités des contrôles techniques et les moyens d'intervention en cas d'incident ou d'accident sont fixés par l'arrêté d'autorisation et, éventuellement, par des actes complémentaires pris postérieurement. / (...) ". En vertu du II de l'article L. 214 4 du code de l'environnement dans sa rédaction applicable, une autorisation délivrée à une installation entraînant notamment des prélèvements sur des eaux superficielles ou une modification du niveau ou du mode d'écoulement des eaux " peut être abrogée ou modifiée, sans indemnité de la part de l'Etat exerçant ses pouvoirs de police, dans les cas suivants : / 1° Dans l'intérêt de la salubrité publique, et notamment lorsque cette abrogation ou cette modification est nécessaire à l'alimentation en eau potable des populations ; / 2° Pour prévenir ou faire cesser les inondations ou en cas de menace pour la sécurité publique ; / 3° En cas de menace majeure pour le milieu aquatique, et notamment lorsque les milieux aquatiques sont soumis à des conditions hydrauliques critiques non compatibles avec leur préservation ; / 4° Lorsque les ouvrages ou installations sont abandonnés ou ne font plus l'objet d'un entretien régulier ". Enfin, aux termes du II de l'article L. 214-6 du même code : " Les installations, ouvrages et activités déclarés ou autorisés en application d'une législation ou réglementation relative à l'eau antérieure au 4 janvier 1992 sont réputés déclarés ou autorisés en application des dispositions de la présente section. (...) ". <br/>
<br/>
              6. Par ailleurs, aux termes de l'article L. 214-17 du code de l'environnement, dans sa version alors en vigueur, issue de la loi du 30 décembre 2006 sur l'eau et les milieux aquatiques : " I. Après avis des conseils généraux intéressés, des établissements publics territoriaux de bassin concernés, des comités de bassins et, en Corse, de l'Assemblée de Corse, l'autorité administrative établit, pour chaque bassin ou sous-bassin : (...) / 2° Une liste de cours d'eau, parties de cours d'eau ou canaux dans lesquels il est nécessaire d'assurer le transport suffisant des sédiments et la circulation des poissons migrateurs. Tout ouvrage doit y être géré, entretenu et équipé selon des règles définies par l'autorité administrative, en concertation avec le propriétaire ou, à défaut, l'exploitant. / III. - Les obligations résultant du I s'appliquent à la date de publication des listes. Celles découlant du 2° du I s'appliquent, à l'issue d'un délai de cinq ans après la publication des listes, aux ouvrages existants régulièrement installés. / Le cinquième alinéa de l'article 2 de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique et l'article L. 432-6 du présent code demeurent.applicables jusqu'à ce que ces obligations y soient substituées, dans le délai prévu à l'alinéa précédent A l'expiration du délai précité, et au plus tard le 1er janvier 2014, le cinquième alinéa de l'article 2 de la loi du 16 octobre 1919 précitée est supprimé et l'article L. 432-6 précité est abrogé. / Les obligations résultant du I du présent article n'ouvrent droit à indemnité que si elles font peser sur le propriétaire ou l'exploitant de l'ouvrage une charge spéciale et exorbitante ".<br/>
<br/>
              7. Il résulte de la combinaison de ces dispositions ainsi que de celles de l'article R. 214-17 citées au point 2, que l'autorité administrative compétente peut, en application de l'article L. 214-17 du code de l'environnement et indépendamment des dispositions générales du II de l'article L. 214-4 du même code, imposer au titulaire d'une autorisation délivrée au double titre de la législation sur les ouvrages hydrauliques et de la législation sur l'eau les travaux nécessaires pour assurer la circulation des poissons migrateurs dans les cours d'eaux alors classés et définir les caractéristiques de ces travaux. Ces prescriptions, qui ne répondent pas aux mêmes objectifs que ceux mentionnés au II de l'article L. 214-4 du code de l'environnement, n'ouvrent, pour leur part, droit à indemnité que si elles font peser sur le propriétaire ou l'exploitant de l'ouvrage une charge spéciale et exorbitante. Par suite, en jugeant qu'il était constant que l'arrêté préfectoral contesté n'avait pas été pris sur le fondement des dispositions de l'article L. 214-4 du code de l'environnement et donc que les prescriptions imposées à la SARL Saint-Léon pouvaient régulièrement trouver leur fondement dans les dispositions de l'article L. 214-17 du code de l'environnement, la cour, qui n'a pas dénaturé les pièces du dossier, n'a pas commis d'erreur de droit.<br/>
<br/>
              Sur l'applicabilité des dispositions de l'article L. 214-17 du code de l'environnement, dans sa rédaction issue de la loi du 30 décembre 2006 sur l'eau et les milieux aquatiques : <br/>
<br/>
              8. L'article L. 232-6 du code rural, introduit par le décret du 27 octobre 1989 portant révision du code rural en ce qui concerne les dispositions législatives relatives à la protection de la nature, disposait que : " Dans les cours d'eau ou parties de cours d'eau et canaux dont la liste est fixée par décret, après avis des conseils généraux rendus dans un délai de six mois, tout ouvrage doit comporter des dispositifs assurant la circulation des poissons migrateurs. L'exploitant de l'ouvrage est tenu d'assurer le fonctionnement et l'entretien de ces dispositifs. Les ouvrages existants doivent être mis en conformité, sans indemnité, avec les dispositions du présent article dans un délai de cinq ans à compter de la publication d'une liste d'espèces migratrices par bassin ou sous-bassin fixée par le ministre chargé de la pêche en eau douce et, le cas échéant, par le ministre chargé de la mer ". A compter de l'entrée en vigueur de l'ordonnance du 18 septembre 2000 relative à la partie législative du code de l'environnement, ces dispositions ont été reprises à l'identique à l'article L. 432-6 du code de l'environnement. <br/>
<br/>
              9. Il résulte de la combinaison de ces dispositions avec celles du III de l'article L. 214-17 du code de l'environnement cité au point 6, telles qu'éclairées par les travaux parlementaires, que si un délai de cinq ans après la publication des listes prévues au 2° du I du même article L. 214-17 est accordé aux exploitants d' " ouvrages existants régulièrement installés " pour mettre en oeuvre les obligations qu'il instaure, ce délai n'est pas ouvert aux exploitants d'ouvrages antérieurement soumis à une obligation de mise en conformité en application de l'article L. 232-6 du code rural, devenu l'article L. 432-6 du code de l'environnement, qui n'auraient pas respecté le délai de cinq ans octroyé par ces dispositions pour mettre en oeuvre cette obligation. Ces ouvrages existants ne peuvent ainsi être regardés comme " régulièrement installés ", au sens du III de l'article L. 214-17 du code de l'environnement, et sont donc soumis aux obligations résultant du I de cet article dès la publication des listes qu'il prévoit.<br/>
<br/>
              10. D'une part, sur le fondement de l'article L. 232-6 du code rural, devenu l'article L. 432-6 du code de l'environnement cité au point 8, le décret du 15 décembre 1999 de classement des cours d'eau, parties de cours d'eau et canaux a classé la Bruche et ses affluents à ce titre et un arrêté du 15 décembre 1999, publié au Journal officiel du 24 décembre 1999, a fixé, pour les cours d'eau ainsi classés, la liste des espèces migratrices de poissons pour lesquels des dispositifs doivent assurer la circulation. Il résulte de la combinaison de ces textes que, sauf dispositions particulières, à compter du 25 décembre 2004, tous les ouvrages implantés sur la Bruche devaient avoir mis en place un dispositif permettant la circulation des poissons migrateurs. <br/>
<br/>
              11. D'autre part, sur le fondement des nouvelles dispositions de l'article L. 214-17 du code de l'environnement cité au point 6, un arrêté du 28 décembre 2012 établissant la liste des cours d'eau mentionnée au 2° du I du même article L. 214-17 sur le bassin Rhin-Meuse, publié au Journal officiel du 1er janvier 2013, a classé certaines partie de la Bruche et ses affluents. Or, il ressort des pièces soumis aux juges du fond et il n'est d'ailleurs pas contesté qu'à la date de publication de cet arrêté, la SARL Saint-Léon n'avait pas mis en place de dispositif permettant la circulation des poissons migrateurs dans sa centrale de Heiligenberg en méconnaissance des exigences de la législation antérieure. <br/>
<br/>
              12. Il résulte de ce qui vient d'être dit que cette centrale hydroélectrique ne constituait pas, à la date du 1er janvier 2013, un ouvrage existant régulièrement installé, au sens du III de l'article L. 214-17 du code de l'environnement, de sorte que les obligations posées par le I de ce même article lui étaient immédiatement applicables à compter de la publication de la liste prévue à son 2°. Par suite, en retenant que la SARL Saint-Léon ne pouvait utilement invoquer le délai de cinq ans prévu par le III de l'article L. 214-17 pour contester la légalité du délai au terme duquel le préfet du Bas-Rhin lui a imposé, par l'arrêté contesté du 26 mars 2013, de mettre en place une passe à poissons opérationnelle avant le 1er octobre 2013, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit.<br/>
<br/>
              Sur les prescriptions de l'arrêté contesté en tant qu'elles fixent les dimensions de la passe à poissons :<br/>
<br/>
              13. Ainsi qu'il a été dit au point 7, l'autorité administrative compétente peut, notamment en application des dispositions des articles L. 214-17, L. 214-18 et R. 214-17 du code de l'environnement, imposer au titulaire d'une autorisation délivrée au double titre de la législation sur les ouvrages hydrauliques et de la législation sur l'eau les travaux nécessaires pour assurer la circulation des poissons migrateurs dans les cours d'eaux alors classés et définir les caractéristiques de ces travaux. <br/>
<br/>
              14. Il ressort des pièces du dossier soumis aux juges du fond que, pour imposer à la SARL Saint-Léon, par l'arrêté litigieux, l'installation d'une passe-à-poissons comportant six bassins d'une longueur de 3 mètres chacun, le préfet du Bas-Rhin s'est notamment fondé sur la présence significative de saumons de plus de 80 cm sur le Rhin, en aval d'Iffezheim, dont la Bruche constitue un affluent, ainsi que sur les préconisations de l'office national de l'eau et des milieux aquatiques concernant les dimensions minimales des bassins des passes-à-poissons en fonction de la taille des poissions et du débit de la passe. <br/>
<br/>
              15. Par suite, en retenant, pour juger que ces prescriptions n'étaient ni excessives ni injustifiées, que plusieurs recensements effectués tant sur la Bruche que sur le Rhin avaient mis en évidence la présence de spécimens de saumons approchant voire dépassant la taille d'un mètre, que ces recensements permettaient également de constater une augmentation de la taille des poissons et que l'office national de l'eau et des milieux aquatiques préconisait que la taille des bassins d'une passe soit au moins égale à trois fois la longueur des poissons, la cour, qui suffisamment motivé son arrêt, n'a pas dénaturé les pièces qui lui étaient soumises. <br/>
<br/>
              16. Il résulte de tout ce qui précède que la SARL Saint-Léon n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nancy qu'elle attaque. Par suite, son pourvoi, y compris ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative, ne peut qu'être rejeté.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SARL Saint-Léon est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SARL Saint-Léon et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-02 EAUX. OUVRAGES. - DISPOSITIFS ASSURANT LA CIRCULATION DES POISSONS MIGRATEURS - MISE EN CONFORMITÉ DES OUVRAGES EXISTANTS RÉGULIÈREMENT INSTALLÉS DANS UN DÉLAI DE 5 ANS À COMPTER DE LA PUBLICATION D'UNE LISTE D'ESPÈCES (III DE L'ART. L. 214-17 DU CODE DE L'ENVIRONNEMENT) - NOTION D'OUVRAGES EXISTANTS RÉGULIÈREMENT INSTALLÉS - OUVRAGES ANTÉRIEUREMENT SOUMIS À UNE OBLIGATION DE MISE EN CONFORMITÉ EN APPLICATION DE L'ARTICLE L. 236-2 DU CODE RURAL, DEVENU ARTICLE L. 432- 6 DU CODE DE L'ENVIRONNEMENT, QUI N'AURAIENT PAS RESPECTÉ LE DÉLAI DE 5 ANS OCTROYÉ PAR CES DISPOSITIONS - EXCLUSION.
</SCT>
<ANA ID="9A"> 27-02 Il résulte de la combinaison de l'article L. 232-6 du code rural, introduit par le décret n° 89-804 du 27 octobre 1989,  avec le III de l'article L. 214-17 du code de l'environnement, tel qu'éclairé par les travaux parlementaires, que si un délai de cinq ans après la publication des listes prévues au 2° du I du même article L. 214-17 est accordé aux exploitants d' ouvrages existants régulièrement installés pour mettre en oeuvre les obligations qu'il instaure, ce délai n'est pas ouvert aux exploitants d'ouvrages antérieurement soumis à une obligation de mise en conformité en application de l'article L. 232-6 du code rural, devenu l'article L. 432-6 du code de l'environnement, qui n'auraient pas respecté le délai de cinq ans octroyé par ces dispositions pour mettre en oeuvre cette obligation. Ces ouvrages existants ne peuvent ainsi être regardés comme régulièrement installés, au sens du III de l'article L. 214-17 du code de l'environnement, dans sa version issue de la loi n° 2006-1772 du 30 décembre 2006, et sont donc soumis aux obligations résultant du I de cet article dès la publication des listes qu'il prévoit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
