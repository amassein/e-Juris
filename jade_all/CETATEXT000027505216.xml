<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027505216</ID>
<ANCIEN_ID>JG_L_2013_06_000000356868</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/50/52/CETATEXT000027505216.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 03/06/2013, 356868, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356868</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356868.20130603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 février et 18 mai 2012 au secrétariat du contentieux du Conseil d'État, présentés pour Mme B...D..., agissant à titre personnel et en sa qualité d'héritière de M. D... décédé, demeurant ... et Mme C...A..., agissant en sa qualité d'héritière, demeurant ... ; elles demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA00510 du 15 décembre 2011 par lequel la cour administrative d'appel de Paris a rejeté leur requête tendant à l'annulation du jugement n° 0502606 du 20 novembre 2009 du tribunal administratif de Paris rejetant leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de cotisations sociales auxquelles M. et Mme D...ont été assujettis au titre de l'année 2002 ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de la justice administrative et les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme D...et de Mme A...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que  M. D...a exercé une activité individuelle jusqu'en 1999 avant d'apporter son fonds de commerce de café-brasserie à la société en nom collectif (SNC) " Le Madrigal ", créée le 14 octobre 1999 ; que M. et MmeD..., qui détenaient chacun 3 000 parts de cette société, ont poursuivi par l'intermédiaire de cette société de personnes l'exploitation de ce fonds et ont déclaré leur quote-part de résultat à l'impôt sur le revenu dans la catégorie des bénéfices industriels et commerciaux ; qu'ils ont cédé l'intégralité de leurs parts le 1er juillet 2002 ; qu'ils ont estimé que la plus-value réalisée lors de cette cession pouvait s'imputer sur le déficit global résultant des déficits antérieurs reportables ; qu'à la suite d'un contrôle sur pièces de leur dossier fiscal portant sur l'année 2002, l'administration a refusé cette imputation ; que Mme D..., agissant à titre personnel et en sa qualité d'héritière à la suite du décès de M. D... et MmeA..., agissant en sa qualité d'héritière, se pourvoient en cassation contre l'arrêt du 15 décembre 2011 par lequel la cour administrative d'appel de Paris a rejeté leur requête tendant à l'annulation du jugement du 20 novembre 2009 du tribunal administratif de Paris rejetant leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mises en recouvrement au titre de l'année 2002 à l'issue de ce contrôle ;<br/>
<br/>
              2. Considérant, en premier lieu, que la cour a relevé que devant le tribunal l'administration avait demandé que la plus-value réalisée par les contribuables, initialement imposée sur le fondement des articles 150-0 A à 150-0 E du code général des impôts, soit regardée comme une plus-value professionnelle taxable sur le fondement de l'article 39 duodecies du même code ; qu'en jugeant, d'une part, que le tribunal avait pu régulièrement faire droit à cette demande dès lors que l'administration avait suivi à leur égard la procédure contradictoire et qu'ils avaient pu faire valoir leurs arguments sur le nouveau fondement proposé dans le cadre de la procédure juridictionnelle et, d'autre part, que l'administration n'était pas tenue d'adresser une nouvelle proposition de rectification, la cour, qui, contrairement à ce que soutiennent les requérantes, n'a nullement estimé que cette substitution de base légale avait pour effet de couvrir l'insuffisance de motivation de la proposition de rectification adressée aux contribuables, n'a pas commis l'erreur de droit alléguée, tirée de ce qu'en accueillant cette nouvelle base légale, elle aurait passé outre à l'exigence, résultant des dispositions de l'article L. 57 du livre des procédures fiscales, de motivation d'un tel acte et, par suite, violé cet article ;  <br/>
<br/>
              3. Considérant, en second lieu et d'une part, qu'aux termes de l'article 8 du code général des impôts : " Sous réserve des dispositions de l'article 6, les associés des sociétés en nom collectif (...) sont, lorsque ces sociétés n'ont pas opté pour le régime fiscal des sociétés de capitaux, personnellement soumis à l'impôt sur le revenu pour la part de bénéfices sociaux correspondant à leurs droits dans la société " ; qu'aux termes de l'article 151 nonies du même code : " I. Lorsqu'un contribuable exerce son activité professionnelle dans le cadre d'une société dont les bénéfices sont, en application des articles 8 et 8 ter, soumis en son nom à l'impôt sur le revenu dans la catégorie des bénéfices agricoles réels, des bénéfices industriels ou commerciaux ou des bénéfices non commerciaux, ses droits ou parts dans la société sont considérés notamment pour l'application des articles 38, 72 et 93, comme des éléments d'actif affectés à l'exercice de la profession. " ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article 38 du même code : " 1. (...) le bénéfice imposable est le bénéfice net déterminé d'après les résultats d'ensemble des opérations de toute nature effectuées par les entreprises, y compris notamment les cessions d'éléments quelconques de l'actif, soit en cours, soit en fin d'exploitation " ; qu'aux termes de l'article 39 duodecies de ce code : " 1. Par dérogation aux dispositions de l'article 38, les plus-values provenant de la cession d'éléments de l'actif immobilisé sont soumises à des régimes distincts suivant qu'elles sont réalisées à court ou à long terme. " ; que le I de l'article 39 quindecies dispose que : " 1 Sous réserve des dispositions des articles 41, 151 octies et 210 A à 210C, le montant net des plus-values à long terme fait l'objet d'une imposition séparée au taux de 16 %. / Il s'entend de l'excédent de ces plus-values sur les moins-values de même nature constatées au cours du même exercice. / Toutefois, ce montant n'est pas imposable lorsqu'il est utilisé à compenser le déficit d'exploitation de l'exercice. Le déficit ainsi annulé ne peut plus être reporté sur les bénéfices des exercices ultérieurs. (...) " ; <br/>
<br/>
              5. Considérant, enfin, que le fait pour un contribuable de cesser d'exercer à titre individuel son activité industrielle ou commerciale pour poursuivre cette activité au sein d'une société en nom collectif dont il devient associé et à laquelle il fait apport de son fonds de commerce constitue une cessation d'exercice de son entreprise individuelle au sens de l'article 201 du même code ;  <br/>
<br/>
              6. Considérant que, par les dispositions du 1 du I de l'article 39 quindecies du code général des impôts, le législateur a offert aux contribuables le choix d'éviter la taxation du montant net des plus-values à long terme en le compensant avec un déficit ordinaire constaté au titre de l'exercice concernant l'exploitation à raison de laquelle les plus-values ont été réalisées ou avec les déficits relatifs à des exercices antérieurs concernant cette exploitation et reportables sur cet exercice ; que ces dispositions, combinées avec celles de l'article 201 relatives aux cas de cession ou cessation d'une entreprise, font obstacle à ce que des déficits constatés dans le cadre de l'exploitation individuelle et relatifs à des exercices antérieurs à l'apport de cette activité à une société en nom collectif puissent être compensés avec la plus-value réalisée par le contribuable lors de la cession ultérieure des titres de cette société, reçus en contrepartie de cet apport ; qu'il en va ainsi, alors même que cet ancien entrepreneur individuel a poursuivi la même activité  au sein de cette société ;  <br/>
<br/>
              7. Considérant que les requérantes ont fait valoir devant les juges du fond que le montant de la plus-value professionnelle à long terme réalisée lors de la cession en 2002 des parts de la SNC " Le Madrigal " devait être compensé, pour l'établissement de l'impôt sur le revenu au titre de l'année 2002, avec le déficit d'exploitation constaté en 1999 par M. D... lors de la cessation de son activité exercée à titre individuel et qui n'avait pu encore être imputé ; qu'il résulte de ce qui vient d'être dit qu'en jugeant, après avoir relevé que ce déficit avait été réalisé par M. D...dans le cadre de son entreprise individuelle, que l'administration avait à bon droit refusé la compensation demandée entre la plus-value constatée en 2002 et ces déficits antérieurs, la cour, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que Mme D...et Mme A... ne sont pas fondées à demander l'annulation de l'arrêt attaqué ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme D...et de Mme A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B...D..., à Mme C... A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
