<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028991690</ID>
<ANCIEN_ID>JG_L_2014_05_000000350472</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/99/16/CETATEXT000028991690.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 26/05/2014, 350472</TITRE>
<DATE_DEC>2014-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350472</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:350472.20140526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 29 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour la commune de Morsang-sur-Seine, représentée par son maire ;  la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°10VE00368 du 14 avril 2011 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre le jugement n° 0709472-0806753 du 3 décembre 2009 du tribunal administratif de Versailles faisant droit aux demandes de Mlle D... et de M. A...tendant à l'annulation des titres de recettes émis le 29 mai 2007 et le 7 mai 2008, relatifs au paiement de la participation pour voirie et réseaux ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de Mlle D...et de M. A...la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la commune de Morsang-sur-Seine et à la SCP Gaschignard, avocat de Mlle D...et de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 31 mars 2006, modifiée le 12 janvier 2007, la commune de Morsang-sur-Seine, estimant que l'implantation de futures constructions dans le secteur du lieudit " Les Basses Montelièvres " impliquait l'aménagement du chemin éponyme et son prolongement en direction de la route de Saintry, a décidé de mettre ces travaux à la charge de l'ensemble des futurs propriétaires des terrains situés à " 80 mètres d'une part de la voie " et institué une participation pour voirie et réseaux d'un montant de 35 euros par mètre carré de terrain desservi ; que, par l'arrêt attaqué, la cour administrative d'appel de Versailles a rejeté l'appel de la commune contre le jugement du 3 décembre 2009 du tribunal administratif de Versailles annulant, à la demande de Mlle D...et de M.A..., les titres de recettes émis le 29 mai 2007 et le 7 mai 2008 par le maire de Morsang-sur-Seine mettant à leur charge, au total, la somme de 54 474 euros au titre de la participation pour voirie et réseaux ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 332-11-1 du code de l'urbanisme, dans sa rédaction applicable au litige : " Le conseil municipal peut instituer une participation pour voirie et réseaux en vue de financer en tout ou en partie la construction des voies nouvelles ou l'aménagement des voies existantes ainsi que l'établissement ou l'adaptation des réseaux qui leur sont associés, lorsque ces travaux sont réalisés pour permettre l'implantation de nouvelles constructions./ (...) Lorsqu'une voie préexiste, si aucun aménagement supplémentaire de la voie n'est prévu par le conseil municipal, ces travaux peuvent ne concerner que les réseaux / (...) Le conseil municipal arrête la part du coût mise à la charge des propriétaires riverains. Cette part est répartie entre les propriétaires au prorata de la superficie des terrains bénéficiant de cette desserte et situés à moins de quatre-vingts mètres de la voie. (...) Le conseil municipal peut également exclure les terrains qui ne peuvent supporter de constructions du fait de contraintes physiques et les terrains non constructibles du fait de prescriptions ou de servitudes administratives dont l'édiction ne relève pas de la compétence de la commune (...) Lorsque, en application de l'alinéa précédent, le conseil municipal n'a prévu aucun aménagement supplémentaire de la voie et que les travaux portent exclusivement sur les réseaux d'eau et d'électricité, la commune peut également exclure les terrains déjà desservis par ces réseaux (...) " ; <br/>
<br/>
              3. Considérant, en premier lieu, que, pour écarter le moyen tiré de ce que les parcelles en cause ne pouvaient bénéficier, comme l'exige le quatrième alinéa de l'article L. 332-11-1 du code l'urbanisme, de la desserte du chemin des Basses Montelièvres en raison d'un fort dénivelé, la cour a jugé que la commune n'apportait pas d'éléments probants quant à l'existence de dénivelés présentant une pente allant jusqu'à 45%, ni quant à l'impossibilité pour les terrains en cause de supporter de nouvelles constructions ; que la cour, qui n'était pas tenue de répondre à l'argument tiré de ce que le chemin ne pouvait tenir lieu de desserte praticable, a ainsi suffisamment motivé son arrêt ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la circonstance que la cour ait, à tort au regard des écritures qui lui étaient soumises, relevé que la commune estimait non constructibles les terrains en cause est demeurée sans incidence sur l'appréciation qu'elle a portée sur cette constructibilité et, par suite, sur le bien-fondé de son arrêt ; que, dès lors, le moyen tiré de ce qu'elle aurait dénaturé les écritures de la commune ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'en jugeant que les riverains bordant à l'Ouest le chemin en cause pouvaient bénéficier de la nouvelle desserte, alors même que leurs terrains, situés en contrebas, étaient déjà desservis par une autre voie, la cour a porté sur les faits une appréciation souveraine, exempte de dénaturation, sur le fondement de laquelle elle a pu, sans entacher son arrêt d'erreur de droit au regard des dispositions de l'article L. 332-11-1 du code de l'urbanisme alors applicables, regarder ces terrains comme bénéficiant de la voie et estimer qu'ils devaient entrer dans l'assiette du calcul de la participation pour voirie et réseaux ; que la circonstance que la cour ait, par une appréciation surabondante, estimé en outre que l'inclusion de ces terrains méconnaissait la délibération du conseil municipal du 31 mars 2006 est sans incidence sur le bien-fondé de l'arrêt attaqué ;<br/>
<br/>
              6. Considérant, en quatrième lieu, qu'en vertu de L. 111-1-1 du code de l'urbanisme, dans sa rédaction applicable au litige, les plans d'occupation des sols doivent être compatibles avec les orientations des schémas directeurs ; qu'en énonçant que la commune ne démontrait pas qu'elle était dans l'obligation de se conformer impérativement aux " recommandations " du schéma directeur de la région Ile-de-France, la cour n'a pas méconnu la portée des dispositions de ce dernier mais s'est bornée à relever que la commune n'établissait pas que l'exclusion de l'assiette de calcul de la participation pour voirie et réseaux d'un terrain boisé inconstructible résultait de l'obligation de mise en compatibilité du plan d'occupation des sols de la commune avec ce schéma ;<br/>
              7. Considérant, en cinquième lieu, que les parcelles bordant à l'Ouest le " chemin des Basses Montelièvres " étant à bon droit regardées par la cour, comme il a été dit, comme bénéficiant de la nouvelle desserte, c'est sans méconnaître ni le droit de propriété garanti par l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ni le principe d'égalité devant les charges publiques entre propriétaires des parcelles situées de part et d'autre de la nouvelle voie, également bénéficiaires de celle-ci, que la cour a jugé que l'assiette de la participation pour voirie et réseaux devait être répartie entre l'ensemble de ces parcelles ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la commune de Morsang-sur-Seine n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de Mlle D...et M. A... qui ne sont pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge, en application des mêmes dispositions, la somme de 3 000 euros à verser à Mlle D...et M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Morsang-sur-Seine est rejeté.<br/>
Article 2 : La commune de Morsang-sur-Seine versera la somme de 3 000 euros à Mlle D...et M.A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Morsang-sur-Seine, à Mlle B...D...et à M. C...A....<br/>
 Copie en sera adressée à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-024 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CONTRIBUTIONS DES CONSTRUCTEURS AUX DÉPENSES D'ÉQUIPEMENT PUBLIC. - PARTICIPATION POUR VOIRIE ET RÉSEAUX (ART. L. 332-11-1 DU CODE DE L'URBANISME) - TERRAINS BÉNÉFICIANT DE LA DESSERTE - NOTION.
</SCT>
<ANA ID="9A"> 68-024 Des terrains déjà desservis par une autre voie peuvent être regardés comme bénéficiant de la nouvelle desserte au sens des dispositions de l'article L. 332-11-1 du code de l'urbanisme et entrer, par suite, dans le calcul de l'assiette de la participation pour voirie et réseaux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
