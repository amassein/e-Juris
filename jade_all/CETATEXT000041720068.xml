<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041720068</ID>
<ANCIEN_ID>JG_L_2020_03_000000438761</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/72/00/CETATEXT000041720068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 05/03/2020, 438761, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438761</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:438761.20200305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              1°/ Sous le n° 438761, par une requête et un mémoire en réplique, enregistrés les 18 et 28 février 2020 au secrétariat du contentieux du Conseil d'Etat, les associations PRIARTEM et Agir pour l'environnement demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'arrêté du 30 décembre 2019 relatif aux modalités et aux conditions d'attribution d'autorisations d'utilisation de fréquences dans la bande 3,5 GHz en France métropolitaine pour établir et exploiter un système mobile terrestre ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - leur requête est recevable dès lors qu'elles disposent d'un intérêt à agir leur donnant qualité pour agir ;<br/>
              - la condition d'urgence est remplie dès lors que le déploiement de la 5G auquel l'arrêté contesté participe est imminent alors même qu'il n'a pas été procédé à une évaluation de ses effets potentiels, notamment en ce qui concerne la santé humaine ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté contesté a été adopté au terme d'une procédure irrégulière, en méconnaissance des exigences posées à l'article 7 de la Charte de l'environnement et à l'article L. 32-1 du code des postes et des communications électroniques, dès lors, d'une part, qu'il a été soumis à consultation publique durant un délai insuffisant, d'autre part, qu'il a été adopté trop hâtivement sans réelle prise en compte des contributions versées et, enfin, que les résultats de la consultation publique ne sont pas accessibles ;<br/>
              - il revêt la qualité d'un plan ou d'un programme et aurait dû faire l'objet d'une évaluation environnementale préalable, conformément aux dispositions de la directive 2001/42/CE du 27 juin 2001 et de l'article L. 122-4 du code de l'environnement, dès lors qu'il emporterait des incidences notables sur l'environnement que ce soit en matière d'atteintes sanitaires, d'épuisement des ressources naturelles, d'augmentation de l'emprunte carbone et de consommation énergétique ;<br/>
              - il contrevient aux exigences de sobriété, d'efficacité énergétique et d'utilisation rationnelle de l'énergie prévues par les articles L. 100-1 du code de l'énergie et<br/>
L. 220-1 du code de l'environnement, dès lors que le déploiement de la 5G pourrait conduire à une consommation énergétique démultipliée ;<br/>
              - il est pris en violation du principe de protection de la santé humaine et à l'objectif de sobriété électromagnétique notamment garantis, d'une part, par les articles 9 et 12 de la directive 2002/21/CE du 7 mars 2002 modifiée et, d'autre part, les articles L. 32 et L. 42 du code des postes et des communications électroniques ; <br/>
              - il est pris en violation du principe de précaution garanti, d'une part, par l'article 5 de la Charte de l'environnement et, d'autre part, par l'article 191 du Traité sur le fonctionnement de l'Union européenne en ce qu'il prévoit un déploiement hâtif de la 5G sans prise en considération des enjeux environnementaux et sanitaires qui y sont associés.<br/>
              Par un mémoire en défense, enregistré le 26 février 2020, le ministre de l'économie et des finances conclut au rejet de la requête. Il soutient que la requête est irrecevable, que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
              Par un mémoire en défense, enregistré le 26 février 2020, l'Autorité de régulation des communications électroniques et des postes conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
              La requête a été communiquée à la ministre de la transition écologique et solidaire qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              2°/ Sous le n° 438763, par une requête et un mémoire en réplique, enregistrés les 18 et 28 février 2020 au secrétariat du contentieux du Conseil d'Etat, les associations PRIARTEM et Agir pour l'environnement demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2019-1592 du 31 décembre 2019 modifiant le décret n° 2007-1532 du 24 octobre 2007 modifié relatif aux redevances d'utilisation des fréquences radioélectriques dues par les titulaires d'autorisations d'utilisation de fréquences délivrées par l'Autorité de régulation des communications électroniques et des postes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - leur requête est recevable dès lors qu'elles disposent d'un intérêt à agir leur donnant qualité pour agir ;<br/>
              - la condition d'urgence est remplie dès lors que le déploiement de la 5G auquel le décret contesté contribue est imminent alors même qu'il n'a pas été procédé à une évaluation de ses effets potentiels, notamment en ce qui concerne la santé humaine ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - le décret contesté a été adopté au terme d'une procédure irrégulière, en méconnaissance des exigences posées à l'article 7 de la Charte de l'environnement et à l'article L. 32-1 du code des postes et des communications électroniques, dès lors, d'une part, qu'il a été soumis à consultation publique durant un délai insuffisant, d'autre part, qu'il a été adopté trop hâtivement sans réelle prise en compte des contributions versées et, enfin, que les résultats de la consultation publique ne sont pas accessibles ;<br/>
              - il revêt la qualité d'un plan ou d'un programme et aurait dû faire l'objet d'une évaluation environnementale préalable, conformément aux dispositions de la directive 2001/42/CE du 27 juin 2001 et de l'article L. 122-4 du code de l'environnement, dès lors qu'il emporterait des incidences notables sur l'environnement que ce soit en matière d'atteintes sanitaires, d'épuisement des ressources naturelles, d'augmentation de l'emprunte carbone et de consommation énergétique ;<br/>
              - il contrevient aux exigences de sobriété, d'efficacité énergétique et d'utilisation rationnelle de l'énergie prévues par les articles L. 100-1 du code de l'énergie et<br/>
L. 220-1 du code de l'environnement, dès lors que le déploiement de la 5G pourrait conduire à une consommation énergétique démultipliée ;<br/>
              - il est pris en violation du principe de protection de la santé humaine et à l'objectif de sobriété électromagnétique notamment garantis, d'une part, par les articles 9 et 12 de la directive 2002/21/CE du 7 mars 2002 modifiée et, d'autre part, les articles L. 32 et L. 42 du code des postes et des communications électroniques ; <br/>
              - il est pris en violation du principe de précaution garanti, d'une part, par l'article 5 de la Charte de l'environnement et, d'autre part, par l'article 191 du traité sur le fonctionnement de l'Union européenne en ce qu'il prévoit un déploiement hâtif de la 5G sans prise en considération des enjeux environnementaux et sanitaires qui y sont associés.<br/>
              Par un mémoire en défense, enregistré le 26 février 2020, le ministre de l'économie et des finances conclut au rejet de la requête. Il soutient que la requête est irrecevable, que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité du décret contesté.<br/>
<br/>
              Par un mémoire en défense, enregistré le 26 février 2020, l'Autorité de régulation des communications électroniques et des postes conclut au rejet de la requête et s'en remet aux écritures présentées par le ministre de l'économie et des finances.<br/>
<br/>
              La requête a été communiquée au Premier ministre et à la ministre de la transition écologique et solidaire qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le Traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive n° 2001/42/CE du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement ;<br/>
              - le code de l'énergie ;<br/>
              - le code de l'environnement ;<br/>
              - le code des postes et des communications électroniques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, les associations PRIARTEM et Agir pour l'environnement et, d'autre part, le ministre de l'économie et des finances, l'Autorité de régulation des communications électroniques et des postes, le Premier ministre et la ministre de la transition écologique et solidaire ;<br/>
<br/>
              Ont été entendus au cours de l'audience publique du lundi 2 mars 2020, à 11 heures 30 :<br/>
              - les représentants des associations PRIARTEM et Agir pour l'environnement ;<br/>
<br/>
              - les représentants du ministre de l'économie et des finances ;<br/>
<br/>
- les représentantes de l'Autorité de régulation des communications électroniques et des postes ;<br/>
à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Les demandes de suspension présentent à juger des questions voisinent. Il y a lieu d'y statuer par une seule ordonnance.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
              3. Il ressort des pièces du dossier que les associations PRIARTEM et Agir pour l'environnement sont opposées au déploiement des éléments de réseau de communication de 5ème génération dit " 5G " en France en raison des conséquences sanitaires et environnementales qu'il implique. Le passage à la 5G en France suit un processus en plusieurs étapes, lequel a débuté par le lancement d'une " feuille de route " par le Gouvernement et l'Autorité de régulation des communications électroniques et des postes en juillet 2018. Dans ce contexte, l'Autorité de régulation des communications électroniques et des postes a proposé, le<br/>
21 novembre 2019, au ministre chargé des communications électroniques, les modalités et conditions d'attribution d'autorisations d'utilisation de fréquences dans la bande 3,4 - 3,8 GHz en France métropolitaine pour établir et exploiter un système mobile terrestre ouvert au public. Les modalités financières de ces attributions ont été fixées, d'une part, par l'arrêté du<br/>
30 décembre 2019 s'agissant de la fixation des prix de réserve et, d'autre part, par le décret du<br/>
31 décembre 2019 s'agissant de la fixation des redevances éligibles et des modalités de versements. Une consultation publique a été conduite du 28 novembre au 12 décembre 2019 visant à recueillir les contributions des acteurs intéressés par la procédure attribution de fréquences 5G. Les associations PRIARTEM et Agir pour l'environnement demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension desdits décret et arrêté.<br/>
              Sans qu'il soit besoin d'examiner les différentes fins de non-recevoir opposées en défense ;<br/>
              4. D'une part, le décret dont la suspension est demandée n'a pour objet que de fixer des montants de redevance et des prix de réserve. La mise en oeuvre de ces dispositions, qui ne peut intervenir qu'au terme de la procédure d'octroi des fréquences régies par l'arrêté également contesté, est par elle-même dépourvue de toute conséquence en matière environnementale ou de santé, seules invoquées par les associations requérantes pour établir l'urgence de la suspension demandée.<br/>
              5. D'autre part, à supposer que l'arrêté critiqué ait pour effet les conséquences alléguées en matière d'environnement, de santé et de consommation énergétique, ces conséquences ne pourront se manifester qu'après que l'attribution des fréquences qu'il régit aura commencé de recevoir exécution, c'est-à-dire, selon les informations recueillies lors de l'audience publique, au cours de l'été 2020, et dans la seule limite du déploiement initial des systèmes de 5ème génération, borné vraisemblablement à une aire urbaine. Cette situation est d'autant moins constitutive d'une urgence que la 2ème chambre de la section du contentieux du Conseil d'Etat est en mesure d'inscrire au rôle d'une formation de jugement les requêtes au fond introduites à l'encontre du décret et de l'arrêté, de manière à ce qu'elles fassent l'objet d'une décision avant l'été 2020.<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les moyens articulés au soutien de la demande de suspension, celle-ci ne peut, la condition d'urgence faisant défaut, qu'être rejetée, ainsi que les conclusions tendant à ce que l'Etat verse une somme d'argent aux associations requérantes sur le fondement des dispositions de l'article L 761-1 du code de justice administrative qui, l'Etat n'étant pas la partie perdante, y font obstacle.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les requêtes des associations PRIARTEM et Agir pour l'environnement sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée aux associations PRIARTEM et Agir pour l'environnement, au ministre de l'économie et des finances et à l'Autorité de régulation des communications électroniques et des postes.<br/>
Copie en sera adressée à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
