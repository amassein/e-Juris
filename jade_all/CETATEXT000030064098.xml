<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030064098</ID>
<ANCIEN_ID>JG_L_2015_01_000000384060</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/06/40/CETATEXT000030064098.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 07/01/2015, 384060, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384060</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384060.20150107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La SNC Champ de la Foux a demandé au tribunal administratif de Toulon de condamner la commune de Grimaud à lui verser une indemnité de 2 200 400 euros en réparation des préjudices qu'elle estime avoir subis en raison du caractère inconstructible de lots du lotissement les Hauts du clos de l'Avelan. Par un jugement n° 0900719 du 12 mai 2011, le tribunal administratif de Toulon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°s 11MA02598, 11MA02599 du 20 mars 2014, la cour administrative d'appel de Marseille a annulé ce jugement du tribunal administratif de Toulon du 12 mai 2011 et condamné la commune de Grimaud à verser à la SNC Champ de la Foux la somme de 108 498,47 euros, augmentée des intérêts au taux légal à compter du 17 décembre 2008 et des intérêts capitalisés.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
              Par une requête et un mémoire en réplique, enregistrés les 29 août et 10 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Grimaud demande au Conseil d'Etat d'ordonner le sursis à exécution de cet arrêt de la cour administrative d'appel de Marseille du 20 mars 2014.<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Grimaud et à la SCP Baraduc, Duhamel, Rameix, avocat de la SNC Champ de la Foux.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle rendue en dernier ressort, l'infirmation de la solution retenue par les juges du fond ".<br/>
<br/>
              2. D'une part, il ne ressort pas des pièces du dossier que la commune de Grimaud, en dépit de sa taille modeste, ne serait pas en mesure, eu égard à sa situation financière, de payer la somme de 108 498,47 euros, assortie des intérêts, que l'arrêt de la cour administrative d'appel de Marseille du 20 mars 2014 l'a condamnée à verser. D'autre part, il n'est pas établi que le versement immédiat de cette indemnité à la SNC Champ de la Foux, dont les associés répondent indéfiniment et solidairement des dettes sociales, l'exposerait à la perte définitive de cette somme au cas où le Conseil d'Etat ferait droit à son pourvoi en cassation. Ainsi, l'arrêt de la cour administrative d'appel de Marseille ne peut être regardé comme susceptible d'entraîner, pour la commune, des conséquences difficilement réparables.<br/>
<br/>
              3. L'une au moins des conditions posées par l'article R. 821-5 du code de justice administrative n'est, dès lors, pas satisfaite. Par suite, la commune n'est pas fondée à demander que soit ordonné le sursis à exécution de l'arrêt de la cour administrative d'appel de Marseille, sans qu'il soit besoin d'examiner si les moyens qu'elle invoque paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de cet arrêt, l'infirmation de la solution retenue par les juges du fond.<br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la SNC Champ de la Foux au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la commune de Grimaud est rejetée.<br/>
Article 2 : Les conclusions de la SNC Champ de la Foux présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la commune de Grimaud et à la SNC Champ de la Foux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
