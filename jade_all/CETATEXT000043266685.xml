<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043266685</ID>
<ANCIEN_ID>JG_L_2021_03_000000435139</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/26/66/CETATEXT000043266685.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 17/03/2021, 435139, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435139</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435139.20210317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 4 octobre 2019 et les 2 juin et 6 août 2020 au secrétariat du contentieux du Conseil d'Etat, la société par actions simplifiée Amomed Pharma et la société par actions simplifiée Centre spécialités pharmaceutiques demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 11 avril 2019 par laquelle la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont refusé l'inscription de la spécialité Reverpleg 40 U.I/2ml, solution à diluer pour perfusion, sur la liste des spécialités pharmaceutiques agréées à l'usage des collectivités publiques prévue à l'article L. 5123-2 du code de la santé publique, ainsi que la décision implicite de rejet de leur recours gracieux formé le 7 juin 2019 ; <br/>
<br/>
              2°) d'enjoindre à ces ministres de procéder à cette inscription dans un délai d'un mois à compter de la décision à intervenir ou, subsidiairement, de réexaminer la demande d'inscription de cette spécialité dans un délai de deux mois ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 89/105/CEE du Conseil du 21 décembre 1988 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... F..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 5123-2 du code de la santé publique, l'achat, la fourniture, la prise en charge et l'utilisation par les collectivités publiques des médicaments disposant d'une autorisation de mise sur le marché sont limités, dans les conditions, propres à ces médicaments, fixées par le décret mentionné à l'article L. 162-17 du code de la sécurité sociale, aux produits agréés dont la liste est établie par arrêté des ministres chargés de la santé et de la sécurité sociale. Cette liste précise les seules indications thérapeutiques ouvrant droit à la prise en charge des médicaments. Il résulte des dispositions combinées des articles L. 5123-3 du code de la santé publique et L. 161-37 du code de la sécurité sociale que l'inscription sur cette liste est proposée par la commission de la transparence de la Haute Autorité de santé.<br/>
<br/>
              2. Il ressort des pièces du dossier que la spécialité Reverpleg 40 U.I/2ml, solution à diluer pour perfusion, bénéficie d'une autorisation de mise sur le marché en France depuis le 31 mai 2018 dans l'indication du traitement de l'hypotension réfractaire aux catécholamines consécutive à un choc septique chez les patients âgés de plus de dix-huit ans. La société Centre spécialités pharmaceutiques, qui exploite la spécialité Reverpleg, a demandé le 5 juillet 2018 son inscription sur la liste des spécialités agréées à l'usage des collectivités publiques prévue à l'article L. 5123-2 du code de la santé publique. Par une décision du 11 avril 2019, la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont refusé l'inscription sur cette liste en se fondant sur l'avis rendu par la commission de la transparence de la Haute Autorité de santé, le 27 février 2019, selon lequel le service médical rendu par cette spécialité était insuffisant. La société Centre spécialités pharmaceutiques et la société Amomed Pharma, filiale française de la société détentrice de l'autorisation de mise sur le marché, demandent l'annulation pour excès de pouvoir de cette décision, ainsi que de la décision implicite rejetant leur recours gracieux.<br/>
<br/>
              Sur la légalité des décisions attaquées :<br/>
<br/>
              En ce qui concerne la compétence des signataires de la décision attaquée :<br/>
<br/>
              3. Aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : (...) 2° Les chefs de service, directeurs adjoints, sous-directeurs (...) ". En vertu de ces dispositions, M. B... C..., reconduit à compter du 30 août 2016 par arrêté publié au Journal officiel de la République française du 11 août 2016 dans les fonctions de sous-directeur du financement du système de soins à la direction de la sécurité sociale à l'administration centrale du ministère des finances et des comptes publics et du ministère des affaires sociales et de la santé, avait qualité pour signer la décision attaquée aux noms de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics, en leur qualité de ministres chargés de la sécurité sociale. De même, Mme A... E..., reconduite à compter du 7 avril 2018 par arrêté publié au Journal officiel du 18 mars 2018 dans les fonctions de sous-directrice de la politique des produits de santé et de la qualité des pratiques et des soins à la direction générale de la santé à l'administration centrale du ministère des solidarités et de la santé, avait qualité pour signer la décision attaquée au nom de la ministre des solidarités et de la santé, en sa qualité de ministre chargée de la santé. Par suite, le moyen tiré de l'incompétence des signataires de la décision attaquée doit être écarté.<br/>
<br/>
              En ce qui concerne la motivation des décisions attaquées :<br/>
<br/>
              4. Pour assurer la transposition des objectifs résultant du 2) de l'article 6 de la directive 89/105/CEE du 21 décembre 1988 du Conseil concernant la transparence des mesures régissant la fixation des prix des médicaments à usage humain et leur inclusion dans le champ d'application des systèmes nationaux d'assurance-maladie, selon lesquels : " Toute décision de ne pas inscrire un médicament sur la liste des produits couverts par le système d'assurance maladie comporte un exposé des motifs fondé sur des critères objectifs et vérifiables, y compris, si nécessaire, les avis ou recommandations sur lesquels les décisions s'appuient (...) ", l'article R. 163-14 du code de la sécurité sociale dispose que : " Les décisions portant refus d'inscription sur la liste prévue (...) à l'article L. 5123-2 du code de la santé publique (...) sont communiquées à l'entreprise avec la mention des motifs de ces décisions ainsi que des voies et délais de recours qui leur sont applicables ".<br/>
<br/>
              5. La décision attaquée mentionne les dispositions dont elle fait application et indique se fonder sur l'insuffisance du service médical rendu par Reverpleg en s'appropriant les motifs de l'avis du 27 février 2019 de la commission de la transparence de la Haute Autorité de santé, précédemment communiqué à la société pétitionnaire et publié sur le site internet de la Haute Autorité de santé. Les requérantes ne sont ainsi pas fondées à soutenir que la règle de motivation applicable à cette décision, qui n'implique pas que le rejet du recours gracieux soit lui-même motivé, aurait été méconnue.<br/>
<br/>
              En ce qui concerne la procédure suivie devant la commission de la transparence :<br/>
<br/>
              6. En premier lieu, aux termes du I de l'article L. 1451-1 du code de la santé publique : " Les membres des commissions et conseils siégeant auprès des ministres chargés de la santé et de la sécurité sociale, (...) les membres des instances collégiales, des commissions, des groupes de travail et conseils des autorités et organismes mentionnés (...) à l'article L. 161-37 du code de la sécurité sociale (...) sont tenus, lors de leur prise de fonctions, d'établir une déclaration d'intérêts ". L'article R. 161-85 du code de la sécurité sociale prévoit que : " Les personnes (...) qui apportent leur concours au collège ou aux commissions spécialisées de la Haute Autorité et les membres des commissions spécialisées ne peuvent (...) traiter une question dans laquelle elles auraient un intérêt direct ou indirect (...). / Les personnes mentionnées au précédent alinéa sont soumises aux dispositions de l'article L. 1451-1 du code la santé publique (...) ". Enfin les I et II de l'article R. 1451-3 du code de la santé publique prévoient que : " Les déclarations d'intérêts sont établies et actualisées par télédéclaration (...) sur un site internet unique (...) " et que : " la publicité de toutes les déclarations publiques d'intérêts (...) est assurée, pendant la durée des fonctions (...) sur le site unique mentionné au I ". Il résulte de ces dispositions que la déclaration d'intérêts que les membres de la commission de la transparence sont tenus d'établir lors de leur prise de fonction et d'actualiser est souscrite par télédéclaration sur un site internet unique, qui en assure la publicité pendant la durée de leurs fonctions. Les requérantes, qui n'apportent aucune précision au soutien de leur moyen selon lequel elles n'auraient pu accéder aux déclarations d'intérêts ainsi publiées des membres de la commission de la transparence, ne sont pas fondées à soutenir que ces déclarations auraient dû leur être communiquées pour qu'elles soient mises en mesure de savoir s'il existait des liens d'intérêts susceptibles de compromettre l'indépendance et l'impartialité des membres de la commission.<br/>
<br/>
              7. En deuxième lieu, aux termes des I et II de l'article R. 163-16 du code de la sécurité sociale dans sa rédaction applicable au litige : " Les délibérations de la commission mentionnée à l'article R. 163-15 ne sont valables que si au moins quatorze membres ayant voix délibérative de la commission sont présents / Les avis sont pris à la majorité des suffrages (...) ". Il ressort des pièces du dossier, en particulier du compte rendu de l'avis définitif du 27 février 2019 de la commission de la transparence, versé au dossier, que lors de la séance au cours de laquelle la commission de la transparence s'est définitivement prononcée sur le service médical rendu de la spécialité Reverpleg après avoir entendu les sociétés requérantes à leur demande, le quorum exigé par le I de l'article R. 163-16, soit quatorze membres ayant voix délibérative, était atteint et que la commission a adopté son avis concluant à un service médical rendu insuffisant par quatorze voix contre cinq en faveur d'un service médical rendu faible et une abstention. Par suite, les requérantes ne sont pas fondées à soutenir que l'avis rendu par la commission l'aurait été en méconnaissance des règles de quorum et de majorité applicables.<br/>
<br/>
              En ce qui concerne l'appréciation du service médical rendu par la spécialité en litige :<br/>
<br/>
              8. En premier lieu, aux termes du I de l'article R. 163-3 du code de la sécurité sociale dans sa rédaction applicable au litige : " Les médicaments sont inscrits sur la liste prévue au premier alinéa de l'article L. 162-17 au vu de l'appréciation du service médical rendu qu'ils apportent indication par indication. Cette appréciation prend en compte l'efficacité et les effets indésirables du médicament, sa place dans la stratégie thérapeutique, notamment au regard des autres thérapies disponibles, la gravité de l'affection à laquelle il est destiné, le caractère préventif, curatif ou symptomatique du traitement médicamenteux et son intérêt pour la santé publique. Les médicaments dont le service médical rendu est insuffisant au regard des autres médicaments ou thérapies disponibles ne sont pas inscrits sur la liste ". Le motif tiré du niveau du service médical rendu est également de nature à fonder légalement le refus d'inscription sur la liste des spécialités agréées à l'usage des collectivités mentionnée à l'article L. 5123-2 du code de la santé publique. En outre, en vertu de l'article R. 163-18 du code de la sécurité sociale, l'avis de la commission de la transparence de la Haute Autorité de santé, qui précède ces décisions, comporte notamment : " 1° L'appréciation du bien-fondé, au regard du service médical rendu, de l'inscription du médicament sur les listes, ou l'une des listes, prévues au premier alinéa de l'article L. 162-17 et à l'article L. 5123-2 du code de la santé publique ; / L'avis porte distinctement sur chacune des indications thérapeutiques mentionnées par l'autorisation de mise sur le marché (...) ". <br/>
<br/>
              9. Il résulte de ces dispositions que l'inscription d'un médicament sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique, comme d'ailleurs sur celle prévue au premier alinéa de l'article L. 162-17 du code de la sécurité sociale, sur le bien-fondé de laquelle la commission de la transparence émet un avis, est effectuée au vu d'une appréciation du service médical rendu que ce médicament apporte, indication par indication. Si cette appréciation doit être portée sur chaque indication telle qu'elle est mentionnée par l'autorisation de mise sur le marché, l'autorisation dont la spécialité dispose ne fait en revanche pas obstacle à ce que son inscription sur la liste prévue à l'article L. 5123-2 du code de la santé publique, qui relève d'une législation indépendante de celle régissant l'autorisation de mise sur le marché, soit refusée au motif que les études cliniques fournies à l'appui de la demande d'inscription ne permettent pas de regarder le service médical rendu de cette spécialité comme suffisant, dans l'indication de son autorisation de mise sur le marché, pour justifier sa prise en charge par l'assurance maladie.<br/>
<br/>
              10. En l'espèce, l'indication pour laquelle la spécialité Reverpleg dispose d'une autorisation de mise sur le marché est le traitement de l'hypotension réfractaire aux catécholamines consécutive à un choc septique chez les patients âgés de plus de dix-huit ans. Le résumé des caractéristiques du produit précise que cette situation d'hypotension réfractaire est caractérisée lorsque, en dépit d'un remplissage vasculaire adéquat et d'un traitement par les catécholamines, la pression artérielle moyenne ne peut être stabilisée à une valeur cible habituellement retenue, pour les patients de réanimation, entre 65 et 75 millimètres de mercure (mmHg). De même, dans son avis du 27 février 2019, la commission de la transparence relève que si la définition d'un état de choc septique réfractaire n'est, à ce jour, pas consensuelle, la pression artérielle moyenne recommandée dans le choc septique est une valeur cible supérieure à 65 mmHg. Or l'avis de la commission de la transparence relève que la démonstration de l'efficacité hémodynamique de Reverpleg n'est apportée que sur le maintien d'une pression artérielle moyenne cible de plus de 65 mmHg chez des patients ayant déjà un choc septique stabilisé, qui ne présentent donc pas d'hypotension réfractaire aux catécholamines. L'avis retient en outre qu'il n'est pas démontré que l'administration de Reverpleg réduirait la morbi-mortalité des patients de manière statistiquement significative. Il ne ressort pas des pièces versées au dossier que cet avis reposerait sur des faits matériellement inexacts ou que l'appréciation portée sur le service médical rendu par Reverpleg, pour l'indication définie par l'autorisation de mise sur le marché, serait entachée d'une erreur manifeste. A cet égard, les sociétés requérantes ne peuvent utilement faire valoir l'intérêt de Reverpleg pour diminuer les doses de noradrénaline administrées aux patients en choc septique, tout en maintenant leur tension artérielle, qui ne correspond pas à l'indication retenue par l'autorisation de mise sur le marché.<br/>
<br/>
              11. En deuxième lieu,  pour apprécier l'intérêt pour la santé publique de la spécialité considérée dans l'indication de son autorisation de mise sur le marché, la commission de la transparence a pris en considération, conformément à sa doctrine et au vu des recommandations internationales, le besoin médical, la gravité de la maladie concernée et la prévalence de la population cible, ainsi que l'impact potentiel supplémentaire du médicament sur l'état de santé de la population considérée en termes de morbidité et de mortalité, par rapport aux alternatives thérapeutiques. Les sociétés requérantes ne sont pas fondées à soutenir que les ministres, qui ont pris leur décision au vu de cet avis sans s'estimer être liés par lui, auraient commis une erreur manifeste d'appréciation en estimant que l'intérêt de Reverpleg n'était pas établi pour la santé publique au regard du besoin thérapeutique. Elles ne peuvent utilement invoquer au soutien de leur recours pour excès de pouvoir des études dont elles ne s'étaient pas prévalues ou un rapport, demandé par la direction générale de la santé au professeur Djilali Annane, publié après les décisions attaquées.<br/>
<br/>
              En ce qui concerne les autres moyens :<br/>
<br/>
              12. Il ressort des pièces du dossier que les spécialités pharmaceutiques qui disposent d'une autorisation de mise sur le marché pour un traitement au même niveau de stratégie thérapeutique, en tant que vasopresseurs intraveineux utilisés en seconde intention, en complément de la noradrénaline, dans la prise en charge hémodynamique des mêmes patients en choc septique, sont l'adrénaline Aguettant et l'adrénaline Renaudin. Dès lors, la commission de la transparence a, en tout état de cause, pu les retenir comme comparateurs cliniquement pertinents dans l'appréciation qu'elle a également portée, en application de l'article R. 163-18 du code de la sécurité sociale, sur l'amélioration du service médical rendu apportée par Reverpleg, nonobstant la circonstance, qu'elle n'a pas ignorée, que la ternipressine et subsidiairement la desmopressine, qui ne possèdent pas d'autorisation de mise sur le marché dans le choc septique, sont davantage utilisées que l'adrénaline.<br/>
<br/>
              13. Eu égard au service médical insuffisant rendu par la spécialité Reverpleg, les requérantes ne sont pas fondées à soutenir que le refus de l'inscrire sur la liste prévue à l'article L. 5123-2 du code de la santé publique porterait atteinte au principe d'égal accès aux soins, créerait une inégalité en défaveur de Reverpleg ou favoriserait l'utilisation d'autres vasopresseurs en dehors de leur autorisation de mise sur le marché.<br/>
<br/>
              14. Il résulte de tout ce qui précède que la société Amomed Pharma et la société Centre spécialités pharmaceutique ne sont pas fondées à demander l'annulation pour excès de pouvoir des décisions qu'elles attaquent.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Amomed Pharma et de la société Centre spécialités pharmaceutique est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Amomed Pharma, première dénommée, pour les deux requérantes, et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la Haute Autorité de santé et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
