<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028353526</ID>
<ANCIEN_ID>JG_L_2013_12_000000352668</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/35/35/CETATEXT000028353526.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 20/12/2013, 352668</TITRE>
<DATE_DEC>2013-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352668</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352668.20131220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 14 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association Cercle de réflexion et de proposition d'action sur la psychiatrie (CRPA), dont le siège est 14, rue des Tapisseries à Paris (75017), représentée par son président ; l'association CRPA demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-847 du 18 juillet 2011 relatif aux droits et à la protection des personnes faisant l'objet de soins psychiatriques et aux modalités de leur prise en charge ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de la santé publique, modifié notamment par la loi n° 2011-803 du 5 juillet 2011 ;<br/>
<br/>
              Vu la loi n° 2013-869 du 27 septembre 2013 ;<br/>
<br/>
              Vu la décision du 8 février 2012 par laquelle le Conseil d'Etat statuant au contentieux a, d'une part, renvoyé au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par l'association CRPA en tant qu'elles portaient sur l'article L. 3211-2-1, le II de l'article L. 3211-12, le 3° du I de l'article L. 3211-12-1 et l'article L. 3213-8 du code de la santé publique dans leur rédaction issue de la loi du 5 juillet 2011, d'autre part, n'a pas renvoyé ces questions en tant qu'elles portaient sur l'article L. 3211-3, le IV de l'article L. 3211-12-1, les troisième et quatrième alinéas de l'article L. 3211-12-4, le deuxième alinéa du I de l'article L. 3213-1, le troisième alinéa de l'article L. 3213-4, l'article L. 3214-1 et le deuxième alinéa de l'article L. 3216-1 de ce code dans leur rédaction issue de cette même loi ;<br/>
<br/>
              Vu la décision n° 2012-235 QPC du 20 avril 2012 du Conseil constitutionnel statuant sur la question prioritaire de constitutionnalité soulevée par l'association CRPA ;<br/>
<br/>
              Vu l'arrêt n° 13-17 984 du 4 décembre 2013 par lequel la Cour de cassation a renvoyé au Conseil constitutionnel une question prioritaire de constitutionnalité portant sur l'article L. 3222-3 du code de la santé publique ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur la recevabilité des conclusions :<br/>
<br/>
              1. Considérant qu'à la suite de la loi du 5 juillet 2011 relative aux droits et à la protection des personnes faisant l'objet de soins psychiatriques et aux modalités de leur prise en charge est intervenu le décret du 18 juillet 2011 relatif aux droits et à la protection des personnes faisant l'objet de soins psychiatriques et aux modalités de leur prise en charge ; que l'association Cercle de réflexion et de proposition d'action sur la psychiatrie (CRPA) a demandé, dans sa requête introductive d'instance, l'annulation pour excès de pouvoir de la totalité de ce décret ; qu'en particulier, elle soutenait qu'il devait être annulé par voie de conséquence de la question prioritaire de constitutionnalité qu'elle soulevait ; que, par suite, et alors même que ses autres moyens ne concernaient initialement que les articles R. 3211-1, R. 3211-2, R. 3213-2 et R. 3222-2 introduits dans le code de la santé publique par le décret, elle est recevable, contrairement à ce que soutient le ministre des affaires sociales et de la santé, à demander l'annulation du décret en tant qu'il introduit dans ce code les articles R. 3222-3 à R. 3322-9 ;<br/>
<br/>
              Sur les effets de la décision n° 2012-235 QPC du 20 avril 2012 :<br/>
<br/>
              2. Considérant que, par cette décision, le Conseil constitutionnel a déclaré conformes à la Constitution l'article L. 3211-2-1 du code de la santé publique et le 3° du paragraphe I de l'article L. 3211-12-1 du même code dans leur rédaction issue de la loi du 5 juillet 2011 ; que le moyen tiré de ce que ces dispositions sont contraires aux droits et libertés garantis par la Constitution doit dès lors être écarté ; que si le Conseil constitutionnel a, par la même décision, déclaré contraires à la Constitution l'article L. 3213-8 et le paragraphe II de l'article L. 3211-12 dans leur rédaction issue de la même loi, il a cependant décidé que l'abrogation de ces articles ne prendrait effet qu'au 1er octobre 2013 et que les décisions prises avant cette date en application des dispositions déclarées contraires à la Constitution ne pourraient être contestées sur le fondement de cette inconstitutionnalité ; que, par suite, l'association CRPA ne saurait utilement soutenir que le décret du 18 juillet 2011 devrait être annulé en raison de l'inconstitutionnalité dont sont entachées les dispositions de l'article L. 3213-8 et du paragraphe II de l'article L. 3211-12 ; <br/>
<br/>
              En ce qui concerne l'article R. 3211-1 introduit dans le code de la santé publique par le décret attaqué :<br/>
<br/>
              3. Considérant qu'en vertu du 2° de l'article L. 3211-2-1 du code de la santé publique, dans sa rédaction applicable à la date d'adoption du décret attaqué, une personne faisant l'objet de soins psychiatriques sans consentement peut être prise en charge sous la forme de l'hospitalisation complète ou " sous une autre forme incluant des soins ambulatoires, pouvant comporter des soins à domicile, dispensés par un établissement mentionné au même article L. 3222-1 et, le cas échéant, des séjours effectués dans un établissement de ce type " ; qu'il résulte du II de l'article R. 3211-1 issu du décret attaqué que, lorsque le patient est pris en charge sous une forme alternative à l'hospitalisation complète, le programme de soins établi par le psychiatre de l'établissement " indique si la prise en charge du patient inclut une ou plusieurs des modalités suivantes : / 1° Une hospitalisation à temps partiel ; / 2° Des soins ambulatoires ; / 3° Des soins à domicile ; / 4° L'existence d'un traitement médicamenteux prescrit dans le cadre des soins psychiatriques. / Il précise, s'il y a lieu, la forme que revêt l'hospitalisation partielle en établissement de santé ou la fréquence des consultations ou des visites en ambulatoire ou à domicile et, si elle est prévisible, la durée pendant laquelle ces soins sont dispensés. (...) / Lorsque le programme inclut l'existence d'un traitement médicamenteux, il ne mentionne ni la nature ni le détail de ce traitement, notamment la spécialité, le dosage, la forme galénique, la posologie, la modalité d'administration et la durée " ;  <br/>
<br/>
              4. Considérant que dans sa décision n° 2012-235 QPC du 20 avril 2012, le Conseil constitutionnel a précisé que les mesures d'" hospitalisation partielle " prévues à l'article L. 3211-2-1 du code de la santé publique ne sauraient être exécutées sous la contrainte et que le juge des libertés et de la détention pouvait être saisi à tout moment, dans les conditions fixées par l'article L. 3211-12, aux fins d'ordonner à bref délai la mainlevée immédiate de telles mesures ; que la portée et, par suite, la légalité des dispositions réglementaires prises pour l'application de l'article L. 3211-2-1 doivent être appréciées sous les mêmes réserves d'interprétation ; que, dès lors, le moyen tiré de ce que l'article R. 3211-1, faute d'imposer un délai minimal entre deux périodes de présence du patient en hospitalisation partielle, porterait atteinte à la liberté d'aller et de venir et au droit au respect de la vie privée protégés par les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen, ainsi qu'aux stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ne peut qu'être écarté ; qu'en outre, contrairement à ce que soutient l'association CRPA, le droit au respect de la vie privée n'impliquait pas que le pouvoir réglementaire prévît un encadrement spécifique des traitements médicamenteux susceptibles d'être prescrits aux personnes faisant l'objet d'une mesure de soins sans consentement ; que l'association CRPA ne saurait sérieusement soutenir qu'en l'absence de précision sur ces deux points, le pouvoir réglementaire aurait empiété sur le domaine réservé à la loi par l'article 34 de la Constitution ; <br/>
<br/>
              En ce qui concerne l'article R. 3211-2 introduit dans le code de la santé publique par le décret attaqué :<br/>
<br/>
              5. Considérant que si cet article mentionne que le collège de soignants mis en place dans chaque établissement d'accueil est composé de " trois membres appartenant au personnel de l'établissement ", il se borne à réitérer sur ce point les dispositions de l'article L. 3211-9 du même code ; qu'en tout état de cause, cette composition ne porte, par elle-même, aucune atteinte au principe d'impartialité ; que le moyen tiré de ce que ces mêmes dispositions seraient incompatibles avec les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'est pas assorti de précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              En ce qui concerne l'article R. 3213-2 introduit dans le code de la santé publique par le décret attaqué :<br/>
<br/>
              6. Considérant qu'il résulte de l'article L. 3213-8 du même code, dans sa rédaction alors applicable, que, lorsqu'une mesure de soins concerne une personne faisant ou ayant déjà fait l'objet d'une hospitalisation en unité pour malades difficiles ou d'une hospitalisation ordonnée directement par l'autorité judiciaire ou par le préfet avisé par l'autorité judiciaire après un classement sans suite ou une déclaration d'irresponsabilité pénale, le représentant de l'Etat ne peut mettre fin à cette mesure, de sa propre initiative ou au vu d'un certificat médical attestant que les conditions ayant justifié l'admission ne sont plus remplies et que cette mesure peut être levée, qu'après l'avis du collège de soignants et deux avis concordants de deux psychiatres, qui doivent tous trois être produits dans les délais fixés par le représentant de l'Etat " dans une limite maximale fixée par décret en Conseil d'Etat " ; que le III de l'article R. 3213-2 prévoit que le délai imparti aux psychiatres ne peut excéder dix jours ;<br/>
<br/>
              7. Considérant que le respect des exigences constitutionnelles s'attachant à ce que les atteintes à la liberté individuelle garantie par l'article 66 de la Constitution et à la liberté personnelle protégée par les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen de 1789 soient toujours adaptées, nécessaires et proportionnées aux objectifs poursuivis, suppose, lorsqu'un certificat médical conclut à l'absence de nécessité d'une mesure de soins en hospitalisation, que la situation du patient soit réexaminée à bref délai afin de vérifier si son hospitalisation reste nécessaire ; que toutefois, contrairement à ce que soutient la requérante, la fixation d'un délai maximal de dix jours ne méconnaît pas, par elle-même, ces exigences ; que, par ailleurs, les dispositions contestées n'ont ni pour objet ni pour effet de priver la personne concernée d'aucun des droits dont elle dispose en vertu de la loi, en particulier de l'article L. 3211-3 du code de la santé publique ;<br/>
<br/>
              En ce qui concerne les articles R. 3222-2 à R. 3222-9 introduits dans le code de la santé publique par le décret attaqué :<br/>
<br/>
              8. Considérant, en premier lieu, qu'aux termes de l'article L. 3222-3 du code de la santé publique, alors en vigueur : " Les personnes faisant l'objet de soins psychiatriques sous la forme d'une hospitalisation complète en application des chapitres III ou IV du titre Ier du présent livre ou de l'article 706-135 du code de procédure pénale peuvent être prises en charge dans une unité pour malades difficiles lorsqu'elles présentent pour autrui un danger tel que les soins, la surveillance et les mesures de sûreté nécessaires ne peuvent être mis en oeuvre que dans une unité spécifique. / Les modalités d'admission dans une unité pour malades difficiles sont prévues par décret en Conseil d'Etat " ; que l'article R. 3222-2 fixe les modalités d'admission des patients dans une unité pour malades difficiles, en précisant l'autorité compétente, la composition du dossier au vu duquel elle se prononce et la procédure à suivre en cas de désaccord du psychiatre responsable de l'unité ; que l'article R. 3222-3 concerne le transfert du malade de son lieu d'hospitalisation ou de détention à l'unité pour malades difficiles ; que les articles R. 3222-5 à R. 3222-8 fixent la composition de la commission de suivi médical, créée dans chaque département d'implantation d'une unité pour malades difficiles et précisent les modalités de la saisine de cette commission ; que ces articles disposent également que celle-ci examine au moins tous les six mois le dossier de chaque patient hospitalisé dans une telle unité, qu'elle saisit le préfet lorsqu'elle estime que les conditions du maintien de l'hospitalisation d'un patient en unité pour malades difficiles ne sont plus remplies, que le préfet prononce alors par arrêté la sortie du patient de cette unité et qu'elle visite l'unité au moins une fois par semestre ; que les articles R. 3222-2, R. 3222-3, R. 3222-5, R. 3222-6, R. 3222-7 et R. 3222-8 déterminent ainsi les modalités d'admission dans une unité pour malades difficiles et n'excèdent pas le champ de l'habilitation donnée au pouvoir réglementaire par le dernier alinéa de l'article L. 3222-3 du code de la santé publique ; <br/>
<br/>
              9. Considérant, toutefois, que le Conseil constitutionnel est saisi, à la date de la présente décision, d'une question prioritaire de constitutionnalité portant sur l'article L. 3222-3 du code de la santé publique, qui lui a été renvoyée par un arrêt du 4 décembre 2013 de la Cour de cassation ; que, dans ces conditions, il y a lieu de surseoir à statuer sur les conclusions de l'association requérante tendant à l'annulation du décret en tant que celui-ci a introduit les articles réglementaires mentionnés au point 8, jusqu'à ce que le Conseil constitutionnel se soit prononcé sur cette question ; <br/>
<br/>
              10. Considérant, en deuxième lieu, que l'article R. 3222-4 du code de la santé publique se borne à préciser que l'admission dans une unité pour malades difficiles ne fait pas obstacle à l'autorisation de sorties accompagnées de courte durée prévues à l'article L. 3211-11-1 ; qu'en rappelant ainsi une garantie prévue par le législateur et applicable à l'ensemble des personnes faisant l'objet de soins psychiatriques sous la forme d'une hospitalisation complète, le pouvoir réglementaire n'a pas excédé sa compétence ; <br/>
<br/>
              11. Considérant, en dernier lieu, que les articles L. 3211-12, L. 3213-1 et L. 3213-8, dans leur rédaction alors applicable, faisaient découler d'une hospitalisation en unité pour malades difficiles excédant une certaine durée, dès lors qu'elle avait eu lieu au cours des dix dernières années, des règles différentes de celles applicables aux autres personnes admises en hospitalisation complète, en prévoyant notamment, en ce qui concerne la levée des soins, des règles plus rigoureuses, et renvoyaient à un décret en Conseil d'Etat le soin de fixer cette durée minimale d'hospitalisation en unité pour malades difficiles ; que l'article R. 3222-9 se borne, conformément aux dispositions législatives mentionnées ci-dessus, à fixer cette durée à un an ; qu'en dehors des cas et conditions où il est saisi sur le fondement de l'article 61-1 de la Constitution, il n'appartient pas au Conseil d'Etat, statuant au contentieux, d'apprécier la conformité à la Constitution des dispositions législatives sur le fondement desquelles le pouvoir réglementaire est ainsi intervenu ; que si la loi du 27 septembre 2013 a abrogé l'article L. 3222-3 et modifié les articles L. 3211-12,  L. 3213-1 et L. 3213-8 du code de la santé publique, supprimant ainsi dans la partie législative de ce code toute référence aux unités pour malades difficiles, la légalité des dispositions réglementaires critiquées doit être appréciée à la date de leur édiction ; qu'ainsi, l'article R. 3222-9 n'est pas entaché d'incompétence ; <br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que l'association requérante n'est pas fondée à demander l'annulation du décret qu'elle attaque en tant qu'il insère dans le code de la santé publique les articles R. 3211-1, R. 3211-2, R. 3213-2, R. 3222-4 et R. 3222-9 ; qu'il y a lieu de surseoir à statuer sur ses conclusions à fin d'annulation en tant qu'elles concernent les articles R. 3222-2, R. 3222-3, R. 3222-5, R. 3222-6, R. 3222-7 et  R. 3222-8 de ce code, ainsi que sur ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur les conclusions à fin d'annulation présentées par l'association Cercle de réflexion et de proposition d'action sur la psychiatrie, en tant qu'elles concernent les dispositions du décret introduisant les articles R. 3222-2, R. 3222-3, R. 3222-5, R. 3222-6, R. 3222-7 et  R. 3222-8 dans le code de la santé publique, ainsi que sur ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, jusqu'à ce que le Conseil constitutionnel se soit prononcé sur la question de prioritaire de constitutionnalité portant sur l'article L. 3222-3 du code de la santé publique que lui a renvoyée la Cour de cassation. <br/>
Article 2 : Le surplus des conclusions de la requête de l'association Cercle de réflexion et de proposition d'action sur la psychiatrie est rejeté. <br/>
Article 3 : La présente décision sera notifiée à l'association Cercle de réflexion et de proposition d'action sur la psychiatrie, au Premier ministre, à la garde des sceaux, ministre de la justice et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-07-01 PROCÉDURE. - QPC PENDANTE DEVANT LE CONS. CONST. PORTANT SUR LA BASE LÉGALE DES DISPOSITIONS RÉGLEMENTAIRES DONT L'ANNULATION EST DEMANDÉE - SURSIS À STATUER SUR LA DEMANDE D'ANNULATION.
</SCT>
<ANA ID="9A"> 54-10-07-01 A la date de la décision du Conseil d'Etat statuant au contentieux, une question prioritaire de constitutionnalité (QPC) portant sur l'article L. 3222-3 du code de la santé publique, qui habilite le pouvoir réglementaire à prévoir les modalités d'admission dans une unité pour malades difficiles, a été renvoyée au Conseil constitutionnel (Cons. const.) par la Cour de cassation.,,,Dans ces conditions, il y a lieu de surseoir à statuer sur les conclusions tendant à l'annulation des dispositons réglementaires prévoyant ces modalités jusqu'à ce que le Cons. const. se soit prononcé sur cette question.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
