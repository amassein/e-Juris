<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022487028</ID>
<ANCIEN_ID>JG_L_2010_07_000000327512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/48/70/CETATEXT000022487028.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 15/07/2010, 327512</TITRE>
<DATE_DEC>2010-07-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>327512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Emilie  Bokdam</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cortot-Boucher Emmanuelle</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:327512.20100715</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 9 juin 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Eric A, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; M. A demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt du 5 février 2009 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'il a interjeté du jugement du 30 mai 2006 du tribunal administratif de Versailles rejetant sa demande tendant à la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 2000, ainsi que des pénalités correspondantes, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du II de l'article 92 B du code général des impôts dans leur rédaction en vigueur jusqu'au 31 décembre 1999 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le II de l'article 92 B du code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emilie Bokdam, Auditeur,<br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de M. A ;  <br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'aux termes du II de l'article 92 B du code général des impôts dans sa rédaction en vigueur jusqu'au 31 décembre 1999 : " 1. A compter du 1er janvier 1992 ou du 1er janvier 1991 pour les apports de titres à une société passible de l'impôt sur les sociétés, l'imposition de la plus-value réalisée en cas d'échange de titres résultant d'une opération d'offre publique, de fusion, de scission, d'absorption d'un fonds commun de placement par une société d'investissement à capital variable réalisée conformément à la réglementation en vigueur ou d'un apport de titres à une société soumise à l'impôt sur les sociétés, peut être reportée au moment où s'opérera la cession, le rachat, le remboursement ou l'annulation des titres reçus lors de l'échange. / (...) / Le report est subordonné à la condition que le contribuable en fasse la demande et déclare le montant de la plus-value dans les conditions prévues à l'article 97. / (...) " ;<br/>
<br/>
              Considérant que M. A soutient que le dispositif du report d'imposition prévu par ces dispositions est contraire aux droits et libertés garantis par la Constitution, dès lors qu'il revêt un caractère confiscatoire en violation du droit de propriété et du principe d'égalité devant les charges publiques ;<br/>
<br/>
              Considérant toutefois qu'il ressort des pièces du dossier que M. A a été assujetti à une cotisation supplémentaire d'impôt sur le revenu au titre de l'année 2000 à la suite de la remise en cause par l'administration fiscale du montant des moins-values qu'il avait reportées de l'année 1999 sur l'année 2000 ; que M. A a refusé ce rehaussement en soutenant que la plus-value d'un montant de 7 759 964 F réalisée lors de l'apport d'actions de la société Axfin en contrepartie de titres de la société Consors AG, qu'il avait déclarée au titre de l'année 1999 et qu'il avait alors intégralement imputée sur des moins-values antérieures, aurait dû être déclarée au titre de l'année 2000 eu égard à la date réelle de cet échange de titres et qu'il aurait, par suite, dû bénéficier du régime du sursis d'imposition prévu par les dispositions de l'article 150-0 B du code général des impôts dans leur rédaction en vigueur à compter du 1er janvier 2000, de sorte que le montant de ses moins-values reportables de l'année 1999 sur l'année suivante aurait été plus élevé que celui calculé par l'administration et qu'il n'aurait pas dégagé de plus-value nette imposable au titre de l'année 2000 ; que les dispositions du II de l'article 92 B du code général des impôts n'ont été ni appliquées par l'administration à la plus-value réalisée lors de l'apport d'actions de la société Axfin en contrepartie de titres de la société Consors AG, ni l'objet, à quelque stade que ce soit, d'une demande de la part de M. A tendant à obtenir le bénéfice du régime qu'elles instaurent, ni invoquées par les parties à l'appui des moyens qu'elles ont soulevés devant les juges du fond ou des moyens de cassation qui sont dirigés contre l'arrêt du 5 février 2009 de la cour administrative d'appel de Versailles ; que ces dispositions dont la conformité à la Constitution est contestée ne sauraient, par suite, être regardées comme applicables au présent litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions du II de l'article 92 B du code général des impôts dans leur rédaction en vigueur au 31 décembre 1999 portent atteinte au droit de propriété et au principe d'égalité devant les charges publiques garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A.<br/>
Article 2 : La présente décision sera notifiée à M. Eric A et au ministre du budget, des comptes publics et de la réforme de l'Etat.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-01-03 PROCÉDURE. - DISPOSITION QUI N'A PAS ÉTÉ APPLIQUÉE À LA SITUATION À L'ORIGINE DU LITIGE, DONT LE BÉNÉFICE N'A PAS ÉTÉ REVENDIQUÉ ET DONT AUCUN DES MOYENS INVOQUÉS AU COURS DU LITIGE NE FAIT ÉTAT.
</SCT>
<ANA ID="9A"> 54-10-05-01-03 Question prioritaire de constitutionnalité soulevée à l'encontre du régime de report d'imposition de certaines plus-values réalisées sur échange de titres figurant au II de l'ancien article 92 B du code général des impôts. Cette disposition n'est pas applicable au litige au sens et pour l'application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 dès lors que l'imposition à l'origine du litige n'a pas été fondée sur cet article, que le contribuable n'en a pas demandé, à quelque stade que ce soit, le bénéfice et qu'aucun des moyens articulés par les parties devant les juges du fond ni aucun des moyens de cassation n'en fait état.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
