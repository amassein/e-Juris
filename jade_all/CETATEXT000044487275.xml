<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487275</ID>
<ANCIEN_ID>JG_L_2021_12_000000459131</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487275.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 07/12/2021, 459131, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459131</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459131.20211207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 3 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme B... D..., première requérante dénommée, et autres demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de reporter au 2 octobre 2022 la troisième consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie prévue le 12 décembre 2021. <br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - ils justifient d'un intérêt à agir ; <br/>
              - la condition d'urgence est satisfaite dès lors que la troisième consultation sur l'indépendance de la Nouvelle-Calédonie doit se tenir le 12 décembre 2021 ;<br/>
              - il est porté une atteinte grave et manifestement illégale au principe d'égalité devant le suffrage et au principe de libre expression du suffrage ; <br/>
              - le maintien des élections, qui doivent être regardées comme constituant un scrutin solennel, méconnaît les principes d'égalité devant le suffrage et le principe de libre expression du suffrage dès lors que des circonstances de fait et des motifs impérieux d'intérêt général liés à la crise sanitaire et à la proximité, dans le temps, des élections présidentielles rendent leur tenue impossible. <br/>
<br/>
              Par un mémoire en défense, enregistré le 5 décembre 2021, le ministre des outre-mer conclut au rejet de la requête. Il soutient que deux des associations requérantes ne justifient pas d'un intérêt à agir, que la condition d'urgence n'est pas satisfaite et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de l'intérieur qui n'ont pas produit d'observations. <br/>
              Vu les pièces complémentaires produites par le ministre des outre-mer, enregistrées le 6 décembre 2021 ; <br/>
<br/>
              Vu les pièces nouvelles, enregistrées le 7 décembre 2021, produites après la clôture de l'instruction par les requérants ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
              - la loi n° 2021-1172 du 11 septembre 2021 ; <br/>
              - le décret n° 2018-424 du 30 mai 2018 ; <br/>
              - le décret n° 2021-866 du 30 juin 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, les requérants, et d'autre part, le Premier ministre, le ministre de l'intérieur et le ministre des outre-mer ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 6 décembre 2021, à 10 heures : <br/>
<br/>
              - le représentant des requérants ; <br/>
<br/>
              - Mme A... C..., requérante ; <br/>
<br/>
              - le représentant du ministre de l'intérieur ; <br/>
<br/>
              - le représentant du ministre des outre-mer ;  <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au  décembre 2021 à 13 heures ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Aux termes de l'article 76 de la Constitution : " Les populations de la Nouvelle-Calédonie sont appelées à se prononcer avant le 31 décembre 1998 sur les dispositions de l'accord signé à Nouméa le 5 mai 1998 et publié le 27 mai 1998 au Journal officiel de la République française. / (...) Les mesures nécessaires à l'organisation du scrutin sont prises par décret en Conseil d'Etat délibéré en conseil des ministres ". Son article 77 dispose que : " Après approbation de l'accord lors de la consultation prévue à l'article 76, la loi organique, prise après avis de l'assemblée délibérante de la Nouvelle-Calédonie, détermine, pour assurer l'évolution de la Nouvelle-Calédonie dans le respect des orientations définies par cet accord et selon les modalités nécessaires à sa mise en œuvre : / (...) -les conditions et les délais dans lesquels les populations intéressées de la Nouvelle-Calédonie seront amenées à se prononcer sur l'accession à la pleine souveraineté (...) ".<br/>
<br/>
              3. Aux termes de l'article 217 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie : " La consultation est organisée au cours du mandat du congrès qui commencera en 2014 (...)./ Si la majorité des suffrages exprimés conclut au rejet de l'accession à la pleine souveraineté, une deuxième consultation sur la même question peut être organisée à la demande écrite du tiers des membres du congrès, adressée au haut-commissaire et déposée à partir du sixième mois suivant le scrutin. La nouvelle consultation a lieu dans les dix-huit mois suivant la saisine du haut-commissaire à une date fixée dans les conditions prévues au II de l'article 216./(...)/Si, lors de la deuxième consultation, la majorité des suffrages exprimés conclut à nouveau au rejet de l'accession à la pleine souveraineté, une troisième consultation peut être organisée dans les conditions prévues aux deuxième et troisième alinéas du présent article. Pour l'application de ces mêmes deuxième et troisième alinéas, le mot : "deuxième" est remplacé par le mot : "troisième"./(...). " Aux termes du II de l'article 216 de cette loi organique : " Les électeurs sont convoqués par décret en conseil des ministres, après consultation du gouvernement et du congrès de la Nouvelle-Calédonie. (...)/La publication au Journal officiel de la Nouvelle-Calédonie du décret de convocation des électeurs appelés à participer à la consultation intervient au plus tard quatre semaines avant le jour du scrutin./ (...) ". <br/>
<br/>
              4. Par un décret du 30 juin 2021, la date de la troisième consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie prévue par l'article 77 de la Constitution a été fixée au dimanche 12 décembre 2021.<br/>
<br/>
              5. Mme D... et autres demandent au juge des référés du Conseil d'Etat statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au Gouvernement de reporter cette consultation au 2 octobre 2022. <br/>
<br/>
              6. En premier lieu, les requérants font valoir que leur demande de report serait justifiée par l'existence de circonstances sanitaires exceptionnelles. Toutefois, il résulte de l'instruction que si la Nouvelle-Calédonie a été confrontée à une flambée épidémique sans précédent au mois de septembre 2021 avec un taux d'incidence atteignant jusqu'à 1 200 contaminations au virus SARS-CoV-2 pour 100 000 habitants, et déplore 279 victimes à ce jour de la covid-19, la situation épidémiologique s'est depuis nettement améliorée avec une diminution régulière du taux d'incidence qui s'établit désormais, à la date du 6 décembre 2021, à 48. Sur le terrain sanitaire, le taux de vaccination s'établit à 77,7 % de la population vaccinable, soit 63,13 % de la population totale et 8 patients seulement sont actuellement en réanimation. Cette amélioration des indicateurs, quand bien même il existerait des disparités selon les parties et les populations du territoire, a justifié que l'état d'urgence sanitaire, déclaré par la loi du 11 septembre 2021 autorisant la prorogation de l'état d'urgence sanitaire dans les outre-mer, ne soit pas prolongé au-delà du 15 novembre 2021, ainsi que l'allègement des mesures de protection sanitaires locales. <br/>
<br/>
              7. Il résulte également de l'instruction que les mesures de protection sanitaire encore maintenues ne font pas obstacle au déroulement de la campagne référendaire : ainsi, les déplacements sont désormais autorisés sur l'ensemble du territoire de la Nouvelle-Calédonie, la mesure limitant à 30 le nombre de participants aux réunions ne s'applique pas à celles à caractère politique permettant par suite l'organisation de grands rassemblements dans le cadre de la campagne et les partis et groupements politiques habilités à participer à la campagne officielle ont eu régulièrement accès au service public de radio et de télévision, pu adresser aux électeurs une circulaire et apposer des affiches sur les panneaux électoraux. Par ailleurs, il ressort de la circulaire du 15 novembre 2021 du ministre de l'intérieur et du ministre des outre-mer sur l'organisation matérielle et le déroulement des opérations électorales que des mesures strictes ont été prévues pour garantir la sécurité sanitaire des électeurs lors des opérations de vote. <br/>
<br/>
              8. En deuxième lieu, les requérants font valoir que la date du 12 novembre 2021 intervient en période de deuil pour les proches des nombreuses victimes de la covid-19 et contrevient aux pratiques coutumières des calédoniens d'origine kanak, particulièrement frappés par la maladie et pour lesquels le deuil d'un an décrété par le sénat coutumier se caractérise notamment par de grandes cérémonies traditionnelles et une période de recueillement. Toutefois cette circonstance ne saurait caractériser, par elle-même, une atteinte à une liberté fondamentale de nature à justifier que le juge des référés, saisi sur le fondement du L. 521-1 du code de justice administrative, ordonne le report du scrutin. <br/>
<br/>
              9. En troisième lieu, si les requérants soutiennent que jusqu'à 2 000 électeurs inscrits sur les listes électorales des communes de Bélèp, de l'île des Pins, de Lifou, de Maré et d'Ouvéa n'auraient pu, en raison des mesures restrictives instituées sur le territoire de Nouvelle-Calédonie par un arrêté du 6 septembre 2021 du haut-commissaire de la République en Nouvelle-Calédonie et du président du gouvernement de la Nouvelle-Calédonie, s'inscrire dans des bureaux de vote délocalisés à Nouméa ainsi que le permet le décret du 30 mai 2018 pris pour l'application de l'article 3 de la loi organique du 19 avril 2018 relative à la consultation sur l'accession de la Nouvelle-Calédonie à la pleine souveraineté, il ne ressort pas de l'instruction que la procédure d'inscription prévue par ce décret aurait été entravée par le contexte sanitaire : ainsi en particulier la période d'option initialement prévue entre le 27 septembre 2021 et le 23 octobre 2021 a été prolongée jusqu'au 6 novembre 2021 et des audiences foraines ont été tenues à Maré, Ouvéa et à l'île des Pins afin de permettre aux électeurs intéressés de s'inscrire, alors qu'au demeurant cette procédure particulière est principalement utilisée pour les habitants des localités éloignées précitées résidant d'ores et déjà à Nouméa et ses environs.<br/>
<br/>
              10. En dernier lieu, la circonstance que la consultation soit fixée à une date trop rapprochée des élections présidentielles n'est pas de nature à caractériser une atteinte à une liberté fondamentale. <br/>
<br/>
              11. Dans ces conditions, il ne résulte de l'instruction aucune circonstance de nature à justifier que le juge des référés du Conseil d'Etat, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, ordonne le report de la consultation prévue le 12 novembre 2021. Par suite, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir soulevées par le ministre des outre-mer et sur la condition d'urgence, il y a lieu de rejeter la requête présentée par Mme D... et autres. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme D... et autres est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... D..., première requérante dénommée, ainsi qu'au ministre des outre-mer et au ministre de l'intérieur. <br/>
Copie en sera adressée au Premier ministre, au Président du gouvernement de la Nouvelle-Calédonie, au Haut-commissaire de la République en Nouvelle-Calédonie et à la commission de contrôle de l'organisation et du déroulement de la consultation.<br/>
Fait à Paris, le 7 décembre 2021<br/>
Signé : Anne Egerszegi<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
