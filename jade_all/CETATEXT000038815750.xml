<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815750</ID>
<ANCIEN_ID>JG_L_2019_07_000000402345</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815750.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 24/07/2019, 402345, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402345</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:402345.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 402345, par une requête, des mémoires en réplique et des nouveaux mémoires, enregistrés les 10 août 2016, 11 juillet 2017, 26 septembre 2017, 7 novembre 2017, 8, 17 et 26 janvier 2018, 15 février 2018, 15, 21, 27 et 29 mars 2018 et 3 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la Société Tekimmo demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du ministre du logement et de l'habitat durable du 25 juillet 2016 définissant les critères de certification des compétences des personnes physiques opérateurs de repérages, d'évaluation périodique de l'état de conservation des matériaux et produits contenant de l'amiante, et d'examen visuel après travaux dans les immeubles bâtis et les critères d'accréditation des organismes de certification.<br/>
<br/>
<br/>
              2° Sous le n° 410094, par une requête, un mémoire en réplique et de nouveaux mémoires, enregistrés les 26 avril 2017, 3 et 27 octobre 2017, 5 janvier 2018, 2 février 2018, 18 avril 2018, 17 septembre 2018, 11 et 18 octobre 2018 et 3 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la Société Tekimmo Expertises demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 24 février 2017 modifiant l'arrêté du 25 juillet 2016 du ministre du logement et de l'habitat durable définissant les critères de certification des compétences des personnes physiques opérateurs de repérages, d'évaluation périodique de l'état de conservation des matériaux et produits contenant de l'amiante, et d'examen visuel après travaux dans les immeubles bâtis et les critères d'accréditation des organismes de certification.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
                          Vu : <br/>
                          - le code de la construction et de l'habitation ;<br/>
                          - le décret n° 2009-697 du 16 juin 2009 ;<br/>
                          - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Pour procéder au diagnostic technique prévu par l'article L. 271-6 du code de la construction et de l'habitation en cas de vente de tout ou partie d'un immeuble bâti, l'article R. 271-1 du même code prévoit qu'il " est recouru soit à une personne physique dont les compétences ont été certifiées par un organisme accrédité dans le domaine de la construction, soit à une personne morale employant des salariés ou constituée de personnes physiques qui disposent des compétences certifiées dans les mêmes conditions ", précise que " Les organismes autorisés à délivrer la certification des compétences sont accrédités par un organisme signataire de l'accord européen multilatéral pris dans le cadre de la coordination européenne des organismes d'accréditation " et renvoie à des arrêtés des ministres chargés du logement, de la santé et de l'industrie le soin d'en préciser les modalités d'application. Par un arrêté du 25 juillet 2016 modifié par un arrêté 24 février 2017, les ministres compétents ont fixé les critères de certification des compétences des personnes physiques opérateurs de repérages, d'évaluation périodique de l'état de conservation des matériaux et produits contenant de l'amiante, et d'examen visuel après travaux dans les immeubles bâtis et les critères d'accréditation des organismes de certification. La société requérante demande l'annulation pour excès de pouvoir de ces arrêtés par deux requêtes qu'il y a lieu de joindre pour statuer par une même décision.<br/>
<br/>
<br/>
<br/>
              Sur les fins de non-recevoir opposées à la requête n° 402345 :<br/>
<br/>
              2.	En premier lieu, contrairement à ce que soutient le ministre chargé du logement, la société Tekimmo, qui emploie des personnes disposant des compétences devant faire l'objet des certifications prévues par l'article R. 271-1 du code de la construction et de l'habitation et précisées par l'arrêté du 25 juillet 2016 modifié par celui du 24 février 2017, est recevable à demander au juge de l'excès de pouvoir l'annulation de ces arrêtés. <br/>
<br/>
              3.	En second lieu, si le ministre chargé du logement soutient, en défense, que la requête dirigée contre l'arrêté du 25 juillet 2016 serait tardive au motif que l'article 1er de cet arrêté, relatif à l'accréditation des organismes de certification visés à l'article R. 271-1 du code de la construction et de l'habitation, se borne à reprendre les dispositions de l'article 1er de l'arrêté du 21 novembre 2006 définissant les critères de certification des compétences des personnes physiques opérateurs de repérage et de diagnostic amiante dans les immeubles bâtis et les critères d'accréditation des organismes de certification, d'une part, la requête ne se borne pas à demander l'annulation de ces seules dispositions et, d'autre part, ces dispositions ne sont pas divisibles du reste de l'arrêté qui précise à la fois les exigences qui s'imposent à ces organismes et celles qui s'imposent aux personnes dont ils sont chargés de certifier le niveau de compétences. <br/>
<br/>
              Sur la légalité des arrêtés attaqués :<br/>
<br/>
              4.	Aux termes de l'article 17 du décret du 16 juin 2009 relatif à la normalisation : " Les normes sont d'application volontaire. / Toutefois, les normes peuvent être rendues d'application obligatoire par arrêté signé du ministre chargé de l'industrie et du ou des ministres intéressés. / Les normes rendues d'application obligatoire sont consultables gratuitement sur le site internet de l'Association française de normalisation ". Il résulte des termes mêmes de ces dispositions qu'une norme ne peut être rendue d'application obligatoire si elle n'est pas gratuitement accessible. Par suite, la société requérante est fondée à soutenir qu'en rendant d'application obligatoire la norme NF EN ISO/CEI 17024, dont il n'est pas contesté qu'elle n'est pas gratuitement accessible, l'arrêté du 25 juillet 2016 a méconnu les exigences posées par le décret du 16 juin 2009. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête n° 402345, que l'arrêté du 25 juillet 2016 doit être annulé. L'arrêté du 24 février 2017, qui a pour objet de modifier l'article 1er de l'arrêté du 25 juillet 2016 ainsi que l'intitulé des deux annexes, doit, sans qu'il soit besoin d'examiner les autres moyens de la requête n° 410094, être annulé par voie de conséquence de l'annulation de l'arrêté du 25 juillet 2016.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 25 juillet 2016 définissant les critères de certification des compétences des personnes physiques opérateurs de repérages, d'évaluation périodique de l'état de conservation des matériaux et produits contenant de l'amiante, et d'examen visuel après travaux dans les immeubles bâtis et les critères d'accréditation des organismes de certification et l'arrêté du 24 février 2017 modifiant cet arrêté sont annulés.<br/>
Article 2 : La présente décision sera notifiée à la Société Tekimmo Expertises, à la ministre de la transition écologique et solidaire, à la ministre des affaires sociales et de la santé et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales. <br/>
Copie en sera adressée à l'Association française de normalisation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
