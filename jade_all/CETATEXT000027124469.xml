<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027124469</ID>
<ANCIEN_ID>JG_L_2013_03_000000342214</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/12/44/CETATEXT000027124469.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 01/03/2013, 342214, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342214</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:342214.20130301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 5 août 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10/00001 du 2 juin 2010 par lequel la cour régionale des pensions de Metz, infirmant le jugement n° 09/00003 du 14 janvier 2010 du tribunal départemental des pensions de la Moselle, a accordé à M. B...A...la revalorisation de sa pension militaire d'invalidité, calculée initialement au grade d'adjudant-chef de l'armée de terre, en fonction de l'indice afférent au grade équivalent de maître principal de la marine nationale, sur les trois années antérieures au 21 juin 2006, date de sa demande préalable à l'administration ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.A... ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil, notamment son article 1351 ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de M.A...,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que par un jugement du 8 mars 2007 devenu définitif, le tribunal départemental des pensions de la Moselle a fait droit à la demande de M. A...tendant à la revalorisation de cette pension en fonction de l'indice, plus favorable, afférent au grade équivalent de maître principal de la marine nationale et a fixé le point de départ de cette revalorisation au 21 juin 2006, date de la demande préalable adressée par M. A...à l'administration ; que le ministre de la défense se pourvoit contre l'arrêt du 2 juin 2010 par lequel la cour régionale des pensions de Metz a annulé le jugement du tribunal départemental des pensions de la Moselle rejetant la demande de M. A...tendant à ce que lui soit versé les arrérages de sa pension revalorisée pour les trois années précédant le 21 juin 2006 et a fait droit à cette demande ; <br/>
<br/>
              2. Considérant que, si M. A...s'est prévalu des dispositions de l'article L. 108 du code des pensions militaires d'invalidité et des victimes de la guerre, qui édictent, dans un but d'intérêt général, une prescription destinée, notamment, à garantir la sécurité juridique des collectivités publiques en fixant un terme aux actions des demandeurs de pension pour réclamer le versement des arrérages de la revalorisation de sa pension pour les trois années précédant le 21 juin 2006, il résulte du jugement 8 mars 2007 devenu définitif que M. A... ne pouvait se prévaloir d'aucun droit à revalorisation de sa pension pour la période antérieure au 21 juin 2006 ; que ces dispositions ne peuvent être utilement invoquées par un pensionné pour faire échec à l'autorité de la chose jugée s'attachant à une décision juridictionnelle devenue définitive ayant limité son droit au paiement des arrérages de la pension dont il a obtenu la liquidation ou la révision ; qu'il résulte de ce qui précède que le ministre de la défense est fondé à demander l'annulation de l'arrêt du 2 juin 2010 par lequel la cour régionale des pensions de Metz a jugé que la demande de l'intéressé, fondée sur les dispositions de cet article L. 108, constituait une demande nouvelle ne se heurtant pas à l'autorité de la chose jugée attachée au jugement du 8 mars 2007 ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que, ainsi qu'il a été dit, le jugement du 8 mars 2007 du tribunal départemental des pensions de la Moselle, devenu définitif, est revêtu de l'autorité de la chose jugée y compris en tant qu'il rejette la demande de revalorisation de la pension de M. A...pour la période antérieure à sa demande, en date du 21 juin 2006 ; qu'il résulte de ce qui a été dit ci-dessus que M. A...ne peut utilement se fonder sur les dispositions de l'article L. 108 du code des pensions militaires d'invalidité et des victimes de la guerre pour faire échec à l'autorité de chose jugée s'y attachant ; que, par suite, il n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal départemental des pensions de la Moselle a rejeté sa demande de rappel des arrérages de sa pension revalorisée au titre des trois années antérieures à la date du 21 juin 2006 à laquelle le jugement du 8 mars 2007 a fixé le point de départ de cette revalorisation ;<br/>
<br/>
              5. Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur leur fondement par la SCP Waquet-Farge-Hazan, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Metz du 2 juin 2010 est annulé.<br/>
Article 2 : La requête présentée par M.A... devant la cour régionale des pensions de Metz et les conclusions présentées par la SCP Waquet-Farge-Hazan devant le Conseil d'Etat sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. B... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
