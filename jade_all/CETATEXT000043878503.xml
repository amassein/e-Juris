<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043878503</ID>
<ANCIEN_ID>JG_L_2021_07_000000447339</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/85/CETATEXT000043878503.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 30/07/2021, 447339, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447339</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Exécution</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447339.20210730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 410347 du 31 juillet 2019, le Conseil d'Etat, statuant au contentieux, a annulé la décision du 31 mars 2017 du ministre de l'intérieur refusant de prendre les mesures sollicitées par l'association " La Cimade " afin d'assurer le respect des délais d'enregistrement des demandes d'asile, fixés à l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, et lui a enjoint de prendre toutes mesures nécessaires pour que soient respectés ces délais d'enregistrement.<br/>
<br/>
              Par une demande, enregistrée le 8 décembre 2020 au secrétariat de la section du rapport et des études du Conseil d'Etat, l'association " La Cimade " demande au Conseil d'Etat d'enjoindre à l'Etat de prendre les mesures qu'implique l'exécution de la décision du 31 juillet 2019.<br/>
<br/>
              La section du rapport et des études du Conseil d'Etat a exécuté les diligences qui lui incombent en application de l'article R. 931-4 du code de justice administrative et la présidente de cette section a transmis la demande d'exécution au président de la section du contentieux.<br/>
<br/>
              Par une ordonnance n° 447339 du 21 janvier 2021, le président de la section du contentieux du Conseil d'Etat a décidé l'ouverture d'une procédure juridictionnelle.<br/>
<br/>
              La note que la présidente de la section du rapport et des études a adressée au président de la section du contentieux a été communiquée aux parties en application des dispositions de l'article R. 931-5 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 2015-925 du 29 juillet 2015 ;<br/>
              - la loi n° 2018-778 du 10 septembre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Par une décision du 31 juillet 2019, le Conseil d'Etat, statuant au contentieux, a annulé la décision du 31 mars 2017 du ministre de l'intérieur refusant de prendre les mesures sollicitées par l'association " La Cimade " afin d'assurer le respect des délais d'enregistrement des demandes d'asile, fixés à l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, et lui a enjoint de prendre toutes mesures nécessaires pour que soient respectés ces délais d'enregistrement. La Cimade a saisi le Conseil d'Etat, sur le fondement de l'article L. 911-5 du code de justice administrative, d'une demande pour assurer l'exécution de cette décision.<br/>
<br/>
              2.	L'article 6 de la directive 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013 relative à des procédures communes pour l'octroi et le retrait de la protection internationale prévoit que : " 1. Lorsqu'une personne présente une demande de protection internationale à une autorité compétente en vertu du droit national pour enregistrer de telles demandes, l'enregistrement a lieu au plus tard trois jours ouvrables après la présentation de la demande. / Si la demande de protection internationale est présentée à d'autres autorités qui sont susceptibles de recevoir de telles demandes, mais qui ne sont pas, en vertu du droit national, compétentes pour les enregistrer, les Etats membres veillent à ce que l'enregistrement ait lieu au plus tard six jours ouvrables après la présentation de la demande. / (...) 5. Lorsque, en raison du nombre élevé de ressortissants de pays tiers ou d'apatrides qui demandent simultanément une protection internationale, il est dans la pratique très difficile de respecter le délai prévu au paragraphe 1, les Etats membres peuvent prévoir de porter ce délai à dix jours ouvrables ". Aux termes de l'article 6 de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale : " Les Etats membres font en sorte que les demandeurs reçoivent, dans un délai de trois jours à compter de l'introduction de leur demande de protection internationale, un document délivré à leur nom attestant leur statut de demandeur ou attestant qu'ils sont autorisés à demeurer sur le territoire de l'Etat membre pendant que leur demande est en attente ou en cours d'examen (...) ". Selon l'article 17 de la même directive, " Les Etats membres font en sorte que les demandeurs aient accès aux conditions matérielles d'accueil lorsqu'ils présentent leur demande de protection internationale ". L'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile disposait, jusqu'au 1er mai 2021, que : " L'enregistrement des demandes d'asile a lieu au plus tard trois jours ouvrés après la présentation de la demande à l'autorité administrative compétente, sans condition préalable de domiciliation. Toutefois, ce délai peut être porté à dix jours ouvrés lorsqu'un nombre élevé d'étrangers demandent l'asile simultanément (...) ". Ces dispositions sont désormais reprises à l'article L. 521-4 du code.<br/>
<br/>
              3.	Ces dispositions, issues de la loi du 29 juillet 2015 relative à la réforme du droit d'asile, transposant les objectifs de la directive 2013/32/UE du 26 juin 2013, font peser sur l'Etat une obligation de résultat s'agissant des délais dans lesquels les demandes d'asile doivent être enregistrées. Le ministre de l'intérieur, dont les services sont chargés de l'enregistrement des demandes d'asile, doit être regardé comme ayant pris les mesures nécessaires pour que cette obligation soit satisfaite si au vu des données d'enregistrement des demandes, les délais moyens d'enregistrement dans la plupart des guichets uniques pour demandeurs d'asile (GUDA) sont conformes à ces dispositions, à moins que, si ce délai n'est pas atteint dans une part substantielle de ceux-ci, il soit établi que le non-respect de ces délais est dû à des circonstances purement locales propres à l'organisation ou au fonctionnement de chaque guichet. Si eu égard au nombre de demandes d'asiles déposées en France depuis le mois de février 2020, le délai applicable pour enregistrer une demande d'asile doit être regardé comme étant de trois jours ouvrés après la présentation de la demande à l'autorité administrative compétente, il n'en va pas de même en Île-de-France et en outre-mer, compte tenu du nombre élevé d'étrangers demandant simultanément l'asile dans ces territoires, où le délai applicable peut être porté à dix jours ouvrés.<br/>
<br/>
              4.	Il résulte de l'instruction que des recrutements supplémentaires d'agents vacataires ou titulaires ont été effectués dans les GUDA depuis 2017 et que la mise en œuvre de la loi du 10 septembre 2018 pour une immigration maîtrisée, un droit d'asile effectif et une intégration réussie, qui a modifié l'organisation et entendu améliorer le fonctionnement du dispositif d'accueil et de prise en charge des demandeurs d'asile, s'est accompagnée d'une poursuite et d'un renforcement de dispositifs d'appui, de conseil et de modernisation propres à réduire les délais d'enregistrement. La mise en œuvre de cette loi et de ces mesures a permis de constater, depuis la reprise d'activité des services au printemps 2020 dans un contexte restant marqué par la crise sanitaire, une amélioration des délais moyens d'enregistrement. <br/>
<br/>
<br/>
              Sur la situation en dehors de l'Île-de-France :<br/>
<br/>
              5.	Des éléments recueillis au cours de l'instruction, en particulier du tableau fourni par le ministre de l'intérieur, donnant, mois par mois, le délai d'enregistrement moyen observé par GUDA, il résulte qu'en dehors de l'Île-de-France, dans la quasi-totalité des guichets, l'éventuel non-respect des délais est dû à des circonstances purement locales, ponctuelles, propres à l'organisation ou au fonctionnement de chaque guichet qui n'appellent pas, à ce stade, de mesure d'organisation au niveau ministériel. Il n'y a pas lieu, dès lors, d'ordonner des mesures pour l'exécution de la décision du Conseil d'Etat du 31 juillet 2019 s'agissant de ces guichets.<br/>
<br/>
              Sur la situation en Île-de-France :<br/>
<br/>
              6.	En Île-de-France, qui représente plus de la moitié de la demande d'asile en métropole, les rendez-vous dans les structures de premier accueil, qui elles-mêmes délivrent les rendez-vous en GUDA pour l'enregistrement des demandes d'asile, sont délivrés, depuis mai 2018, par une plateforme téléphonique dédiée mise en œuvre par l'Office français de l'immigration et de l'intégration (OFII). <br/>
<br/>
              7.	En dépit de la séance orale d'instruction tenue par la 2ème chambre de la section du contentieux et de la mesure d'instruction écrite diligentée ensuite sur ce point, le ministre de l'intérieur n'a pas produit les éléments permettant d'établir qu'en moyenne un délai de dix jours est respecté entre la présentation et l'enregistrement des demandes d'asile en Île-de-France. Il en résulte que les différents éléments produits au cours de la procédure juridictionnelle ne permettent pas d'établir que l'obligation de résultat à laquelle l'Etat est soumis en matière d'enregistrement des demandes d'asile serait, s'agissant de l'Île-de-France, satisfaite. <br/>
<br/>
              8.	Par suite, eu égard au délai écoulé depuis l'intervention de la décision dont l'exécution est demandée et à l'importance qui s'attache au respect effectif des exigences découlant du droit de l'Union européenne, il y a lieu, dans les circonstances de l'affaire, de prononcer contre l'Etat, à défaut pour lui de justifier de cette exécution complète en Île-de-France dans un délai de quatre mois à compter de la notification de la présente décision, une astreinte de 500 euros par jour de retard jusqu'à la date à laquelle la décision du 31 juillet 2019 aura reçu une complète exécution. <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Une astreinte de 500 euros par jour est prononcée à l'encontre de l'Etat s'il n'est pas justifié, dans un délai de quatre mois à compter de la notification de la présente décision, du respect en Île-de-France du délai d'enregistrement des demandes d'asile de dix jours ouvrés après la présentation de la demande à l'autorité administrative compétente.<br/>
Article 2 : Le ministre de l'intérieur communiquera à la section du rapport et des études du Conseil d'Etat copie des actes justifiant des mesures prises pour exécuter la décision du 31 juillet 2019.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à l'association " La Cimade ", au ministre de l'intérieur et à l'Office français de l'immigration et de l'intégration. <br/>
Copie en sera adressée à la section du rapport et des études.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
