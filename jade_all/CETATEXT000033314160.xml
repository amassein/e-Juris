<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033314160</ID>
<ANCIEN_ID>JG_L_2016_10_000000393514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/31/41/CETATEXT000033314160.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre jugeant seule, 27/10/2016, 393514, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393514.20161027</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille de condamner l'Etat à lui verser la somme de 152 706,83 euros, assortie des intérêts au taux légal, en réparation du préjudice résultant du défaut d'affiliation par l'Etat au régime général de sécurité sociale et au régime de retraite complémentaire des agents non titulaires de l'Etat et des collectivités publiques pour les missions qu'il a effectuées au titre d'un mandat sanitaire. Par un jugement n° 1007352 du 24 février 2014, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA01779 du 13 juillet 2015, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 septembre et 14 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Marseille du 13 juillet 2015 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - la loi n° 89-412 du 22 juin 1989 ;<br/>
              - le code de justice administrative, notamment son article R. 611-8 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
- les conclusions de M. Jean Lessi, rapporteur public.<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A...B..., qui a exercé la profession de vétérinaire jusqu'en septembre 2003, date de son admission à la retraite, a accompli des actes de prophylaxie collective des maladies des animaux en vertu d'un mandat sanitaire dont il a été investi à compter de juillet 1957. Après le rejet de sa demande par le directeur départemental de la cohésion sociale et de la protection des populations des Alpes-de-Haute-Provence, le 14 septembre 2010, il a saisi le tribunal administratif de Marseille pour obtenir la condamnation de l'Etat à lui verser la somme de 152 706,83 euros, assortie des intérêts au taux légal, en réparation du préjudice que lui aurait causé le défaut de versement par l'Etat des cotisations dues par l'employeur au régime général d'assurance vieillesse et au régime de retraite complémentaire auxquels il devait être affilié en raison de cette activité. Par un jugement du 24 février 2014, le tribunal administratif de Marseille a rejeté sa demande, au motif que le délai de prescription avait commencé à courir à compter de la liquidation de sa pension de retraite en septembre 2003, qu'il n'avait été interrompu que par un courrier du 13 avril 2005 et que sa créance était ainsi prescrite à la date de sa demande indemnitaire du 15 juin 2010. La cour administrative d'appel de Marseille a, par un arrêt du 13 juillet 2015, rejeté la requête d'appel de M.B..., pour le même motif.<br/>
<br/>
              2. Aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis. (...) ". Aux termes de l'article 3 de cette même loi : " La prescription ne court ni contre le créancier qui ne peut agir, soit par lui-même ou par l'intermédiaire de son représentant légal, soit pour une cause de force majeure, ni contre celui qui peut être légitimement regardé comme ignorant l'existence de sa créance ou de la créance de celui qu'il représente légalement ". <br/>
<br/>
              3. En premier lieu, pour l'application de l'article 1er de la loi du 31 décembre 1968, une créance telle que celle dont se prévaut M. B...ne se rattache pas à chaque année au titre de laquelle les cotisations de sécurité sociale sont dues, non plus qu'à chaque année au cours de laquelle les pensions correspondantes auraient dû être versées, mais à l'année au cours de laquelle le préjudice peut être connu dans toute son étendue, c'est-à-dire celle au cours de laquelle l'intéressé cesse son activité et fait valoir ses droits à la retraite. Ainsi, le moyen tiré de l'erreur de droit commise, sur ce point, par la cour administrative d'appel doit être écarté.<br/>
<br/>
              4. En deuxième lieu, la nature de salaires des sommes correspondant à la rémunération des missions effectuées par un vétérinaire dans le cadre d'un mandat sanitaire avait été clairement établie, compte tenu notamment de la reconnaissance aux intéressés de la qualité d'agent public de l'Etat par des décisions du Conseil d'Etat, statuant au contentieux, des 12 juillet 1969 et 12 juin 1974, ayant donné lieu à diffusion et dont la teneur a été retranscrite les années suivantes dans plusieurs instructions de la direction générale des impôts. C'est seulement à compter du 1er janvier 1990, date d'entrée en vigueur de la loi du 22 juin 1989 modifiant et complétant certaines dispositions du livre deuxième du code rural, que les rémunérations perçues au titre des actes accomplis dans le cadre d'un mandat sanitaire ont été " assimilées ", pour l'application du code général des impôts et du code de la sécurité sociale, à des revenus tirés de l'exercice d'une profession libérale. Si M. B...a produit devant la cour un courrier de la direction départementale des services vétérinaires des Alpes-de-Haute-Provence du 13 avril 2005 évoquant le versement " d'honoraires ", ce courrier était postérieur à la liquidation de sa pension de retraite en septembre 2003. Il ne ressort ainsi pas des pièces soumises aux juges du fond qu'il n'aurait pas été en mesure, à la date de sa cessation d'activité, de disposer d'indications suffisantes quant au caractère salarial des rémunérations qu'il avait perçues et à l'obligation de cotisation qui en découlait pour l'Etat jusqu'en 1989. Par suite, en jugeant que M. B...ne pouvait, au moment où ses droits à la retraite ont été liquidés, être légitimement regardé, au sens de l'article 3 de la loi du 31 décembre 1968, comme ignorant l'existence de sa créance, la cour administrative d'appel, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit et a porté sur les faits et les pièces du dossier une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              5. Il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille du 13 juillet 2015. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B....<br/>
Copie en sera adressée au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
