<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043147948</ID>
<ANCIEN_ID>JG_L_2021_02_000000435352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/14/79/CETATEXT000043147948.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/02/2021, 435352</TITRE>
<DATE_DEC>2021-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435352.20210212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1610583 du 8 octobre 2019, enregistrée le 15 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Cergy-Pontoise a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 9 novembre 2016 au greffe de ce tribunal, de M. B... A....<br/>
<br/>
              Par cette requête et un mémoire en réplique, enregistré le 26 février 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 31 août 2016 de la ministre de l'environnement, de l'énergie et de la mer et du ministre de l'agriculture, de l'agroalimentaire et de la forêt lui infligeant la sanction de déplacement d'office ; <br/>
<br/>
              2°) d'enjoindre aux mêmes ministres de lui communiquer le procès-verbal du conseil de discipline, l'intégralité du rapport d'enquête administrative et les témoignages transmis à l'inspecteur en santé et sécurité au travail ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. M. B... A..., qui occupait au moment des faits en litige l'emploi de sous-directeur de la régulation européenne à la direction des affaires européennes et internationales du secrétariat général à l'administration centrale du ministère de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire, a fait l'objet d'une enquête administrative réalisée par une mission du conseil général de l'environnement et du développement durable à la suite de signalements en 2015. La mission d'enquête ayant conclu que les faits signalés n'étaient pas établis mais ayant relevé un ensemble de " fautes, manquements et défaillances managériales ", une procédure disciplinaire a été engagée à l'encontre de M. A..., du chef des seuls éléments retenus par la mission d'enquête. M. A... demande l'annulation pour excès de pouvoir de l'arrêté du 31 août 2016 par lequel la ministre de l'environnement, de l'énergie et de la mer et le ministre de l'agriculture, de l'agroalimentaire et de la forêt lui ont infligé la sanction de déplacement d'office.<br/>
<br/>
              2. Aux termes de l'article 19 de la loi du 13 juillet 1983 portant droits et obligation des fonctionnaires : " (...) Le fonctionnaire à l'encontre duquel une procédure disciplinaire est engagée a droit à la communication de l'intégralité de son dossier individuel et de tous les documents annexes et à l'assistance de défenseurs de son choix. L'administration doit informer le fonctionnaire de son droit à communication du dossier. Aucune sanction disciplinaire autre que celles classées dans le premier groupe par les dispositions statutaires relatives aux fonctions publiques de l'Etat, territoriale et hospitalière ne peut être prononcée sans consultation préalable d'un organisme siégeant en conseil de discipline dans lequel le personnel est représenté. / L'avis de cet organisme de même que la décision prononçant une sanction disciplinaire doivent être motivés ".<br/>
<br/>
              3. En premier lieu, aucun avis motivé de la commission administrative paritaire compétente siégeant en conseil de discipline le 21 juillet 2016 pour examiner le cas de M. A... ni même aucun procès-verbal de sa réunion n'ayant été produits au dossier, l'exigence de motivation de l'avis du conseil de discipline prévue par les dispositions citées au point précédent, qui constitue une garantie, ne peut être regardée comme ayant été respectée.<br/>
<br/>
              4. En second lieu, il résulte des mêmes dispositions que lorsqu'une enquête administrative a été diligentée sur le comportement d'un agent public, y compris lorsqu'elle a été confiée à des corps d'inspection, le rapport établi à l'issue de cette enquête, ainsi que, lorsqu'ils existent, les procès-verbaux des auditions des personnes entendues sur le comportement de l'agent faisant l'objet de l'enquête font partie des pièces dont ce dernier doit recevoir communication, sauf si la communication de ces procès-verbaux est de nature à porter gravement préjudice aux personnes qui ont témoigné.<br/>
<br/>
              5. Il ressort des pièces du dossier que si M. A... a pu consulter préalablement à la réunion du conseil de discipline le rapport de l'enquête administrative réalisée par la mission du conseil général de l'environnement et du développement durable, il n'a toutefois pas eu communication, malgré la demande qu'il a faite en ce sens, des procès-verbaux des auditions auxquelles la mission a procédé au cours de l'enquête, sans qu'il soit établi ni même allégué que cette communication aurait été de nature à porter gravement préjudice aux personnes auditionnées. Dès lors, M. A... n'ayant pas eu communication de l'ensemble des pièces qu'il était en droit d'obtenir, la sanction litigieuse a été prononcée au terme d'une procédure irrégulière.<br/>
<br/>
              6. Par suite, et sans qu'il soit besoin d'examiner les autres moyens de sa requête, M. A... est fondé à demander l'annulation de l'arrêté attaqué. Cette annulation n'implique en revanche pas que l'administration communique au requérant les pièces qu'il demande, de sorte qu'il y a lieu de rejeter ses conclusions aux fins d'injonction.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 31 août 2016 de la ministre de l'environnement, de l'énergie et de la mer et du ministre de l'agriculture, de l'agroalimentaire et de la forêt est annulé.<br/>
Article 2 : L'Etat versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de M. A... est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., à la ministre de la transition écologique et solidaire et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-09-05-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. PROCÉDURE. CONSEIL DE DISCIPLINE. - MOTIVATION DE L'AVIS - 1) GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE - 2) PREUVE - PRODUCTION DE L'AVIS MOTIVÉ - PRODUCTION DU PROCÈS-VERBAL DE LA RÉUNION COMPORTANT DES MENTIONS SUFFISANTES (SOL. IMP.) [RJ2] - 3) CONSÉQUENCES [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-04 PROCÉDURE. INSTRUCTION. PREUVE. - LÉGALITÉ DE LA SANCTION DISCIPLINAIRE INFLIGÉE À UN FONCTIONNAIRE - MOTIVATION DE L'AVIS DE LA CAP SIÉGEANT EN CONSEIL DE DISCIPLINE [RJ1] - 1) PREUVE - PRODUCTION DE L'AVIS MOTIVÉ - PRODUCTION DU PROCÈS-VERBAL DE LA RÉUNION COMPORTANT DES MENTIONS SUFFISANTES (SOL. IMP.) [RJ2] - 2) CONSÉQUENCES [RJ3].
</SCT>
<ANA ID="9A"> 36-09-05-01 1) L'exigence de motivation, prévue par l'article 19 de la loi n° 83-634 du 13 juillet 1983, de l'avis de la commission administrative paritaire (CAP) compétente siégeant en conseil de discipline constitue une garantie.,,,2) Cette motivation peut être attestée par la production, sinon de l'avis motivé lui-même, du moins du procès-verbal de la réunion de la CAP comportant des mentions suffisantes.,,,3) Dans le cas où aucun avis motivé de la CAP siégeant en conseil de discipline ni même aucun procès-verbal de sa réunion ne sont produits devant le juge, l'exigence de motivation de l'avis du conseil de discipline ne peut être regardée comme ayant été respectée.</ANA>
<ANA ID="9B"> 54-04-04 L'exigence de motivation, prévue par l'article 19 de la loi n° 83-634 du 13 juillet 1983, de l'avis de la commission administrative paritaire (CAP) compétente siégeant en conseil de discipline constitue une garantie.,,,1) Cette motivation peut être attestée par la production, sinon de l'avis motivé lui-même, du moins du procès-verbal de la réunion de la CAP comportant des mentions suffisantes.,,,2) Dans le cas où aucun avis motivé de la CAP siégeant en conseil de discipline ni même aucun procès-verbal de sa réunion ne sont produits devant le juge, l'exigence de motivation de l'avis du conseil de discipline ne peut être regardée comme ayant été respectée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 21 juillet 1972, Sieur,, n° 79559, p. 587. Rappr., CE, Assemblée, 23 décembre 2011, M. Danthony et autres, n° 335033, p. 649.,,[RJ2] Cf. CE, 16 mai 1975, Secrétaire d'Etat aux Transports c/ Sieur,, n° 96973, T. p. 830-1114.,,[RJ3] Rappr., CE, Assemblée, 28 mai 1954, Barel et autres, n°s 28238 28493 28524 30237 30256, p. 308.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
