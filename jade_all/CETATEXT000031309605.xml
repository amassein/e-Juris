<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031309605</ID>
<ANCIEN_ID>JG_L_2015_10_000000374008</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/30/96/CETATEXT000031309605.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 09/10/2015, 374008, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374008</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374008.20151009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Eco Delta a demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir six arrêtés du 2 décembre 2009 du préfet d'Eure-et-Loir refusant de lui délivrer six permis de construire huit éoliennes sur le territoire des communes de La Bourdinière-Saint-Loup, d'Ermenonville-la-Grande et de Luplanté, ainsi que la décision du 8 avril 2010 par laquelle le préfet a rejeté son recours gracieux. Par un jugement nos 1001965, 1001966, 1001967, 1001968, 1001969 et 1001970 du 6 mars 2012, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 12NT01207 du 11 octobre 2013, la cour administrative d'appel de Nantes a, sur appel de la société Eco Delta, annulé ce jugement, les arrêtés préfectoraux du 2 décembre 2009, ainsi que la décision du 8 avril 2010.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 décembre 2013 et 14 mars 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'égalité des territoires et du logement demande au Conseil d'Etat d'annuler cet arrêt de la cour administrative d'appel de Nantes.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Eco Delta ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par six arrêtés du 2 décembre 2009, le préfet d'Eure-et-Loir a refusé de délivrer les permis de construire sollicités par la société Eco Delta en vue de l'implantation de huit éoliennes sur le territoire des communes de La Bourdinière-Saint-Loup, Ermenonville-la-Grande et Luplanté ; que par un jugement du 6 mars 2012, le tribunal administratif d'Orléans a rejeté la demande de la société Eco Delta tendant à l'annulation de ces arrêtés et de la décision du 8 avril 2010 rejetant son recours gracieux ; que le ministre du logement, de l'égalité des territoires et de la ruralité se pourvoit en cassation contre l'arrêt du 11 octobre 2013 par lequel la cour administrative d'appel de Nantes a annulé ce jugement ainsi que les six arrêtés préfectoraux du 2 décembre 2009 et la décision du 8 avril 2010 ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que le pourvoi sommaire et le mémoire complémentaire ont été enregistrés respectivement le 16 décembre 2013 et le 14 mars 2014 au secrétariat du contentieux du Conseil d'Etat ; que, contrairement à ce que soutient la société Eco Delta, le délai de trois mois prévu à l'article R. 611-22 du code de justice administrative pour la présentation du mémoire complémentaire a ainsi été respecté ; que, par suite, contrairement à ce que soutient la société Eco Delta, le ministre ne peut être regardé comme s'étant désisté d'office de son pourvoi ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 111-21 du code de l'urbanisme : " Le permis de construire peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales " ; qu'il résulte de ces dispositions que, si les constructions projetées portent atteinte aux paysages naturels avoisinants, l'autorité administrative compétente peut refuser de délivrer le permis de construire sollicité ou l'assortir de prescriptions spéciales ; que, pour rechercher l'existence d'une atteinte à un paysage naturel de nature à fonder le refus de permis de construire ou les prescriptions spéciales accompagnant la délivrance de ce permis, il lui appartient d'apprécier, dans un premier temps, la qualité du site naturel sur lequel la construction est projetée et d'évaluer, dans un second temps, l'impact que cette construction, compte tenu de sa nature et de ses effets, pourrait avoir sur le site ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond et des énonciations de l'arrêt attaqué, que la cathédrale de Chartres, monument historique de renommée internationale, est inscrite sur la liste du patrimoine mondial de l'UNESCO ; que le projet litigieux prévoit la construction de huit éoliennes d'une hauteur de cent cinquante mètres, pales comprises, dont les plus proches ne sont distantes que de treize kilomètres de la cathédrale de Chartres ; que ces éoliennes sont situées dans la " zone de sensibilité forte du point de vue des enjeux de préservation de la cathédrale de Chartres " identifiée par le schéma éolien départemental d'Eure-et-Loir, qui comprend un rayon de vingt-trois kilomètres autour de la cathédrale et qui préconise de n'autoriser un projet éolien dans cette zone que s'il est " prouvé qu'il n'est nulle part en situation de co-visibilité avec la cathédrale de Chartres " ; qu'il ressort tant des pièces du dossier soumis aux juges du fond que des énonciations de l'arrêt attaqué que l'un des bouquets d'éoliennes serait visible depuis les perspectives donnant sur la cathédrale à partir d'au moins trois lieux des environs, soit, les routes départementales 12-4 et 127-3 et le nord de la commune de Saint-Loup ; que, dans ces conditions, en estimant, pour annuler le jugement du tribunal administratif d'Orléans, que le projet litigieux ne portait pas atteinte au caractère ou à l'intérêt des lieux avoisinants ou à la conservation des perspectives offertes sur la cathédrale de Chartres, la cour administrative d'appel de Nantes a entaché son arrêt de dénaturation ; que, par suite, le ministre de l'égalité des territoires et du logement est fondé à demander l'annulation de l'arrêt qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 11 octobre 2013 de la cour administrative d'appel de Nantes est annulé. <br/>
Article 2 : Les conclusions présentées par la société Eco Delta au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 4 : La présente décision sera notifiée à la ministre du logement, de l'égalité des territoires et de la ruralité et à la société Eco Delta.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
