<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032883007</ID>
<ANCIEN_ID>JG_L_2016_07_000000389212</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/88/30/CETATEXT000032883007.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/07/2016, 389212, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389212</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; RICARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389212.20160711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par la voie du déféré, le préfet des Yvelines a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir l'arrêté du 2 mai 2011 par lequel le maire de Gambais a délivré à M. A...B...un permis de construire autorisant la réalisation d'une maison individuelle sur un terrain, correspondant aux parcelles cadastrées AI 455 et AI 460, situé rue du Verger à Gambais (78950). Par un jugement n° 1106246 du 16 mai 2013, le tribunal administratif a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 13VE02504 du 29 janvier 2015, la cour administrative d'appel de Versailles a, sur appel du préfet des Yvelines, annulé ce jugement et annulé l'arrêté du maire de Gambais du 2 mai 2011. <br/>
<br/>
              1° Sous le n° 389212, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 avril et 3 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du préfet des Yvelines ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 389326, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 avril et 9 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Gambais demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Versailles du 29 janvier 2015 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du préfet des Yvelines ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M.B..., et à Me Ricard, avocat de la commune de Gambais ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les pourvois de M. B...et de la commune de Gambais présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de Gambais a délivré le 2 mai 2011 à M. B...un permis de construire une maison individuelle sur un terrain situé rue du Verger ; que, par un jugement du 16 mai 2013, le tribunal administratif de Versailles a rejeté le déféré contre cet acte, dont le préfet des Yvelines l'avait saisi ; que, sur appel du préfet, la cour administrative d'appel de Versailles a annulé le jugement du tribunal administratif de Versailles ainsi que l'arrêté du maire de Gambais accordant le permis de construire litigieux au motif que les dispositions du II de l'article UH 3 du règlement du plan local d'urbanisme de la commune de Gambais relatives à l'aménagement des voies se terminant en impasse avaient été méconnues ; <br/>
<br/>
              3.	Considérant qu'aux termes de l'article UH 3 du règlement du plan local d'urbanisme de la commune de Gambais, approuvé le 22 février 2008 : " UH 3 - Accès et voirie / I. Accès / Tout terrain enclavé est inconstructible, à moins que son propriétaire ne produise une servitude de passage suffisante, instituée par acte authentique ou par voie judiciaire, en application de l'article 682 du code civil. / Lorsque le terrain est riverain de deux ou plusieurs voies, l'accès sur l'une de ces voies, qui présenterait une gêne ou un risque pour la circulation, peut-être interdit. / Les accès doivent permettre de satisfaire aux règles minimales de desserte : défense contre l'incendie, protection civile, brancardage, etc. Ils doivent être adaptés à l'opération et être aménagés de façon à apporter la moindre gêne à la circulation publique et garantir un bon état de visibilité. / II. Voirie : / Les voies doivent avoir des caractéristiques adaptées à l'approche du matériel de lutte contre l'incendie et de ramassage des déchets ménagers. / Les dimensions, formes, et caractéristiques techniques des voies privées doivent être adaptées aux usages qu'elles supportent ou aux opérations qu'elles doivent desservir et avoir au moins 8 m de largeur en tout point quelle que soit leur longueur. / Les voies de plus de 60 m de longueur se terminant en impasse doivent être aménagées de telle sorte que les véhicules puissent faire demi-tour " ;<br/>
<br/>
              4.	Considérant que les dispositions du II de l'article précité sont relatives à l'aménagement des voies nouvelles et n'ont pas pour objet, à la différence de celles qui figurent au I, de définir les conditions de constructibilité des terrains situés dans la zone concernée ; que, par suite, elles ne font pas obstacle à la délivrance d'un permis de construire en vue de l'édification d'une maison desservie par des voies construites avant leur adoption ; que lorsqu'il est saisi d'un tel projet, il appartient au maire d'apprécier notamment si les conditions d'accès prévues au dernier alinéa du I de l'article UH 3 sont remplies ; qu'en se fondant, pour annuler le jugement du tribunal administratif de Versailles du 16 mai 2013 et l'arrêté du 2 mai 2011 sur la circonstance que le permis accordé à M. B...ne respectait pas les prescriptions des dispositions du II de l'article UH 3, alors que le projet ne comportait pas l'aménagement d'une voie nouvelle, la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens des pourvois, son arrêt doit être annulé ; <br/>
<br/>
              5.	Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ; <br/>
<br/>
              6.	Considérant, en premier lieu, que la référence erronée faite par les premiers juges au certificat d'urbanisme délivré à M.B..., lequel avait également été déféré au tribunal par le préfet des Yvelines, n'a pas été de nature à affecter la régularité du jugement ; <br/>
<br/>
              7.	Considérant, en deuxième lieu, que la circonstance que le tribunal a relevé que le préfet des Yvelines contestait la largeur de l'impasse de la rue du Verger alors qu'il avait mis en cause son aménagement a en tout état de cause été sans incidence sur la régularité du jugement dès lors que le tribunal a jugé que le moyen tiré de la méconnaissance des dispositions du plan local d'urbanisme relatives aux dimensions des voies était inopérant et l'a écarté pour ce motif ; <br/>
<br/>
              8.	Considérant, en troisième lieu, que, ainsi qu'il a été dit au point 4, les dispositions du II de l'article UH 3 du règlement du plan local d'urbanisme de la commune de Gambais ne peuvent utilement être invoquées à l'encontre de l'arrêté du 2 mai 2011 accordant un permis de construire à M.B..., lequel ne porte pas sur un projet ayant pour objet la construction de voies nouvelles ; <br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que le préfet des Yvelines n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté sa demande ;  <br/>
<br/>
              10.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 500 à verser à la commune de Gambais pour la procédure engagée devant la cour administrative d'appel de Versailles et devant le Conseil d'Etat et la même somme à verser à M.B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 29 janvier 2015 est annulé. <br/>
<br/>
Article 2 : La requête présentée par le préfet des Yvelines devant la cour administrative d'appel de Versailles est rejetée.<br/>
<br/>
Article 3 : L'Etat versera à la commune de Gambais, d'une part, et à M.B..., d'autre part, une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., à la commune de Gambais et à la ministre du logement et de l'habitat durable. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
