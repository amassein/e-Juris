<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030249881</ID>
<ANCIEN_ID>JG_L_2015_02_000000382164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/24/98/CETATEXT000030249881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 16/02/2015, 382164, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382164.20150216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B... E..., d'une part, M. A... F..., d'autre part, ont saisi le tribunal administratif de Besançon de deux protestations tendant à l'annulation des opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Lyoffans (Haute-Saône). Par un jugement n° 1400448, 1400467 du 10 juin 2014, le tribunal administratif a rejeté ces protestations.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 382164, par une requête et un mémoire en réplique, enregistrés les 3 juillet et 15 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... E...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1400448, 1400467 du 10 juin 2014 du tribunal administratif de Besançon ;<br/>
<br/>
              2°) d'annuler les opérations électorales contestées.<br/>
<br/>
<br/>
              2° Sous le n° 382388, par une requête et un mémoire en réplique, enregistrés les 8 juillet et 16 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A... F...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1400448, 1400467 du 10 juin 2014 du tribunal administratif de Besançon ;<br/>
<br/>
              2°) d'annuler les opérations électorales contestées.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces des dossiers ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du premier tour des élections municipales, qui se sont déroulées le 23 mars 2014 dans la commune de Lyoffans (Haute-Saône), les onze candidats de la liste " Agir ensemble pour Lyoffans " conduite par M. D... C..., ont obtenu la majorité absolue et été proclamés élus. Les onze candidats de la liste " Unis pour Lyoffans ", conduite par M. E..., ont chacun obtenu entre 100 et 112 voix. M. F..., candidat indépendant, a obtenu 33 voix. M. E... et M. F... forment appel contre le même jugement du 10 juin 2014 par lequel le tribunal administratif de Besançon a rejeté leur protestation dirigée contre ces élections. Il y a lieu de joindre leurs requêtes pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 48-2 du code électoral : " Il est interdit à tout candidat de porter à la connaissance du public un élément nouveau de polémique électorale à un moment tel que ses adversaires n'aient pas la possibilité d'y répondre utilement avant la fin de la campagne électorale ". Aux termes du premier alinéa de l'article L. 49 du même code : " A partir de la veille du scrutin à zéro heure, il est interdit de distribuer ou faire distribuer des bulletins, circulaires et autres documents ".<br/>
<br/>
              3. Il est constant que M. C... a procédé le samedi 22 mars 2014, soit la veille du scrutin, à la distribution d'un tract dans les boîtes aux lettres de vingt-quatre habitations de la commune, en méconnaissance des dispositions de l'article L. 49 du code électoral. Toutefois, il résulte de l'instruction que ce tract ne comportait aucun élément nouveau de propagande dès lors que M. F...avait indiqué lui-même dans sa déclaration de candidature que cette dernière avait été déposée " en complément d'une autre liste d'opposition ". Il n'excédait pas non plus les limites habituelles de la polémique électorale dès lors qu'il constituait une réponse aux attaques personnelles dont M. C... avait été l'objet de la part de M. F... dans sa déclaration de candidature présentée quatre jours avant le scrutin. Dans ces circonstances, la distribution de ce tract n'a pas constitué une manoeuvre susceptible d'altérer la sincérité du scrutin.<br/>
<br/>
              4. Aux termes du second alinéa de l'article L. 52-1 du même code : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. (...) " Il résulte de l'instruction que le contenu du tract distribué avant le début de la campagne électorale n'a pas été de nature à porter atteinte à la sincérité du scrutin.<br/>
<br/>
              5. Il résulte de ce qui précède que M. E... et M. F... ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Besançon a rejeté leurs protestations dirigées contre les opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Lyoffans. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de M. E... et M. F... sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à M. B... E..., M. A... F..., M. D... C...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
