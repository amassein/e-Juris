<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033074861</ID>
<ANCIEN_ID>JG_L_2016_08_000000402457</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/07/48/CETATEXT000033074861.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/08/2016, 402457, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-08-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402457</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:402457.20160819</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution des arrêtés des 19 et 21 juillet 21 juillet 2016 par lesquels le ministre de l'intérieur a prononcé son expulsion du territoire français et a décidé qu'il serait éloigné à destination de l'Algérie et, d'autre part, d'ordonner toute mesure de nature à faire cesser l'illégalité de son expulsion.  Par une ordonnance n° 1612522 du 12 août 2016, le juge des référés du tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par une requête enregistrée le 16 août 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant en appel sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que son éloignement du territoire français peut intervenir à tout moment, et qu'il est placé sous le régime de la rétention administrative et ainsi privé de la liberté d'aller et venir ;<br/>
              - il est porté une atteinte grave et manifestement illégale à sa liberté d'aller et venir et à son droit au respect de sa vie privée et familiale ; <br/>
               - les arrêtés contestés méconnaissent l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au motif qu'un éloignement à destination de l'Algérie l'exposerait à des traitements prohibés par ces stipulations. <br/>
<br/>
              Par un mémoire en défense, enregistré le 18 août 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient qu'aucun des moyens soulevés par M. B...à l'appui de son appel n'est fondé.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 19 août 2016 à 11  heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, représentant M.B... ; <br/>
<br/>
              - le représentant de M.B... ;<br/>
              - la représentante du ministre de l'intérieur ; <br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 août 2016, présentée par le ministre de l'intérieur ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; <br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que M. B..., ressortissant algérien né le 21 juillet 1979 à Bejaïa (Algérie) est entré sur le territoire français en 1982 pour y rejoindre son père dans le cadre d'une procédure de regroupement familial, et a résidé habituellement en France depuis lors ; qu'il est titulaire depuis sa majorité d'une carte de résident renouvelée en dernier lieu le 21 juillet 2007 et valable jusqu'au 20 juillet 2017 ; qu'ayant fait l'objet de diverses condamnations pénales, il est incarcéré depuis le 23 janvier 2010 ; que, par l'effet de remises de peines dont il avait bénéficié, les peines dont il avait fait l'objet ont été accomplies à la date du 29 juillet 2016 ; que, toutefois, par un arrêté du 19 juillet 2016, le ministre de l'intérieur a décidé d'expulser M. B...du territoire français, cette expulsion revêtant un caractère d'urgence absolue au sens de l'article L. 522-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, par un arrêté du 21 juillet 2016, le ministre de l'intérieur a décidé que M. B...serait éloigné à destination de l'Algérie, pays dont il a la nationalité ; que, le 29 juillet 2016, le préfet de Seine-et-Marne a placé l'intéressé en rétention administrative en application de l'article L. 551-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, cette rétention ayant été prolongée pour une durée de vingt jours par une ordonnance du juge des libertés et de la détention du 2 août 2016 ; que M. B...a saisi le juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à la suspension de l'exécution des arrêtés des 19 et 21 juillet 2016 ; que M. B...relève appel de l'ordonnance du 12 août 2016 par laquelle le juge des référés du tribunal administratif a rejeté sa demande ; <br/>
              3. Considérant qu'aux termes de l'article L. 521-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions des articles L. 521-2, L. 521-3 et L. 521-4, l'expulsion peut être prononcée si la présence en France d'un étranger constitue une menace grave pour l'ordre public " ; qu'aux termes de l'article L. 521-3 du même code : " Ne peuvent faire l'objet d'une mesure d'expulsion qu'en cas de comportements de nature à porter atteinte aux intérêts fondamentaux de l'Etat, ou liés à des activités à caractère terroriste, ou constituant des actes de provocation explicite et délibérée à la discrimination, à la haine ou à la violence contre une personne déterminée ou un groupe de personnes : 1° L'étranger qui justifie par tous moyens résider habituellement en France depuis qu'il a atteint au plus l'âge de treize ans (...) " ; qu'aux termes de l'article L. 522-1 du même code : " I. - Sauf en cas d'urgence absolue, l'expulsion ne peut être prononcée que dans les conditions suivantes : / 1° L'étranger doit être préalablement avisé dans des conditions fixées par décret en Conseil d'Etat ; / 2° L'étranger est convoqué pour être entendu par une commission qui se réunit à la demande de l'autorité administrative et qui est composée : / a) Du président du tribunal de grande instance du chef-lieu du département, ou d'un juge délégué par lui, président ; / b) D'un magistrat désigné par l'assemblée générale du tribunal de grande instance du chef-lieu du département ; / c) D'un conseiller de tribunal administratif " ;<br/>
<br/>
              Sur le moyen tiré de la méconnaissance du code de l'entrée et du séjour des étrangers et du droit d'asile :<br/>
               4. Considérant que, pour prendre la mesure d'expulsion litigieuse, le ministre de l'intérieur s'est fondé, d'une part, sur la circonstance que M. B...aurait adopté pendant sa détention un comportement tel que ceux qui sont mentionnés à l'article L. 521-3 du code de l'entrée et du séjour des étrangers et du droit d'asile et, d'autre part, sur la gravité de la menace pour l'ordre public qui résulterait d'un maintien de la présence de l'intéressé sur le territoire français, constitutive d'une urgence absolue de nature à priver l'intéressé de la garantie résultant de l'examen de son cas par la commission instituée par l'article L. 522-1 du même code ;  <br/>
<br/>
               5. Considérant qu'il résulte de l'instruction que M. B... a été condamné à diverses reprises pour des actes de délinquance, et a été condamné en dernier lieu à une peine de cinq ans d'emprisonnement pour des faits de violence aggravée ; qu'il résulte également de l'instruction, et notamment d'une " note blanche " versée au dossier et soumise au contradictoire, qu'il a à diverses reprises, et alors même que chacun de ces faits n'a pas nécessairement donné lieu à des sanctions disciplinaires, proféré contre les fonctionnaires de l'administration pénitentiaire des menaces physiques, y compris des menaces de mort ; qu'il n'est pas contesté qu'il a entretenu pendant son incarcération des relations avec des détenus notoirement radicalisés et prosélytes, l'un d'entre eux ayant été expulsé à la fin de sa détention après avoir été déchu de sa nationalité française ; qu'il n'est pas davantage sérieusement contesté que le 14 novembre 2015, soit le lendemain des attentats perpétrés à Paris et en banlieue parisienne à l'instigation de l'organisation dite " Etat islamique " et ayant causé plusieurs centaines de victimes, tuées ou blessées, M. B...a exprimé son approbation et sa satisfaction ; qu'une fouille de sa cellule conduite trois semaines plus tard a conduit l'administration à confisquer un ordinateur portable acheté en février 2015 ; que l'analyse de celui-ci a fait apparaître qu'alors même qu'il est interdit aux détenus de faire usage d'Internet, cet ordinateur avait servi entre février et décembre 2015 à des connexions ou tentatives de connexion sur des sites promouvant les activités de l'organisation dite " Etat islamique " ou appelant à la haine ou à l'usage de la violence contre des personnes qui, à raison de leurs responsabilités politiques ou de leur appartenance religieuse, sont regardées par les promoteurs de l'islamisme radical comme faisant obstacle à la réalisation des buts assignés par ceux-ci à leur idéologie ; que, si M. B...soutient que l'analyse de son ordinateur n'apporte techniquement pas la preuve de ce qu'il aurait été personnellement l'auteur de ces connexions ou tentatives de connexion, il ne produit aucun élément ni même aucun commencement d'explication de nature à établir que son ordinateur personnel aurait été utilisé à cette fin par un de ses codétenus, frauduleusement ou contre sa volonté ;    <br/>
<br/>
               6. Considérant qu'eu égard à ce qui a été dit au point 5, et notamment de la propension de l'intéressé à manifester un comportement violent, avant comme pendant son incarcération, et des signes objectifs de son ancrage délibéré dans une idéologie, qui encourage le passage à l'acte de ses adeptes notamment au travers de la consultation des sites Internet relayant les mots d'ordre de l'organisation mentionnée ci-dessus, et est directement à l'origine depuis le mois de janvier 2015 d'attentats terroristes ayant causé sur le territoire national des centaines de victimes, il ne résulte pas de l'instruction qu'en prescrivant l'expulsion en urgence absolue de M.B..., le ministre de l'intérieur aurait porté une atteinte manifestement illégale à la liberté d'aller et venir de l'intéressé ;<br/>
<br/>
<br/>
               Sur le moyen tiré de la méconnaissance des droits garantis par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              7. Considérant que M. B...soutient que la procédure mise en oeuvre à son encontre, en tant notamment que l'article L. 523-1 du code de l'entrée et du séjour des étrangers et du droit d'asile autorise l'exécution d'office par l'administration d'un arrêté prescrivant l'expulsion d'un étranger, porterait atteinte au droit au recours garanti par les stipulations de cet article, faute de donner un effet suspensif de plein droit au recours formé par l'étranger contre cette mesure devant le juge de l'excès de pouvoir ; que, toutefois et ainsi qu'en témoigne la présente procédure engagée par l'intéressé, la faculté ainsi reconnue à l'administration ne fait pas obstacle à la saisine du juge des référés sur le fondement de l'article L. 521-2 du code de justice administrative ; qu'alors même que M. B...n'a pas cru devoir faire en l'espèce usage de cette voie procédurale, cette faculté ne fait pas davantage obstacle à l'exercice d'une demande de suspension formée en application de l'article L. 521-1 du même code, laquelle si elle était accueillie suspendrait jusqu'au jugement au fond les effets de la mesure d'expulsion litigieuse ; que, dès lors que M. B...ne peut être regardé comme étant privé de tout recours juridictionnel effectif, le moyen tiré de ce que la mesure prise à son encontre serait manifestement illégale au motif que la procédure suivie l'aurait privé d'un droit garanti par l'article 13 de cette convention doit être écarté ;<br/>
<br/>
<br/>
               Sur le moyen tiré de la méconnaissance des droits garantis par les articles 8 et 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
               8. Considérant que, si M. B...se prévaut de ce qu'il est père de trois enfants mineurs de nationalité française respectivement nés en 2005, 2008 et 2009 et résidant en France, il résulte de l'instruction qu'il n'a reconnu ces enfants qu'en 2013 et n'exerce pas l'autorité parentale sur ceux-ci ; qu'il n'allègue pas avoir, avant son incarcération, contribué à l'entretien et à l'éducation de ces enfants ; que, par suite, la mesure d'expulsion litigieuse n'a pas porté une atteinte manifestement illégale aux droits que l'intéressé tient des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
               9. Considérant que M. B...fait valoir qu'en cas de retour dans le pays dont il est ressortissant, il pourrait légitimement craindre d'être soumis à des traitements prohibés par l'article 3 de la même convention ; que, s'il est vrai qu'en cas d'éloignement à destination de l'Algérie les autorités de ce pays seront nécessairement informées des motifs ayant justifié son expulsion, M. B...se borne à produire des extraits de rapports d'organisations non-gouvernementales comportant des considérations générales relatives à la situation des droits de l'homme prévalant dans ce pays ; qu'en revanche M.B..., qui d'ailleurs n'allègue pas avoir eu antérieurement en Algérie des activités de nature à attirer l'attention des autorités, n'établit pas qu'il serait susceptible d'être personnellement exposé à des traitements attentatoires aux droits garantis par ces stipulations en cas de retour dans son pays ; que, par suite, la mesure prescrivant son éloignement à destination de l'Algérie n'a pas porté une atteinte manifestement illégale aux droits que l'intéressé tient des stipulations de l'article 3 de la même convention ;<br/>
<br/>
               10. Considérant qu'il résulte de tout ce qui précède que M. B... n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative ; que, par suite, sa requête doit être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du même code, l'Etat n'étant pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
