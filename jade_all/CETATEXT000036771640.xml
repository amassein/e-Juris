<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036771640</ID>
<ANCIEN_ID>JG_L_2018_04_000000406291</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/77/16/CETATEXT000036771640.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 04/04/2018, 406291, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406291</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406291.20180404</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nîmes de condamner le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard à l'indemniser des préjudices ayant résulté pour lui de la décision du 8 mars 2012 par laquelle ce conseil a refusé de procéder à son inscription au tableau de l'ordre. Par un jugement n° 1303158 du 26 mars 2015, le tribunal administratif a condamné le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard à lui verser la somme de 2 000 euros.<br/>
<br/>
              Par un arrêt n° 15MA01927 du 25 octobre 2016, la cour administrative d'appel de Marseille, statuant sur l'appel de M. B...et sur l'appel incident du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard, a annulé ce jugement et rejeté les conclusions de M.B....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 26 décembre 2016, 27 mars 2017 et 7 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ; <br/>
<br/>
              - l'arrêté du 5 septembre 1989 relatif aux études préparatoires et au diplôme d'Etat de masseur-kinésithérapeute ; <br/>
<br/>
              - l'arrêté du 31 janvier 1991 relatif aux dispenses accordées à certains candidats en vue de la préparation au diplôme d'Etat de masseur-kinésithérapeute ;<br/>
<br/>
              - l'arrêté du 6 août 2004 relatif aux dispenses susceptibles d'être accordées aux candidats titulaires d'un diplôme extracommunautaire de masseur-kinésithérapeute sollicitant l'exercice de la profession en France en vue de la préparation du diplôme d'Etat de masseur-kinésithérapeute ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. B...et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B...a demandé son inscription au tableau de l'ordre départemental des masseurs-kinésithérapeutes du Gard ; que, par une décision du 8 mars 2012, le conseil départemental de l'ordre a refusé son inscription, au motif notamment qu'il avait dissimulé sa double nationalité franco-marocaine pour bénéficier des facilités d'accès à la formation de masseur-kinésithérapeute ouvertes aux ressortissants non-communautaires par l'arrêté du 31 janvier 1991 relatif aux dispenses accordées à certains candidats en vue de la préparation au diplôme d'Etat de masseur-kinésithérapeute et que cette circonstance révélait un défaut de moralité professionnelle ; que, par une décision du 15 mai 2012, le conseil régional de l'ordre des masseurs-kinésithérapeutes de Languedoc-Limousin a annulé la décision du 8 mars 2012 du conseil départemental de l'ordre et procédé à l'inscription de M.B... ; que celui-ci a demandé réparation des conséquences dommageables du refus d'inscription qui lui avait été opposé ; que, par un jugement du 26 mars 2015, le tribunal administratif de Nîmes a retenu une faute du conseil départemental de l'ordre et a condamné celui-ci à verser à M. B...la somme de 2 000 euros ; que M. B...se pourvoit en cassation contre l'arrêt du 25 octobre 2016 par lequel la cour administrative d'appel de Marseille, statuant sur l'appel du conseil départemental de l'ordre, a annulé ce jugement et rejeté ses conclusions indemnitaires ; <br/>
<br/>
              2. Considérant, d'une part, qu'à la date à laquelle M. B...s'est inscrit à la formation dispensée par l'institut de formation en masso-kinésithérapie de Montpellier, les dispositions du titre IV de l'arrêté du 31 janvier 1991 relatif aux dispenses accordées à certains candidats en vue de la préparation au diplôme d'Etat de masseur-kinésithérapeute prévoyaient que les ressortissants de pays autres que ceux de la Communauté européenne titulaires d'un diplôme de fin d'études secondaires de niveau équivalent au baccalauréat français pouvaient suivre la formation de masseur-kinésithérapeute dans les mêmes conditions que les étudiants français sans avoir à se présenter au concours d'entrée en formation, mais qu'en cas de succès à l'épreuve finale de mise en situation professionnelle prévue à l'article 22 de l'arrêté du 5 septembre 1989, le préfet de région leur délivrait une attestation d'études ne permettant pas l'exercice de la profession de masseur-kinésithérapeute sur le territoire français ; que, s'ils souhaitaient exercer leur profession en France, ils devaient obligatoirement passer le concours d'admission à l'école où ils avaient suivi leur formation pour obtenir l'échange de cette attestation d'études contre le diplôme d'Etat ; <br/>
<br/>
              3. Considérant, d'autre part, qu'au cours de la scolarité de M.B..., les dispositions de l'arrêté du 31 janvier 1991 mentionnées ci-dessus ont été abrogées par l'arrêté du 6 août 2004 relatif aux dispenses susceptibles d'être accordées aux candidats titulaires d'un diplôme extracommunautaire de masseur-kinésithérapeute sollicitant l'exercice de la profession en France en vue de la préparation du diplôme d'Etat de masseur-kinésithérapeute, dont l'article 4 institue un concours d'entrée dans les instituts de formation réservé aux titulaires d'un diplôme étranger de masseur-kinésithérapeute ; que l'article 11 de cet arrêté dispose que " les personnes titulaires de l'attestation d'études délivrée en application de l'article 27 de l'arrêté du 31 janvier 1991 susvisé depuis moins de cinq ans à compter de la date de publication du présent arrêté, qui ont suivi et validé en France au moins une année de scolarité dans un institut de formation en masso-kinésithérapie et qui souhaiteraient obtenir le diplôme d'Etat de masseur-kinésithérapeute, disposent d'un délai de deux ans à compter de la mise en application du présent arrêté, prévue à l'article 10, pour pouvoir se présenter aux épreuves de sélection susmentionnées à l'article 4. En cas de réussite à ces épreuves, l'attestation d'études pourra être échangée contre le diplôme d'Etat précité " ; que, sur le fondement de ces dispositions, M. B..., après réussite aux épreuves de sélection, a échangé son attestation d'études contre le diplôme d'Etat de masseur-kinésithérapie. <br/>
<br/>
              4. Considérant qu'il résulte des dispositions citées au point 2 que lorsque M. B... s'est inscrit à la formation de masseur-kinésithérapeute en tant qu'étudiant non communautaire en 2003, il ne pouvait obtenir, à l'issue de cette formation, qu'une attestation d'études ne lui permettant pas d'exercer cette profession en France ; que c'est ultérieurement  l'arrêté du 6 août 2004 mentionné au point 3 qui a ouvert aux titulaires de cette attestation la possibilité de se présenter à des épreuves de sélection leur permettant d'obtenir le diplôme de masseur-kinésithérapeute afin d'exercer en France; qu'en retenant que M B...avait eu un comportement frauduleux en demandant illégalement à suivre une formation réservée aux ressortissants non communautaires sans rechercher si, à la date de son inscription, il avait eu l'intention de contourner les règles d'accès à un exercice professionnel en France, la cour a commis une erreur de droit ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de condamner le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard à verser à M. B...la somme de 3 000 euros au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 25 octobre 2016 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
Article 3 : Le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au conseil départemental de l'ordre des masseurs-kinésithérapeutes du Gard.<br/>
Copie en sera transmise à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
