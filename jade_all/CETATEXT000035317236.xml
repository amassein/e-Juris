<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317236</ID>
<ANCIEN_ID>JG_L_2017_07_000000397419</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/72/CETATEXT000035317236.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 28/07/2017, 397419</TITRE>
<DATE_DEC>2017-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397419</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397419.20170728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Fondation Jérôme Lejeune a demandé au tribunal administratif de Paris d'annuler la décision du 15 juillet 2012 par laquelle l'Agence de la biomédecine a autorisé, pour une durée de cinq ans, le centre hospitalier universitaire de Montpellier à mettre en oeuvre un protocole de recherche sur l'embryon ayant pour finalité l'étude de l'identification des biomarqueurs impliqués dans la régulation des embryons préimplantatoires humains et l'analyse de l'effet de l'âge maternel sur ces processus de régulation. Par un jugement n° 1220509/6-3 du 11 juin 2015, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 15PA03405 du 31 décembre 2015, la cour administrative d'appel de Paris a, sur appel de l'Agence de la biomédecine, annulé ce jugement et rejeté la demande de la Fondation Jérôme Lejeune.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 février 2016, 30 mai 2016 et 23 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la Fondation Jérôme Lejeune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'Agence de la biomédecine ;<br/>
<br/>
              3°) de mettre à la charge de l'Agence de la biomédecine la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :  <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 2011-814 du 7 juillet 2011 ; <br/>
<br/>
              - la loi n° 2013-715 du 6 août 2013 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la Fondation Jérôme Lejeune et à la SCP Piwnica, Molinié, avocat de l'Agence de la biomédecine.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 juillet 2017, présentée par la Fondation Jérôme Lejeune ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 juillet 2017, présentée par l'Agence de la biomédecine ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 15 juillet 2012, l'Agence de la biomédecine a autorisé, pour une durée de cinq ans, le centre hospitalier universitaire de Montpellier à mettre en oeuvre un protocole de recherche sur l'embryon ayant pour finalité l'étude de l'identification des biomarqueurs impliqués dans la régulation des embryons préimplantatoires humains et l'analyse de l'effet de l'âge maternel sur ces processus de régulation ; que la Fondation Jérôme Lejeune se pourvoit en cassation contre l'arrêt du 31 décembre 2015 par lequel la cour administrative d'appel de Paris a rejeté ses conclusions tendant à l'annulation de cette autorisation ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2151-5 du code de la santé publique, dans sa rédaction applicable entre le 9 juillet 2011 et le 8 août 2013 : " I. - La recherche sur l'embryon humain, les cellules souches embryonnaires et les lignées de cellules souches est interdite. / II. - Par dérogation au I, la recherche est autorisée si les conditions suivantes sont réunies : / 1° La pertinence scientifique du projet de recherche est établie ; / 2° La recherche est susceptible de permettre des progrès médicaux majeurs ; / 3° Il est expressément établi qu'il est impossible de parvenir au résultat escompté par le biais d'une recherche ne recourant pas à des embryons humains, des cellules souches embryonnaires ou des lignées de cellules souches ; / 4° Le projet de recherche et les conditions de mise en oeuvre du protocole respectent les principes éthiques relatifs à la recherche sur l'embryon et les cellules souches embryonnaires. / Les recherches alternatives à celles sur l'embryon humain et conformes à l'éthique doivent être favorisées. / III. - Une recherche ne peut être menée qu'à partir d'embryons conçus in vitro dans le cadre d'une assistance médicale à la procréation et qui ne font plus l'objet d'un projet parental. La recherche ne peut être effectuée qu'avec le consentement écrit préalable du couple dont les embryons sont issus, ou du membre survivant de ce couple, par ailleurs dûment informés des possibilités d'accueil des embryons par un autre couple ou d'arrêt de leur conservation. Dans le cas où le couple ou le membre survivant du couple consent à ce que ses embryons surnuméraires fassent l'objet de recherches, il est informé de la nature des recherches projetées afin de lui permettre de donner un consentement libre et éclairé. A l'exception des situations mentionnées au dernier alinéa de l'article L. 2131-4 et au troisième alinéa de l'article L. 2141-3, le consentement doit être confirmé à l'issue d'un délai de réflexion de trois mois. Dans tous les cas, le consentement des deux membres du couple ou du membre survivant du couple est révocable sans motif tant que les recherches n'ont pas débuté (....) " ; qu'aux termes de l'article R. 2151-4 du même code, dans sa rédaction applicable à la même date : " La délivrance de l'information préalable et le recueil par écrit du consentement libre et éclairé de chacun des membres du couple ou du membre survivant du couple, prévus au dernier alinéa de l'article L. 2131-4 et aux articles L. 2141-3, L. 2141-4 et L. 2151-5, sont réalisés, en cas de diagnostic préimplantatoire, par le praticien agréé en application de l'article L. 2131-4-2 et, dans les autres cas, par le praticien intervenant, conformément au cinquième alinéa de l'article L. 2142-1, dans un établissement, laboratoire ou organisme autorisé en application du même article. / L'information relative à la nature des recherches projetées porte sur les différentes catégories de recherches susceptibles d'être mises en oeuvre dans le cadre de l'article L. 2151-5. (...) " ; <br/>
<br/>
              Sur le consentement du couple donneur :<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le projet de recherche autorisé par la décision attaquée constitue le prolongement d'un précédent projet autorisé par une décision du 11 juillet 2007 et fait appel aux mêmes embryons que ceux visés dans cette précédente autorisation ; que préalablement à la délivrance de cette première autorisation, l'information préalable exigée avait été délivrée au couple donneur dont le consentement avait été recueilli par écrit conformément aux dispositions alors applicables ; qu'à la date à laquelle ce consentement a été recueilli, l'article L. 2151-5 du code de la santé publique ne prévoyait pas que le couple fût informé de la nature des recherches projetées, cette condition ayant été introduite par l'article 41 de la loi du 7 juillet 2011 relative à la bioéthique modifiant l'article L. 2151-5 du code de la santé publique, avant d'être supprimée par la loi du 6 août 2013 tendant à modifier la loi n° 2011-814 du 7 juillet 2011 relative à la bioéthique en autorisant sous certaines conditions la recherche sur l'embryon et les cellules souches embryonnaires ;<br/>
<br/>
              4. Considérant que la situation juridique qui résulte du consentement donné par un couple donneur est constituée à la date à laquelle ce consentement est délivré, et ne peut être remise en cause par une modification ultérieure des dispositions législatives ou réglementaires applicables à la délivrance du consentement ; que ceci ne fait pas obstacle à la possibilité pour le couple de révoquer son consentement dans les conditions prévues, en ce domaine, par la loi ; qu'ainsi, en jugeant que les dispositions de l'article L. 2151-5 du code de la santé publique n'imposaient pas que le couple donneur, qui avait déjà consenti à l'utilisation de ses embryons surnuméraires à des fins de recherche, réitère son consentement préalablement à la décision attaquée selon les modalités introduites à cet article par la loi précitée du 7 juillet 2011, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit ; <br/>
<br/>
              Sur la nécessité de recourir à des cellules souches embryonnaires :<br/>
<br/>
              5. Considérant qu'aux termes du 3° du II de l'article L. 2151-5 précité, dans sa rédaction applicable entre le 9 juillet 2011 et le 8 août 2013, la recherche sur l'embryon humain, les cellules souches embryonnaires et les lignées de cellules souches n'est autorisée que s'il " est expressément établi qu'il est impossible de parvenir au résultat escompté par le biais d'une recherche ne recourant pas à des embryons humains, des cellules souches embryonnaires ou des lignées de cellules souches " ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'autorisation litigieuse concerne un protocole de recherche ayant notamment pour finalité de comprendre quels gènes et quels mécanismes moléculaires sont impliqués dans le développement embryonnaire humain précoce et de préciser les modalités d'intervention de différents facteurs de ce développement, notamment l'âge de la mère ; qu'en écartant le moyen tiré de la méconnaissance des dispositions citées au point précédent au motif qu'il n'aurait pas été possible de mener à bien les recherches envisagées en recourant à des embryons d'origine animale, notamment de souris, dès lors que le développement embryologique de la souris et de l'homme diffèrent sensiblement et que l'utilisation d'embryons animaux n'aurait pas permis de comprendre les mécanismes à l'oeuvre dans l'embryon préimplantatoire humain, la cour n'a pas commis d'erreur de droit dans l'application des dispositions de l'article L. 2151-5 du code de la santé publique et a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la Fondation Jérôme Lejeune n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Fondation Jérôme Lejeune la somme de 3 000 euros à verser à l'Agence de la biomédecine au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Agence de la biomédecine qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la Fondation Jérôme Lejeune est rejeté.<br/>
Article 2 : La Fondation Jérôme Lejeune versera à l'Agence de la biomédecine une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la Fondation Jérôme Lejeune et à l'Agence de la biomédecine.<br/>
Copie en sera adressée pour information à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-05 SANTÉ PUBLIQUE. BIOÉTHIQUE. - RECHERCHES SUR L'EMBRYON ET LES CELLULES SOUCHES EMBRYONNAIRES HUMAINES (ART. L. 2151-5 DU CODE DE LA SANTÉ PUBLIQUE) - CONSENTEMENT DU COUPLE DONNEUR - RÈGLES APPLICABLES - RÈGLES EN VIGUEUR À LA DATE À LAQUELLE LE CONSENTEMENT EST DÉLIVRÉ.
</SCT>
<ANA ID="9A"> 61-05 La situation juridique qui résulte du consentement donné par un couple donneur à des recherches sur leurs embryons est constituée à la date à laquelle ce consentement est délivré et ne peut être remise en cause par une modification ultérieure des dispositions législatives ou réglementaires applicable à la délivrance du consentement. Ceci ne fait pas obstacle à la possibilité pour le couple de révoquer son consentement dans les conditions prévues, en ce domaine, par la loi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
