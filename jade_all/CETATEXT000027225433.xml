<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027225433</ID>
<ANCIEN_ID>JG_L_2013_03_000000353988</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/22/54/CETATEXT000027225433.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 25/03/2013, 353988, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353988</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:353988.20130325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 novembre 2011 et 8 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA Peugeot Citroën Automobiles, dont le siège est Route de Gizy à Vélizy-Villacoublay (78943), représentée par son président directeur général en exercice ; la SA Peugeot Citroën Automobiles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10VE00489 du 20 septembre 2011 par lequel la cour administrative d'appel de Versailles a annulé le jugement n° 0810078-5 du 1er décembre 2009 par lequel le tribunal administratif de Montreuil a rejeté la demande de M. B...A...tendant à l'annulation de la décision du 12 juin 2008 du ministre du travail, des relations sociales, de la famille et de la solidarité, autorisant la SA Peugeot Citroën Automobiles à le licencier, ainsi que la décision du ministre ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A...;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini, avocat de la SA Peugeot Citroën Automobiles  et de Me Spinosi, avocat de M.A...,<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini, avocat de la SA Peugeot Citroën Automobiles  et à Me Spinosi, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une décision en date du 12 juin 2008, le ministre du travail, des relations sociales, de la famille et de la solidarité a accordé à la SA Peugeot Citroën Automobiles, l'autorisation de licencier M. A..., employé en qualité de technicien et représentant syndical au comité d'établissement du site de Saint-Ouen ; que, saisie en appel par M. A... du jugement du 1er décembre 2009 par lequel le tribunal administratif de Montreuil a rejeté sa demande, la cour administrative d'appel de Versailles, par un arrêt contre lequel la SA Peugeot Citroën automobile se pourvoit en cassation, a annulé la décision du ministre ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque leur licenciement est envisagé, celui-ci ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou avec leur appartenance syndicale ; que, lorsque la demande de licenciement est fondée sur des faits accomplis dans le cadre du contrat de travail, ayant un caractère fautif, il appartient à l'autorité administrative, sous le contrôle du juge de l'excès de pouvoir, de rechercher si ces faits sont d'une gravité suffisante pour justifier le licenciement ; qu'en revanche, dans l'hypothèse où la demande est motivée par un acte ou un comportement du salarié survenu en dehors de l'exécution de son contrat de travail, notamment dans le cadre de l'exercice de ses fonctions représentatives, il appartient à l'inspecteur du travail, et le cas échéant au ministre, pour apprécier le bien-fondé de la demande, de rechercher, sous le contrôle du juge de l'excès de pouvoir, non la nature fautive des actes reprochés, mais si ces actes sont établis et de nature, compte tenu de leur répercussion sur le fonctionnement de l'entreprise, à rendre impossible le maintien du salarié dans l'entreprise, eu égard à la nature de ses fonctions et à l'ensemble des règles applicables au contrat de travail de l'intéressé ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que la demande d'autorisation de licenciement portait sur des faits accomplis par M. A...dans le cadre de ses fonctions représentatives ; que le ministre a estimé que ces faits, rendant impossible le maintien du salarié dans l'entreprise, étaient de nature à justifier le licenciement de M. A... ; qu'il ressort de l'arrêt attaqué que la cour a annulé la décision du ministre au motif que les faits reprochés n'étaient pas constitutifs d'une faute d'une gravité suffisante, alors qu'il résulte de ce qui a été dit ci-dessus qu'il lui appartenait d'apprécier, non pas leur caractère fautif, mais si leurs répercussions sur le fonctionnement de l'entreprise  rendaient impossible le maintien du salarié ; qu'en statuant ainsi, la cour a entaché son arrêt d'erreur de droit ; que, par suite, la SA Peugeot Citroën Automobiles est fondée à en demander l'annulation ;<br/>
<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M.A..., la somme que demande la SA Peugeot Citroën Automobiles au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ; que ces dispositions font obstacle à ce que soit mise à la charge de la SA Peugeot Citroën, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande l'avocat de  M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 20 septembre 2011 de la cour administrative d'appel de Versailles est     annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la SA Peugeot Citroën Automobile et au titre de l'article 37 de la loi du 10 juillet 1991 par M A...sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SA Peugeot Citroën Automobiles, à M. B... A...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
