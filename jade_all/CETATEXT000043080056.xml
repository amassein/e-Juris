<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043080056</ID>
<ANCIEN_ID>JG_L_2021_01_000000441767</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/08/00/CETATEXT000043080056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 28/01/2021, 441767, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441767</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441767.20210128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 10 juillet et 15 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Victimes coronavirus covid-19 France, M. C... F... et M. E... D... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 6-2 de l'arrêté du ministre des solidarités et de la santé du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, créé par l'arrêté du 26 mai 2020 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n°2020-290 du 23 mars 2020 ;<br/>
              - la loi n°2020-546 du 11 mai 2020 ;<br/>
              - le décret n°2020-260 du 16 mars 2020 ;<br/>
              - le décret n°2020-293 du 23 mars 2020 ;<br/>
              - le décret n° 2020-337du 26 mars 2020<br/>
              - le décret n°2020-545 du 11 mai 2020 ;<br/>
              - le décret n°2020-548 du 11 mai 2020 ;<br/>
              - le décret n° 2020-630 du 26 mai 2020 ;<br/>
              - le décret n° 2020-663 du 31 mai 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 12 et 14 janvier 2021, présentées respectivement par M. D... et par l'association Victimes coronavirus Covid-19 et M. F... ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 3131-1 du code de la santé publique, dans sa rédaction issue de l'article 2 de la loi du 23 mars 2020 : " En cas de menace sanitaire grave appelant des mesures d'urgence, notamment en cas de menace d'épidémie, le ministre chargé de la santé peut, par arrêté motivé, prescrire dans l'intérêt de la santé publique toute mesure proportionnée aux risques courus et appropriée aux circonstances de temps et de lieu afin de prévenir et de limiter les conséquences des menaces possibles sur la santé de la population. Le ministre peut également prendre de telles mesures après la fin de l'état d'urgence sanitaire prévu au chapitre Ier bis du présent titre, afin d'assurer la disparition durable de la situation de crise sanitaire ".<br/>
<br/>
              2. L'article L. 3131-12 inséré dans le code de la santé publique par la loi du 23 mars 2020 prévoit que : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire (...) en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". Aux termes du I de l'article L. 3131-15 du code de la santé publique : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) / 9° En tant que de besoin, prendre toute mesure permettant la mise à la disposition des patients de médicaments appropriés pour l'éradication de la catastrophe sanitaire ; / 10° En tant que de besoin, prendre par décret toute autre mesure réglementaire limitant la liberté d'entreprendre, dans la seule finalité de mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12 du présent code. (...) ". Aux termes du III du même article : " Les mesures prescrites en application du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ". D'autre part, aux termes du premier alinéa de l'article L. 3131-16 du code de la santé publique : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le ministre chargé de la santé peut prescrire, par arrêté motivé, toute mesure réglementaire relative à l'organisation et au fonctionnement du dispositif de santé, à l'exception des mesures prévues à l'article L. 3131-15, visant à mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12. ". Aux termes du 3e alinéa du même article : " Les mesures prescrites en application du présent article sont strictement nécessaires et proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ". Ces dispositions, dans leur rédaction initiale issue de la loi du 23 mars 2020, puis dans leur rédaction citée ci-dessus issue de loi du 11 mai 2020, qui s'est bornée à en réorganiser la présentation, étaient applicables à la date d'édiction des dispositions attaquées par l'effet de l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 puis de l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, qui ont déclaré puis prorogé l'état d'urgence sanitaire. <br/>
<br/>
              Sur les circonstances :<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. Au vu de l'évolution de la situation sanitaire, de nouvelles mesures générales ont été adoptées par deux décrets du 11 mai 2020 pour assouplir progressivement les sujétions imposées afin de faire face à l'épidémie.<br/>
<br/>
              4. Le sulfate d'hydroxychloroquine est commercialisé par le laboratoire Sanofi sous le nom de marque de Plaquenil, en vertu d'une autorisation de mise sur le marché initialement délivrée le 27 mai 2004, avec pour indications thérapeutiques le traitement symptomatique d'action lente de la polyarthrite rhumatoïde, le lupus érythémateux discoïde, le lupus érythémateux subaigu, le traitement d'appoint ou prévention des rechutes des lupus systémiques et la prévention des lucites. <br/>
<br/>
              5. A la suite d'un avis sur les recommandations thérapeutiques dans la prise en charge du covid-19 du 23 mars 2020 du Haut Conseil de la santé publique, le Premier ministre, par un décret du 25 mars 2020, modifié par un décret du 26 mars, a complété d'un article 12-2 le décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, pour prévoir notamment les conditions dans lesquelles l'hydroxychloroquine peut être prescrite, dispensée et administrée aux patients atteints de covid-19, en dehors des indications de l'autorisation de mise sur le marché du Plaquenil. A ce titre, d'une part, par dérogation aux dispositions du code de la santé publique relatives aux autorisations de mise sur le marché, il a autorisé la prescription, la dispensation et l'administration sous la responsabilité d'un médecin de l'hydroxychloroquine aux patients atteints de covid-19, dans les établissements de santé qui les prennent en charge, ainsi que, pour la poursuite de leur traitement si leur état le permet et sur autorisation du prescripteur initial, à domicile, en précisant que ces prescriptions interviennent, après décision collégiale, dans le respect des recommandations du Haut Conseil de la santé publique et, en particulier, de l'indication pour les patients atteints de pneumonie oxygéno-requérante ou d'une défaillance d'organe. D'autre part, il a prévu, au cinquième alinéa de cet article 12-2, que : " La spécialité pharmaceutique Plaquenil (c), dans le respect des indications de son autorisation de mise sur le marché, et les préparations à base d'hydroxychloroquine ne peuvent être dispensées par les pharmacies d'officine que dans le cadre d'une prescription initiale émanant exclusivement de spécialistes en rhumatologie, médecine interne, dermatologie, néphrologie, neurologie ou pédiatrie ou dans le cadre d'un renouvellement de prescription émanant de tout médecin ".<br/>
<br/>
              6. Ces dispositions ont été reprises à l'identique à l'article 17 du décret n° 2020-545 du 11 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, qui abroge notamment l'article 12-2 du décret du 25 mars 2020, puis à l'article 19 du décret n° 2020-548 du même jour ayant le même objet, qui abroge le précédent et est entré en vigueur dès sa publication au Journal officiel de la République française le 12 mai 2020. <br/>
<br/>
              7. A la suite d'un nouvel avis du Haut Conseil de la santé publique relatif à l'utilisation de l'hydroxychloroquine dans le covid-19 du 24 mai 2020, le Premier ministre a abrogé, par décret du 26 mai 2020, l'article 19 du décret précité et le ministre des solidarités et de la santé a, par un arrêté du même jour pris sur le fondement de l'article L. 3131-16 du code de la santé publique, complété l'arrêté du 23 mars 2020 prescrivant les mesures d'organisation et de fonctionnement du système de santé nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire par un article 6-2 reprenant les dispositions du cinquième alinéa de l'article 12-2 du décret du 23 mars 2020 cité au point 5. Les requérants demandent au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 26 mai 2020, en tant qu'il crée cet article 6-2 au sein de l'arrêté du 23 mars 2020.<br/>
<br/>
              Sur la légalité de l'arrêté attaqué : <br/>
<br/>
              8. Par les dispositions citées au point 2 et ainsi qu'il ressort des travaux parlementaires préalables à l'adoption de la loi du 23 mars 2020, le législateur a entendu permettre l'adoption par le pouvoir exécutif de mesures plus contraignantes que celles susceptibles d'être adoptées en cas de " menace sanitaire grave appelant des mesures d'urgence " sur le fondement de l'article L. 3131 1 du code de la santé publique. A cette fin, il a entendu, d'une part, permettre au Premier ministre de prendre certaines mesures limitant la liberté d'aller et venir, la liberté d'entreprendre et la liberté de réunion ou procédant à des réquisitions et, d'autre part, permettre au ministre chargé de la santé de prendre les mesures générales touchant au dispositif de santé, notamment aux établissements et services, aux professionnels, aux actes et aux produits de santé, qui ne relèvent pas de la compétence du Premier ministre, ainsi que les mesures individuelles d'application des mesures prescrites par ce dernier, sous réserve, dans tous les cas, que ces mesures soient nécessaires pour garantir la santé publique dans la situation de catastrophe sanitaire, strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il suit de là que les dispositions du 9° de l'article L. 3131-15 du code de la santé publique et celles de l'article L. 3131-16 du même code doivent être interprétées, en ce qui concerne les mesures susceptibles d'être adoptées en matière de médicaments, comme réservant au Premier ministre les mesures restreignant la liberté d'entreprendre ou le droit de propriété pour assurer la disponibilité des médicaments nécessaires pour faire face à la catastrophe sanitaire et comme habilitant le ministre chargé de la santé à prendre les autres mesures générales nécessaires pour que les patients puissent bénéficier des soins dont ils ont besoin pendant la catastrophe sanitaire, sous réserve qu'elles soient strictement nécessaires et proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu, le cas échéant en dérogeant sur des points limités à des dispositions législatives.<br/>
<br/>
              9. Une mesure se bornant, comme l'arrêté du 26 mai 2020 dont sont issues les dispositions contestées, à prévoir qu'une spécialité pharmaceutique ne pourra être dispensée en officine, dans le respect des indications de son autorisation de mise sur le marché, que sur le fondement d'une prescription initiale émanant de certains spécialistes susceptibles de la prescrire dans le cadre de son autorisation de mise sur le marché ou dans le cadre d'un renouvellement de prescription émanant de tout médecin, visant ainsi à éviter une tension sur son approvisionnement pour les patients y recourant dans les indications prévues par son autorisation de mise sur le marché afin qu'ils puissent bénéficier des soins dont ils ont besoin pendant la catastrophe sanitaire, entre dans le champ de l'article L. 3131-16 du code de la santé publique. Par suite, les requérants ne sont pas fondés à soutenir que les dispositions attaquées auraient été prises par une autorité incompétente et à demander pour ce motif leur annulation. <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de l'association Victimes coronavirus covid-19 et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association Victimes coronavirus covid-19 France, représentant unique désigné, pour l'ensemble des requérants, et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
