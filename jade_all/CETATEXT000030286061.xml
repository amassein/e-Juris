<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286061</ID>
<ANCIEN_ID>JG_L_2015_02_000000370843</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286061.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 25/02/2015, 370843, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370843</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:370843.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. E...B...et Mme C...D...ont demandé au tribunal administratif de Grenoble de condamner le centre hospitalier universitaire de Grenoble à leur verser une indemnité de 20 934 euros en réparation des préjudices subis à l'occasion du suivi de leur fils A...entre le 9 mars et le 10 juin 2005. Par un jugement n° 0904343 du 23 mars 2012, le tribunal administratif a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 12LY01251 du 6 juin 2013, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par M. B...et MmeD....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 août et 24 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, M. B...et Mme D...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier universitaire de Grenoble les sommes de 3 500 euros et 35 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. B...et de Mme D...et à Me Le Prado, avocat du centre hospitalier universitaire de Grenoble ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le jeune A...B..., alors âgé de six ans, a été victime, le 9 mars 2005 d'une crise d'épilepsie qui a justifié sa prise en charge par le centre hospitalier universitaire de Grenoble ; que le traitement antiépileptique mis en place à compter de cette date s'est révélé peu efficace et a été modifié à compter du 11 avril 2005 ; qu'il a alors été décidé de diminuer progressivement, suivant un calendrier précis, le médicament initialement administré, la Micropakine, et, dans le même temps, d'introduire par paliers un nouveau médicament, le Lamictal ; qu'après une nouvelle hospitalisation au centre hospitalier universitaire de Grenoble en mai 2005, justifiée par la persistance des absences et des crises de grand mal, les doses de Lamictal ont été augmentées et un troisième médicament, le Zarontin, a été introduit ; que, le 9 juin 2005, l'enfant s'est plaint d'une douleur au pied, accompagnée dans les heures suivantes de fièvre, de vomissements et d'absences ; que, le lendemain, devant l'aggravation de ces symptômes et l'apparition de réactions cutanées, il a fait l'objet d'une nouvelle hospitalisation, en urgence, au centre hospitalier universitaire de Grenoble où a été diagnostiqué un syndrome de Lyell, pathologie cutanée très grave provoquée par une allergie médicamenteuse ; que, devant l'aggravation des lésions cutanées, l'enfant a été transféré en réanimation puis au service des brûlés de l'hôpital Saint-Joseph et Saint-Luc de Lyon ; que le syndrome a évolué favorablement à partir du 22 juin 2005, sa consolidation étant fixée au 14 février 2006, et n'a finalement pas laissé de séquelles ; <br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              2. Considérant que les parties ont été régulièrement avisées que l'audience se tiendrait le 16 mai 2013 ; que le premier mémoire en défense du centre hospitalier universitaire de Grenoble, qui ne comportait aucun élément de droit ou de fait nouveau au regard du débat qui avait eu lieu en première instance, a été enregistré au greffe du tribunal le 6 mai 2013 et immédiatement communiqué aux parties ; que, l'instruction ayant été close le 12 mai, les requérants ont disposé d'un délai suffisant pour répliquer ; qu'ils n'ont d'ailleurs pas demandé que l'examen de l'affaire soit reporté afin de disposer d'un délai supplémentaire pour préparer un mémoire ; que, dans ces conditions, le moyen tiré de ce que l'arrêt aurait été rendu dans des conditions irrégulières doit être écarté ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt : <br/>
<br/>
              3. Considérant, en premier lieu, que, pour juger que la prescription de Lamictal en association avec la Micropakine n'avait pas présenté le caractère d'une faute médicale, la cour, se fondant sur le rapport de l'expert, a retenu que le fait de débuter le traitement avec la Micropakine puis de le poursuivre avec le Lamictal dans le cadre d'une substitution progressive était conforme aux pratiques en matière de traitement de l'épilepsie, compte tenu de la très grande gravité de la pathologie de l'enfant ; qu'elle a relevé que le calendrier retenu respectait les recommandations d'augmentation progressive du Lamictal en association avec la Dépakine et que si l'expert faisait état d'une posologie initiale de Lamictal supérieure aux recommandations, ce choix n'avait pas revêtu, en l'espèce, un caractère fautif et qu'en tout état de cause l'existence d'un lien de causalité direct et certain entre la posologie initiale et l'apparition, deux mois après le début du traitement, du syndrome de Lyell n'était pas établie ; que la cour, dont l'arrêt est suffisamment motivé sur ce point, n'a ni dénaturé les faits qui lui étaient soumis, ni entaché de contradiction les motifs de son arrêt en écartant, conformément aux conclusions de l'expert, l'existence d'un lien causal entre l'administration initiale du Lamictal en doses supérieures à celles préconisées et l'apparition du syndrome, motif qui suffit à lui seul à justifier son arrêt sur ce point ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la cour a retenu que la circonstance, à la supposer établie, que, le 9 juin 2005, à la suite d'un entretien téléphonique entre les parents du jeuneA..., et les services du centre hospitalier universitaire de Grenoble, une dose de Lamictal supérieure à celle préconisée dans le cadre du calendrier de substitution aurait été administrée à l'enfant au domicile familial n'était pas la conséquence d'une faute commise par l'établissement, et qu'en tout état de cause l'existence d'un lien direct entre l'administration de cette dose, postérieure à l'apparition des premiers symptômes allergiques, et le syndrome de Lyell n'était pas établie ; que la cour n'a pas dénaturé les pièces du dossier en ne regardant pas comme établie l'existence d'un lien causal entre le fait allégué et le dommage ; qu'au surplus, l'arrêt n'est pas critiqué en tant qu'il nie l'existence d'une faute commise le 9 juin 2005 par les services du centre hospitalier, motif qui suffit à lui seul à le justifier sur ce point ; <br/>
<br/>
              5. Considérant, en troisième lieu, que la cour a estimé qu'à supposer que, lors de l'admission, le 10 juin 2005, du jeune A...dans le service des urgences pédiatriques du centre hospitalier universitaire de Grenoble, un premier diagnostic de rougeole ou de scarlatine aurait été posé de manière erronée, cette erreur, à la supposer fautive, n'avait pas eu d'incidence sur l'évolution de l'état de santé de l'enfant, dès lors que l'administration de Lamictal avait été suspendue ; qu'en ne retenant pas l'existence d'un lien causal entre un éventuel retard de diagnostic et le dommage, la cour a porté une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              6. Considérant, enfin, que pour rejeter les conclusions indemnitaires fondées sur l'absence d'une information des parents du jeune A...sur les risques d'allergie médicamenteuse susceptibles de découler de l'administration de Lamictal alors, selon eux, que leur enfant aurait pu bénéficier d'un autre traitement, moins risqué et aussi efficace, la cour a estimé que ce traitement était impérieusement requis et que la gravité des troubles épileptiques manifestés par le jeune patient ne permettait aucune alternative thérapeutique présentant des risques moindres ; qu'elle en a déduit que le défaut d'information commis par le centre hospitalier n'avait pas entraîné de perte de chance pour le jeune A...de se soustraire au risque qui s'était réalisé ; qu'elle n'a, ce faisant, pas dénaturé les faits soumis à son appréciation souveraine ;  <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. B...et Mme D...ne sont pas fondés à demander l'annulation de l'arrêt du 6 juin 2013 de la cour administrative d'appel de Lyon ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier universitaire de Grenoble qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le centre hospitalier universitaire de Grenoble au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu de laisser la contribution pour l'aide juridique à la charge de M. B...et MmeD... ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. B...et de Mme D...est rejeté. <br/>
<br/>
Article 2 : Les conclusions du centre hospitalier universitaire de Grenobles présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La contribution pour l'aide juridique est laissée à la charge de M. B...et Mme D....<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. E...B...et Mme C...D..., au centre hospitalier universitaire de Grenoble et à la caisse primaire d'assurance maladie de l'Isère. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
