<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938385</ID>
<ANCIEN_ID>JG_L_2016_01_000000386869</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/83/CETATEXT000031938385.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 27/01/2016, 386869, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386869</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:386869.20160127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 2 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, l'association Très Grande Vigilance en Albret (TGV en Albret), l'association de Sauvegarde des Landes et Coteaux de Gascogne et l'association Landes Graves Viticulture Environnement en Arruan, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la décision implicite née du silence gardé par le Premier ministre sur leur demande du 1er septembre 2014 tendant à l'abrogation des dispositions du 5° des I et II de l'article R. 11-3 du code de l'expropriation pour cause d'utilité publique, et, d'autre part, les dispositions du 5° de l'article R. 112-4 du nouveau code de l'expropriation pour cause d'utilité publique, issu du décret n° 2014-1635 du 26 décembre 2014 relatif à la partie réglementaire de ce code ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier les articles litigieux afin d'y prévoir la mention, dans le dossier soumis à enquête, des conditions de financement et des engagements pris par les financeurs du projet de travaux ou d'ouvrage.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :	<br/>
              -  la Constitution, notamment son Préambule ;<br/>
              -  le traité sur le fonctionnement de l'Union européenne ; <br/>
              - la convention sur l'accès à l'information, la participation du public au processus décisionnel et l'accès à la justice en matière d'environnement fait à d'Aarhus le 25 juin 1998 ;<br/>
              - la directive n° 2011/92/UE du 13 décembre 2011 ;<br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code des transports ; <br/>
              - la loi n° 2014-872 du 4 août 2014 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article R. 11-3 du code de l'expropriation pour cause d'utilité publique, abrogé par le décret du 26 décembre 2014 relatif à la partie réglementaire de ce code, et dont la substance a été reprise à l'article R. 112-4 du même code, entré en vigueur le 1er janvier 2015, lorsqu'une déclaration d'utilité publique est demandée en vue de la réalisation de travaux ou d'ouvrages, le dossier adressé au préfet par l'expropriant comprend obligatoirement une appréciation sommaire des dépenses ; que les associations requérantes demandent au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite de refus d'abroger les dispositions de l'article R. 11-3 du code de l'expropriation pour cause d'utilité publique, maintenues transitoirement en vigueur pour certaines enquêtes publiques, ainsi que l'article R. 112-4 du même code, en tant qu'elles ne prévoient pas l'obligation, pour les projets ferroviaires, d'inclure au dossier d'enquête une description des conditions de financement de tels projets ainsi que les engagements pris par les financeurs ; <br/>
<br/>
              2. Considérant, d'une part, qu'en vertu de l'article R. 1511-4 du code des transports, issu du décret du 22 mai 2014 relatif à certaines dispositions de la partie réglementaire de ce code, le document d'évaluation des grands projets d'infrastructures, notamment ferroviaires, comporte une analyse de leurs conditions de financement ; que, d'autre part, ni l'article 2 de la convention du 25 juin 1998 sur l'accès à l'information, la participation du public au processus décisionnel et l'accès à la justice en matière d'environnement, dite " convention d'Aarhus ", ni la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, ni l'article 191 du traité sur le fonctionnement de l'Union européenne, ni l'article 3 de la Charte de l'environnement, ni l'article 6 de la loi du 4 août 2014 portant réforme ferroviaire, pas plus qu'aucun autre texte ou principe, n'impose d'inclure au dossier d'expropriation soumis à enquête publique un document formalisant les engagements pris par les financeurs ; que, par suite, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le garde des sceaux, ministre de la justice, les associations requérantes ne sont pas fondées à demander l'annulation de la décision implicite et des dispositions qu'elles attaquent ; que le surplus de leurs conclusions ne peut, par suite, qu'être rejeté ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association Très Grande Vigilance en Albret (TGV en Albret) et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association Très Grande Vigilance en Albret (TGV en Albret), à l'association de Sauvegarde des Landes et Coteaux de Gascogne (ASLCG), à l'association Landes Graves Viticulture Environnement en Arruan, au Premier ministre, à la garde des sceaux, ministre de la justice et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
