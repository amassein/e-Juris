<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032095894</ID>
<ANCIEN_ID>JG_L_2016_02_000000373516</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/09/58/CETATEXT000032095894.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 22/02/2016, 373516</TITRE>
<DATE_DEC>2016-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373516</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:373516.20160222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 373516, par une requête et trois nouveaux mémoires, enregistrés les 26 novembre 2013, 9 juillet 2014, 23 décembre 2015 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la République et Canton de Genève demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2012-DC-0311 du 4 décembre 2012 par laquelle l'Autorité de sûreté nucléaire (ASN) a imposé à Electricité de France (EDF) des prescriptions complémentaires applicables au site électronucléaire du Bugey (Ain), au vu des conclusions du troisième réexamen de sûreté du réacteur n° 2 de l'installation nucléaire de base (INB) n° 78 ;<br/>
<br/>
              2°) d'annuler la décision n° 2012-DC-0276 du 26 juin 2012 par laquelle l'ASN a imposé à EDF des prescriptions complémentaires applicables au site électronucléaire du Bugey, au vu des conclusions des évaluations complémentaires de sûreté des INB nos 78 et 89 ; <br/>
<br/>
              3°) d'annuler l'avis n° 2012-AV-0155 du 10 juillet 2012 de l'ASN sur la poursuite de l'exploitation du réacteur n° 2 de la centrale nucléaire du Bugey après son troisième réexamen de sûreté ; <br/>
<br/>
              4°) d'annuler la décision implicite ou révélée du ministre de l'écologie, du développement durable et de l'énergie autorisant la poursuite de l'exploitation du réacteur n° 2 de l'INB n° 78 du site électronucléaire du Bugey ; <br/>
<br/>
              5°) d'annuler la décision implicite ou révélée de l'ASN autorisant la poursuite de l'exploitation du réacteur n° 2 de l'INB n° 78 du site électronucléaire du Bugey ; <br/>
<br/>
              6°) de mettre à la charge de l'ASN et de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les dépens en application de l'article R. 761-1 du même code.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 373517, par une requête et trois nouveaux mémoires, enregistrés les 26 novembre 2013, 9 juillet 2014, 23 décembre 2015 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la République et Canton de Genève demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2012-DC-0361 du 25 juillet 2013 par laquelle l'Autorité de sûreté nucléaire (ASN) a imposé à Electricité de France (EDF) les prescriptions complémentaires applicables au site électronucléaire du Bugey (Ain), au vu des conclusions du troisième réexamen de sûreté du réacteur n° 4 de l'installation nucléaire de base (INB) n° 89 ; <br/>
<br/>
              2°) d'annuler la décision n° 2012-DC-0276 du 26 juin 2012 par laquelle l'ASN a imposé à EDF des prescriptions complémentaires applicables au site électronucléaire du Bugey, au vu des conclusions des évaluations complémentaires de sûreté des INB nos 78 et 89 ;<br/>
<br/>
              3°) d'annuler la décision implicite ou révélée du ministre de l'écologie, du développement durable et de l'énergie autorisant la poursuite de l'exploitation du réacteur n° 4 de l'INB n° 89 du site électronucléaire du Bugey ; <br/>
<br/>
              4°) d'annuler la décision implicite ou révélée de l'ASN autorisant la poursuite de l'exploitation du réacteur n° 4 de l'INB n° 89 du site électronucléaire du Bugey ; <br/>
<br/>
              5°) de mettre à la charge de l'ASN et de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les dépens en application de l'article R. 761-1 du même code.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention d'Espoo du 25 février 1991 sur l'évaluation de l'impact sur l'environnement dans un contexte transfrontière ;<br/>
              - la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 ;<br/>
              - la directive 2009/71/Euratom du Conseil du 25 juin 2009 ;<br/>
              - le code de l'environnement ; <br/>
              - le décret n° 2007-1557 du 2 novembre 2007 ;<br/>
              - l'arrêté du 7 février 2012 du ministre de l'écologie, du développement durable, des transports et du logement, du ministre de l'économie, des finances et de l'industrie, et du ministre chargé de l'industrie, de l'énergie et de l'économie numérique fixant les règles générales relatives aux installations nucléaires de base ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la République et Canton de Genève et autre et à la SCP Coutard, Munier-Apaire, avocat d'Electricité de France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les interventions de la ville de Genève :<br/>
<br/>
              2. Considérant que la ville de Genève justifie d'un intérêt suffisant à l'annulation des décisions attaquées ; qu'ainsi, ses interventions sont recevables ;<br/>
<br/>
              Sur la recevabilité des conclusions tendant à l'annulation des décisions " implicites ou révélées " du ministre chargé de la sûreté nucléaire et de l'Autorité de sûreté nucléaire ainsi que de l'avis du 10 juillet 2012 de cette autorité :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article L. 593-1 du code de l'environnement : " Les installations nucléaires de base énumérées à l'article L. 593-2 sont soumises au régime légal défini par les dispositions du présent chapitre et du chapitre VI du présent titre en raison des risques ou inconvénients qu'elles peuvent présenter pour la sécurité, la santé et la salubrité publiques ou la protection de la nature et de l'environnement. " ; qu'aux termes de l'article L. 593-7 du même code : " La création d'une installation nucléaire de base est soumise à une autorisation. / Cette autorisation ne peut être délivrée que si, compte tenu des connaissances scientifiques et techniques du moment, l'exploitant démontre que les dispositions techniques ou d'organisation prises ou envisagées aux stades de la conception, de la construction et de l'exploitation ainsi que les principes généraux proposés pour le démantèlement ou, pour les installations de stockage de déchets radioactifs, pour leur entretien et leur surveillance après leur arrêt définitif sont de nature à prévenir ou à limiter de manière suffisante les risques ou inconvénients que l'installation présente pour les intérêts mentionnés à l'article L. 593-1. / L'autorisation prend en compte les capacités techniques et financières de l'exploitant qui doivent lui permettre de conduire son projet dans le respect de ces intérêts, en particulier pour couvrir les dépenses de démantèlement de l'installation et de remise en état, de surveillance et d'entretien de son lieu d'implantation ou, pour les installations de stockage de déchets radioactifs, pour couvrir les dépenses d'arrêt définitif, d'entretien et de surveillance. " ; qu'aux termes de l'article L. 593-18 du même code : " L'exploitant d'une installation nucléaire de base procède périodiquement au réexamen de la sûreté de son installation en prenant en compte les meilleures pratiques internationales. / Ce réexamen doit permettre d'apprécier la situation de l'installation au regard des règles qui lui sont applicables et d'actualiser l'appréciation des risques ou inconvénients que l'installation présente pour les intérêts mentionnés à l'article L. 593-1, en tenant compte notamment de l'état de l'installation, de l'expérience acquise au cours de l'exploitation, de l'évolution des connaissances et des règles applicables aux installations similaires. / Les réexamens de sûreté ont lieu tous les dix ans. Toutefois, le décret d'autorisation peut fixer une périodicité différente si les particularités de l'installation le justifient. (...) " ; qu'aux termes de l'article L. 593-19 du même code : " L'exploitant adresse à l'Autorité de sûreté nucléaire et au ministre chargé de la sûreté nucléaire un rapport comportant les conclusions de l'examen prévu à l'article L. 593-18 et, le cas échéant, les dispositions qu'il envisage de prendre pour remédier aux anomalies constatées ou pour améliorer la sûreté de son installation. / Après analyse du rapport, l'Autorité de sûreté nucléaire peut imposer de nouvelles prescriptions techniques. Elle communique au ministre chargé de la sûreté nucléaire son analyse du rapport. (...) " ; qu'il résulte de ces dispositions et de celles du chapitre III du titre IX du livre V de la partie législative du même code qu'aussi longtemps qu'aucun décret de mise à l'arrêt définitif et de démantèlement n'est intervenu, après la mise en oeuvre de la procédure prévue à l'article L. 593-25 du code de l'environnement, une installation nucléaire de base est autorisée à fonctionner, dans des conditions de sûreté auxquelles il appartient à l'Autorité de sûreté nucléaire de veiller en vertu de l'article L. 592-1 du même code ; que, par suite, la République et Canton de Genève n'est pas fondée à soutenir que l'édiction de nouvelles prescriptions techniques par l'Autorité de sûreté nucléaire, à la suite de la transmission, par l'exploitant de l'installation, du rapport de réexamen de sûreté, constituerait une décision implicite d'autoriser l'exploitation de cette dernière pour dix années supplémentaires ; qu'ainsi, les conclusions de la requête tendant à l'annulation des décisions " implicites ou révélées " de l'Autorité de sûreté nucléaire et du ministre chargé de la sûreté nucléaire autorisant de nouveau, pour dix ans, l'exploitation de la centrale nucléaire du Bugey sont irrecevables ; <br/>
<br/>
              4. Considérant, d'autre part, qu'il résulte des dispositions de l'article L. 593-19 du code de l'environnement citées au point précédent que l'analyse par l'Autorité de sûreté nucléaire du rapport de réexamen de sûreté adressée au ministre chargé de la sûreté nucléaire constitue un simple avis qui ne présente pas le caractère d'une décision faisant grief ; qu'ainsi, les conclusions tendant à l'annulation de l'avis de l'Autorité de sûreté nucléaire du 10 juillet 2012 sur la poursuite de l'exploitation du réacteur n° 2 de la centrale nucléaire du Bugey après son troisième réexamen de sûreté ne peuvent qu'être rejetées comme irrecevables ; <br/>
<br/>
              Sur la légalité des décisions des 26 juin et 4 décembre 2012 et du 25 juillet 2013 de l'Autorité de sûreté nucléaire :<br/>
<br/>
              5. Considérant, en premier lieu, que les décisions attaquées édictent des prescriptions techniques nouvelles concernant les installations n° 78 (réacteur n° 4) et n° 89 (réacteur n° 2) ; que, par suite, la République et Canton de Genève ne saurait utilement contester la légalité de l'autorisation, par le décret du 23 avril 2010, de la création d'une installation nucléaire de base dénommée " Installation de conditionnement et d'entreposage de déchets activés " (ICEDA) au regard des dispositions de l'article L. 593-14 du code de l'environnement et de l'article 31 du décret du 2 novembre 2007 susvisé relatif aux installations nucléaires de base et au contrôle, en matière de sûreté nucléaire, du transport de substances radioactives, celle-ci étant, en tout état de cause, sans incidence sur la légalité de ces décisions ; que le moyen tiré de ce que l'article 31 du décret du 2 novembre 2007 serait dépourvu de fondement légal est, dès lors, inopérant ;<br/>
<br/>
              6. Considérant, en deuxième lieu, que si la requérante soutient que les décisions attaquées ont été prises en méconnaissance des dispositions de l'article 2 de la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, relatives à une procédure d'évaluation des incidences sur l'environnement des projets susceptibles d'avoir des incidences notables sur l'environnement, ces dispositions ont été transposées en droit interne par les articles R. 122-1 et suivants du code de l'environnement ; que la requérante ne peut donc directement se prévaloir, à l'encontre des décisions attaquées, qui ne sont pas réglementaires, des dispositions de cette directive ; qu'ainsi, le moyen tiré de la méconnaissance des dispositions de cette directive, faute d'une évaluation des incidences des décisions attaquées sur l'environnement et d'une information du public, ne peut qu'être écarté ; <br/>
<br/>
              7. Considérant, en troisième lieu, qu'aux termes de l'article 2 de la convention d'Espoo du 25 février 1991 sur l'évaluation de l'impact sur l'environnement dans un contexte transfrontière, susvisée : " (...) 3. La Partie d'origine veille à ce que, conformément aux dispositions de la présente Convention, il soit procédé à une évaluation de l'impact sur l'environnement avant que ne soit prise la décision d'autoriser ou d'entreprendre une activité proposée inscrite sur la liste figurant à l'Appendice I, qui est susceptible d'avoir un impact transfrontière préjudiciable important. / 4. La Partie d'origine veille, conformément aux dispositions de la présente Convention, à ce que toute activité proposée inscrite sur la liste figurant à l'Appendice I, qui est susceptible d'avoir un impact transfrontière préjudiciable important, soit notifiée aux Parties touchées. (...) " ; que les décisions attaquées, qui n'ont ni pour objet ni pour effet d'autoriser une activité au sens de ces stipulations, n'avaient pas à être précédées d'une évaluation de leur impact sur l'environnement ni d'une notification de la République helvétique ; que, par suite, le moyen tiré de la méconnaissance de ces stipulations ne peut qu'être écarté ; <br/>
<br/>
              8. Considérant, en quatrième lieu, que l'article L. 593-7 du code de l'environnement, qui transpose sur ce point en droit interne les dispositions de l'article 6 de la directive 2009/71/Euratom du Conseil du 25 juin 2009 établissant un cadre communautaire pour la sûreté nucléaire des installations nucléaires, prévoit que l'exploitant d'une installation nucléaire maintient les capacités techniques et financières nécessaires pour couvrir les dépenses de démantèlement de l'installation et de remise en état, de surveillance et d'entretien de son lieu d'implantation ; que ces dispositions législatives ont été précisées par les dispositions du décret du 2 novembre 2007 et par l'arrêté interministériel du 7 février 2012 fixant les règles générales relatives aux installations nucléaires de base ; que la République et Canton de Genève soutient que les décisions attaquées méconnaîtraient ces dispositions, faute pour l'Autorité de sûreté nucléaire d'avoir suffisamment évalué les capacités financières de l'exploitant ; que toutefois, d'une part, les décisions contestées n'édictent pas de prescriptions relatives aux capacités financières de l'exploitant Electricité de France ; que, d'autre part, la requérante n'apporte aucune précision au soutien de son moyen en ce qui concerne une insuffisance des capacités financières de l'exploitant au regard des exigences fixées par les dispositions précitées ; que, par suite, ce moyen ne peut qu'être écarté ;<br/>
<br/>
              9. Considérant, en cinquième lieu, qu'à la différence des prescriptions que l'Autorité de sûreté nucléaire peut édicter, sur le fondement de l'article L. 592-19 du code de l'environnement, afin de compléter les dispositions des décrets et arrêtés pris en matière de sûreté nucléaire, les " règles fondamentales de sûreté " que cette Autorité élabore, et qui sont progressivement remplacées par des " guides de l'Autorité de sûreté nucléaire ", sont dépourvues de caractère impératif ; qu'il appartient toutefois au juge de prendre en compte ces " règles " ou " guides " parmi les éléments de fait et de droit appréciés dans son contrôle de l'évaluation qui a été faite du risque par l'Autorité de sûreté nucléaire ; <br/>
<br/>
              10. Considérant que la République et Canton de Genève, sans mettre en cause aucune disposition des décisions contestées, soutient que l'évaluation du risque de chute d'avion accidentelle par l'Autorité de sûreté serait fondée sur une règle obsolète, faute pour cette règle de prendre en compte le risque de chute d'un aéronef de l'aviation civile commerciale ; que, toutefois, contrairement à ce qui est soutenu, et en tout état de cause, la règle fondamentale de sûreté I.2.a prend en compte un tel risque ; qu'il résulte par ailleurs de l'instruction que l'Autorité de sûreté nucléaire a procédé à une évaluation du risque de chute d'avion, en fonction des différentes hypothèses envisageables et de la probabilité d'un tel évènement ; <br/>
<br/>
              11. Considérant que le risque de perte totale de source froide en cas d'agression externe a fait l'objet d'une étude spécifique, conduite par Electricité de France à la demande de l'Autorité de sûreté nucléaire, et d'une modification des règles générales d'exploitation du site pour garantir le niveau d'eau requis dans les réservoirs d'eau déminéralisée ; qu'il ne résulte pas de l'instruction que ce risque aurait fait l'objet d'une évaluation inexacte ; <br/>
<br/>
              12. Considérant qu'il ne résulte pas de l'instruction que la configuration du site ne permettrait pas de préserver la centrale des risques de crue du Rhône ; que le risque d'inondation lié à d'autres facteurs, tels que l'effacement du barrage de Vouglans et la conjonction d'une crue historique de l'Ain et du Rhône, a été pris en compte, dans les différentes hypothèses testées ; qu'il ne résulte pas de l'instruction que l'évaluation de ces risques, compte tenu notamment des " retours d'expérience " à la suite de l'inondation de la centrale du Blayais survenue en 1999, qui ont été formalisés dans le guide n° 13 de l'Autorité de sûreté nucléaire, aurait été inexacte ;<br/>
<br/>
              13. Considérant qu'il résulte de l'instruction que, si la requérante soutient que les corps des cuves des réacteurs nos 2 et 4 seraient formés de l'alliage Inconel 600, dont l'Autorité de sûreté nucléaire a relevé la forte sensibilité à la corrosion, cet alliage n'est présent que dans un nombre limité de pièces spécifiques de ces cuves ; que la résistance des cuves au vieillissement a fait l'objet de vérifications dans le cadre des examens décennaux ; que ces pièces font l'objet, de la part de l'exploitant, d'un programme de suivi du vieillissement et de contrôle des cuves, sous le contrôle de l'Autorité de sûreté nucléaire ; que les défauts de corrosion affectant les dômes de l'enceinte de confinement du réacteur n° 2, relevés dans une lettre d'inspection du 13 mars 2013 de l'Autorité de sûreté nucléaire, ont fait l'objet d'une étude spécifique conduite par le centre d'ingénierie d'Electricité de France ; que le rapport d'expertise a conclu à l'absence de nocivité des défauts relevés sur la tenue mécanique des enceintes de confinement ; qu'en outre, des travaux de maintenance ont été réalisés sur les dômes des bâtiments ; qu'ainsi, il ne résulte pas de l'instruction que les décisions attaquées seraient entachées d'une inexacte évaluation des risques relatifs à l'état des cuves et des enceintes de confinement des réacteurs nos 2 et 4 ; <br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que la République et Canton de Genève n'est pas fondée à soutenir que les décisions contestées procéderaient d'une erreur d'appréciation de l'Autorité de sûreté nucléaire quant aux risques inhérents aux réacteurs nos 2 et 4 ; <br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par les défendeurs, les requêtes de la République et Canton de Genève doivent être rejetées, y compris leurs conclusions présentées au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la République et Canton de Genève la somme de 3 000 euros à verser à Electricité de France au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de la ville de Genève sont admises.<br/>
<br/>
      Article 2 : Les requêtes de la République et Canton de Genève sont rejetées.<br/>
<br/>
Article 3 : La République et Canton de Genève versera à Electricité de France une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la République et Canton de Genève, à Electricité de France, à l'Autorité de sûreté nucléaire, à la ville de Genève et à la ministre de l'environnement, de l'énergie et de la mer.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE DE DÉCISION. ACTES NE PRÉSENTANT PAS CE CARACTÈRE. - ANALYSE PAR L'ASN DU RAPPORT DE RÉEXAMEN DE SÛRETÉ D'UNE INSTALLATION NUCLÉAIRE, ADRESSÉE AU MINISTRE CHARGÉ DE LA SÛRETÉ NUCLÉAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-03-005 ENERGIE. INSTALLATIONS NUCLÉAIRES. - 1) EDICTION DE NOUVELLES PRESCRIPTIONS TECHNIQUES PAR L'ASN À LA SUITE DE LA TRANSMISSION PAR L'EXPLOITANT DU RAPPORT DE RÉEXAMEN DE SÛRETÉ DE L'INSTALLATION - DÉCISION IMPLICITE D'AUTORISER L'EXPLOITATION DE L'INSTALLATION POUR DIX ANNÉES SUPPLÉMENTAIRES - ABSENCE - 2) ANALYSE PAR L'ASN DU RAPPORT DE RÉEXAMEN DE SÛRETÉ D'UNE INSTALLATION NUCLÉAIRE, ADRESSÉE AU MINISTRE CHARGÉ DE LA SÛRETÉ NUCLÉAIRE - DÉCISION FAISANT GRIEF - ABSENCE.
</SCT>
<ANA ID="9A"> 01-01-05-02-02 L'analyse qu'effectue l'Autorité de sûreté nucléaire (ASN) du rapport de réexamen de sûreté d'une installation nucléaire réalisé par l'exploitant, qui est adressée au ministre chargé de la sûreté nucléaire, constitue un simple avis qui ne présente pas le caractère d'une décision faisant grief.</ANA>
<ANA ID="9B"> 29-03-005 1) Il résulte des articles L. 593-1, L. 593-7, L. 593-18 et L. 293-19 du code de l'environnement ainsi que du chapitre III du titre IX du livre V de la partie législative du même code qu'aussi longtemps qu'aucun décret de mise à l'arrêt définitif et de démantèlement n'est intervenu, après la mise en oeuvre de la procédure prévue à l'article L. 593-25, une installation nucléaire de base est autorisée à fonctionner, dans des conditions de sûreté auxquelles il appartient à l'Autorité de sûreté nucléaire (ASN) de veiller en vertu de l'article L. 592-1. Par suite, l'édiction de nouvelles prescriptions techniques par l'ASN, à la suite de la transmission, par l'exploitant de l'installation, du rapport de réexamen de sûreté, ne constitue pas une décision implicite d'autoriser l'exploitation de cette dernière pour dix années supplémentaires.... ,,2) L'analyse qu'effectue l'Autorité de sûreté nucléaire (ASN) du rapport de réexamen de sûreté d'une installation nucléaire réalisé par l'exploitant, qui est adressée au ministre chargé de la sûreté nucléaire, constitue un simple avis qui ne présente pas le caractère d'une décision faisant grief.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
