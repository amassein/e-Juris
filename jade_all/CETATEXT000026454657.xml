<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454657</ID>
<ANCIEN_ID>JG_L_2012_10_000000355105</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454657.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/10/2012, 355105, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355105</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Nicolas Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:355105.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 décembre 2011 et 21 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Lazare A, demeurant ..., M. Francis A, demeurant ..., M. Benjamin A, demeurant ..., Mme Myriam A, épouse B, demeurant ..., Mme Régine A, épouse C, demeurant ... ; M. A et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA04106 du 20 octobre 2011 par lequel la cour administrative d'appel de Paris a rejeté leur requête tendant à l'annulation du jugement n° 0811025 du 25 juin 2010 du tribunal administratif de Paris rejetant leur demande d'annulation pour excès de pouvoir de la décision du 24 avril 2008 par laquelle le Premier ministre a fixé à la somme de 3 817 000 euros le montant de l'indemnisation du préjudice non encore réparé résultant de la spoliation des biens ayant appartenu à M. Georges D, qui avait désigné leur père comme légataire à titre universel et à titre personnel, ainsi qu'à l'annulation de cette décision du Premier ministre ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du tribunal administratif ainsi que la décision du Premier ministre et d'enjoindre au Premier ministre de fixer le montant du préjudice de M. D en évaluant les biens spoliés à la date de sa décision, ou, à tout le moins, à la date de la publication du décret du 10 septembre 1999 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 99-778 du 10 septembre 1999 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de M. A et autres,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de M. A et autres ;<br/>
<br/>
<br/>
<br/>1. Considérant que la responsabilité de l'Etat est engagée en raison des dommages causés par les agissements qui, ne résultant pas d'une contrainte directe de l'occupant, ont permis ou facilité, durant l'Occupation, la déportation à partir de la France de personnes victimes de persécutions antisémites ; que, pour compenser les préjudices matériels et moraux exceptionnels et d'une gravité extrême subis par les victimes de la déportation et par leurs ayants droit, l'Etat a pris une série de mesures, telles que des pensions, des indemnités, des aides ou des mesures de réparation qui, prises dans leur ensemble, et bien qu'elles aient procédé d'une démarche graduelle et reposent sur des bases largement forfaitaires, doivent être regardées comme ayant permis, autant qu'il est possible, l'indemnisation des préjudices de toute nature causés par les actions de l'Etat qui ont concouru à la déportation ; <br/>
<br/>
              2. Considérant que, notamment, le décret du 10 septembre 1999 institue auprès du Premier ministre une commission, chargée, aux termes de l'article 1er de ce décret, " d'examiner les demandes individuelles présentées par les victimes ou par leurs ayants droit pour la réparation des préjudices consécutifs aux spoliations de biens intervenues du fait des législations antisémites prises, pendant l'Occupation, tant par l'occupant que par les autorités de Vichy ", et, à ce titre, " de rechercher et de proposer les mesures de réparation, de restitution ou d'indemnisation appropriées " ; que l'article 8-2 du même décret prévoit que les décisions d'indemnisation sont prises par le Premier ministre sur la base des recommandations de la commission ;<br/>
<br/>
              3. Considérant que le dispositif institué par ces dispositions aboutit, au terme d'une procédure de conciliation, à ce que la commission recommande, le cas échéant, au Premier ministre de prendre une mesure de réparation, de restitution ou d'indemnisation ; que les décisions prises par le Premier ministre sont susceptibles de faire l'objet d'un recours pour excès de pouvoir ; qu'elles peuvent être annulées notamment si elles sont entachées d'erreur de droit, d'erreur de fait, d'erreur manifeste d'appréciation ou de détournement de pouvoir ; qu'elles doivent notamment permettre la restitution à leurs propriétaires ou aux ayants droit de ceux-ci des biens dont ils ont été, soit spoliés dans des conditions exorbitantes du droit commun, soit, s'il apparaît qu'ils ont subi des pressions ou violences et qu'un préjudice direct leur a été causé, privés par une transaction d'apparence légale, en contrepartie, le cas échéant, du remboursement de l'indemnisation qu'ils auraient précédemment perçue à ce titre ; que dans le cas où cette restitution est impossible, ces biens ayant été détruits ou n'ayant pu être retrouvés, les propriétaires ou leurs ayants droit doivent en être indemnisés selon les règles particulières issues du décret du 10 septembre 1999 ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, statuant sur la demande présentée par M. A et autres, qui tendait à l'indemnisation du préjudice non encore réparé résultant de la spoliation des biens, saisis le 15 mars 1943 par les forces d'occupation, de M. Georges D, mort en déportation, qui avait désigné comme légataire à titre universel, à charge pour ce dernier de répartir le produit de la vente de ces biens pour venir en aide à des familles israélites éprouvées par la guerre, leur père, M. Jacob A, aux droits et obligations duquel ils succèdent, le Premier ministre a décidé de répartir entre trois institutions dont l'objet social répond aux volontés du testateur une indemnité d'un montant fixé, conformément à la proposition de la commission, et compte tenu de l'indemnité versée à M. Jacob A par la République fédérale d'Allemagne en exécution d'un accord conclu avec lui le 9 octobre 1963, à la moitié de la valeur des biens déterminée par cet accord, sur la base d'une expertise réalisée en 1956 et actualisée en 1963, cette valeur étant augmentée d'un coefficient d'érosion monétaire pour tenir compte de la dépréciation de la monnaie depuis 1963 ;<br/>
<br/>
              5. Considérant qu'en jugeant que le Premier ministre pouvait à bon droit pour fixer le montant de l'indemnité allouée, en application des dispositions du décret du 10 septembre 1999, évaluer les biens spoliés, qui n'ont pu être retrouvés, à la date à laquelle l'étendue de la spoliation a pu être connue et majorer le montant ainsi évalué pour tenir compte de la dépréciation de la monnaie, la cour administrative d'appel de Paris n'a commis aucune erreur de droit ; que M. A et autres ne sont, par suite, pas fondés à demander l'annulation de l'arrêt du 20 octobre 2011 de la cour administrative d'appel de Paris ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les conclusions du pourvoi de M. A et autres doivent être rejetées, y compris celles qui tendent à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A et autres est rejeté.<br/>
Article 2 : La présente décision sera notifiée à MM. Lazare A, Francis A et Benjamin A, à Mmes Myriam A, épouse B, et Régine A, épouse C, et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60 - DISPOSITIF D'INDEMNISATION DES VICTIMES DE SPOLIATIONS INTERVENUES DU FAIT DES LÉGISLATIONS ANTISÉMITES EN VIGUEUR PENDANT L'OCCUPATION (DÉCRET DU 10 SEPTEMBRE 1999) - DÉCISION DU PREMIER MINISTRE SUR LA PROPOSITION DE LA COMMISSION DE PRENDRE UNE MESURE D'INDEMNISATION [RJ1].
</SCT>
<ANA ID="9A"> 60 Le dispositif issu du décret n° 99-778 du 10 septembre 1999 instituant une commission pour l'indemnisation des victimes de spoliations intervenues du fait des législations antisémites en vigueur pendant l'Occupation, qui participe à l'indemnisation des préjudices de toute nature causés par les actions de l'Etat ayant concouru à la déportation, aboutit, au terme d'une procédure de conciliation, à ce que la commission prévue par ce décret recommande, le cas échéant, au Premier ministre de prendre une mesure d'indemnisation.,,Les décisions prises par le Premier ministre doivent notamment permettre, que, lorsque la restitution des biens est impossible, les propriétaires ou leurs ayants droit soient indemnisés selon les règles particulières issues du décret du 10 septembre 1999. Dans ce cadre, en application des dispositions de ce décret, possibilité d'évaluer les biens spoliés qui n'ont pu être retrouvés à la date à laquelle l'étendue de la spoliation a pu être connue et de majorer le montant ainsi évalué pour tenir compte de la dépréciation de la monnaie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le caractère d'excès de pouvoir des recours dirigés contre les décisions prises par le Premier ministre dans le cadre de ce dispositif, lequel participe à l'indemnisation des préjudices de toute nature causés par les actions de l'Etat ayant concouru à la déportation, et sur le contrôle de l'erreur manifeste d'appréciation du juge, CE, 28 juillet 2012, Callou et autres, n° 348105, à publier au Recueil. Cf., pour l'ensemble des mesures prises en vue de compenser les préjudices matériels et moraux subis par les victimes de la déportation et par leurs ayants droit, CE, Assemblée, avis, 16 février 2009, Mme Hoffman-Glémane, n° 315499, p. 43.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
