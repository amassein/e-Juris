<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845261</ID>
<ANCIEN_ID>JG_L_2018_04_000000413585</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 26/04/2018, 413585, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413585</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413585.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. C...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 20 janvier 2017 du ministre de la défense prononçant à son encontre la sanction disciplinaire du premier groupe du " blâme du ministre " ;<br/>
<br/>
              2°)  de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la défense ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. B...Pez-Lavergne, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que M.A..., capitaine dans la gendarmerie nationale, commandait, au moment des faits ayant justifié la sanction en litige, la division d'observation surveillance de la région de gendarmerie Rhône-Alpes ; que, par la décision attaquée du 20 janvier 2017, le major général de la gendarmerie nationale a infligé à M. A...un " blâme du ministre " au motif que les " violences conjugales [qu'il a commises] sont incompatibles avec son statut de militaire de la gendarmerie " ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 4137-4 du code de la défense : " Le ministre de la défense ou les autorités habilitées à cet effet prononcent les sanctions disciplinaires et professionnelles prévues aux articles L. 4137-1 et L. 4137 2 (...) " ; qu'aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " (...) peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité / (...) 3° (...) le major général des armées, les majors généraux de l'armée de terre, de la marine, de l'armée de l'air et de la gendarmerie (...) " ; que la décision attaquée comporte la signature du major général de la gendarmerie nationale ; qu'il résulte des dispositions précitées du décret du 27 juillet 2005 que celui-ci était compétent pour signer cette décision au nom du ministre de la défense ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article R. 4137-15 du code de la défense : " Avant d'être reçu par l'autorité militaire de premier niveau dont il relève, le militaire a connaissance de l'ensemble des pièces et documents au vu desquels il est envisagé de le sanctionner " ; qu'aux termes de l'article R. 4137-16 du même code : " Lorsqu'un militaire a commis une faute ou un manquement, il fait l'objet d'une demande de sanction motivée qui est adressée à l'autorité militaire de premier niveau dont il relève, même si elle émane d'une autorité extérieure à la formation. / L'autorité militaire de premier niveau entend l'intéressé, vérifie l'exactitude des faits, et, si elle décide d'infliger une sanction disciplinaire du premier groupe, arrête le motif correspondant à la faute ou au manquement et prononce la sanction dans les limites de son pouvoir disciplinaire. / Si l'autorité militaire de premier niveau estime que la gravité de la faute ou du manquement constaté justifie soit une sanction disciplinaire du premier groupe excédant son pouvoir disciplinaire, soit une sanction du deuxième ou troisième groupe, la demande de sanction est adressée à l'autorité militaire de deuxième niveau dont relève l'autorité militaire de premier niveau même si le militaire fautif a changé de formation administrative durant cette période. " ;<br/>
<br/>
              4. Considérant que M. A...soutient qu'il n'a eu connaissance ni des avis de l'autorité militaire de premier niveau et de deuxième niveau, pourtant visés par la décision contestée, ni de la demande de sanction ; que toutefois, d'une part, ces avis sont simplement constitutifs des actes par lesquels l'autorité militaire initialement saisie transmet la demande de sanction à l'autorité militaire supérieure compétente pour prendre la sanction susceptible d'être infligée à l'intéressé, compte tenu de la nature des faits ou du comportement qui lui sont reprochés, aucune disposition du code de la défense n'imposant au demeurant leur communication au militaire ; que, d'autre part, il ressort des pièces du dossier que le requérant a pris connaissance de la demande de sanction le 16 septembre 2016, antérieurement au prononcé de la sanction en litige ; que, par suite, le moyen tiré de l'irrégularité de la procédure de communication du dossier ne peut qu'être écarté ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article R. 156 du code de procédure pénale : " En matière criminelle, correctionnelle ou de police, aucune expédition autre que celle des arrêts, jugements, ordonnances pénales définitifs et titres exécutoires ne peut être délivrée à un tiers sans une autorisation du procureur de la République ou du procureur général, selon le cas, notamment en ce qui concerne les pièces d'une enquête terminée par une décision de classement sans suite " ; qu'il ressort des pièces du dossier que la communication du dossier de l'instruction judiciaire pour violences conjugales aux fins d'être versée au dossier disciplinaire visant M. A...a été autorisée par le procureur de la République ; que cette communication, exclusivement destinée à l'autorité investie du pouvoir disciplinaire, visait à éclairer celle-ci sur l'origine et la gravité des violences conjugales reprochées au requérant eu égard à son statut d'officier de gendarmerie et au fait qu'elles se sont produites dans une enceinte militaire ; qu'elle n'a donc méconnu ni les dispositions précitées du code de procédure pénale ni les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui garantissent à toute personne le droit au respect de la vie privée et familiale ;<br/>
<br/>
              6. Considérant, enfin, qu'aux termes de l'article L. 4137-2 du code de la défense : " 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre (...) " ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que M. A...a été condamné, par un jugement du tribunal correctionnel de Lyon du 16 mars 2016, à une peine de quatre mois d'emprisonnement avec sursis pour violences conjugales ; que cette peine a été confirmée par la cour d'appel de Lyon ; que ces violences ont eu lieu dans un logement situé sur une enceinte militaire ; que le requérant a, en outre, été sanctionné d'un blâme, le 20 janvier 2017, pour comportements inappropriés à l'égard de plusieurs de ses subordonnées féminines ; qu'eu égard aux responsabilités de M. A...et alors même que sa manière de servir par ailleurs aurait donné pleine satisfaction, à la gravité des faits incompatible avec les exigences inhérentes au statut de militaire, l'autorité investie du pouvoir disciplinaire n'a pas, dans les circonstances de l'espèce et au regard du pouvoir d'appréciation dont elle disposait, pris une sanction disciplinaire disproportionnée en lui infligeant la sanction du premier groupe du " blâme du ministre " ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C...A...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
