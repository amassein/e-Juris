<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074271</ID>
<ANCIEN_ID>JG_L_2021_01_000000432846</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074271.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 26/01/2021, 432846, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432846</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432846.20210126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Marseille de condamner l'Etat à lui verser la somme de 387 721,19 euros en réparation des préjudices qu'elle estime avoir subis du fait d'une situation de harcèlement moral dans l'exercice de ses fonctions. Par un jugement n° 1510453 du 11 septembre 2017, le tribunal administratif de Marseille a rejeté la demande de Mme A....<br/>
<br/>
              Par un arrêt n° 17MA04384 du 21 mai 2019, la cour administrative d'appel de Marseille a, sur appel de Mme A..., annulé le jugement du tribunal administratif de Marseille, a condamné l'Etat à verser à titre de dommages et intérêts à Mme A... une somme de 5000 euros et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Procédures devant le Conseil :<br/>
<br/>
              1° Sous le n° 432846, par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 22 juillet et le 18 octobre 2019, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Marseille du 21 mai 2019 en tant qu'il n'a pas entièrement fait droit à ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 433104, par un pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 29 juillet 2019, le ministre de l'éducation nationale et de jeunesse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce même arrêt en tant qu'il a fait partiellement droit aux conclusions de l'appel de Mme A... ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'ensemble des conclusions présentées par Mme A....<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 72-580 du 4 juillet 1972 ;<br/>
              - le décret n° 72-581 du 4 juillet 1972 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., professeure certifiée hors classe d'anglais, qui exerce ses fonctions au lycée Victor Hugo à Marseille depuis 2002, a sollicité par un courrier du 17 août 2015, adressé au ministre de l'éducation nationale, le bénéfice de la protection fonctionnelle, ainsi que la réparation des préjudices financier et moral qu'elle estime avoir subis dans l'exercice de ses fonctions, notamment du fait d'une situation de harcèlement moral de la part des proviseurs successifs du lycée. Le recteur de l'académie de Marseille a rejeté sa demande par un courrier du 4 novembre 2015. Le tribunal administratif de Marseille, par un jugement du 11 septembre 2017, a rejeté la demande de Mme A... tendant à la condamnation de l'Etat à lui verser la somme de 387 721,19 euros en réparation des préjudices invoqués. Mme A... et le ministre de l'éducation nationale et de la jeunesse se pourvoient en cassation, chacun dans la mesure où il lui fait grief, contre l'arrêt du 21 mai 2019 par lequel la cour administrative d'appel de Marseille a annulé ce jugement du tribunal administratif de Marseille, condamné l'Etat à verser à Mme A... la somme de 5 000 euros et rejeté le surplus des conclusions de sa demande. Il y a lieu de joindre leurs pourvois pour y statuer par une seule décision.<br/>
<br/>
              Sur le pourvoi de Mme A... :<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond qu'en demandant qu'il soit enjoint au ministre de l'éducation nationale et de la jeunesse de prendre en charge les frais de réparation de sa voiture vandalisée et de payer divers frais depuis 2008, Mme A... a présenté des conclusions indemnitaires, tendant à la réparation de préjudices distincts de ceux, dont elle demandait par ailleurs également réparation, imputables aux faits de harcèlement moral dont elle soutenait être victime. En ne statuant pas sur ces conclusions, la cour administrative d'appel a entaché son arrêt d'irrégularité.  <br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 6 quinquies de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel. / Aucune mesure concernant notamment le recrutement, la titularisation, la rémunération, la formation, l'évaluation, la notation, la discipline, la promotion, l'affectation et la mutation ne peut être prise à l'égard d'un fonctionnaire en prenant en considération : / 1° Le fait qu'il ait subi ou refusé de subir les agissements de harcèlement moral visés au premier alinéa ; / 2° Le fait qu'il ait exercé un recours auprès d'un supérieur hiérarchique ou engagé une action en justice visant à faire cesser ces agissements ; / 3° Ou bien le fait qu'il ait témoigné de tels agissements ou qu'il les ait relatés. / Est passible d'une sanction disciplinaire tout agent ayant procédé ou ayant enjoint de procéder aux agissements définis ci-dessus ".<br/>
<br/>
              4. Il appartient à un agent public qui soutient avoir été victime d'agissements constitutifs de harcèlement de soumettre au juge des éléments de fait susceptibles de faire présumer l'existence d'un tel harcèlement. Il incombe à l'administration de produire, en sens contraire, une argumentation de nature à démontrer que les agissements en cause sont justifiés par des considérations étrangères à tout harcèlement. La conviction du juge, à qui il revient d'apprécier si les agissements de harcèlement sont ou non établis, se détermine au vu de ces échanges contradictoires, qu'il peut compléter, en cas de doute, en ordonnant toute mesure d'instruction utile. Pour apprécier si des agissements dont il est allégué qu'ils sont constitutifs d'un harcèlement moral revêtent un tel caractère, le juge administratif doit tenir compte des comportements respectifs de l'administration auquel il est reproché d'avoir exercé de tels agissements et de l'agent qui estime avoir été victime d'un harcèlement moral. Pour être qualifiés de harcèlement moral, ces agissements doivent être répétés et excéder les limites de l'exercice normal du pouvoir hiérarchique. Dès lors qu'elle n'excède pas ces limites, une simple diminution des attributions justifiée par l'intérêt du service, en raison d'une manière de servir inadéquate ou de difficultés relationnelles, n'est pas constitutive de harcèlement moral.<br/>
<br/>
              5. Pour écarter l'existence d'un harcèlement moral à l'égard de Mme A..., la cour a jugé, d'une part, que certains des faits qu'elle invoquait ne permettaient pas de présumer l'existence d'un tel harcèlement et, d'autre part, que si, en revanche, les conditions dans lesquelles le service Mme A... avait été réorganisé, à compter de 2010, qui se traduisaient notamment par le retrait d'enseignements valorisants en BTS et d'heures supplémentaires, l'affectation à des classes de seconde réputées plus difficiles et un morcèlement significatif de ses emplois du temps, l'empêchant de poursuivre les activités accessoires qu'elle exerçait antérieurement dans l'enseignement supérieur, permettaient de faire présumer un tel harcèlement, l'administration établissait néanmoins que cette évolution était justifiée par des considérations étrangères à tout harcèlement. La cour a, sur ce point, entaché son arrêt d'insuffisance de motivation et d'erreur de droit dès lors que, d'une part, elle a omis de se prononcer sur l'ensemble des faits invoqués par Mme A... pour étayer l'existence d'un harcèlement à son égard, notamment sur sa mise en cause par le proviseur devant des élèves ou des représentants syndicaux ou que le fait qu'elle était la seule enseignante de l'établissement à avoir vu son service réorganisé dans de telles conditions et que, d'autre part, elle s'est bornée, pour juger que le ministre établissait que l'évolution défavorable des conditions de service de Mme A... était justifiée par des considérations étrangères à tout harcèlement, à mentionner que le ministre invoquait l'intérêt du service, sans caractériser ce dernier, et à retenir l'existence de difficultés relationnelles de Mme A... avec sa hiérarchie, alors que celles-ci, dès lors qu'il n'était pas soutenu qu'elles auraient affecté son enseignement, n'étaient pas de nature à justifier une évolution de ses conditions de service. <br/>
<br/>
              6. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, que Mme A... est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel dans la mesure où il a rejeté le surplus des conclusions de sa requête.<br/>
<br/>
              Sur le pourvoi du ministre de l'éducation nationale et de la jeunesse :<br/>
<br/>
              7. En jugeant, d'une part, que les conditions dans lesquelles le service de Mme A... avait été organisé de 2010 à 2016 étaient justifiées par l'intérêt du service et n'excédaient pas l'exercice normal du pouvoir hiérarchique, pour en déduire qu'elles ne révélaient aucune méconnaissance des dispositions de l'article 6 quinquies de la loi du 13 juillet 1983, citées au point 5, qui prohibent les agissements répétés de harcèlement moral ayant pour objet ou pour effet une dégradation des conditions de travail susceptible, notamment, d'altérer la santé du fonctionnaire concerné, et, d'autre part, que l'administration devait néanmoins, dans les circonstances particulières de l'espèce, être regardée comme ayant commis une faute engageant la responsabilité de l'Etat pour avoir organisé le service de Mme A... de manière défavorable en ignorant la fragilité et les difficultés de santé de l'intéressée, la cour a entaché son arrêt de contradiction de motifs. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, le ministre de l'éducation nationale et de la jeunesse est fondé à demander l'annulation de l'arrêt de la cour administrative d'appel dans la mesure où il a partiellement fait droit aux conclusions de Mme A.... <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Il n'y a pas lieu dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme à verser à Mme A... au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 21 mai 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le surplus des conclusions de Mme A... est rejeté.<br/>
Article 4 : La présente décision sera notifiée à Mme B... A... et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
