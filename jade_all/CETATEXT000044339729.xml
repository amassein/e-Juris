<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044339729</ID>
<ANCIEN_ID>JG_L_2021_11_000000439609</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/33/97/CETATEXT000044339729.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 17/11/2021, 439609, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439609</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439609.20211117</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... A... a demandé au tribunal administratif de Lyon de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2007, ainsi que des pénalités correspondantes. Par un jugement n° 1707188 du 23 octobre 2018, ce tribunal a prononcé la décharge des pénalités pour manquement délibéré dont ont été assorties les cotisations supplémentaires d'impôt sur le revenu et rejeté le surplus des conclusions de cette demande.  <br/>
<br/>
              Par un arrêt n° 18LY04724 du 19 décembre 2019, la cour administrative d'appel de Lyon a constaté qu'il n'y avait pas lieu de statuer sur les conclusions de la demande à concurrence d'une somme totale de 51 219 euros ayant donné lieu à un dégrèvement en cours d'instance et a rejeté le surplus des conclusions de l'appel formé par M. A... ainsi que l'appel incident du ministre de l'économie, des finances et de la relance.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 mars et 20 août 2020 et le 29 juin 2021 au secrétariat du contentieux du conseil d'Etat, M. A... demande au conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinie, avocat de M. A... ;<br/>
<br/>
              Vu la note en délibéré, présentée par M. A..., enregistrée le 13 octobre 2021 ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des pièces du dossier soumis aux juges du fond que le fonds d'investissement britannique Lion Capital a acquis en juin 2004, par l'intermédiaire de la société Materne Luxembourg Holdco, société de droit luxembourgeois créée à cet effet, la totalité des parts de la SAS Holding Materne contrôlant la société Materne BOIN Holding France, tête d'un groupe opérationnel ayant pour activité la fabrication et la commercialisation de compotes de fruits, confitures et biscuits. En amont de la cession de Materne BOIN Holding France au fonds d'investissement Activa Capital survenue en novembre 2006, a été créée une société, la SAS Materne et Cie, en vue de faire bénéficier des cadres du groupe Materne d'un intéressement au capital du groupe. M. A..., directeur d'usine au sein de la SAS Materne a souscrit 20 300 actions de la SAS Materne et Cie, émises par augmentation de capital les 3 et 6 octobre 2006 pour un prix d'un euro par action. Cette société, ayant elle-même acquis 2 030 050 bons de souscription d'action (BSA) émis le 3 octobre 2006 par la SAS Holding Materne pour un prix de 0,10 euro par BSA, a été rachetée dans son intégralité le 19 janvier 2007 par Materne Luxembourg Holdco au prix de 9,667 euros par action. A la suite d'un contrôle portant sur les revenus de l'année 2007 de l'intéressé, après avoir estimé qu'aucun élément ne mentionnait la cession des titres litigieux, l'administration a regardé les gains résultant de cette opération comme une rémunération supplémentaire occulte imposable entre les mains du requérant au titre des revenus de capitaux mobiliers, sur le fondement du c de l'article 111 du code général des impôts. Par un jugement du 23 octobre 2018, le tribunal administratif de Lyon, après avoir fait droit à la demande de substitution de base légale formulée par l'administration fiscale visant à maintenir les impositions en litige au titre des traitements et salaires sur le fondement des articles 79 et 82 du code général des impôts, a prononcé la décharge des pénalités pour manquement délibéré dont avaient été assorties les cotisations supplémentaires d'impôt sur le revenu en litige et rejeté le surplus de la demande. Par un arrêt du 19 décembre 2019, la cour administrative d'appel de Lyon a, d'une part, constaté un non-lieu à statuer à concurrence d'un dégrèvement prononcé en cours d'instance, portant sur les prélèvements sociaux en litige et sur la majoration d'assiette de 25 % appliquée par l'administration aux revenus qualifiés de rémunérations occultes, d'autre part, rejeté le surplus des conclusions de la requête de M. A... et, enfin, rejeté l'appel incident du ministre portant sur les pénalités. <br/>
<br/>
              2. L'article 79 du code général des impôts dispose que : " Les traitements, indemnités, émoluments, salaires, pensions et rentes viagères concourent à la formation du revenu global servant de base à l'impôt sur le revenu ". Aux termes de l'article 82 du même code : " Pour la détermination des bases d'imposition, il est tenu compte du montant net des traitements, indemnités et émoluments, salaires, pensions et rentes viagères, ainsi que de tous les avantages en argent ou en nature accordés aux intéressés en sus des traitements, indemnités, émoluments, salaires, pensions et rentes viagères proprement dits (...) ". Aux termes de l'article 150-0 A du même code, dans sa rédaction applicable aux impositions en litige : " Sous réserve des dispositions propres aux bénéfices industriels et commerciaux, aux bénéfices non commerciaux et aux bénéfices agricoles ainsi que des articles 150 UB et 150 UC, les gains nets retirés des cessions à titre onéreux, effectuées directement ou par personne interposée, de valeurs mobilières (...) sont soumis à l'impôt sur le revenu lorsque le montant de ces cessions excède, par foyer fiscal, 20 000 euros pour l'imposition des revenus de l'année 2007 ".<br/>
<br/>
              3. Les gains nets retirés par une personne physique de la cession à titre onéreux de valeurs mobilières sont en principe imposables suivant le régime des plus-values de cession de valeurs mobilières des particuliers institué par l'article 150-0 A du code général des impôts, y compris lorsque ces titres ont été acquis ou souscrits auprès d'une société dont le contribuable était alors dirigeant ou salarié, ou auprès d'une société du même groupe. Il en va toutefois autrement lorsque, eu égard aux conditions de réalisation du gain de cession, ce gain doit essentiellement être regardé comme acquis, non à raison de la qualité d'investisseur du cédant, mais en contrepartie de ses fonctions de salarié ou de dirigeant et constitue, ainsi, un revenu imposable dans la catégorie des traitements et salaires en application des articles 79 et 82 du code général des impôts, réalisé et disponible l'année de la cession de ces titres. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la SAS Materne et Cie, dont étaient actionnaires les cadres salariés du groupe Materne, a acquis le 3 octobre 2006 des bons de souscription d'actions permettant, pour chacun d'entre eux, de souscrire, pour un prix déterminé à l'avance, à une action de la SAS Holding Materne. Si ces bons de souscription n'ont jamais été exercés, la plus-value réalisée par les détenteurs des titres de la SAS Materne et Cie lors de la cession de ces derniers à la SAS Materne Luxembourg Hodlco ne pouvait que refléter la valeur prise par ces BSA, unique actif de la SAS Materne et Cie, entièrement déterminée à l'occasion de la cession de Materne BOIN Holding France, détenue par la SAS Holding Materne, annoncée le même mois.<br/>
<br/>
              5. En premier lieu, la cour administrative d'appel a relevé, sans dénaturer les pièces du dossier qui lui était soumis, que cette opération d'investissement avait été retracée dans un pacte d'actionnaires conclu le 3 octobre 2006 et que celui-ci encadrait les conditions de cession des titres de la SAS Materne et Cie acquis le même jour, en accordant notamment à la société Materne Luxembourg Holdco un droit de préemption des titres acquis par chacun des cadres salariés du groupe Materne en cause, une obligation de cession ainsi qu'une promesse de vente en cas de départ sans faute de chacun d'entre eux. Eu égard à la proximité chronologique entre la souscription des bons de souscription d'action émis par la SAS Holding Materne, la cession de Materne BOIN Holding France par celle-ci, et la cession par les cadres salariés de Materne des titres de la société détenant ces bons de souscription, la cour administrative d'appel n'a pas davantage dénaturé les pièces du dossier en relevant que ce pacte trouvait sa logique dans l'existence d'une plus-value sur le point d'être réalisée par Lion Capital portant sur la période allant d'août 2004 à octobre 2006 et dans le rachat imminent de la société détenant les bons de souscription par la société Materne Luxembourg Holdco. Elle a également relevé, sans erreur de droit ni insuffisance de motivation, que les bons de souscription d'action avaient été acquis à un prix inférieur à leur valeur, garantissant la réalisation d'une plus-value lors de leur cession, trois mois plus tard, par la société les détenant. Elle a enfin relevé que le pacte d'actionnaires prévoyait qu'en contrepartie de l'acquisition des titres litigieux, les investisseurs, cadres salariés du groupe Materne, s'engageaient à exercer loyalement leurs fonctions opérationnelles avec un éventuel cessionnaire trouvé par la société Materne Luxembourg Holdco et à souscrire une clause de non-concurrence. <br/>
<br/>
              6. Il en résulte que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant, au motif que les cadres du groupe Materne avaient bénéficié, dans des conditions avantageuses trouvant essentiellement leur source dans l'exercice de leurs fonctions de salarié, d'un mécanisme leur garantissant le prix de cession de ces titres, que le gain réalisé par M. A... à l'issue de cette cession constituait un avantage en argent devant être imposé dans la catégorie des traitements et salaires, sans qu'ait d'incidence à cet égard la circonstance qu'aucun lien de subordination direct n'existait entre celui-ci et la société Materne Luxembourg Holdco, cessionnaire.<br/>
<br/>
              7. Les motifs par lesquels l'arrêt attaqué a jugé que le gain réalisé n'avait pas vocation à compenser un risque revêtent un caractère surabondant. Par suite, les moyens tirés de ce qu'ils seraient entachés d'erreur de droit sont inopérants. <br/>
<br/>
              8. En second lieu, aux termes de l'article 12 du code général des impôts : " L'impôt est dû chaque année à raison des bénéfices ou revenus que le contribuable réalise ou dont il dispose au cours de la même année ". Aux termes de l'article 156 du même code : " L'impôt sur le revenu est établi d'après le montant total du revenu net annuel dont dispose chaque foyer fiscal. Ce revenu net est déterminé eu égard aux propriétés et aux capitaux que possèdent les membres du foyer fiscal désignés aux 1 et 3 de l'article 6, aux professions qu'ils exercent, aux traitements, salaires, pensions et rentes viagères dont ils jouissent ainsi qu'aux bénéfices de toutes opérations lucratives auxquelles ils se livrent (...) ". Il résulte de la combinaison de ces dispositions que les sommes à retenir, au titre d'une année déterminée, pour l'assiette de l'impôt sur le revenu, sont celles qui, au cours de cette année, ont été mises à la disposition du contribuable, soit par voie de paiement, soit par voie d'inscription à un compte courant sur lequel l'intéressé a opéré, ou aurait pu, en droit ou en fait, opérer un prélèvement au plus tard le 31 décembre.<br/>
<br/>
              9. Si, ainsi que le relevait sans être contesté le requérant devant les juges du fond, la société Materne et Cie a pu bénéficier d'un avantage sous la forme d'une souscription à des prix préférentiels des bons de souscription d'actions émis le 3 octobre 2006 par la SAS Holding Materne, la cour administrative d'appel ne s'est pas méprise sur la portée de ses écritures et n'a pas commis d'erreur de droit en jugeant, sans se fonder sur cette circonstance inopérante, que le gain résultant, pour M. A..., de la cession en 2007 des titres Materne et Cie, correspondant à la différence entre leur prix d'acquisition et leur prix de vente, avait à bon droit été imposé au titre de l'année 2007, non prescrite, au cours de laquelle ce gain a été réalisé et mis à sa disposition, et non au titre de l'année 2006, année de leur acquisition.  <br/>
<br/>
              10. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'article 2 de l'arrêt qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat lequel n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                                  D E C I D E :<br/>
                                  --------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. D... A... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 28 septembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Benoît Bohnert, conseiller d'Etat et Mme Ophélie Champeaux, maître des requêtes-rapporteure. <br/>
<br/>
<br/>
              Rendu le 17 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		La rapporteure : <br/>
      Signé : Mme Ophélie Champeaux<br/>
<br/>
                 La secrétaire :<br/>
                                  Signé : Mme B... C...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
