<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035260344</ID>
<ANCIEN_ID>JG_L_2017_07_000000398911</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/26/03/CETATEXT000035260344.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 21/07/2017, 398911</TITRE>
<DATE_DEC>2017-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398911</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398911.20170721</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris :<br/>
              - d'annuler la décision du 21 janvier 2015 par laquelle la présidente du conseil de Paris a rejeté son recours dirigé contre la décision du directeur de la caisse d'allocations familiales de Paris refusant de prendre en compte son enfant dans le calcul de ses droits au revenu de solidarité active ;<br/>
              - d'enjoindre à la présidente du conseil de Paris de réexaminer sa situation et de lui verser la moitié de la majoration pour enfant à charge au titre du revenu de solidarité active pour la période de mars 2010 à novembre 2014.<br/>
<br/>
              Par un jugement n° 1504775 du 12 février 2016, le tribunal administratif de Paris a annulé la décision du 21 janvier 2015 et enjoint à la présidente du conseil de Paris de réexaminer la situation de M. A... afin d'établir ses droits au revenu de solidarité active dans un délai de trois mois.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 avril et 11 mai 2016 au secrétariat du contentieux du Conseil d'Etat, le département de Paris demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Paris du 12 février 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du département de Paris et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. B...A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M.A..., bénéficiaire du revenu de solidarité active et père d'une petite fille, a demandé sans succès à la caisse d'allocations familiales de Paris de prendre en considération la situation de résidence alternée de son enfant dans le calcul du montant de son allocation pour la période de mars 2010 à novembre 2014. Par une décision du 21 janvier 2015, la présidente du conseil de Paris a confirmé à M. A...le rejet de cette demande. Par un jugement du 12 février 2016, le tribunal administratif de Paris a, à la demande de M.A..., annulé la décision du 21 janvier 2015 et enjoint à la présidente du conseil de Paris de réexaminer sa situation afin d'établir ses droits au revenu de solidarité active dans un délai de trois mois. Le département de Paris se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes du premier alinéa de l'article 372 du code civil : " Les père et mère exercent en commun l'autorité parentale ". Le premier alinéa de l'article  373-2 du même code dispose que : " La séparation des parents est sans incidence sur les règles de dévolution de l'exercice de l'autorité parentale ". Selon le premier alinéa de l'article 373-2-9 de ce code : " (...) la résidence de l'enfant peut être fixée en alternance au domicile de chacun des parents ou au domicile de l'un d'eux ". <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 262-2 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un revenu garanti, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu garanti est calculé, pour chaque foyer, en faisant la somme : / 1° D'une fraction des revenus professionnels des membres du foyer ; / 2° D'un montant forfaitaire, dont le niveau varie en fonction de la composition du foyer et du nombre d'enfants à charge (...) ". L'article L. 262-9 du même code, dans sa rédaction applicable au litige, dispose que : " Le montant forfaitaire mentionné au 2° de l'article L. 262-2 est majoré, pendant une période d'une durée déterminée, pour : / 1° Une personne isolée assumant la charge d'un ou de plusieurs enfants ; / (...) / Est considérée comme isolée une personne veuve, divorcée, séparée ou célibataire, qui ne vit pas en couple de manière notoire et permanente (...) ". Aux termes de l'article R. 262-3 de ce code : " Pour le bénéfice du revenu de solidarité active, sont considérés comme à charge : / 1° Les enfants ouvrant droit aux prestations familiales ; / 2° Les autres enfants et personnes de moins de vingt-cinq ans qui sont à la charge effective et permanente du bénéficiaire à condition, lorsqu'ils sont arrivés au foyer après leur dix-septième anniversaire, d'avoir avec le bénéficiaire ou son conjoint, son concubin ou le partenaire lié par un pacte civil de solidarité un lien de parenté jusqu'au quatrième degré inclus (...) ". <br/>
<br/>
              4. Il résulte de ces dispositions que, pour calculer le montant forfaitaire mentionné au 2° de l'article L. 262-2 du code de l'action sociale et des familles, ainsi que pour déterminer le droit d'une personne isolée assumant la charge d'un ou plusieurs enfants à la majoration de ce montant forfaitaire en application de l'article L. 262-9 du même code, doivent être regardés comme à la charge de l'allocataire du revenu de solidarité active les enfants ouvrant droit aux prestations familiales, ainsi que les autres enfants à sa charge effective et permanente, sous réserve des conditions définies au 2° de l'article R. 262-3 du même code. Eu égard à l'objet du revenu de solidarité active, qui est notamment, en vertu de l'article L. 262-1 du même code, d'assurer à ses bénéficiaires des moyens convenables d'existence, en fonction de la composition du foyer et du nombre d'enfants à charge, lorsqu'un parent allocataire du revenu de solidarité active bénéficie pour son enfant, conjointement avec l'autre parent dont il est divorcé ou séparé de droit ou de fait, d'un droit de résidence alternée qui est mis en oeuvre de manière effective et équivalente, ce parent doit être regardé comme assumant la charge effective et permanente de l'enfant et a droit, sauf accord contraire entre les parents ou mention contraire dans une décision du juge judiciaire,  au bénéfice de la moitié de la majoration pour enfant à charge du montant forfaitaire mentionné au 2° de l'article L. 262-2 du code de l'action sociale et des familles et, s'il en remplit les autres conditions, de la moitié de la majoration pour parent isolé mentionnée à l'article L. 262-9 du même code. Toutefois, compte tenu des incidences possibles de ce partage sur les droits de l'autre parent, susceptible de bénéficier lui aussi du revenu de solidarité active, il appartient au parent qui sollicite une telle répartition d'établir l'existence d'une résidence alternée mise en oeuvre de manière effective et équivalente, laquelle doit être présumée s'il fournit à l'organisme chargé du service du revenu de solidarité active, à défaut de partage de la charge de l'enfant pour le calcul des allocations familiales, une convention homologuée par le juge aux affaires familiales, une décision de ce juge ou un document attestant l'accord existant entre les parents sur ce mode de résidence.<br/>
<br/>
              5. Pour annuler la décision de la présidente du conseil de Paris du 21 janvier 2015, le tribunal administratif de Paris s'est fondé sur les déclarations faites par M. A...à l'occasion de ses demandes de bénéficier du revenu de solidarité active et de certaines prestations familiales et sur un document par lequel la mère de l'enfant attestait, en septembre 2010, qu'ils pratiquaient d'un commun accord la résidence alternée, sans précision sur la durée de la résidence au domicile de chacun des parents. Par suite, en retenant que la fille de M. A... devait être prise en compte pour le calcul de ses droits au montant forfaitaire majoré du revenu de solidarité active, sans disposer d'éléments susceptibles d'établir l'existence d'une résidence alternée de leur enfant mise en oeuvre de façon effective et équivalente, et en jugeant que la charge de l'enfant devait être prise en compte " à proportion de sa présence au domicile de l'intéressé ", le tribunal a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que le département de Paris est fondé à demander l'annulation du jugement qu'il attaque. <br/>
<br/>
              7. Dès lors, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de Paris, qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 12 février 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : Les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au département de Paris et à M. B...A....<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - MAJORATION DU MONTANT FORFAITAIRE POUR ENFANT À CHARGE (ART. L. 262-2 DU CASF) ET PARENT ISOLÉ (ART. L. 262-9 DU CASF) - 1) NOTION D'ENFANT À CHARGE - 2) CAS D'ENFANTS EN SITUATION DE GARDE ALTERNÉE - PRISE EN COMPTE - EXISTENCE [RJ1] - MODALITÉS.
</SCT>
<ANA ID="9A"> 04-02-06 1) Pour calculer le montant forfaitaire mentionné au 2° de l'article L. 262-2 du code de l'action sociale et des familles (CASF), ainsi que pour déterminer le droit d'une personne isolée assumant la charge d'un ou de plusieurs enfants à la majoration de ce montant forfaitaire en application de l'article L. 262-9 du même code, doivent être regardés comme à la charge de l'allocataire du revenu de solidarité active (RSA) les enfants ouvrant droit aux prestations familiales, ainsi que les autres enfants à sa charge effective et permanente, sous réserve des conditions définies au 2° de l'article R. 262-3 du même code.... ,,2) Eu égard à l'objet du RSA, qui est notamment, en vertu de l'article L. 262-1 du même code, d'assurer à ses bénéficiaires des moyens convenables d'existence, en fonction de la composition du foyer et du nombre d'enfants à charge, lorsqu'un parent allocataire du RSA bénéficie pour son enfant, conjointement avec l'autre parent dont il est divorcé ou séparé de droit ou de fait, d'un droit de résidence alternée qui est mis en oeuvre de manière effective et équivalente, ce parent doit être regardé comme assumant la charge effective et permanente de l'enfant et a droit, sauf accord contraire entre les parents ou mention contraire dans une décision du juge judiciaire, au bénéfice de la moitié de la majoration pour enfant à charge du montant forfaitaire mentionné au 2° de l'article L. 262-2 du CASF et, s'il en remplit les autres conditions, de la moitié de la majoration pour parent isolé mentionnée à l'article L. 262-9 du même code.... ,,Toutefois, compte tenu des incidences possibles de ce partage sur les droits de l'autre parent, susceptible de bénéficier lui aussi du RSA, il appartient au parent qui sollicite une telle répartition d'établir l'existence d'une résidence alternée mise en oeuvre de manière effective et équivalente, laquelle doit être présumée s'il fournit à l'organisme chargé du service du RSA, à défaut de partage de la charge de l'enfant pour le calcul des allocations familiales, une convention homologuée par le juge aux affaires familiales, une décision de ce juge ou un document attestant de l'accord existant entre les parents sur ce mode de résidence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'aide personnalisée au logement, CE, 21 juillet 2017, Ministre du logement et de l'habitat durable c/,, n° 398563, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
