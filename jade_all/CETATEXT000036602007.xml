<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036602007</ID>
<ANCIEN_ID>JG_L_2018_02_000000414810</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/60/20/CETATEXT000036602007.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 14/02/2018, 414810, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414810</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul-François Schira</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414810.20180214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 3 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 14 septembre 2017 par laquelle la présidente de la Commission nationale de l'informatique et des libertés (CNIL) l'a informé de la clôture de sa plainte relative aux difficultés rencontrées dans l'exercice de son droit d'accès à l'enregistrement de la conversation qu'il a eue avec le maire de Savigny-sur-Orge (Essonne) lors de la permanence téléphonique du 6 avril 2016 ;  <br/>
<br/>
              2°) d'enjoindre à la commune de Savigny-sur-Orge de se justifier devant la CNIL quant aux propos tenus par le maire dans l'enregistrement produit à l'appui de la requête ; <br/>
<br/>
              3°) d'enjoindre à la CNIL de reprendre l'instruction de la plainte au regard des nouveaux éléments de réponses apportés par la commune ou, à défaut, de lui adresser un nouveau rappel à la loi par rapport à ses obligations.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code pénal ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le code de justice administrative, notamment son article R. 611-8 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul-François Schira, auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 janvier 2018, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que M. B...a saisi la Commission nationale de l'informatique et des libertés (CNIL) d'une plainte relative aux difficultés rencontrées dans l'exercice de son droit d'accès à l'enregistrement de la conversation qu'il a eue avec le maire de Savigny-sur-Orge lors de la permanence téléphonique du 6 avril 2016. Par courrier du 19 avril 2017, la présidente de la CNIL a informé M. B...que ses services avaient procédé à un rappel des obligations en la matière auprès du responsable de traitement, que celui-ci avait indiqué que la conversation téléphonique du 6 avril 2016 n'avait fait l'objet d'aucun enregistrement, et que la commune de Savigny-sur-Orge n'avait installé aucun dispositif d'écoute et d'enregistrement des conversations téléphoniques. Par un courrier du 14 septembre 2017, la présidente de la CNIL a informé l'intéressé qu'elle avait procédé à la clôture de sa plainte. M. B... demande l'annulation pour excès de pouvoir de cette décision.<br/>
<br/>
              2. Aux termes de l'article 11 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " La Commission nationale de l'informatique et des libertés est une autorité administrative indépendante. Elle exerce les missions suivantes : (...) 2° Elle veille à ce que les traitements de données à caractère personnel soient mis en oeuvre conformément aux dispositions de la présente loi. / A ce titre : (...) c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en oeuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci (...) ". Aux termes de l'article 39 de la même loi : " I.-Toute personne physique justifiant de son identité a le droit d'interroger le responsable d'un traitement de données à caractère personnel en vue d'obtenir : (...) 4° La communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci ".<br/>
<br/>
              3. Il ressort des pièces du dossier qu'en estimant, au vu des réponses que lui avait fournies la commune de Savigny-sur-Orge et en dépit de la production par M. B...de la version dématérialisée, au demeurant non authentifiée, d'un enregistrement audio d'une conversation qu'il décrit comme étant un entretien téléphonique avec le maire de Savigny-sur-Orge lors de la permanence téléphonique du 6 avril 2016, que l'instruction de la plainte ne pouvait plus utilement être poursuivie et qu'il y avait lieu de procéder à la clôture du dossier, la CNIL n'a entaché sa décision ni d'erreur manifeste d'appréciation ni de méconnaissance de l'article 39 de la loi du 6 janvier 1978 et de l'article L. 311-6 du code des relations entre le public et l'administration. <br/>
<br/>
              4. Il résulte de ce qui précède que la requête de M. B...doit être rejetée, y compris ses conclusions aux fins d'injonction.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.    <br/>
Article 2 : La présente décision sera notifiée à M. A...B....<br/>
Copie en sera adressée pour information à la Commission nationale de l'informatique et des libertés et à la commune de Savigny-sur-Orge.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
