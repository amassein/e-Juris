<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410956</ID>
<ANCIEN_ID>JG_L_2013_05_000000358856</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/09/CETATEXT000027410956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 15/05/2013, 358856, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358856</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:358856.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 1106483/5-1 du 19 avril 2012, enregistrée le 25 avril 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par le syndicat national Force ouvrière des personnels de préfecture (SNFOPP) ;<br/>
<br/>
              Vu la requête, enregistrée le 4 avril 2011 au greffe du tribunal administratif de Paris, présentée par le SNFOPP, dont le siège est 8, rue de Penthièvre à Paris (75008) ; le SNFOPP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 27 janvier 2011 par laquelle le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration a rejeté sa demande tendant à l'abrogation des circulaires n° 268 à 279 du 21 juillet 2010 fixant le régime indemnitaire des fonctionnaires relevant de son département ministériel pour l'année 2010 ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration d'abroger ces circulaires, dans le délai d'un mois à compter de la présente décision ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration de fixer de nouveaux montants de " taux moyens d'objectifs " pour l'année 2010, qui ne portent pas atteinte au principe d'égalité, dans un délai de quatre mois à compter de la présente décision ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 50-196 du 6 février 1950 ;<br/>
<br/>
              Vu le décret n° 97-1223 du 26 décembre 1997 ;<br/>
<br/>
              Vu le décret n° 98-1235 du 29 décembre 1998 ;<br/>
<br/>
              Vu le décret n° 2002-61 du 14 janvier 2002 ;<br/>
<br/>
              Vu le décret n° 2002-62 du 14 janvier 2002 ;<br/>
<br/>
              Vu le décret n° 2002-63 du 14 janvier 2002 ;<br/>
<br/>
              Vu le décret n° 2002-1105 du 30 août 2002 ;<br/>
<br/>
              Vu le décret n° 2002-1247 du 4 octobre 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 20 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Les fonctionnaires ont droit, après service fait, à une rémunération comprenant [...] les indemnités instituées par un texte législatif ou réglementaire [...]. " ;<br/>
<br/>
              2. Considérant que, par douze circulaires n° 268 à 279 du 21 juillet 2010, le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration a porté à la connaissance des chefs de services du ministère le niveau et les règles de modulation des " taux moyens d'objectifs " applicables en 2010 pour l'attribution aux fonctionnaires relevant de son département ministériel des indemnités auxquels ils sont éligibles ; que le syndicat national Force ouvrière des personnels de préfecture (SNFOPP) demande l'annulation pour excès de pouvoir de ces circulaires ;<br/>
<br/>
              3. Considérant que les circulaires attaquées, qui sont rédigées de façon similaire, fixent, pour les différentes catégories d'agents du ministère de l'intérieur, selon les fonctions qu'ils exercent et leur affectation géographique, le taux de progression annuel et le niveau du taux moyen d'objectifs, lequel ne constitue qu'une valeur de référence pour la détermination du montant global des indemnités auxquelles ils sont éligibles, " recommandent " de ne pas moduler ce taux moyen d'objectifs en-deçà de 90% pour les agents de catégories B et C et de 80 % pour les agents de catégorie A et rappellent les orientations générales à respecter pour l'attribution des indemnités individuelles, telle que l'organisation d'un entretien individuel avec tout agent dont les indemnités ont baissé entre 2009 et 2010 ;<br/>
<br/>
              4. Considérant, en premier lieu, d'une part, qu'il résulte de ce qui précède que les dispositions des circulaires attaquées n'ont, pour les chefs de service auxquelles elles sont destinées, qu'une valeur indicative en vue de faciliter l'attribution des différentes indemnités dont ils assurent le paiement et ne peuvent, dès lors, avoir pour effet de leur permettre d'arrêter les montants individuels des indemnités selon des critères autres que ceux prévus par les différents décrets indemnitaires qui les ont instituées et en dehors des montants de référence fixés par les arrêtés interministériels pris pour leur application et, d'autre part, que ni l'article 20 précité de la loi du 13 juillet 1983, ni les décrets indemnitaires, ni aucun autre texte ne faisaient obstacle à ce que le ministre, en vertu de son pouvoir d'organisation du service, rappelât, par voie de circulaire, les orientations générales devant être respectées pour l'attribution de ces indemnités ; que, par suite, le moyen tiré de ce que le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration n'était pas compétent pour édicter les circulaires attaquées doit être écarté ;<br/>
<br/>
              5. Considérant, en second lieu, que, si les circulaires attaquées fixent un taux de progression annuel et un niveau de taux moyen d'objectifs qui varient selon les fonctions et l'affectation géographique des agents, ces dispositions n'ont, ainsi qu'il a été dit au point 4, qu'une valeur indicative pour les chefs de service auxquelles elles sont destinées et ne peuvent, dès lors, avoir davantage pour effet de leur permettre de moduler les montants individuels des indemnités attribuées aux agents selon des critères qui n'auraient pas été prévus par les différents décrets indemnitaires ; que, par suite, le moyen tiré de ce que les circulaires attaquées méconnaissent le principe d'égalité entre agents d'un même corps ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par le ministre, que le SNFOPP n'est pas fondé à demander l'annulation des circulaires attaquées ; que, par voie de conséquence, ses conclusions à fin d'injonction doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du SNFOPP est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat national Force Ouvrière des personnels de préfecture et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
