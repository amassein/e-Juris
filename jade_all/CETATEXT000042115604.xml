<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115604</ID>
<ANCIEN_ID>JG_L_2020_07_000000425463</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/56/CETATEXT000042115604.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 03/07/2020, 425463, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425463</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:425463.20200703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Dijon d'annuler la décision implicite par laquelle le maire de Montceau-les-Mines a rejeté la demande de versement d'allocation d'aide au retour à l'emploi qu'elle lui avait adressée le 3 mai 2016 et d'enjoindre à cette autorité de la "restaurer rétroactivement dans ses droits" dans un délai de quinze jours ou, subsidiairement, de prendre une nouvelle décision dans le même délai. <br/>
<br/>
              Par un jugement n° 1602067 du 3 mai 2018, le tribunal administratif de Dijon a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 18LY02583 du 17 septembre 2018, le président de la troisième chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 19 novembre 2018 et le 19 février 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Montceau-les-Mines la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
              - le code du travail ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... B... a été engagée à partir du 27 août 2012 par la commune de Montceau-les-Mines pour exercer, à temps partiel puis à temps complet, par quatre contrats à durée déterminée successifs, les fonctions d'agent territorial spécialisé des écoles maternelles (ATSEM). Avant l'expiration de son dernier contrat, Mme B... a présenté sa candidature au poste d'adjoint d'animation au cours du mois de juillet 2015. Par un courrier du 21 août 2015, le maire de la commune l'a informée que sa candidature avait été retenue et lui a proposé un contrat à durée déterminée, allant du 1er septembre 2015 au 31 août 2016, à temps non complet.  Par un courrier du 2 septembre 2015, Mme B... a refusé cette proposition au motif que la durée hebdomadaire de travail prévue par ce nouveau contrat était de 31 heures. Par un courrier du 3 mai 2016 elle a demandé au maire de la commune le bénéfice de l'allocation d'aide au retour à l'emploi. Mme B... a demandé au tribunal administratif de Dijon d'annuler la décision implicite par laquelle le maire de Montceau-les-Mines a rejeté la demande de versement d'allocation d'aide au retour à l'emploi qu'elle lui avait adressée le 3 mai 2016 et d'enjoindre à cette autorité de la " restaurer rétroactivement dans ses droits " dans un délai de quinze jours ou, subsidiairement, de prendre une nouvelle décision dans le même délai. Par un jugement du 3 mai 2018, le tribunal administratif de Dijon a rejeté sa demande. Par une ordonnance du 17 septembre 2018, le président de la troisième chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme B... contre ce jugement. Mme B... se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. Aux termes du 1° de l'article R. 811-1 du code de justice administrative, le tribunal administratif statue en premier et dernier ressort " sur les litiges relatifs aux prestations, allocations ou droits attribués (...) en faveur des travailleurs privés d'emploi, mentionnés à l'article R. 772-5 (...) ". <br/>
<br/>
              3. L'allocation d'aide au retour à l'emploi constitue une allocation en faveur des travailleurs privés d'emploi au sens du 1° de l'article R. 811-1 du code de justice administrative. Le tribunal administratif statue donc en premier et dernier ressort sur les litiges relatifs au versement de cette allocation. Le président de la troisième chambre de la cour administrative d'appel de Lyon était donc incompétent pour statuer sur les conclusions de Mme B... tendant à l'annulation de la décision implicite par laquelle le maire de Montceau-les-Mines a rejeté la demande de versement d'allocation d'aide au retour à l'emploi qu'elle lui avait adressée le 3 mai 2016. Dès lors, son ordonnance doit être annulée. <br/>
<br/>
              4. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". Cette procédure est applicable à tout pourvoi en cassation dont le Conseil d'Etat est saisi. Elle est, par suite, applicable aux conclusions contre un jugement ayant statué en premier et dernier ressort sur lesquelles une cour administrative d'appel a statué et qui doivent être regardées, après l'annulation de l'arrêt de la cour, comme des conclusions de cassation.<br/>
<br/>
              5. Les conclusions que Mme B... a présentées devant la cour administrative d'appel de Lyon et qui sont dirigées contre le jugement n° 1602067 du tribunal administratif de Dijon du 3 mai 2018, statuant en premier et dernier ressort, constituent un pourvoi en cassation. Pour demander l'annulation du jugement, Mme B... soutient, dans le dernier état de ses écritures de cassation, que le tribunal administratif de Dijon : <br/>
<br/>
              - a dénaturé les faits qui lui étaient soumis en estimant que le nouveau contrat qui lui était proposé entrainait une diminution de son temps de travail de quatre heures hebdomadaires, alors qu'il entrainait une diminution de sept heures ; <br/>
              - a dénaturé les faits qui lui étaient soumis en estimant qu'il ne ressortait pas des pièces du dossier que la diminution de quatre heures du temps de travail hebdomadaire prévue par le contrat proposé par la commune relatif au poste d'adjoint d'animation aurait entrainé une perte substantielle de sa rémunération, alors que le contrat proposé par la commune retenait un indice de rémunération supérieur à celui dont elle bénéficiait antérieurement. <br/>
<br/>
              6. Eu égard aux moyens soulevés ou susceptibles d'être relevés d'office, il y a lieu d'admettre les conclusions de ce pourvoi. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 18LY02583 du 17 septembre 2018 du président de la troisième chambre de la cour administrative d'appel de Lyon est annulée.   <br/>
<br/>
Article 2 : Le pourvoi de Mme B... dirigé contre le jugement n° 1602067 du tribunal administratif de Dijon du 3 mai 2018 est admis. <br/>
Article 3 : La présente décision sera notifiée à Mme A... B... et à la commune de Montceau-les-Mines.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
