<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028110464</ID>
<ANCIEN_ID>JG_L_2013_10_000000354778</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/04/CETATEXT000028110464.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 23/10/2013, 354778, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354778</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354778.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 12 décembre 2011 et 12 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération des entreprises de boulangeries et pâtisseries françaises, dont le siège est 34 quai de Loire à Paris (75019), représentée par son président, et pour la société La Panetière du Rouergue, dont le siège est au Causseroux à Villefranche-de-Rouergue (12200), représentée par son président-directeur général ; les requérantes demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre du travail, de l'emploi et de la santé a rejeté leur demande tendant à l'abrogation de l'arrêté du 28 décembre 1988 par lequel le préfet de la Haute-Garonne a ordonné la fermeture hebdomadaire des boulangeries, boulangeries-pâtisseries et dépôts de pain du département ; <br/>
<br/>
              2°) d'enjoindre au ministre chargé du travail de procéder à l'abrogation de l'arrêté litigieux dans un délai d'un mois à compter de la notification de la décision à intervenir ou, à titre subsidiaire, de procéder à une consultation de l'ensemble des professionnels intéressés dans un délai de trois mois, dans les deux cas sous astreinte de 5 000 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la Fédération des entreprises de boulangeries et pâtisseries française et de la société La Panetière du Rouergue ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 3132-29 du code du travail, reprenant les dispositions du premier alinéa de l'ancien article L. 221-17 : " Lorsqu'un accord est intervenu entre les organisations syndicales de salariés et les organisations d'employeurs d'une profession et d'une zone géographique déterminées sur les conditions dans lesquelles le repos hebdomadaire est donné aux salariés, le préfet peut, par arrêté, sur la demande des syndicats intéressés, ordonner la fermeture au public des établissements de la profession ou de la zone géographique concernée pendant toute la durée de ce repos. (...) " ; que la fermeture au public des établissements d'une profession ne peut légalement être ordonnée sur la base d'un accord syndical que dans la mesure où cet accord correspond pour la profession à la volonté de la majorité indiscutable de tous ceux qui exercent cette profession à titre principal ou accessoire et dont l'établissement ou partie de celui-ci est susceptible d'être fermé ; qu'aux termes de l'article R. 3132-22 du même code : " Lorsqu'un arrêté préfectoral de fermeture au public, pris en application de l'article L. 3132-29, concerne des établissements concourant d'une façon directe à l'approvisionnement de la population en denrées alimentaires, il peut être abrogé ou modifié par le ministre chargé du travail après consultation des organisations professionnelles intéressées. / Cette décision ne peut intervenir qu'après l'expiration d'un délai de six mois à compter de la mise en application de l'arrêté préfectoral " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que le préfet de la Haute-Garonne a, par un arrêté du 28 décembre 1988 intervenu à la suite d'un accord conclu le 21 novembre 1988 entre la chambre patronale de la boulangerie et de la boulangerie-pâtisserie de ce département et des organisations syndicales de salariés concernées, prescrit la fermeture, un jour par semaine, des boulangeries, boulangeries-pâtisseries et dépôts de pain du département ; que la Fédération des entreprises de boulangeries et pâtisseries françaises et la société La Panetière du Rouergue demandent l'annulation de la décision implicite par laquelle le ministre du travail, de l'emploi et de la santé a refusé d'abroger cet arrêté ;<br/>
<br/>
              3. Considérant que l'autorité compétente, saisie d'une demande tendant à  l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce  règlement ait été illégal dès sa signature, soit que l'illégalité résulte  de circonstances de droit ou de fait postérieures à cette date ;<br/>
<br/>
              4. Considérant, en premier lieu, que la circonstance, invoquée par les requérantes, que l'arrêté litigieux aurait été pris sans que le préfet ait préalablement recueilli leur propre avis, ni celui de la Fédération des entreprises du commerce et de la distribution, ni d'aucun syndicat représentant la boulangerie industrielle, à la supposer avérée, n'est par elle-même pas de nature à établir que l'accord du 21 novembre 1988 ne correspondait pas, à la date de cet arrêté, à la volonté de la majorité indiscutable des établissements concernés ; qu'il en est de même de la circonstance que le préfet n'aurait pas réalisé une enquête auprès des professionnels non syndiqués ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que, contrairement à ce que soutiennent la Fédération des entreprises de boulangeries et pâtisseries françaises et la société La Panetière du Rouergue, l'arrêté, qui délimite son champ professionnel en des termes généraux, s'applique à tous les établissements et, le cas échéant, parties d'établissement du département pratiquant une activité de boulangerie, de boulangerie-pâtisserie ou de dépôt de pain à titre principal ou accessoire ; que, par suite, le moyen tiré de ce que le préfet, en cantonnant le champ de son arrêté aux seuls établissements exerçant une telle activité à titre principal, aurait fait une inexacte application des dispositions de l'article L. 3132-29, eu égard à l'objectif de préservation des conditions du libre jeu de la concurrence entre établissements exerçant une même profession qu'elles poursuivent, manque en fait ;<br/>
<br/>
              6. Considérant, en troisième et dernier lieu, que si les requérantes affirment, sans d'ailleurs apporter la moindre précision à l'appui de leurs allégations, que l'organisation d'employeurs signataire de l'accord du 28 décembre 1988 serait désormais " faiblement représentative ", et si elles indiquent que les boulangeries artisanales sont aujourd'hui minoritaires dans le département, ces circonstances ne sont, en tout état de cause, pas par elles-mêmes de nature à établir que se serait produit dans l'opinion d'un nombre important des établissements intéressés un changement  susceptible de modifier la volonté de la majorité d'entre eux ; que le moyen tiré de ce que le ministre ne pouvait régulièrement refuser de faire droit à la demande d'abrogation sans avoir préalablement procédé à une nouvelle consultation des organisations professionnelles intéressées, afin de s'assurer que l'arrêté litigieux correspondait encore à la volonté de la majorité indiscutable des établissements proposant du pain à la vente dans le département, doit, dès lors, être écarté ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la Fédération des entreprises de boulangeries et pâtisseries françaises et la société La Panetière du Rouergue ne sont pas fondées à demander l'annulation de la décision qu'elles attaquent ; que leur requête doit, dès lors, être rejetée, y compris, par voie de conséquence, leurs conclusions à fin d'injonction et d'astreinte ainsi que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération des entreprises de boulangeries et pâtisseries françaises et de la société La Panetière du Rouergue est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération des entreprises de boulangeries et pâtisseries françaises, à la société La panetière du Rouergue, à la Chambre patronale de la boulangerie et de la boulangerie-pâtisserie de la Haute-Garonne et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée pour information à l'Union départementale CFTC de Haute-Garonne, à l'Union départementale CGC de Haute-Garonne et à l'Union départementale FO de Haute-Garonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
