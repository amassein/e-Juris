<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038755642</ID>
<ANCIEN_ID>JG_L_2019_07_000000413588</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/75/56/CETATEXT000038755642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 11/07/2019, 413588, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413588</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:413588.20190711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) MBDA France a demandé au tribunal administratif de Cergy-Pontoise de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties et de taxe d'enlèvement des ordures ménagères auxquelles la société PAPSO V, aux droits de laquelle elle vient, a été assujettie au titre des années 2008 et 2009 à raison d'un ensemble immobilier situé sur le territoire de la commune du Plessis-Robinson (Hauts-de-Seine). Par un jugement no 1405898 du 22 juin 2017, le tribunal a dit n'y avoir lieu à statuer sur les conclusions aux fins de réduction des cotisations de taxe foncière au titre de l'année 2009 à hauteur de 289 933 euros et a fait partiellement droit au surplus des conclusions de sa demande. <br/>
<br/>
              Par un pourvoi enregistré le 22 août 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler les articles 2 et 3 de ce jugement.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société MBDA France ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1494 du code général des impôts : " La valeur locative des biens passibles de la taxe foncière sur les propriétés bâties (...) est déterminée, conformément aux règles définies par les articles 1495 à 1508, pour chaque propriété ou fraction de propriété normalement destinée à une utilisation distincte. ". Aux termes de l'article 1498 du même code : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : (...) 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; b. La valeur locative des termes de comparaison est arrêtée : Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe. ". Enfin, aux termes de l'article 324 AA, alors en vigueur, de l'annexe III au même code : " La valeur locative cadastrale des biens loués à des conditions anormales ou occupés par leur propriétaire, occupés par un tiers à un titre autre que celui de locataire, vacants ou concédés à titre gratuit est obtenue en appliquant aux données relatives à leur consistance - telles que superficie réelle, nombre d'éléments - les valeurs unitaires arrêtées pour le type de la catégorie correspondante. Cette valeur est ensuite ajustée pour tenir compte des différences qui peuvent exister entre le type considéré et l'immeuble à évaluer, notamment du point de vue de la situation, de la nature de la construction, de son état d'entretien, de son aménagement, ainsi que de l'importance plus ou moins grande de ses dépendances bâties et non bâties si ces éléments n'ont pas été pris en considération lors de l'appréciation de la consistance. ". Il résulte de ces dispositions combinées que, lorsque la valeur d'une propriété ou d'une fraction de propriété normalement destinée à une utilisation distincte fait l'objet d'une évaluation par comparaison,  l'appréciation de sa consistance par référence à sa superficie peut faire l'objet d'une pondération de la surface des différents éléments qui la composent, afin de tenir compte des différences de commercialité de ceux-ci en fonction de leur affectation et de leur emplacement au sein du local.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la surface des places de stationnement de l'ensemble immobilier en cause prise en compte par le tribunal administratif pour la détermination de sa valeur locative correspondait au nombre de places de stationnement multiplié par la surface de chacune de ces places, soit 25 560 m². Dès lors que cette surface n'était pas composée d'éléments différents et était dévolue à une affectation unique, il n'y avait pas lieu, pour la comparer avec un local-type qui était lui-même à usage de parking, de la pondérer. Par suite, en appliquant un coefficient de pondération de 0,2 à la surface des places de stationnement, le tribunal a commis une erreur de droit. Il en résulte que le ministre de l'action et des comptes publics est fondé, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation des articles 2 et 3 du jugement qu'il attaque.<br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 3 du jugement du 22 juin 2017 du tribunal administratif de Cergy-Pontoise sont annulés.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 3 : Les conclusions présentées par la société MBDA France au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la SAS MBDA France. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
