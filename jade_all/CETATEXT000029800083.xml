<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800083</ID>
<ANCIEN_ID>JG_L_2014_10_000000357393</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/00/CETATEXT000029800083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 15/10/2014, 357393, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357393</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357393.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 mars et 6 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association des riverains de la Baie de Sainte-Marie, dont le siège est situé 11 bis, rue Henri Guilbaud à Nouméa (98800), représentée par son président ; l'association requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA02629 du 1er décembre 2011 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0700278 du 26 février 2009 par lequel le tribunal administratif de Nouvelle-Calédonie a rejeté sa demande d'annulation du permis de construire n° 2007-1042 délivré le 30 août 2007 par le maire de Nouméa à la société Magenta Développement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
               3°) de mettre à la charge de la commune de Nouméa et de la société Magenta Développement la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi organique n° 99-209 et la loi n° 99-210 du 19 mars 1999 relatives à la Nouvelle-Calédonie ;<br/>
<br/>
              Vu la loi du 16 septembre 1807 relative au desséchement des marais ;<br/>
<br/>
              Vu le décret du 17 janvier 1908 relatif au régime domanial en Nouvelle-Calédonie ; <br/>
<br/>
              Vu la délibération n° 74 des 10 et 11 mars 1959 de l'assemblée territoriale de Nouvelle-Calédonie, portant réglementation de l'urbanisme en Nouvelle-Calédonie, modifiée notamment par la délibération n° 21-2003/APS du 18 juillet 2003 ;<br/>
<br/>
              Vu la délibération n° 19 du 8 juin 1973 relative au permis de construire dans la province Sud, modifiée notamment par la délibération n° 6-2011/APS du 17 mars 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association des riverains de la Baie de Sainte-Marie, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la commune de Nouméa et  la SCP Meier-Bourdeau, Lecuyer, avocat de la société Magenta Développement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté n° 2007-1042 du 30 août 2007, le maire de Nouméa a autorisé la société Magenta Développement à construire un immeuble à usage d'habitation ; que, par un jugement du 26 février 2009, le tribunal administratif de Nouvelle-Calédonie a rejeté la demande de l'association des riverains de la Baie de Sainte-Marie tendant à l'annulation de ce permis de construire ; qu'après avoir, par un arrêt avant dire droit du 19 mai 2011, sursis à statuer et ordonné une mesure d'instruction, la cour administrative d'appel de Paris a, par un arrêt du 1er décembre 2011, rejeté l'appel formé contre ce jugement  par l'association ; que l'association des riverains de la Baie de Sainte-Marie se pourvoit en cassation contre ce dernier arrêt ;<br/>
<br/>
              2. Considérant, en premier lieu, que l'association requérante soutient que l'arrêt attaqué est entaché d'irrégularité en ce que la cour administrative d'appel de Paris, d'une part, a visé sans l'analyser la note en délibéré produite le 16 mai 2011 par les sociétés défenderesses dans le cadre de l'instance ayant donné lieu à l'arrêt avant dire droit du 19 mai 2011 et, d'autre part, elle n'a, par la suite, ni rouvert l'instruction ni communiqué cette note ; que, toutefois, l'association requérante ne peut utilement contester la régularité de l'arrêt avant dire droit du 19 mai 2011 qui n'a pas fait l'objet d'un pourvoi et est ainsi devenu irrévocable ; que, s'agissant de l'arrêt statuant au fond sur l'affaire, d'une part, il ressort des pièces du dossier que la cour a, contrairement à ce qui est soutenu, rouvert l'instruction à la suite de l'arrêt avant dire droit ; que, d'autre part, lorsqu'une note en délibéré est produite entre le prononcé des conclusions du rapporteur public et la décision avant dire droit, le juge n'est tenu, dès lors qu'il ne se fonde pas sur des éléments contenus dans cette seule note, ni de l'analyser ni de la soumettre au débat contradictoire avant de régler l'affaire au fond ; que, par suite, l'association requérante n'est pas fondée à soutenir que la cour aurait entaché l'arrêt attaqué d'irrégularité en s'abstenant d'analyser et de lui communiquer la note en délibéré produite le 16 mai 2011, qui ne comportait aucun élément nécessaire à la solution des points restant en litige ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que la cour, qui a définitivement rejeté, dans son arrêt avant dire droit, les conclusions à fin de non-lieu présentées par l'association des riverains de la Baie de Sainte-Marie, fondées sur la caducité du permis de construire litigieux, s'est estimée saisie, dans le cadre du règlement au fond, d'un moyen tiré de l'illégalité de ce permis du fait de sa caducité, en raison de l'absence de commencement d'exécution des travaux dans un délai de vingt-quatre mois ; que l'association requérante soutient qu'elle a commis une erreur de droit en faisant application, pour écarter ce moyen, d'une délibération de l'assemblée de la province Sud entachée d'une rétroactivité illégale ; que, cependant, la circonstance qu'un permis de construire soit devenu caduc en raison de l'absence de commencement d'exécution des travaux est sans incidence sur sa légalité ; qu'ainsi, le moyen soulevé devant les juges du fond était inopérant ; qu'il convient de l'écarter pour ce motif, qui doit être substitué au motif retenu par les juges du fond ; <br/>
<br/>
              4. Considérant, en troisième lieu, que, contrairement à ce que soutient l'association requérante, la cour a, par une appréciation souveraine exempte de dénaturation, jugé que le lot n° 73, qui constituait la parcelle d'assiette de la construction autorisée par le permis litigieux, correspondait à des terrains exondés en vertu de la concession d'endigage conclue le 17 novembre 1975 entre l'Etat et la société Magenta Plage ; que si l'association requérante soutient également que la cour a commis une erreur de droit en omettant de rechercher l'origine et la situation juridique de la parcelle anciennement dénommée L 4, dont est issu le lot n° 73, elle ne peut utilement invoquer pour la première fois devant le juge de cassation ce moyen, qui n'est pas d'ordre public et n'est pas né de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant, en quatrième lieu, que le principe, énoncé à l'article 41 de la loi du 16 septembre 1807 relative au dessèchement des marais et ultérieurement codifié à l'article L. 64 du code du domaine de l'Etat, selon lequel l'Etat peut concéder le droit d'endigage sur le domaine public maritime était applicable en Nouvelle-Calédonie ; que, dès lors, la cour n'a pas commis d'erreur de droit en jugeant que le terrain d'assiette du projet litigieux avait pu faire l'objet de la concession d'endigage conclue entre l'Etat et la société Magenta Plage le 17 novembre 1975 ; <br/>
<br/>
              6. Considérant que les dispositions du décret du 17 janvier 1908 relatif au régime domanial en Nouvelle-Calédonie ne concernent pas le domaine public maritime de Nouvelle-Calédonie ; que, dès lors, l'association requérante ne pouvait utilement se prévaloir de ces dispositions pour soutenir que la décision de concession aurait dû être prise pour le compte de l'Etat non par le haut-commissaire mais par le gouvernement ou le ministre ; qu'il convient d'écarter le moyen tiré de l'incompétence du haut-commissaire pour ce motif, qui doit être substitué au motif retenu par la cour ; que, par suite, l'association requérante n'est, en tout état de cause, pas fondée à soutenir que la cour, qui a suffisamment motivé son arrêt sur ce point, aurait commis une erreur de droit en jugeant que le haut-commissaire avait compétence pour consentir à la société Magenta Plage une concession à charge d'endigage ;<br/>
<br/>
              7. Considérant que la cour, pour écarter le moyen tiré de l'incompétence du haut-commissaire, ne s'est pas fondée sur la circulaire du 3 janvier 1973 relative à l'utilisation du domaine public maritime en dehors des ports de commerce et de pêche ; que le moyen tiré de ce que la concession litigieuse méconnaîtrait les termes de cette circulaire n'a pas été invoqué devant la cour administrative d'appel de Paris, n'est pas né de l'arrêt attaqué et n'est pas d'ordre public ; qu'il ne peut, dès lors, être utilement soulevé pour contester le bien-fondé de cet arrêt ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la cour n'a, en tout état de cause, pas commis d'erreur de droit en jugeant que la concession d'endigage en vertu de laquelle le terrain d'assiette du projet avait cessé d'appartenir au domaine public maritime n'avait pas été irrégulièrement accordée ;<br/>
<br/>
              9. Considérant, en cinquième lieu, qu'il résulte de l'article 9 de la délibération n° 74 des 10 et 11 mars 1959 modifiée portant réglementation de l'urbanisme en Nouvelle-Calédonie qu'à compter de la publication de l'acte prescrivant la révision d'un plan d'urbanisme et jusqu'à l'approbation du projet de révision, une autorisation d'utilisation du sol peut être refusée ou faire l'objet d'un sursis à statuer lorsque l'opération est de nature à compromettre ou à rendre plus onéreuse ou difficile la mise en oeuvre des objectifs poursuivis par ce plan ; qu'en tenant compte, pour écarter le moyen tiré de ce que le maire de Nouméa aurait commis une erreur manifeste d'appréciation en ne sursoyant pas à statuer sur la demande de permis, de l'état d'avancement des travaux de révision du plan d'urbanisme directeur, la cour n'a pas commis d'erreur de droit ; que, contrairement à ce qui est soutenu, la cour n'a pas ajouté une condition tenant à l'existence de règles précises applicables à la zone concernée mais s'est bornée, pour écarter le moyen d'erreur manifeste d'appréciation soulevée devant elle, à relever qu'en l'absence de règles spécifiques à la zone concernée, l'état d'avancement des travaux de révision du plan d'urbanisme directeur ne lui permettait pas de caractériser une telle erreur ; qu'elle a, ce faisant, porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              10. Considérant, en sixième lieu, qu'il résulte de l'article 18 de la délibération n° 19 du 8 juin 1973 relative au permis de construire dans la province Sud que le permis peut être refusé si les conditions de desserte ne répondent pas à l'importance et à la destination de l'immeuble ; qu'en l'espèce, la cour a jugé, par une appréciation souveraine exempte de dénaturation, que la parcelle était accessible par le rond-point du 18 juin et que les conditions d'accès au terrain d'assiette étaient suffisantes au regard du trafic généré par les constructions projetées ; qu'elle n'a pas davantage dénaturé les pièces du dossier en jugeant que le terrain d'assiette du permis litigieux n'était pas enclavé et que le moyen tiré de la méconnaissance des dispositions de l'article UB 3 du règlement, qui interdisent toute construction sur un terrain enclavé, manquait en fait ;<br/>
<br/>
              11. Considérant, en septième lieu, qu'aux termes de l'article UB 4 du règlement du plan d'urbanisme directeur de Nouméa : " Toute construction doit être raccordée au réseau public d'assainissement par l'intermédiaire de dispositifs appropriés (...). Dans l'attente du raccordement au réseau collectif, seul est autorisé un dispositif d'assainissement individuel (...) " ; qu'aux termes de l'article 21 de la délibération n° 19 du 8 juin 1973 de l'assemblée de la province Sud : " L'alimentation en eau potable et l'assainissement de toute construction à usage d'habitation (...) doivent être assurés dans les conditions conformes aux règlements en vigueur et aux prévisions des projets d'alimentation en eau potable et d'assainissement (...) " ; <br/>
<br/>
              12. Considérant que ces dispositions se bornent à imposer l'existence d'un raccordement au réseau public d'assainissement ou, à défaut, d'un dispositif d'assainissement individuel ; que l'association requérante ne pouvait, dès lors, utilement soutenir que la capacité du réseau public d'assainissement de Magenta ne serait pas suffisante pour accueillir les eaux usées issues de la construction envisagée ; que le moyen tiré de ce que la cour aurait dénaturé les pièces du dossier en écartant le moyen tiré de l'insuffisance du dispositif d'assainissement au regard des dispositions de l'article UB 4 du règlement du plan d'urbanisme directeur de Nouméa ne peut, par suite, qu'être écarté ;<br/>
<br/>
              13. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'association requérante développait, à l'appui du moyen tiré de ce que le permis de construire litigieux méconnaîtrait le principe de prévention résultant de l'article 3 de la Charte de l'environnement, une argumentation analogue à celle développée à l'appui du moyen tiré de la méconnaissance des articles UB 4 du règlement du plan d'urbanisme directeur de Nouméa et 21 de la délibération du 8 juin 1973 ; qu'il ressort des énonciations de l'arrêt attaqué que la cour, pour écarter le moyen tiré de la violation de ces deux derniers articles, a relevé qu'il ne ressortait pas des pièces du dossier que le réseau public d'assainissement de Magenta, dont le renforcement des capacités était prévu, serait saturé ni que la construction envisagée entraînerait des rejets dans le milieu naturel ; que si elle n'a pas ensuite expressément repris ces éléments pour répondre au moyen tiré de la violation du principe de prévention, qu'elle a, par une erreur de plume, désigné comme le principe de " précaution ", son arrêt ne saurait être regardé comme entaché, pour ce motif, d'une insuffisance de motivation ; <br/>
<br/>
              14. Considérant, en dernier lieu, qu'aux termes de l'article UB 11 du règlement du plan d'urbanisme directeur de Nouméa : " les constructions doivent présenter un aspect compatible (architecture, couleur,...) avec le caractère ou l'intérêt des lieux avoisinants, du site et des paysages " ; que, compte tenu notamment du traitement architectural et paysager de la construction envisagée et de l'aspect des autres constructions environnantes, la cour a relevé que le maire de Nouméa n'avait pas commis d'erreur d'appréciation dans l'application de ces dispositions ; qu'elle a, ce faisant, porté une appréciation souveraine qui, dès lors qu'elle est exempte de dénaturation, ne saurait être discutée devant le juge de cassation ;<br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que l'association des riverains de la Baie de Sainte-Marie n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'elle attaque ; <br/>
<br/>
              16. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Nouméa et de la société Magenta Développement qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association des riverains de la Baie de Sainte-Marie une somme de 1 500 euros à verser à la commune de Nouméa au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association des riverains de la Baie de Sainte-Marie est rejeté.<br/>
Article 2 : L'association des riverains de la baie de Sainte-Marie versera à la commune de Nouméa une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association des riverains de la Baie de Sainte-Marie, à la commune de Nouméa et à la société Magenta Développement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
