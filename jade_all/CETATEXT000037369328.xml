<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037369328</ID>
<ANCIEN_ID>JG_L_2018_08_000000423124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/36/93/CETATEXT000037369328.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/08/2018, 423124, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:423124.20180827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Paris d'enjoindre au préfet de police, sur le fondement de l'article L. 521-2 du code de justice administrative, d'enregistrer sa demande d'asile, de lui délivrer une attestation de demande d'asile et de mettre fin à sa rétention administrative. Par une ordonnance n° 1813788/9 du 31 juillet 2018, le juge des référés a enjoint au préfet de police d'enregistrer sa demande d'asile et de lui délivrer une attestation de demande d'asile et rejeté le surplus de ses conclusions. <br/>
<br/>
              Par une requête enregistrée le 13 août 2018 au secrétariat du contentieux<br/>
du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au juge des référés du<br/>
Conseil d'Etat d'annuler cette ordonnance et de rejeter les conclusions de M.A....<br/>
<br/>
<br/>
<br/>
              Il soutient :<br/>
              - que l'exécution de l'injonction prononcée par le juge des référés du tribunal administratif de Paris n'a pas eu pour objet et ne saurait avoir pour effet de priver d'objet<br/>
son appel ;<br/>
              - que la condition d'urgence n'était pas remplie à la date de l'ordonnance attaquée dès lors que l'intéressé s'était placé lui-même dans la situation justifiant sa rétention en faisant obstacle à la mise à exécution de la mesure d'éloignement et que la Cour européenne des droits de l'homme avait suspendu l'exécution de la mesure d'éloignement jusqu'au 6 août ; <br/>
              - que la condition d'urgence n'est pas davantage remplie au stade de l'appel dès lors que M. A...n'est plus en rétention administrative depuis le 2 août 2018, que la Bulgarie n'est pas un Etat défaillant au sens du règlement " Dublin III " et que la Cour européenne des droits de l'homme a indiqué ne plus s'opposer à son transfert vers la Bulgarie ; <br/>
              - que la décision contestée ne porte pas une atteinte grave et manifestement illégale au droit d'asile dès lors que la situation de fuite au sens de l'article 29 du règlement Dublin III est caractérisée, l'intéressé ayant refusé d'embarquer dans le cadre d'un départ contrôlé ; <br/>
              - que le requérant n'est pas davantage fondé à soutenir que la préfecture aurait porté une atteinte grave et manifestement illégale au droit d'asile en refusant d'enregistrer sa demande compte tenu des risques de mauvais traitements qu'il déclare encourir en Bulgarie dès lors que, d'une part, la Bulgarie n'a pas été reconnue comme un Etat défaillant au sens des dispositions de l'article 3 du règlement " Dublin III ", que le tribunal administratif de Paris, dans son jugement du 26 janvier 2018, a écarté ce moyen soulevé par le requérant à l'encontre de son recours contre la décision de transfert du 24 janvier 2018 et qu'enfin la Cour européenne des droits de l'homme a décidé le 6 août 2018 de ne pas reconduire la mesure provisoire, ne s'opposant pas au renvoi de l'intéressé en Bulgarie.<br/>
<br/>
              Par un mémoire en défense, enregistré le 21 août, M. A...demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) de rejeter la requête ; <br/>
<br/>
              2°) de l'admettre au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
              Il soutient : <br/>
              - que la décision de transfert, dans la mesure où elle peut être exécutée à tout moment, crée par elle-même une situation d'urgence au sens de l'article L. 521-2 du code de justice administrative ;  <br/>
              - que le comportement de fuite au sens de l'article 29 du règlement<br/>
" Dublin III " implique que l'intéressé se soit soustrait de façon intentionnelle et systématique<br/>
au contrôle de l'autorité, de sorte que le seul prétendu refus d'embarquer du 19 avril 2018 ne saurait caractériser un tel comportement, alors qu'il a en outre respecté l'assignation à résidence qui lui a été imposée et s'est présenté à la préfecture les 23 avril, 4 juin et 4 juillet 2018. <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique d'une part, le ministre d'Etat, ministre de l'intérieur et, d'autre part, M.A... ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 août 2018 à 9 heures 30 au cours de laquelle ont été entendus : <br/>
              - les représentants du ministre de l'intérieur ;<br/>
              - Me Texier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 23 août à 18 heures ; <br/>
<br/>
              Vu le nouveau mémoire, présenté par M. A...le 23 août 2018, par lequel l'intéressé reprend les conclusions et moyens de son précédent mémoire ; il soutient en outre que le juge des référés pourra le cas échéant poser à la Cour de justice de l'Union européenne la question préjudicielle suivante : " Le seul fait, à l'occasion d'un départ contrôlé, de refuser d'embarquer suffit-il à caractériser l'état de fuite au sens de l'article 29, paragraphe 2, seconde phrase, du règlement (UE) n° 604/2013 ' " ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) no 1560/2003 de la Commission du 2 septembre 2003 portant modalités d'application du règlement (CE) no 343/2003 du Conseil établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du<br/>
26 juin 2013, établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride (refonte) ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la demande d'admission provisoire à l'aide juridictionnelle :<br/>
<br/>
              1. Aux termes de l'article 20 de la loi du 10 juillet 1991 relative à l'aide juridique : " Dans les cas d'urgence (...), l'admission provisoire à l'aide juridictionnelle peut être prononcée (...) par la juridiction compétente ou son président ". Eu égard aux circonstances de l'espèce, il y a lieu de prononcer, en application de ces dispositions, l'admission provisoire de M. A...au bénéfice de l'aide juridictionnelle. <br/>
<br/>
              Sur l'appel du ministre : <br/>
<br/>
              2. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              3. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. <br/>
<br/>
              4. Il résulte de l'article 29 du règlement du 26 juin 2013 que le transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, cette période étant susceptible d'être portée à dix-huit mois si l'intéressé " prend la fuite ". Aux termes de l'article 7 du règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003, qui n'a pas été modifié sur ce point par le règlement d'exécution (UE) n° 118/2014 de la Commission du 30 janvier 2014 : "  1. Le transfert vers l'Etat responsable s'effectue de l'une des manières suivantes : a) à l'initiative du demandeur, une date limite étant fixée ; b) sous la forme d'un départ contrôlé, le demandeur étant accompagné jusqu'à l'embarquement par un agent de l'Etat requérant et le lieu, la date et l'heure de son arrivée étant notifiées à l'Etat responsable dans un délai préalable convenu : c) sous escorte, le demandeur étant accompagné par un agent de l'Etat requérant, ou par le représentant d'un organisme mandaté par l'Etat requérant à cette fin, et remis aux autorités de l'Etat responsable (...) ". Il résulte de ces dispositions que le transfert d'un demandeur d'asile vers un Etat membre qui a accepté sa prise ou sa reprise en charge, sur le fondement du règlement du 26 juin 2013, s'effectue selon l'une des trois modalités définies à l'article 7 cité ci-dessus : à l'initiative du demandeur, sous la forme d'un départ contrôlé ou sous escorte. <br/>
<br/>
              5. Il résulte clairement des dispositions mentionnées au point précédent que, d'une part, la notion de fuite doit s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. D'autre part, dans l'hypothèse où le transfert du demandeur d'asile s'effectue sous la forme d'un départ contrôlé, il appartient, dans tous les cas, à l'État responsable de ce transfert d'en assurer effectivement l'organisation matérielle et d'accompagner le demandeur d'asile jusqu'à l'embarquement vers son lieu de destination. Une telle obligation recouvre la prise en charge du titre de transport permettant de rejoindre l'État responsable de l'examen de la demande d'asile depuis le territoire français ainsi que, le cas échéant et si nécessaire, celle du pré-acheminement du lieu de résidence du demandeur au lieu d'embarquement. Enfin, dans l'hypothèse où le demandeur d'asile se soustrait intentionnellement à l'exécution de son transfert ainsi organisé, il doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013 rappelées au point 4.<br/>
<br/>
              6. M.A..., ressortissant afghan, a déclaré être entré irrégulièrement sur le territoire français le 28 novembre 2017. Il a sollicité l'asile le 11 décembre 2017 et le relevé de ses empreintes effectué à cette occasion a révélé que l'intéressé avait déjà sollicité l'asile en Bulgarie. Le 2 janvier 2018, une demande de reprise en charge a été envoyée aux autorités bulgares qui y ont répondu favorablement le 4 janvier 2018. Par deux arrêtés du 24 janvier 2018, le préfet de police a prononcé le transfert de l'intéressé vers la Bulgarie et l'a assigné à résidence. L'intéressé a formé un recours contre ces deux arrêtés, ce qui a eu pour effet d'interrompre le délai de six mois prévu à l'article 29 du règlement du 26 juin 2013. Ce délai a recommencé à courir le 26 janvier 2018, date à laquelle le tribunal administratif de Paris a rejeté ses recours. Le 18 avril 2018, M. A...a été placé en rétention en vue de son éloignement le lendemain. Il a refusé d'embarquer le 19 avril 2018 sur le vol à destination de Sofia sur lequel une place lui avait été réservée. Le 20 avril 2018, les autorités françaises ont informé les autorités bulgares qu'elles considéraient que l'intéressé était en fuite et que ceci portait le délai de transfert à dix-huit mois. M. A...a été de nouveau été placé en rétention le 4 juillet 2018 et sa rétention a été prolongée en vue de son éloignement prévu le 26 juillet 2018. Toutefois, le 24 juillet 2018, le requérant a introduit une requête devant la Cour européenne des droits de l'homme sur le fondement de l'article 39 du règlement intérieur de la Cour sollicitant la suspension de la procédure de transfert en se prévalant du traitement des demandeurs d'asile en Bulgarie. Ce même jour, la Cour a indiqué au gouvernement français de ne pas procéder à l'éloignement de l'intéressé avant le 6 août 2018. Le 28 juillet 2018, M. A... a saisi le juge des référés du tribunal administratif de Paris d'un recours sollicitant, sur le fondement de l'article L. 521-2 du code de justice administrative, l'enregistrement de sa demande d'asile en raison de l'expiration du délai de 6 mois dans lequel la France pouvait procéder à son transfert. Par une ordonnance du 31 juillet 2018 dont le ministre relève appel, le juge des référés du tribunal administratif de Paris a fait droit à la demande de l'intéressé. Le placement en rétention de l'intéressé a pris fin le 2 août 2018. Le 6 août 2018, la Cour européenne des droits de l'homme n'a pas reconduit la suspension de la mesure de transfert et a indiqué qu'elle ne s'opposerait pas au renvoi du requérant vers la Bulgarie.<br/>
<br/>
              7. Il résulte de ce qui est indiqué au point 5. que dans l'hypothèse où l'administration a respecté les obligations qui sont les siennes dans l'organisation d'un départ contrôlé et où l'intéressé s'est soustrait intentionnellement à l'exécution de ce départ, puis a demandé à nouveau l'enregistrement de sa demande après l'expiration du délai de transfert de six mois, il doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013. Il résulte de l'instruction, et il n'est d'ailleurs pas contesté par M.A..., que l'administration a effectué toutes les diligences qui lui incombaient dans l'organisation du départ contrôlé qui était prévu le 19 avril 2018. Par suite, et alors même que M. A...a respecté les termes de son assignation à résidence et s'est présenté à plusieurs reprises à la préfecture au cours de la période considérée, spontanément ou sur convocation, l'administration est fondée, en raison de l'obstruction qu'il a opposée le jour de son transfert, à estimer qu'il était en fuite pour l'application de l'article 29 du règlement cité ci-dessus. C'est, dès lors, à tort que le juge des référés du tribunal administratif de Paris a estimé que M. A...n'était pas en fuite et pouvait se prévaloir du délai de six mois prévu par ces dispositions.<br/>
<br/>
              8. Il y a lieu pour le juge des référés du Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. A...à  l'appui de sa demande. L'intéressé soutient qu'il ne peut être transféré en Bulgarie en raison du risque de mauvais traitements qu'il y encourt. Il ne produit toutefois aucun élément précis à l'appui de cette affirmation. Par ailleurs, il ne résulte pas de l'instruction que l'examen par la Bulgarie des demandes d'asile présentées par les personnes faisant l'objet d'un transfert ne respecterait pas l'ensemble des garanties exigées par le respect du droit d'asile. La Cour européenne des droits de l'homme a d'ailleurs implicitement mais nécessairement estimé que ces garanties étaient respectées lorsqu'elle a indiqué ne pas s'opposer au transfert du requérant. <br/>
<br/>
              9. Il résulte de tout ce qui précède qu'en refusant d'enregistrer la demande d'asile de M. A...au motif que le délai de transfert n'était pas expiré, l'administration n'a pas porté une atteinte grave et manifestement illégale au droit d'asile de l'intéressé. Il y a lieu, par suite, d'annuler l'ordonnance attaquée et de rejeter les conclusions présentées par M. A...sur  le fondement de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              10. L'Etat n'étant pas la partie perdante dans la présente instance, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme mise à sa charge au profit de l'avocat de M. A...en application de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : M. A...est admis à l'aide juridictionnelle à titre provisoire.<br/>
Article 2 : L'ordonnance du 31 juillet 2018 du juge des référés du tribunal administratif de Paris est annulée.<br/>
Article 3 : Les conclusions présentées par M. A...sur le fondement de l'article L. 521-2 du code de justice administrative sont rejetées. <br/>
Article 4 : Les conclusions présentées par M. A...sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 5 : La présente ordonnance sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
