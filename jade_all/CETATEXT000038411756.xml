<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411756</ID>
<ANCIEN_ID>JG_L_2019_04_000000416072</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411756.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 24/04/2019, 416072, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416072</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:416072.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 28 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le Syndicat national des agents des douanes CGT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 13 septembre 2017 relatif au réseau comptable de la direction générale des douanes et droits indirects en tant qu'il abroge l'arrêté du 10 mars 2017 relatif au réseau comptable de la direction générale des douanes et droits indirects et qu'il crée la recette interrégionale de Bordeaux et la recette interrégionale de Montpellier, qu'il ferme les recettes régionales de Bayonne, de Perpignan et de Toulouse, qu'il transfère l'activité des recettes régionales de Bordeaux et de Bayonne à la recette interrégionale de Bordeaux et qu'il transfère l'activité des recettes régionales de Montpellier, de Perpignan et de Toulouse à la recette interrégionale de Montpellier.<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983, notamment son article 9 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 82-453 du 28 mai 1982 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le Syndicat national des agents des douanes CGT demande l'annulation pour excès de pouvoir de l'arrêté du 13 septembre 2017 relatif au réseau comptable des douanes et droits indirects en tant qu'il abroge, par son article 1er, l'arrêté du 10 mars 2017 relatif au réseau comptable de la direction générale des douanes et droits indirects et qu'il procède à la fermeture des recettes régionales de Bordeaux et Bayonne, d'une part, et des recettes régionales de Montpellier, de Perpignan et de Toulouse, d'autre part, qu'il crée les deux recettes interrégionales de Bordeaux et de Montpellier et qu'il transfère respectivement l'activité de ces recettes régionales à ces recettes interrégionales.<br/>
<br/>
              En ce qui concerne l'abrogation de l'arrêté du 10 mars 2017 par l'article 1er de l'arrêté attaqué :<br/>
<br/>
              2. L'article 1er de l'arrêté attaqué abroge l'arrêté du 10 mars 2017 relatif au réseau comptable de la direction générale des douanes et droits indirects qui, à la date d'adoption de l'arrêté attaqué, n'était pas encore entré en vigueur, notamment s'agissant de l'organisation de la direction interrégionale d'Ile-de-France. Cette abrogation, qui se borne à mettre fin, avant sa prise d'effet, à un projet de réorganisation des services sans procéder, par elle-même, à aucune réorganisation, ne nécessitait pas, contrairement à ce qui est soutenu, la consultation du comité technique des services déconcentrés d'Ile-de-France. Par suite, le moyen tiré du défaut de nouvelle consultation de ce comité technique ne peut qu'être écarté et les conclusions dirigées contre l'article 1er de l'arrêté attaqué rejetées. <br/>
<br/>
              En ce qui concerne la création de la recette interrégionale de Bordeaux et le transfert des missions de la recette régionale de Bordeaux :<br/>
<br/>
              3. Le syndicat requérant soutient que le comité technique des services déconcentrés de Bordeaux ne pouvait pas émettre d'avis sur la transformation de la recette régionale de Bordeaux en recette interrégionale lors de sa séance du 8 décembre 2016 dès lors qu'aucun texte réglementaire n'avait institué de poste comptable dénommé " recette interrégionale ". Toutefois, si ce n'est que postérieurement à cette séance qu'un arrêté en date du 4 janvier 2017 a créé, au sein des postes comptables de la direction générale des douanes et droits indirects, la catégorie des " recettes interrégionales ", cette circonstance est sans incidence sur la régularité de la consultation du comité technique dès lors qu'il n'est pas contesté que celui-ci a notamment été informé des missions et du périmètre géographique de la future recette interrégionale et, ainsi qu'il ressort du procès-verbal de la réunion du 8 décembre 2016, qu'il a émis, à l'issue d'un débat, un avis sur le projet qui lui était soumis. Par suite, le Syndicat national des agents des douanes CGT n'est pas fondé à demander l'annulation de l'arrêté attaqué en tant qu'il crée la recette interrégionale de Bordeaux, qu'il procède à la fermeture de la recette régionale de Bordeaux et qu'il transfère l'activité de cette dernière à la recette interrégionale.<br/>
<br/>
              En ce qui concerne la fermeture de la recette régionale de Bayonne et le transfert de son activité à la recette interrégionale de Bordeaux :<br/>
<br/>
              4. Le moyen tiré de l'irrégularité de la consultation du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) des Pyrénées-Atlantiques n'est pas assorti des précisions suffisantes permettant d'en apprécier le bien-fondé. Par suite, le Syndicat national des agents des douanes CGT n'est pas fondé à demander l'annulation de l'arrêté attaqué en tant qu'il procède à la fermeture de la recette régionale de Bayonne et au transfert de son activité à la recette interrégionale de Bordeaux.<br/>
<br/>
              En ce qui concerne la création de la recette interrégionale de Montpellier et le transfert des missions de la recette régionale de Montpellier :<br/>
<br/>
              5. En premier lieu, le syndicat requérant soutient que le comité d'hygiène, de sécurité et des conditions de travail de l'Hérault a été irrégulièrement consulté dès lors que l'administration ne lui a pas remis l'ensemble des documents lui permettant de se prononcer en toute connaissance de cause, s'agissant en particulier de la nouvelle implantation immobilière de différents services de la direction interrégionale de Montpellier.<br/>
<br/>
              6. D'une part, aux termes de l'article 17 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique : " Le médecin de prévention est obligatoirement consulté sur les projets de construction ou aménagement importants des bâtiments administratifs et de modifications apportées aux équipements ". Il ressort des pièces du dossier, notamment du procès-verbal de la réunion du comité d'hygiène, de sécurité et des conditions de travail du 15 décembre 2016, que le médecin de prévention a été consulté sur l'installation du nouveau site du pôle comptable interrégional de Montpellier, quand bien même il n'avait pas formalisé son rapport par écrit à la date de la réunion du comité, et que le comité a eu connaissance de la teneur de son avis. Ainsi, le syndicat requérant n'est fondé à soutenir ni que l'article 17 du décret du 28 mai 1982 aurait été méconnu ni, en tout état de cause, qu'il n'aurait pas disposé de l'avis du médecin de prévention.<br/>
<br/>
              7. D'autre part, il ressort également du procès-verbal que l'inspecteur santé et sécurité au travail a présenté au comité un rapport sur la nouvelle implantation immobilière. Il ressort enfin de ce procès-verbal que, notamment sur la base de ce rapport, les représentants des organisations syndicales ont été en mesure d'évoquer leurs préoccupations relatives à la sécurité et aux conditions de travail résultant du déménagement des services de la direction interrégionale et, en particulier, les risques routiers, la surface par agent dans le nouveau bâtiment, l'absence de salle de réunion, l'installation de placards plutôt que des armoires hautes, le respect de la confidentialité des échanges pour les entretiens individuels conduits par les agents du service viticulture, la capacité du parking au regard du nombre d'agents en poste, auxquelles les représentants de l'administration ont apporté des réponses circonstanciées. <br/>
<br/>
              8. Par suite, le syndicat requérant n'est pas fondé à soutenir que l'information transmise au comité d'hygiène, de sécurité et des conditions de travail était insuffisante. Dès lors, bien que les représentants des organisations syndicales aient refusé de se prononcer sur le projet qui leur était soumis, l'avis de ce comité doit être réputé rendu.<br/>
<br/>
              9. En second lieu, le syndicat requérant soutient que le comité technique des services déconcentrés de la direction interrégionale de Montpellier ne pouvait émettre d'avis sur la réorganisation des services envisagée dès lors que le comité d'hygiène, de sécurité et des conditions de travail de l'Hérault n'avait pas rendu d'avis. Toutefois, il résulte de ce qui a été dit aux points 6 à 8 que ce comité a été régulièrement consulté. <br/>
<br/>
              10. Par suite, le Syndicat national des agents des douanes CGT n'est pas fondé à demander l'annulation de l'arrêté attaqué en tant qu'il crée la recette interrégionale de Montpellier, qu'il procède à la fermeture de la recette régionale de Montpellier et qu'il transfère l'activité de cette dernière à la recette interrégionale.<br/>
<br/>
              En ce qui concerne la fermeture de la recette régionale de Perpignan et le transfert de son activité à la recette interrégionale de Montpellier :<br/>
<br/>
              11. En premier lieu, le syndicat requérant soutient que le comité d'hygiène, de sécurité et des conditions de travail des Pyrénées-Orientales a été irrégulièrement consulté faute d'avoir été saisi de toutes les questions soulevées par le projet de fermeture de la recette régionale de Perpignan. Il ressort des pièces du dossier, notamment du procès-verbal de la réunion du comité d'hygiène, de sécurité et des conditions de travail du 4 octobre 2016 que celui-ci a été consulté sur la " fermeture en 2017 du pôle régional comptable de Perpignan pour le transfert de l'activité vers le pôle interrégional comptable de Montpellier " et que les représentants des organisations syndicales se sont prononcées sur ce projet. Si les représentants des organisations syndicales ont essentiellement sollicité des éclaircissements sur les conséquences de cette fermeture pour les agents en poste à la recette régionale, il ne ressort pas de ce document, contrairement à ce que soutient le syndicat requérant, que le comité aurait été saisi de cette unique question et non de l'ensemble du projet. Le moyen tiré du caractère irrégulier de la consultation du comité doit par suite être écarté.<br/>
<br/>
              12. En second lieu, le syndicat requérant soutient que le comité technique des services déconcentrés de la direction interrégionale de Montpellier ne pouvait émettre d'avis sur la réorganisation des services envisagée dès lors que le comité d'hygiène, de sécurité et des conditions de travail des Pyrénées-Orientales avait été irrégulièrement consulté. Toutefois, il résulte de ce qui a été dit au point précédent que la consultation de ce comité a été régulière. <br/>
<br/>
              13. Par suite, le Syndicat national des agents des douanes CGT n'est pas fondé à demander l'annulation de l'arrêté attaqué en tant qu'il procède à la fermeture de la recette régionale de Perpignan et au transfert de son activité à la recette interrégionale de Montpellier.<br/>
<br/>
              En ce qui concerne la fermeture de la recette régionale de Toulouse et le transfert de son activité à la recette interrégionale de Montpellier :<br/>
<br/>
              14. Aux termes de l'article 55 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique : " Le comité d'hygiène, de sécurité et des conditions de travail peut demander au président de faire appel à un expert agréé conformément aux articles R. 4614-6 et suivants du code du travail : / (...) 2° En cas de projet important modifiant les conditions de santé et de sécurité ou les conditions de travail, prévu à l'article 57. (...) / La décision de l'administration refusant de faire appel à un expert doit être substantiellement motivée. Cette décision est communiquée au comité d'hygiène, de sécurité et des conditions de travail ministériel. / En cas de désaccord sérieux et persistant entre le comité et l'autorité administrative sur le recours à l'expert agréé, la procédure prévue à l'article 5-5 peut être mise en oeuvre ". L'article 5-5 du même décret dispose que : " (...) en cas de désaccord sérieux et persistant entre l'administration et le comité d'hygiène, de sécurité et des conditions de travail, le chef de service compétent ainsi que le comité d'hygiène et de sécurité compétent peuvent solliciter l'intervention de l'inspection du travail. Les inspecteurs santé et sécurité au travail, peuvent également solliciter cette intervention. / Dans le cas d'un désaccord sérieux et persistant, l'inspection du travail n'est saisie que si le recours aux inspecteurs santé et sécurité au travail n'a pas permis de lever le désaccord (...) ". Aux termes de l'article 69 du même décret : " (...) les comités d'hygiène, de sécurité et des conditions de travail se réunissent au moins trois fois par an sur convocation de leur président, à son initiative ou dans le délai maximum de deux mois, sur demande écrite de la moitié des représentants titulaires du personnel sans que ce chiffre ne puisse excéder trois représentants ".<br/>
<br/>
              15. Il ressort des pièces du dossier et n'est pas contesté que la fermeture de la recette régionale de Toulouse et le transfert de son activité à la recette interrégionale de Montpellier constitue un projet important au sens de l'article 55 du décret du 28 mai 1982. Cette réorganisation a été inscrite à l'ordre du jour du comité d'hygiène, de sécurité et des conditions de travail de Haute-Garonne du 7 novembre 2016, lequel a demandé, en application de l'article 55 du décret du 28 mai 1982, de recourir à un expert agréé. Par courrier en date du 22 novembre 2016, adressé au président du comité d'hygiène, de sécurité et des conditions de travail, le directeur interrégional des douanes a refusé de faire droit à cette demande. D'une part, le comité n'a pas été ensuite convoqué pour évoquer à nouveau cette réorganisation. D'autre part, il ne ressort pas des pièces du dossier, ni n'est soutenu, que ce courrier aurait été transmis aux membres du comité. Par suite, ceux-ci n'ont pas été mis à même de demander, conformément aux dispositions de l'article 69 du décret du 28 mai 1982, la convocation du comité pour, éventuellement, constater un désaccord sérieux et persistant et enclencher la procédure prévue par l'article 5-5 du même décret.<br/>
<br/>
              16. Il résulte de ce qui précède, eu égard à la garantie que constitue le recours à un expert agréé, que la consultation du comité d'hygiène, de sécurité et des conditions de travail de Haute-Garonne a été irrégulière. Par suite, sans qu'il soit besoin d'examiner les autres moyens relatifs à ces conclusions, le syndicat requérant est fondé à demander l'annulation de l'arrêté attaqué en tant qu'il procède à la fermeture de la recette régionale de Toulouse et au transfert de son activité à la recette interrégionale de Montpellier.<br/>
<br/>
              Sur les autres moyens de la requête :<br/>
<br/>
              17. Aux termes de l'article 9 de la loi portant droits et obligations des fonctionnaires du 13 juillet 1983 : " Les fonctionnaires participent par l'intermédiaire de leurs délégués siégeant dans des organismes consultatifs à l'organisation et au fonctionnement des services publics ". Il résulte de ce qui a été dit ci-dessus que les dispositions attaquées n'ont pas été prises en méconnaissance cet article.<br/>
<br/>
              18. Le moyen tiré de ce que les dispositions attaquées n'auraient été précédées d'aucun dialogue social véritable et méconnaîtraient ainsi les dispositions de la loi du 5 juillet 2010 relative à la rénovation du dialogue social ne peut qu'être écarté compte tenu de ce qui a été dit ci-dessus et dès lors, en outre, qu'il n'est pas assorti de précisions suffisantes pour permettre d'apprécier quelles dispositions de cette loi auraient été méconnues.<br/>
<br/>
              19. Il résulte de tout ce qui précède que le Syndicat national des agents des douanes CGT est fondé à demander l'annulation de l'arrêté qu'il attaque en tant seulement qu'il décide la fermeture de la recette régionale de Toulouse et le transfert intégral de son activité comptable à la recette interrégionale de Montpellier.<br/>
<br/>
              20. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Syndicat nationale des agents des douanes CGT au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
DECIDE :<br/>
                                                                         ------------<br/>
<br/>
<br/>
Article 1er : L'arrêté du 13 septembre 2017 est annulé en tant qu'il décide la fermeture de la recette régionale de Toulouse et le transfert intégral de son activité comptable à la recette interrégionale de Montpellier.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Syndicat national des agents des douanes CGT et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
