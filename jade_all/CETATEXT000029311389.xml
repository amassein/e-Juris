<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311389</ID>
<ANCIEN_ID>JG_L_2014_07_000000378233</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/13/CETATEXT000029311389.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 30/07/2014, 378233, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378233</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:378233.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 18 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la commune de Saint-André-Capcèze, représentée par son maire ; la commune demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-245 du 25 février 2014 portant délimitation des cantons dans le département de la Lozère ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 2013-403 du 17 mai 2013 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article L. 3113-2 du code général des collectivités territoriales, dans sa version issue de la loi du 17 mai 2013, prévoit que : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil départemental qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes :     a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ; ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant, en premier lieu, que le décret attaqué a été pris après avis du conseil général de la Lozère  rendu le 27 janvier 2014 ; qu'aucune disposition législative ou réglementaire n'imposait au Gouvernement de procéder préalablement à l'intervention du décret attaqué à une consultation de l'ensemble des maires et élus du département et de mentionner dans le décret les motifs justifiant la nouvelle carte cantonale ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il résulte des dispositions du III de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques et qu'il doit être continu ; <br/>
<br/>
               4. Considérant que l'article 1er du décret attaqué procède à une nouvelle délimitation de l'ensemble des cantons du département de la Lozère compte tenu de l'exigence de réduction du nombre des cantons de ce département de vingt-cinq à treize résultant de l'application de l'article L. 191-1 du code électoral en se fondant sur une population moyenne et en rapprochant la population de chaque canton de cette moyenne ; que la nouvelle carte cantonale permet de réduire l'écart existant entre la population du canton le moins peuplé et le plus peuplé de 1 à 1,43, alors qu'il était précédemment de 1 à 9,93 ; que si la commune fait valoir que le critère démographique retenu est inadapté au département de la Lozère et que les nouveaux cantons se caractérisent par de grandes disparités de taille et de densité de population, il n'est pas soutenu que cette nouvelle délimitation méconnaîtrait les critères définis au III de l'article L. 3113-2 du code général des collectivités territoriales ; que si elle  soutient que les populations des cantons de Langogne et de Florac ont un écart par rapport à la population moyenne du département de - 18,56 % et de - 17,71 %, sans qu'aucune considération géographique ou impératif d'intérêt général ne le justifie, le ministre fait valoir, sans être contredit, que ces écarts répondent à la prise en compte de critères démographiques et géographiques, que le premier canton s'inscrit dans les limites cantonales antérieures étendues à deux nouvelles communes afin que soit respectée la règle relative à l'écart de population et que le second, défini selon un critère géographique, réunit des communes issues de trois anciens cantons autour du Causse Méjean ; que si le projet de décret prévoyait que la population du canton du Collet-de-Dèze devait être inférieure à la moyenne départementale de 19,65 %, cet écart a été ramené à 14,75 % par le décret attaqué, qui définit ce canton autour des Cévennes lozériennes ; que si la population du canton d'Aumont-Aubrac excède la moyenne départementale de 16,76 %, cet écart se justifie par le critère géographique retenu qui conduit à regrouper les communes du plateau de l'Aubrac et à réunir les anciens cantons d'Aumont-Aubrac, de Fournels et de Nasbinals ; que la présence du Mont Lozère dans le canton de Saint-Etienne-de-Valdonnez n'est pas de nature à rompre la continuité territoriale de ce canton qui regroupe les communes de la haute vallée du Tarn ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'aucune disposition législative ou réglementaire n'impose au pouvoir réglementaire le respect des limites géographiques des diverses structures de coopération intercommunale et des bassins économiques et sociaux existants ;  que la proximité géographique des communes d'un même canton n'est pas au nombre des critères définis à l'article L. 3113-2 du code général des collectivités territoriales ; que, par suite, la commune ne saurait utilement invoquer les circonstances que la nouvelle carte cantonale serait inadaptée aux " bassins de vie " du département, serait susceptible d'entraîner des déséquilibres économiques et sociaux, ne tiendrait pas compte des obstacles naturels, et que le rattachement des communes de Bédouès, de Cocurès et des Bondons au canton de Saint-Etienne-de-Valdonnez serait arbitraire, au motif que leur maintien dans le canton de Florac aurait respecté le " bassin de vie " auquel elles appartiennent ; <br/>
<br/>
              6. Considérant, enfin, que les cantons étant des circonscriptions électorales, le moyen tiré de ce que la nouvelle carte cantonale serait susceptible de conduire au regroupement de services publics dans les chefs-lieux de cantons ou à leur disparition est sans influence sur la légalité du décret attaqué ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la requête de la commune de Saint-André-Capcèze doit être rejetée ;<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                  --------------<br/>
<br/>
 Article 1er : La requête de la commune de Saint-André-Capcèze est rejetée.<br/>
<br/>
 Article 2 : La présente décision sera notifiée à la commune de Saint-André-Capcèze et au ministre de l'intérieur.<br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
