<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031984304</ID>
<ANCIEN_ID>JG_L_2016_02_000000376269</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/98/43/CETATEXT000031984304.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 03/02/2016, 376269</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376269</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:376269.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 11 mars 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision née du silence gardé pendant plus de deux mois par le garde des sceaux, ministre de la justice, sur sa demande tendant à l'abrogation des dispositions du deuxième alinéa du VII de l'article 19 du règlement intérieur type des établissements pénitentiaires, annexé au décret du 30 avril 2013 relatif aux règlements intérieurs types des établissements pénitentiaires, et du troisième alinéa de l'article 37 de ce même règlement ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Baraduc, Duhamel, Rameix, avocat de M.B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le pacte international relatif aux droits civils et politiques ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ; <br/>
              - le code de procédure pénale ; <br/>
              - la loi n° 91-646 du 10 juillet 1991 ; <br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ; <br/>
              - le décret n° 2013-368 du 30 avril 2013 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes de l'article 37 du règlement intérieur type des établissements pénitentiaires, annexé au décret du 30 avril 2013 relatif aux règlements intérieurs types des établissements pénitentiaires : " Une aide matérielle peut être attribuée à toute personne détenue dépourvue de ressources au moment de sa sortie de détention afin de lui permettre de subvenir à ses besoins pendant le temps nécessaire pour rejoindre le lieu où elle a déclaré se rendre. / L'établissement pénitentiaire fournit, dans toute la mesure possible, des vêtements à la personne détenue libérable qui n'en posséderait pas et serait dépourvue de ressources suffisantes pour s'en procurer. / L'établissement pénitentiaire peut procéder ou participer à l'acquisition d'un titre de transport pour la personne détenue qui, à sa sortie de détention, n'aurait pas un solde suffisant sur son compte nominatif pour rejoindre le lieu où elle a déclaré se rendre. / (...) " ;<br/>
<br/>
              2. Considérant que, contrairement à ce que soutient M.B..., la circonstance que cette disposition ne prévoit, pour l'administration pénitentiaire, qu'une faculté, et non une obligation, de procéder ou de participer à l'acquisition d'un titre de transport pour la personne détenue à sa sortie de détention ne méconnaît, par elle-même, aucun principe ou règle s'imposant au pouvoir réglementaire ;<br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes du premier alinéa de l'article 22 de la loi pénitentiaire du 24 novembre 2009 : " L'administration pénitentiaire garantit à toute personne détenue le respect de sa dignité et de ses droits. L'exercice de ceux-ci ne peut faire l'objet d'autres restrictions que celles résultant des contraintes inhérentes à la détention, du maintien de la sécurité et du bon ordre des établissements, de la prévention de la récidive et de la protection de l'intérêt des victimes. " ; qu'aux termes du VII de l'article 19 du règlement intérieur type des établissements pénitentiaires, dont le deuxième alinéa est contesté : " VII. - La personne détenue peut acquérir par l'intermédiaire de l'administration et selon les modalités qu'elle détermine des équipements informatiques. / En aucun cas elle n'est autorisée à conserver des documents, autres que ceux liés à des activités socioculturelles, d'enseignement, de formation ou professionnelles, sur un support informatique. / Ces équipements ainsi que les données qu'ils contiennent sont soumis au contrôle de l'administration. Sans préjudice d'une éventuelle saisie par l'autorité judiciaire, tout équipement informatique appartenant à une personne détenue peut être retenu et ne lui être restitué qu'au moment de sa libération, dans les cas suivants : / 1° Pour des raisons d'ordre et de sécurité ; / 2° En cas d'impossibilité d'accéder aux données informatiques, du fait volontaire de la personne détenue. " ; <br/>
<br/>
              4. Considérant, d'une part, que si M. B...critique le deuxième alinéa du VII de l'article cité ci-dessus, en tant qu'il interdit aux détenus de conserver sur support informatique des données personnelles qui proviendraient de l'extérieur de l'établissement pénitentiaire, une telle limitation, qui se justifie notamment par la difficulté pour l'administration pénitentiaire d'exercer le contrôle des données conservées sur support informatique, répond à l'objectif d'intérêt général de protection de la sécurité et du bon ordre dans les établissements pénitentiaires ; qu'une telle restriction ne peut, alors que les intéressés sont autorisés à disposer de photographies de leurs proches conservées sur support papier, être regardée comme portant une atteinte disproportionnée au droit au respect de la vie privée et familiale des personnes détenues ; qu'ainsi, les moyens tirés de la méconnaissance de l'article 2 de la Déclaration des droits de l'homme et du citoyen, de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 22 de la loi pénitentiaire du 24 novembre 2009 cité ci-dessus doivent être écartés ; <br/>
<br/>
              5. Considérant, d'autre part, que l'interdiction en cause ne se rapporte qu'au stockage de données et ne met pas par elle-même en cause la liberté de s'exprimer ni de communiquer ; qu'ainsi, les moyens tirés de la méconnaissance de l'article 10 de la Déclaration des droits de l'homme et du citoyen, de l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, de l'article 19 du pacte international relatif aux droits civils et politiques et de l'article 11 de la charte des droits fondamentaux de l'Union européenne, qui garantissent la liberté d'expression, sont inopérants et ne peuvent qu'être écartés ; <br/>
<br/>
              6. Considérant, enfin, que, contrairement à ce qui est soutenu, l'interdiction litigieuse ne porte pas atteinte aux droits de la défense, dès lors qu'elle ne fait pas obstacle à la préparation par la personne détenue de sa défense, y compris lorsque cette dernière décide de ne pas solliciter l'assistance d'un avocat ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par le ministre de la justice en défense, que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Baraduc, Duhamel, Rameix, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-10 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. SECRET DE LA VIE PRIVÉE. - MÉCONNAISSANCE - ABSENCE - LIMITATION DES DONNÉES CONSERVABLES SUR SUPPORT INFORMATIQUE PAR LES DÉTENUS (DÉCRET N° 2013-368 DU 30 AVRIL 2013).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-03-11 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. DROITS DE LA PERSONNE. - DROIT AU RESPECT DE LA VIE FAMILIALE - MÉCONNAISSANCE - ABSENCE - LIMITATION DES DONNÉES CONSERVABLES SUR SUPPORT INFORMATIQUE PAR LES DÉTENUS (DÉCRET N° 2013-368 DU 30 AVRIL 2013).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-055-01-08 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT AU RESPECT DE LA VIE PRIVÉE ET FAMILIALE (ART. 8). - MÉCONNAISSANCE - ABSENCE - LIMITATION DES DONNÉES CONSERVABLES SUR SUPPORT INFORMATIQUE PAR LES DÉTENUS (DÉCRET N° 2013-368 DU 30 AVRIL 2013).
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - LIMITATION DES DONNÉES CONSERVABLES SUR SUPPORT INFORMATIQUE PAR LES DÉTENUS (DÉCRET N° 2013-368 DU 30 AVRIL 2013) - MÉCONNAISSANCE DU DROIT AU RESPECT À LA VIE PRIVÉE ET FAMILIALE - ABSENCE.
</SCT>
<ANA ID="9A"> 26-03-10 Le deuxième alinéa du VII de l'article 19 du règlement intérieur type des établissements pénitentiaires annexé au décret n° 2013-368 du 30 avril 2013, qui interdit aux détenus de conserver sur support informatique des données personnelles qui proviendraient de l'extérieur de l'établissement pénitentiaire, se justifie notamment par la difficulté pour l'administration pénitentiaire d'exercer le contrôle des données conservées sur support informatique et répond à l'objectif d'intérêt général de protection de la sécurité et du bon ordre dans les établissements pénitentiaires. Une telle restriction ne peut, alors que les intéressés sont autorisés à disposer de photographies de leurs proches conservées sur support papier, être regardée comme portant une atteinte disproportionnée au droit au respect de la vie privée et familiale des personnes détenues et ne méconnaît ni l'article 2 de la Déclaration des droits de l'homme et du citoyen, ni l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9B"> 26-03-11 Le deuxième alinéa du VII de l'article 19 du règlement intérieur type des établissements pénitentiaires annexé au décret n° 2013-368 du 30 avril 2013, qui interdit aux détenus de conserver sur support informatique des données personnelles qui proviendraient de l'extérieur de l'établissement pénitentiaire, se justifie notamment par la difficulté pour l'administration pénitentiaire d'exercer le contrôle des données conservées sur support informatique et répond à l'objectif d'intérêt général de protection de la sécurité et du bon ordre dans les établissements pénitentiaires. Une telle restriction ne peut, alors que les intéressés sont autorisés à disposer de photographies de leurs proches conservées sur support papier, être regardée comme portant une atteinte disproportionnée au droit au respect de la vie privée et familiale des personnes détenues et ne méconnaît ni l'article 2 de la Déclaration des droits de l'homme et du citoyen, ni l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9C"> 26-055-01-08 Le deuxième alinéa du VII de l'article 19 du règlement intérieur type des établissements pénitentiaires annexé au décret n° 2013-368 du 30 avril 2013, qui interdit aux détenus de conserver sur support informatique des données personnelles qui proviendraient de l'extérieur de l'établissement pénitentiaire, se justifie notamment par la difficulté pour l'administration pénitentiaire d'exercer le contrôle des données conservées sur support informatique et répond à l'objectif d'intérêt général de protection de la sécurité et du bon ordre dans les établissements pénitentiaires. Une telle restriction ne peut, alors que les intéressés sont autorisés à disposer de photographies de leurs proches conservées sur support papier, être regardée comme portant une atteinte disproportionnée au droit au respect de la vie privée et familiale des personnes détenues et ne méconnaît ni l'article 2 de la Déclaration des droits de l'homme et du citoyen, ni l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9D"> 37-05-02-01 Le deuxième alinéa du VII de l'article 19 du règlement intérieur type des établissements pénitentiaires annexé au décret n° 2013-368 du 30 avril 2013, qui interdit aux détenus de conserver sur support informatique des données personnelles qui proviendraient de l'extérieur de l'établissement pénitentiaire, se justifie notamment par la difficulté pour l'administration pénitentiaire d'exercer le contrôle des données conservées sur support informatique et répond à l'objectif d'intérêt général de protection de la sécurité et du bon ordre dans les établissements pénitentiaires. Une telle restriction ne peut, alors que les intéressés sont autorisés à disposer de photographies de leurs proches conservées sur support papier, être regardée comme portant une atteinte disproportionnée au droit au respect de la vie privée et familiale des personnes détenues et ne méconnaît ni l'article 2 de la Déclaration des droits de l'homme et du citoyen, ni l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
