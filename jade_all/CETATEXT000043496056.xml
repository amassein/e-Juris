<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043496056</ID>
<ANCIEN_ID>JG_L_2021_05_000000447963</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/49/60/CETATEXT000043496056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 11/05/2021, 447963</TITRE>
<DATE_DEC>2021-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447963</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Eric Buge</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447963.20210511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement du 19 octobre 2020, enregistré le 18 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le tribunal judiciaire de Bobigny a sursis à statuer sur la demande de Mme B... A... tendant au versement par la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes des prestations journalières d'invalidité pour la période allant du 23 avril au 31 août 2017 et saisi le Conseil d'État de la question de la légalité de l'article 7 des statuts du régime d'assurance invalidité décès de cette caisse.<br/>
<br/>
              Par trois mémoires, enregistrés au secrétariat du contentieux le 26 janvier, le 4 février et le 18 mars 2021, Mme B... A... demande au Conseil d'État :<br/>
<br/>
              1°) d'apprécier la légalité de cet article et de déclarer qu'il est entaché d'illégalité ;<br/>
<br/>
              2°) d'enjoindre, sur le fondement de l'article L. 911-1 du code de justice administrative, au ministre des solidarités et de la santé d'abroger cet article dans un délai de quinze jours ;<br/>
<br/>
              3°) de mettre à la charge de la caisse et de l'Etat la somme de 3 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n°68-884 du 10 octobre 1968 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Buge, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Delvolvé et Trichet, avocat de la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Par un jugement du 19 octobre 2020, le tribunal judiciaire de Bobigny, saisi d'un litige opposant Mme A... à la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes (Carpimko), a sursis à statuer jusqu'à ce que le Conseil d'Etat se soit prononcé sur la légalité de l'article 7 des statuts du régime d'assurance invalidité-décès de cette caisse, en ce qu'il porte une atteinte disproportionnée au droit de propriété garanti par l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en permettant de priver de droits à une prestation d'invalidité les personnes qui présenteraient un arriéré, même faible, de cotisations.<br/>
<br/>
              2. Aux termes de l'article L. 644-2 du code de la sécurité sociale : " À la demande du conseil d'administration de la caisse nationale d'assurance vieillesse des professions libérales, des décrets peuvent fixer, en sus des cotisations prévues aux articles L. 642-1 et L. 644-1, et servant à financer le régime d'assurance vieillesse de base et le régime d'assurance vieillesse complémentaire, une cotisation destinée à couvrir un régime d'assurance invalidité-décès, fonctionnant à titre obligatoire dans le cadre, soit de l'ensemble du groupe professionnel, soit d'une activité professionnelle particulière et comportant des avantages en faveur des veuves et des orphelins (...) ". Sur ce fondement, l'article 1er du décret du 10 octobre 1968 relatif au régime d'assurance invalidité-décès des infirmiers, masseurs-kinésithérapeutes, pédicures et orthophonistes institue des cotisations destinées à financer un régime d'assurance invalidité-décès fonctionnant à titre obligatoire, comportant des avantages en faveur des infirmiers, masseurs-kinésithérapeutes, pédicures, orthophonistes et orthoptistes atteints d'invalidité temporaire de plus de 90 jours ou d'incapacité totale et définitive et en faveur notamment de leur conjoint survivant et de leurs enfants à charge. L'article 4 de ce décret prévoit que ce régime d'assurance invalidité-décès est établi par les statuts de la section professionnelle des infirmiers, masseurs-kinésithérapeutes, pédicures et auxiliaires médicaux. Selon l'article 3 des statuts de ce régime, approuvés par le ministre chargé de la sécurité sociale dans les conditions prévues par l'article L. 641-5 du code de la sécurité sociale, les affiliés peuvent bénéficier du service d'une allocation journalière d'inaptitude totale à compter du 91ème jour d'incapacité professionnelle totale. Aux termes de l'article 7 des mêmes statuts, dont l'appréciation de la légalité est soumise au Conseil d'Etat : " Le non-paiement de tout ou partie des cotisations et le cas échéant, des majorations de retard dues au titre de l'ensemble des régimes gérés par la Carpimko entraîne, en ce qui concerne les risques visés aux 1°, 2° et 3° de l'article 3  : / 1°) la suppression du droit à prestations jusqu'au premier jour du mois suivant l'extinction de la dette lorsque cette dernière est afférente à l'année de survenance du risque et aux exercices antérieurs ou à ces derniers seulement ; / 2°) le maintien du droit à prestations lorsque la dette est afférente exclusivement à l'année de survenance du risque, sous réserve que l'assuré procède à la régularisation de son compte dans le délai d'un mois à partir de la déclaration d'incapacité ou d'invalidité. Passé ce délai, le droit à prestations est supprimé dans les conditions prévues au 1°). "<br/>
<br/>
              4. Aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes ". Une personne ne peut prétendre au bénéfice de ces stipulations que si elle peut faire état de la propriété d'un bien qu'elles ont pour objet de protéger et à laquelle il aurait été porté atteinte. A défaut de créance certaine, l'espérance légitime d'obtenir une somme d'argent doit être regardée comme un bien au sens de ces stipulations. Les prestations servies par un régime d'assurance sociale en matière d'invalidité constituent, pour les personnes qui en remplissent les conditions légales, des biens au sens de ces stipulations.<br/>
<br/>
              5. En prévoyant que le droit à l'allocation journalière d'inaptitude ne peut être ouvert aux personnes qui ne se sont pas acquittées du paiement de tout ou partie de leurs cotisations, et le cas échéant des majorations de retard, l'article contesté fixe une condition pour pouvoir bénéficier de cette allocation. Les personnes qui remplissent cette condition disposent, sous réserve de satisfaire également aux autres conditions auxquelles le bénéfice de cette allocation est subordonné, d'un droit, constitutif d'un bien protégé par les stipulations mentionnées au point précédent, à percevoir les prestations prévues par ce régime. En revanche, tel n'est pas le cas, en principe, des personnes qui ne remplissent pas cette condition, alors même qu'elles se seraient acquittées, par le passé, d'une partie des cotisations. L'article contesté ne peut être regardé, en ce qu'il subordonne à cette condition le bénéfice de l'allocation journalière d'inaptitude, comme contraire à l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              6. Il résulte de ce qui précède que Mme A... n'est pas fondée à soutenir que l'article 7 des statuts du régime d'assurance invalidité-décès de cette caisse est entaché d'illégalité, sans qu'aient à être examinés les autres moyens qu'elle soulève, qui sont irrecevables dès lors qu'ils excèdent la portée de la question renvoyée par le tribunal judiciaire de Bobigny, limitée au moyen mentionné au point 1.<br/>
<br/>
              7. Il n'appartient pas à la juridiction administrative, lorsqu'elle est saisie d'une question préjudicielle en appréciation de validité d'un acte administratif, de trancher d'autres questions que celle qui lui a été renvoyée par l'autorité judiciaire. Les conclusions de Mme A... tendant à ce qu'il soit enjoint au ministre des solidarités et de la santé d'abroger l'article 7 des statuts du régime d'assurance invalidité-décès de la section professionnelle des auxiliaires médicaux ne sont dès lors, en tout état de cause, pas recevables.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A... la somme que cette caisse demande au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il est déclaré que l'exception d'illégalité de l'article 7 des statuts du régime d'assurance invalidité-décès de la section professionnelle des auxiliaires médicaux de la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes soulevée par Mme A... devant le tribunal judiciaire de Bobigny n'est pas fondée.<br/>
Article 2 : Les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B... A..., à la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes, au ministre des solidarités et de la santé et au tribunal judiciaire de Bobigny.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-02-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LES PROTOCOLES. DROIT AU RESPECT DE SES BIENS (ART. 1ER DU PREMIER PROTOCOLE ADDITIONNEL). - CHAMP D'APPLICATION - PRESTATIONS SERVIES PAR UN RÉGIME D'ASSURANCE SOCIALE EN MATIÈRE D'INVALIDITÉ - 1) INCLUSION [RJ1] - 2) ESPÈCE - BÉNÉFICE D'UNE ALLOCATION JOURNALIÈRE D'INAPTITUDE RÉSERVÉE AUX PERSONNES QUI SE SONT ACQUITTÉES DU PAIEMENT DE LEURS COTISATIONS - MÉCONNAISSANCE DE L'ARTICLE 1P1 - ABSENCE, ALORS MÊME QUE CES PERSONNES SE SERAIENT ACQUITTÉES, PAR LE PASSÉ, D'UNE PARTIE DES COTISATIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-03 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCE INVALIDITÉ. - CHAMP D'APPLICATION DE L'ARTICLE 1P1 - PRESTATIONS SERVIES PAR UN RÉGIME D'ASSURANCE SOCIALE EN MATIÈRE D'INVALIDITÉ - 1) INCLUSION [RJ1] - 2) ESPÈCE - BÉNÉFICE D'UNE ALLOCATION JOURNALIÈRE D'INAPTITUDE RÉSERVÉE AUX PERSONNES QUI SE SONT ACQUITTÉES DU PAIEMENT DE LEURS COTISATIONS - MÉCONNAISSANCE DE L'ARTICLE 1P1 - ABSENCE, ALORS MÊME QUE CES PERSONNES SE SERAIENT ACQUITTÉES, PAR LE PASSÉ, D'UNE PARTIE DES COTISATIONS.
</SCT>
<ANA ID="9A"> 26-055-02-01 1) Les prestations servies par un régime d'assurance sociale en matière d'invalidité constituent, pour les personnes qui en remplissent les conditions légales, des biens au sens de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (art. 1P1).,,,2) En prévoyant que le droit à l'allocation journalière d'inaptitude ne peut être ouvert aux personnes qui ne se sont pas acquittées du paiement de tout ou partie de leurs cotisations, et le cas échéant des majorations de retard, l'article 7 des statuts du régime d'assurance invalidité-décès de la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes (CARPINKO), fixe une condition pour pouvoir bénéficier de cette allocation. Les personnes qui remplissent cette condition disposent, sous réserve de satisfaire également aux autres conditions auxquelles le bénéfice de cette allocation est subordonné, d'un droit, constitutif d'un bien protégé par l'article 1P1, à percevoir les prestations prévues par ce régime. En revanche, tel n'est pas le cas, en principe, des personnes qui ne remplissent pas cette condition, alors même qu'elles se seraient acquittées, par le passé, d'une partie des cotisations. L'article contesté ne peut être regardé, en ce qu'il subordonne à cette condition le bénéfice de l'allocation journalière d'inaptitude, comme contraire à l'article 1P1.</ANA>
<ANA ID="9B"> 62-04-03 1) Les prestations servies par un régime d'assurance sociale en matière d'invalidité constituent, pour les personnes qui en remplissent les conditions légales, des biens au sens de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (art. 1P1).,,,2) En prévoyant que le droit à l'allocation journalière d'inaptitude ne peut être ouvert aux personnes qui ne se sont pas acquittées du paiement de tout ou partie de leurs cotisations, et le cas échéant des majorations de retard, l'article 7 des statuts du régime d'assurance invalidité-décès de la caisse autonome de retraite et de prévoyance des infirmiers, masseurs-kinésithérapeutes, pédicures-podologues, orthophonistes et orthoptistes (CARPINKO), fixe une condition pour pouvoir bénéficier de cette allocation. Les personnes qui remplissent cette condition disposent, sous réserve de satisfaire également aux autres conditions auxquelles le bénéfice de cette allocation est subordonné, d'un droit, constitutif d'un bien protégé par l'article 1P1, à percevoir les prestations prévues par ce régime. En revanche, tel n'est pas le cas, en principe, des personnes qui ne remplissent pas cette condition, alors même qu'elles se seraient acquittées, par le passé, d'une partie des cotisations. L'article contesté ne peut être regardé, en ce qu'il subordonne à cette condition le bénéfice de l'allocation journalière d'inaptitude, comme contraire à l'article 1P1.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cour EDH, Gd. ch., 13 décembre 2016, Béláné Nagy c. Hongrie, n° 53080/13.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
