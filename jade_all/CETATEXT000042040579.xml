<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040579</ID>
<ANCIEN_ID>JG_L_2020_06_000000441206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040579.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/06/2020, 441206, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441206.20200622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 15 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. Q... C..., M. B... K..., M. N..., M. B... H..., M. V... B..., M. J... P..., M. S..., M. R..., M. T... A... U..., Mme G... M'sa, Mme E... L..., Mme I... O..., Mme D... F... et M. A... M... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2020-643 du 27 mai 2020 relatif au report du second tour du renouvellement général des conseillers municipaux et communautaires, des conseillers de Paris et des conseillers métropolitains de Lyon de 2020 et à l'adaptation du décret du 9 juillet 1990 à l'état d'urgence sanitaire, notamment son article 6 relatif au vote par procuration ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, les dispositions contestées s'appliqueront au scrutin prévu le 28 uin 2020 et, en second lieu, le maintien de la validité des procurations établies en vue d'un scrutin qui devait se tenir le 22 mars 2020 risque de changer les résultats de l'élection et de porter atteinte à la sincérité du scrutin ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
              - l'article 6 du décret attaqué méconnaît les dispositions de l'article L. 71 du code électoral dès lors qu'il est susceptible de permettre à un mandataire de voter par procuration alors même que le mandant ne remplit pas les conditions d'empêchement énoncées par le code électoral voire de voter pour une personne éventuellement décédée durant l'épidémie de covid-19 ;<br/>
              - il est contraire au principe de sincérité du scrutin et à la liberté de l'électeur dès lors que les procurations, qui avaient été établies pour une durée déterminée, seront utilisées plus de trois mois après la date initiale du second tour ; <br/>
              - il est contraire à l'article 1989 du code civil selon lequel le mandataire ne peut agir au-delà de ce qui est prévu par son mandat. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
- la Constitution ;<br/>
- le code civil ;<br/>
- le code électoral ;<br/>
- le code de la santé publique ;<br/>
- la loi n° 2020-290 du 23 mars 2020 ;<br/>
- la loi n° 2020-546 du 11 mai 2020 ;<br/>
- le décret n° 2020-267 du 17 mars 2020 ;	<br/>
- le décret n° 2020-642 du 27 mai 2020 ;<br/>
- le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
              Sur les circonstances et le cadre juridique du litige :<br/>
              2. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion, mesures assouplies à la date de la présente ordonnance. Le législateur, par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, puis, par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus.<br/>
              3. Aux termes de l'article 19 de la loi du 23 mars 2020 : " I. - Lorsque, à la suite du premier tour organisé le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires, des conseillers de Paris et des conseillers métropolitains de Lyon, un second tour est nécessaire pour attribuer les sièges qui n'ont pas été pourvus, ce second tour, initialement fixé au 22 mars 2020, est reporté au plus tard en juin 2020, en raison des circonstances exceptionnelles liées à l'impérative protection de la population face à l'épidémie de covid-19. Sa date est fixée par décret en conseil des ministres, pris le mercredi 27 mai 2020 au plus tard si la situation sanitaire permet l'organisation des opérations électorales au regard, notamment, de l'analyse du comité de scientifiques institué sur le fondement de l'article L. 3131-19 du code de la santé publique. / Les déclarations de candidature à ce second tour sont déposées au plus tard le mardi qui suit la publication du décret de convocation des électeurs. / Si la situation sanitaire ne permet pas l'organisation du second tour au plus tard au mois de juin 2020, le mandat des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains concernés est prolongé pour une durée fixée par la loi. Les électeurs sont convoqués par décret pour les deux tours de scrutin, qui ont lieu dans les trente jours qui précèdent l'achèvement des mandats ainsi prolongés. La loi détermine aussi les modalités d'entrée en fonction des conseillers municipaux élus dès le premier tour dans les communes de moins de 1000 habitants pour lesquelles le conseil municipal n'a pas été élu au complet. / Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution (...) / III. - Les conseillers municipaux et communautaires élus dès le premier tour organisé le 15 mars 2020 entrent en fonction à une date fixée par décret au plus tard au mois de juin 2020, aussitôt que la situation sanitaire le permet au regard de l'analyse du comité de scientifiques. (...) / IV. - Par dérogation à l'article L. 227 du code électoral : / 1° Dans les communes pour lesquelles le conseil municipal a été élu au complet, les conseillers municipaux en exercice avant le premier tour conservent leur mandat jusqu'à l'entrée en fonction des conseillers municipaux élus au premier tour. Le cas échéant, leur mandat de conseiller communautaire est également prorogé jusqu'à cette même date ; / 2° Dans les communes, autres que celles mentionnées au 3° du présent IV, pour lesquelles le conseil municipal n'a pas été élu au complet, les conseillers municipaux en exercice avant le premier tour conservent leur mandat jusqu'au second tour. Le cas échéant, leur mandat de conseiller communautaire est également prorogé jusqu'au second tour, sous réserve du 3 du VII ; (...) ". Aux termes de l'article L. 3131-19 du code de la santé publique " En cas de déclaration de l'état d'urgence sanitaire, il est réuni sans délai un comité de scientifiques. Son président est nommé par décret du Président de la République. Ce comité comprend deux personnalités qualifiées respectivement nommées par le Président de l'Assemblée nationale et le Président du Sénat ainsi que des personnalités qualifiées nommées par décret. Le comité rend périodiquement des avis sur l'état de la catastrophe sanitaire, les connaissances scientifiques qui s'y rapportent et les mesures propres à y mettre un terme, y compris celles relevant des articles L. 3131-15 à L. 3131-17, ainsi que sur la durée de leur application. Ces avis sont rendus publics sans délai. Le comité est dissous lorsque prend fin l'état d'urgence sanitaire ".<br/>
              4. Le décret du 17 mars 2020 portant report du second tour du renouvellement des conseillers municipaux et communautaires, des conseillers de Paris et des conseillers métropolitains de Lyon initialement fixé au 22 mars 2020 par le décret n° 2019-928 du 4 septembre 2019, a abrogé l'article 6 de ce décret qui fixait la date de ce second tour au dimanche 22 mars 2020. Par un décret n° 2020-642 du 27 mai 2020, pris en application du I de l'article 19 de la loi précitée du 23 mars 2020, les électeurs sont convoqués le dimanche 28 juin 2020 en vue de procéder au second tour du scrutin dont le premier tour a eu lieu le 15 mars 2020. Les requérants demandent la suspension du décret n° 2020-643 du 27 mai 2020 relatif au report du second tour du renouvellement général des conseillers municipaux et communautaires, des conseillers de Paris et des conseillers métropolitains de Lyon de 2020 et à l'adaptation du décret du 9 juillet 1990 à l'état d'urgence sanitaire. Eu égard aux écritures des requérants, ils doivent être regardés comme demandant uniquement la suspension de l'article 6 de ce décret aux termes duquel : " Les procurations établies en vue du second tour initialement prévu le 22 mars 2020 restent valables pour le second tour reporté ".<br/>
              5. Sur le fondement de l'article 19 de la loi du 23 mars 2020, le décret n° 2020-642 du 27 mai 2020 a fixé le second tour des élections municipales au 28 juin 2020. Pour permettre d'organiser au mieux la tenue de ce scrutin dans un contexte de sortie de la crise sanitaire, compte tenu des contraintes qui continuent de peser notamment sur les services publics, les dispositions contestées ont maintenu la validité des procurations établies en vue du second tour prévu le 22 mars pour le second tour fixé désormais à la date du 28 juin. La circonstance que les conditions légales, posées par l'article L. 71 du code électoral et permettant d'établir les procurations, pourraient, pour certains électeurs, ne plus être réunies à la date du 28 juin, n'est pas de nature à affecter en elle-même la sincérité de l'ensemble des opérations électorales. En effet, le mandant a toujours la faculté de résilier sa procuration ou, s'il ne la résilie pas, peut toujours exercer personnellement son droit de vote s'il se présente avant son mandataire au bureau de vote, en vertu respectivement des articles L. 75 et L. 76 du code électoral. En cas de décès du mandant, hypothèse invoquée par les requérants, l'article L. 77 du même code prévoit l'annulation de plein droit de la procuration. Au surplus, il appartiendra au juge électoral dans chaque litige, s'il est saisi de griefs en ce sens, d'apprécier la validité des suffrages exprimés par procurations et d'en tirer les conséquences sur les résultats du scrutin.  <br/>
              6. Les requérants soutiennent également que la situation aura changé entre le moment où les procurations ont été établies et la date du second tour ainsi reporté au 28 juin et que cette circonstance affecterait en elle-même la sincérité du scrutin. Toutefois, les mandants qui n'auront pas résilié leur procuration ou qui n'auraient pas décidé d'exprimer directement leur suffrage sont en principe toujours à même de transmettre à leurs mandataires leurs indications de vote. Par ailleurs, le report au 28 juin du second tour des élections municipales est, par lui-même, sans effet sur les relations entre le mandant et le mandataire et, par suite, n'est pas susceptible d'entraîner une méconnaissance de l'article 1989 du code civil en vertu duquel le mandataire ne peut rien faire au-delà de ce qui est porté dans son mandat.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la condition d'urgence, qu'en l'absence manifestement de moyen propre, en l'état de l'instruction, à faire naître un doute sérieux sur la légalité des dispositions contestées, il y a lieu de rejeter la requête de M C... et autres par application des dispositions précitées de l'article L. 522-3 du code de justice administrative, ensemble leurs conclusions tendant à l'application des dispositions de l'article L. 761-1 du même code.  <br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. Q... C... et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Q... C..., premier requérant dénommé. <br/>
Copie en sera adressée au ministre de l'intérieur et à la ministre de l'outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
