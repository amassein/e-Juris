<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042409945</ID>
<ANCIEN_ID>JG_L_2020_10_000000424976</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/40/99/CETATEXT000042409945.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 07/10/2020, 424976, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424976</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:424976.20201007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés le 19 octobre 2018 et le 22 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, l'association One Voice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions implicites de rejet, née le 20 août 2018, résultant du silence gardé par le ministre d'Etat, ministre de la transition écologique et solidaire et par le ministre de l'agriculture et de l'alimentation sur ses recours gracieux tendant à l'adoption d'un nouvel arrêté assurant le même degré de protection que l'arrêté du 3 mai 2017 fixant les caractéristiques générales et les règles de fonctionnement des établissements présentant au public des spécimens vivants de cétacés ;<br/>
<br/>
              2°) d'enjoindre au ministre d'Etat, ministre de la transition écologique et solidaire et au ministre de l'agriculture et de l'alimentation de prendre, sous une astreinte de 500 euros par jour de retard, cet arrêté dans le délai de trois mois à compter de la notification de la décision à venir ; <br/>
<br/>
              3°) de condamner l'Etat au versement de la somme de 500 000 euros à titre de dommages et intérêts en réparation du préjudice moral porté aux intérêts qu'elle défend ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association One Voice ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 septembre 2020, présentée par l'association One Voice.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier qu'en application des dispositions de l'article R. 413-9 du code de l'environnement, les ministres chargés de la protection de la nature et de l'agriculture ont pris, le 3 mai 2017, un arrêté fixant les caractéristiques générales et les règles de fonctionnement des établissements présentant au public des spécimens vivants de cétacés et abrogeant leur précédent arrêté du 24 août 1981. Par une décision du 29 janvier 2018, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêté du 3 mai 2017. L'association One Voice demande au Conseil d'Etat d'annuler le refus implicite né du silence gardé par le ministre de la transition écologique et solidaire et par le ministre de l'agriculture et de l'alimentation sur sa demande tendant à ce que soit pris un nouvel arrêté fixant les caractéristiques générales et les règles de fonctionnement des établissements présentant au public des spécimens vivants de cétacés. <br/>
<br/>
              2. En premier lieu, l'annulation de l'arrêté du 3 mai 2017 par une décision du Conseil d'Etat statuant au contentieux ayant eu pour effet de remettre en vigueur l'arrêté du 24 août 1981 relatif aux règles de fonctionnement, au contrôle et aux caractéristiques auxquelles doivent satisfaire les installations abritant des cétacés vivants, le refus de prendre un nouvel arrêté a pour conséquence le maintien en vigueur du droit résultant de ce dernier arrêté. Par suite, la requérante n'est, en tout état de cause, pas fondée à soutenir que le refus des ministres compétents de prendre un nouvel arrêté complétant ou abrogeant l'arrêté du 24 août 1981 méconnaitrait le principe de non-régression énoncé par l'article L. 110-1 du code de l'environnement. <br/>
<br/>
              3. En deuxième lieu, il ne résulte pas de l'article 13 du traité sur le fonctionnement de l'Union européenne qu'elle invoque, qui se borne à prévoir que " les États membres tiennent pleinement compte des exigences du bien-être des animaux en tant qu'êtres sensibles ", que le refus de prendre un arrêté modifiant ou se substituant à l'arrêté du 24 août 1981 méconnaîtrait les exigences du droit de l'Union.  <br/>
<br/>
              4. En troisième lieu, l'association requérante n'est, en tout état de cause, pas fondée à soutenir que le refus attaqué méconnaitrait le principe de promotion du développement durable tel qu'il résulte des dispositions de la Charte de l'environnement, notamment de son 3ème considérant et de son article 6, ni les principes posés aux articles L. 110-1 et L. 110-2 du code de l'environnement.<br/>
<br/>
              5. En dernier lieu, l'article L. 214-1 du code rural et de la pêche maritime dispose que : " Tout animal étant un être sensible doit être placé par son propriétaire dans des conditions compatibles avec les impératifs biologiques de son espèce ". Aux termes de l'article L. 214-2 du même code : " Tout homme a le droit de détenir des animaux dans les conditions définies à l'article L. 214-1 et de les utiliser dans les conditions prévues à l'article L. 214-3 (...). Les établissements ouverts au public pour l'utilisation d'animaux sont soumis au contrôle de l'autorité administrative qui peut prescrire des mesures pouvant aller jusqu'à la fermeture de l'établissement, indépendamment des poursuites pénales qui peuvent être exercées au titre de la loi précitée. Un décret en Conseil d'Etat précise les modalités d'application du présent article et de l'article L. 214-1. " L'article L. 214-3 du même code dispose que : " Il est interdit d'exercer des mauvais traitements envers les animaux domestiques ainsi qu'envers les animaux sauvages apprivoisés ou tenus en captivité. Des décrets en Conseil d'Etat déterminent les mesures propres à assurer la protection de ces animaux contre les mauvais traitements ou les utilisations abusives et à leur éviter des souffrances lors des manipulations inhérentes aux diverses techniques d'élevage, de parcage, de transport et d'abattage des animaux. (...) ". Enfin, l'article R. 214-17 du même code dispose que : " Il est interdit à toute personne qui, à quelque fin que ce soit, élève, garde ou détient des animaux domestiques ou des animaux sauvages apprivoisés ou tenus en captivité : / (...) / 3° De les placer et de les maintenir dans un habitat ou un environnement susceptible d'être, en raison de son exiguïté, de sa situation inappropriée aux conditions climatiques supportables par l'espèce considérée ou de l'inadaptation des matériels, installations ou agencements utilisés, une cause de souffrances, de blessures ou d'accidents ; / 4° D'utiliser, sauf en cas de nécessité absolue, des dispositifs d'attache ou de contention ainsi que de clôtures, des cages ou plus généralement tout mode de détention inadaptés à l'espèce considérée ou de nature à provoquer des blessures ou des souffrances. / (...) / Si, du fait de mauvais traitements ou d'absence de soins, des animaux domestiques ou des animaux sauvages apprivoisés ou tenus en captivité sont trouvés gravement malades ou blessés ou en état de misère physiologique, le préfet prend les mesures nécessaires pour que la souffrance des animaux soit réduite au minimum ; il peut ordonner l'abattage ou la mise à mort éventuellement sur place. Les frais entraînés par la mise en oeuvre de ces mesures sont à la charge du propriétaire. " <br/>
<br/>
              6. Si l'association requérante soutient que le régime mis en place par l'arrêté du 24 août 1981 méconnaitrait ces dispositions en ce qu'il ne ferait pas obstacle à ce que des animaux détenus soient victimes de traitement prohibés par les dispositions citées plus haut, elle n'apporte, à l'appui de ces allégations, que des considérations générales et fait valoir qu'un régime plus restrictif, tel que celui qui était prévu par l'arrêté du 3 mai 2017, serait plus opportun. Il ne ressort cependant pas des pièces du dossier que l'arrêté du 24 août 1981 méconnaîtrait les dispositions rappelées plus haut, eu égard aux prérogatives dont dispose l'administration, notamment sur le fondement de l'article R. 214-17 précité, pour faire cesser d'éventuels mauvais traitements. Il ne ressort pas davantage des pièces du dossier que le refus attaqué serait entaché d'une erreur manifeste d'appréciation. Il en résulte que les ministres saisis par l'association n'étaient pas tenus de modifier ou de remplacer l'arrêté du 24 août 1981.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la requête doit être rejetée, y compris ses conclusions à fins indemnitaires, tendant au prononcé d'une injonction ou présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association One Voice est rejetée. <br/>
Article 2: La présente décision sera notifiée à l'association One Voice, à la ministre de la transition écologique et au ministre de l'agriculture et de l'alimentation.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
