<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038151185</ID>
<ANCIEN_ID>J6_L_2019_02_000001802068</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/11/CETATEXT000038151185.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de MARSEILLE, 4ème chambre - formation à 3, 05/02/2019, 18MA02068, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-05</DATE_DEC>
<JURIDICTION>CAA de MARSEILLE</JURIDICTION>
<NUMERO>18MA02068</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre - formation à 3</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. ANTONETTI</PRESIDENT>
<AVOCATS>CABINET ARSENE TAXAND</AVOCATS>
<RAPPORTEUR>M. André  MAURY</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme BOYER</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       L'association Carcassonne Aficion a demandé au tribunal administratif de Montpellier d'annuler la décision du 5 décembre 2016 par laquelle le directeur départemental des finances publiques de l'Aude a rejeté sa réclamation, de prononcer la décharge des rappels de taxe sur la valeur ajoutée à laquelle elle a été assujettie au titre de la période du 1er janvier 2013 au 31 décembre 2014, ainsi que des pénalités correspondantes, de condamner l'Etat aux entiers dépens en application de l'article R. 761-1 du code de justice administrative et de mettre à la charge de l'Etat une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Par un jugement n° 1700566 du 5 mars 2018, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête et un mémoire, enregistrés le 4 mai 2018 et le 29 octobre 2018, l'association Carcassonne Aficion, représentée par Me A..., demande à la Cour :<br/>
<br/>
       1°) d'annuler le jugement du tribunal administratif de Montpellier n° 1700566 du 5 mars 2018 ;<br/>
<br/>
       2°) d'annuler la décision du 5 décembre 2016 par laquelle le directeur départemental des finances publiques de l'Aude a rejeté sa réclamation ;<br/>
<br/>
       3°) de prononcer la décharge des rappels de taxe sur la valeur ajoutée à laquelle elle a été assujettie au titre de la période du 1er janvier 2013 au 31 décembre 2014, ainsi que des pénalités correspondantes ;<br/>
<br/>
       4°) de condamner l'Etat aux entiers dépens en application de l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
       5°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Elle soutient que conformément aux dispositions du b du 1° du 7 de l'article 261 du code général des impôts, elle n'a pas à être assujettie à la taxe sur la valeur ajoutée dès lors que sa gestion est désintéressée, qu'elle n'entretient aucune relation privilégiée avec des entreprises, qu'elle n'exerce pas une activité identique à celle d'une entreprise commerciale dans une même zone géographique s'adressant au même public et que ses conditions d'exploitation ne sont pas comparables à celles qui ont cours dans les autres villes taurines du sud-est ou du sud-ouest.<br/>
<br/>
<br/>
       Par un mémoire en défense, enregistré le 24 septembre 2018, le ministre de l'action et des comptes publics, conclut au rejet de la requête.<br/>
<br/>
       Il fait valoir qu'aucun des moyens de la requête n'est fondé. <br/>
<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code de justice administrative.<br/>
<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. Maury, <br/>
       - et les conclusions de Mme Boyer, rapporteur public.<br/>
<br/>
<br/>
       Considérant ce qui suit : <br/>
<br/>
<br/>
       1. L'association Carcassonne Aficion a fait l'objet d'une vérification de comptabilité du 16 juin au 2 septembre 2015, portant sur la période allant du 1er janvier 2013 au 31 décembre 2014, à l'issue de laquelle des droits supplémentaires de taxe sur la valeur ajoutée, assorties des pénalités de l'article 1759 du code général des impôts ont été mises en recouvrement le 15 septembre 2016. La réclamation de l'association Carcassonne Aficion a été rejetée par une décision du 5 décembre 2016. Celle-ci a alors saisi le tribunal administratif de Montpellier qui par jugement du 5 mars 2018 a rejeté sa demande. C'est de ce jugement dont elle relève appel.<br/>
       2. En premier lieu, aux termes de l'article 256 du code général des impôts : " I. Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel ". Aux termes de l'article 261 du code général des impôts : " Sont exonérés de la taxe sur la valeur ajoutée:/ (...) 7. (Organismes d'utilité générale) :/ 1° (...) b. les opérations faites au bénéfice de toutes personnes par des oeuvres sans but lucratif qui présentent un caractère social ou philanthropique et dont la gestion est désintéressée, lorsque les prix pratiqués ont été homologués par l'autorité publique ou que des opérations analogues ne sont pas couramment réalisées à des prix comparables par des entreprises commerciales, en raison notamment du concours désintéressé des membres de ces organismes ou des contributions publiques ou privées dont ils bénéficient. ".<br/>
<br/>
       3. Il résulte de ces dispositions que les associations régies par la loi du 1er juillet 1901 ne sont exonérées de la taxe sur la valeur ajoutée que si, d'une part, leur gestion présente un caractère désintéressé et, d'autre part, les services qu'elles rendent ne sont pas offerts en concurrence dans la même zone géographique d'attraction avec ceux proposés au même public par des entreprises commerciales exerçant une activité identique. Toutefois, même dans le cas où l'association intervient dans un domaine d'activité et dans un secteur géographique où existent des entreprises commerciales, l'exonération de taxe sur la valeur ajoutée lui est acquise, si elle exerce son activité dans des conditions différentes de celles des entreprises commerciales, soit en répondant à certains besoins insuffisamment satisfaits par le marché, soit en s'adressant à un public qui ne peut normalement accéder aux services offerts par les entreprises commerciales, notamment en pratiquant des prix inférieurs à ceux du secteur concurrentiel et à tout le moins des tarifs modulés en fonction de la situation des bénéficiaires, sous réserve de ne pas recourir à des méthodes commerciales excédant les besoins de l'information du public sur les services qu'elle offre. Sous réserve des cas où la loi attribue la charge de la preuve au contribuable, il appartient au juge de l'impôt, au vu de l'instruction et compte tenu, le cas échéant, de l'abstention d'une des parties à produire les éléments qu'elle est seule en mesure d'apporter et qui ne sauraient être réclamés qu'à elle-même, d'apprécier si la situation du contribuable entre dans le champ de l'assujettissement à l'impôt ou, le cas échéant, s'il remplit les conditions légales d'une exonération.<br/>
<br/>
       4. Eu égard aux services qu'elle fournit aux amateurs de spectacles tauromachiques, la zone géographique d'attraction de l'association requérante ne peut être limitée à 50 km autour de Carcassonne ainsi qu'elle le soutient, et doit être regardée comme couvrant à tout le moins l'ensemble du territoire de la région Occitanie. Dans cette zone, ces activités sont également proposées par des entreprises commerciales, en particulier dans les arènes des villes de Béziers, Nîmes et Arles où sont organisés des spectacles tauromachiques, notamment sous la forme de " novilladas " à l'instar de ceux proposés par l'association. Contrairement à ce qu'elle indique les tarifs pratiqués à Carcassonne (quinze à quarante-cinq euros) sont à peu près équivalents avec les " novilladas " de Nîmes (quinze à quarante-huit euros et d'Arles (quatorze à cinquante-deux euros) et légèrement inférieurs a ceux pratiqués à Béziers (vingt-huit à quarante-huit euros), la gratuité étant prévue pour les moins de douze ans en Arles, comme à Carcassonne. Ainsi en dépit de la capacité d'accueil moindre des arènes de Carcassonne et du plus faible nombre de spectacles offerts par l'association requérante, celle-ci doit être regardée comme offrant des services en concurrence effective avec ceux proposés au même public par des entreprises commerciales exerçant une activité identique dans des conditions de prix comparables. L'association ne peut dès lors être regardée comme exerçant son activité dans des conditions différentes de celles des entreprises commerciales. En ne remplissant pas cette condition, il ne résulte donc pas de l'instruction que l'association requérante puisse prétendre au bénéfice de l'exonération prévue au b du 1° du 7 de l'article 261 du code général des impôts.<br/>
<br/>
       5. Il résulte de tout ce qui précède, que l'association Carcassonne Aficion n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier a rejeté sa demande. Par suite, la requête doit être rejetée, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
D É C I D E :<br/>
<br/>
<br/>
<br/>
Article 1er : La requête de l'association Carcassonne Aficion est rejetée.<br/>
<br/>
Article 2 : Le présent arrêt sera notifié à l'association Carcassonne Aficion et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée à la direction de contrôle fiscal sud-est.<br/>
<br/>
<br/>
<br/>
       Délibéré après l'audience du 22 janvier 2019, où siégeaient :<br/>
<br/>
       - M. Antonetti, président,<br/>
       - M. Barthez , président assesseur, <br/>
       - M. Maury, premier conseiller.<br/>
<br/>
       Lu en audience publique, le 5 février 2019.<br/>
<br/>
<br/>
2<br/>
N° 18MA02068<br/>
		nc<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-06-02-01 Contributions et taxes. Taxes sur le chiffre d'affaires et assimilées. Taxe sur la valeur ajoutée. Personnes et opérations taxables.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
