<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030556712</ID>
<ANCIEN_ID>JG_L_2015_05_000000366713</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/55/67/CETATEXT000030556712.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 06/05/2015, 366713</TITRE>
<DATE_DEC>2015-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366713</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:366713.20150506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>
<br clear="none"/>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 11 mars et 11 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le syndicat des copropriétaires " Arcades des Champs Elysées, dont le siège est 76/78 avenue des Champs Elysées à Paris (75008) ; le syndicat des copropriétaires " Arcades des Champs Elysées " demande au Conseil d'Etat : <br clear="none"/>
<br clear="none"/>1°) d'annuler la délibération n° 2012-475 du 3 janvier 2013 par laquelle la Commission nationale de l'informatique et des libertés (CNIL) a prononcé à son encontre une sanction pécuniaire d'un euro, a enjoint au responsable du traitement de mettre fin au caractère continu du traitement de vidéosurveillance en litige et a décidé de rendre publique sa décision sur son site Internet et sur le site Légifrance ; <br clear="none"/>
<br clear="none"/>2°) de mettre à la charge de l'Etat une somme de 4000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br clear="none"/>
<br clear="none"/>
<br clear="none"/>Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>Vu la loi n° 78-17 du 6 janvier 1978 ; <br clear="none"/>
<br clear="none"/>Vu le décret n° 67-223 du 17 mars 1967 ; <br clear="none"/>
<br clear="none"/>Vu le décret n° 2005-1309 du 20 octobre 2005 ; <br clear="none"/>
<br clear="none"/>Vu le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>- le rapport de Mme Anne Iljic, auditeur, <br clear="none"/>
<br clear="none"/>- les conclusions de M. Edouard Crépey, rapporteur public ;<br clear="none"/>La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du Syndicat des copropriétaires " Arcades des champs Elysées " ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>1. Considérant qu'il résulte de l'instruction que, saisie d'une plainte de plusieurs agents de sécurité affectés à la surveillance du bâtiment à usage mixte d'habitation et de commerce dont le syndicat des copropriétaires " Arcades des Champs-Elysées " assure la gestion, la présidente de la Commission nationale de l'informatique et des libertés (CNIL) a, par une décision du 19 juillet 2012, mis en demeure ce syndicat de supprimer la caméra de vidéosurveillance qu'il avait mise en place afin de filmer le poste de travail des agents de sécurité, dans lequel se trouvent les écrans de contrôle des caméras placées dans le reste du bâtiment ; que, par une décision du 21 août 2012, la présidente de la CNIL a décidé de faire procéder à des vérifications relatives au traitement mis en oeuvre par le syndicat requérant, qui se sont déroulées dans les locaux du syndicat le 4 septembre 2012 après que celui-ci eut déclaré auprès de la CNIL, le 30 août 2012, un " dispositif de vidéosurveillance installé dans un local de PC de sécurité " ; que, constatant que le syndicat ne s'était pas conformé à la mise en demeure qui lui avait été adressée, la formation restreinte de la CNIL a, par une délibération du 3 janvier 2013, enjoint au responsable du traitement de mettre fin au caractère continu de ce traitement et a infligé au syndicat une sanction pécuniaire d'un montant d'un euro, assortie d'une sanction complémentaire de publication sur son site Internet et sur le site Légifrance ; que le syndicat des copropriétaires " Arcades des Champs Elysées " demande l'annulation de cette délibération ;<br clear="none"/>
<br clear="none"/>2. Considérant qu'en vertu de l'article 18 de la loi du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis, le syndic représente le syndicat des copropriétaires en justice ; qu'aux termes de l'article 55 du décret du 17 mars 1967 pris pour l'application de cette loi : " Le syndic ne peut agir en justice au nom du syndicat sans y avoir été autorisé par une décision de l'assemblée générale. / Une telle autorisation n'est pas nécessaire pour les actions en recouvrement de créance, la mise en oeuvre des voies d'exécution forcée à l'exception de la saisie en vue de la vente d'un lot, les mesures conservatoires et les demandes qui relèvent des pouvoirs de juge des référés, ainsi que pour défendre aux actions intentées contre le syndicat. / Dans tous les cas, le syndic rend compte à la prochaine assemblée générale des actions introduites. " ; qu'il résulte de ces dispositions que, dans les cas où une autorisation est requise, le syndic, agissant au nom de la copropriété, est tenu de disposer, sous peine d'irrecevabilité de sa demande, d'une autorisation formelle de l'assemblée générale des copropriétaires pour agir en justice en son nom, habilitation qui doit préciser l'objet et la finalité du contentieux engagé ; que le pouvoir ainsi donné au syndic est compris dans les limites qui ont, le cas échéant, été fixées par la décision de l'assemblée générale ; <br clear="none"/>
<br clear="none"/>3. Considérant qu'il résulte de l'instruction et n'est pas contesté par les parties que la requête introduite devant le Conseil d'Etat par le syndic au nom du syndicat des copropriétaires " Arcades des Champs Elysées " contre la délibération du 3 janvier 2013 de la formation restreinte de la CNIL n'a été précédée d'aucune autorisation formelle de l'assemblée générale des copropriétaires ; qu'il suit de là que le syndic ne justifiait d'aucune qualité pour agir ; qu'ainsi, sans qu'il soit besoin d'examiner les moyens soulevés par le syndicat des copropriétaires " Arcades des Champs Elysées ", sa requête doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>D E C I D E :<br clear="none"/>--------------<br clear="none"/>Article 1er : La requête du syndicat des copropriétaires " Arcades des Champs Elysées " est rejetée. <br clear="none"/>
<br clear="none"/>Article 2 : La présente décision sera notifiée au syndicat des copropriétaires " Arcades des Champs Elysées " et à la Commission nationale de l'informatique et des libertés.<br clear="none"/>Copie pour information en sera adressée au Premier ministre. <br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-09 LOGEMENT. - SYNDIC DE COPROPRIÉTÉ - OBLIGATION, À PEINE D'IRRECEVABILITÉ, DE DISPOSER D'UNE HABILITATION FORMELLE ET PRÉCISE POUR AGIR EN JUSTICE - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-05-005 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. REPRÉSENTATION DES PERSONNES MORALES. - SYNDIC DE COPROPRIÉTÉ - OBLIGATION, À PEINE D'IRRECEVABILITÉ, DE DISPOSER D'UNE HABILITATION FORMELLE ET PRÉCISE - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 38-09 Il résulte des dispositions de l'article 18 de la loi n° 65-557 du 10 juillet 1965 et de l'article 55 du décret n° 67-223 du 17 mars 1967 que, dans les cas où une autorisation est requise, le syndic, agissant au nom de la copropriété, est tenu de disposer, sous peine d'irrecevabilité de sa demande, d'une autorisation formelle de l'assemblée générale des copropriétaires pour agir en justice en son nom, habilitation qui doit préciser l'objet et la finalité du contentieux engagé. Le pouvoir ainsi donné au syndic est compris dans les limites qui ont, le cas échéant, été fixées par la décision de l'assemblée générale.</ANA>
<ANA ID="9B"> 54-01-05-005 Il résulte des dispositions de l'article 18 de la loi n° 65-557 du 10 juillet 1965 et de l'article 55 du décret n° 67-223 du 17 mars 1967 que, dans les cas où une autorisation est requise, le syndic, agissant au nom de la copropriété, est tenu de disposer, sous peine d'irrecevabilité de sa demande, d'une autorisation formelle de l'assemblée générale des copropriétaires pour agir en justice en son nom, habilitation qui doit préciser l'objet et la finalité du contentieux engagé. Le pouvoir ainsi donné au syndic est compris dans les limites qui ont, le cas échéant, été fixées par la décision de l'assemblée générale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 9 juillet 2008, Syndicat des copropriétaires de l'immeuble 54 rue Diderot, n°297370, T. p. 849.


</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
