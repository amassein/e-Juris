<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039168438</ID>
<ANCIEN_ID>JG_L_2019_10_000000422197</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/84/CETATEXT000039168438.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 02/10/2019, 422197, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422197</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422197.20191002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 11 juillet 2018, 1er février 2019 et 1er mars 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Groupement d'information et de soutien sur les questions sexuées et sexuelles (GISS) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le protocole national de diagnostic et de soins intitulé " Insensibilités aux androgènes " daté du 21 décembre 2017 et publié le 10 janvier 2018 par la Haute Autorité de santé sur son site internet, ainsi que les décisions de cette autorité de valider et de diffuser ce protocole ;<br/>
<br/>
              2°) d'enjoindre à la Haute Autorité de santé de retirer ce protocole de son site internet dans un délai d'un mois à compter de la décision à intervenir, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de la Haute Autorité de santé la somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de l'association Groupement d'information et de soutien sur les questions sexuees et sexuelles ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'association Groupement d'information et de soutien sur les questions sexuées et sexuelles demande, dans le dernier état de ses écritures, l'annulation pour excès de pouvoir du protocole national de diagnostic et de soins intitulé " Insensibilités aux androgènes " daté du 21 décembre 2017, ainsi que des décisions de la Haute Autorité de santé de valider ce document et de le mettre en ligne sur son site internet.<br/>
<br/>
              Sur les conclusions à fin d'annulation du protocole national de diagnostic et de soins et de la décision par laquelle la Haute Autorité de santé aurait validé ce document :<br/>
<br/>
              2. Aux termes de l'article L. 161-37 du code de la sécurité sociale :  " La Haute Autorité de santé, autorité publique indépendante à caractère scientifique, est chargée de : / 1° Procéder à l'évaluation périodique du service attendu des produits, actes ou prestations de santé et du service qu'ils rendent, et contribuer par ses avis à l'élaboration des décisions relatives à l'inscription, au remboursement et à la prise en charge par l'assurance maladie des produits, actes ou prestations de santé ainsi qu'aux conditions particulières de prise en charge des soins dispensés aux personnes atteintes d'affections de longue durée. A cet effet, elle émet également un avis sur les conditions de prescription, de réalisation ou d'emploi des actes, produits ou prestations de santé ainsi que sur leur efficience. Elle réalise ou valide notamment les études médico-économiques nécessaires à l'évaluation des actes mentionnés aux articles L. 162-1-7-1 et L. 162-1-8 et des produits et technologies de santé. (...) ; / 1° bis Elaborer ou mettre à jour des fiches sur le bon usage de certains médicaments permettant notamment de définir leur place dans la stratégie thérapeutique, à l'exclusion des médicaments anticancéreux pour lesquels l'Institut national du cancer élabore ou met à jour les fiches de bon usage ; / 2° Elaborer les guides de bon usage des soins ou les recommandations de bonne pratique, procéder à leur diffusion et contribuer à l'information des professionnels de santé et du public dans ces domaines, sans préjudice des mesures prises par l'Agence nationale de sécurité du médicament et des produits de santé dans le cadre de ses missions de sécurité sanitaire. Elle élabore ou valide également, à destination des professionnels de santé, dans des conditions définies par décret, un guide des stratégies diagnostiques et thérapeutiques les plus efficientes ainsi que des listes de médicaments à utiliser préférentiellement (...) ". L'article D. 161-22 du même code précise les conditions d'élaboration de tels guides par la Haute Autorité de santé ou par un tiers sélectionné à la suite d'un appel à candidatures.<br/>
<br/>
              3. En outre, il résulte de l'article L. 5121-12-1 du code de la santé publique que les recommandations temporaires d'utilisation que l'Agence nationale de sécurité du médicament et des produits de santé peut établir pour garantir la sécurité d'utilisation d'une spécialité pharmaceutique dans une indication ou des conditions d'utilisation non conformes à son autorisation de mise sur le marché sont élaborées, pour les maladies rares, " en s'appuyant notamment sur les travaux des professionnels de santé prenant en charge ces pathologies et, le cas échéant, les résultats des essais thérapeutiques et les protocoles nationaux de diagnostics et de soins ". L'article R. 5121-76-6 du même code précise, à cet égard, que : " Sur la base des informations mentionnées aux articles R. 5121-76-4 et R. 5121-76-5 ainsi que des connaissances scientifiques disponibles et notamment, s'agissant de la prise en charge d'une maladie rare, du protocole national de diagnostic et de soins publié par la Haute Autorité de santé lorsqu'il existe, l'agence procède à l'évaluation de l'efficacité et de la sécurité présumées de la spécialité dans l'indication ou les conditions d'utilisation considérées. (...) ".<br/>
<br/>
              4. Il ressort des pièces du dossier que le document dont l'association requérante demande l'annulation pour excès de pouvoir, intitulé protocole national de diagnostic et de soins " Insensibilités aux androgènes ", a été élaboré par deux médecins et un assistant de recherche clinique exerçant dans des établissements de santé labellisés " centre de référence du développement génital : du foetus à l'adulte " par un arrêté des ministres chargés de la santé et de la recherche du 25 novembre 2017, ainsi que par le responsable de la plateforme d'expertise " maladies rares " des hôpitaux universitaires Paris-Sud de l'Assistance publique - hôpitaux de Paris, et a été validé par un groupe de relecture composé de quatorze médecins. Si cette élaboration répond à l'un des objectifs du plan national " maladies rares " arrêté au niveau interministériel pour la période de 2011 à 2016, qui présente de tels protocoles comme des référentiels de bonnes pratiques à l'usage des professionnels pour améliorer la prise en charge des patients atteints d'une maladie rare et si elle a suivi un guide méthodologique rédigé par la Haute Autorité de santé, son contenu a été arrêté sous la seule responsabilité des médecins coordonnateurs et non sous celle de la Haute Autorité ou d'aucune autre autorité administrative. <br/>
<br/>
              5. D'une part, s'il identifie les pratiques de prise en charge diagnostique et thérapeutique des insensibilités aux androgènes regardées qui, en l'état des connaissances, peuvent être regardées comme les mieux adaptées par les professionnels spécialistes de ces maladies génétiques rares, le protocole litigieux constitue néanmoins un document de référence scientifique dépourvu de portée contraignante. Il n'est susceptible de lier ni l'Agence nationale de sécurité du médicament et des produits de santé dans l'élaboration des recommandations temporaires d'utilisation prévues par l'article L. 5121-12-1 du code de la santé publique précité, ni aucune autre autorité administrative. <br/>
<br/>
              6. D'autre part, si la Haute Autorité de santé est notamment chargée, en vertu de l'article L. 161-37 du code de la sécurité sociale, d'élaborer ou de valider, à destination des professionnels de santé, des guides des stratégies diagnostiques et thérapeutiques les plus efficientes, ces dispositions ne font pas obstacle à ce qu'elle publie sur son site internet des travaux de professionnels participant de l'information sur l'état de la science, sans qu'une telle publication, qui doit au demeurant faire apparaître sans ambiguïté les auteurs et le statut de ces travaux, ne puisse être regardée comme révélant une décision de la Haute Autorité de les valider. Aucun texte, notamment pas l'article R. 5121-76-6 du code de la santé publique cité ci-dessus, ne prévoit qu'il appartient à la Haute Autorité d'élaborer ou de valider les protocoles nationaux de diagnostic et de soins. Enfin, l'association requérante ne peut se prévaloir utilement, pour soutenir que le protocole dont elle demande l'annulation devrait être regardé comme émanant de la Haute Autorité de santé, des dispositions de l'article L. 1414-3-3 du code de la santé publique relatives aux référentiels élaborés ou validés par la Haute Autorité de santé au titre de sa mission d'accréditation des médecins exerçant en établissements de santé. <br/>
<br/>
              7. Il résulte de ce qui précède que le protocole national de diagnostic et de soins attaqué ne présente pas le caractère d'un acte administratif faisant grief et n'a fait l'objet d'aucune décision de validation par la Haute Autorité de santé dont l'association requérante pourrait demander l'annulation pour excès de pouvoir. Ses conclusions tendant à l'annulation de ce document et d'une telle décision sont ainsi entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance et le Conseil d'Etat, statuant au contentieux peut les rejeter par application de l'article R. 351-4 du code de justice administrative.  <br/>
<br/>
              Sur les conclusions à fin d'annulation de la décision de la Haute Autorité de santé de diffuser le protocole national de diagnostic et de soins sur son site internet :<br/>
<br/>
              8. Aux termes du premier alinéa de l'article R. 421-1 du code de justice : " La juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ". <br/>
<br/>
              9. Il ressort des pièces du dossier que, par un courrier du 8 mars 2018, l'association requérante a demandé à la présidente de la Haute Autorité de santé de retirer la décision de publication du protocole critiqué sur le site internet de cette autorité. Elle est, dès lors, réputée avoir eu connaissance acquise de cette décision au plus tard à cette date et le délai de recours contentieux a recommencé à courir à la date du rejet de son recours gracieux, intervenu au plus tard le 12 mai 2018. Par suite, ses conclusions tendant à l'annulation de la décision de la Haute autorité de santé de diffuser le protocole en cause sur son site internet, présentées pour la première fois dans son mémoire enregistré le 1er février 2019 au secrétariat du contentieux du Conseil d'Etat, sont tardives. Elles sont ainsi entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance et le Conseil d'Etat, statuant au contentieux peut les rejeter par application de l'article R. 351-4 du code de justice administrative. <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              10. Il résulte de ce qui précède que les conclusions à fin d'annulation présentées par l'association Groupement d'information et de soutien sur les questions sexuées et sexuelles doivent être rejetées. Par suite, ses conclusions à fin d'injonction ne peuvent qu'être également rejetées.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la Haute Autorité de santé ni, en tout état de cause, de l'Etat, qui ne sont pas, dans la présente instance, la partie perdante. Il résulte de ces mêmes dispositions que, si une personne publique qui n'a pas eu recours au ministère d'avocat peut néanmoins demander au juge l'application de cet article au titre des frais spécifiques exposés par elle à l'occasion de l'instance, elle ne saurait se borner à faire état d'un surcroît de travail de ses services. En l'espèce, les éléments avancés par la Haute Autorité de santé, qui ne se prévaut pas de frais autres que la rémunération des agents qui ont préparé sa défense, ne sont pas de nature à justifier qu'une somme soit mise, à ce titre, à la charge de l'association requérante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de l'association Groupement d'information et de soutien sur les questions sexuées et sexuelles est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par la Haute Autorité de santé au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association Groupement d'information et de soutien sur les questions sexuées et sexuelles et à la Haute Autorité de santé.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
