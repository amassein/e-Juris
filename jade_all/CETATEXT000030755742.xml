<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030755742</ID>
<ANCIEN_ID>JG_L_2015_06_000000385824</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/57/CETATEXT000030755742.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 19/06/2015, 385824, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385824</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:385824.20150619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E...D...A...et autres ont demandé au tribunal administratif de Saint-Denis d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 pour la désignation des conseillers municipaux et communautaires de la commune de Saint-Pierre (La Réunion). Par un jugement n° 1400279 du 23 octobre 2014, le tribunal administratif de Saint-Denis a rejeté leur protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 19 novembre 2014 et le 4 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. D...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 pour la désignation des conseillers municipaux et communautaires de la commune de Saint-Pierre ;<br/>
<br/>
              3°) de mettre à la charge de M. B...C...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. C...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 juin 2015, présentée par M. D... A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du premier tour des élections municipales qui s'est déroulé le 23 mars 2014 dans la commune de Saint-Pierre (La Réunion), la liste conduite par M. B...C..., maire sortant, a recueilli 21 877 voix, soit 57,78% des suffrages exprimés, dépassant ainsi de 2 948 voix le seuil de la majorité absolue, tandis que la liste conduite par M. E...D...A..., arrivée en deuxième position, a obtenu 6 390 voix, soit 16,87% des suffrages exprimés. M. D...A...relève appel du jugement du 23 octobre 2014 par lequel le tribunal administratif de Saint-Denis a rejeté sa protestation tendant à l'annulation de ces opérations électorales.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. M. D...A...soutient, en premier lieu, que le jugement attaqué méconnaît le principe du contradictoire et le principe d'impartialité dès lors que la lettre par laquelle la Commission nationale des comptes de campagnes et des financements politiques (CNCCFP) a saisi le juge de l'élection ne lui a pas été transmise, alors que M. B...C..., maire sortant, qui la mentionnait dans ses écritures, en disposait. Il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 113 et suivants du code électoral que, par dérogation aux dispositions de l'article R. 611-1 du code de justice administrative, le tribunal administratif n'est pas tenu, compte tenu des règles et délais propres au contentieux électoral, de communiquer aux parties la décision de la CNCCFP relative au compte de campagne du conseiller municipal dont l'élection est contestée, non plus que le compte lui-même. Il appartient seulement au tribunal, une fois ces pièces enregistrées par le greffe, de les tenir à la disposition des parties, de sorte que celles-ci puissent, si elles l'estiment utile, en prendre connaissance. En l'espèce, il résulte de l'instruction que cette pièce a été tenue à la disposition des protestataires au greffe du tribunal de Saint-Denis. Le requérant n'allègue pas que le greffe du tribunal administratif ne lui aurait pas permis de la consulter. Par suite, le moyen tiré de la méconnaissance du principe du contradictoire doit être écarté. La circonstance que M. C... a disposé de cette pièce, qu'il a produite à l'appui de son mémoire du 11 septembre 2014, lequel a d'ailleurs été communiqué aux requérants le 12 septembre, n'est ainsi pas, par elle-même, de nature à établir que le juge de l'élection aurait méconnu le principe d'impartialité. Dès lors, le requérant n'est pas fondé à soutenir que le jugement du tribunal administratif de Saint-Denis a été rendu au terme d'une procédure irrégulière pour ces motifs.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article L. 118-2 du code électoral : " Si le juge administratif est saisi de la contestation d'une élection dans une circonscription où le montant des dépenses électorales est plafonné, il sursoit à statuer jusqu'à réception des décisions de la commission instituée par l'article L. 52-14 qui doit se prononcer sur les comptes de campagne des candidats à cette élection dans le délai de deux mois suivant l'expiration du délai fixé au deuxième alinéa de l'article L. 52-12 ". Selon l'article L. 52-12 de ce code, le compte de campagne doit être déposé au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin. Aux termes de l'article R. 120 du même code : " Le tribunal administratif prononce sa décision dans le délai de deux mois à compter de l'enregistrement de la réclamation au greffe (...)/ En cas de renouvellement général, le délai est porté à trois mois./ (...) Lorsqu'il est fait application des dispositions de l'article L. 118-2, le délai, prévu au premier alinéa, dans lequel le tribunal administratif doit se prononcer, court à partir de la date de réception par le tribunal administratif des décisions de la commission nationale des comptes de campagne et des financements politiques ou, à défaut de décision explicite, à partir de l'expiration du délai de deux mois prévu audit article ". M. D...A...soutient que la CNCCFP, puis le tribunal administratif, se sont prononcés au-delà des délais fixés par ces dispositions. La commune de Saint-Pierre, qui compte plus de 9 000 habitants, fait partie des communes pour lesquelles le montant des dépenses électorales est plafonné. Par suite, en application des dispositions précitées de l'article L. 118-2 du code électoral, le tribunal administratif de Saint-Denis devait, à la suite des élections qui se sont déroulées le 23 mars 2014, surseoir à statuer dans l'attente de la décision de la CNCCFP et, s'agissant d'un renouvellement général, disposait d'un délai de trois mois pour statuer après la décision de celle-ci. La CNCCFP a saisi le tribunal administratif le 30 juillet 2014, soit deux mois après le vendredi 30 mai, dixième vendredi suivant le 23 mars. Dès lors, en rendant son jugement le 23 octobre 2014, soit moins de trois mois après la décision de la CNCCFP, elle-même rendue dans les délais prescrits par les dispositions précitées, le tribunal administratif n'a pas statué au-delà du délai qui lui était imparti.<br/>
<br/>
              4. En troisième lieu, contrairement à ce que soutient le requérant, le tribunal administratif a suffisamment répondu aux griefs soulevés devant lui dans le délai de recours, notamment à ceux tirés des pressions sur les électeurs et des violences qui ont marqué la campagne électorale.<br/>
<br/>
              Sur les moyens relatifs aux listes électorales :<br/>
<br/>
              5. Il n'appartient pas au juge de l'élection, en l'absence de manoeuvre, d'apprécier la régularité des inscriptions ou radiations opérées sur les listes électorales. Si M. D... A...fait valoir que deux personnes ont été inscrites sur les listes électorales après leur décès, il ne résulte pas de l'instruction que ces inscriptions procédaient d'une manoeuvre. Dès lors, ce moyen ne saurait utilement être invoqué devant le juge de l'élection.<br/>
<br/>
              Sur les moyens relatifs à la campagne électorale :<br/>
<br/>
              6. M. D...A...soutient, en premier lieu, que des recrutements massifs auraient été opérés par la communauté intercommunale des villes solidaires du Sud, à laquelle appartient la commune de Saint-Pierre, pendant les trois années précédant les élections, et notamment en 2013. Toutefois, il ne résulte pas de l'instruction que ces recrutements ne répondaient pas aux besoins de la communauté intercommunale et avaient pour objectif d'exercer une influence sur le scrutin municipal.<br/>
<br/>
              7. Le requérant soutient, en deuxième lieu, en produisant à l'appui de ce moyen des attestations, dont certaines sont contestées ou peu circonstanciées, que des partisans de M. C... se sont livrés pendant la campagne à des actes de pression, voire à des tentatives d'intimidation, sur des électeurs et ont fortement perturbé l'une des réunions électorales de sa liste. Toutefois, à les supposer avérées, ces irrégularités n'ont pas été de nature, compte tenu de l'écart des voix, à influer sur le résultat du scrutin.<br/>
<br/>
              8. Si M. D...A...soutient, en troisième lieu, que des affiches ont été apposées en dehors des emplacements autorisés, en violation des dispositions de l'article L. 51 du code électoral, les deux photographies, non datées, qu'il produit ne permettent pas d'établir que cet affichage irrégulier aurait été important. Dès lors, cette irrégularité n'a pas été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              9. En quatrième lieu, il n'est pas établi, contrairement à ce que le requérant soutient en produisant un article de presse, l'extrait d'un compte Facebook et le bulletin municipal n° 75, que la liste conduite par M. C...aurait utilisé, pour la campagne électorale, des véhicules communaux et aurait eu recours à du personnel municipal.<br/>
<br/>
              En ce qui concerne les moyens tirés de la violation des articles L. 52-1 et L. 52-12 du code électoral :<br/>
<br/>
              10. Aux termes de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédent le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin ". Aux termes de l'article L. 52-12 du même code : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnées à l'article L. 52-4. Sont réputées faites pour son compte les dépenses exposées directement au profit du candidat et avec l'accord de celui-ci, par les personnes physiques qui lui apportent leur soutien, ainsi que par les partis et groupements politique qui ont été créés en vue de lui apporter leur soutien ou qui lui apportent leur soutien. Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié (...) ".<br/>
<br/>
              11. En premier lieu, il est soutenu, d'une part, que le budget consacré aux festivités du 20 décembre 2013 aurait été plus élevé que celui des années précédentes, d'autre part, que le maire sortant aurait mis en valeur son équipe municipale, tant à l'occasion d'un évènement sportif, pour lequel une vidéo a été réalisée, qu'à celle d'un week-end organisé pour les personnes âgées, pour lequel un bulletin municipal spécial a été édité. Il résulte toutefois de l'instruction que les manifestations de la fin de l'année 2013 n'ont pas eu une ampleur particulière, que le bulletin municipal incriminé a eu un caractère purement informatif et que la vidéo produite sur l'événement sportif mettait essentiellement en valeur ce dernier. Par suite, les moyens tirés de la méconnaissance des dispositions des articles L. 52-1 et L. 52-12 du code électoral doivent être écartés.<br/>
<br/>
              12. En deuxième lieu, si M. D...A...soutient que les dépenses liées aux visites du président du conseil régional et d'un responsable politique national auraient dû être intégrées dans le compte de campagne du maire sortant et que, dès lors, celui-ci aurait dépassé le plafond autorisé, les frais liés aux déplacements de représentants de formations politiques ne constituent pas, pour la liste qu'ils viennent soutenir, une dépense électorale devant figurer dans le compte de campagne. Ce moyen ne peut donc qu'être écarté.<br/>
<br/>
              13. En troisième lieu, si le requérant soutient que M. C...a bénéficié de la prestation, lors de l'une de ses réunions électorales, d'un artiste populaire à La Réunion, et que les dépenses occasionnées par celle-ci auraient dû figurer dans son compte de campagne, dont le plafond autorisé aurait ainsi été dépassé, il n'assortit pas ce grief des précisions, relatives notamment à son coût, permettant d'en apprécier le bien-fondé.<br/>
<br/>
              Sur les moyens relatifs aux opérations électorales :<br/>
<br/>
              14. En premier lieu, le requérant soutient que des procurations irrégulièrement établies ont été utilisées pendant le scrutin et que certaines procurations régulièrement établies ont été utilisées plusieurs fois. Toutefois, les attestations et pièces apportées au soutien de ce moyen ne permettent pas d'établir la réalité des irrégularités alléguées.<br/>
<br/>
              15. En deuxième lieu, aux termes de l'article L. 60 du code électoral : " Le vote a lieu sous enveloppe, obligatoirement de couleur différente de celle de la précédente consultation générale. Le jour du vote, celles-ci sont mises à la disposition des électeurs dans la salle de vote. Avant l'ouverture du scrutin, le bureau doit constater que le nombre des enveloppes correspond exactement à celui des électeurs inscrits ". Si M. D...A...soutient que le nombre d'enveloppes mis à la disposition des électeurs était inférieur à celui des inscrits dans six bureaux de vote et que cette irrégularité était révélatrice d'une fraude, certains électeurs étant entrés dans le bureau de vote déjà munis d'une enveloppe électorale, ces faits, à les supposer établis, ne sont pas, par eux-mêmes, constitutifs d'une manoeuvre, dès lors qu'il n'est pas établi que des électeurs n'auraient pas reçu dans le bureau de vote l'enveloppe qu'ils ont utilisée pour voter.<br/>
<br/>
              16. En troisième lieu, si le requérant soutient que, dans deux bureaux de vote, une coupure d'électricité s'est produite pendant les opérations de dépouillement, cette circonstance, dont il n'est pas allégué qu'elle aurait été intentionnellement provoquée, ne saurait, par elle-même, caractériser une manoeuvre électorale.<br/>
<br/>
              17. En quatrième lieu, aux termes de l'article L. 65 du code électoral : " Dès la clôture du scrutin, il est procédé au dénombrement des émargements. Ensuite, le dépouillement se déroule de la manière suivante : l'urne est ouverte et le nombre des enveloppes est vérifié. (...) / Les enveloppes contenant les bulletins sont regroupées par paquet de cent. Ces paquets sont introduits dans des enveloppes spécialement réservées à cet effet. ". Si M. D... A...soutient que, lors du dépouillement, des enveloppes, de couleur marron, dans lesquelles avaient été insérés les paquets de cent enveloppes contenant les bulletins de vote, ont été soustraites à la vue du public et des scrutateurs, cette circonstance, qui n'a pas fait l'objet d'une mention d'irrégularité sur les procès-verbaux des bureaux de vote concernés, à la supposer établie, n'est pas de nature, par elle-même, à porter atteinte à la sincérité du scrutin.<br/>
<br/>
              18. En cinquième lieu, la seule circonstance que le compteur de certaines urnes ait enregistré un nombre de votants distinct de celui du nombre d'enveloppes qu'elles contenaient ne saurait être regardée comme constituant une irrégularité, dès lors que le nombre d'émargements était égal au nombre d'enveloppes trouvées dans l'urne. <br/>
<br/>
              19. En sixième lieu, M. D...A...soutient, d'une part, en produisant une attestation, que dans le bureau n° 38, le nombre de bulletins dans les urnes dépassait de 11 celui des émargements et, d'autre part, que, dans le bureau 42, l'écart était de 9 suffrages. A les supposer avérées, ces irrégularités, compte tenu du nombre de suffrages sur lesquels elles portent, du nombre de suffrages obtenus par les listes en présence et des modalités du scrutin, n'ont pu affecter la répartition et l'attribution des sièges entre les listes.   <br/>
<br/>
              20. En septième lieu, s'il est soutenu que, dans certains bureaux de vote, des bulletins de la liste de M. D...A...souillés, mouillés ou froissés auraient tardé à être remplacés, cette circonstance, à la supposer établie, n'est pas de nature à altérer la sincérité du scrutin.<br/>
<br/>
              21. En huitième lieu, le requérant soutient que, dans trois bureaux de vote, des assesseurs de la liste de M. D...A...auraient été empêchés ou dissuadés de reporter des irrégularités sur les procès-verbaux. Si ces assesseurs ont attesté ces faits, ces refus et les circonstances dans lesquelles ils auraient été opposés ne sont toutefois pas établis. Dès lors, ce moyen doit être écarté.<br/>
<br/>
              22. En neuvième lieu, les moyens tirés, d'une part, de l'existence d'un climat de violence et d'intimidation pendant le scrutin et, d'autre part, de ce que, dans certains bureaux de vote, des enveloppes de vote de couleurs différentes auraient été utilisées, ont été présentés devant le tribunal administratif de Saint-Denis dans des mémoires enregistrés après l'expiration du délai de cinq jours prévu à l'article R. 119 du code électoral. Ne constituant pas le développement de moyens invoqués avant l'expiration de ce délai, ils sont tardifs et par suite, irrecevables.<br/>
<br/>
              23. En dixième lieu, il ne résulte pas de l'instruction que la commune aurait subventionné à des fins électorales l'association Hibiscus, dont le président est un colistier de M. C....<br/>
<br/>
              24. Il résulte de tout ce qui précède que M. D...A...n'est pas fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Saint-Denis a rejeté sa protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 23 mars 2014 en vue de la désignation des conseillers municipaux de la commune de Saint-Pierre.<br/>
<br/>
              25. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. C... qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce même titre par M.C....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de M. D...A...est rejetée.<br/>
Article 2 : Les conclusions présentées par M. D...A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. E...D...A...et à M. B...C....<br/>
Copie en sera adressée au ministre de l'intérieur et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
