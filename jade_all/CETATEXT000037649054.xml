<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037649054</ID>
<ANCIEN_ID>JG_L_2018_11_000000403978</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/64/90/CETATEXT000037649054.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 26/11/2018, 403978</TITRE>
<DATE_DEC>2018-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403978</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:403978.20181126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Boucherie de la paix a demandé au tribunal administratif de Paris de la décharger de la contribution spéciale de 34 900 euros et de la contribution forfaitaire représentative des frais de réacheminement de 4 248 euros dont elle a été déclarée redevable par une décision du 21 mars 2014 du directeur général de l'Office français de l'immigration et de l'intégration. Par un jugement n° 1413899 du 5 mai 2015, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15PA02518 du 29 juillet 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Boucherie de la paix contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 octobre 2016 et 2 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la société Boucherie de la paix demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de l'immigration et de l'intégration la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2009/52/CE du Parlement européen et du Conseil du 18 juin 2009 prévoyant des normes minimales concernant les sanctions et les mesures à l'encontre des employeurs de ressortissants de pays tiers en séjour irrégulier ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de la société Boucherie de la paix et à la SCP Lévis, avocat de l'Office français de l'immigration et de l'intégration.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 19 mars 2013, lors d'un contrôle inopiné d'un inspecteur du recouvrement des cotisations et contributions de la sécurité sociale effectué en présence des services de police, a été constaté l'emploi par la société Boucherie de la paix de deux ressortissants étrangers démunis de titre les autorisant à séjourner et à travailler en France. Par une décision du 21 mars 2014, le directeur général de l'Office français de l'immigration et de l'intégration a mis à la charge de la société Boucherie de la paix la contribution spéciale prévue par l'article L. 8253-1 du code du travail, à hauteur de 34 900 euros, et la contribution forfaitaire représentative des frais de réacheminement prévue par l'article L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, pour un montant de 4 248 euros. La société Boucherie de la paix se pourvoit en cassation contre l'arrêt du 29 juillet 2016 par lequel la cour administrative d'appel de Paris, confirmant le jugement du tribunal administratif de Paris du 5 mai 2015, a rejeté sa demande tendant à la décharge des sommes ainsi réclamées. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 8251-1 du code du travail : " Nul ne peut, directement ou indirectement, embaucher, conserver à son service ou employer pour quelque durée que ce soit un étranger non muni du titre l'autorisant à exercer une activité salariée en France ". L'article L. 5221-8 du même code dispose que : " L'employeur s'assure auprès des administrations territorialement compétentes de l'existence du titre autorisant l'étranger à exercer une activité salariée en France, sauf si cet étranger est inscrit sur la liste des demandeurs d'emploi tenue par l'institution mentionnée à l'article L. 5312-1 ". Aux termes de l'article L. 8253-1 de ce code, dans sa rédaction en vigueur à la date des manquements relevés à l'encontre de la société Boucherie de la paix, qui n'a pas été modifiée par une loi nouvelle plus douce entre cette date et celle à laquelle les juges du fond se sont prononcés : " Sans préjudice des poursuites judiciaires pouvant être intentées à son encontre, l'employeur qui a employé un travailleur étranger en méconnaissance des dispositions du premier alinéa de l'article L. 8251-1 acquitte, pour chaque travailleur étranger sans titre de travail, une contribution spéciale. Le montant de cette contribution spéciale est déterminé dans des conditions fixées par décret en Conseil d'Etat. Il est, au plus, égal à 5 000 fois le taux horaire du minimum garanti prévu à l'article L. 3231-12. Ce montant peut être minoré en cas de non-cumul d'infractions ou en cas de paiement spontané par l'employeur des salaires et indemnités dus au salarié étranger sans titre mentionné à l'article R. 8252-6. Il est alors, au plus, égal à 2 000 fois ce même taux (...) / L'Office français de l'immigration et de l'intégration est chargé de constater et de liquider cette contribution ". Enfin, aux termes du premier alinéa de l'article L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sans préjudice des poursuites judiciaires qui pourront être engagées à son encontre et de la contribution spéciale prévue à l'article L. 8253-1 du code du travail, l'employeur qui aura occupé un travailleur étranger en situation de séjour irrégulier acquittera une contribution forfaitaire représentative des frais de réacheminement de l'étranger dans son pays d'origine ".<br/>
<br/>
              3. Il résulte des dispositions précitées de l'article L. 8253-1 du code du travail et de l'article L. 626-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que les contributions qu'ils prévoient ont pour objet de sanctionner les faits d'emploi d'un travailleur étranger séjournant irrégulièrement sur le territoire français ou démuni de titre l'autorisant à exercer une activité salariée, sans qu'un élément intentionnel soit nécessaire à la caractérisation du manquement. Toutefois, un employeur ne saurait être sanctionné sur le fondement de ces dispositions, qui assurent la transposition des articles 3, 4 et 5 de la directive 2009/52/CE du Parlement européen et du Conseil du 18 juin 2009 prévoyant des normes minimales concernant les sanctions et les mesures à l'encontre des employeurs de ressortissants de pays tiers en séjour irrégulier, lorsque tout à la fois, d'une part, il s'est acquitté des obligations qui lui incombent en vertu de l'article L. 5221-8  du code du travail et, d'autre part, il n'était pas en mesure de savoir que les documents qui lui étaient présentés revêtaient un caractère frauduleux ou procédaient d'une usurpation d'identité. En outre, lorsqu'un salarié s'est prévalu lors de son embauche de la nationalité française ou de sa qualité de ressortissant d'un Etat pour lequel une autorisation de travail n'est pas exigée, l'employeur ne peut être sanctionné s'il s'est assuré que ce salarié disposait d'un document d'identité de nature à en justifier et s'il n'était pas en mesure de savoir que ce document revêtait un caractère frauduleux ou procédait d'une usurpation d'identité. <br/>
<br/>
              4. Il résulte de ce qui précède que la cour administrative d'appel de Paris a commis une erreur de droit en se fondant, pour juger que, pour aucun des deux salariés considérés, la société requérante ne pouvait utilement se prévaloir de sa bonne foi et de ce qu'elle ignorait l'irrégularité de la situation des ressortissants étrangers qu'elle employait, dont l'un s'était présenté à elle comme de nationalité française, sur la circonstance qu'il lui appartenait, en vertu des dispositions de l'article L. 5221-8 du code du travail, d'entreprendre toutes les démarches utiles afin de vérifier la régularité de la situation de ses salariés, lesquelles ne peuvent s'appliquer qu'à des titres qui ont pour objet d'autoriser un étranger à exercer une activité salariée en France. <br/>
<br/>
              5. La société Boucherie de la paix est, par suite, fondée à demander pour ce motif l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner l'autre moyen de son pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de l'immigration et de l'intégration une somme de 1 500 euros à verser à la société Boucherie de la paix au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Boucherie de la paix, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 29 juillet 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Office français de l'immigration et de l'intégration versera à la société Boucherie de la paix une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de l'Office français de l'immigration et de l'intégration présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Boucherie de la paix et à l'Office français de l'immigration et de l'intégration. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">59-02-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. BIEN-FONDÉ. - CONTRIBUTIONS SANCTIONNANT L'EMPLOI IRRÉGULIER D'UN ÉTRANGER (ART. L. 8253-1 DU CODE DU TRAVAIL ET L. 626-1 DU CESEDA) - SALARIÉ S'ÉTANT PRÉVALU DE SA NATIONALITÉ FRANÇAISE OU DE SA QUALITÉ DE RESSORTISSANT D'UN ETAT DE L'UE POUR LEQUEL UNE AUTORISATION N'EST PAS EXIGÉE - POSSIBILITÉ D'INFLIGER UNE SANCTION À L'EMPLOYEUR - ABSENCE, SI CELUI-CI S'EST ASSURÉ QUE CE SALARIÉ DISPOSAIT D'UN DOCUMENT D'IDENTITÉ DE NATURE À EN JUSTIFIER ET N'ÉTAIT PAS EN MESURE DE SAVOIR QUE CE DOCUMENT REVÊTAIT UN CARACTÈRE FRAUDULEUX OU PROCÉDAIT D'UNE USURPATION D'IDENTITÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-032-01 TRAVAIL ET EMPLOI. RÉGLEMENTATIONS SPÉCIALES À L'EMPLOI DE CERTAINES CATÉGORIES DE TRAVAILLEURS. EMPLOI DES ÉTRANGERS (VOIR : ÉTRANGERS). - CONTRIBUTIONS SANCTIONNANT L'EMPLOI IRRÉGULIER D'UN ÉTRANGER (ART. L. 8253-1 DU CODE DU TRAVAIL ET L. 626-1 DU CESEDA) - SALARIÉ S'ÉTANT PRÉVALU DE SA NATIONALITÉ FRANÇAISE OU DE SA QUALITÉ DE RESSORTISSANT D'UN ETAT DE L'UE POUR LEQUEL UNE AUTORISATION N'EST PAS EXIGÉE - POSSIBILITÉ D'INFLIGER UNE SANCTION À L'EMPLOYEUR - ABSENCE, SI CELUI-CI DISPOSAIT D'UN DOCUMENT D'IDENTITÉ DE NATURE À EN JUSTIFIER ET N'ÉTAIT PAS EN MESURE DE SAVOIR QUE CE DOCUMENT REVÊTAIT UN CARACTÈRE FRAUDULEUX OU PROCÉDAIT D'UNE USURPATION D'IDENTITÉ [RJ1].
</SCT>
<ANA ID="9A"> 59-02-02-03 Lorsqu'un salarié s'est prévalu lors de son embauche de la nationalité française ou de sa qualité de ressortissant d'un Etat pour lequel une autorisation de travail n'est pas exigée, l'employeur ne peut être sanctionné s'il s'est assuré que ce salarié disposait d'un document d'identité de nature à en justifier et s'il n'était pas en mesure de savoir que ce document revêtait un caractère frauduleux ou procédait d'une usurpation d'identité.</ANA>
<ANA ID="9B"> 66-032-01 Lorsqu'un salarié s'est prévalu lors de son embauche de la nationalité française ou de sa qualité de ressortissant d'un Etat pour lequel une autorisation de travail n'est pas exigée, l'employeur ne peut être sanctionné s'il s'est assuré que ce salarié disposait d'un document d'identité de nature à en justifier et s'il n'était pas en mesure de savoir que ce document revêtait un caractère frauduleux ou procédait d'une usurpation d'identité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 12 octobre 2018, SARL Super Coiffeur, n° 408567, p. 373.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
