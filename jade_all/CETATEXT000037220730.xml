<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220730</ID>
<ANCIEN_ID>JG_L_2018_07_000000414760</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220730.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/07/2018, 414760, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414760</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414760.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un mémoire additionnel enregistrés les 2 octobre 2017, 22 décembre 2017 et 2 et 23 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Ouï FM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 26 juillet 2017 par laquelle le Conseil supérieur de l'audiovisuel (CSA) l'a mise en demeure de respecter ses obligations de diffusion d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France ;<br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - la loi n° 2016-925 du 7 juillet 2016 ;<br/>
<br/>
              Vu la décision du 14 février 2018 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Ouï FM ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Ouï FM.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article 28 de la loi du 30 septembre 1986 relative à la liberté de communication subordonne la délivrance de l'autorisation d'usage de la ressource radioélectrique à tout service diffusé par voie hertzienne terrestre, autre que ceux exploités par les sociétés nationales de programme, à la conclusion d'une convention passée entre le Conseil supérieur de l'audiovisuel (CSA) au nom de l'Etat et la personne qui demande l'autorisation ; qu'aux termes du 2° bis de cet article, cette convention porte notamment sur " La proportion substantielle d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France, qui doit atteindre un minimum de 40 % de chansons d'expression française, dont la moitié au moins provenant de nouveaux talents ou de nouvelles productions, diffusées aux heures d'écoute significative par chacun des services de radio autorisés par le Conseil supérieur de l'audiovisuel, pour la part de ses programmes composée de musique de variétés... " ; que les alinéas suivants de ce même 2° bis prévoient, pour les radios répondant à certains critères des règles de quotas dérogatoires portant notamment sur la programmation de nouveaux talents et de nouvelles productions ; que la loi du 7 juillet 2016 relative à la liberté de la création, à l'architecture et au patrimoine a complété le 2° bis par un alinéa introduisant un mécanisme dit de " plafonnement des rotations " ou " malus ", ainsi défini : " Dans l'hypothèse où plus de la moitié du total des diffusions d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France se concentre sur les dix oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France les plus programmées par un service, les diffusions intervenant au-delà de ce seuil ou n'intervenant pas à des heures d'écoute significative ne sont pas prises en compte pour le respect des proportions fixées par la convention pour l'application du présent 2° bis " ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions de l'article 4-2-1 de la convention relative au service radiophonique " Ouï FM ", conclue le 12 janvier 2012, en application de l'article 28 de la loi du 30 septembre 1986, entre le CSA et la société Ouï FM, éditrice de ce service, le conseil supérieur peut mettre l'éditeur en demeure de respecter ses obligations conventionnelles ; que, le 26 juillet 2017, le CSA a, sur le fondement de cet article, mis la société en demeure de respecter les stipulations de la convention relatives à la diffusion par cette société d'une proportion substantielle d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France conformément aux dispositions précitées de l'article 2° bis de la loi du 30 septembre 1986 ; que la société Ouï FM demande l'annulation de cette mise en demeure ;<br/>
<br/>
              3. Considérant que, par une communication du 23 novembre 2016, le CSA a énoncé la méthode qu'il entendait mettre en oeuvre pour vérifier le respect, par les éditeurs de services de radio, des dispositions citées au point 1 ; qu'aux termes de cette communication : " Pour vérifier le respect des quotas, le Conseil ne prend pas en compte les diffusions des dix titres francophones intervenant au-delà de 50 % du total des titres francophones diffusés. Ces diffusions sont par conséquent retirées du sous-total des diffusions des titres francophones, c'est-à-dire du numérateur " ; que les quotas prévus par les dispositions citées au point 1 sont calculés en proportion de l'ensemble de la programmation de musique de variétés de chaque radio, sans que la loi prévoie la modulation de cette assiette de calcul ; qu'en ne retranchant pas les diffusions excédentaires du total des diffusions figurant au dénominateur servant au calcul du quota, le CSA a fait une exacte application des dispositions du 2° bis de l'article 28 rappelées ci-dessus ; que le moyen tiré de ce que la mise en demeure attaquée aurait été prise en vertu d'une communication illégale doit par suite être écarté ;<br/>
<br/>
              4. Considérant qu'il ressort de la mise en demeure litigieuse qu'elle est motivée par le non-respect, par la société requérante, des obligations conventionnelles qu'elle a contractées en matière de diffusion de chansons d'expression française dans ses programmes composés de " musique de variétés " ; qu'au sens et pour l'application du 2° bis de l'article 28 de la loi du 30 septembre 1986, ces termes désignent l'ensemble de la chanson populaire et de divertissement accessible à un large public, par opposition, notamment, à l'art lyrique et au chant du répertoire savant ; qu'il ne ressort pas des pièces du dossier que le CSA aurait commis une erreur d'appréciation en retenant, pour l'application de ces dispositions, que la programmation de la société Ouï FM, essentiellement composée de " musique rock ", était en totalité composée de musique de variétés ;<br/>
<br/>
              5. Considérant que, se prononçant sur la conformité à la Constitution du texte adopté par le Parlement et qui allait devenir la loi du 17 janvier 1989 modifiant la loi du 30 septembre 1986 relative à la liberté de communication, le Conseil constitutionnel, par sa décision n° 88-248 DC du 17 janvier 1989, a estimé que les pouvoirs de sanction conférés par le législateur au CSA ne sont susceptibles de s'exercer qu'après mise en demeure des titulaires d'autorisation pour l'exploitation de services de communication audiovisuelle de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires, et faute pour les intéressés de respecter ces obligations ou de se conformer aux mises en demeure qui leur ont été adressées ; que c'est sous réserve de cette interprétation que les articles en cause ont été déclarés conformes à l'article 8 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 et à l'article 34 de la Constitution ; que cette réserve d'interprétation assure notamment le respect du principe de légalité des délits et des peines, consacré par l'article 8 de la Déclaration du 26 août 1789 et qui s'applique notamment devant les organismes administratifs dotés d'un pouvoir de sanction ; que le CSA ne peut, en effet, prononcer une sanction contre le titulaire de l'autorisation qu'en cas de réitération d'un comportement ayant fait auparavant l'objet d'une mise en demeure par laquelle il a été au besoin éclairé sur ses obligations ;<br/>
<br/>
              6. Considérant que si la société requérante soutient que la notion de musique de variété est insuffisamment précise et en déduit que le CSA a méconnu le principe de légalité des délits et des peines ainsi que les stipulations de l'article 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en la mettant en demeure de respecter des obligations déterminées en fonction de cette notion, ce moyen ne saurait être accueilli dès lors que, d'une part, la notion de musique de variétés répond à la définition indiquée au point 4 et que, d'autre part, la mise en demeure attaquée indique les pourcentages de diffusion de chansons d'expression française retenus pour apprécier le respect des quotas et met ainsi son destinataire en mesure d'en vérifier l'exactitude ou, le cas échéant, de demander au CSA toute précision relative à leur mode de calcul ; <br/>
<br/>
              7. Considérant que si la société Ouï FM soutient que la mise en demeure attaquée méconnaît le principe d'impartialité et est entachée d'erreur de droit et d'erreur d'appréciation faute de se fonder sur des données de diffusion fiables et de tenir compte d'une marge d'erreur ou de tolérance, ces moyens ne sont pas assortis des précisions suffisantes permettant d'en apprécier le bien-fondé ; que si elle soutient par ailleurs que la méthode consistant pour le CSA à vérifier le respect des obligations des opérateurs radiophoniques au moyen d'un " panel fixe " et d'un " panel tournant " de stations revêt un caractère discriminatoire, cette circonstance, qui ne remet pas en cause les constatations effectuées quant à la programmation de la société Ouï FM, est sans incidence sur la légalité de la mise en demeure attaquée ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la société Ouï FM n'est pas fondée à demander l'annulation de la mise en demeure attaquée ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du CSA qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Ouï FM est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Ouï FM, au Conseil supérieur de l'audiovisuel et à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
