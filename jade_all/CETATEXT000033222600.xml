<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033222600</ID>
<ANCIEN_ID>JG_L_2016_10_000000398617</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/22/26/CETATEXT000033222600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 07/10/2016, 398617, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398617</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:398617.20161007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a transmis au tribunal administratif de Marseille, en application de l'article L. 52-15 du code électoral, la décision du 19 octobre 2015 par laquelle elle a rejeté le compte de campagne de M. C...D...et Mme B...A..., candidats en binôme à l'élection départementale des 22 et 29 mars 2015 dans la circonscription de Manosque-1 (Alpes de Haute-Provence). Par un jugement n° 1508987 du 26 janvier 2016, le tribunal administratif a déclaré que le compte de campagne du candidat avait été rejeté à bon droit et déclaré M. D... et Mme A...inéligibles pour une durée de six mois à compter de la date du jugement.<br/>
<br/>
              Par une ordonnance n° 16MA00730 du 5 avril 2016, enregistrée le 8 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis, en vertu de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 25 février 2016 au greffe de cette cour, présentée par M. D...et Mme A.... Par cette requête, M. D...et Mme A...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de ne pas les déclarer inéligibles ;<br/>
<br/>
              3°) de mettre à la charge de la Commission nationale des comptes de campagne et des financements politiques la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que, par une décision du 19 octobre 2015, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne déposé par M. D...et MmeA..., candidats en binôme à l'élection départementale des 22 et 29 mars 2015 dans la circonscription de Manosque-1 (Alpes de Haute-Provence) ; qu'en application des dispositions de l'article L. 52-15 du code électoral, la Commission nationale des comptes de campagne et des financements politiques a saisi le tribunal administratif de Marseille qui, par un jugement du 26 janvier 2016, a déclaré M. D...et Mme A...inéligibles pour une durée de six mois ; que M. D...et Mme A...font appel de ce jugement ;<br/>
<br/>
              Sur le rejet du compte de campagne :<br/>
<br/>
              2. Considérant qu'aux termes du troisième alinéa l'article L. 52-4 du code électoral, le mandataire financier " règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique. Les dépenses antérieures à sa désignation payées directement par le candidat ou à son profit font l'objet d'un remboursement par le mandataire et figurent dans son compte bancaire ou postal " ; que si, par dérogation à la formalité substantielle que constitue l'obligation de recourir à un mandataire pour toute dépense effectuée en vue de sa campagne, le règlement direct de menues dépenses par un candidat peut être admis, ce n'est qu'à la double condition que leur montant soit faible par rapport au total des dépenses du compte de campagne et négligeable au regard du plafond de dépenses autorisées fixé par l'article L. 52-11 du même code ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M. D...et Mme A... ont réglé directement, sans passer par l'intermédiaire de leur mandataire financier, un montant de 401 euros, représentant 12 % du montant total de leurs dépenses et 4,4 % du plafond des dépenses autorisées dans le canton ; qu'ils ont ainsi méconnu les dispositions de l'article L. 52-4 du code électoral ; que, par suite, alors même que les autres motifs de rejet de leur compte retenus par la Commission nationale des comptes de campagne et des financements politiques ne seraient pas établis, ils ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Marseille a jugé que leur compte de campagne avait été rejeté à bon droit ;<br/>
<br/>
              Sur l'inéligibilité :<br/>
<br/>
              4. Considérant que le troisième alinéa de l'article L. 118-3 du code électoral dispose que le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, " prononce (...) l'inéligibilité du candidat dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales " ; que, pour déterminer si un manquement est d'une particulière gravité au sens de ces dispositions, il incombe au juge de l'élection d'apprécier, d'une part, s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et, d'autre part, s'il présente un caractère délibéré ; qu'en cas de manquement aux dispositions de l'article L. 52-4 du code électoral, il incombe, en outre, au juge de tenir compte de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que si M. D...et Mme A... ne pouvaient ignorer la portée des dispositions de l'article L. 52-4 du code électoral qu'ils ont méconnues, cette méconnaissance ne traduit aucune volonté de fraude de leur part ; que leur compte de campagne ne fait pas apparaître d'autres irrégularités de nature à justifier une déclaration d'inéligibilité ; qu'il résulte également de l'instruction que les dépenses directement acquittées par les candidats l'ont été par commodité et pour un montant global qui, sans être faible par rapport au total des dépenses du compte de campagne ni négligeable au regard du plafond de dépenses autorisées, est demeuré limité ; que pour blâmable qu'elle soit, pareille légèreté de la part de M. D...et Mme A...ne peut être qualifiée de manquement d'une particulière gravité aux règles de financement des campagnes électorales, de nature à justifier leur inéligibilité ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. D...et Mme A... ne sont fondés à demander l'annulation du jugement attaqué qu'en tant qu'il les déclare inéligibles pour une durée de six mois ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit à leurs conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 26 janvier 2016 est annulé en tant qu'il déclare M. D...et Mme A...inéligibles pour une durée de six mois à compter de la date du jugement.<br/>
Article 2 : Le surplus des conclusions présentées par M. D...et Mme A...est rejeté. <br/>
Article 3 : La présente décision sera notifiée à M. C...D...et Mme B...A..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
