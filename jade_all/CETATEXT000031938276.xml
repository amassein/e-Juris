<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938276</ID>
<ANCIEN_ID>JG_L_2016_01_000000384221</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/82/CETATEXT000031938276.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 27/01/2016, 384221, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384221</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:384221.20160127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif d'Amiens d'annuler pour excès de pouvoir la décision du 3 mars 2010 de l'inspectrice du travail de l'Oise autorisant son licenciement. Par un jugement n° 1001215 du 14 février 2013, le tribunal administratif a annulé cette décision. <br/>
<br/>
              Par un arrêt n° 13DA00688 du 22 mai 2014, la cour administrative d'appel de Douai a, sur appel de la société Continental France SNC, annulé ce jugement et rejeté la demande de M.B.... <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 septembre et 4 décembre 2014 et le 13 octobre 2015, au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Continental France SNC ;<br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et de la société Continental France SNC la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administratif.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Continental France SNC ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Continental France SNC, suite à sa décision de fermer son établissement de Clairoix, a demandé à l'inspecteur du travail de l'Oise l'autorisation de licencier pour motif économique M. B..., salarié protégé ; que par une décision du 3 mars 2010, l'inspecteur du travail a autorisé son licenciement ; que par un jugement du 14 février 2013, le tribunal administratif d'Amiens a annulé cette décision ; qu'enfin, sur appel de la société, la cour administrative d'appel de Douai a, par l'arrêt attaqué, annulé pour irrégularité le jugement du tribunal administratif d'Amiens et, statuant par la voie de l'évocation, rejeté la demande d'annulation de M. B...; <br/>
<br/>
              2. Considérant, en premier lieu, que la cour n'a pas méconnu la portée des écritures de M. B...en jugeant qu'il n'avait soulevé devant le tribunal administratif, dans le délai de recours contentieux contre la décision du 3 mars 2010, que des moyens de légalité externe ; qu'elle a pu par suite, sans erreur de droit, juger qu'en se fondant, pour annuler cette décision, sur des moyens de légalité interne soulevés par M. B...après l'expiration du délai de recours contentieux, le tribunal avait entaché son jugement d'irrégularité ; que M. B...n'est, par suite, pas fondé à demander l'annulation de l'arrêt attaqué en tant qu'il a annulé le jugement du tribunal administratif d'Amiens ;<br/>
<br/>
              3. Mais considérant, en second lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que l'établissement de Clairoix constituait l'un des deux établissements de la société Continental France SNC et que le second établissement de la société était maintenu en activité ; que, par suite, en relevant, pour rejeter la demande de M.B..., que la fermeture de l'établissement de Clairoix avait le caractère d'une cessation d'activité de la société Continental France SNC, la cour administrative d'appel de Douai a dénaturé les pièces du dossier ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'arrêt du 22 mai 2014 doit être annulé en tant seulement que, statuant après évocation, il a rejeté la demande de première instance de M. B...;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés protégés, qui bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle, est subordonné à une autorisation motivée de l'inspecteur du travail dont dépend l'établissement ;<br/>
<br/>
              7. Considérant qu'en se bornant à énoncer que " la société Continental France SNC a décidé de fermer son site de Clairoix et de licencier pour motif économique l'ensemble de ses salariés ", alors que, ainsi qu'il a été dit ci-dessus, l'établissement de Clairoix n'était pas le seul établissement de l'entreprise et que sa fermeture ne pouvait, par suite, justifier à elle seule le licenciement demandé, l'inspecteur du travail a insuffisamment motivé sa décision ; que M. B... est, par suite, fondé à demander son annulation ; <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Continental France SNC la somme de 3 500 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt n° 13DA00688 de la cour administrative d'appel de Douai du 22 mai 2014 est annulé en tant qu'il rejette la demande de première instance de M.B....<br/>
Article 2 : La décision de l'inspecteur du travail du 3 mars 2010 est annulée. <br/>
Article 3 : La société Continental France SNC versera une somme de 3 500 euros à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions de M. B...est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la société Continental France SNC.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
