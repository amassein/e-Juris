<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240918</ID>
<ANCIEN_ID>JG_L_2021_02_000000437235</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/09/CETATEXT000043240918.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 12/02/2021, 437235, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437235</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437235.20210212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés le 30 décembre 2019 et le 27 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision du 6 novembre 2019 de la procureure générale près la cour d'appel de Douai prononçant un avertissement à son encontre ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le code de justice administrative  et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... B..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que, par une décision du 6 novembre 2019, la procureure générale près la cour d'appel de Douai a infligé un avertissement à M. A..., procureur de la République près du tribunal de grande instance de Saint-Omer, pour des faits qu'elle a estimés constitutifs de manquements aux devoirs de l'état de magistrat, notamment aux devoirs de diligence, de délicatesse et de dignité, et qu'elle a regardé comme ayant, en outre, porté atteinte à l'image de l'institution judiciaire. M. A... demande au Conseil d'Etat d'annuler cette décision.<br/>
<br/>
              2. Aux termes de l'article 43 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Tout manquement par un magistrat aux devoirs de son état, à l'honneur, à la délicatesse ou à la dignité, constitue une faute disciplinaire (...) ". Aux termes de l'article 44 de la même ordonnance : " En dehors de toute action disciplinaire, l'inspecteur général, chef de l'inspection générale de la justice, les premiers présidents, les procureurs généraux et les directeurs ou chefs de service à l'administration centrale ont le pouvoir de donner un avertissement aux magistrats placés sous leur autorité. / Le magistrat à l'encontre duquel il est envisagé de délivrer un avertissement est convoqué à un entretien préalable. Dès sa convocation à cet entretien, le magistrat a droit à la communication de son dossier et des pièces justifiant la mise en oeuvre de cette procédure. Il est informé de son droit de se faire assister de la personne de son choix. / (...) L'avertissement est effacé automatiquement du dossier au bout de trois ans si aucun nouvel avertissement ou aucune sanction disciplinaire n'est intervenu pendant cette période. " L'article 45 de la même ordonnance définit par ailleurs les sanctions disciplinaires applicables aux magistrats.<br/>
<br/>
              3. S'il ne constitue pas une sanction disciplinaire au sens de l'article 45 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature, un avertissement donné sur le fondement de l'article 44 de cette ordonnance est une mesure prise en considération de la personne, qui est mentionnée au dossier du magistrat, duquel il n'est effacé automatiquement qu'en l'absence de nouvel avertissement ou de sanction disciplinaire dans les trois années suivantes. En vertu des dispositions précitées de l'article 44 de l'ordonnance du 22 décembre 1958, le magistrat auquel il est envisagé d'infliger un avertissement doit être convoqué à un entretien préalable et a droit, dès cette convocation, à la communication de son dossier et des pièces justifiant la mise en oeuvre de la procédure susceptible de conduire au prononcé d'un avertissement.<br/>
<br/>
              4. Si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie.<br/>
<br/>
              5. Il ressort des pièces du dossier que M. A..., d'une part, a été convoqué par une lettre de la procureure générale près la cour d'appel de Douai en date du 4 avril 2019 à une " audition ", le 30 avril 2019, dans le cadre de la préparation du rapport qui avait été demandé à la procureure générale par le directeur des services judiciaires sur le " management " du parquet de Saint-Omer et, d'autre part, a reçu de la procureure générale une lettre datée du 20 septembre 2019 l'informant qu'elle envisageait de lui infliger un avertissement reposant sur les pièces qui avaient été portées à sa connaissance à  l'occasion de l'audition du 30 avril 2019 et l'invitant à lui faire d'ultimes observations avant le 11 octobre 2019. Toutefois, " l'audition " du 30 avril 2019, est présentée par la lettre du 4 avril comme une simple " audition sur le fond ", réalisée dans le cadre de la préparation du rapport demandé à la procureure générale par le directeur des services judiciaires en septembre 2018 à la suite d'un courrier d'un syndicat de magistrats et pour la préparation duquel la procureure générale avait déjà auditionné une quinzaine de personnes entre le 22 octobre 2018 et le 22 mars 2019. Il ne ressort pas des pièces du dossier que M. A... aurait été informé, à l'occasion de cette " audition ", qu'un avertissement était envisagé à son encontre. Cette " audition " ne peut dès lors être regardée comme constituant l'entretien préalable exigé par l'article 44 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature, alors même que M. A... y a participé en étant assisté par un avocat. La possibilité qui lui a par ailleurs été laissée, par la lettre du 20 septembre 2019 l'informant qu'un avertissement était envisagé à son encontre, de faire par écrit " d'ultimes observations ", ne peut davantage être regardée comme de nature à tenir lieu de l'entretien préalable exigé par l'article 44 de l'ordonnance du 22 décembre 1958. Par suite, M. A... est fondé à soutenir qu'il a été privé de la garantie, résultant de l'article 44 de l'ordonnance du 22 décembre 1958, que constitue la convocation à un entretien préalable et qu'ainsi, la décision du 6 novembre 2019 par laquelle la procureure générale près la cour d'appel de Douai lui a infligé un avertissement est intervenue à l'issue d'une procédure irrégulière.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens de sa requête, que M. A... est fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 6 novembre 2019 par laquelle la procureure générale près la cour d'appel de Douai a infligé un avertissement à M. A... est annulée.<br/>
<br/>
Article 2 : L'Etat versera à M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C... A... et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
