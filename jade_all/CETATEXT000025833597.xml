<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025833597</ID>
<ANCIEN_ID>JG_L_2012_05_000000355756</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/83/35/CETATEXT000025833597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 09/05/2012, 355756, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355756</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP BORE ET SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:355756.20120509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 et 26 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE SAINT-MAUR-DES-FOSSES, représentée par son maire ; la COMMUNE DE SAINT-MAUR-DES-FOSSES demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1109268-2 du 26 décembre 2011 par laquelle le juge des référés du tribunal administratif de Melun, statuant en application de l'article L. 551-1 du code de justice administrative, a, à la demande de la société Bâtiment Industrie Réseaux, d'une part, annulé la procédure de passation du marché engagée par la commune pour la réalisation de travaux sur son réseau d'eau potable à compter de la remise des offres et, d'autre part, enjoint à la commune, si elle entendait conclure le contrat, de reprendre la procédure de passation du marché à ce stade ; <br/>
<br/>
              2°) statuant en référé, de faire droit à ses conclusions de première instance ; <br/>
<br/>
              3°) de mettre à la charge de la société Bâtiment Industrie Réseaux la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de la COMMUNE DE SAINT-MAUR-DES-FOSSES et de la SCP Boré et Salve de Bruneton, avocat de la société Bâtiment Industrie Réseaux, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de la COMMUNE DE SAINT-MAUR-DES-FOSSES et à la SCP Boré et Salve de Bruneton, avocat de la société Bâtiment Industrie Réseaux ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation (...). / Le juge est saisi avant la conclusion du contrat " ; qu'aux termes de l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ; qu'en application de ces dispositions, il appartient au juge des référés précontractuels de rechercher si l'entreprise qui le saisit se prévaut de manquements qui, eu égard à leur portée et au stade de la procédure auxquels ils se rapportent, sont susceptibles de l'avoir lésée ou risquent de la léser, fût-ce de façon indirecte en avantageant une entreprise concurrente ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la COMMUNE DE SAINT-MAUR-DES-FOSSES a lancé, le 30 août 2011, une procédure d'appels d'offres ouvert pour la passation d'un marché de travaux portant sur l'amélioration de son réseau d'eau potable ; que par lettre du 25 novembre 2011, la commune a informé la société Bâtiment Industrie Réseaux, qui s'était portée candidate à l'attribution du marché, que son offre avait été rejetée sans être examinée au motif que cette société " a des liens avec un des membres du conseil municipal " ; que, saisi par la société Bâtiment Industrie Réseaux sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative, le juge des référés du tribunal administratif de Melun a, par l'ordonnance attaquée du 26 décembre 2011, annulé la procédure de passation du marché à compter de la remise des offres et enjoint à la commune, si elle entendait conclure le contrat, de reprendre la procédure à ce stade ; <br/>
<br/>
              Considérant que le juge des référés a relevé que Mme Valérie Fiastre, conseillère municipale de Saint-Maur-des-Fossés déléguée à l'urbanisme, avait un lien de parenté avec le président de la société Bâtiment Industrie Réseaux, était actionnaire de cette société et avait participé à la délibération du conseil municipal autorisant le lancement de la procédure de passation du marché, mais que, d'une part, à ce stade de la délibération, la procédure n'avait pas encore été organisée et les soumissionnaires n'étaient pas connus et que, d'autre part, Mme Fiastre n'avait pas siégé à la commission d'appel d'offres et n'avait pris aucune part dans le choix de l'entreprise attributaire ; qu'ayant, ce faisant, porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation, le juge des référés, s'agissant de travaux habituels dont l'utilité n'était pas contestée et sur la définition et le lancement desquels il n'est pas allégué que l'intéressée aurait exercé une influence particulière, n'a ni inexactement qualifié ces faits ni commis d'erreur de droit en jugeant qu'ils n'étaient pas susceptibles de faire naître un doute sur l'impartialité du pouvoir adjudicateur et, qu'en conséquence, en éliminant par principe l'offre de la société Bâtiment Industrie Réseaux, celui-ci avait méconnu le principe de libre accès à la commande publique et manqué à ses obligations de mise en concurrence ; qu'il suit de là que le pourvoi de la COMMUNE DE SAIT-MAUR-DES-FOSSES doit être rejeté ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la société Bâtiment Industrie Réseaux, qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée à ce titre  par la COMMUNE DE SAINT-MAUR-DES-FOSSES ; qu' il y a lieu en revanche, dans les circonstances de l'espèce, de faire droit aux conclusions de la société Bâtiment Industrie Réseaux présentées sur le fondement des mêmes dispositions et de mettre à la charge de la commune le versement à cette société de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
 Article 1er : Le pourvoi de la COMMUNE DE SAINT-MAUR-DES-FOSSES est rejeté.<br/>
<br/>
 Article 2 : La COMMUNE DE SAINT-MAUR-DES-FOSSES versera à la société Bâtiment Industrie Réseaux la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée à la COMMUNE DE SAINT-MAUR-DES-FOSSES et à la Société Bâtiment Industrie Réseaux.<br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MEMBRE DE L'ORGANE DÉLIBÉRANT DE LA COLLECTIVITÉ PUBLIQUE ADJUDICATRICE ACTIONNAIRE D'UNE ENTREPRISE CANDIDATE ET AYANT UN LIEN DE PARENTÉ AVEC SON DIRIGEANT - OFFRE ÉCARTÉE PAR PRINCIPE - ERREUR DE DROIT.
</SCT>
<ANA ID="9A"> 39-02-005 La seule circonstance qu'un membre du conseil municipal soit actionnaire d'une des entreprises cadidates à un marché de la commune et ait un lien de parenté avec son dirigeant ne justifie pas d'écarter par principe l'offre de cette société, alors qu'il s'agit d'un marché de travaux habituels dont l'utilité n'est pas contestée et qu'il n'est pas allégué que le conseiller municipal, qui n'a participé qu'à la délibération autorisant la procédure de passation du marché, aurait exercé une influence particulière sur le vote.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
