<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039400797</ID>
<ANCIEN_ID>JG_L_2019_11_000000434325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/40/07/CETATEXT000039400797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 15/11/2019, 434325, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET</AVOCATS>
<RAPPORTEUR>M. Sylvain Humbert</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:434325.20191115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B..., à l'appui de la demande qu'il a formée devant le tribunal administratif de Lille tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et des contributions sociales auxquelles il a été assujetti au titre de l'année 2010, a présenté un mémoire et des observations, enregistrées les 17 mai et 16 juillet 2019 au greffe de ce tribunal, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il a soulevé une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1706575-QPC du 6 septembre 2019, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 4ème chambre du tribunal administratif de Lille a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité ainsi soulevée portant sur la conformité aux droits et libertés garantis par la Constitution des articles 156 et 199 octodecies du code général des impôts.<br/>
<br/>
              Par la question prioritaire de constitutionnalité transmise, M. B... soutient que le 2° du II de l'article 156 et le II de l'article 199 octodecies du code général des impôts, applicables au litige et qui n'ont pas déjà été déclarées conformes à la Constitution, méconnaissent les principes d'égalité devant la loi et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              Par un mémoire, enregistré le 17 octobre 2019, le ministre de l'action et des comptes publics soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
- l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
- le code civil ;<br/>
- le code général des impôts, notamment ses articles 156 et 199 octodecies ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Humbert, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Colin-Stoclet, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 274 du code civil : " Le juge décide des modalités selon lesquelles s'exécutera la prestation compensatoire en capital parmi les formes suivantes : 1° Versement d'une somme d'argent (...) 2° Attribution de biens en propriété (...) ". Aux termes du premier alinéa de l'article 275 du même code : " Lorsque le débiteur n'est pas en mesure de verser le capital dans les conditions prévues par l'article 274, le juge fixe les modalités de paiement du capital, dans la limite de huit années, sous forme de versements périodiques (...) ". Aux termes du premier alinéa de l'article 276 du même code : " A titre exceptionnel, le juge peut, par décision spécialement motivée, lorsque l'âge ou l'état de santé du créancier ne lui permet pas de subvenir à ses besoins, fixer la prestation compensatoire sous forme de rente viagère (...) ". Enfin, aux termes de l'article 278 du même code, dans sa version applicable à l'année 2010 : " En cas de divorce par consentement mutuel, les époux fixent le montant et les modalités de la prestation compensatoire dans la convention qu'ils soumettent à l'homologation du juge. (...) La prestation peut prendre la forme d'une rente attribuée pour une durée limitée. (...) ".<br/>
<br/>
              3. Le régime fiscal de la prestation compensatoire versée en application des dispositions des articles 274, 275, 276 et 278 du code civil citées ci-dessus est fixé, pour le débiteur de la prestation, par les articles 156 et 199 octodecies du code général des impôts. Aux termes de l'article 156 du code général des impôts, dans sa rédaction applicable à l'année 2010 : " L'impôt sur le revenu est établi d'après le montant total du revenu net annuel dont dispose chaque foyer fiscal. Ce revenu net est déterminé (...) sous déduction : / II. Des charges ci-après lorsqu'elles n'entrent pas en compte pour l'évaluation des revenus des différentes catégories / (...) 2° (...) versements de sommes d'argent mentionnés à l'article 275 du code civil lorsqu'ils sont effectués sur une période supérieure à douze mois à compter de la date à laquelle le jugement de divorce, que celui-ci résulte ou non d'une demande conjointe, est passé en force de chose jugée et les rentes versées en application des articles 276, 278 (...) du même code en cas de (...) divorce (...) ". Aux termes de l'article 199 octodecies du même code, dans sa rédaction applicable à l'année 2010 : " I. Les versements de sommes d'argent et l'attribution de biens ou de droits effectués en exécution de la prestation compensatoire dans les conditions et selon les modalités définies aux articles 274 et 275 du code civil sur une période, conformément (...) au jugement de divorce, au plus égale à douze mois à compter de la date à laquelle le jugement de divorce, que celui-ci résulte ou non d'une demande conjointe, est passé en force de chose jugée, ouvrent droit à une réduction d'impôt sur le revenu lorsqu'ils proviennent de personnes domiciliées en France au sens de l'article 4 B. / La réduction d'impôt est égale à 25 % du montant des versements effectués, des biens ou des droits attribués, retenu pour la valeur fixée (...) par le jugement de divorce, et dans la limite d'un plafond égal à 30 500 &#128; apprécié par rapport à la période mentionnée au premier alinéa. (...) / Lorsque la prestation compensatoire prend la forme d'une rente conformément aux dispositions des articles 276,278 et 279-1 du code civil, la substitution d'un capital aux arrérages futurs, versé ou attribué sur une période au plus égale à douze mois à compter de la date à laquelle le jugement prononçant la conversion est passé en force de chose jugée, ouvre également droit à la réduction d'impôt (...) II. Nonobstant la situation visée au troisième alinéa, les dispositions du I ne s'appliquent pas lorsque la prestation compensatoire est versée pour partie sous forme de rente ".<br/>
<br/>
              4. Il résulte des dispositions citées au point 3 ci-dessus que les versements de sommes d'argent et l'attribution de biens ou de droits effectués en exécution de la prestation compensatoire en application des articles 274, 275 et 278 du code civil, sur une période au plus égale à douze mois à compter de la date à laquelle le jugement de divorce est passé en force de chose jugée, ouvrent droit, pour le débiteur, à la réduction d'impôt prévue au I de l'article 199 octodecies du code général des impôts sous la réserve, prévue au II de ce même article, de l'absence du versement, en plus de ce capital, d'une partie de la prestation compensatoire sous forme de rente. Par ailleurs, sont déductibles des revenus du débiteur, sur le fondement du 2° du II de l'article 156 du code général des impôts, les versements de sommes d'argent effectués en application des articles 274, 275 et 278 du code civil sur une période supérieure à douze mois à compter de la date à laquelle le jugement de divorce est passé en force de chose jugée ainsi que, le cas échéant, les rentes versées en application des articles 276 et 278 du même code. Ainsi, en cas de paiement d'une prestation compensatoire à la fois sous forme d'un capital versé dans une période de douze mois et sous forme de rentes, le montant du capital versé n'ouvre droit ni à la réduction d'impôt prévue à l'article 199 octodecies précité, ni à la déduction du revenu global prévue au 2° du II de l'article 156 précité.<br/>
<br/>
              5. Les dispositions du 2° du II de l'article 156 et du II de l'article 199 octodecies du code général des impôts citées au point 3 sont applicables au litige et n'ont pas été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel.<br/>
<br/>
              6. Le moyen tiré de ce que, en cas de paiement d'une prestation compensatoire à la fois sous forme d'un capital et sous forme de rentes, le traitement fiscal de la partie en capital versée dans une période inférieure à douze mois méconnaît le principe d'égalité devant les charges publiques dès lors que le montant versé n'ouvre droit ni à la réduction d'impôt prévue à l'article 199 octodecies précité ni à la déduction du revenu global prévue au 2° du II de l'article 156 précité, alors que le même montant est déductible lorsque la partie de la prestation compensatoire sous forme d'un capital est versée sur une période supérieure à douze mois ou bénéficie de la réduction d'impôt précitée lorsque la prestation compensatoire est versée uniquement sous forme de capital sur une période inférieure de douze mois, soulève une question présentant un caractère sérieux. Il y a lieu, dès lors, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du 2° du II de l'article 156 et de l'article 199 octodecies du code général des impôts dans leurs versions applicables à l'année 2010 est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., au Premier ministre et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
