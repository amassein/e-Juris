<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555843</ID>
<ANCIEN_ID>JG_L_2012_10_000000334987</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555843.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 29/10/2012, 334987, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334987</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Thierry Tuot</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:334987.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 24 décembre 2009 au secrétariat du contentieux du Conseil d'Etat, présenté par l'Autorité de régulation des communications électroniques et des postes (ARCEP), dont le siège est au 7 square Max Hymans à Paris Cedex 15 (75730) ; l'ARCEP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07PA01797 du 22 octobre 2009 par lequel la cour administrative d'appel de Paris a, sur l'appel de la société CG Pan European Crossing France, d'une part, annulé le jugement n° 0512833/7-1 du 29 mars 2007 du tribunal administratif de Paris rejetant les conclusions de la société tendant à la restitution de la taxe de gestion et de contrôle acquittée au titre de l'année 2000 et au versement des intérêts moratoires afférents à la taxe acquittée au titre de l'année 1999 et remboursée en cours d'instance, et, d'autre part, condamné l'Etat à rembourser à la société CG Pan European Crossing France la somme de 330 375,77 euros, correspondant à la taxe acquittée au titre de l'année 2000, assortie des intérêts au taux légal à compter du 24 mars 2004, capitalisés à la date du 9 juin 2008 et à chaque échéance annuelle à compter de cette date ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société CG Pan European Crossing France ;<br/>
<br/>
              3°) de mettre à la charge de la société CG Pan European Crossing France le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 97/13/CEE du 10 avril 1997 relative à un cadre commun pour les autorisations générales et les licences individuelles dans le secteur des services des télécommunications ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code des postes et télécommunications ;<br/>
<br/>
              Vu la loi n° 86-1317 du 30 décembre 1986 de finances pour 1987, notamment son article 45 ;<br/>
<br/>
              Vu la loi n° 91-1323 du 30 décembre 1991 de finances rectificatives pour 1991 ;<br/>
<br/>
              Vu la loi n° 96-1181 du 30 décembre 1996 de finances pour 1997, notamment son article 36 ;<br/>
<br/>
              Vu la loi n° 99-1172 du 30 décembre 1999 de finances pour 2000 notamment son article 38 ;<br/>
<br/>
              Vu le décret n° 62-1587 du 29 décembre 1962 ;<br/>
<br/>
              Vu le décret n° 92-1369 du 29 décembre 1992 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la société CG Pan European Crossing France,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la société CG Pan European Crossing France ;<br/>
<br/>
<br/>
<br/>1. Considérant que le paragraphe VII de l'article 45 de la loi du 30 décembre 1986 de finances pour 1987, issu de l'article 36 de la loi du 30 décembre 1996 de finances pour 1997, a assujetti les titulaires d'autorisations relatives à des réseaux et services de télécommunications mentionnés aux articles L. 33-1 et L. 34-1 du code des postes et télécommunications à une taxe annuelle de gestion et de contrôle des autorisations dont ils disposent ; qu'aux termes du paragraphe VI de ce même article, dans sa rédaction issue de l'article 40 de la loi du 30 décembre 1991 de finances rectificatives pour 1991 : " Sauf en ce qui concerne la taxe forfaitaire prévue au premier alinéa du III, le recouvrement et le contentieux des taxes visées au présent article sont suivis par les comptables du Trésor selon les modalités fixées aux articles 80 à 95 du décret n° 62-1587 du 29 décembre 1962 portant règlement général sur la comptabilité publique dans leur rédaction en vigueur à la date de promulgation de la présente loi " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société CG Pan European Crossing France, attributaire d'une licence d'opérateur et de fournisseur de service de télécommunication au public, a été assujettie, à compter de l'année 1999, à la taxe annuelle de gestion et de contrôle de l'autorisation prévue par le VII de l'article 45 de la loi du 30 décembre 1986 de finances pour 1987 ; que, le tribunal administratif de Paris ayant jugé, par un jugement n° 9920574/7 et 0009720/7 du 19 juin 2003 devenu définitif, que l'article 22 de la loi du 30 décembre 1997 de finances pour 1998 avait augmenté les forfaits de cette taxe dans des conditions méconnaissant les objectifs fixés par l'article 11 de la directive 97/13/CEE du 10 avril 1997 relative à un cadre commun pour les autorisations générales et les licences individuelles dans le secteur des services de télécommunications, la société CG Pan European Crossing France a saisi, le 22 mars 2004, la ministre déléguée à l'industrie d'une demande tendant à la restitution des montants acquittés au titre de cette taxe pour les années 1999 et 2000 ; qu'en l'absence de réponse à cette demande, elle a porté le litige devant le tribunal administratif de Paris qui, par jugement du 29 mars 2007, a prononcé un non-lieu à statuer sur les conclusions de la demande tendant à la restitution des sommes acquittées au titre l'année 1999, du fait du remboursement de ces sommes intervenu en cours d'instance et a rejeté le surplus de la demande ; que, saisie en appel, la cour administrative d'appel de Paris a, par un arrêt du 22 octobre 2009, annulé ce jugement et condamné l'Etat à rembourser à la société CG Pan European Crossing France la somme de 330 375,77 euros acquittée au titre de la taxe annuelle de gestion et de contrôle de l'autorisation pour l'année 2000, assortie des intérêts au taux légal à compter du 24 mars 2004 et de la capitalisation des intérêts ; que l'Autorité de régulation des communications électroniques et des postes (ARCEP), qui a succédé à l'Autorité de régulation des télécommunications (ART), se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 190 du livre des procédures fiscales, dans sa rédaction alors applicable : " (...) Sont instruites et jugées selon les règles du présent chapitre toutes actions tendant à la décharge ou à la réduction d'une imposition ou à l'exercice de droits à déduction, fondées sur la non-conformité de la règle de droit dont il a été fait application à une règle de droit supérieure. / Lorsque cette non-conformité a été révélée par une décision juridictionnelle, l'action en restitution des sommes versées ou en paiement des droits à déduction non exercés ou l'action en réparation du préjudice subi ne peut porter que sur la période postérieure au 1er janvier de la quatrième année précédant celle où la décision révélant la non-conformité est intervenue. " ; que selon l'article R. 196-1 du même livre : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas :/ a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement ; / b) Du versement de l'impôt contesté lorsque cet impôt n'a pas donné lieu à l'établissement d'un rôle ou à la notification d'un avis de mise en recouvrement ; / c) De la réalisation de l'événement qui motive la réclamation. (...) " ;<br/>
<br/>
              4. Considérant que, pour rejeter la demande de la société CG Pan European Crossing France, le tribunal administratif de Paris l'avait jugée tardive au motif, notamment, que le jugement n° 9920574/7 et 0009720/7 du 19 juin 2003 ne pouvait être regardé comme une décision révélant la non-conformité à la règle de droit en vertu de laquelle la société avait dû s'acquitter de la somme de 330 375,77 euros au titre de la taxe de gestion et de contrôle de l'autorisation pour l'année 2000 et qu'il ne constituait donc pas un événement de nature à rouvrir, au bénéfice de la société, le délai de réclamation prévu par les dispositions précitées de l'article R. 196-1 du livre des procédures fiscales ; que la cour administrative d'appel a, pour sa part, estimé que la taxe en cause constituait une imposition non visée par le code général des impôts pouvant faire l'objet d'une réclamation dans les conditions prévues au VI de l'article 45 de la loi du 30 décembre 1986 de finances pour 1987 selon les seules modalités du décret du 29 décembre 1962 et que c'était, dès lors, à tort que le tribunal administratif s'était fondé sur les dispositions du c de l'article R. 196-1 du livre des procédures fiscales pour estimer que la réclamation préalable de la société était tardive et sa demande, en conséquence, irrecevable ; qu'après avoir annulé, pour ce motif, le jugement du tribunal, la cour, statuant par la voie de l'évocation a jugé, d'une part, que la réclamation formée, le 22 mars 2004, par la société CG Pan European Crossing France n'était pas tardive au regard des règles qui lui étaient applicables et, d'autre part, que la société était fondée à soutenir que la loi du 30 décembre 1999, en fixant les forfaits de la taxe de gestion et de contrôle pour l'année 2000, avait méconnu les objectifs de l'article 11 de la directive du 10 avril 1997 et à demander, pour ce motif, la restitution de la taxe acquittée au titre de cette année ;<br/>
<br/>
              5. Considérant, en premier lieu, que, pour juger la réclamation de la société non tardive et, par suite, sa demande recevable, la cour, après avoir cité les dispositions des articles R. 772-1, R. 772-2, R. 421-1, R. 421-3 et R. 421-5 du code de justice administrative et rappelé le contenu de l'article 1er du décret du 6 juin 2001 pris pour l'application du chapitre II du titre II de la loi n° 2000-321 du 12 avril 2000, a indiqué qu'il résultait de ces dispositions que l'absence de mention, tant sur l'ordre de paiement émis par une autorité administrative en vue du recouvrement d'une taxe que sur la décision rejetant la demande en décharge des sommes versées au titre de cette taxe, des voies et délais de recours faisait obstacle à ce que les délais fixés par l'article 8 du décret du 29 décembre 1992, d'une part, et l'article R. 421-1 du code de justice administrative, d'autre part, soient opposables ; qu'elle a ensuite relevé que la facture émise le 29 décembre 2000 par l'Autorité de régulation des Télécommunications, devenue ARCEP, à l'encontre de la société CG Pan European Crossing France, pour un montant de 330 375,77 euros, ne mentionnait pas les voies et délais de recours ; qu'elle en a déduit qu'aucun délai n'était opposable à la société pour effectuer sa réclamation et qu'elle avait pu, dès lors, valablement déposer, le 22 mars 2004, une réclamation en vue d'obtenir le remboursement de la taxe au titre de l'année 2000 ; qu'en statuant ainsi la cour a suffisamment motivé sa décision et n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant, en deuxième lieu, que la cour a jugé qu'en vertu de l'article 11 de la directive 97/13/CE du 10 avril 1997, le montant de la taxe de gestion et de contrôle de l'autorisation devait " permettre de couvrir exclusivement les coûts administratifs afférents au contrôle de l'utilisation des autorisations individuelles délivrées et/ou à la gestion du régime d'autorisations générales " ; qu'elle a relevé que l'Autorité de régulation des communications électroniques et des postes, qui se bornait à une description du processus d'élaboration de la décision, ne donnait aucun élément chiffré quant aux coûts exacts auxquels elle devait faire face, ni " aucune précision sur la détermination, de manière forfaitaire, sans tenir compte des particularités de chaque dossier, du montant de la taxe, et notamment sur la relation établie par le législateur entre la zone géographique couverte par une autorisation et le coût de contrôle de cette autorisation et/ou une répartition objective et non discriminatoire des frais plus globaux du régime d'autorisations générales " de nature à établir que la taxe litigieuse aurait satisfait à l'exigence de couverture exclusive du travail requis par la gestion et le contrôle des autorisations délivrées ; qu'elle a estimé que la circonstance que le montant de la taxe en cause avait fait l'objet de modifications forfaitaires au titre d'autres années ne suffisait pas à démontrer que ces changements auraient été eux-mêmes proportionnés à une évolution du coût des frais administratifs de gestion et de contrôle des autorisations délivrées aux réseaux ouverts au public ; qu'elle en a déduit que la loi du 30 décembre 1999 en fixant les forfaits de la taxe de gestion et de contrôle pour l'année 2000 avait méconnu les objectifs de l'article 11 de la directive du 10 avril 1997 ; que sa décision est, sur ce point encore, suffisamment motivée ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'il ressort des termes mêmes de son arrêt que la cour s'est à bon droit fondée, s'agissant de la taxe de gestion et de contrôle due au titre de l'année 2000, sur les dispositions du VII de l'article 45 de la loi du 30 décembre 1986 telles que modifiées par l'article 38 de la loi du 30 décembre 1999 de finances pour 2000 ; que l'ARCEP n'est, dès lors, pas fondée à soutenir que la cour aurait commis une erreur de droit quant au texte applicable ;<br/>
<br/>
              8. Considérant, en quatrième lieu, qu'il résulte des dispositions précitées du VI de l'article 45 de la loi du 30 décembre 1986 modifiée que le recouvrement et le contentieux de la taxe de gestion et de contrôle des autorisations sont suivis par les comptables du Trésor selon les modalités fixées aux articles 80 à 95 du décret du 29 décembre 1962 portant règlement général sur la comptabilité publique dans leur rédaction en vigueur à la date de promulgation de la loi du 30 décembre 1986 ; que la cour a, dès lors, commis une erreur de droit en estimant que les modalités de réclamation concernant cette taxe étaient régies par le décret du 29 décembre 1962 tel que modifié par le décret du 29 décembre 1992 ; que, toutefois, et ainsi qu'il ressort des termes mêmes de l'arrêt attaqué, les dispositions relatives aux réclamations applicables à cette taxe sont fixées, non pas par le décret du 29 décembre 1962 modifié, mais par les dispositions autonomes des articles 7 et 8 du décret n° 92-1369 du 29 décembre 1992 fixant les dispositions applicables au recouvrement des créances mentionnées à l'article 80 du décret du 29 décembre 1962, auxquelles l'article 89 de celui-ci, dans sa rédaction en vigueur à la date de promulgation de la loi du 30 décembre 1986, renvoyait d'ailleurs ; qu'il y a lieu, par suite, ainsi que le demande de la société CG Pan European Crossing France, de substituer ce fondement légal au fondement légal erroné mentionné par la cour ;<br/>
<br/>
              9. Considérant, enfin, que c'est sans commettre d'erreur de droit que la cour a jugé qu'en renvoyant, pour la détermination des règles applicables au contentieux relatif à la taxe de gestion et de contrôle des autorisations prévue au VII de l'article 45 de la loi du 30 décembre 1986, aux modalités fixées aux articles 80 à 95 du décret du 29 décembre 1962 portant règlement général sur la comptabilité publique, le législateur avait entendu exclure l'application des règles relatives au contentieux des impositions de toute nature prévues au livre des procédures fiscales ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que l'ARCEP n'est pas fondée à demander l'annulation de l'arrêt attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société CG Pan European Crossing France qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, en revanche, de mettre une somme de 2 000 euros à la charge de l'Etat au bénéfice de cette société ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de l'Autorité de régulation des communications électroniques et des postes est rejeté.<br/>
Article 2 : L'Etat versera une somme de 2 000 euros à la société CG Pan European Crossing France en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'Autorité de régulation des communications électroniques et des postes, à la société CG Pan European Crossing France et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
