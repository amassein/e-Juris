<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861178</ID>
<ANCIEN_ID>JG_L_2015_12_000000376018</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861178.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 23/12/2015, 376018, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376018</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP ORTSCHEIDT</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:376018.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Broadband Pacifique a demandé à l'arbitre désigné le 14 janvier 2013 par une ordonnance du président du tribunal de première instance de Nouméa de statuer sur les demandes d'interconnexion qu'elle a adressées à l'administrateur supérieur des îles Wallis et Futuna, d'évaluer le préjudice qu'elle a subi en raison du refus d'y faire droit, de condamner l'administration à réparer ce préjudice et d'enjoindre à cette dernière de se conformer à l'autorisation d'établir et d'exploiter un réseau de communications électroniques ouvert au public délivrée par un arrêté du 18 mai 2009 en faisant droit à ses demandes d'interconnexion. Par une sentence du 4 décembre 2013, l'arbitre désigné a fait droit à cette demande.<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 mars et 4 juin 2014 et le 14 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le préfet, administrateur supérieur des îles Wallis et Futuna demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette sentence arbitrale ;<br/>
<br/>
              2°) de rejeter la demande de la société Broadband Pacifique ; <br/>
<br/>
              3°) de mettre à la charge de la société Broadband Pacifique la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code civil ; <br/>
              - la loi n° 61-814 du 29 juillet 1961 ;<br/>
              - la décision n° 4025 du Tribunal des conflits du 16 novembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'Administration supérieure des îles Wallis et Futuna, et à la SCP Ortscheidt, avocat de la société Broadband Pacifique ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier que, par un arrêté du 18 mai 2009, l'administrateur supérieur des îles Wallis et Futuna a autorisé pour cinq ans la société Broadband Pacifique à établir et exploiter un réseau de communications électroniques sur le territoire de cette collectivité ; que le cahier des charges annexé à cet arrêté définit les conditions de cette exploitation et les obligations de la société ; que son article 10.1 prévoit que " l'interconnexion entre le réseau de l'opérateur et de celui du service des postes et télécommunications (SPT) fait l'objet d'une convention (...) Les litiges de toute nature relatifs à la conclusion ou à l'exécution de ces conventions seront arbitrés par un technicien indépendant désigné, à l'initiative de la partie la plus diligente, par le président du tribunal de première instance de Nouméa. Cette procédure arbitrale est limitée à la première instance, tout appel et autre recours relevant des tribunaux compétents de l'ordre judiciaire ou administratif " ; qu'après avoir vainement sollicité, après la délivrance de l'autorisation du 18 mai 2009, la conclusion d'une convention d'interconnexion avec le service des postes et télécommunications des îles Wallis et Futuna, la société Broadband Pacifique a demandé la désignation d'un arbitre en se fondant sur les dispositions précitées du cahier des charges ; que, par une sentence du 4 décembre 2013, l'arbitre nommé par l'ordonnance n° 12/00452 du président du tribunal de première instance de Nouméa, a jugé que le refus de l'administrateur supérieur méconnaissait le droit que tenait la société de l'arrêté du 18 mai 2009 de conclure une convention d'interconnexion et que cette illégalité fautive lui avait causé un préjudice direct qui devait être réparé ; que l'administrateur supérieur des îles Wallis et Futuna a formé, devant le Conseil d'Etat, une requête tendant à l'annulation de cette sentence arbitrale ; <br/>
<br/>
              Sur les fins de non-recevoir opposées par la société Broadband Pacifique :<br/>
<br/>
              2.	Considérant, d'une part, qu'il n'existe pas, dans le contentieux de la légalité, de principes généraux en vertu desquels une partie ne saurait se contredire dans la procédure contentieuse au détriment d'une autre partie ; que, d'autre part, l'administrateur supérieur des îles Wallis et Futuna est recevable à soulever pour la première fois devant le juge d'appel le moyen tiré de l'illégalité du recours à l'arbitrage sans que la société puisse utilement invoquer un " principe de  bonne foi " pour y faire obstacle ; que, dès lors, les fins de non-recevoir opposées par la société Broadband Pacifique  ne peuvent qu'être écartées ;<br/>
<br/>
              Sur la compétence :<br/>
<br/>
              3.	Considérant, ainsi que l'a jugé le Tribunal des conflits par décision du 16 novembre 2015, que le litige soumis à l'arbitre dont la sentence est contestée porte sur la légalité et les conséquences préjudiciables d'une décision administrative prise par l'administrateur supérieur des îles Wallis et Futuna dans l'exercice de sa prérogative d'autoriser l'exploitation, par un opérateur privé, d'un réseau de communications électroniques et d'en fixer les conditions ; qu'un tel litige relève de la compétence de la juridiction de l'ordre administratif ;<br/>
<br/>
              4.	Considérant que le Conseil d'Etat est compétent, en vertu de l'article L. 321-2 du code de justice administrative, pour connaître de l'appel formé par l'administrateur supérieur des îles Wallis et Futuna au nom de cette collectivité contre la sentence arbitrale du 4 décembre 2013 ;<br/>
<br/>
              Sur l'appel :<br/>
<br/>
              5.	Considérant qu'il résulte des principes généraux du droit public français que, sous réserve des dérogations découlant de dispositions législatives expresses ou, le cas échéant, des stipulations de conventions internationales régulièrement incorporées dans l'ordre juridique interne, les personnes morales de droit public ne peuvent pas se soustraire aux règles qui déterminent la compétence des juridictions nationales en remettant à la décision d'un arbitre la solution des litiges auxquelles elles sont parties ;<br/>
<br/>
              6.	Considérant qu'aucune disposition législative n'autorisait le préfet, administrateur supérieur des îles Wallis et Futuna, à soumettre à arbitrage les litiges de toute nature relatifs à la conclusion ou à l'exécution des conventions d'interconnexion entre le réseau d'un opérateur de communications électroniques et le service des postes et télécommunications de Wallis et Futuna ; qu'il suit de là que les dispositions de l'article 10.1.2 du cahier des charges annexé à l'arrêté du 18 mai 2009, par lequel le préfet, administrateur supérieur des îles Wallis et Futuna a autorisé la société Broadband Pacifique à établir et exploiter un réseau de communications électroniques ouvert au public, sont illégales ; que, par voie de conséquence, la sentence arbitrale rendue le 4 décembre 2013 est dépourvue de base légale ; que, dès lors et sans qu'il soit besoin d'examiner les autres moyens de la requête, le préfet, administrateur supérieur des îles Wallis et Futuna est fondé à demander l'annulation de la sentence rendue le 4 décembre 2013 ; <br/>
<br/>
              7.	Considérant qu'il y a lieu de renvoyer le litige au tribunal administratif de Wallis et Futuna, compétent pour en connaître en vertu des dispositions des articles L. 311-1 et R. 312-1 du code de justice administrative ;<br/>
<br/>
              8.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le préfet, administrateur supérieur des îles Wallis et Futuna au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce que soient accueillies les conclusions présentées au même titre par la société Broadband Pacifique ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La sentence arbitrale du 4 décembre 2013 est annulée.<br/>
<br/>
Article 2 : Le litige est renvoyé au tribunal administratif de Wallis et Futuna.<br/>
<br/>
Article 3 : Le surplus des conclusions de la requête et les conclusions présentées par la société Broadband Pacifique au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au territoire des îles Wallis et Futuna, à la société Broadband Pacifique, à la ministre des outre-mer et au président du tribunal administratif de Wallis et Futuna.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. - CONTESTATION D'UNE SENTENCE ARBITRALE PORTANT SUR LA LÉGALITÉ ET LES CONSÉQUENCES PRÉJUDICIABLES D'UNE DÉCISION PRISE PAR L'ADMINISTRATEUR SUPÉRIEUR DES ÎLES WALLIS-ET-FUTUNA DANS L'EXERCICE D'UNE PRÉROGATIVE DE PUISSANCE PUBLIQUE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-07-03 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. - 1) CONTESTATION D'UNE SENTENCE ARBITRALE PORTANT SUR LA LÉGALITÉ ET LES CONSÉQUENCES PRÉJUDICIABLES D'UNE DÉCISION PRISE PAR L'ADMINISTRATEUR SUPÉRIEUR DES ÎLES WALLIS-ET-FUTUNA DANS L'EXERCICE D'UNE PRÉROGATIVE DE PUISSANCE PUBLIQUE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE - EXISTENCE [RJ1] - 2) INTERDICTION POUR LES PERSONNES PUBLIQUES D'AVOIR RECOURS À L'ARBITRAGE - PRINCIPE GÉNÉRAL DU DROIT [RJ2] - 3) FACULTÉ DE SOULEVER POUR LA PREMIÈRE FOIS DEVANT LE JUGE D'APPEL LE MOYEN TIRÉ DE L'ILLÉGALITÉ DU RECOURS À L'ARBITRAGE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">46-01-02-04 OUTRE-MER. DROIT APPLICABLE. STATUTS. WALLIS ET FUTUNA. - COMPÉTENCE POUR AUTORISER L'EXPLOITATION, PAR UN OPÉRATEUR PRIVÉ, D'UN RÉSEAU DE COMMUNICATION ÉLECTRONIQUES ET EN FIXER LES CONDITIONS - COMPÉTENCE DU TERRITOIRE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-01-01 PROCÉDURE. VOIES DE RECOURS. APPEL. RECEVABILITÉ. - APPEL D'UNE SENTENCE ARBITRALE - 1) PRINCIPE DE L'ESTOPPEL - APPLICATION - ABSENCE [RJ3] - 2) FACULTÉ DE SOULEVER POUR LA PREMIÈRE FOIS DEVANT LE JUGE D'APPEL LE MOYEN TIRÉ DE L'ILLÉGALITÉ DU RECOURS À L'ARBITRAGE - EXISTENCE.
</SCT>
<ANA ID="9A"> 17-03-02 Le recours contre une sentence arbitrale rendue dans un litige qui porte sur la légalité et les conséquences préjudiciables d'une décision administrative prise par l'administrateur supérieur des îles Wallis-et-Futuna dans l'exercice de sa prérogative d'autoriser l'exploitation, par un opérateur privé, d'un réseau de communications électroniques et d'en fixer les conditions relève de la compétence de la juridiction de l'ordre administratif.</ANA>
<ANA ID="9B"> 37-07-03 1) Le recours contre une sentence arbitrale rendue dans un litige qui porte sur la légalité et les conséquences préjudiciables d'une décision administrative prise par l'administrateur supérieur des îles Wallis et Futuna dans l'exercice de sa prérogative d'autoriser l'exploitation, par un opérateur privé, d'un réseau de communications électroniques et d'en fixer les conditions relève de la compétence de la juridiction de l'ordre administratif.,,,2) Il résulte des principes généraux du droit public français que, sous réserve des dérogations découlant de dispositions législatives expresses ou, le cas échéant, des stipulations de conventions internationales régulièrement incorporées dans l'ordre juridique interne, les personnes morales de droit public ne peuvent pas se soustraire aux règles qui déterminent la compétence des juridictions nationales en remettant à la décision d'un arbitre la solution des litiges auxquelles elles sont parties.,,,3) Le moyen tiré de l'illégalité du recours à l'arbitrage peut être soulevé pour la première fois devant le juge d'appel saisi de la sentence arbitrale, sans que puisse être utilement invoqué un principe de bonne foi pour y faire obstacle.</ANA>
<ANA ID="9C"> 46-01-02-04 Le territoire de Wallis-et-Futuna est compétent pour autoriser l'exploitation, par un opérateur privé, d'un réseau de communications électroniques et pour en fixer les conditions.</ANA>
<ANA ID="9D"> 54-08-01-01 1) Il n'existe pas, dans le contentieux de la légalité, de principes généraux en vertu desquels une partie ne saurait se contredire dans la procédure contentieuse au détriment d'une autre partie.,,,2) Le moyen tiré de l'illégalité du recours à l'arbitrage peut être soulevé pour la première fois devant le juge d'appel saisi de la sentence arbitrale, sans que puisse être utilement invoqué un principe de bonne foi pour y faire obstacle.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. TC, 16 novembre 2015, Société Broadband Pacifique c/ Administrateur supérieur des îles Wallis-et-Futuna, n° 4025, à mentionner aux Tables.,,[RJ2]Cf. CE, 29 octobre 2004, M.,et autres, n°s 269814 e. a., p. 393., ,[RJ3]Cf. CE, 2 juillet 2014, Société Pace Europe, n° 368590, p. 206 ; pour le contentieux fiscal, CE, avis, 1er avril 2010, SAS Marsadis, n° 334465, p. 93.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
