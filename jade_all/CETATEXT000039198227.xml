<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039198227</ID>
<ANCIEN_ID>JG_L_2019_10_000000426349</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/82/CETATEXT000039198227.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 09/10/2019, 426349, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426349</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426349.20191009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire enregistrés le 17 décembre 2018 et le 24 mai 2019 au secrétariat du contentieux du Conseil d'Etat, l'Union des entreprises Transport et Logistique de France (UTL) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du ministre de l'action et des comptes publics du 17 octobre 2018 de rejet de sa demande d'abrogation, d'une part, des dispositions du 1er alinéa de l'article 5 de l'arrêté du 13 avril 2016 relatif à la représentation en douane et à l'enregistrement des représentants en douane, et, d'autre part, de l'avant-dernier alinéa du point 5 de la fiche 4 " Obligations du représentant en douane enregistré " de la circulaire du 14 juin 2018 relative aux modalités d'enregistrement et de suivi des représentants en douane enregistrés  ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement n° 952/2013 du Parlement européen et du Conseil du 9 octobre 2013 établissant le code des douanes de l'Union ;<br/>
              - le code civil ;<br/>
              - le code de commerce ;<br/>
              - l'arrêté du 13 avril 2016 relatif à la représentation en douane et à l'enregistrement des représentants en douane ;<br/>
              - la circulaire du 14 juin 2018 relative aux modalités d'enregistrement et de suivi des représentants en douane enregistrés ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'Union des entreprises Transport et Logistique de France ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 septembre 2019, présentée par l'Union des entreprises Transport et Logistique de France ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort de ses écritures que l'UTL critique, non pas l'avant-dernier alinéa du point 5 de la fiche n° 4 de la circulaire contestée, mais l'avant-dernier paragraphe de ce point, relatif à la possibilité de sous-traitance de l'établissement des déclarations en douane. Ainsi, sa requête doit être regardée, en tant qu'elle concerne la circulaire du ministre, comme dirigée contre l'ensemble de ce paragraphe.<br/>
<br/>
              2. Aux termes de l'article 19 du règlement du 9 octobre 2013 établissant le code des douanes de l'Union : " 1.  Lorsqu'il traite avec les autorités douanières, le représentant en douane déclare agir pour le compte de la personne représentée et précise s'il s'agit d'une représentation directe ou indirecte. / Les personnes qui ne déclarent pas qu'elles agissent en tant que représentant en douane ou qui déclarent agir en tant que représentant en douane sans y être habilitées sont réputées agir en leur nom propre et pour leur propre compte. / 2.  Les autorités douanières peuvent exiger des personnes déclarant agir en tant que représentant en douane la preuve de leur habilitation par la personne représentée. / (...) / 3.  Les autorités douanières n'exigent pas d'une personne agissant en tant que représentant en douane qui accomplit des actes ou des formalités régulièrement qu'elle fournisse à chaque occasion la preuve de son habilitation, pour autant que cette personne soit en mesure de fournir une telle preuve à la demande des autorités douanières ". <br/>
<br/>
              3. Pour l'application du point 2 de l'article 19 cité ci-dessus, l'article 5 de l'arrêté relatif à la représentation en douane et à l'enregistrement des représentants en douane pris le 13 avril 2016 par le ministre des finances et des comptes publics prévoit, dans sa rédaction modifiée par l'arrêté du 31 mai 2018, que " la preuve de l'habilitation par la personne représentée est une preuve écrite qui peut être fournie de manière électronique. " Aux termes de l'avant-dernier paragraphe du point 5 de la fiche n°4 de la circulaire du 14 juin 2018 du ministre de l'action et des comptes publics relative aux modalités d'enregistrement et de suivi des représentants en douane enregistrés : " le mandat de représentation (habilitation) initial doit prévoir la possibilité de sous-traitance de l'établissement des déclarations en douane. / Il est donc admis que la preuve de l'habilitation entre la personne représentée et le représentant au sens de l'article 19 du CDU puisse être apportée par la production d'un acte faisant intervenir un tiers. / Si le sous-traitant n'est pas indiqué de manière nominative dans le mandat de représentation (prévoyant une clause générale de sous-traitance de la prestation de passage des déclarations), le représentant initial doit informer la personne représentée de l'identité du représentant auquel il sous-traite le pouvoir et être en mesure d'apporter, sur demande du service des douanes, la preuve de cette information. / Cette information, quelle que soit sa forme écrite, satisfait le critère prescrit à l'article 19 du CDU, jusqu'à preuve du contraire ". L'UTL France demande l'annulation pour excès de pouvoir du refus d'abroger ces dispositions.<br/>
<br/>
              Sur la légalité externe des actes attaqués :<br/>
<br/>
              4. La requérante soutient que le ministre chargé des douanes n'était pas compétent, d'une part, pour prendre l'arrêté et la circulaire attaqués en tant qu'ils définissent le mode de preuve de l'habilitation des représentants en douane enregistrés et, d'autre part, pour prévoir, par la circulaire attaquée, la forme de l'information que le représentant en douane enregistré doit donner à son mandant sur l'identité du sous-traitant qu'il s'est le cas échéant substitué, ainsi que le mode de preuve de cette information. Elle fait valoir que ces règles de preuve et de forme des mandats relèvent de la compétence exclusive du législateur au titre des principes fondamentaux des obligations civiles et que le code des douanes n'a confié au ministre, pour l'application du droit de l'Union européenne, qu'une habilitation restreinte aux seules mesures nécessaires. <br/>
<br/>
              5. Toutefois, aux termes de l'article 17 bis du code des douanes : " Le ministre chargé des douanes arrête les mesures nécessaires à la mise en oeuvre des réglementations édictées par l'Union européenne ou par des traités ou accords internationaux régulièrement ratifiés ou approuvés par la France, que l'administration des douanes est tenue d'applique ". Sur le fondement de ces dispositions, le ministre a pu, pour assurer, ainsi que l'exige l'article 19 du code des douanes de l'Union, un contrôle étroit, par les autorités douanières, des pouvoirs dont se prévalent les professionnels agissant pour le compte des opérateurs économiques, édicter, par l'arrêté et la circulaire contestés, les mesures réglementaires nécessaires, au nombre desquelles la définition des modes de preuve, d'une part, de l'habilitation dont les représentants en douane enregistrés doivent pouvoir justifier auprès des autorités, tant dans le cas d'une délégation simple que d'une subdélégation, et d'autre part, de la bonne information du mandant sur l'identité du représentant agissant pour son compte. Contrairement à ce que soutient l'Union requérante, les règles ainsi édictées ne concernent pas les règles de formation et de délégation des mandats et ne constituent pas davantage des dérogations aux règles de preuve civile des mandats, dès lors qu'elles s'appliquent seulement aux relations entre l'administration et les opérateurs professionnels en douane à des fins de contrôle. <br/>
<br/>
              6. Il résulte de ce qui précède que les moyens tirés de ce que le ministre aurait méconnu l'habilitation prévue par l'article 17 bis du code des douanes et de ce qu'il aurait pris des mesures qui ne relèveraient que du législateur doivent être écartés.<br/>
<br/>
              Sur la légalité interne de l'arrêté :<br/>
<br/>
              7. En premier lieu, aux termes de l'article 1158 du code civil : " Le tiers qui doute de l'étendue du pouvoir du représentant conventionnel à l'occasion d'un acte qu'il s'apprête à conclure, peut demander par écrit au représenté de lui confirmer, dans un délai qu'il fixe et qui doit être raisonnable, que le représentant est habilité à conclure cet acte. / L'écrit mentionne qu'à défaut de réponse dans ce délai, le représentant est réputé habilité à conclure cet acte. " Aux termes de l'article 1985 du même code : " Le mandat peut être donné par acte authentique ou par acte sous seing privé, même par lettre. Il peut aussi être donné verbalement, mais la preuve testimoniale n'en est reçue que conformément au titre " Des contrats ou des obligations conventionnelles en général ". / L'acceptation du mandat peut n'être que tacite et résulter de l'exécution qui lui a été donnée par le mandataire. " Aux termes de l'article L. 110-3 du code de commerce : " A l'égard des commerçants, les actes de commerce peuvent se prouver par tous moyens à moins qu'il n'en soit autrement disposé par la loi ".<br/>
<br/>
              8. La requérante soutient que l'obligation de présenter une preuve écrite de leur habilitation mise à la charge des représentants en douane enregistrés méconnaît les dispositions des articles 1158 et 1985 du code civil et L. 110-3 du code de commerce citées ci-dessus. Toutefois, il résulte de ce qui a été dit au point 5 ci-dessus que les règles probatoires civiles ou commerciales sont sans incidence sur les obligations prévues par l'arrêté contesté, qui sont prévues aux seules fins du contrôle par l'administration des douanes. Par suite, l'Union requérante ne saurait utilement soutenir que les dispositions contestées méconnaîtraient les dispositions citées au point 7. <br/>
<br/>
              9. En second lieu, l'UTL France fait valoir qu'en ce qu'il prévoit que la preuve de l'habilitation du représentant en douane enregistré est nécessairement écrite, l'arrêté contesté est entaché d'une erreur manifeste d'appréciation. Toutefois, il résulte de ce qui a été dit ci-dessus aux points 5 et 8 que l'obligation ainsi prévue a pour objet de permettre le contrôle, par les autorités douanières, des opérations accomplies par les représentants en douane enregistrés. Par ailleurs, l'arrêté contesté, qui prévoit d'ailleurs que la preuve écrite de l'habilitation peut être dématérialisée, ne fait pas peser sur les professionnels concernés une charge excessive ou inappropriée. Au surplus, contrairement à ce soutient l'Union requérante, il est sans incidence sur le régime général de la responsabilité douanière des représentants. Le moyen soulevé par l'Union requérante ne peut dès lors qu'être écarté.<br/>
<br/>
              Sur la légalité interne de la circulaire :<br/>
<br/>
              10. Aux termes de l'article 1994 du code civil : " Le mandataire répond de celui qu'il s'est substitué dans la gestion : / 1° quand il n'a pas reçu le pouvoir de se substituer quelqu'un ; / 2° quand ce pouvoir lui a été conféré sans désignation d'une personne, et que celle dont il a fait choix était notoirement incapable ou insolvable. / Dans tous les cas, le mandant peut agir directement contre la personne que le mandataire s'est substituée. "<br/>
<br/>
              11. Dès lors que la circulaire contestée se borne à prévoir une obligation d'information permettant d'assurer l'efficacité du contrôle administratif des opérations en douane, sans édicter aucune règle dérogatoire en matière de responsabilité des mandataires, l'Union requérante ne peut utilement soutenir qu'elle méconnaîtrait les dispositions précitées. <br/>
<br/>
              12. Il résulte de tout ce qui précède que l'UTL France n'est pas fondée à demander l'abrogation de la décision du 17 octobre 2018 du ministre de l'action et des comptes publics refusant de faire droit à sa demande d'abroger le premier alinéa de l'article 5 de l'arrêté du 13 avril 2016 relatif à la représentation en douane et à l'enregistrement des représentants en douane et l'avant-dernier paragraphe du point 5 de la fiche n° 4 " Obligations du représentant en douane enregistré " de la circulaire du 14 juin 2018 relative aux modalités d'enregistrement et de suivi des représentants en douane enregistrés. Sa requête doit dès lors être rejetée, y compris les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'UTL est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'Union des entreprises Transport et Logistique de France et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
