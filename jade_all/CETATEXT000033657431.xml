<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033657431</ID>
<ANCIEN_ID>JG_L_2016_12_000000393521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/65/74/CETATEXT000033657431.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 16/12/2016, 393521, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393521.20161216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir la décision du 21 septembre 2012 par laquelle le préfet d'Ille-et-Vilaine a refusé de procéder à l'échange de son permis de conduire géorgien contre un permis de conduire français et d'enjoindre au préfet de procéder à cet échange. Par un jugement n° 1300342 du 10 juillet 2015, le tribunal administratif a annulé la décision du préfet d'Ille-et-Vilaine et enjoint à celui-ci de procéder à l'échange du permis de conduire dans un délai d'un mois à compter du jugement.<br/>
<br/>
              Par un pourvoi enregistré le 14 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le décret n° 2008-1281 du 8 décembre 2008 ;<br/>
<br/>
              - l'arrêté du ministre de l'équipement, des transports et du logement en date du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement et du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration en date du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., titulaire d'un permis de conduire délivré par les autorités de la Géorgie le 27 juillet 2001, dont il affirmait qu'il l'avait été en échange d'un permis de conduire délivré en 1989 par les autorités de l'Union des républiques socialistes soviétiques (URSS), a demandé le 14 décembre 2010 au préfet d'Ille-et-Vilaine d'échanger ce permis contre un permis de conduire français ; que le préfet a rejeté sa demande par une décision du 30 janvier 2011 ; qu'après l'annulation de cette décision par un jugement du 14 septembre 2012 du tribunal administratif de Rennes, le préfet d'Ille-et-Vilaine a de nouveau refusé d'échanger le permis de conduire géorgien de M. A...contre un permis de conduire français le 21 septembre 2012 ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 10 juillet 2015 par lequel le tribunal administratif de Rennes a annulé cette seconde décision à la demande de l'intéressé ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-3 du code de la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. Pendant ce délai, il peut être échangé contre le permis français, sans que son titulaire soit tenu de subir les examens prévus au premier alinéa de l'article  R. 221-3. Les conditions de cette reconnaissance et de cet échange sont définies par arrêté du ministre chargé des transports, après avis du ministre de la justice, du ministre de l'intérieur et du ministre chargé des affaires étrangères. Au terme de ce délai, ce permis n'est plus reconnu et son titulaire perd tout droit de conduire un véhicule pour la conduite duquel le permis de conduire est exigé " ; qu'aux termes de l'article 5 de l'arrêté du 12 janvier 2012 visé ci-dessus, pris pour l'application de ces dispositions : " I. - Pour être échangé contre un titre français, tout permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen doit répondre aux conditions suivantes :/ A.  Avoir été délivré au nom de l'Etat dans le ressort duquel le conducteur avait alors sa résidence normale, sous réserve qu'il existe un accord de réciprocité entre la France et cet Etat conformément à l'article R. 222-1 du code de la route./ (...)  " ;  qu'aux termes de l'article 14 du même arrêté : " Une liste des Etats dont les permis de conduire nationaux sont échangés en France contre un permis français est établie conformément aux articles R. 222-1 et R. 222-3 du code de la route. Cette liste précise pour chaque Etat la ou les catégories de permis de conduire concernée (s) par l'échange contre un permis français. Elle ne peut inclure que des Etats qui procèdent à l'échange des permis de conduire français de catégorie équivalente et dans lesquels les conditions effectives de délivrance des permis de conduire nationaux présentent un niveau d'exigence conforme aux normes françaises dans ce domaine. / Les demandes d'échange de permis introduites avant la date de publication au JORF de la liste prévue au premier alinéa du présent article sont traitées sur la base de la liste prévue à l'article 14 de l'arrêté du 8 février 1999 modifié fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen " ; que l'article 14 de l'arrêté du 8 février 1999 dispose que le ministre chargé des transports établit, après consultation du ministre des affaires étrangères, la liste des Etats qui procèdent à l'échange des permis de conduire français ; <br/>
<br/>
              3. Considérant qu'il résulte des termes du premier alinéa de l'article 14 de l'arrêté du 12 janvier 2012 cité ci-dessus que la liste d'Etats qu'il prévoit doit être établie conformément aux articles R. 222-1 et R. 222-3 du code de la route, à savoir " par arrêté du ministre chargé des transports, après avis du ministre de l'intérieur et du ministre chargé des affaires étrangères " ; qu'aucune liste n'a été établie par le ministre des transports en application de ces dispositions ; que le second alinéa du même article prévoit qu'en pareil cas les demandes d'échange sont traitées sur la base de la liste prévue à l'article 14 de l'arrêté du 8 février 1999 ; que si une circulaire du 22 septembre 2006 du ministre des transports avait fixé une liste d'Etats sur le fondement de cet article, l'annexe de cette circulaire fixant la liste n'a pas été mise en ligne sur le site internet relevant du Premier ministre prévu au premier alinéa de l'article 1er du décret du 8 décembre 2008 relatif aux conditions de publication des instructions et circulaires, repris à l'article R. 312-8 du code des relations entre le public et l'administration ; que, par suite, en application de l'article 2 du même décret, aux termes duquel les instructions et circulaire déjà signées " sont regardées comme abrogées si elles ne sont pas reprises sur le site mentionné à l'article 1er ", la liste doit être regardée comme abrogée ; que, dans ces conditions, pour déterminer si un permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen est susceptible d'être échangé contre un permis français, il y a seulement lieu de vérifier si, conformément aux dispositions précitées du I de l'article 5 de l'arrêté du 12 janvier 2012, cet Etat est lié à la France par un accord de réciprocité en matière d'échange de permis de conduire ; <br/>
<br/>
              4. Considérant que, pour annuler le refus opposé à la demande de M. A...par le préfet d'Ille-et-Vilaine, le tribunal administratif de Rennes, après avoir relevé qu'aucun accord de réciprocité portant sur les échanges de permis de conduire n'avait été conclu entre la France et la Géorgie, s'est fondé sur la circonstance " que la circulaire du 3 août 2012 prévoit, dans la liste prise en application de l'article 14 de l'arrêté du 12 janvier 2012, la possibilité, s'agissant de la Géorgie, d'un échange, dans le cas où le permis de conduire a été délivré avant le 1er janvier 1992 au nom de l'URSS " ;  que, toutefois, la circulaire du 3 août 2012 relative à la mise en oeuvre de l'arrêté du 12 janvier 2012, émanant du ministre de l'intérieur et non du ministre chargé des transports, n'a pu légalement avoir pour objet ni pour effet de fixer la liste prévue par l'article 14 de cet arrêté ; qu'en se fondant sur cette circulaire pour annuler la décision attaquée, le tribunal administratif a commis une erreur de droit ; que le ministre de l'intérieur est, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant que le permis de conduire présenté par M. A... ayant été délivré par les autorités géorgiennes, le préfet devait apprécier sa demande au regard des règles gouvernant l'échange des permis de conduire délivrés par ce pays ; que la circonstance, à la supposer établie, que ce permis lui avait été délivré en échange du permis de conduire qu'il avait antérieurement obtenu des autorités de l'URSS ne pouvait conduire à lui appliquer les règles d'échange applicables aux permis délivrés par cet Etat avant sa disparition ; qu'il n'est pas contesté qu'aucun accord de réciprocité n'existe entre la France et la Géorgie en matière d'échange de permis de conduire ; que, dès lors, le préfet était tenu, en application des dispositions de l'article 5 de l'arrêté du 12 janvier 2012 cité ci-dessus, de refuser l'échange demandé ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision de refus du préfet d'Ille-et-Vilaine ; que ses conclusions à fins d'injonction doivent, par voie de conséquence, être rejetées ainsi que ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Rennes du 10 juillet 2015 est annulé.<br/>
Article 2 : La demande présentée par M. A...devant le tribunal administratif de Rennes est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
