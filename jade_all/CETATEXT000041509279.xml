<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041509279</ID>
<ANCIEN_ID>JG_L_2020_01_000000421952</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/50/92/CETATEXT000041509279.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 30/01/2020, 421952</TITRE>
<DATE_DEC>2020-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421952</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421952.20200130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Païta a demandé au tribunal administratif de Nouvelle-Calédonie d'annuler pour excès de pouvoir la décision du président de la province Sud en date du 27 octobre 2016 refusant de lancer l'enquête administrative prévue à l'article 3 de la délibération n° 74 du 11 mars 1959 relative aux plans d'urbanisme en province Sud. Par un jugement n°1600394 du 15 juin 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA03089 du 29 mars 2018, la cour administrative d'appel de Paris a rejeté l'appel de la commune de Païta tendant à l'annulation de ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 3 juillet et 3 octobre 2018 et le 28 août 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Païta demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la province Sud de Nouvelle-Calédonie la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -  la loi organique n° 99-209 du 19 mars 1999 ; <br/>
              - la loi n° 2013-907 du 11 octobre 2013 ; <br/>
              - le décret n° 2014-90 du 31 janvier 2014 ; <br/>
              - le code des communes de la Nouvelle-Calédonie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Paita et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la Province Sud de Nouvelle-Calédonie<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une délibération du 14 octobre 2010, l'assemblée de la province Sud de Nouvelle-Calédonie a décidé d'élaborer le plan d'urbanisme directeur de la commune de Païta suivant les modalités définies par la délibération n° 74 des 10 et 11 mars 1959 portant réglementation de l'urbanisme en Nouvelle-Calédonie, maintenue en vigueur, à titre transitoire, par l'article 3 de la loi du pays n° 2015-1 du 13 février 2015, qui prévoit que les plans d'urbanisme directeurs " mis en élaboration ou en révision avant la publication de la présente loi du pays sont arrêtés et rendus publics dans le délai de deux ans à compter de cette publication ". Compte tenu de divergences d'appréciation entre la province Sud et la commune de Païta et dans l'attente d'une mise en conformité des projets de règlement et de zonage du plan, le président de l'assemblée de la province Sud a refusé le 28 avril 2016 de réunir le troisième comité d'études chargé de valider les étapes d'avancement de l'élaboration du plan d'urbanisme directeur, puis réitéré ce refus par une nouvelle décision du 22 juillet 2016. Toutefois, en exécution d'une ordonnance du juge des référés du tribunal administratif de Nouvelle-Calédonie du 8 septembre 2016, le président de l'assemblée de la province Sud a convoqué le comité d'études qui s'est réuni le 29 septembre 2016. Se fondant sur les réserves émises par ce comité et sur l'absence d'accord sur les modifications à apporter au projet, le président de l'assemblée de la province Sud, par un courrier du 27 octobre 2016, a refusé d'ouvrir l'enquête administrative relative au plan d'urbanisme directeur.<br/>
<br/>
              2. Par un jugement du 15 juin 2017, le tribunal administratif de Nouvelle-Calédonie a rejeté la demande de la commune de Païta tendant à l'annulation de la décision du 27 octobre 2016 du président de l'assemblée de la province Sud refusant d'ouvrir l'enquête administrative relative au plan d'urbanisme directeur de cette commune. La commune de Païta se pourvoit en cassation contre l'arrêt du 29 mars 2018 de la cour administrative d'appel de Paris confirmant ce jugement.<br/>
<br/>
              3. D'une part, l'article 2 de la loi du 11 octobre 2013 relative à la transparence de la vie publique, applicable en Nouvelle-Calédonie en vertu de son article 35, dispose que : " Au sens de la présente loi, constitue un conflit d'intérêts toute situation d'interférence entre un intérêt public et des intérêts publics ou privés qui est de nature à influencer ou à paraître influencer l'exercice indépendant, impartial et objectif d'une fonction. / Lorsqu'ils estiment se trouver dans une telle situation : / (...)  2° Sous réserve des exceptions prévues au deuxième alinéa de l'article 432-12 du code pénal, les personnes titulaires de fonctions exécutives locales sont suppléées par leur délégataire, auquel elles s'abstiennent d'adresser des instructions (...) ". L'article 5 du décret du 31 janvier 2014 portant application de l'article 2 de cette loi, applicable en Nouvelle-Calédonie en vertu de son article 8, dispose que : " Le présent article est applicable aux titulaires d'une fonction (...) de maire (...). / Lorsqu'elles estiment se trouver en situation de conflit d'intérêts, qu'elles agissent en vertu de leurs pouvoirs propres ou par délégation de l'organe délibérant, les personnes mentionnées au précédent alinéa prennent un arrêté mentionnant la teneur des questions pour lesquelles elles estiment ne pas devoir exercer leurs compétences et désignant, dans les conditions prévues par la loi, la personne chargée de les suppléer. / (...) elles ne peuvent adresser aucune instruction à leur délégataire ".<br/>
<br/>
              4. D'autre part, aux termes de l'article L. 316-1 du code des communes de la Nouvelle-Calédonie : " Sous réserve des dispositions du 15 de l'article L. 122-20, le conseil municipal délibère sur les actions à intenter au nom de la commune ". Aux termes de l'article L. 122-20 du même code : " Le maire peut, en outre, par délégation du conseil municipal, être chargé, en tout ou en partie et pour la durée de son mandat :/ (...) 15° D'intenter au nom de la commune les actions en justice ou de défendre la commune dans les actions intentées contre elle dans les cas définis par le conseil municipal (...) ". L'article L. 122-21 du même code prévoit que : " Les décisions prises par le maire en vertu du précédent article sont soumises aux mêmes règles que celles qui sont applicables aux délibérations des conseils municipaux portant sur les mêmes objets. (...)/ Sauf disposition contraire dans la délibération, les décisions relatives aux matières ayant fait l'objet de la délégation sont prises, en cas d'empêchement du maire, par le conseil municipal (...) ". Enfin, aux termes de l'article L. 122-12 du même code, qui sont les mêmes que ceux de l'article L. 2122-26 du code général des collectivités territoriales : " Dans le cas où les intérêts du maire se trouvent en opposition avec ceux de la commune, le conseil municipal désigne un autre de ses membres pour représenter la commune, soit en justice, soit dans les contrats ".<br/>
<br/>
              5. Il résulte d'une part des dispositions, citées au point 3, de l'article 2 de la loi du 11 octobre 2013 relative à la transparence de la vie publique et de l'article 5 du décret du 31 janvier 2014 pris pour son application, qu'un maire qui estime se trouver dans une situation de conflit d'intérêts doit prendre un arrêté mentionnant la teneur des questions pour lesquelles il estime ne pas devoir exercer ses compétences et désigner, dans les conditions prévues par la loi, la personne chargée de le suppléer. D'autre part, il résulte des dispositions du code des communes de la Nouvelle-Calédonie citées au point 4, notamment de l'article L. 122-12 du code des communes de la Nouvelle-Calédonie, que lorsque les intérêts du maire se trouvent en opposition avec ceux de la commune dans un litige donné ou pour la signature ou l'exécution d'un contrat, seul le conseil municipal est compétent pour désigner un autre de ses membres soit pour représenter la commune en justice soit pour signer le contrat ou intervenir dans son exécution. Il s'ensuit que lorsque le maire estime ne pas devoir exercer ses compétences à raison d'un conflit d'intérêts, il ne saurait désigner la personne habilitée soit à représenter la commune en justice dans un litige donné soit à signer ou exécuter un contrat que si ses intérêts ne se trouvent pas en opposition avec ceux de la commune. Lorsqu'une telle opposition ressort des pièces du dossier qui lui est soumis, il appartient au juge de relever d'office l'irrecevabilité de la demande de la commune représentée par son maire ou par une personne qui n'a pas été légalement désignée. <br/>
<br/>
              6. En se bornant à relever, pour juger irrecevable la demande de la commune représentée par le premier adjoint au maire enregistrée au greffe du tribunal administratif le 7 novembre 2016, qu'en dépit de l'arrêté du 15 janvier 2016 par lequel le maire de Païta avait délégué à son premier adjoint ses compétences en matière d'urbanisme, seul le conseil municipal de la commune avait compétence pour désigner un autre de ses membres pour ester en justice en son nom, sans rechercher si les intérêts du maire se trouvaient, dans ce litige, en opposition avec ceux de la commune, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, son arrêt du 29 mars 2018 doit être annulé.<br/>
<br/>
              Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la province Sud de Nouvelle-Calédonie la somme de 3 000 euros à verser à la commune de Païta au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Païta qui n'est pas, dans cette instance, la partie perdante.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                        --------------<br/>
<br/>
Article 1er : L'arrêt n° 17PA03089 du 29 mars 2018 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : La province Sud de Nouvelle-Calédonie versera une somme de 3 000 euros à la commune de Païta au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la province Sud de Nouvelle-Calédonie sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Païta et à la province Sud de Nouvelle-Calédonie.<br/>
Copie en sera adressée au ministre des outre-mer et au haut-commissaire de la République en Nouvelle-Calédonie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-02-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. DISPOSITIONS GÉNÉRALES. - PROCÉDURE DE DÉPORT PRÉVUE PAR L'ARTICLE 2 DE LA LOI DU 11 OCTOBRE 2013 EN CAS DE CONFLIT D'INTÉRÊTS ENTRE LE MAIRE ET LA COMMUNE - PROCÉDURE PRÉVUE PAR L'ARTICLE L. 2122-26 DU CGCT EN CAS D'OPPOSITION D'INTÉRÊTS - ARTICULATION ENTRE LES DEUX PROCÉDURES EN CAS DE CONFLIT D'INTÉRÊTS - 1) PRINCIPE - FACULTÉ POUR LE MAIRE DE DÉSIGNER LA PERSONNE HABILITÉE À REPRÉSENTER LA COMMUNE EN JUSTICE OU À SIGNER OU EXÉCUTER UN CONTRAT, SAUF SI SES INTÉRÊTS SONT EN OPPOSITION AVEC CEUX DE LA COMMUNE DANS L'AFFAIRE EN CAUSE - 2) ILLUSTRATION - CAS DANS LEQUEL UN MAIRE S'EST DÉPORTÉ DE CERTAINES DE SES COMPÉTENCES AU PROFIT DE L'UN DE SES ADJOINTS - OBLIGATION DU JUGE DE RECHERCHER SI LES INTÉRÊTS DU MAIRE SE TROUVENT, DANS LE LITIGE DONT IL EST SAISI, EN OPPOSITION AVEC CEUX DE LA COMMUNE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-05-005 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. REPRÉSENTATION DES PERSONNES MORALES. - PROCÉDURE DE DÉPORT PRÉVUE PAR L'ARTICLE 2 DE LA LOI DU 11 OCTOBRE 2013 EN CAS DE CONFLIT D'INTÉRÊTS ENTRE LE MAIRE ET LA COMMUNE - PROCÉDURE PRÉVUE PAR L'ARTICLE L. 2122-26 DU CGCT EN CAS D'OPPOSITION D'INTÉRÊTS - ARTICULATION ENTRE LES DEUX PROCÉDURES EN CAS DE CONFLIT D'INTÉRÊTS - 1) A) FACULTÉ POUR LE MAIRE DE DÉSIGNER LA PERSONNE HABILITÉE À REPRÉSENTER LA COMMUNE EN JUSTICE OU À SIGNER OU EXÉCUTER UN CONTRAT, SAUF SI SES INTÉRÊTS SONT EN OPPOSITION AVEC CEUX DE LA COMMUNE DANS L'AFFAIRE EN CAUSE - B) OFFICE DU JUGE EN CAS D'OPPOSITION D'INTÉRÊTS - OBLIGATION DE RELEVER D'OFFICE L'IRRECEVABILITÉ DE LA DEMANDE PRÉSENTÉE PAR LE MAIRE OU LA PERSONNE QU'IL A DÉSIGNÉE - 3) ILLUSTRATION - CAS DANS LEQUEL UN MAIRE S'EST DÉPORTÉ DE CERTAINES DE SES COMPÉTENCES AU PROFIT DE L'UN DE SES ADJOINTS - OBLIGATION DU JUGE DE RECHERCHER SI LES INTÉRÊTS DU MAIRE SE TROUVENT, DANS LE LITIGE DONT IL EST SAISI, EN OPPOSITION AVEC CEUX DE LA COMMUNE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-02-01-02 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. CONDITIONS DE RECEVABILITÉ. - OPPOSITION DES INTÉRÊTS DU MAIRE AVEC CEUX DE LA COMMUNE DANS UN LITIGE DONNÉ - CONSEIL MUNICIPAL SEUL COMPÉTENT POUR DÉSIGNER UN AUTRE DE SES MEMBRES AFIN DE REPRÉSENTER LA COMMUNE (ART. L. 2122-26 DU CGCT) - CONSÉQUENCE - IRRECEVABILITÉ DE LA DEMANDE PRÉSENTÉE PAR LE MAIRE OU PAR UNE PERSONNE ILLÉGALEMENT DÉSIGNÉE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-04-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. - OPPOSITION DES INTÉRÊTS DU MAIRE AVEC CEUX DE LA COMMUNE DANS UN LITIGE DONNÉ - CONSEIL MUNICIPAL SEUL COMPÉTENT POUR DÉSIGNER UN AUTRE DE SES MEMBRES AFIN DE REPRÉSENTER LA COMMUNE (ART. L. 2122-26 DU CGCT) - OFFICE DU JUGE - OBLIGATION DE RELEVER D'OFFICE L'IRRECEVABILITÉ DE LA DEMANDE PRÉSENTÉE PAR LE MAIRE OU PAR UNE PERSONNE ILLÉGALEMENT DÉSIGNÉE.
</SCT>
<ANA ID="9A"> 135-02-01-02-02-01 Il résulte de l'article 2 de la loi n° 2013-907 du 11 octobre 2013 et de l'article 5 du décret n° 2014-90 du 31 janvier 2014 pris pour son application qu'un maire qui estime se trouver dans une situation de conflit d'intérêts doit prendre un arrêté mentionnant la teneur des questions pour lesquelles il estime ne pas devoir exercer ses compétences et désigner, dans les conditions prévues par la loi, la personne chargée de le suppléer.,,,Il résulte des dispositions du code des communes de la Nouvelle-Calédonie, notamment de son article L. 122-12, rédigé dans des termes identiques à l'article L. 2122-26 du code général des collectivités territoriales (CGCT), que lorsque les intérêts du maire se trouvent en opposition avec ceux de la commune dans un litige donné ou pour la signature ou l'exécution d'un contrat, seul le conseil municipal est compétent pour désigner un autre de ses membres soit pour représenter la commune en justice soit pour signer le contrat ou intervenir dans son exécution.,,,1) Lorsque le maire estime ne pas devoir exercer ses compétences à raison d'un conflit d'intérêts, il ne saurait désigner la personne habilitée soit à représenter la commune en justice dans un litige donné soit à signer ou exécuter un contrat que si ses intérêts ne se trouvent pas en opposition avec ceux de la commune.... ,,2) Commet une erreur de droit une cour qui se borne à relever, pour juger irrecevable la demande d'une commune représentée par le premier adjoint au maire, qu'en dépit de l'arrêté par lequel le maire avait délégué à son premier adjoint ses compétences en matière d'urbanisme, seul le conseil municipal de la commune avait compétence pour désigner un autre de ses membres pour ester en justice en son nom, sans rechercher si les intérêts du maire se trouvaient, dans ce litige, en opposition avec ceux de la commune.</ANA>
<ANA ID="9B"> 54-01-05-005 Il résulte de l'article 2 de la loi n° 2013-907 du 11 octobre 2013 et de l'article 5 du décret n° 2014-90 du 31 janvier 2014 pris pour son application, qu'un maire qui estime se trouver dans une situation de conflit d'intérêts doit prendre un arrêté mentionnant la teneur des questions pour lesquelles il estime ne pas devoir exercer ses compétences et désigner, dans les conditions prévues par la loi, la personne chargée de le suppléer.,,,Il résulte des dispositions du code des communes de la Nouvelle-Calédonie, notamment de son article L. 122-12, rédigé dans des termes identiques à l'article L. 2122-26 du code général des collectivités territoriales (CGCT), que lorsque les intérêts du maire se trouvent en opposition avec ceux de la commune dans un litige donné ou pour la signature ou l'exécution d'un contrat, seul le conseil municipal est compétent pour désigner un autre de ses membres soit pour représenter la commune en justice soit pour signer le contrat ou intervenir dans son exécution.,,,1) a) Lorsque le maire estime ne pas devoir exercer ses compétences à raison d'un conflit d'intérêts, il ne saurait désigner la personne habilitée soit à représenter la commune en justice dans un litige donné soit à signer ou exécuter un contrat que si ses intérêts ne se trouvent pas en opposition avec ceux de la commune.... ,,b) Lorsqu'une telle opposition ressort des pièces du dossier qui lui est soumis, il appartient au juge de relever d'office l'irrecevabilité de la demande de la commune représentée par son maire ou par une personne qui n'a pas été légalement désignée.,,,2) Commet une erreur de droit une cour qui se borne à relever, pour juger irrecevable la demande d'une commune représentée par le premier adjoint au maire, qu'en dépit de l'arrêté par lequel le maire avait délégué à son premier adjoint ses compétences en matière d'urbanisme, seul le conseil municipal de la commune avait compétence pour désigner un autre de ses membres pour ester en justice en son nom, sans rechercher si les intérêts du maire se trouvaient, dans ce litige, en opposition avec ceux de la commune.</ANA>
<ANA ID="9C"> 54-02-01-02 Article L. 122-12 du code des communes de la Nouvelle-Calédonie, rédigé dans des termes identiques à l'article L. 2122-26 du code général des collectivités territoriales (CGCT), prévoyant que, dans le cas où les intérêts du maire se trouvent en opposition avec ceux de la commune, le conseil municipal désigne un autre de ses membres pour représenter la commune, soit en justice, soit dans les contrats.,,,Lorsqu'une telle opposition ressort des pièces du dossier qui lui est soumis, il appartient au juge de relever d'office l'irrecevabilité de la demande de la commune représentée par son maire ou par une personne qui n'a pas été légalement désignée.</ANA>
<ANA ID="9D"> 54-07-01-04-01 Article L. 122-12 du code des communes de la Nouvelle-Calédonie, rédigé dans des termes identiques à l'article L. 2122-26 du code général des collectivités territoriales (CGCT), prévoyant que, dans le cas où les intérêts du maire se trouvent en opposition avec ceux de la commune, le conseil municipal désigne un autre de ses membres pour représenter la commune, soit en justice, soit dans les contrats.,,,Lorsqu'une telle opposition ressort des pièces du dossier qui lui est soumis, il appartient au juge de relever d'office l'irrecevabilité de la demande de la commune représentée par son maire ou par une personne qui n'a pas été légalement désignée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
