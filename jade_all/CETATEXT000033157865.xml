<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157865</ID>
<ANCIEN_ID>JG_L_2016_09_000000398347</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157865.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 26/09/2016, 398347, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398347</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:398347.20160926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...E..., Mme C...B..., Mme F...H...et M. D... G... ont demandé au juge des référés du tribunal administratif de Toulouse, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 27 janvier 2016 par lequel le président du conseil départemental de l'Aveyron a fixé le forfait journalier du lieu de vie et d'accueil " Le Brox ". Par une ordonnance n° 1601087 du 14 mars 2016, le juge des référés du tribunal administratif de Toulouse a rejeté leur demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 mars, 14 avril et 6 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. E...et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge du département de l'Aveyron la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. E...et de Mme B...et à la SCP Waquet, Farge, Hazan, avocat du département de l'Aveyron ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative (...) fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision (...) lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 351-1 du code de l'action sociale et des familles : " Les recours dirigés contre les décisions prises par (...) le président du conseil départemental (...) déterminant (...) les prix de journée et autres tarifs des établissements et services sanitaires, sociaux et médico-sociaux de statut public ou privé et d'organismes concourant aux soins, sont portés, en premier ressort, devant le tribunal interrégional de la tarification sanitaire et sociale " ; <br/>
<br/>
              3. Considérant qu'il résulte de l'article L. 312-1 du même code que les lieux de vie et d'accueil qui ne constituent pas des établissements et services sociaux ou médicaux-sociaux au sens du I de cet article sont néanmoins, en vertu du III du même article, soumis à plusieurs des obligations imposées à ces établissements et services, y compris, lorsqu'ils ne relèvent pas des dispositions relatives aux assistants maternels ou aux particuliers accueillant des personnes âgées ou handicapées, à l'autorisation mentionnée à l'article L. 313-1 de ce code et au contrôle de l'autorité qui a délivré l'autorisation, ainsi qu'à des règles de financement et de tarification renvoyées par ces dispositions à un décret ; que l'article D. 316-5 du même code, pris en application de ces dispositions, prévoit que les frais de fonctionnement de chaque lieu de vie et d'accueil relevant du III de l'article L. 312-1 sont pris en charge par les organismes financeurs sous la forme d'un forfait journalier, arrêté par les autorités de tarification après l'envoi par la personne ayant qualité pour représenter le lieu de vie et d'accueil d'une proposition fondée sur un projet de budget ; <br/>
<br/>
              4. Considérant que le législateur a ainsi entendu que, même lorsqu'ils ne constituent pas des établissements et services sociaux ou médicaux-sociaux au sens du I de l'article L. 312-1 du code de l'action sociale et des familles, les lieux de vie et d'accueil soient soumis à plusieurs des dispositions applicables à ces établissements et services, en particulier à des règles de tarification ; que cette tarification prend la forme d'un forfait journalier, qui est l'une de celles que prévoit l'article R. 314-8 de ce code pour la tarification des établissements et services sociaux et médico-sociaux et qui est arrêtée par l'autorité de tarification au vu d'un budget prévisionnel et à un niveau destiné à permettre la prise en charge des dépenses nécessaires à l'accueil des personnes qui leur sont adressées ; qu'il résulte ainsi de l'ensemble des dispositions citées au point 3 que le recours contre une décision par laquelle une autorité de tarification mentionnée à l'article L. 351-1 du même code, au nombre desquelles le président du conseil départemental, détermine le forfait journalier d'un lieu de vie et d'accueil relève des litiges que cet article attribue à la compétence du tribunal interrégional de la tarification sanitaire et sociale ; que c'est donc sans erreur de droit que le juge des référés du tribunal administratif de Toulouse a jugé, pour rejeter la demande de suspension de l'arrêté par lequel le président du conseil départemental de l'Aveyron a fixé le forfait journalier du lieu de vie et d'accueil " Le Brox ", qu'il n'appartenait manifestement pas au tribunal administratif de connaître de la demande d'annulation de cet arrêté ; que M. E...et Mme B...ne sont, par suite, pas fondés à demander l'annulation de l'ordonnance qu'ils attaquent ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de l'Aveyron, qui n'est pas la partie perdante dans la présente instance ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. E...et Mme B...la somme que le département de l'Aveyron demande au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                              D E C I D E :<br/>
                              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. E...et Mme B...est rejeté.<br/>
Article 2 : Les conclusions du département de l'Aveyron présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...E..., à Mme C...B...et au département de l'Aveyron.<br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
