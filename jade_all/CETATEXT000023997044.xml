<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023997044</ID>
<ANCIEN_ID>JG_L_2011_05_000000347002</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/99/70/CETATEXT000023997044.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 11/05/2011, 347002, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347002</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:347002.20110511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement du 22 février 2011, enregistré le 24 février 2011 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Cergy-Pontoise, avant de statuer sur la demande de la SOCIETE REBILLON SCHMIT PREVOT, tendant à l'annulation du contrat conclu le 6 avril 2009 entre la commune d'Enghien-les-Bains et la société OGF et portant sur la reprise de sépultures au cimetière Nord d'Enghien-les-Bains, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si les demandes indemnitaires présentées par un concurrent évincé dans le cadre du recours en contestation de la validité du contrat doivent, à peine d'irrecevabilité, être présentées dans le même délai que ce recours, à savoir dans le délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées de l'avis d'attribution du contrat, sans que l'exercice ultérieur d'une demande indemnitaire préalable auprès de l'administration soit de nature à permettre la réouverture des délais de recours ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative, notamment ses articles L. 113-1 et R. 113-1 à R. 113-4 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Fabrice Aubert, Auditeur, <br/>
 - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
                             REND L'AVIS SUIVANT<br/>
<br/>
<br/>1. Ainsi que l'a jugé le Conseil d'Etat, statuant au contentieux, par une décision n° 291545 du 16 juillet 2007, tout concurrent évincé de la conclusion d'un contrat administratif est recevable à former devant le juge du contrat, dans un délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées, un recours de pleine juridiction contestant la validité de ce contrat ou de certaines de ses clauses qui en sont divisibles, afin d'en obtenir la résiliation ou l'annulation. <br/>
<br/>
              Il appartient au juge saisi de telles conclusions, lorsqu'il constate l'existence de vices entachant la validité du contrat, d'en apprécier les conséquences. Il lui revient, après avoir pris en considération la nature de l'illégalité éventuellement commise, soit de prononcer la résiliation du contrat ou de modifier certaines de ses clauses, soit de décider de la poursuite de son exécution, éventuellement sous réserve de mesures de régularisation par la collectivité contractante, soit d'accorder des indemnisations en réparation des droits lésés, soit enfin, après avoir vérifié si l'annulation du contrat ne porterait pas une atteinte excessive à l'intérêt général ou aux droits du cocontractant, d'annuler, totalement ou partiellement, le cas échéant avec un effet différé, le contrat. <br/>
<br/>
              2. En vue d'obtenir réparation de ses droits lésés, le concurrent évincé a ainsi la possibilité de présenter devant le juge du contrat des conclusions indemnitaires, à titre accessoire ou complémentaire à ses conclusions à fin de résiliation ou d'annulation du contrat. Il peut également engager un recours de pleine juridiction distinct, tendant exclusivement à une indemnisation du préjudice subi à raison de l'illégalité de la conclusion du contrat dont il a été évincé.<br/>
<br/>
              Dans les deux cas, la présentation de conclusions indemnitaires par le concurrent évincé n'est pas soumise au délai de deux mois suivant l'accomplissement des mesures de publicité du contrat, applicable aux seules conclusions tendant à sa résiliation ou à son annulation.<br/>
<br/>
              3. La recevabilité des conclusions indemnitaires, présentées à titre accessoire ou complémentaire aux conclusions contestant la validité du contrat, est en revanche soumise, selon les modalités du droit commun, à l'intervention d'une décision préalable de l'administration de nature à lier le contentieux, le cas échéant en cours d'instance, sauf en matière de travaux publics.<br/>
<br/>
              Elles doivent également, à peine d'irrecevabilité, être motivées et chiffrées. Il n'appartient en effet pas au juge du contrat, saisi d'un tel recours contestant la validité du contrat, d'accorder au concurrent évincé une indemnité alors même que celui-ci n'aurait pas formulé de conclusions en ce sens.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Cergy-Pontoise, à la SOCIETE REBILLON SCHMIT PREVOT et à la commune d'Enghien-les-Bains.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS D'UN CONCURRENT ÉVINCÉ CONTESTANT LA VALIDITÉ D'UN CONTRAT ADMINISTRATIF (RECOURS TROPIC) [RJ1] - 1) POSSIBILITÉ DE PRÉSENTER, PARALLÈLEMENT AUX CONCLUSIONS TENDANT À L'ANNULATION OU À LA RÉSILIATION DE CE CONTRAT, DES CONCLUSIONS INDEMNITAIRES - A) CONCLUSIONS PRÉSENTÉES À TITRE ACCESSOIRE OU COMPLÉMENTAIRE - EXISTENCE - B) CONCLUSIONS PRÉSENTÉES DANS UN RECOURS DISTINCT - EXISTENCE - 2) CONDITIONS DE RECEVABILITÉ DE TELLES CONCLUSIONS - A) DÉLAI DE DEUX MOIS SUIVANT LA PUBLICATION DU CONTRAT APPLICABLE AUX CONCLUSIONS D'ANNULATION OU DE RÉSILIATION DU CONTRAT - INAPPLICABILITÉ - B) NÉCESSITÉ D'UNE DÉCISION PRÉALABLE DE REFUS DE L'ADMINISTRATION - EXISTENCE - C) OBLIGATION DE CHIFFRAGE ET DE MOTIVATION - EXISTENCE - 3) OFFICE DU JUGE DU CONTRAT - IMPOSSIBILITÉ D'ACCORDER AU TIERS ÉVINCÉ UNE INDEMNITÉ EN L'ABSENCE DE CONCLUSIONS FORMÉES PAR LUI EN CE SENS.
</SCT>
<ANA ID="9A"> 39-08 Tout concurrent évincé de la conclusion d'un contrat administratif est recevable à former devant le juge du contrat, dans un délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées, un recours de pleine juridiction contestant la validité de ce contrat ou de certaines de ses clauses qui en sont divisibles, afin d'en obtenir la résiliation ou l'annulation.... ...1) a) En vue d'obtenir réparation de ses droits lésés, le concurrent évincé a la possibilité de présenter devant le juge du contrat des conclusions indemnitaires, à titre accessoire ou complémentaire à ses conclusions à fin de résiliation ou d'annulation du contrat. b) Il peut également engager un recours de pleine juridiction distinct, tendant exclusivement à une indemnisation du préjudice subi à raison de l'illégalité de la conclusion du contrat dont il a été évincé.,,2) a) Dans les deux cas, la présentation de conclusions indemnitaires par le concurrent évincé n'est pas soumise au délai de deux mois suivant l'accomplissement des mesures de publicité du contrat, applicable aux seules conclusions tendant à sa résiliation ou à son annulation.,,b) La recevabilité des conclusions indemnitaires, présentées à titre accessoire ou complémentaire aux conclusions contestant la validité du contrat, est en revanche soumise, selon les modalités du droit commun, à l'intervention d'une décision préalable de l'administration de nature à lier le contentieux, le cas échéant en cours d'instance, sauf en matière de travaux publics.,,c) Elles doivent également, à peine d'irrecevabilité, être motivées et chiffrées.,,3) Il n'appartient pas au juge du contrat, saisi d'un recours contestant la validité du contrat, d'accorder au concurrent évincé une indemnité alors même que celui-ci n'aurait pas formulé de conclusions en ce sens.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 16 juillet 2007, Société Tropic Travaux Signalisation, n° 291545, p. 360.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
