<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487228</ID>
<ANCIEN_ID>JG_L_2021_12_000000433232</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487228.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/12/2021, 433232, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433232</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:433232.20211213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 août et 4 novembre 2019 et le 25 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la Fédération CFDT des services demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre du travail du 29 mai 2019 portant extension d'un avenant a` la convention collective nationale des entreprises de services à la personne (n° 3127), en tant qu'il exclut de l'extension le dernier alinéa de l'article 1er de cet avenant ;<br/>
<br/>
              2°) à titre subsidiaire, d'annuler l'arrêté dans son ensemble ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 2017-1340 du 15 septembre 2017 ;  <br/>
              - la loi n° 2018-217 du 29 mars 2018 ; <br/>
              - l'ordonnance n° 2017-1385 du 22 septembre 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Thalia Breton, auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la Fédération CFDT des services ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que, le 1er mai 2018, quatre organisations syndicales, la Fédération CFDT des services, la Confédération française des travailleurs chrétiens Santé sociaux, la Confédération générale du travail du commerce et des services et la Fédération générale des travailleurs de l'agriculture, de l'alimentation, des tabacs et des services annexes Force ouvrière, et quatre organisations professionnelles d'employeurs, la Fédération française des services à la personne et de proximité, la Fédération française des entreprises de crèches, la Fédération du service aux particuliers et le Syndicat national des établissements et résidences privés, ont conclu un avenant à la convention collective nationale des entreprises de services à la personne (n° 3127). Par un arrêté du 29 mai 2019, la ministre du travail a procédé à l'extension de cet avenant en excluant du champ de l'extension les stipulations du dernier alinéa de son article 1er La Fédération CFDT des services demande l'annulation pour excès de pouvoir de cet arrêté en tant qu'il procède à cette exclusion ou à défaut, dans son intégralité.<br/>
<br/>
              Sur l'intervention de la Confédération française démocratique du travail :<br/>
<br/>
              2. L'intervention de la Confédération française démocratique du travail, qui justifie d'un intérêt suffisant à l'annulation de l'arrêté attaqué, est recevable.<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              En ce qui concerne le droit applicable avant l'entrée en vigueur de l'ordonnance du 22 septembre 2017 :<br/>
<br/>
              3. D'une part, aux termes de l'article L. 2232-5-1 du code du travail, dans sa rédaction antérieure à l'entrée en vigueur de l'ordonnance du 22 septembre 2017 relative au renforcement de la négociation collective : " La branche a pour missions : / 1° De définir, par la négociation, les garanties applicables aux salariés employés par les entreprises relevant de son champ d'application, notamment en matière de salaires minima, de classifications, de garanties collectives complémentaires mentionnées à l'article L. 912-1 du code de la sécurité sociale, de mutualisation des fonds de la formation professionnelle, de prévention de la pénibilité prévue au titre VI du livre Ier de la quatrième partie du présent code et d'égalité professionnelle entre les femmes et les hommes mentionnée à l'article L. 2241-3 ; / 2° De définir, par la négociation, les thèmes sur lesquels les conventions et accords d'entreprise ne peuvent être moins favorables que les conventions et accords conclus au niveau de la branche, à l'exclusion des thèmes pour lesquels la loi prévoit la primauté de la convention ou de l'accord d'entreprise ; / 3° De réguler la concurrence entre les entreprises relevant de son champ d'application ".<br/>
<br/>
              4. D'autre part, aux termes de l'article L. 2253-1 du code du travail, dans sa version applicable avant l'entrée en vigueur de cette ordonnance : " Une convention ou un accord d'entreprise ou d'établissement peut adapter les stipulations des conventions de branche ou des accords professionnels ou interprofessionnels applicables dans l'entreprise aux conditions particulières de celle-ci ou des établissements considérés. / Une convention ou un accord peut également comporter des stipulations nouvelles et des stipulations plus favorables aux salariés ". Aux termes du premier alinéa de l'article L. 2253-3 du code du travail, dans sa rédaction alors applicable : " En matière de salaires minima, de classifications, de garanties collectives complémentaires mentionnées à l'article L. 912-1 du code de la sécurité sociale et de mutualisation des fonds de la formation professionnelle, une convention ou un accord d'entreprise ou d'établissement ne peut comporter des clauses dérogeant à celles des conventions de branche ou accords professionnels ou interprofessionnels ". <br/>
<br/>
              5. Il résulte des dispositions précitées qu'avant l'entrée en vigueur de l'ordonnance du 22 septembre 2017, il revenait à la branche, par voie d'accord collectif s'imposant à tout accord d'entreprise, de fixer un salaire minimum conventionnel pour chaque niveau hiérarchique de la grille de classification des emplois prévue par la convention collective, auquel la rémunération effectivement perçue par les salariés de la branche ne pouvait être inférieure. A cet égard, les conventions de branche pouvaient déterminer, d'une part, le montant de ce salaire minimum conventionnel, et, d'autre part, les éléments de rémunération à prendre en compte pour s'assurer que la rémunération effective des salariés atteigne au moins le niveau du salaire minimum conventionnel correspondant à leur niveau hiérarchique. A défaut de stipulations conventionnelles expresses sur les éléments de rémunération des salariés à prendre en compte pour procéder à cette comparaison, il convenait de retenir, en vertu d'une jurisprudence constante de la Cour de cassation, le salaire de base et les compléments de salaire constituant une contrepartie directe à l'exécution de la prestation de travail par les salariés. <br/>
<br/>
              En ce qui concerne le droit applicable depuis l'entrée en vigueur de l'ordonnance du 22 septembre 2017 :<br/>
<br/>
              6. D'une part, aux termes de l'article L. 2232-5-1 du code du travail dans sa rédaction issue de l'ordonnance du 22 septembre 2017 et applicable au litige : " La branche a pour missions : / 1° De définir les conditions d'emploi et de travail des salariés ainsi que les garanties qui leur sont applicables dans les matières mentionnées aux articles L. 2253-1 et L. 2253-2 dans les conditions prévues par lesdits articles. / 2° De réguler la concurrence entre les entreprises relevant de son champ d'application ". <br/>
<br/>
              7. D'autre part, aux termes de l'article L. 2253-1 du code du travail, dans sa rédaction, applicable à l'espèce, issue de la même ordonnance : " La convention de branche définit les conditions d'emploi et de travail des salariés. Elle peut en particulier définir les garanties qui leur sont applicables dans les matières suivantes : / 1° Les salaires minima hiérarchiques ; (...) / Dans les matières énumérées au 1° à 13°, les stipulations de la convention de branche ou de l'accord couvrant un champ territorial ou professionnel plus large prévalent sur la convention d'entreprise conclue antérieurement ou postérieurement à la date de leur entrée en vigueur, sauf lorsque la convention d'entreprise assure des garanties au moins équivalentes. Cette équivalence des garanties s'apprécie par ensemble de garanties se rapportant à la même matière. " Aux termes de l'article L. 2253-2  de ce code, dans sa rédaction applicable, issue de l'ordonnance précitée : " Dans les matières suivantes, lorsque la convention de branche ou l'accord couvrant un champ territorial ou professionnel plus large le stipule expressément, la convention d'entreprise conclue postérieurement à cette convention ou à cet accord ne peut comporter des stipulations différentes de celles qui lui sont applicables en vertu de cette convention ou de cet accord sauf lorsque la convention d'entreprise assure des garanties au moins équivalentes : (...) / 4° Les primes pour travaux dangereux ou insalubres. / L'équivalence des garanties mentionnée au premier alinéa du présent article s'apprécie par ensemble de garanties se rapportant à la même matière ". Aux termes de l'article L. 2253-3 du même code, dans sa rédaction applicable au litige, issue de cette ordonnance: " Dans les matières autres que celles mentionnées aux articles L. 2253-1 et L. 2253-2, les stipulations de la convention d'entreprise conclue antérieurement ou postérieurement à la date d'entrée en vigueur de la convention de branche ou de l'accord couvrant un champ territorial ou professionnel plus large prévalent sur celles ayant le même objet prévues par la convention de branche ou l'accord couvrant un champ territorial ou professionnel plus large. En l'absence d'accord d'entreprise, la convention de branche ou l'accord couvrant un champ territorial ou professionnel plus large s'applique ".<br/>
<br/>
              8. Il résulte des dispositions précitées, issues de l'ordonnance du 22 septembre 2017 relative au renforcement de la négociation collective qui a notamment modifié l'articulation entre les conventions de branche et les accords d'entreprise, que la convention de branche peut définir les garanties applicables en matière de salaires minima hiérarchiques, auxquelles un accord d'entreprise ne peut déroger que s'il prévoit des garanties au moins équivalentes. En outre, si la convention de branche peut, y compris indépendamment de la définition des garanties applicables en matière de salaires minima hiérarchiques, prévoir l'existence de primes, ainsi que leur montant, les stipulations d'un accord d'entreprise en cette matière prévalent sur celles de la convention de branche, qu'elles soient ou non plus favorables, sauf, le cas échéant, en ce qui concerne les primes pour travaux dangereux ou insalubres pour lesquelles la convention de branche, lorsqu'elle le stipule expressément, s'impose aux accords d'entreprise qui ne peuvent que prévoir des garanties au moins équivalentes. Par suite, faute pour les dispositions citées au point 7 de définir la notion de salaires minima hiérarchiques, laquelle n'est pas davantage éclairée par les travaux préparatoires de l'ordonnance du 22 septembre 2017, il est loisible à la convention de branche, d'une part, de définir les salaires minima hiérarchiques et, le cas échéant à ce titre de prévoir qu'ils valent soit pour les seuls salaires de base des salariés, soit pour leurs rémunérations effectives résultant de leur salaires de base et de certains compléments de salaire, d'autre part, d'en fixer le montant par niveau hiérarchique. Lorsque la convention de branche stipule que les salaires minima hiérarchiques s'appliquent aux rémunérations effectives des salariés résultant de leurs salaires de base et de compléments de salaire qu'elle identifie, elle ne fait pas obstacle à ce que le montant de ces minima soit atteint dans une entreprise par des modalités de rémunération différentes de celles qu'elle mentionne, un accord d'entreprise pouvant réduire ou supprimer les compléments de salaire qu'elle mentionne au titre de ces minima, dès lors toutefois que sont prévus d'autres éléments de rémunération permettant aux salariés de l'entreprise de percevoir une rémunération effective au moins égale au montant des salaires minima hiérarchiques fixé par la convention. <br/>
<br/>
<br/>
Sur le litige :<br/>
<br/>
              9. En premier lieu, aux termes du premier alinéa de l'article L. 2261-25 du code du travail : " Le ministre chargé du travail peut exclure de l'extension, après avis motivé de la Commission nationale de la négociation collective, les clauses qui seraient en contradiction avec des dispositions légales. (...) ".<br/>
<br/>
              10. Il ressort des pièces du dossier, d'une part, que la sous-commission des conventions et accords de la Commission nationale de la négociation collective s'est prononcée au vu d'un rapport de la ministre chargée du travail mentionnant l'exclusion dont elle entendait assortir l'extension et les motifs pour lesquels elle envisageait de procéder à cette exclusion et, d'autre part, que son avis, rendu lors de la séance du 21 mai 2019, comporte l'exposé de considérations relatives à l'exclusion envisagée. Par suite, la Fédération CFDT des services n'est pas fondée à soutenir que l'arrêté contesté a été adopté en méconnaissance de l'article L. 2261-5 du code du travail. <br/>
<br/>
              11. En second lieu, il ressort des pièces du dossier qu'en application des dispositions citées au point 9, par un arrêté du 29 mai 2019, la ministre du travail a procédé à l'extension de l'avenant du 1er mars 2018 dit de révision de la convention collective des entreprises de services à la personne (IDCC 3127), qui prévoit l'existence d'une prime d'ancienneté et en fixe le montant, en excluant de l'extension la stipulation, figurant au dernier alinéa de son article 1er, prévoyant qu'un accord d'entreprise ne peut déroger de façon moins favorable à cette prime d'ancienneté. Il résulte de ce qui a été dit au point 8 que, si la convention de branche peut retenir que les salaires minima hiérarchiques s'appliquent aux rémunérations effectives des salariés résultant de leurs salaires de base et de certains compléments de salaire, elle ne peut, lorsqu'elle prévoit l'existence de primes, ainsi que leur montant, indépendamment, comme au cas d'espèce, de la définition des garanties applicables en matière de salaires minima hiérarchiques, faire obstacle à ce que les stipulations d'un accord d'entreprise en cette matière prévalent sur celles de la convention de branche, y compris si elles y sont moins favorables. Par suite, la Fédération CFDT des services n'est pas fondée à soutenir qu'en excluant du champ de l'extension le dernier alinéa de l'article 1er de l'avenant du 1er mars 2018, la ministre a entaché son arrêté attaqué d'erreur de droit.<br/>
<br/>
              12. Il résulte de tout ce qui précède que la Fédération CFDT des services n'est pas fondée à demander l'annulation pour excès de pouvoir de l'arrêté de la ministre du 29 mai 2019 portant extension d'un avenant a` la convention collective nationale des entreprises de services à la personne (n° 3127), à titre principal, en tant qu'il exclut de l'extension le dernier alinéa de l'article 1er de cet avenant, ou, à titre subsidiaire, dans son intégralité. <br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Confédération française démocratique du travail est admise.<br/>
Article 2 : La requête de la Fédération CFDT des services est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la Fédération CFDT des services, à la Confédération française des travailleurs chrétiens Santé sociaux, à la Confédération générale du travail du commerce et des services, à la Fédération générale des travailleurs de l'agriculture, de l'alimentation, des tabacs et des services annexes Force ouvrière, à la Fédération française des services à la personne et de proximité, à la Fédération française des entreprises de crèches, à la Fédération du service aux particuliers et au Syndicat national des établissements et résidences privés, à la ministre du travail, de l'emploi et de l'insertion et à la Confédération française démocratique du travail.<br/>
Copie en sera adressée à Fédération nationale de l'encadrement du commerce et des services CFE-CGC.<br/>
<br/>
Délibéré à l'issue de la séance du 8 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Thalia Breton, auditrice-rapporteure. <br/>
<br/>
              Rendu le 13 décembre 2021.<br/>
<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Thalia Breton<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme Romy Raquil<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
