<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815760</ID>
<ANCIEN_ID>JG_L_2019_07_000000408624</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815760.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 24/07/2019, 408624, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408624</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:408624.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              MmeB..., agissant en son nom propre et en qualité de représentante légale de son fils M. C...B..., a demandé au tribunal administratif de Lille de condamner le centre hospitalier régional universitaire (CHRU) de Lille à les indemniser des préjudices ayant résulté pour eux des conditions de son accouchement dans cet établissement le 11 janvier 1988. Par un jugement n° 1106644 du 2 juillet 2014, le tribunal administratif a partiellement fait droit à sa demande en condamnant le CHRU de Lille et son assureur, la société hospitalière d'assurance mutuelle (SHAM), d'une part, à verser à Mme B...en qualité de tutrice légale de son fils une somme de 1 038 517,93 euros sous déduction du montant de l'allocation aux adultes handicapés perçue de la majorité de celui-ci à la date de lecture du jugement, d'autre part, à lui verser une rente trimestrielle calculée au prorata du nombre de nuits passées à domicile par M. C...B...au titre de ses frais de médicaments, de fournitures d'hygiène et d'assurance par une tierce personne sur la base de 313,07 euros par nuit et une rente trimestrielle au titre de ses frais de matériel liés au handicap et de ses frais de déplacement sur la base de 1693,17 euros par trimestre, sous déduction des sommes perçues dans le cadre des plans de compensation du handicap et des sommes versées au titre de l'allocation aux adultes handicapés à compter de la date de lecture du jugement, enfin, au titre du préjudice propre de MmeB..., une somme de 50 000 euros.<br/>
<br/>
              Par un arrêt n° 14DA01526, 14DA01530 du 30 décembre 2016, la cour administrative d'appel de Douai, ayant joint la requête du CHRU de Lille et de la SHAM et la requête de Mme B..., a réformé ce jugement pour condamner le CHRU de Lille et la SHAM à verser à Mme B...en qualité de tutrice légale de son fils une indemnité de 359 847,42 euros, une rente annuelle d'un montant de 18 180 euros, une rente trimestrielle calculée au prorata du nombre de nuits passées à domicile par M. C...B...sur la base de 269 euros par nuit et à lui rembourser sur justificatifs ses frais futurs de trajet, d'adaptation du véhicule et d'adaptation du logement, sous déduction des sommes versées en réparation des mêmes préjudices au titre de la prestation de compensation du handicap.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 mars 2017, 6 juin 2017 et 24 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du CHRU de Lille et de la SHAM la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ; <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de Mme B...et à Me Le Prado, avocat du CHRU de Lille et de la SHAM.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C...B..., né le 11 janvier 1988 au centre hospitalier régional universitaire (CHRU) de Lille, a conservé de graves séquelles des conditions de sa naissance. La responsabilité de l'établissement ayant été recherchée, la cour administrative d'appel de Nantes, par un arrêt du 27 mai 1999 devenu définitif, a mis à sa charge le versement à l'enfant, jusqu'à l'âge de dix-huit ans, d'une rente annuelle de 270 000 F, en précisant que les débours de la caisse primaire d'assurance maladie des Flandres s'imputeraient sur ce montant dans la limite des trois quarts, et le versement à ses parents et à ses frères et soeurs d'indemnités réparant leurs préjudices propres. A la majorité de M. C...B..., le tribunal administratif de Lille a été saisi par la caisse primaire d'une demande de remboursement des frais lui incombant au titre de la période postérieure à son dix-huitième anniversaire. Mme B...est intervenue à l'instance, en qualité de représentante légale de son fils et en son nom propre, pour demander, d'une part, l'indemnisation des préjudices subis au cours de la même période, d'autre part, une indemnisation complémentaire au titre des préjudices antérieurs au dix-huitième anniversaire de son fils. Par un jugement du 2 juillet 2014, le tribunal administratif a partiellement fait droit aux conclusions dont il était saisi. Par un arrêt du 30 décembre 2016, la cour administrative d'appel a réformé ce jugement. Mme B...se pourvoit en cassation contre cet arrêt en tant qu'il refuse toute indemnisation complémentaire au titre des préjudices subis antérieurs au dix-huitième anniversaire de son fils et n'accueille que partiellement les conclusions relatives aux préjudices subis au cours de la période ultérieure. <br/>
<br/>
              Sur l'arrêt en tant qu'il se prononce sur les préjudices de la victime antérieurs à sa majorité : <br/>
<br/>
              2. Par son arrêt du 27 mai 1999, la cour administrative d'appel de Nancy a accordé à M. C... B..., alors mineur, une rente annuelle dont elle n'a pas détaillé le montant poste de préjudice par poste de préjudice et dont elle a précisé qu'elle indemniserait jusqu'à sa majorité tous les préjudices résultant pour lui de son invalidité, sans qu'il y ait lieu, au jour de sa décision, d'ordonner une expertise en vue d'en évaluer le montant définitif. Il ressort des termes de cet arrêt que, par l'octroi de la rente, la cour a entendu assurer une indemnisation provisionnelle des préjudices subis par la victime jusqu'à son dix-huitième anniversaire, l'évaluation définitive de ces préjudices, de même que la détermination de ceux qu'elle continuerait de subir à l'âge adulte, devant être effectuée à sa majorité après expertise. Par suite, en jugeant que l'autorité de la chose jugée s'attachant à l'arrêt du 27 mai 1999 faisait obstacle à l'indemnisation complémentaire sollicitée par Mme B...au titre des préjudices subis par son fils pendant sa minorité, la cour administrative d'appel de Douai a commis une erreur de droit qui justifie l'annulation de son arrêt en tant qu'il statue sur cette demande. <br/>
<br/>
              Sur l'arrêt en tant qu'il se prononce sur les préjudices de la victime postérieurs à sa majorité : <br/>
<br/>
              En ce qui concerne les frais de médicaments : <br/>
<br/>
              3. Pour refuser d'indemniser les frais de médicaments restant à la charge de la victime pour la période postérieure à son arrêt, la cour administrative d'appel a souverainement retenu qu'il ne résultait pas de l'instruction que M. C...B...doive nécessairement continuer de recevoir, à l'avenir, les traitements non pris en charge par l'assurance maladie dont elle condamnait l'hôpital à rembourser le montant pour la période comprise entre la majorité de la victime et la date de son arrêt. En se prononçant par ces motifs, suffisants et exempts de dénaturation, qui ne font en tout état de cause pas obstacle à ce que soit demandée ultérieurement une indemnisation au titre de frais de médicaments effectivement exposés, elle n'a pas commis d'erreur de droit.  <br/>
<br/>
              En ce qui concerne les préjudices scolaire et professionnel : <br/>
<br/>
              4. Lorsque la victime se trouve, du fait d'un accident corporel survenu dans son jeune âge, privée de toute possibilité d'exercer un jour une activité professionnelle, la seule circonstance qu'il soit impossible de déterminer le parcours professionnel qu'elle aurait suivi ne fait pas obstacle à ce que soit réparé le préjudice, qui doit être regardé comme présentant un caractère certain, résultant pour elle de la perte des revenus qu'une activité professionnelle lui aurait procurés et de la pension de retraite consécutive. Il y a lieu de réparer ce préjudice par l'octroi à la victime, à compter de sa majorité et sa vie durant, d'une rente fixée sur la base du salaire médian net mensuel de l'année de sa majorité et revalorisée par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale. Doivent être déduites de cette rente les sommes éventuellement perçues par la victime au titre de l'allocation aux adultes handicapés.<br/>
<br/>
              5. Lorsque la victime se trouve également privée de toute possibilité d'accéder à une scolarité, la seule circonstance qu'il soit impossible de déterminer le parcours scolaire qu'elle aurait suivi ne fait pas davantage obstacle à ce que soit réparé le préjudice ayant résulté pour elle de l'impossibilité de bénéficier de l'apport d'une scolarisation. La part patrimoniale de ce préjudice, tenant à l'incidence de l'absence de scolarisation sur les revenus professionnels, est réparée par l'allocation de la rente décrite au point 4. La part personnelle de ce préjudice ouvre à la victime le droit à une réparation que les juges du fond peuvent, sans commettre d'erreur de droit, assurer par l'octroi d'une indemnité globale couvrant également d'autres chefs de préjudice personnels au titre des troubles dans les conditions d'existence.<br/>
<br/>
              6. Mme B...soutenait devant les juges du fond que son fils avait été privé de la possibilité d'accéder à une scolarité et ne pourrait jamais exercer la moindre activité professionnelle ni bénéficier d'une retraite et demandait qu'il soit indemnisé à ce titre. En incluant la réparation de la part personnelle du préjudice scolaire dans l'indemnité qu'elle a accordée à M. C...B...au titre des troubles subis dans ses conditions d'existence, la cour n'a pas commis d'erreur de droit. En revanche, en jugeant que l'intéressé ne pouvait pas se prévaloir d'un préjudice, distinct des troubles dans ses conditions d'existence, résultant de la perte des revenus qu'une activité professionnelle lui aurait procurés, ainsi que de la perte consécutive de droits à pension, préjudice  incluant la part patrimoniale de son préjudice scolaire, la cour administrative d'appel a méconnu les principes énoncés ci-dessus. Cette erreur de droit justifie l'annulation de son arrêt en tant qu'il refuse l'indemnisation de ce préjudice. <br/>
<br/>
              7. Il résulte de tout ce qui précède qu'il y a lieu d'annuler l'arrêt attaqué en tant qu'il statue sur l'indemnisation des préjudices subis par la victime avant sa majorité ainsi que sur l'indemnisation de  son préjudice résultant de la perte des revenus qu'une activité professionnelle lui aurait procurés et la perte consécutive de droits à pension.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond en ce qui concerne le préjudice résultant de la perte de revenus professionnels et de la perte consécutive de droits à pension, incluant la part patrimoniale du préjudice scolaire de M. C...B.... En revanche, en ce qui concerne les préjudices subis par lui avant sa majorité, il y a lieu de renvoyer l'affaire devant les juges du fond.<br/>
<br/>
              9. Il résulte de l'instruction que le handicap de M. C...B...l'a placé dans l'incapacité totale et définitive d'être scolarisé et d'exercer un jour une activité professionnelle. Il est, par suite, fondé à se prévaloir à ce titre de la perte de revenus professionnels et de la perte consécutive de ses droits à pension, préjudice incluant la part patrimoniale de son préjudice scolaire. <br/>
<br/>
              Pour la période antérieure à la présente décision : <br/>
<br/>
              10. Il résulte de l'instruction que le salaire mensuel médian net s'établissait en 2006, année de la majorité de M. C...B..., à 1 555 euros. Il y a lieu, par suite, de lui allouer au titre de la perte de revenus professionnels et de la perte consécutive de droits à pension, préjudice incluant la part patrimoniale du préjudice scolaire qu'il a subi, pour la période écoulée depuis sa majorité, une somme égale à 162 fois ce montant, revalorisé chaque année par application des coefficients annuels prévus à l'article L. 434-17 du code de la sécurité sociale. Il y a lieu de renvoyer Mme B...devant le CHRU de Lille pour qu'il soit procédé à la liquidation de cette indemnité, en déduction de laquelle viendront les sommes éventuellement perçues par la victime au titre de l'allocation aux adultes handicapés. <br/>
<br/>
              Pour la période future : <br/>
<br/>
              11. Ainsi qu'il a été dit au point 4, il y a lieu d'allouer à M. C...B...pour l'avenir, en réparation de sa perte de revenus professionnels et de la perte consécutive de droits à pension, préjudice incluant la part patrimoniale de son préjudice scolaire, une rente dont le montant sera calculé sur la base du salaire médian net  de 2006, soit 4 665 euros par trimestre, actualisé pour l'année 2019 en fonction des coefficients annuels de revalorisation fixés en application de l'article L. 434-17 du code de la sécurité sociale depuis l'année 2006 et revalorisé annuellement à l'avenir par application des coefficients qui seront légalement fixés. Les sommes perçues par M. B...au titre de l'allocation aux adultes handicapés viendront, le cas échéant, en déduction de cette rente.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge conjointe du CHRU de Lille et de la SHAM la somme de 4 000 euros à verser à Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 30 décembre 2016 de la cour administrative d'appel de Douai est annulé en tant qu'il statue sur l'indemnisation des préjudices de M. C...B...antérieurs à sa majorité, de la perte de revenus professionnels et de la perte consécutive de droits à pension.<br/>
 Article 2 : Le CHRU de Lille versera à M. C...B..., en réparation de son préjudice tiré de la perte de revenus professionnel et de la perte consécutive de droits à pension les indemnités et la rente calculées comme indiqué aux points 10 et 11 de la présente décision.<br/>
Article 3 : Le jugement du 2 juillet 2014 du tribunal administratif de Lille est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : L'affaire est renvoyée à la cour administrative d'appel de Douai en ce qui concerne les préjudices subis par M. C...B...avant son dix-huitième anniversaire.<br/>
Article 5 : Le CHRU de Lille et la SHAM verseront conjointement à Mme B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Le surplus des conclusions du pourvoi de Mme B...est rejeté. <br/>
Article 7 : La présente décision sera notifiée à Mme A...B..., au centre hospitalier régional universitaire de Lille, à la société hospitalière d'assurance mutuelle, à la caisse primaire d'assurance maladie des Flandres et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. CARACTÈRE CERTAIN DU PRÉJUDICE. EXISTENCE. - VICTIME D'UN ACCIDENT SURVENU DANS SON JEUNE ÂGE - 1) PERTE DE REVENUS RÉSULTANT DE L'IMPOSSIBILITÉ D'EXERCER UN JOUR UNE ACTIVITÉ PROFESSIONNELLE [RJ1] - 2) PRÉJUDICE RÉSULTANT DE L'IMPOSSIBILITÉ D'ACCÉDER À TOUTE SCOLARITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03-02-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MATÉRIEL. PERTE DE REVENUS. PERTE DE REVENUS SUBIE PAR LA VICTIME D'UN ACCIDENT. - PERTE DE REVENUS QU'UNE ACTIVITÉ PROFESSIONNELLE AURAIT PROCURÉS À LA VICTIME D'UN ACCIDENT SURVENU DANS SON JEUNE ÂGE - 1) PRINCIPE - CARACTÈRE INDEMNISABLE - EXISTENCE [RJ1] - 2) MODALITÉS D'INDEMNISATION - RENTE FIXÉE SUR LA BASE DU SALAIRE MÉDIAN NET MENSUEL, RÉDUITE DES SOMMES ÉVENTUELLEMENT PERÇUES AU TITRE DE L'AAH [RJ2] - 3) RENTE RÉPARANT ÉGALEMENT LA PART PATRIMONIALE DU PRÉJUDICE RÉSULTANT DE L'IMPOSSIBILITÉ D'ACCÉDER À TOUTE SCOLARITÉ - EXISTENCE, LA PART PERSONNELLE ÉTANT RÉPARÉE PAR AILLEURS AU TITRE DES TROUBLES DANS LES CONDITIONS D'EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03-03-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. TROUBLES DANS LES CONDITIONS D'EXISTENCE. TROUBLES DANS LES CONDITIONS D'EXISTENCE SUBIS PAR LA VICTIME D'UN ACCIDENT. - IMPOSSIBILITÉ DE BÉNÉFICIER DE L'APPORT D'UNE SCOLARISATION EN RAISON D'UN ACCIDENT SURVENU À UN JEUNE ÂGE - INDEMNISATION DE LA PART PERSONNELLE DE CE PRÉJUDICE POUVANT ÊTRE ASSURÉE PAR L'OCTROI D'UNE INDEMNITÉ GLOBALE COUVRANT D'AUTRES CHEFS DE PRÉJUDICE PERSONNEL, LA PART PATRIMONIALE ÉTANT RÉPARÉE PAR AILLEURS PAR LA RENTE INDEMNISANT LA PERTE DE REVENUS FUTURS.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-04-03-07 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. MODALITÉS DE FIXATION DES INDEMNITÉS. - VICTIME D'UN ACCIDENT SURVENU DANS SON JEUNE ÂGE - 1) RÉPARATION DE LA PERTE DE REVENUS QU'UNE ACTIVITÉ PROFESSIONNELLE LUI AURAIT PROCURÉS - RENTE FIXÉE SUR LA BASE DU SALAIRE MÉDIAN NET MENSUEL, RÉDUITE DES SOMMES ÉVENTUELLEMENT PERÇUES AU TITRE DE L'AAH [RJ2] - 2) RÉPARATION DU PRÉJUDICE RÉSULTANT DE L'IMPOSSIBILITÉ D'ACCÉDER À TOUTE SCOLARITÉ - A) PART PATRIMONIALE RÉPARÉE PAR LA RENTE PRÉCITÉE - B) PART PERSONNELLE POUVANT ÊTRE RÉPARÉE PAR L'OCTROI D'UNE INDEMNITÉ GLOBALE COUVRANT D'AUTRES CHEFS DE PRÉJUDICE PERSONNEL.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">60-04-04-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. FORMES DE L'INDEMNITÉ. RENTE. - RENTE ALLOUÉE EN RÉPARATION DE LA PERTE DE REVENUS QU'UNE ACTIVITÉ PROFESSIONNELLE AURAIT PROCURÉS À LA VICTIME D'UN ACCIDENT SURVENU DANS SON JEUNE ÂGE - FIXATION SUR LA BASE DU SALAIRE MÉDIAN NET MENSUEL, DÉDUCTION FAITE DES SOMMES ÉVENTUELLEMENT PERÇUES AU TITRE DE L'AAH [RJ2].
</SCT>
<ANA ID="9A"> 60-04-01-02-02 1) Lorsque la victime se trouve, du fait d'un accident corporel survenu dans son jeune âge, privée de toute possibilité d'exercer un jour une activité professionnelle, la seule circonstance qu'il soit impossible de déterminer le parcours professionnel qu'elle aurait suivi ne fait pas obstacle à ce que soit réparé le préjudice, qui doit être regardé comme présentant un caractère certain, résultant pour elle de la perte des revenus qu'une activité professionnelle lui aurait procurés ainsi que de la pension de retraite consécutive.,,,2) Lorsque la victime se trouve également privée de toute possibilité d'accéder à une scolarité, la seule circonstance qu'il soit impossible de déterminer le parcours scolaire qu'elle aurait suivi ne fait pas davantage obstacle à ce que soit réparé le préjudice ayant résulté pour elle de l'impossibilité de bénéficier de l'apport d'une scolarisation.</ANA>
<ANA ID="9B"> 60-04-03-02-01-01 1) Lorsque la victime se trouve, du fait d'un accident corporel survenu dans son jeune âge, privée de toute possibilité d'exercer un jour une activité professionnelle, la seule circonstance qu'il soit impossible de déterminer le parcours professionnel qu'elle aurait suivi ne fait pas obstacle à ce que soit réparé le préjudice, qui doit être regardé comme présentant un caractère certain, résultant pour elle de la perte des revenus qu'une activité professionnelle lui aurait procurés ainsi que de la pension de retraite consécutive.... ,,2) Il y a lieu de réparer ce préjudice par l'octroi à la victime, à compter de sa majorité et sa vie durant, d'une rente fixée sur la base du salaire médian net mensuel de l'année de sa majorité et revalorisée par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale (CSS). Doivent être déduites de cette rente les sommes éventuellement perçues par la victime au titre de l'allocation aux adultes handicapés (AAH).,,,3) Lorsque la victime se trouve également privée de toute possibilité d'accéder à une scolarité, la seule circonstance qu'il soit impossible de déterminer le parcours scolaire qu'elle aurait suivi ne fait pas davantage obstacle à ce que soit réparé le préjudice ayant résulté pour elle de l'impossibilité de bénéficier de l'apport d'une scolarisation. La part patrimoniale de ce préjudice, tenant à l'incidence de l'absence de scolarisation sur les revenus professionnels, est réparée par l'allocation de la rente décrite ci-dessus. La part personnelle de ce préjudice ouvre à la victime le droit à une réparation que les juges du fond peuvent, sans commettre d'erreur de droit, assurer par l'octroi d'une indemnité globale couvrant également d'autres chefs de préjudice personnels au titre des troubles dans les conditions d'existence.</ANA>
<ANA ID="9C"> 60-04-03-03-01 Lorsque la victime se trouve, du fait d'un accident corporel survenu dans son jeune âge, privée de toute possibilité d'accéder à une scolarité, la seule circonstance qu'il soit impossible de déterminer le parcours scolaire qu'elle aurait suivi ne fait pas davantage obstacle à ce que soit réparé le préjudice ayant résulté pour elle de l'impossibilité de bénéficier de l'apport d'une scolarisation.... ,,La part patrimoniale de ce préjudice, tenant à l'incidence de l'absence de scolarisation sur les revenus professionnels, est réparée par l'allocation de la rente octroyée pour l'indemnisation de la perte de revenus futurs.... ,,La part personnelle de ce préjudice ouvre à la victime le droit à une réparation que les juges du fond peuvent, sans commettre d'erreur de droit, assurer par l'octroi d'une indemnité globale couvrant également d'autres chefs de préjudice personnels au titre des troubles dans les conditions d'existence.</ANA>
<ANA ID="9D"> 60-04-03-07 1) Lorsque la victime se trouve, du fait d'un accident corporel survenu dans son jeune âge, privée de toute possibilité d'exercer un jour une activité professionnelle, la seule circonstance qu'il soit impossible de déterminer le parcours professionnel qu'elle aurait suivi ne fait pas obstacle à ce que soit réparé le préjudice, qui doit être regardé comme présentant un caractère certain, résultant pour elle de la perte des revenus qu'une activité professionnelle lui aurait procurés ainsi que de la pension de retraite consécutive.... ,,Il y a lieu de réparer ce préjudice par l'octroi à la victime, à compter de sa majorité et sa vie durant, d'une rente fixée sur la base du salaire médian net mensuel de l'année de sa majorité et revalorisée par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale (CSS). Doivent être déduites de cette rente les sommes éventuellement perçues par la victime au titre de l'allocation aux adultes handicapés (AAH).,,,2) Lorsque la victime se trouve également privée de toute possibilité d'accéder à une scolarité, la seule circonstance qu'il soit impossible de déterminer le parcours scolaire qu'elle aurait suivi ne fait pas davantage obstacle à ce que soit réparé le préjudice ayant résulté pour elle de l'impossibilité de bénéficier de l'apport d'une scolarisation.... ,,a) La part patrimoniale de ce préjudice, tenant à l'incidence de l'absence de scolarisation sur les revenus professionnels, est réparée par l'allocation de la rente décrite ci-dessus.... ,,b) La part personnelle de ce préjudice ouvre à la victime le droit à une réparation que les juges du fond peuvent, sans commettre d'erreur de droit, assurer par l'octroi d'une indemnité globale couvrant également d'autres chefs de préjudice personnels au titre des troubles dans les conditions d'existence.</ANA>
<ANA ID="9E"> 60-04-04-02-01 Lorsque la victime se trouve, du fait d'un accident corporel survenu dans son jeune âge, privée de toute possibilité d'exercer un jour une activité professionnelle, la seule circonstance qu'il soit impossible de déterminer le parcours professionnel qu'elle aurait suivi ne fait pas obstacle à ce que soit réparé le préjudice, qui doit être regardé comme présentant un caractère certain, résultant pour elle de la perte des revenus qu'une activité professionnelle lui aurait procurés ainsi que de la pension de retraite consécutive.... ,,Il y a lieu de réparer ce préjudice par l'octroi à la victime, à compter de sa majorité et sa vie durant, d'une rente fixée sur la base du salaire médian net mensuel de l'année de sa majorité et revalorisée par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale (CSS). Doivent être déduites de cette rente les sommes éventuellement perçues par la victime au titre de l'allocation aux adultes handicapés (AAH).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. sur ce point CE, 28 avril 1978,,, n° 4225, T. pp. 941-943-945. Rappr. Cass. civ. 2e, n° 17-25855, publié.,,[RJ2] Cf., sur la déduction des prestations ayant le même objet, CE, 6 mai 1988, Administration générale de l'Assistance publique à Paris c/ consorts,, n° 64295, p. 186 ; CE, 23 septembre 2013, Centre hospitalier universitaire de Saint-Etienne, n° 350799, T. pp. 432-839-840. Comp. Cass. civ. 2e, n° 17-25855, publié.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
