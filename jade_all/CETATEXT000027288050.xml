<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288050</ID>
<ANCIEN_ID>JG_L_2013_04_000000348559</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288050.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 08/04/2013, 348559, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348559</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:348559.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 avril et 18 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C... B..., demeurant..., ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NC00978 du 17 février 2011 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant à l'annulation du jugement n° 0803902 du 11 mai 2010 par lequel le tribunal administratif de Strasbourg a rejeté sa demande d'annulation de la décision du 11 juillet 2008 de l'inspectrice du travail de la 3ème section de la Moselle autorisant son licenciement pour motif économique ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et de l'association " &#140;uvre de Guénange-Richemont " une somme de 3 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B... et de la SCP Gatineau, Fattaccini, avocat M. Andrez, commissaire à la liquidation de l'association " Oeuvres de Guenange-Richemont ",<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...et à la SCP Gatineau, Fattaccini, avocat de M. Andrez, commissaire à la liquidation de l'association " Oeuvres de Guenange-Richemont " ;<br/>
<br/>
<br/>
<br/>
<br/>
              	Sur le moyen tiré de la faute et de la légèreté blâmable de l'employeur : <br/>
<br/>
              1. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande d'autorisation de licenciement présentée par l'employeur est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié ; qu'à ce titre, lorsque la demande est fondée sur la cessation d'activité de l'entreprise, celle-ci n'a pas à être justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise ; qu'il appartient alors à l'autorité administrative de contrôler, outre le respect des exigences procédurales légales et des garanties conventionnelles, que la cessation d'activité de l'entreprise est totale et définitive, que l'employeur a satisfait, le cas échéant, à l'obligation de reclassement prévue par le code du travail et que la demande ne présente pas de caractère discriminatoire ; qu'il ne lui appartient pas, en revanche, de rechercher si cette cessation d'activité est due à la faute ou à la légèreté blâmable de l'employeur, sans que sa décision fasse obstacle à ce que le salarié, s'il s'y estime fondé, mette en cause devant les juridictions compétentes la responsabilité de l'employeur en demandant réparation des préjudices que lui auraient causé cette faute ou légèreté blâmable dans l'exécution du contrat de travail ;<br/>
<br/>
              2. Considérant qu'il résulte de ce qui précède qu'après avoir constaté que la réalité du motif économique de licenciement de l'ensemble des personnels du centre éducatif et de formation professionnelle de Guénange, où était employé M.B..., était établie dès lors que le préfet en avait prononcé la fermeture totale et définitive, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que les moyens tirés de ce que cette fermeture était due à une faute et une légèreté blâmable de l'employeur était sans incidence sur la légalité de l'autorisation de licenciement attaquée ; <br/>
<br/>
              Sur les autres moyens du pourvoi : <br/>
<br/>
              3. Considérant, en premier lieu, que, contrairement à ce que soutient le requérant, si les dispositions de l'article R. 2421-8 du code du travail imposent que la réunion du comité d'entreprise appelé à se prononcer sur le projet de licenciement d'un salarié protégé ait lieu après l'entretien préalable, elles n'interdisent pas que la convocation des membres du comité d'entreprise soit envoyée antérieurement à l'entretien préalable ; <br/>
<br/>
              4. Considérant, en second lieu, qu'eu égard au fait, relevé par la cour, que le requérant avait une connaissance précise des motifs avancés par l'employeur pour justifier son licenciement, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis en estimant que le délai qui lui a été accordé pour préparer son audition par le comité d'entreprise était suffisant ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. Andrez, agissant en qualité de commissaire à la liquidation de l'association " &#140;uvres de Guéanange-Richemont " au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : Les conclusions de l'association " &#140;uvres de Guénange-Richemont ", présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C... B..., à M. A...Andrez, commissaire à la liquidation amiable de l'association " &#140;uvre de Guénange-Richemont " et au ministre du travail, de l'emploi de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. - LICENCIEMENT FONDÉ SUR LA CESSATION D'ACTIVITÉ DE L'ENTREPRISE - 1) MOTIF LÉGAL - EXISTENCE - 2) CONTRÔLE DE L'INSPECTEUR DU TRAVAIL - PORTÉE [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-04-03 1) Une demande d'autorisation de licenciement d'un salarié protégé pour un motif économique peut légalement être fondée sur la cessation d'activité de l'entreprise, sans que celle-ci doive être justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise. 2) Il appartient alors à l'autorité administrative de contrôler, outre le respect des exigences procédurales légales et des garanties conventionnelles, que la cessation d'activité de l'entreprise est totale et définitive, que l'employeur a satisfait, le cas échéant, à l'obligation de reclassement prévue par le code du travail et que la demande ne présente pas de caractère discriminatoire. Il ne lui appartient pas, en revanche, de rechercher si cette cessation d'activité est due à la faute ou à la légèreté blâmable de l'employeur, sans que sa décision fasse obstacle à ce que le salarié, s'il s'y estime fondé, mette en cause devant les juridictions compétentes la responsabilité de l'employeur en demandant réparation des préjudices que lui auraient causé cette faute ou légèreté blâmable dans l'exécution du contrat de travail.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. Cass. soc., 16 janvier 2001,  Morvant c/ Le Royal Printemps, n° 98-44647, Bull. 2001 V n° 10 p. 7.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
