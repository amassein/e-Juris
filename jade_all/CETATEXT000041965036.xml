<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041965036</ID>
<ANCIEN_ID>JG_L_2020_06_000000431994</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/96/50/CETATEXT000041965036.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 05/06/2020, 431994</TITRE>
<DATE_DEC>2020-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431994</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431994.20200605</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux demandes distinctes, l'association des riverains du Barbot - Chambre d'Amour, M. AA... F..., Mme N... P... épouse F..., M. Q... D..., Mme R... M... épouse D..., Mme J... I... épouse A... X..., M. U... O..., Mme B... S..., Mme Z... W..., M. C... G..., M. Y... E..., Mme T... V... épouse E..., et Mme L... E... épouse H..., d'une part, et M. U... K..., d'autre part, ont demandé au juge des référés du tribunal administratif de Pau de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de l'arrêté du 24 février 2017 par lequel le maire d'Anglet a délivré à la société à responsabilité limitée M2B le permis de construire un bâtiment comportant sept logements sur la parcelle cadastrée section BM n° 334, de l'arrêté du 25 juillet 2017 par lequel ce maire a délivré à la même société un permis de construire modificatif et de l'arrêté du 4 juillet 2018 par lequel ce maire a délivré à la société civile de construction vente Villa Bali un permis de construire modificatif se rapportant au même projet. Par une ordonnance nos 1901052, 1901098 du 11 juin 2019, le juge des référés du tribunal administratif de Pau a suspendu l'exécution de ces arrêtés.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 juin et 9 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, les sociétés M2B et Villa Bali demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de chacun des requérants la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du patrimoine ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2016-925 du 7 juillet 2016 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société M2B et de la société SCCV Villa Bali et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. K... ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 mai 2020, présentée par M. K... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-4 du même code : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Pau que le maire d'Anglet a, par un arrêté du 24 février 2017, délivré à la société M2B le permis de construire un immeuble collectif de sept logements sur un terrain situé 26, avenue Guynemer. Il a ensuite délivré, par un arrêté du 25 juillet 2017, un permis de construire modificatif à cette même société et, enfin, par un arrêté du 4 juillet 2018, un second permis modificatif à la société Villa Bali, en vue de la réalisation du même projet. Après avoir saisi le tribunal administratif de Pau de demandes tendant à l'annulation de ces arrêtés pour excès de pouvoir, l'association des riverains du Barbot - Chambre d'Amour et douze autres requérants, d'une part, et M. U... K..., d'autre part, ont saisi le juge des référés de demandes tendant à la suspension de l'exécution de ces arrêtés sur le fondement de l'article L. 521-1 du code de justice administrative. Sur ces demandes, le juge des référés a suspendu l'exécution des trois arrêtés par une ordonnance du 11 juin 2019, en jugeant que les moyens tirés de l'absence d'accord de l'architecte des Bâtiments de France et de l'absence de servitude de passage au profit du terrain d'assiette du projet étaient de nature, en l'état de l'instruction, à faire naître un doute sérieux quant à la légalité du permis de construire initial, ainsi que, par voie de conséquence, quant à celle des permis modificatifs. Ultérieurement saisi par la société Villa Bali, sur le fondement de l'article L. 521-4 du même code, d'une demande tendant à ce qu'il soit mis fin à cette suspension, il a refusé d'y faire droit par une ordonnance du 14 novembre 2019, en jugeant néanmoins que le second de ces moyens n'était plus de nature à créer un doute sérieux sur la légalité des décisions contestées. Les sociétés M2B et Villa Bali se pourvoient en cassation contre l'ordonnance de suspension rendue le 11 juin 2019. <br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 621-30 du code du patrimoine, dans sa rédaction issue de la loi du 7 juillet 2016 relative à la liberté de la création, à l'architecture et au patrimoine : " I. - Les immeubles ou ensembles d'immeubles qui forment avec un monument historique un ensemble cohérent ou qui sont susceptibles de contribuer à sa conservation ou à sa mise en valeur sont protégés au titre des abords (...). / II. - La protection au titre des abords s'applique à tout immeuble, bâti ou non bâti, situé dans un périmètre délimité par l'autorité administrative dans les conditions fixées à l'article L. 621-31 (...). / En l'absence de périmètre délimité, la protection au titre des abords s'applique à tout immeuble, bâti ou non bâti, visible du monument historique ou visible en même temps que lui et situé à moins de cinq cents mètres de celui-ci (...) ". Aux termes de l'article L. 621-32 du même code, dans sa rédaction applicable au litige : " Les travaux susceptibles de modifier l'aspect extérieur d'un immeuble, bâti ou non bâti, protégé au titre des abords sont soumis à une autorisation préalable. / L'autorisation peut être refusée ou assortie de prescriptions lorsque les travaux sont susceptibles de porter atteinte à la conservation ou à la mise en valeur d'un monument historique ou des abords. / Lorsqu'elle porte sur des travaux soumis à formalité au titre du code de l'urbanisme ou au titre du code de l'environnement, l'autorisation prévue au présent article est délivrée dans les conditions et selon les modalités de recours prévues à l'article L. 632-2 du présent code ". Enfin, aux termes du I de l'article L. 632-2 de ce code, dans sa rédaction applicable au litige : " Le permis de construire (...) tient lieu de l'autorisation prévue à l'article L. 632-1 du présent code si l'architecte des Bâtiments de France a donné son accord, le cas échéant assorti de prescriptions motivées (...) ". L'article R. 425-1 du code de l'urbanisme prévoit, de même, que lorsque le projet est situé dans les abords des monuments historiques, le permis de construire tient lieu de l'autorisation prévue à l'article L. 621-32 du code du patrimoine si l'architecte des Bâtiments de France a donné son accord, le cas échéant assorti de prescriptions motivées.<br/>
<br/>
              4. Il résulte de la combinaison de ces dispositions que ne peuvent être délivrés qu'avec l'accord de l'architecte des Bâtiments de France les permis de construire portant sur des immeubles situés, en l'absence de périmètre délimité, à moins de cinq cents mètres d'un édifice classé ou inscrit au titre des monuments historiques, s'ils sont visibles à l'oeil nu de cet édifice ou en même temps que lui depuis un lieu normalement accessible au public, y compris lorsque ce lieu est situé en dehors du périmètre de cinq cents mètres entourant l'édifice en cause.<br/>
<br/>
              5. Pour juger que le moyen tiré de l'absence d'autorisation de l'architecte des Bâtiments de France faisait naître un doute sérieux sur la légalité du permis de construire du 24 février 2017, le juge des référés du tribunal administratif de Pau s'est fondé sur l'existence d'une covisibilité entre le projet et l'église Sainte-Marie de la Chambre d'Amour, classée au titre des monuments historiques, depuis un point de la promenade des sables d'or normalement accessible au public, situé à l'aplomb de l'héliport. Si les dispositions de l'article L. 621-30 du code du patrimoine ne s'opposaient pas à ce que l'existence d'une covisibilité soit constatée depuis un point situé à plus de cinq cents mètres du monument concerné, il ressort toutefois des pièces du dossier soumis au juge des référés que cette covisibilité n'était révélée que par l'utilisation d'un appareil photographique muni d'un objectif à fort grossissement. Il suit de là que les sociétés requérantes sont fondées à soutenir qu'en retenant l'existence d'une covisibilité entre le projet et l'église Sainte-Marie de la Chambre d'Amour, pour en déduire que le moyen tiré du défaut d'accord de l'architecte des Bâtiments de France faisait naître un doute sérieux sur la légalité du permis de construire litigieux, le juge des référés a dénaturé les faits de l'espèce.<br/>
<br/>
              6. En second lieu, pour suspendre par l'ordonnance attaquée l'exécution du permis du 24 février 2017, le juge des référés du tribunal administratif de Pau a également regardé comme propre à créer un doute sérieux quant à sa légalité le moyen tiré de ce que, faute de justifier d'une servitude de passage sur la parcelle à emprunter, le terrain d'assiette du projet ne bénéficiait pas d'un accès à la voie publique conforme aux exigences de l'article R. 431-9 du code de l'urbanisme. Toutefois, le juge des référés, ultérieurement saisi sur le fondement de l'article L. 521-4 du code de justice administrative, a, par une ordonnance du 14 novembre 2019, compte tenu du nouveau permis de construire modificatif délivré le 3 septembre 2019 à la société Villa Bali, jugé que ce moyen n'était plus de nature à justifier la suspension des permis litigieux. Dans ces conditions, la censure du premier motif retenu par le juge des référés dans son ordonnance du 11 juin 2019 suffit à entraîner l'annulation de cette ordonnance, sans qu'il y ait lieu, pour le juge de cassation, de se prononcer sur le bien-fondé des moyens du pourvoi dirigés contre le second motif retenu par le juge des référés. <br/>
<br/>
              7. Il résulte de ce qui précède que les sociétés requérantes sont fondées à demander l'annulation de l'ordonnance qu'elles attaquent.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9. Pour demander la suspension des permis de construire litigieux, l'association des riverains du Barbot - Chambre d'Amour et autres et M. K... soutiennent que les dossiers de demande de permis de construire étaient incomplets faute de comporter une déclaration au titre de la loi sur l'eau, une demande de permis de démolir le lavoir existant et un plan de masse indiquant l'emplacement et les caractéristiques de la servitude de passage et le caractère indivis de la propriété de la parcelle cadastrée section BM n° 344, et de justifier du titre de propriété du pétitionnaire, que l'arrêté du 24 février 2017 méconnaît les articles R. 111-2, R. 111-27, R. 425-1 et R. 431-9 du code de l'urbanisme et l'article DC 11 du règlement du plan local d'urbanisme de la commune d'Anglet, que le permis de construire délivré le 25 juillet 2017 devait être regardé comme un nouveau permis de construire auquel s'appliquait le plan local d'urbanisme entré en vigueur le 8 avril 2017, que l'arrêté du 4 juillet 2018 n'a pas été précédé d'un avis de la communauté d'agglomération du Pays basque, qu'il méconnaît les dispositions de l'arrêté municipal du 1er octobre 2007, qu'il méconnaît les articles DC 12 et UC 13 du règlement du plan local d'urbanisme de la commune et, enfin, que les arrêtés attaqués ne respectent pas les prescriptions du certificat d'urbanisme délivré le 7 septembre 2015 et méconnaissent les articles DC 3 et UB 7 du règlement du plan local d'urbanisme de la commune.<br/>
<br/>
              10. Aucun de ces moyens n'est de nature, en l'état de l'instruction, à faire naître un doute sérieux sur la légalité des arrêtés contestés.<br/>
<br/>
              11. Il suit de là que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, les demandes de suspension présentées par l'association des riverains du Barbot - Chambre d'Amour et autres, d'une part, et par M. K..., d'autre part, doivent être rejetées.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre une somme de cinq cents euros à verser à chacune des sociétés requérantes à la charge, d'une part, solidairement de l'association des riverains du Barbot - Chambre d'Amour, de M. et Mme F..., de M. et Mme D..., de Mme A... X..., de M. O..., de Mme S..., de Mme W..., de M. G..., de M. et Mme E... et de Mme H... et, d'autre part, de M. K..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font en revanche obstacle à ce qu'il soit fait droit aux conclusions de ces derniers présentées au même titre.<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 11 juin 2019 du juge des référés du tribunal administratif de Pau est annulée.<br/>
Article 2 : Les demandes présentées au juge des référés du tribunal administratif de Pau par l'association des riverains du Barbot - Chambre d'Amour, M. et Mme F..., M. et Mme D..., Mme A... X..., M. O..., Mme S..., Mme W..., M. G..., M. et Mme E... et Mme H..., d'une part, et M. K..., d'autre part, sont rejetées.<br/>
Article 3 : L'association des riverains du Barbot - Chambre d'Amour, M. et Mme F..., M. et Mme D..., Mme A... X..., M. O..., Mme S..., Mme W..., M. G..., M. et Mme E... et Mme H... solidairement, d'une part, et M. K..., d'autre part, verseront tant à la société M2B qu'à la société Villa Bali une somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de M. K... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société M2B, à la société Villa Bali, à l'association des riverains du Barbot - Chambre d'Amour, première dénommée, pour l'ensemble des demandeurs de première instance ayant présenté une requête commune avec elle, et à M. U... K....<br/>
Copie en sera adressée à la commune d'Anglet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">41-01-05-03 MONUMENTS ET SITES. MONUMENTS HISTORIQUES. MESURES APPLICABLES AUX IMMEUBLES SITUÉS DANS LE CHAMP DE VISIBILITÉ D'UN ÉDIFICE CLASSÉ OU INSCRIT. PERMIS DE CONSTRUIRE. - COVISIBILITÉ (ART. L. 621-30 DU CODE DU PATRIMOINE) - MODALITÉ D'APPRÉCIATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02-05 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). VOIES DE RECOURS. - CONTRÔLE DU JUGE DE CASSATION - CAS OÙ LE JUGE DES RÉFÉRÉS A PRONONCÉ UNE SUSPENSION EN IDENTIFIANT PLUSIEURS MOYENS DE NATURE À CRÉER UN DOUTE SÉRIEUX - JUGE DES RÉFÉRÉS AYANT ULTÉRIEUREMENT, PAR UNE ORDONNANCE RENDUE SUR LE FONDEMENT DE L'ARTICLE L. 521-4 DU CJA, ESTIMÉ QUE L'UN DE CES MOYENS N'ÉTAIT PLUS SÉRIEUX [RJ1] - CENSURE EN CASSATION DU MOTIF RESTANT EMPORTANT L'ANNULATION DE LA PREMIÈRE ORDONNANCE - EXISTENCE, SANS QU'IL Y AIT LIEU DE SE PRONONCER SUR LE MOTIF ULTÉRIEUREMENT ABANDONNÉ.
</SCT>
<ANA ID="9A"> 41-01-05-03 Il résulte de la combinaison des articles L. 621-30, L. 621-32, du I de l'article L. 632-2  du code du patrimoine et de l'article R. 425-1 du code de l'urbanisme que ne peuvent être délivrés qu'avec l'accord de l'architecte des Bâtiments de France les permis de construire portant sur des immeubles situés, en l'absence de périmètre délimité, à moins de cinq cents mètres d'un édifice classé ou inscrit au titre des monuments historiques, s'ils sont visibles à l'oeil nu de cet édifice ou en même temps que lui depuis un lieu normalement accessible au public, y compris lorsque ce lieu est situé en dehors du périmètre de cinq cents mètres entourant l'édifice en cause.</ANA>
<ANA ID="9B"> 54-035-02-05 Lorsqu'un juge des référés, après avoir par une première ordonnance regardé deux moyens comme propres à créer un doute sérieux quant à la légalité d'un permis de construire, juge, par une seconde ordonnance rendue sur le fondement de l'article L. 521-4 du code de justice administrative, que l'un de ces moyens n'était plus de nature à justifier la suspension du permis litigieux, la censure de l'autre motif retenu par le juge des référés dans sa première ordonnance suffit à entraîner l'annulation de cette ordonnance, sans qu'il y ait lieu, pour le juge de cassation, de se prononcer sur le bien-fondé des moyens du pourvoi dirigés contre le motif ultérieurement abandonné par le juge des référés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., sur l'office du juge de cassation dans le cas général, CE, 23 novembre 2005, M. et Mme,, n° 279968, T. p. 1032.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
