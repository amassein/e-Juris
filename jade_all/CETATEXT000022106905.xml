<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022106905</ID>
<ANCIEN_ID>JG_L_2010_04_000000313557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/10/69/CETATEXT000022106905.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 09/04/2010, 313557</TITRE>
<DATE_DEC>2010-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>313557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP VIER, BARTHELEMY, MATUCHANSKY ; ODENT</AVOCATS>
<RAPPORTEUR>M. Nicolas  Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:313557.20100409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 20 février 2008, 19 mai 2008 et 5 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE VIVENDI, dont le siège est 42 avenue de Friedland à Paris (75380) ; la SOCIETE VIVENDI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 20 décembre 2007 par lequel la cour administrative d'appel de Nancy, après avoir annulé le jugement du 10 mai 2005 par lequel le tribunal administratif de Châlons-en-Champagne avait rejeté ses conclusions tendant à la condamnation de la commune de Saint-Dizier à l'indemniser du préjudice subi du fait de la modification unilatérale du contrat d'affermage de l'eau conclu le 28 juin 1990, a rejeté sa demande présentée devant le tribunal administratif ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Saint-Dizier la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code des communes ;<br/>
<br/>
              Vu la loi n° 82-213 du 2 mars 1982 ;<br/>
<br/>
              Vu la loi n° 96-142 du 21 février 1996 ;<br/>
<br/>
              Vu le décret du 17 mars 1980 portant approbation d'un cahier des charges type pour l'exploitation par affermage d'un service de distribution publique d'eau potable ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Vier, Barthélemy, Matuchansky, avocat de la SOCIETE VIVENDI et de Me Odent, avocat de la commune de Saint-Dizier, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Vier, Barthélemy, Matuchansky, avocat de la SOCIETE VIVENDI et à Me Odent, avocat de la commune de Saint-Dizier ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que les dispositions de l'article L. 321-1 du code des communes, dans leur rédaction antérieure à l'entrée en vigueur de la loi du 2 mars 1982, confiaient au ministre de l'intérieur la mission, notamment, d'établir des cahiers des charges types obligatoirement applicables aux services publics communaux et intercommunaux exploités sous le régime de la concession ou de l'affermage ; que les dispositions de l'article L. 322-1 du même code, abrogées par la même loi, prévoyaient alors l'approbation de ces cahiers des charges types par décret en Conseil d'Etat ; qu'aux termes de l'article L. 321-1 du code des communes, dans sa rédaction issue de la loi du 2 mars 1982, en vigueur à la date de conclusion du contrat litigieux, ensuite abrogées par la loi du 21 février 1996 relative à la partie législative du code général des collectivités territoriales : " Le ministre de l'intérieur a notamment pour mission : / (...) 2° D'établir des modèles de cahiers des charges auxquels les communes peuvent se référer pour leurs services exploités sous le régime de la concession ou de l'affermage ainsi que des modèles de règlements auxquels elles peuvent se référer pour leurs services exploités en régie " ; <br/>
<br/>
              Considérant que les stipulations des modèles de cahiers des charges établis dans ces conditions, reprises par les contrats conclus postérieurement à l'entrée en vigueur de la loi du 2 mars 1982, ou auxquelles ces contrats se référaient expressément, étaient appelées, jusqu'à l'entrée en vigueur de la loi du 21 février 1996, à s'appliquer à un grand nombre de contrats sur l'ensemble du territoire national ; qu'en raison des conditions de leur élaboration, de leur portée et de leur approbation par l'autorité administrative, il appartient au juge de cassation, qui a pour mission d'assurer l'application uniforme de la règle de droit, de contrôler l'interprétation que les juges du fond en ont donnée ;<br/>
<br/>
              Considérant qu'aux termes de l'article 40 du contrat conclu le 28 juin 1990 entre la commune de Saint-Dizier et la Compagnie générale des eaux, dont les stipulations sont reprises du cahier des charges type annexé au décret du 17 mars 1980 : " Pour tenir compte de l'évolution des conditions économiques et techniques et pour s'assurer que la formule d'indexation est bien représentative des coûts réels, le niveau du tarif fermier, d'une part, et la composition de la formule de variation, y compris la partie fixe, d'autre part, devront être soumis à réexamen sur production par le fermier des justifications nécessaires, et notamment des comptes d'exploitation, dans les cas suivants : / 1) après cinq ans ; / 2) en cas de variation de plus de 20 % du volume global vendu, calculé sur la moyenne des trois dernières années, depuis la dernière révision ; / 3) en cas de révision du périmètre d'affermage, notamment par application de l'article 9 ; / 4) si le prix fermier a varié de plus de 50 % par rapport au prix constaté au moment de la dernière révision ; / 5) en cas de modification substantielle des ouvrages et des procédés de production et de traitement (...) ; / 6) si le montant des impôts et redevances à la charge du fermier varie de façon significative ; 7) en cas de variation de plus de 30 % du volume annuel d'eau acheté ou vendu en dehors du périmètre d'affermage " ; qu'aux termes de l'article 42 du même contrat, dont les stipulations sont également reprises du même cahier des charges type :  " (...) Si dans les trois mois à compter de la date de la demande de révision présentée par l'une des parties, un accord n'est pas intervenu, il sera procédé à cette révision par une commission composée de trois membres dont l'un sera désigné par la collectivité, l'autre par le fermier, et le troisième par les deux premiers. Faute à ceux-ci de s'entendre dans un délai de quinze jours, la désignation du troisième membre sera faite par le président du tribunal administratif (...) " ;<br/>
<br/>
              Considérant qu'il ressort de ces stipulations que dans les cas prévus à l'article 40, et en l'absence d'accord entre les parties dans le délai fixé à l'article 42, il revient à la commission instituée par cet article de procéder au réexamen tarifaire prévu à l'article 40 et d'arrêter pour l'avenir, s'il y a lieu, le niveau du tarif  du service et la composition de la formule de variation de ce tarif ; que la décision prise par la commission s'impose ainsi contractuellement aux parties ; que, par suite, la cour administrative d'appel de Nancy n'a pas inexactement interprété les stipulations du contrat en jugeant que la commune, en décidant d'adopter les propositions de la commission, n'a fait que tirer les conséquences des choix de procédure auxquelles les parties avaient entendu se soumettre en cas de désaccord sur la révision des prix de l'eau et des formules de variation, sans commettre de faute dans l'exécution du contrat ; que cette révision ne procédant ainsi pas d'une décision unilatérale de la commune, la cour n'a pas non plus commis d'erreur de droit en écartant le moyen tiré de ce que la modification unilatérale du contrat engageait la responsabilité sans faute de la commune ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE VIVENDI n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de Saint-Dizier, qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la SOCIETE VIVENDI et non compris dans les dépens ; qu'il y a lieu en revanche, en application de ces dispositions, de mettre à la charge de la SOCIETE VIVENDI le versement à la commune de Saint-Dizier de la somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SOCIETE VIVENDI est rejeté.<br/>
<br/>
Article 2 : La SOCIETE VIVENDI versera à la commune de Saint-Dizier une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE VIVENDI et à la commune de Saint-Dizier.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-04-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. VOIES DE RECOURS. CASSATION. - CONTRÔLE DU JUGE - EXISTENCE - INTERPRÉTATION D'UNE STIPULATION D'UN CAHIER DES CHARGES TYPE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. RÉGULARITÉ INTERNE. - INTERPRÉTATION D'UNE STIPULATION D'UN CONTRAT-TYPE [RJ1].
</SCT>
<ANA ID="9A"> 39-08-04-02 Le juge de cassation contrôle l'interprétation que les juges du fond ont donnée des stipulations d'un cahier des charges type.</ANA>
<ANA ID="9B"> 54-08-02-02-01 Le juge de cassation contrôle l'interprétation que les juges du fond ont donnée des stipulations d'un cahier des charges type.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant du contrôle de l'interprétation des clauses d'un cahier des clauses administratives générales, Section, 27 mars 1998, Société d'assurances La Nantaise et L'Angevine réunies, n° 144240, p. 109.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
