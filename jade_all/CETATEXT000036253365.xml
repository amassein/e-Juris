<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253365</ID>
<ANCIEN_ID>JG_L_2017_12_000000397220</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème chambres réunies, 22/12/2017, 397220, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397220</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397220.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris d'annuler la décision du 25 novembre 2014 par laquelle le directeur régional des finances publiques d'Ile-de-France et du département de Paris a rejeté sa demande de décharge gracieuse de responsabilité solidaire. Par un jugement n° 1501367 du 23 décembre 2015, le tribunal administratif de Paris a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 février et 23 mai 2016 et le 14 février 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêt du 16 mai 2002, la cour d'appel de Paris, confirmant un jugement du tribunal de grande instance de Paris en date du 20 novembre 2000, a déclaré M. A...solidairement responsable, à hauteur d'une somme de 246 218,93 euros, du paiement des impositions dues par la société Licia, dont il était le dirigeant et qui a été mise en liquidation judiciaire le 22 avril 1997. M. A...a adressé, le 7 mars 2013, au  directeur régional des finances publiques d'Ile-de-France et du département de Paris une demande de décharge de responsabilité solidaire dans le paiement de cette imposition. Celui-ci a rejeté sa demande par une première décision du 30 juin 2014, confirmée par une seconde décision du 25 novembre 2014 prise à la suite d'un recours gracieux formé le 29 juillet 2014 par M.A.... Celui-ci se pourvoit en cassation contre le jugement du 23 décembre 2015 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de cette décision.<br/>
<br/>
              2. La circonstance, invoquée par le ministre en défense, que M. A...se soit acquitté de sa dette après le rejet de sa demande en décharge de responsabilité solidaire est sans incidence sur la légalité de la décision de rejet de sa demande de remise gracieuse, qui s'apprécie à la date à laquelle elle a été prise. Dès lors, il y a lieu de statuer sur le pourvoi. <br/>
<br/>
              3. Aux termes de l'article L. 247 du livre des procédures fiscales : " L'administration peut accorder sur la demande du contribuable ; 1° Des remises totales ou partielles d'impôts directs régulièrement établis lorsque le contribuable est dans l'impossibilité de payer par suite de gêne ou d'indigence / (...) / L'administration peut également décharger de leur responsabilité les personnes tenues au paiement d'impositions dues par un tiers. (...) ".<br/>
<br/>
              4. En premier lieu, aux termes de l'article 212 du code civil : " Les époux se doivent mutuellement respect, fidélité, secours, assistance. ". Pour juger que M. A...ne pouvait être regardé comme étant dans l'impossibilité de payer par suite de gêne ou d'indigence, le tribunal administratif de Paris a relevé que la décision attaquée s'était référée à bon droit à l'obligation de secours entre époux, telle qu'elle résulte des dispositions précitées du code civil. En statuant ainsi, alors que ces dispositions sont sans incidence sur l'appréciation de la situation de gêne ou d'indigence au sens de l'article L. 247 du livre des procédures fiscales, le tribunal a commis une erreur de droit.<br/>
<br/>
              5. En second lieu, aux termes de l'article 225 du code civil : " Chacun des époux administre, oblige et aliène seul ses biens personnels. " et, aux termes du premier alinéa de l'article 1536 du même code : " Lorsque les époux ont stipulé dans leur contrat de mariage qu'ils seraient séparés de biens, chacun d'eux conserve l'administration, la jouissance et la libre disposition de ses biens personnels. ". Pour juger que M. A...ne pouvait être regardé comme étant dans l'impossibilité de payer par suite de gêne ou d'indigence, le tribunal  a relevé qu'il était propriétaire d'un appartement à Neuilly-sur-Seine avec son épouse, qui avait été subrogée dans les droits d'une partie des créanciers disposant d'hypothèques sur ce bien, après avoir racheté leurs créances. En prenant ainsi en compte, pour apprécier la capacité contributive de M. A..., la valeur de cet appartement dans sa totalité, au lieu de se fonder sur  les seuls droits que M. A...détenait en propre sur ce bien, nets des dettes s'y rapportant le cas échéant, alors que sa dette fiscale était une dette personnelle et qu'il était séparé de biens avec son épouse, le tribunal a commis une seconde erreur de droit.  <br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, le jugement du tribunal administratif de Paris attaqué doit être annulé. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre une somme de 3 000 euros à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 23 décembre 2015 est annulé. <br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
