<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036694127</ID>
<ANCIEN_ID>JG_L_2018_03_000000410477</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/69/41/CETATEXT000036694127.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 09/03/2018, 410477, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410477</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Laure Denis</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410477.20180309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés respectivement les 11 mai 2017 et 22 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 mars 2017 par laquelle la commission d'avancement a émis un avis défavorable à une nomination directe dans le corps de la magistrature aux fonctions hors hiérarchie au titre de l'article 40 de l'ordonnance du 22 décembre 1958 ; <br/>
<br/>
              2°) d'enjoindre à la commission d'avancement de réexaminer son dossier d'intégration dans un délai d'un mois à compter de la décision à intervenir, ou au plus tard à sa prochaine session ;<br/>
<br/>
              3°) d'enjoindre au garde des sceaux de lui communiquer le procès-verbal de la délibération de la commission d'avancement du 5 au 16 décembre 2016.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Laure Denis, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 40 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Peuvent être nommés directement aux fonctions hors hiérarchie s'ils remplissent les conditions prévues à l'article 16 ci-dessus : / (...) / Peuvent également être nommés aux fonctions hors hiérarchie des cours d'appel, à l'exception, toutefois, des fonctions de premier président et de procureur général, les avocats inscrits à un barreau français justifiant de vingt-cinq années au moins d'exercice de leur profession. / Les candidats visés aux 3°, 4° et 5° ainsi que les candidats visés au septième alinéa du présent article ne peuvent être nommés qu'après avis de la commission prévue à l'article 34. (...) ".<br/>
<br/>
              2. Mme B...a présenté sa candidature à une nomination directe en qualité de magistrat hors hiérarchie au titre des dispositions rappelées au point 1. Sa candidature n'a toutefois pas été retenue. Elle a été informée, par un courrier du premier président et du procureur général de la cour d'appel de Grenoble du 15 mars 2017, que la commission d'avancement instituée par l'article 34 de l'ordonnance du 22 décembre 1958, qui s'est réunie du 5 au 9 décembre 2015 et du 12 au 16 décembre 2016 avait émis un avis défavorable à sa nomination directe. Elle demande l'annulation pour excès de pouvoir de cet avis.<br/>
<br/>
              3. Mme B...soutient que la décision litigieuse n'est pas motivée, en méconnaissance des dispositions des articles L. 211-1 à L. 211-8 et L. 232-4 du code des relations entre le public et l'administration. En vertu de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, dont les dispositions ont été reprises à l'article L. 211-2 du code des relations entre l'administration et les administrés, doivent être motivées les décisions qui "  6° Refusent un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir ". Les avis défavorables rendus par la commission d'avancement sur une candidature à un recrutement direct dans la magistrature ne sauraient être regardés comme le refus d'une autorisation ou d'un avantage dont l'attribution constitue un droit au sens de ces dispositions. Aucune autre disposition n'impose la motivation d'une telle décision. Par suite, le moyen tiré du défaut de motivation de la décision attaquée doit être écarté.<br/>
<br/>
              4. Par ailleurs, le moyen tiré de ce que la décision attaquée serait illégale du fait des erreurs de fait entachant les avis de chefs de cour qui l'ont précédée n'est, en tout état de cause, pas assorti de précisions suffisantes pour permettre d'en apprécier le bien-fondé.<br/>
<br/>
              5. Il résulte de ce qui précède que Mme B...n'est pas fondée à demander l'annulation de la décision qu'elle attaque. Il n'y a pas lieu de faire droit à ses conclusions à fin d'injonction.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
