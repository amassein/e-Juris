<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037461539</ID>
<ANCIEN_ID>JG_L_2018_09_000000424009</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/46/15/CETATEXT000037461539.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/09/2018, 424009, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424009</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; CORLAY</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:424009.20180927</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 10 et 21 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération des experts- comptables et commissaires aux comptes de France demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision du 12 juillet 2018 par laquelle la garde des sceaux, ministre de la justice a reporté sine die les opérations électorales en vue de la désignation des conseillers régionaux dans les compagnies régionales des commissaires aux comptes ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la décision attaquée est détachable du contentieux des élections professionnelles ;<br/>
              - la condition de l'urgence est remplie dès lors que, d'une part, le report sine die d'opérations électorales imminentes nuit à l'intérêt public qui s'attache à ce que les instances représentant légalement la profession soient les plus représentatives possible, et que, d'autre part, la décision litigieuse prive le corps électoral professionnel de la possibilité d'élire ses représentants ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision litigieuse méconnaît le principe de périodicité raisonnable de l'exercice du droit au suffrage ;<br/>
              - elle méconnaît le principe de l'égalité de suffrage ;<br/>
              - elle est entachée d'erreur de droit en ce qu'elle se fonde sur des motifs impropres à justifier un tel report ;<br/>
              - elle est constitutive d'un détournement de pouvoir.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 septembre 2018, la garde des sceaux, ministre de la justice, conclut au rejet de la requête. Elle soutient que celle-ci est irrecevable dès lors que l'acte attaqué ne fait pas grief et, à titre subsidiaire, que la condition d'urgence n'est pas remplie et que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 septembre 2018, la Compagnie nationale des commissaires aux comptes conclut au rejet de la requête. Elle soutient que celle-ci est irrecevable dès lors que l'acte attaqué ne fait pas grief et, à titre subsidiaire, que la condition d'urgence n'est pas remplie et que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Fédération des experts comptables et commissaires aux comptes de France et, d'autre part, la garde des sceaux, ministre de la justice et la Compagnie nationale des commissaires aux comptes ;<br/>
<br/>
              Par application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être fondée sur les moyens, relevés d'office, tirés, d'une part, de l'incompétence de la juridiction administrative pour connaître du présent litige et, d'autre part, de l'incompétence de la garde des sceaux, ministre de la justice, pour procéder au report des opérations électorales en vue de la désignation des conseillers régionaux dans les compagnies régionales des commissaires aux comptes.<br/>
<br/>
              Vu le procès-verbal de l'audience publique du lundi 24 septembre 2018, à 10 heures 25 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Briard avocat au Conseil d'Etat et à la Cour de cassation, représentant de la Fédération des experts-comptables et commissaires aux comptes de France ;<br/>
<br/>
              - les représentants de la Fédération des experts-comptables et commissaires aux comptes de France ;<br/>
<br/>
              - Me Remy-Corlay avocat au Conseil d'Etat et à la Cour de cassation, représentante de la Compagnie nationale des commissaires aux comptes ;<br/>
<br/>
              - la représentante de la Compagnie nationale des commissaires aux comptes ;<br/>
<br/>
              - les représentants de la garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au mardi 25 septembre 2018 à 12 heures ;<br/>
<br/>
<br/>
              Vu le nouveau mémoire enregistré le 25 septembre 2018, avant la clôture de l'instruction, par lequel la Compagnie nationale des commissaires aux comptes maintient ses conclusions et ses moyens. Elle soutient en outre que le juge administratif est incompétent pour connaître du litige.<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 25 septembre 2018, avant la clôture de l'instruction, par lequel la Fédération des experts-comptables et commissaires aux comptes de France maintient ses conclusions et ses moyens. Elle soutient en outre que, d'une part, le juge administratif est compétent et, d'autre part, il existe un doute sérieux quant à la légalité de la décision contestée résultant de l'incompétence de la garde des sceaux, ministre de la justice pour prendre celle-ci.<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 25 septembre 2018 avant la clôture de l'instruction, par lequel la garde des sceaux, ministre de la justice, maintient ses conclusions et ses moyens. Elle soutient en outre que le juge administratif est incompétent pour connaître du litige.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Aux termes de l'article R. 821-54 du code de commerce relatif aux conseils régionaux des commissaires aux comptes : " Les membres du conseil régional sont élus au scrutin secret, pour une durée de quatre ans./ Le conseil régional est renouvelé par moitié tous les deux ans./ (...) ". Aux termes de l'article R. 821-35 du même code : " Le règlement intérieur de chaque compagnie fixe les modalités de la publicité à donner aux candidatures, de l'organisation des élections, du dépouillement du scrutin, du règlement des contestations et de la publication des résultats. ". Aux termes de l'article 10-5 du règlement intérieur de la Compagnie nationale des commissaires aux comptes : " Le Conseil National fixe, au moins six mois avant la date d'expiration du mandat des membres sortants des conseils régionaux, les dates retenues pour l'élection aux conseils régionaux ". Il résulte des articles 10-5 et suivants du même règlement qu'il appartient aux compagnies régionales d'organiser l'élection des membres des conseils régionaux aux dates fixées par le conseil national.<br/>
<br/>
              3. Il ressort des pièces du dossier que le mandat des membres actuels des conseils régionaux des commissaires aux comptes expire le 31 décembre 2018 et que le conseil national a décidé que le premier tour de scrutin pour l'élection des conseillers régionaux se déroulerait entre le 25 septembre et le 8 octobre 2018, et que le second tour aurait lieu entre le 30 octobre et le 12 novembre 2018. <br/>
<br/>
              4. La fédération requérante soutient que l'annonce de la garde des sceaux, ministre de la justice, dans un discours prononcé le 12 juillet 2018, indiquant que " l'expression majoritaire en faveur d'un report des élections m'amène à accueillir favorablement cette demande de vos instances représentatives qui m'apparaît légitime et conforme à l'intérêt général ", constitue une décision de report sine die de l'élection des membres des conseils régionaux des commissaires aux comptes. Toutefois, la compétence pour fixer la date des élections en litige appartenant au conseil national des commissaires aux comptes, en vertu des dispositions citées au point 2, la ministre précise, dans ses écritures, que son annonce visait seulement à indiquer qu'elle proposerait au Premier ministre de déroger, par un décret en Conseil d'Etat, aux dispositions précitées du code de commerce en prorogeant le mandat en cours des membres des conseils régionaux. Si le litige portant sur une telle annonce relève de la compétence de la juridiction administrative, cette annonce, qui est dépourvue par elle-même de tout effet juridique direct, ne révèle pas l'existence d'une décision susceptible d'être attaquée par la voie du recours en excès de pouvoir. Il suit de là qu'ainsi que le soutiennent la ministre et la compagnie nationale des commissaires aux comptes, la requête tendant à l'annulation de l'acte attaqué n'est pas recevable. Dès lors, la requête devant le juge des référés à fin de suspension du même acte ne peut qu'être rejetée.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Fédération des experts-comptables et commissaires aux comptes de France est rejetée.  <br/>
Article 2 : La présente ordonnance sera notifiée à la Fédération des experts-comptables et commissaires aux comptes de France, à la Compagnie nationale des commissaires aux comptes et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
