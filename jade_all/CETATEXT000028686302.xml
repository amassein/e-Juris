<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028686302</ID>
<ANCIEN_ID>JG_L_2014_03_000000369996</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/68/63/CETATEXT000028686302.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 05/03/2014, 369996</TITRE>
<DATE_DEC>2014-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369996</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP CELICE, BLANCPAIN, SOLTNER ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369996.20140305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 8 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'association Société pour la protection des paysages et de l'esthétique de la France, M. et Mme A... et l'association SOS Paris ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1307371 du 4 juillet 2013 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté leur demande tendant à la suspension de l'exécution de l'arrêté du 17 décembre 2012 par lequel le maire de Paris a accordé un permis de construire n° PC07510111V0027 à la société Grands Magasins de la Samaritaine Maison Ernest Cognacq pour la restructuration d'un ensemble de bâtiments de sept à dix étages sur quatre niveaux de sous-sol, dits " Sauvage ", " Jourdain plateau " et " Jourdain verrière ", avec démolition et reconstruction de planchers à tous les niveaux, restauration totale des façades sur rue et aménagement d'une cour intérieure ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande de suspension ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de l'association Société pour la protection des paysages et de l'esthétique de la France et autres, à Me Foussard, avocat de la ville de Paris et à la SCP Célice, Blancpain, Soltner, avocat de la société Grands magasins de la Samaritaine Maison Ernest Cognacq ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que le juge des référés ne peut ordonner la suspension de l'exécution d'une décision en application de cette disposition lorsqu'il apparaît, en l'état de l'instruction, que la requête au fond contre cette décision n'est pas recevable ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article R. 600-1 du code de l'urbanisme : " En cas (...) de recours contentieux à l'encontre (...) d'un permis de construire, d'aménager ou de démolir, (...) l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. (...) / La notification prévue au précédent alinéa doit intervenir par lettre recommandée avec accusé de réception, dans un délai de quinze jours francs à compter du dépôt du (...) recours. (...) " ; que ces dispositions font obligation à l'auteur d'un recours contentieux de notifier une copie du texte intégral de son recours à l'auteur ainsi qu'au bénéficiaire du permis attaqué ; que lorsque le destinataire de cette notification soutient que la notification qui lui a été adressée ne comportait pas la copie de ce recours, mais celle d'un recours dirigé contre un autre acte, il lui incombe d'établir cette allégation en faisant état des diligences qu'il aurait vainement accomplies auprès de l'expéditeur pour obtenir cette copie ou par tout autre moyen ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés, d'une part, que pour satisfaire à l'obligation de notification de leur recours contre le permis n° PC07510111V0027 délivré par le maire de Paris à la société Grands Magasins de la Samaritaine Maison Ernest Cognacq, l'association Société pour la protection des paysages et de l'esthétique de la France, M. et Mme A... et l'association SOS Paris ont adressé à la ville de Paris, dans le délai de quinze jours imparti par l'article R. 600-1 du code de l'urbanisme, un courrier indiquant le numéro du permis contesté ainsi que les travaux qu'il autorise et précisant qu'une copie de ce recours était jointe à ce pli et, d'autre part, que la ville de Paris a soutenu ne pas avoir reçu cette copie, mais celle du recours formé contre un autre permis, n° PC07510111V0026 ; <br/>
<br/>
              4. Considérant que, pour rejeter la demande de suspension qui lui avait été présentée par les requérants au motif que leur recours pour excès de pouvoir ne satisfaisait pas aux prescriptions de l'article R. 600-1 du code de l'urbanisme et était, par suite, irrecevable, le juge des référés a relevé qu'il ressortait des pièces du dossier que les demandeurs n'avaient pas notifié leur recours à la ville de Paris dans le délai requis ; qu'en statuant ainsi, sans rechercher si la ville de Paris établissait le caractère incomplet de cette notification, le juge des référés a commis une erreur de droit ; que, par suite, la Société pour la protection des paysages et de l'esthétique de la France et autres sont fondés à demander l'annulation de l'ordonnance qu'ils attaquent, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Société pour la protection des paysages et de l'esthétique de la France, de M. et Mme A...et de l'association SOS Paris, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions qu'ils présentent au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1307371 du juge des référés du tribunal administratif de Paris du 4 juillet 2013 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant le juge des référés du tribunal administratif de Paris.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 4 : Les conclusions de la société Grands Magasins de la Samaritaine Maison Ernest Cognacq et de la ville de Paris présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'association Société pour la protection des paysages et de l'esthétique de la France, premier requérant dénommé, à la société Grands Magasins de la Samaritaine Maison Ernest Cognacq et à la ville de Paris.<br/>
Les autres requérants seront informés de la présente décision par la SCP de Chaisemartin, Courjon, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. - NOTIFICATION DES RECOURS EN MATIÈRE D'URBANISME (ART. R. 600-1 DU CODE DE L'URBANISME) - AUTEUR OU BÉNÉFICIAIRE DE LA DÉCISION D'URBANISME ATTAQUÉE SOUTENANT QUE LA NOTIFICATION QU'IL A REÇUE COMPORTAIT LA COPIE D'UN RECOURS DIRIGÉ CONTRE UNE AUTRE DÉCISION - OBLIGATION POUR LUI D'ÉTABLIR SON ALLÉGATION - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-01-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. OBLIGATION DE NOTIFICATION DU RECOURS. - AUTEUR OU BÉNÉFICIAIRE DE LA DÉCISION D'URBANISME ATTAQUÉE SOUTENANT QUE LA NOTIFICATION QU'IL A REÇUE COMPORTAIT LA COPIE D'UN RECOURS DIRIGÉ CONTRE UNE AUTRE DÉCISION - OBLIGATION POUR LUI D'ÉTABLIR SON ALLÉGATION - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-01 Les dispositions de l'article R. 600-1 du code de l'urbanisme font obligation à l'auteur d'un recours contentieux de notifier une copie du texte intégral de son recours à l'auteur ainsi qu'au bénéficiaire du permis attaqué. Lorsque le destinataire de cette notification soutient que la notification qui lui a été adressée ne comportait pas la copie de ce recours, mais celle d'un recours dirigé contre un autre acte, il lui incombe d'établir cette allégation en faisant état des diligences qu'il aurait vainement accomplies auprès de l'expéditeur pour obtenir cette copie ou par tout autre moyen.</ANA>
<ANA ID="9B"> 68-06-01-04 Les dispositions de l'article R. 600-1 du code de l'urbanisme font obligation à l'auteur d'un recours contentieux de notifier une copie du texte intégral de son recours à l'auteur ainsi qu'au bénéficiaire du permis attaqué. Lorsque le destinataire de cette notification soutient que la notification qui lui a été adressée ne comportait pas la copie de ce recours, mais celle d'un recours dirigé contre un autre acte, il lui incombe d'établir cette allégation en faisant état des diligences qu'il aurait vainement accomplies auprès de l'expéditeur pour obtenir cette copie ou par tout autre moyen.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., lorsque le requérant invité à régulariser sa requête répond au tribunal par une lettre ne contenant pas les justificatifs annoncés, CE, 26 mai 2009, Mme Kyung A Min épouse Loiseau, n° 316252, T. pp. 844-908-992.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
