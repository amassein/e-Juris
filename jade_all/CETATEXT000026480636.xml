<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026480636</ID>
<ANCIEN_ID>JG_L_2012_10_000000361257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/48/06/CETATEXT000026480636.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 11/10/2012, 361257, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean Courtial</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Christine Allais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:361257.20121011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1200979-1 du 19 juillet 2012, enregistrée le 23 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Limoges a transmis au Conseil d'Etat, en application de l'article R. 341-2 du code de justice administrative, la requête présentée le 18 juillet 2012 à ce tribunal par M. Rémy A ;<br/>
<br/>
              Vu le mémoire, enregistré le 18 juillet 2012 au greffe du tribunal administratif de Limoges, présenté par M. A, demeurant ... en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; M. A demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation des décisions attaquées, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 105 de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et ses articles 34 et 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu l'article 105 de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Allais, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que l'article 105 de la loi du 28 décembre 2011 de finances pour 2012 dispose : " Hormis les cas de congé de longue maladie, de congé de longue durée ou si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de leurs fonctions, les agents publics civils et militaires en congé de maladie, ainsi que les salariés dont l'indemnisation du congé de maladie n'est pas assurée par un régime obligatoire de sécurité sociale, ne perçoivent pas leur rémunération au titre du premier jour de ce congé. " ;<br/>
<br/>
              3. Considérant, en premier lieu, que, si M. A soutient qu'en faisant figurer dans la loi du 28 décembre 2011 de finances pour 2012 une disposition qui relevait normalement du domaine de la loi ordinaire, le législateur a méconnu les règles de l'article 34 de la Constitution, ces règles ne sont pas au nombre des droits et libertés garantis par la Constitution ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que le droit à rémunération après service fait, tel qu'il est défini à l'article 34 de la loi du 11 janvier 1984 qui dispose que le fonctionnaire en activité a droit à l'intégralité de son traitement pendant les trois premiers mois d'un congé maladie, s'il constitue une garantie fondamentale accordée aux fonctionnaires au sens et pour l'application de l'article 34 de la Constitution, ne figure pas, contrairement à ce que soutient M. A, au nombre des droits et libertés qui sont garantis par le Préambule de la Constitution de 1946 ;<br/>
<br/>
              5. Considérant, en troisième lieu, que M. A soutient que, faute d'avoir été soumises par le Gouvernement à la consultation préalable des représentants des fonctionnaires alors qu'elles étaient relatives à leurs conditions de travail, les dispositions de l'article 105 de la loi du 28 décembre 2011 de finances pour 2012 ont été adoptées en méconnaissance du principe de participation des travailleurs à la détermination collective des conditions de travail, garanti par les dispositions du huitième alinéa du Préambule de la Constitution de 1946 ; que, toutefois, en tout état de cause, les dispositions du huitième alinéa n'obligent pas, par elles-mêmes, le Gouvernement à faire précéder la présentation au Parlement d'un projet de loi touchant à la détermination des conditions de travail des agents publics d'une consultation préalable de leurs représentants ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article 105 de la loi du 28 décembre 2011 de finances pour 2012 porte atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A.<br/>
Article 2 : La présente décision sera notifiée à M. Rémy A, au Premier ministre, à la ministre de l'écologie, du développement durable et de l'énergie et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
Copie en sera adressée au Conseil constitutionnel et au tribunal administratif de Limoges.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
