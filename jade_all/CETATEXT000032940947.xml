<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032940947</ID>
<ANCIEN_ID>JG_L_2016_07_000000390415</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/09/CETATEXT000032940947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 27/07/2016, 390415, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390415</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Simon Chassard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:390415.20160727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au magistrat désigné par le président du tribunal administratif de Saint-Denis d'annuler la décision du 26 mars 2013 par laquelle le directeur régional des finances publiques de la Réunion a refusé de lui accorder le bénéfice de l'indemnité temporaire de retraite prévue à l'article 137 de la loi n° 2008-1443 du 30 décembre 2008. Par un jugement n° 1300680 du 26 mars 2015, le magistrat désigné par le président du tribunal administratif de Saint-Denis a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 26 mai, 24 août et 15 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2008-1443 du 30 décembre 2008 ;<br/>
              - le décret n° 78-399 du 20 mars 1978 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Simon Chassard, auditeur, <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du II de l'article 137 de la loi du 30 décembre 2008 de finances rectificative pour 2008 : " II. A compter du 1er  janvier 2009, l'attribution de nouvelles indemnités temporaires est réservée aux pensionnés ayants droit remplissant, à la date d'effet de leur pension, en sus de l'effectivité de la résidence, les conditions suivantes : / 1° a) Justifier de quinze ans de services effectifs dans une ou plusieurs collectivités mentionnées au I à partir d'un état récapitulatif de ces services fourni par les pensionnés et communiqué par leurs ministères d'origine ; / b) Ou remplir, au regard de la collectivité dans laquelle l'intéressé justifie de sa résidence effective, les critères d'éligibilité retenus pour l'octroi des congés bonifiés à leur bénéficiaire principal ; / (...) ". Aux termes de l'article 1er du décret du 20 mars 1978 relatif, pour les départements d'outre-mer, à la prise en charge des frais de voyage de congés bonifiés accordés aux magistrats et fonctionnaires civils de l'Etat : " Les dispositions du présent décret s'appliquent aux magistrats et aux fonctionnaires relevant du statut général des fonctionnaires de l'Etat qui exercent leurs fonctions : / a) Dans un département d'outre-mer et dont le lieu de résidence habituelle, tel qu'il est défini à l'article 3 ci-dessous, est situé soit sur le territoire européen de la France, soit dans le même département d'outre-mer, soit dans un autre département d'outre-mer ; / b) Sur le territoire européen de la France si leur lieu de résidence habituelle est situé dans un département d'outre-mer. ". Aux termes de l'article 3 de ce même décret : " Le lieu de résidence habituelle est le territoire européen de la France ou le département d'outre-mer où se trouve le centre des intérêts moraux et matériels de l'intéressé. ". <br/>
<br/>
              2. Pour l'application des dispositions précitées du II de l'article 137 de la loi du 30 décembre 2008 de finances rectificative pour 2008, un pensionné qui demande à bénéficier de l'indemnité temporaire de retraite, lorsqu'il ne justifie pas de quinze ans de services effectifs dans une ou plusieurs collectivités pour lesquelles le bénéfice de l'indemnité temporaire de retraite est ouvert, doit justifier qu'à la date d'effet de sa pension, il avait sur le territoire de la collectivité dans laquelle il réside effectivement le centre de ses intérêts matériels et moraux.<br/>
<br/>
              3. Pour juger que M.A..., qui a été admis à faire valoir ses droits à la retraite par un arrêté du 14 janvier 2013 à compter du 20 février 2013, ne satisfaisait pas aux conditions fixées par ces dispositions, le magistrat désigné par le président du tribunal administratif de Saint-Denis, après avoir rappelé les éléments invoqués par l'intéressé au soutien de sa demande, lesquels portaient sur sa situation personnelle, familiale et patrimoniale, s'est borné à relever qu'il était propriétaire de deux appartements en métropole et n'avait dès lors pas transféré l'intégralité de ses intérêts matériels et moraux à La Réunion. En statuant ainsi, alors que ce seul élément pris isolément ne pouvait être déterminant et qu'il lui appartenait de rechercher s'il ressortait de l'ensemble des circonstances de l'espèce que M. A... justifiait avoir le centre de ses intérêts matériels et moraux à La Réunion, l'auteur du jugement attaqué a commis une erreur de droit. Par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, ce jugement doit être annulé. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 26 mars 2015 du magistrat désigné du tribunal administratif de Saint-Denis est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Saint-Denis.<br/>
<br/>
Article 3 : L'Etat versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M.  B...A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
