<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034078363</ID>
<ANCIEN_ID>JG_L_2017_01_000000406420</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/83/CETATEXT000034078363.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 02/01/2017, 406420, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406420</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:406420.20170102</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Caen, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'arrêté du 16 décembre 2016 par lequel la préfète de l'Orne a fixé la composition du conseil communautaire de la communauté de communes des Vallées d'Auge et du Merlerault à compter du 1er janvier 2017 ainsi que la délibération du 22 décembre 2016 par laquelle le conseil municipal de la commune de Sainte-Gauburge-Sainte-Colombe a élu trois conseillers communautaires chargés de représenter la commune au sein du conseil communautaire de la communauté de communes des Vallées d'Auge et du Merlerault, et, d'autre part, d'enjoindre à la préfète de l'Orne de modifier l'arrêté du 16 décembre 2016 dans un délai de 48 heures en attribuant notamment quatre sièges de conseillers communautaires à la commune de Sainte-Gauburge-Sainte-Colombe.<br/>
              Par une ordonnance n° 1602450 du 28 décembre 2016, le juge des référés du tribunal administratif de Caen a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 29 décembre 2016 au secrétariat du contentieux du Conseil d'État, M. A...demande au juge des référés du Conseil d'État, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de la préfète de l'Orne la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :  <br/>
              - il justifie d'un intérêt direct et personnel à agir dès lors que l'arrêté contesté entraîne la fin de son mandat de conseiller communautaire ;<br/>
              - la condition d'urgence est remplie dès lors que son mandat prendra fin à l'occasion de la première réunion du conseil communautaire de la nouvelle communauté de communes des Vallées d'Auge et du Merlerault, prévue le 3 janvier 2017 à 20 heures ;<br/>
              - les décisions attaquées portent une atteinte grave et manifestement illégale à sa liberté d'exercer librement son mandat d'élu local, liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative ;<br/>
              - les décisions attaquées méconnaissent l'article L. 5211-6-1 du code général des collectivités territoriales dès lors qu'elles ont attribué un nombre erroné de sièges à la commune de Sainte-Gauburge-Sainte-Colombe au regard de la règle de l'arrondi à l'entier supérieur.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée ;<br/>
<br/>
              2. Considérant qu'il résulte des pièces du dossier, notamment des éléments recueillis par le juge de première instance, que la préfète de l'Orne a décidé de la création à compter du 1er janvier 2017 de la communauté de communes des Vallées d'Auge et du Merlerault issue de la fusion des communautés de communes du Pays du Camembert, du canton de la région de Gacé et des Vallées du Merlerault ; qu'elle a fixé à soixante-douze le nombre de sièges au conseil de la communauté de communes, dont trois pour la commune de Sainte-Gauburge-Sainte-Colombe ; que cette commune a désigné trois conseillers communautaires, parmi lesquels ne figurait pas M.A..., élu conseiller communautaire au sein de la communauté de communes des Vallées du Merlerault lors des élections municipales et communautaires de mars 2014 ; que M. A...demande l'annulation de l'ordonnance par laquelle le juge des référés du tribunal administratif de Caen, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant notamment à la suspension de l'exécution de l'arrêté préfectoral fixant le nombre de conseillers communautaires et de la délibération du conseil municipal de Sainte-Gauburge-Sainte-Colombe désignant ses représentants au conseil de communauté ;   <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 5211-6-2 du code général des collectivités territoriales : " Par dérogation aux articles L. 5211-6 et L. 5211-6-1, entre deux renouvellements généraux des conseils municipaux : / 1° En cas de création d'un établissement public de coopération intercommunale à fiscalité propre, de fusion entre plusieurs établissements publics de coopération intercommunale dont au moins l'un d'entre eux est à fiscalité propre, d'extension du périmètre d'un tel établissement par l'intégration d'une ou de plusieurs communes ou la modification des limites territoriales d'une commune membre ou d'annulation par la juridiction administrative de la répartition des sièges de conseiller communautaire, il est procédé à la détermination du nombre et à la répartition des sièges de conseiller communautaire dans les conditions prévues à l'article L. 5211-6-1. (...) " ; qu'aux termes de l'article L. 5211-6-1 du même code : " I.-Le nombre et la répartition des sièges de conseiller communautaire sont établis : / 1° Soit selon les modalités prévues aux II à VI du présent article ; / 2° Soit, dans les communautés de communes et dans les communautés d'agglomération, par accord des deux tiers au moins des conseils municipaux des communes membres représentant plus de la moitié de la population de celles-ci ou de la moitié au moins des conseils municipaux des communes membres représentant plus des deux tiers de la population de celles-ci. (...) II. -Dans les métropoles et les communautés urbaines et, à défaut d'accord, dans les communautés de communes et les communautés d'agglomération, la composition de l'organe délibérant est établie par les III à VI selon les principes suivants : / 1° L'attribution des sièges à la représentation proportionnelle à la plus forte moyenne aux communes membres de l'établissement public de coopération intercommunale, en fonction du tableau fixé au III, garantit une représentation essentiellement démographique ; / 2° L'attribution d'un siège à chaque commune membre de l'établissement public de coopération intercommunale assure la représentation de l'ensemble des communes. (...) IV.-La répartition des sièges est établie selon les modalités suivantes : / 1° Les sièges à pourvoir prévus au tableau du III sont répartis entre les communes à la représentation proportionnelle à la plus forte moyenne, sur la base de leur population municipale authentifiée par le plus récent décret publié en application de l'article 156 de la loi n° 2002-276 du 27 février 2002 relative à la démocratie de proximité ; / 2° Les communes n'ayant pu bénéficier de la répartition de sièges prévue au 1° du présent IV se voient attribuer un siège, au-delà de l'effectif fixé par le tableau du III ; / 3° Si, après application des modalités prévues aux 1° et 2° du présent IV, une commune obtient plus de la moitié des sièges de l'organe délibérant : / -seul un nombre de sièges portant le nombre total de ses conseillers communautaires à la moitié des sièges de l'organe délibérant, arrondie à l'entier inférieur, lui est finalement attribué ; (...) V.-Dans les communautés de communes et les communautés d'agglomération, si les sièges attribués sur le fondement du 2° du IV excèdent 30% du nombre de sièges définis au deuxième alinéa du III, 10 % du nombre total de sièges issus de l'application des III et IV sont attribués aux communes selon les modalités prévues au IV. Dans ce cas, il ne peut être fait application du VI. (...) " ;<br/>
<br/>
              4. Considérant qu'eu égard au nombre de communes regroupées au sein de la nouvelle communauté de communes et au nombre total d'habitants, la préfète de l'Orne, en l'absence d'accord des communes, a fait application des dispositions des II à V de l'article L. 5211-6-1 du code général des collectivités territoriales cité ci-dessus, en particulier du V en attribuant aux communes, selon les modalités prévues au IV, 10% du nombre total de sièges obtenus par application des III et IV ; que ce total étant égal à 66, le préfet a, sans arrondir à l'entier supérieur, augmenté de 6 le nombre de sièges au conseil de la communauté et attribué ces sièges supplémentaires aux communes à la représentation proportionnelle à la plus forte moyenne sur la base d'un quotient électoral calculé au regard de l'ensemble des sièges déjà attribués et non de ces seuls 10% ; que la commune de Sainte-Gauburge-Sainte-Colombe a, par application de ces différentes dispositions obtenu trois sièges au conseil de la nouvelle communauté de communes ; qu'en procédant ainsi pour la fixation du nombre de membres de ce conseil, la préfète, comme l'a estimé à bon droit le juge des référés de première instance, n'a en tout état de cause pas porté une atteinte manifestement illégale à l'exercice par M. A...de son mandat de conseiller communautaire au sein de la communauté de communes des Vallées du Merlerault ; qu'il en va de même, pour la désignation des trois conseillers de la nouvelle communauté par le conseil municipal de Sainte-Gauburge-Sainte-Colombe ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'il est manifeste que l'appel de M. A...ne peut être accueilli ; qu'il y a lieu, par suite, de rejeter sa requête, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A....<br/>
Copie en sera adressée, pour information, à la préfète de l'Orne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
