<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038524828</ID>
<ANCIEN_ID>JG_L_2019_05_000000411691</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/52/48/CETATEXT000038524828.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 27/05/2019, 411691, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411691</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:411691.20190527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 21 juin 2017 et 10 septembre 2018 au secrétariat du contentieux du Conseil d'État, M. A...B...demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les projets de nomination de magistrats de l'ordre judiciaire portés à sa connaissance les 21 avril et 23 mai 2017, en tant qu'ils ne retiennent pas sa candidature aux fonctions de procureur de la République près les tribunaux de grande instance d'Orléans et de Valenciennes et les décrets du 21 avril et du 6 juillet 2017 en tant qu'ils nomment M. D...C...et M. F...E...dans ces fonctions ;<br/>
<br/>
              2°) d'enjoindre à l'État de tirer toutes les conséquences de l'annulation, notamment en réexaminant son dossier ;<br/>
<br/>
              3°) de mettre à la charge de l'État une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la Constitution, notamment ses articles 64 et 65 ;<br/>
              -	l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              -	la loi organique n° 94-100 du 5 février 1994 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	M. A...B..., procureur de la République adjoint près le tribunal de grande instance de Marseille, demande l'annulation pour excès de pouvoir, d'une part, des projets de nomination de magistrats de l'ordre judiciaire portés à sa connaissance les 21 avril et 23 mai 2017, en tant qu'ils ne retiennent pas sa candidature aux fonctions de procureur de la République près les tribunaux de grande instance d'Orléans et de Valenciennes et, d'autre part, des décrets du 21 avril et du 6 juillet 2017 en tant qu'ils nomment dans ces fonctions respectivement M. C...et M.E....<br/>
<br/>
<br/>
              2.	Il résulte des dispositions des articles 27-1 et 28 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature qu'il appartient au garde des sceaux, ministre de la justice, de communiquer ses projets de nomination aux fonctions du premier ou du second grade à la formation compétente du Conseil supérieur de la magistrature et, après avoir recueilli ces avis, qui doivent être conformes pour les nominations des magistrats du siège, de proposer les nominations de ces magistrats au Président de la République. Le garde des sceaux dispose en la matière, en vertu des dispositions de l'article 28 de l'ordonnance du 22 décembre 1958, d'un large pouvoir d'appréciation en fonction, d'une part, de l'expérience et des aptitudes des intéressés et, d'autre part, des exigences du poste à pourvoir.<br/>
<br/>
              3.	En premier lieu, la proposition de nomination que le garde des sceaux, ministre de la justice, présente au Président de la République constitue un acte préparatoire au décret de nomination. Une telle proposition n'a, dès lors, pas le caractère d'une décision faisant grief susceptible de faire l'objet d'un recours pour excès de pouvoir. Le garde des sceaux est ainsi fondé à soutenir que M. B...n'est pas recevable à demander l'annulation des propositions qu'il a faite au Président de la République, après avis du Conseil supérieur de magistrature, tendant à la nomination de MM. C...etE.... Les conclusions de la requête dirigées contre ces propositions doivent donc être rejetées.<br/>
<br/>
              4.	En second lieu, si M. B...fait valoir, à l'appui de ses conclusions dirigées contre les décrets attaqués, que son ancienneté et son expérience dans la magistrature sont supérieures à celles des deux candidats nommés dans les fonctions de procureur de la République près les tribunaux de grande instance d'Orléans et de Valenciennes, il ressort des pièces du dossier, et notamment de la mesure d'instruction diligentée par la 6e chambre de la section du contentieux, que la nomination des intéressés dans ces fonctions ne résulte pas d'une erreur manifeste d'appréciation eu égard à leurs qualités professionnelles, leur expérience et leur aptitude à satisfaire aux besoins du service. M. B...n'est pas, dès lors, fondé à demander l'annulation des décrets qu'il attaque. <br/>
<br/>
              5.	Il résulte de tout ce qui précède que ses conclusions aux fins d'injonction ne peuvent, par suite, qu'être rejetées ainsi que ses conclusions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
      Copie en sera adressée à M. F...E...et à M. D...C....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
