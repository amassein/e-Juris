<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044553290</ID>
<ANCIEN_ID>JG_L_2021_12_000000442921</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/55/32/CETATEXT000044553290.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/12/2021, 442921, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442921</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROCHETEAU, UZAN-SARANO ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:442921.20211222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Générale de manutention portuaire a demandé au tribunal administratif de Rouen, à titre principal, de condamner le grand port maritime du Havre à lui verser la somme de 42 307 935,70 euros hors taxes à titre de dommages-intérêts avec intérêts au taux légal eux-mêmes capitalisés à compter du 15 septembre 2014, à titre subsidiaire, de condamner le grand port maritime à lui verser la somme de 41 492 767,70 euros à titre de dommages-intérêts avec intérêts au taux légal eux-mêmes capitalisés à compter du 15 septembre 2014, à titre infiniment subsidiaire, de condamner le grand port maritime à lui verser la somme de 5 780 000 euros à titre de dommages-intérêts avec intérêts au taux légal eux-mêmes capitalisés à compter du 15 septembre 2014 et, dans l'hypothèse où il serait fait droit à l'appel en garantie, de condamner en tout état de cause le grand port maritime au paiement solidaire de tous les dommages-intérêts imposés à la société Terminal Porte Océane. <br/>
<br/>
              Par un jugement n° 1403058 du 23 janvier 2018, ce tribunal a condamné le grand port maritime du Havre à verser à la société Générale de manutention portuaire la somme de 1 920 000 euros avec intérêts au taux légal à compter du 15 septembre 2014, les intérêts échus à la date du 15 septembre 2015 puis à chaque échéance annuelle à compter de cette date étant capitalisés à chacune de ces dates pour produire eux-mêmes intérêts, et condamné la société Terminal Porte Océane à garantir le grand port maritime du Havre à hauteur de 70 % de cette condamnation.<br/>
<br/>
              Par un arrêt nos 18DA00631, 18DA00632 du 18 juin 2020, la cour administrative d'appel de Douai, statuant sur les appels formés par la société Générale de manutention portuaire et par la société Terminal Porte Océane ainsi que sur l'appel incident formé par le grand port maritime du Havre, a annulé les articles 1er à 4 de ce jugement et rejeté dans cette mesure la demande de la société Générale de manutention portuaire ainsi que ses conclusions d'appel.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 août 2020, 16 novembre 2020 et 10 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Générale de manutention portuaire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter l'appel de la société Terminal Porte Océane ainsi que l'appel incident formé par le grand port maritime du Havre ;<br/>
<br/>
              3°) de mettre à la charge du Grand port maritime du Havre et de la société Terminal Porte Océane le versement d'une somme globale de 12 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Générale de manutention portuaire, à la SCP Foussard, Froger, avocat de la société Terminal Porte Océane et à la SCP Thouin-Palat, Boucard, avocat du grand port Fluvio-Maritime de l'Axe Seine ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 décembre 2021, présentée par la société Générale de manutention portuaire ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, dans la perspective de la création de nouveaux postes à quai destinés à accueillir des porte-conteneurs dans le cadre du projet Port 2000, le port autonome du Havre a, par des conventions d'exploitation respectivement conclues les 28 octobre 2004 et 11 mai 2006, concédé, d'une part, l'exploitation des postes à quai n° 3, 4 et 5 à la société Générale de manutention portuaire (GMP) et, d'autre part, l'exploitation des postes à quai n° 1, 2 et 6 à la société Terminal Porte Océane (TPO). Par un avenant conclu le 12 septembre 2007, le port autonome du Havre et la société GMP ont convenus de l'extension vers l'ouest du terminal exploité par cette même société, après la libération par la société TPO du poste à quai n° 2 et le déplacement de ses installations portuaires sur le poste à quai n° 6. Alors que l'opération devait aboutir en octobre 2010, la société GMP n'a déplacé ses installations sur le poste n° 2 qu'en mars 2014. La société GMP a demandé au tribunal administratif de Rouen de condamner le grand port maritime du Havre, lequel s'était substitué au port autonome du Havre à compter du 10 octobre 2008, à lui verser, à titre principal, la somme de 42 307 935,70 euros hors taxes en réparation des dommages causés par le retard pris dans le déplacement de ses installations portuaires. Par un jugement du 23 janvier 2018, le tribunal administratif de Rouen a condamné le grand port maritime du Havre à verser à la société GMP la somme de 1 920 000 euros et condamné la société TPO à garantir le grand port maritime du Havre à hauteur de 70 % de cette condamnation. Par un arrêt du 18 juin 2020, contre lequel la société GMP se pourvoit en cassation, la cour administrative d'appel de Douai a annulé ce jugement et rejeté la demande de la société GMP.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que l'avenant à la convention d'exploitation de terminal portant sur les postes à quai n° 3, 4 et 5, conclu le 12 septembre 2007 entre le port autonome du Havre et la société GMP stipule que : " (...) l'objectif d'extension du terminal vers l'Ouest après libération de 350 m de quai entre le pm 2800 et le pm 3150 - poste 2 - par l'opérateur précédent est octobre 2010 et se fera dans les conditions prévues par le cahier des prescriptions techniques de premier établissement. (...) L'entreprise, sollicitant le ripage, négociera, si nécessaire, avec l'opérateur voisin concerné, les conditions économiques de transfert de propriété des aménagements qu'il a réalisés et de prise en charge éventuelle des coûts directs, matériels et certains, qu'il aurait effectivement supportés en raison de ce ripage et ce, sur présentation des factures y afférentes, le tout à l'exception des conséquences qui étaient prévisibles lors de la signature de la convention d'exploitation de terminal et de ses avenants. / 1 - dans l'hypothèse d'un accord entre l'entreprise sollicitant le ripage et l'opérateur voisin, les deux parties établiront un projet de transaction directe qu'elles porteront à la connaissance du port. Un projet d'avenant à la présente convention d'exploitation de terminal sera alors établi.(...) / 2 - dans l'hypothèse où aucun accord n'aurait été trouvé dans un délai de 60 jours entre l'entreprise sollicitant le ripage et l'opérateur voisin concerné (ou un tiers investisseur précédemment désigné), le port et l'opérateur voisin concerné saisiront dans les 30 jours suivant un collège d'experts composé de trois membres, l'opérateur voisin et le Port désignant chacun un expert, les deux experts ainsi désignés choisissant le troisième membre du collège. (...) Ce collège déterminera le montant que devra acquitter l'entreprise pour le transfert de propriété des aménagements que l'opérateur voisin a réalisés et pour un remboursement éventuel et sur présentation de justificatifs des charges non prévisibles qui s'imposeraient à lui en raison de ce ripage. A défaut de désignation d'un expert par l'opérateur voisin dans un délai de 30 jours, les montants seront fixés par l'expert désigné par le port. Les montants ainsi déterminés s'imposeront aux parties concernées. / En cas de désaccord de l'entreprise, sollicitant le ripage, sa demande sera alors rejetée et le dossier sera clos. / En cas d'accord de l'opérateur voisin, l'entreprise sollicitant le ripage et cet opérateur établiront, sur ces bases, un projet de transaction directe qu'ils porteront à la connaissance du port. Un projet d'avenant à la présente convention d'exploitation de terminal sera alors établi. (...) L'entreprise et le Port procéderont à la signature conjointe de l'avenant dans un délai qui ne pourra excéder un mois ". <br/>
<br/>
              3. En premier lieu, en estimant qu'il résultait des stipulations de l'avenant du 12 septembre 2007 que la société GMP et le port autonome du Havre n'avaient pas entendu faire reposer sur ce dernier une obligation de résultat quant à la mise à disposition, en octobre 2010, du poste à quai n° 2 au profit de la société GMP, la cour administrative d'appel de Douai, qui a suffisamment motivé son arrêt sur ce point, n'a pas dénaturé les stipulations litigieuses.<br/>
<br/>
              4. En deuxième lieu, la cour administrative d'appel a relevé, par une appréciation souveraine des faits non entachée de dénaturation, qu'en novembre 2008, les sociétés GMP et TPO étaient parvenues à un accord sur la prise en charge des coûts de l'opération litigieuse, à l'exception d'une somme devant, selon elles, être supportée par le grand port maritime et que, par un courrier du 27 mai 2009, la société GMP avait émis le souhait de poursuivre sa démarche amiable avec la société TPO afin d'éviter la saisine du collège d'experts. En estimant que le grand port maritime du Havre n'avait, dans ces conditions, pas méconnu les obligations résultant de l'avenant du 12 septembre 2007 conclu avec la société GMP en s'abstenant de désigner un expert dans un collège dont le seul rôle était de déterminer le montant que la société GMP devait verser à la société TPO en cas de désaccord, la cour a, par un arrêt suffisamment motivé, porté sur les pièces et faits du dossier une appréciation souveraine exempte de dénaturation. <br/>
<br/>
              5. En troisième lieu, l'exécution d'un contrat administratif par une personne publique ne saurait impliquer que celle-ci soit tenue d'exercer son pouvoir de résiliation unilatérale pour motif d'intérêt général d'un autre contrat administratif conclu avec un autre co-contractant. Par suite, en jugeant, après avoir relevé qu'aucune stipulation contractuelle ne prévoyait la résiliation par le grand port maritime du Havre de la convention de terminal conclue avec la société Terminal Porte Océane en l'absence d'accord entre celle-ci et la société GMP, que cette dernière ne pouvait utilement invoquer, au soutien de sa demande indemnitaire, l'abstention de l'autorité portuaire de mettre en œuvre ses prérogatives de puissance publique pour prononcer la résiliation partielle, pour motif d'intérêt général, de cette convention de terminal, la cour n'a ni insuffisamment motivé son arrêt, ni commis d'erreur de droit ni, en tout état de cause, méconnu les principes de bonne foi et de loyauté contractuelle. <br/>
<br/>
              6. En quatrième lieu, la cour administrative d'appel a relevé, par des motifs de son arrêt non argués de dénaturation que, par une délibération du 24 septembre 2010, le conseil de surveillance du grand port maritime avait décidé que le transfert des installations de la société TPO interviendrait à sa demande pour un motif d'intérêt général, qu'il prendrait à sa charge l'indemnisation du préjudice de la société TPO résultant de cette opération et, enfin, qu'il se rendrait temporairement propriétaire des aménagements effectués par la société TPO sur le poste à quai n° 2, sous réserve de l'engagement irrévocable de la société GMP de lui racheter au même prix les droits réels attachés à ces aménagements. La cour a également relevé que la société GMP avait fait part, par un courrier du 20 octobre 2011, de son engagement ferme et définitif sur l'acquisition de ces aménagements et sur leur prix. En estimant que, par cet acte unilatéral et cette acceptation, les parties avaient conjointement entendu renoncer à l'exécution des engagements fixés par l'avenant du 12 septembre 2017, la cour a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation. Il en résulte que le moyen tiré de ce que la cour aurait commis une erreur de droit, inexactement qualifié les faits de l'espèce et insuffisamment motivé son arrêt en jugeant que le grand port maritime n'avait commis aucune faute contractuelle durant la période postérieure à la délibération du 24 septembre 2010 ne peut qu'être écarté. <br/>
<br/>
              7. Il résulte de ce qui précède que le pourvoi de la société GMP ne peut qu'être rejeté, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société GMP une somme de 3 000 euros à verser, respectivement, au grand port maritime du Havre et à la société TPO au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société GMP est rejeté.<br/>
Article 2 : la société GMP versera la somme de 3 000 euros, d'une part, au grand port maritime du Havre et, d'autre part, à la société TPO au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Générale de manutention portuaire, au grand port maritime du Havre et à la société Terminal Porte Océane.<br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : M. Pierre Collin, président de chambre, présidant ; M. Hervé Cassagnabère, conseiller d'Etat et M. D... A..., maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 22 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Pierre Collin<br/>
 		Le rapporteur : <br/>
      Signé : M. Jean-Marc Vié<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
