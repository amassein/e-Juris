<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143068</ID>
<ANCIEN_ID>JG_L_2020_07_000000424439</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/30/CETATEXT000042143068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 22/07/2020, 424439, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424439</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:424439.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de la Polynésie française d'annuler la décision du 5 avril 2016 par laquelle la directrice des affaires juridiques du centre hospitalier de la Polynésie française a rejeté sa demande tendant au versement d'une indemnité de sujétions spéciales au titre de missions effectuées dans les îles de la Polynésie française et de condamner le centre hospitalier de la Polynésie française à lui verser cette indemnité, assortie des intérêts au taux légal à compter de sa demande. Par un jugement n° 1600387 du 21 février 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA01744 du 21 juin 2018, la cour administrative d'appel de Paris a, sur appel de M. B..., annulé ce jugement, annulé la décision du 5 avril 2016 et condamné le centre hospitalier de Polynésie française à verser à M. B... la somme de 1 262 500 F CFP, assortie des intérêts au taux légal à compter du 17 mars 2016. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 septembre et 24 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de la Polynésie française demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - la loi n° 2004-193 du 27 février 2004 ;<br/>
              - l'arrêté n° 580 du président du gouvernement de la Polynésie française du 5 juillet 1993 ; <br/>
              - la délibération n° 97-153 de l'assemblée de la Polynésie française du 13 août 1997 ;<br/>
              - la délibération n° 2011-66 de l'assemblée de la Polynésie française du 22 septembre 2011 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Colin-Stoclet, avocat du centre hospitalier de la Polynésie française, et à la SCP Lyon-Caen, Thiriez, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., rééducateur au centre hospitalier de la Polynésie française, a demandé à ce centre hospitalier de lui accorder, au titre de missions effectuées dans les îles de la Polynésie française, le bénéfice de l'indemnité de sujétions spéciales. Par une décision du 5 avril 2016, le centre hospitalier de la Polynésie française a rejeté sa demande. Le centre hospitalier se pourvoit en cassation contre l'arrêt du 21 juin 2018 par lequel la cour administrative d'appel de Paris a annulé le jugement du 21 février 2017 du tribunal administratif de la Polynésie française ainsi que la décision du 5 avril 2016 et l'a condamné à verser à M. B... la somme de 1 262 500 francs CFP.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur la délibération du 23 décembre 2014 :<br/>
<br/>
              2. Aux termes de l'article 12 de l'arrêté du 5 juillet 1993 du président du gouvernement de la Polynésie française, relatif aux commissaires du gouvernement et à la force exécutoire des délibérations des établissements publics territoriaux : " Les délibérations des conseils d'administration (...) intervenant dans les matières suivantes : (...) règles relatives à la rémunération du personnel et aux indemnités diverses, sont soumises à l'approbation du conseil des ministres (...) ". Aux termes de l'article 15 de ce même arrêté : " Le conseil des ministres approuve et rend exécutoires, par arrêté (...) les délibérations énumérées au premier alinéa de l'article 12 (...) ". Enfin, aux termes de son article 16 : " Les arrêtés rendant exécutoires les délibérations des conseils d'administration sont publiés en extrait au Journal officiel de la Polynésie française avec, le cas échéant, le texte desdites délibérations. A la diligence du commissaire de gouvernement ou, à défaut, du directeur de l'établissement, les textes des délibérations exécutoires de plein droit dans les conditions prévues aux articles 12, premier alinéa, et 13 ci-dessus, sont adressés au secrétaire général du gouvernement pour être, le cas échéant, publiés au Journal officiel de la Polynésie française (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que l'indemnité de sujétions spéciales a été instituée par une délibération du 13 août 1997 de l'assemblée de la Polynésie française au bénéfice, dans sa version applicable au litige, de certains personnels de l'administration de la Polynésie française et de ses établissements publics. Par une délibération du 9 août 2005 du conseil d'administration du centre hospitalier de la Polynésie française, elle a été rendue applicable à certains personnels de ce centre hospitalier, puis étendue aux psychologue cliniciens par une délibération du 23 décembre 2014, approuvée par un arrêté du conseil des ministres de Polynésie française du 19 février 2015 et publiée, en même temps que cet arrêté, au Journal officiel de la Polynésie française le 27 février 2015.<br/>
<br/>
              4. S'il résulte des dispositions citées au point 2 que l'approbation de la délibération du 23 décembre 2014 par le conseil des ministres de la Polynésie française a eu pour effet de rendre cette délibération exécutoire, elle ne pouvait toutefois, eu égard à son caractère réglementaire et en l'absence de texte en disposant autrement, entrer en vigueur qu'à compter de sa publication. Par suite, en jugeant que cette délibération était entrée en vigueur à la date du 23 décembre 2014, à laquelle elle avait été rendue exécutoire, alors qu'il ressortait des pièces du dossier qui lui était soumis qu'elle n'avait fait l'objet d'aucune mesure de publicité avant sa publication, le 27 février 2015, au Journal officiel de la Polynésie française, la cour administrative d'appel de Paris a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur la responsabilité pour faute du centre hospitalier : <br/>
<br/>
              5. En jugeant que la minoration prévue par le deuxième alinéa de l'article 1er de la délibération du 22 septembre 2011 de l'assemblée de la Polynésie française, qui n'est applicable qu'aux agents de l'administration de la Polynésie française, n'était pas applicable aux agents des établissements publics de la Polynésie française et, notamment, pas à ceux du centre hospitalier requérant, la cour administrative d'appel n'a pas commis d'erreur de droit.<br/>
<br/>
              6. Elle n'a pas davantage commis d'erreur de droit et n'a pas méconnu son office en se fondant, pour déterminer le montant de l'indemnité due à M. B..., sur le nombre de jours de mission invoqué par celui-ci, qui ressortait de manière constante des pièces du dossier et n'était pas contesté devant elle. <br/>
<br/>
              7. Il résulte de tout ce qui précède que l'arrêt attaqué doit être annulé en tant seulement qu'il annule le jugement du 21 décembre 2017 du tribunal administratif de la Polynésie française en tant que celui-ci rejette les conclusions tendant à l'annulation pour excès de pouvoir de la décision du 5 avril 2016 de la directrice des affaires juridiques et des droits des patients du centre hospitalier de la Polynésie française relative à l'application de la délibération du 23 décembre 2014, en tant qu'il annule cette décision et en tant qu'il condamne le centre hospitalier à payer, en application de cette délibération, l'indemnité de sujétion spéciale pour les missions assurées par M. B... du 23 décembre 2014 au 27 février 2015. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Polynésie française la somme de 3 000 euros à verser à M. B... au titre de l'article L.761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise, au même titre, à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 juin 2018 de la cour administrative d'appel de Paris est annulé en tant, premièrement, qu'il il annule le jugement du 21 décembre 2017 du tribunal administratif de la Polynésie française en tant qu'il rejette les conclusions tendant à l'annulation de la décision du 5 avril 2016 de la directrice des affaires juridiques et des droits des patients du centre hospitalier de la Polynésie française relative à l'application de la délibération du 23 décembre 2014, en tant, deuxièmement, qu'il annule cette décision et, enfin, en tant qu'il condamne le centre hospitalier à payer, en application de cette délibération, l'indemnité de sujétion spéciale pour les missions assurées par M. B... du 23 décembre 2014 au 27 février 2015.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : Le centre hospitalier de la Polynésie française versera 3 000 euros à M. B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée au centre hospitalier de Polynésie française et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
