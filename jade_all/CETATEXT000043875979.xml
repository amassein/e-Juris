<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043875979</ID>
<ANCIEN_ID>JG_L_2021_07_000000454754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/59/CETATEXT000043875979.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/07/2021, 454754, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:454754.20210726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Sous le n° 454754, par une requête et un mémoire en réplique, enregistrés les 20 et 23 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B..., la Fédération nationale des entreprises des activités physiques de loisirs (ACTIVE-FNEAPL), l'Association française des espaces de loisirs indoor (SPACE) et le Syndicat des loisirs actifs (SLA) demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-955 du 19 juillet 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 304,96 euros au titre de l'article L. 761-1 du code de justice administrative, au profit de M. B....<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, le décret contesté porte une atteinte grave et immédiate aux intérêts moraux des personnes physiques, en ce que, d'une part, il les oblige à présenter un " passe sanitaire " pour accéder à de nombreux lieux qu'elles fréquentent et, d'autre part, elles n'ont pas été en mesure de se faire vacciner depuis l'annonce du Président de la République du 12 juillet 2021, en deuxième lieu, ce décret occasionnera une perte substantielle de chiffre d'affaires aux adhérents et intérêts professionnels représentés par les personnes morales requérantes, en troisième lieu, la réglementation antérieure au décret attaqué permettait de maîtriser la situation épidémiologique et, en dernier lieu, le décret n'a vocation à s'appliquer que pour une durée de quelques jours, jusqu'à ce que le Parlement, saisi le 19 juillet par le Conseil des ministres, adopte une loi étendant le champ d'application du " passe sanitaire " ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - le décret attaqué méconnaît en effet les dispositions du 2° du A du II de l'article 1er de la loi n° 2021-689 du 31 mai 2021 qui, ainsi que l'ont évoqué de manière récurrente les parlementaires et le Gouvernement, ont fixé à un chiffre pivot de mille personnes le seuil de fréquentation limite à partir duquel l'usage du " passe sanitaire " pouvait être imposé ;<br/>
              - il porte atteinte au principe de stricte proportionnalité dès lors que la situation épidémiologique est globalement stable eu égard au faible taux de positivité des tests et au nombre de décès et d'hospitalisations, que le nombre de vaccinations est en forte hausse, qu'aucune évaluation ne permet de mesurer les conséquences positives et négatives de l'extension du " passe sanitaire " et, en dernier lieu, que le décret contesté se borne à viser les " circonstances exceptionnelles " et l'article L. 3131-15 du code de santé publique, alors que l'état d'urgence sanitaire n'est plus en vigueur en métropole depuis le 1er juin 2021 ; <br/>
              - ce décret méconnaît le principe de sécurité juridique en ce qu'il ne prévoit pas de période transitoire avant son entrée en vigueur permettant, d'une part, aux particuliers de bénéficier d'une vaccination complète à deux doses avant l'expiration du délai d'application effective de dix jours du décret contesté et, d'autre part, aux établissements concernés par le " passe sanitaire " de s'organiser pour sa mise en oeuvre.<br/>
<br/>
              Par un mémoire en défense, enregistré le 22 juillet 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que les moyens soulevés ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2021-689 du 31 mai 2021 ; <br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le décret n°2021-724 du 7 juin 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... et autres, et d'autre part, le Premier ministre, le ministre de l'intérieur, la ministre de la culture, le ministre des solidarités et de la santé et le ministre des outre-mer ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 23 juillet 2021, à 15 heures : <br/>
<br/>
              - M. B... et la représentante de l'Association française des espaces de loisirs indoor ; <br/>
              - les représentants du ministre de la santé et des solidarités ; <br/>
<br/>
              à l'issue de laquelle la juge des référés a prononcé la clôture de l'instruction. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur les circonstances et le cadre du litige :<br/>
<br/>
              2. En raison de l'amélioration progressive de la situation sanitaire, les mesures de santé publique destinées à prévenir la circulation du virus de la Covid 19 prises dans le cadre de l'état d'urgence sanitaire ont été remplacées, après l'expiration de celui-ci le 1er juin 2021, par celles de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire et du décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de la crise sanitaire. <br/>
<br/>
              3. En vertu du A du II de l'article 1er de la loi du 31 mai 2021, le Premier ministre peut, à compter du 2 juin 2021 et jusqu'au 30 septembre 2021, imposer la présentation du résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la Covid 19, un justificatif de statut vaccinal concernant la Covid 19 ou un certificat de rétablissement à la suite d'une contamination par la Covid 19, d'une part, aux personnes souhaitant se déplacer à destination ou en provenance du territoire hexagonal, de la Corse ou de l'une des collectivités mentionnées à l'article 72-3 de la Constitution et, d'autre part, aux personnes souhaitant accéder à certains lieux, établissements ou événements impliquant de grands rassemblements de personnes pour des activités de loisirs ou des foires ou salons professionnels. <br/>
<br/>
              4. Le décret du 7 juin 2021 a modifié le décret du 1er juin 2021 pris en application de cette loi, afin d'imposer la présentation du passe sanitaire pour les déplacements mentionnés au titre 2 bis de ce décret et pour l'accès aux établissements, lieux et évènements mentionnés au chapitre 7 de son titre 4 et d'en déterminer les modalités d'utilisation. Pour les établissements, lieux et évènements énumérés au II de l'article 47-1 issu de ce décret, l'accès des personnes âgées de onze ans et plus est subordonné, lorsque ces établissements, lieux et évènements accueillent un nombre de visiteurs ou de spectateurs au moins égal à mille personnes, à la présentation de l'un des documents énumérés au I du même article, à savoir " 1° Le résultat d'un test ou examen de dépistage mentionné au 1° de l'article 2-2 réalisé moins de 48 heures avant l'accès à l'établissement, au lieu ou à l'évènement. Les seuls tests antigéniques pouvant être valablement présentés pour l'application du présent 1° sont ceux permettant la détection de la protéine N du SARS-CoV-2 ; 2° Un justificatif du statut vaccinal délivré dans les conditions mentionnées au 2° de l'article 2-2 ; 3° Un certificat de rétablissement délivré dans les conditions mentionnées au 3° de l'article 2-2  /... ". Le dernier alinéa du I de l'article 47-1 précise qu'à défaut de présentation de l'un de ces documents, l'accès à l'établissement, au lieu ou à l'évènement est refusé.<br/>
<br/>
              5. Le décret litigieux du 19 juillet 2021 a modifié les dispositions de l'article 47-1 du décret du 1er juin 2021 afin d'étendre à plusieurs nouvelles catégories d'établissements l'obligation de présenter les documents énumérés au I du même article, faute de quoi l'accès à ces établissements est refusé, et d'abaisser le seuil d'application de cette limitation de l'accès à l'accueil de 50 personnes. <br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              6. Les requérants demandent la suspension du décret du 19 juillet 2021 en tant qu'il a abaissé à 50 personnes le seuil au-delà duquel l'accès aux catégories d'établissements énumérées au II de l'article 47-1 du décret du 1er juin 2021 est subordonné à la présentation d'un des documents énumérés au I du même article.<br/>
<br/>
              7. En premier lieu, le Premier ministre peut, en vertu de ses pouvoirs propres, édicter des mesures de police applicables à l'ensemble du territoire, en particulier en cas de circonstances exceptionnelles, telle une épidémie avérée, à l'instar de celle de la Covid 19. La circonstance qu'un régime législatif ait été institué pour gérer une telle crise ne fait pas obstacle à ce que le Premier ministre prenne les mesures appropriées, dans le cadre de son pouvoir de police générale, dans le cas où le régime institué ne permet pas de répondre à une situation d'urgence mettant en danger de manière imminente la santé de la population, en particulier dans l'intervalle nécessaire à l'adoption d'un nouveau cadre législatif. <br/>
<br/>
              8. Il résulte des données scientifiques disponibles qu'à la date du 21 juillet 2021 la situation sanitaire s'est de nouveau dégradée en raison de la diffusion croissante du variant Delta du virus de la Covid 19 sur le territoire, avec près de 80,2 % des tests révélant sa présence, et que la transmissibilité de ce virus est augmentée de 60 % par rapport au variant Alpha. A la date du décret litigieux, le taux d'incidence était marqué par une forte augmentation de la circulation du virus (+ 111 % sur la période du 11 au 17 juillet 2021 par rapport à la période du 4 au 10 juillet, + 244 % par rapport à la période du 27 juin au 3 juillet). Au 21 juillet 2021 le taux d'incidence était de 98,2 pour 100 000 habitants, soit + 143 % par rapport à la semaine du 5 au 11 juillet. Le nombre des entrées à l'hôpital et des admissions en services de soins critiques a augmenté de respectivement 57 % et 67 % pour la semaine du 14 au 21 juillet par rapport à la semaine précédente. Ces données, qui montrent une dégradation de la situation sanitaire au cours de la période très récente, pourraient se révéler encore plus préoccupantes au début du mois d'août, selon les modélisations de l'Institut Pasteur, dans le contexte de diffusion de ce variant, ceci alors que la couverture vaccinale de la population dont, au 20 juillet 2021, seule 46,4 % avait reçu un schéma vaccinal complet, n'est pas suffisante pour conduire à un reflux durable de l'épidémie. <br/>
<br/>
              9. Par suite, le moyen tiré de ce que le décret serait illégal car il méconnaîtrait les dispositions de l'article 1er de la loi du 31 mai 2021 en ce qu'il a abaissé le seuil d'application du passe sanitaire à 50 personnes alors que la loi n'a prévu la possibilité de subordonner l'accès à certains lieux, établissements ou évènements à la présentation du passe sanitaire que s'ils accueillent de grands rassemblements de personnes, n'est pas de nature, en l'état de l'instruction, à faire naître un doute sérieux sur sa légalité, le décret attaqué ayant été pris au vu des circonstances exceptionnelles liées à la reprise de l'épidémie décrites au point 8 ci-dessus.<br/>
<br/>
              10. En deuxième lieu, le moyen tiré de ce que le décret méconnaîtrait les principes de nécessité et de proportionnalité n'est pas non plus de nature à faire naître un doute sérieux sur la légalité du décret alors que, ainsi qu'il a été dit ci-dessus, la reprise de l'épidémie liée à la diffusion du variant Delta était de nature à justifier un élargissement de l'utilisation du passe sanitaire.<br/>
<br/>
              11. En troisième lieu, il en va de même du moyen tiré de ce que le décret méconnaîtrait le principe de sécurité juridique faute d'avoir assorti les mesures édictées d'un différé d'entrée en vigueur, les circonstances exceptionnelles dans lesquelles le décret est intervenu commandant une application rapide des dispositions contestées.<br/>
<br/>
              12. Il résulte de tout ce qui précède que les requérants ne font état d'aucun moyen propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité du décret qu'ils attaquent. Par suite, et sans qu'il soit besoin de se prononcer sur la condition d'urgence, leur demande de suspension doit être rejetée.<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B..., premier requérant dénommé, et au ministre de la santé et des solidarités.<br/>
Copie en sera adressée au Premier ministre, au ministre de l'intérieur, au ministre de la culture, ainsi qu'au ministre des Outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
