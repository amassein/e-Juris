<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032613685</ID>
<ANCIEN_ID>JG_L_2016_05_000000387338</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/61/36/CETATEXT000032613685.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 30/05/2016, 387338, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387338</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387338.20160530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 30 novembre 2011 de l'inspecteur du travail de la 7ème section de l'unité territoriale du Calvados autorisant la société Brocéliande-ALH à la licencier. Par un jugement n° 1200084 du 28 mars 2013, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 13NT01419 du 9 octobre 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé par Mme A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 22 janvier et 23 avril 2015, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de la société Brocéliande-ALH la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la décision du 29 juin 2015 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par MmeA... ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de Mme A...et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Brocéliande-ALH ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 1226-2 du code du travail : " Lorsque, à l'issue des périodes de suspension du contrat de travail consécutives à une maladie ou un accident non professionnel, le salarié est déclaré inapte par le médecin du travail à reprendre l'emploi qu'il occupait précédemment, l'employeur lui propose un autre emploi approprié à ses capacités. / Cette proposition prend en compte les conclusions écrites du médecin du travail et les indications qu'il formule sur l'aptitude du salarié à exercer l'une des tâches existantes dans l'entreprise. / L'emploi proposé est aussi comparable que possible à l'emploi précédemment occupé, au besoin par la mise en oeuvre de mesures telles que mutations, transformations de postes de travail ou aménagement du temps de travail " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un avis du 13 mai 2011, le médecin du travail a déclaré que MmeA..., salariée de la société Brocéliande-ALH ayant la qualité de membre suppléant du comité d'établissement, était inapte à reprendre son poste ou tout autre dans l'entreprise mais apte à accomplir " des tâches de manutention, de vente, de secrétariat, de conduite de véhicule " dans tout autre établissement du groupe Cooperl Arc Atlantique auquel appartient la société Brocéliante-ALH ; que cette société a proposé à MmeA..., qui les a tous refusés, trente-cinq postes différents sur sept sites des entreprises de ce groupe ; que l'inspecteur du travail de la 7ème section de l'unité territoriale du Calvados a, au vu de ces refus, autorisé son licenciement par une décision du 30 novembre 2011 ; que Mme A...se pourvoit en cassation contre l'arrêt du 9 octobre 2014 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre le jugement du tribunal administratif de Caen qui a rejeté sa demande d'annulation de cette décision ; <br/>
<br/>
              3. Considérant qu'en vertu du code du travail, les salariés protégés bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; que lorsque le licenciement de l'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale ; que, dans le cas où la demande de licenciement est motivée par l'inaptitude physique, il appartient à l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que l'employeur a, conformément aux dispositions citées ci-dessus de l'article L. 1226-2 du code du travail, cherché à reclasser le salarié sur d'autres postes appropriés à ses capacités, le cas échéant par la mise en oeuvre, dans l'entreprise, de mesures telles que mutations ou transformations de postes de travail ou aménagement du temps de travail ; que le licenciement ne peut être autorisé que dans le cas où l'employeur n'a pu reclasser le salarié dans un emploi approprié à ses capacités au terme d'une recherche sérieuse, menée tant au sein de l'entreprise que dans les entreprises dont l'organisation, les activités ou le lieu d'exploitation permettent, en raison des relations qui existent avec elles, d'y effectuer la permutation de tout ou partie de son personnel ;<br/>
<br/>
              4. Considérant, par suite, qu'en jugeant que l'employeur avait satisfait à son obligation de reclassement du seul fait qu'il avait proposé à l'intéressée au moins un emploi compatible avec les préconisations du médecin du travail, alors que, ainsi qu'il a été dit au point 3, il lui appartenait d'apprécier, au vu des éléments qui lui étaient soumis, si les postes proposés à Mme A...étaient, compte tenu des possibilités existant au sein de la société Brocéliande-ALH et des autres sociétés du groupe Cooperl Arc Atlantique ainsi que des motifs de refus avancés par MmeA..., de nature à caractériser une recherche sérieuse de reclassement, la cour administrative d'appel de Nantes a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme A...est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de MmeA..., qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que demande à ce titre la société Brocéliande-ALH ; que dans les circonstances de l'espèce, il y a lieu, au même titre, de mettre à la charge de la société Brocéliande-ALH le versement à Mme A...d'une somme de 3 500 euros ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt du 9 octobre 2014 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
Article 3 : Les conclusions de la société Brocéliande-ALH présentées au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La société Brocéliande-ALH versera à Mme A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A..., à la société Brocéliande-ALH et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-035-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. MOTIFS AUTRES QUE LA FAUTE OU LA SITUATION ÉCONOMIQUE. INAPTITUDE ; MALADIE. - OBLIGATION DE RECLASSEMENT - OBLIGATION D'UNE RECHERCHE SÉRIEUSE [RJ1] AU SEIN DU GROUPE DE RECLASSEMENT, APPRÉCIÉE COMPTE TENU DES POSSIBILITÉS ET DES MOTIFS DE REFUS DU SALARIÉ [RJ2].
</SCT>
<ANA ID="9A"> 66-07-01-04-035-02 Dans le cas où la demande de licenciement d'un salarié protégé est motivée par l'inaptitude physique, il appartient à l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que l'employeur a, conformément aux dispositions de l'article L. 1226-2 du code du travail, cherché à reclasser le salarié sur d'autres postes appropriés à ses capacités, le cas échéant par la mise en oeuvre, dans l'entreprise, de mesures telles que mutations ou transformations de postes de travail ou aménagement du temps de travail. Le licenciement ne peut être autorisé que dans le cas où l'employeur n'a pu reclasser le salarié dans un emploi approprié à ses capacités au terme d'une recherche sérieuse, menée tant au sein de l'entreprise que dans les entreprises dont l'organisation, les activités ou le lieu d'exploitation permettent, en raison des relations qui existent avec elles, d'y effectuer la permutation de tout ou partie de son personnel [RJ3].... ,,Une cour administrative d'appel commet une erreur de droit en jugeant que l'employeur a satisfait à son obligation du seul fait qu'il a proposé à l'intéressé au moins un emploi compatible avec les préconisations du médecin du travail, alors qu'il lui appartenait d'apprécier si les postes proposés étaient, compte tenu des possibilités existant au sein de la société et du groupe  ainsi que des motifs de refus avancés par le salarié, de nature à caractériser une recherche sérieuse de reclassement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 2 février 1996, M. Breuil, n° 157713, T. p. 1192 ; CE, 30 décembre 1996, M. Porras et ministre du travail, n°s 163746 164472, T. pp. 1190-1192 ; CE, 30 avril 1997, M. Perre, n° 158474, T. p.  1107 ; Cass. soc., 27 octobre 1993, n° 90-42.560, Bull. civ. V n° 250 ; Cass. soc., 9 mai 1995, n° 91-43.749, Bull. civ. V n° 149., ,[RJ2] Comp., sur l'appréciation du sérieux de la recherche, Cass. soc., 30 novembre 2010, n° 09-66.687, Bull. civ. V n° 271., ,[RJ3] Rappr., s'agissant du groupe de reclassement pour un licenciement économique, CE, 9 mars 2016, Société Etudes techniques Ruiz, n° 384175, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
