<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487263</ID>
<ANCIEN_ID>JG_L_2021_12_000000458635</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 03/12/2021, 458635, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>458635</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:458635.20211203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Assistance publique - hôpitaux de Marseille (AP-HM), d'une part, de le laisser reprendre ses fonctions et percevoir sa rémunération et, d'autre part, de lui accorder sa mise en télétravail, sans condition de vaccination, de prendre toute mesure d'organisation des locaux et de leur accès aux fins d'interdire l'accès aux locaux de la direction des services numériques à toute personne étrangère au service jusqu'à la fin de la situation d'urgence sanitaire, ainsi que condamner temporairement les éventuelles portes de communication avec les autres services.<br/>
<br/>
              Par une ordonnance n° 2109172 du 28 octobre 2021, le juge des référés du tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 23 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) à titre principal, d'enjoindre à l'AP-HM de le laisser reprendre ses fonctions et percevoir sa rémunération ;<br/>
<br/>
              3°) à titre subsidiaire, d'enjoindre à l'AP-HM de lui accorder sa mise en télétravail ;<br/>
<br/>
              4°) de mettre à la charge de l'AP-HM la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable dès lors que, d'une part, elle a été introduite dans le délai de recours contentieux et, d'autre part, il justifie d'un intérêt à agir ;<br/>
              - la condition d'urgence est satisfaite dès lors qu'il est suspendu de l'exercice de ses fonctions professionnelles et syndicales et privé de sa rémunération sans limitation de durée ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'expression et à la liberté d'opinion ; <br/>
              - la mesure de l'AP-HM méconnaît le décret n° 2021-699 du 1er juin 2021 dès lors que le personnel de la direction des services numériques, qui dispose de ses propres locaux et ne reçoit pas de patients ou de professionnels de santé, n'est pas soumis à l'obligation vaccinale ;<br/>
              - elle porte une atteinte disproportionnée à la liberté d'expression et à la liberté d'opinion dès lors que, en premier lieu, elle s'applique immédiatement et sans limitation de durée, en deuxième lieu, il n'a pas été convoqué pour un entretien trois jours après le début de la suspension de son contrat de travail et, en dernier lieu, la vaccination n'empêche pas de transmettre la Covid-19 ;<br/>
              - elle est entachée d'une erreur de droit dès lors qu'elle suspend la totalité de sa rémunération et l'empêche de conserver le bénéfice des garanties de protection sociale complémentaire, qui est une mesure d'ordre public ; <br/>
              - elle est entachée d'un détournement de pouvoir dès lors que l'objectif poursuivi n'est pas la lutte contre la propagation du virus mais la prise de mesures de réorganisation interne.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 86-635 du 13 juillet 1983 ;<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - le décret n° 2021-699 du 1er juin 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              2. En raison de l'amélioration progressive de la situation sanitaire, les mesures de santé publique destinées à prévenir la circulation du virus de la covid-19 prises dans le cadre de l'état d'urgence sanitaire ont été remplacées, après l'expiration de celui-ci le 1er juin 2021, par celles de la loi du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire. Mais après une diminution de l'épidémie, la situation sanitaire, à partir du mois de juin 2021, s'est dégradée du fait de la diffusion croissante du variant Delta qui présente une transmissibilité augmentée de 60 % par rapport au variant Alpha, avec une sévérité au moins aussi importante. Au 21 juillet 2021, le taux d'incidence était de 98,2 pour 100 000 habitants, soit une augmentation de 143 % par rapport à la semaine du 5 au 11 juillet alors que les admissions en service de soins critiques augmentaient de 76 %. Au regard de cette évolution de la situation épidémiologique et alors que la couverture vaccinale de la population, au 20 juillet 2021, n'était que de 46,4%, soit un taux insuffisant pour conduire à un reflux durable de l'épidémie, la loi du 31 mai 2021 a été modifiée et complétée par la loi du 5 août 2021 relative à la gestion de la crise sanitaire. Ses articles 12 à 19 ont institué une obligation de vaccination pour un certain nombre de professionnels. <br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              3. Aux termes de l'article 12 de la loi du 5 août 2021 relative à la gestion de la crise sanitaire : " I. - Doivent être vaccinés, sauf contre-indication médicale reconnue, contre la covid-19 : 1° Les personnes exerçant leur activité dans : a) Les établissements de santé mentionnés à l'article L. 6111-1 du code de la santé publique ainsi que les hôpitaux des armées mentionnés à l'article L. 6147-7 du même code (...); III. - Le I ne s'applique pas aux personnes chargées de l'exécution d'une tâche ponctuelle au sein des locaux dans lesquels les personnes mentionnées aux 1°, 2°, 3° et 4° du même I exercent ou travaillent ". Son article 13 dispose que " I. - Les personnes mentionnées au I de l'article 12 établissent : 1° Satisfaire à l'obligation de vaccination en présentant le certificat de statut vaccinal prévu au second alinéa du II du même article 12 (...). 2° Ne pas être soumises à cette obligation en présentant un certificat médical de contre-indication (...) ".  Aux termes du I B de l'article 14 de la même loi : " A compter du 15 septembre 2021, les personnes mentionnées au I de l'article 12 ne peuvent plus exercer leur activité si elles n'ont pas présenté les documents mentionnés au I de l'article 13 ou, à défaut, le justificatif de l'administration des doses de vaccins requises par le décret mentionné au II de l'article 12 " et aux termes de son III " Lorsque l'employeur constate qu'un agent public ne peut plus exercer son activité en application du I, il l'informe sans délai des conséquences qu'emporte cette interdiction d'exercer sur son emploi ainsi que des moyens de régulariser sa situation. L'agent public qui fait l'objet d'une interdiction d'exercer peut utiliser, avec l'accord de son employeur, des jours de congés payés. A défaut, il est suspendu de ses fonctions ou de son contrat de travail. La suspension mentionnée au premier alinéa du présent III, qui s'accompagne de l'interruption du versement de la rémunération, prend fin dès que l'agent public remplit les conditions nécessaires à l'exercice de son activité prévues au I. Elle ne peut être assimilée à une période de travail effectif pour la détermination de la durée des congés payés ainsi que pour les droits acquis par l'agent public au titre de son ancienneté. Pendant cette suspension, l'agent public conserve le bénéfice des garanties de protection sociale complémentaire auxquelles il a souscrit (...) ". <br/>
<br/>
              4. D'une part, l'article 12 de la loi du 5 août 2021 a défini le champ de l'obligation de vaccination contre la covid-19 en retenant, notamment, un critère géographique pour y inclure toutes les personnes exerçant leur activité dans un certain nombre d'établissements, principalement les établissements de santé et des établissements sociaux et médico-sociaux. Le législateur a ainsi entendu protéger les personnes accueillies par ces établissements qui présentent une vulnérabilité particulière au virus de la covid-19. C'est pourquoi l'obligation de vaccination concerne aussi des personnels, notamment administratifs, qui ne sont pas en contact direct avec les malades, y compris s'ils exercent des responsabilités syndicales, dès lors qu'ils entretiennent nécessairement, eu égard à leur lieu de travail, des interactions avec des professionnels de santé en contact avec ces derniers. Il s'ensuit que, eu égard à la gravité de l'épidémie que connaît le territoire, l'extension du champ de l'obligation de vaccination imposée par la loi du 5 août 2021 à l'ensemble des personnels d'un établissement de santé entrant dans le champ du I 1° de son article 12, y compris ceux y exerçant une activité syndicale, ne saurait être regardée comme portant une atteinte disproportionnée à la liberté d'opinion et la liberté d'expression garanties par la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              5. D'autre part, il résulte de l'instruction devant le juge des référés du tribunal administratif que M. B..., agent de l'Assistance publique - Hôpitaux de Marseille, exerce les fonctions d'ingénieur en chef au sein de la direction des services numériques et est représentant syndical. Si la direction des services numériques occupe un bâtiment qui lui est dédié et dont l'accès est réservé aux agents qui y sont affectés, elle se situe néanmoins au sein d'un établissement de santé, à proximité immédiate de l'unité de psychiatrie. Il s'ensuit que M. B... entre dans le champ de l'obligation vaccinale prévue par les dispositions du 1° du I de l'article 12 de la loi du 5 août 2021, citées au point 3, sans pouvoir être regardé comme se bornant à exercer une tâche ponctuelle au sens du III du même article. Dès lors, en décidant, le 16 septembre 2021, de le suspendre de ses fonctions jusqu'à ce qu'il produise un justificatif de vaccination ou de contre-indication à la vaccination, mais contrairement à ce que soutient le requérant, sans le priver du bénéfice de la protection sociale, le directeur général de l'Assistance publique - Hôpitaux de Marseille n'a porté aucune atteinte grave et manifestement illégale à une liberté fondamentale. M. B..., qui ne saurait utilement se prévaloir de ce que la mesure dont il a fait l'objet s'applique immédiatement et sans limitation de durée, n'a pas été précédée d'un entretien préalable et aurait en réalité pour objet la réorganisation du service, n'est, par suite, pas fondé soutenir que c'est à tort que par l'ordonnance attaquée, le juge des référés du tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              6. Il résulte de tout ce qui précède que la requête de M. B... doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du même code. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
Copie en sera adressée au ministre des solidarités et de la santé et à l'Assistance publique des Hôpitaux de Marseille. <br/>
Fait à Paris, le 3 décembre 2021<br/>
Signé : Nathalie Escaut<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
