<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783363</ID>
<ANCIEN_ID>JG_L_2018_12_000000419964</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783363.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 07/12/2018, 419964, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419964</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:419964.20181207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...A..., épouse B...a demandé au tribunal administratif de Grenoble d'annuler le titre exécutoire émis le 9 février 2018 par le département de la Drôme en vue du paiement de la somme de 1 400 euros, correspondant à une fraction des frais d'hébergement de son père au sein d'un établissement d'hébergement pour personnes âgées dépendantes (EHPAD) mise à sa charge au titre de son obligation alimentaire. <br/>
<br/>
              Par une ordonnance n° 1801276 du 18 avril 2018, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Grenoble a transmis au Conseil d'Etat, en application de l'article R. 351-3 du code de justice administrative, le dossier de la requête de MmeA....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2016-1547 du 18 novembre 2016 ;<br/>
              - le décret n° 2015-233 du 27 février 2015 ;<br/>
              - le décret n° 2018-928 du 29 octobre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 351-3 du code de justice administrative : " Lorsqu'une cour administrative d'appel ou un tribunal administratif est saisi de conclusions qu'il estime relever de la compétence d'une juridiction administrative autre que le Conseil d'Etat, son président, ou le magistrat qu'il délègue, transmet sans délai le dossier à la juridiction qu'il estime compétente. / Toutefois, en cas de difficultés particulières, il peut transmettre sans délai le dossier au président de la section du contentieux du Conseil d'Etat qui règle la question de compétence et attribue le jugement de tout ou partie de l'affaire à la juridiction qu'il déclare compétente. (...) ". Sur le fondement de ces dispositions, le président du tribunal administratif de Grenoble a transmis au Conseil d'Etat le dossier de la requête de Mme A...tendant à l'annulation du titre exécutoire émis à son encontre le 9 février 2018 par le département de la Drôme, pour le paiement de la somme de 1 400 euros correspondant à une part des frais d'hébergement de son père au sein d'un établissement d'hébergement pour personnes âgées dépendantes du 1er octobre 2017 au 31 janvier 2018, mise à sa charge au titre de son obligation alimentaire à la suite de jugements rendus les 11 juin 2014 et 21 décembre 2016 par le juge aux affaires familiales du tribunal de grande instance de Valence, saisi sur le fondement de l'article L. 132-7 du code de l'action sociale et des familles. <br/>
<br/>
              2. En l'absence de dispositions législatives contraires, il appartient à la juridiction administrative de connaître, sous réserve, le cas échéant, des questions préjudicielles à l'autorité judiciaire pouvant tenir notamment à l'obligation alimentaire, des contestations relatives au recouvrement des sommes demandées à des particuliers, en raison des dépenses exposées par une collectivité publique au titre de l'aide sociale, que ces contestations mettent en cause les bénéficiaires de l'aide sociale eux-mêmes ou d'autres personnes, en particulier leurs obligés alimentaires. Au sein de la juridiction administrative, cette compétence relève, jusqu'au 31 décembre 2018, pour les prestations d'aide sociale entrant dans le champ de l'article L. 134-1 du code de l'action sociale et des familles, des commissions départementales d'aide sociale en premier ressort et de la Commission centrale d'aide sociale en appel. <br/>
<br/>
              3. Toutefois, aux termes de l'article L. 134-3 du code de l'action sociale et des familles, dans sa rédaction issue de la loi du 18 novembre 2016 de modernisation de la justice du XXIe siècle, applicable à compter du 1er janvier 2019 y compris aux affaires en cours à cette date devant les commissions départementales d'aide sociale, en vertu des dispositions combinées de l'article 114 de cette loi et de l'article 17 du décret du 29 octobre 2018 relatif au contentieux de la sécurité sociale et de l'aide sociale : " Le juge judiciaire connaît des contestations formées contre les décisions relatives à : (...) 4° Les recours exercés par l'Etat ou le département en présence d'obligés alimentaires prévus à l'article L. 132-6 ".<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 35 du décret du 27 février 2015 relatif au Tribunal des conflits et aux questions préjudicielles : " Lorsqu'une juridiction est saisie d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse et mettant en jeu la séparation des ordres de juridiction, elle peut, par une décision motivée qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence ".<br/>
<br/>
              5. L'article L. 132-6 du code de l'action sociale et des familles dispose que : " Les personnes tenues à l'obligation alimentaire instituée par les articles 205 et suivants du code civil sont, à l'occasion de toute demande d'aide sociale, invitées à indiquer l'aide qu'elles peuvent allouer aux postulants et à apporter, le cas échéant, la preuve de leur impossibilité de couvrir la totalité des frais. (...) / La proportion de l'aide consentie par les collectivités publiques est fixée en tenant compte du montant de la participation éventuelle des personnes restant tenues à l'obligation alimentaire. La décision peut être révisée sur production par le bénéficiaire de l'aide sociale d'une décision judiciaire rejetant sa demande d'aliments ou limitant l'obligation alimentaire à une somme inférieure à celle qui avait été envisagée par l'organisme d'admission. La décision fait également l'objet d'une révision lorsque les débiteurs d'aliments ont été condamnés à verser des arrérages supérieurs à ceux qu'elle avait prévus ". L'article L. 132-7 du même code prévoit, en outre, que : " En cas de carence de l'intéressé, le représentant de l'Etat ou le président du conseil départemental peut demander en son lieu et place à l'autorité judiciaire la fixation de la dette alimentaire et le versement de son montant, selon le cas, à l'Etat ou au département qui le reverse au bénéficiaire, augmenté le cas échéant de la quote-part de l'aide sociale ".<br/>
<br/>
              6. La question de savoir quels sont les recours visés par les dispositions du 4° de l'article L. 134-3 du code de l'action sociale et des familles, dans leur rédaction applicable à compter du 1er janvier 2019, et notamment si sont visés les recours contre les décisions d'admission à l'aide sociale qui fixent l'aide de la collectivité publique en déduisant la participation des personnes tenues à l'obligation alimentaire et les recours exercés par l'Etat ou le département contre les obligés alimentaires pour recouvrer des sommes exposées au titre de l'aide sociale, soulève une difficulté sérieuse. <br/>
<br/>
              7. Dès lors qu'à la date à laquelle le Conseil d'Etat se prononce par la présente décision, il est certain que la commission départementale d'aide sociale de la Drôme, compétente jusqu'au 31 décembre 2018, ne sera pas en mesure de se prononcer avant cette date si le jugement de l'affaire lui est attribué, l'action introduite par Mme A...devant le tribunal administratif de Grenoble présente à juger une question de compétence de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 27 février 2015. Par suite, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si cette action relève ou non de compétence de la juridiction administrative et de surseoir à statuer sur sa transmission à la juridiction compétente jusqu'à la décision de ce tribunal.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
Article 2 : Il est sursis à statuer sur la question transmise par le tribunal administratif de Grenoble jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridiction compétent pour statuer sur la demande de MmeA....<br/>
Article 3 : La présente décision sera notifiée à Mme C...A...épouse B...et au département de la Drôme. <br/>
Copie en sera adressée à la ministre des solidarités et de la santé et au président du tribunal administratif de Grenoble.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
