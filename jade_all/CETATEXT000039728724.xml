<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728724</ID>
<ANCIEN_ID>JG_L_2019_12_000000428856</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/87/CETATEXT000039728724.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 31/12/2019, 428856, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428856</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:428856.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 mars, 2 novembre et 10 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Cimade demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du 31 décembre 2018 du ministre de l'intérieur relative au parc d'hébergement des demandeurs d'asile et des bénéficiaires de la protection internationale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive n° 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordiniare,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 décembre 2019, présentée par la Cimade ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Par une circulaire du 31 décembre 2018, qui se présente comme une information relative au parc d'hébergement des demandeurs d'asile et des bénéficiaires de la protection internationale, le ministre de l'intérieur a adressé des instructions aux préfets de région et de département en matière d'hébergement et d'accompagnement des demandeurs d'asile et des réfugiés, notamment en définissant les actions devant être conduites en 2019, en indiquant, pour cette année, les objectifs de création de places d'hébergement, en précisant les critères d'appréciation pour la sélection de projets de création de lieux d'hébergement, les règles d'organisation du parc d'hébergement en différentes structures d'accueil selon la situation des personnes concernées et les conditions du pilotage national et local de la politique d'asile. La Cimade demande l'annulation pour excès de pouvoir de cette circulaire.<br/>
<br/>
              2.	En premier lieu, si les centres d'accueil pour demandeurs d'asile appartiennent à la catégorie des établissements et services sociaux et médico-sociaux au sens et pour l'application du II de l'article L. 312-1 du code de l'action sociale et des familles, la circulaire attaquée ne fixe pas de conditions techniques minimales d'organisation et de fonctionnement de ces centres d'accueil. Par suite, le moyen tiré de ce qu'elle serait illégale, faute d'avoir été précédée de la consultation du Comité national d'organisation sanitaire et sociale prévue par le II de cet article L. 312-1, ne peut qu'être écarté.<br/>
<br/>
              3.	En deuxième lieu, aux termes de l'article L. 744-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sont des lieux d'hébergement pour demandeurs d'asile : / 1° Les centres d'accueil pour demandeurs d'asile mentionnés à l'article L. 348-1 du code de l'action sociale et des familles ; / 2° Toute structure bénéficiant de financements du ministère chargé de l'asile pour l'accueil de demandeurs d'asile et soumise à déclaration, au sens de l'article L. 322-1 du même code ". Le même article prévoit que : " les décisions d'admission dans un lieu d'hébergement pour demandeurs d'asile, de sortie de ce lieu et de changement de lieu sont prises par l'Office français de l'immigration et de l'intégration, après consultation du directeur du lieu d'hébergement, sur la base du schéma national d'accueil des demandeurs d'asile et, le cas échéant, du schéma régional prévus à l'article L. 744-2 et en tenant compte de la situation du demandeur ". L'article L. 744-2 du même code dispose, dans sa version en vigueur à la date de la circulaire attaquée, que : " Le schéma national d'accueil des demandeurs d'asile et d'intégration des réfugiés fixe la part des demandeurs d'asile accueillis dans chaque région ainsi que la répartition des lieux d'hébergement qui leur sont destinés. Il est arrêté par le ministre chargé de l'asile, après avis des ministres chargés du logement et des affaires sociales. Il est transmis au Parlement. / Un schéma régional est établi par le représentant de l'Etat dans la région (...) en conformité avec le schéma national d'accueil des demandeurs d'asile. Il fixe les orientations en matière de répartition des lieux d'hébergement pour demandeurs d'asile sur le territoire de la région, présente le dispositif régional prévu pour l'enregistrement des demandes d'asile ainsi que le suivi et l'accompagnement des demandeurs d'asile et définit les actions en faveur de l'intégration des réfugiés (...) ".<br/>
<br/>
              4.	Il ressort des pièces du dossier qu'alors que le précédent schéma national d'accueil des demandeurs d'asile pris pour l'application de ces dispositions, qui résultait d'un arrêté du 21 décembre 2015, couvrait la période courant jusqu'au 31 décembre 2017, aucun arrêté n'a édicté un nouveau schéma national pour la période ultérieure. La circulaire attaquée fixe l'objectif d'atteindre, pour 2019, un niveau de plus de 97 000 places disponibles dans le cadre du dispositif national d'accueil des demandeurs d'asile, notamment par la création, au cours de cette même année, de 1 000 nouvelles places en centres d'accueil pour demandeurs d'asile, de 2 500 places en hébergement d'urgence pour demandeurs d'asile et de 2 000 places en centres provisoires d'hébergement réservés aux réfugiés et aux bénéficiaires de la protection internationale. Son annexe 1.1 prévoit la répartition de ces créations entre les régions et définit pour chaque région l'état prévisionnel du parc, selon le type d'hébergement, au 31 décembre 2019. Dans ces conditions, la circulaire attaquée doit être regardée comme tenant lieu du schéma national d'accueil fixant la répartition des places d'hébergement pour demandeurs d'asile, tel que prévu à l'article L. 744-2 du code de l'entrée et du séjour des étrangers et du droit d'asile. <br/>
<br/>
              5.	Si l'association requérante soutient que, ce faisant, la circulaire attaquée serait entachée d'incompétence, il résulte des dispositions précédemment citées de l'article L. 744-2 du code de l'entrée et du séjour des étrangers et du droit d'asile que le ministre de l'intérieur est compétent pour arrêter le schéma national. Les moyens tirés de ce que le ministre aurait été incompétent pour adopter la circulaire attaquée en ce qu'elle tient lieu de schéma national et pour donner instruction aux préfets de région d'actualiser en conséquence les schémas régionaux d'accueil des demandeurs d'asile ne peuvent, par suite, qu'être écartés. <br/>
<br/>
              6.	Il est vrai que l'article L. 744-2 prévoit la consultation des ministres chargés du logement et des affaires sociales avant l'adoption du schéma national d'accueil des demandeurs d'asile. Mais l'absence d'avis de ces ministres ne saurait être regardée comme privant les demandeurs d'asile d'une garantie et il ne ressort pas des pièces du dossier que le défaut d'avis formalisé de ces ministres aurait, dans les circonstances de l'espèce, exercé une influence sur le sens de la circulaire attaquée, qui a été prise pour mettre en oeuvre la politique gouvernementale en matière d'hébergement et d'accueil des demandeurs d'asile. <br/>
<br/>
              7.	Enfin, si l'association requérante soutient que le ministre de l'intérieur aurait commis une erreur manifeste d'appréciation en fixant les objectifs de création de places d'hébergement, ce moyen n'est pas assorti des précisions nécessaires pour en apprécier le bien-fondé. <br/>
<br/>
              8.	En troisième lieu, en invitant les préfets à limiter le plus possible les structures mixtes, rassemblant dans un même lieu des places dédiées à l'hébergement des demandeurs d'asile et des places relevant du dispositif d'hébergement " généraliste ", le ministre de l'intérieur s'est borné à demander à ses services d'organiser l'hébergement et l'orientation d'étrangers identifiés comme demandeurs d'asile en priorité vers les dispositifs d'hébergement qu'il finance. La circulaire attaquée ne peut ni n'entend interdire par elle-même l'accès des demandeurs d'asile au dispositif d'hébergement d'urgence ni imposer un transfert de logement qui ne soit pas nécessaire. Par suite, doivent être écartés les moyens invoqués à l'encontre de cette partie de la circulaire et tirés de l'incompétence du ministre de l'intérieur, de l'erreur de droit et de l'erreur manifeste d'appréciation. Il ne peut pas davantage être soutenu que la circulaire méconnaîtrait à cet égard les objectifs du 6 de l'article 18 de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013, selon lequel " les Etats membres font en sorte que les demandeurs ne soient transférés d'un logement à l'autre que lorsque cela est nécessaire ". <br/>
<br/>
              9.	En quatrième lieu, si la circulaire attaquée rappelle, en cas de mesures d'éloignement prononcées à l'encontre de personnes qui se sont vu refuser le bénéfice du droit d'asile, la possibilité d'assignations à résidence prévue par les dispositions de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, en faisant référence notamment à des " dispositifs de préparation au retour ", elle n'a, ce faisant, ni pour objet ni pour effet de créer de tels dispositifs qui ont pu être décrits par une autre circulaire. L'association requérante ne peut, par suite, utilement contester la légalité de ces dispositifs à l'appui du recours qu'elle a formé contre la circulaire qu'elle attaque. <br/>
<br/>
              10.	Il résulte de tout ce qui précède, que la Cimade n'est pas fondée à demander l'annulation de la circulaire du 31 décembre 2018 qu'elle attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la Cimade est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Cimade et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
