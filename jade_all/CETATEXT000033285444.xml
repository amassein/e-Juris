<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285444</ID>
<ANCIEN_ID>JG_L_2016_10_000000386551</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285444.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 18/10/2016, 386551, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386551</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:386551.20161018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A...a demandé au tribunal administratif de Dijon de condamner l'Etat à réparer le préjudice ayant résulté pour lui de l'arrêté du 2 février 2010 par lequel le préfet de la Nièvre a suspendu son permis de conduire pour une durée de trois mois. Par un jugement n° 1401140 du 28 octobre 2014, le tribunal administratif a condamné l'Etat à lui verser la somme de 1 000 euros en réparation du préjudice.<br/>
<br/>
              Par un pourvoi enregistré le 18 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;  <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route;<br/>
<br/>
              - l'arrêté du 5 septembre 2001 modifié fixant les modalités de dépistage des substances témoignant de l'usage de stupéfiants et des analyses et examens prévus par le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 30 janvier 2010, M.A..., qui conduisait un camion, a causé un grave accident de circulation ; que son permis a fait l'objet d'une mesure de rétention ; qu'au vu des résultats de l'analyse de sang pratiquée, le préfet de la Nièvre, par un arrêté du 2 février 2010, a prononcé la suspension du permis pour une durée de trois mois ; qu'il a toutefois mis un terme à la suspension le 6 avril 2010 au vu des résultats d'une contre-expertise effectuée à partir du même prélèvement sanguin ; que, par le jugement du 28 octobre 2014 contre lequel le ministre de l'intérieur se pourvoit en cassation, le tribunal administratif de Dijon, saisi par M. A...d'une demande indemnitaire, a jugé que l'arrêté du 2 février 2010 était illégal comme résultant d'une erreur de fait et a condamné l'Etat à verser à l'intéressé une indemnité de 1 000 euros en réparation du préjudice moral et des troubles dans les conditions d'existence qu'il avait subis entre le 2 février et le 6 avril 2010 ;<br/>
<br/>
              2. Considérant, en premier lieu, que, pour juger que la mesure de suspension résultait d'une erreur de fait, le tribunal administratif a relevé qu'elle avait été décidée au vu d'une analyse sanguine indiquant un taux de tétrahydrocannabinol " inférieur à 0,5 nanogramme par litre " et présentée comme positive, alors que l'analyse réalisée dans le cadre de la contre-expertise mentionnait le même résultat chiffré et était présentée comme négative ; que le ministre de l'intérieur soutient que le tribunal a commis une erreur de droit en retenant qu'une mesure de suspension du permis de conduire ne peut légalement être prise dans le cas où la quantité de tétrahydrocannabinol présente dans le sang est inférieure au seuil de 1 nanogramme par litre prévu à l'article 11 de l'arrêté du 5 septembre 2001 visé ci-dessus, alors que ce seuil définit seulement la quantité minimale que les appareils de mesure utilisés doivent pouvoir détecter ; que, toutefois, le tribunal administratif a pu, dans le cadre de son appréciation souveraine des faits de l'espèce, estimer que la mention " inférieur à 0,5 nanogramme par litre " n'impliquait pas la détection d'une quantité quelconque de la substance recherchée mais signifiait que l'appareil utilisé n'avait rien détecté, ce qui, compte tenu de sa sensibilité, permettait d'affirmer que la quantité était soit nulle, soit comprise entre 0 et 0,5 nanogrammes par litre, et retenir en conséquence que c'était par suite d'une erreur matérielle que la première analyse avait été présentée comme positive ; que, dans ces conditions, le moyen invoqué par le ministre est, en tout état de cause, inopérant ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que c'est seulement à titre surabondant que le tribunal administratif a mentionné la circonstance que le procureur de la République près le tribunal de grande instance de Nevers avait, le 12 mars 2010, classé sans suite la procédure engagée à l'encontre de M. A... du chef de conduite d'un véhicule après avoir fait usage de stupéfiants ; que, par suite, le moyen tiré de l'erreur de droit qu'il aurait commise en se fondant sur cette circonstance pour juger  fautive la suspension du permis de conduire doit être écarté ;<br/>
<br/>
              4. Considérant, enfin, que le jugement attaqué écarte tout lien de causalité entre la mesure de suspension et le licenciement de M. A...et rejette en conséquence les conclusions de l'intéressé tendant à la réparation d'un préjudice professionnel ; qu'en estimant que la suspension de son permis de conduire avait causé des troubles dans les conditions d'existence de l'intéressé et lui avait occasionné un préjudice moral, le tribunal administratif n'a pas dénaturé les pièces du dossier qui lui était soumis ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation du jugement du 28 octobre 2014 du tribunal administratif de Dijon ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
Article 2 : La présente décision sera notifiée au ministre de l'intérieur et à M. C... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
