<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027482077</ID>
<ANCIEN_ID>JG_L_2013_05_000000366456</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/48/20/CETATEXT000027482077.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 29/05/2013, 366456, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366456</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:366456.20130529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 366456, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 février et 15 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la communauté urbaine Marseille Provence Métropole, dont le siège est BP 48014 à Marseille cedex 02 (13567) ; la communauté urbaine Marseille Provence Métropole demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1300606 du 15 février 2013 du juge des référés du tribunal administratif de Marseille, statuant en application de l'article L. 551-1 du code de justice administrative, en tant que, sur la demande de la société communication et application de transmissions industrielles et système (Comatis), d'une part, elle annule la décision du 18 janvier 2013 par laquelle la communauté urbaine Marseille Provence Métropole a rejeté l'offre de la société Comatis pour le marché relatif à la fourniture d'un système de priorité aux feux pour des lignes de bus à haut niveau de service et des lignes de bus structurantes à Marseille, et, d'autre part, elle enjoint à la communauté urbaine de reprendre la procédure au stade de l'analyse des offres ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Comatis ;<br/>
<br/>
              3°) de mettre à la charge de la société Comatis le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 366577, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 et 19 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Comatis, dont le siège est 8 rue Carnot à Saint-Cyr-L'Ecole (78210), représentée par son président directeur général ; la société Comatis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance visée sous le n° 366456 en tant qu'elle n'a pas annulé l'intégralité de la procédure et enjoint seulement à la communauté urbaine Marseille Provence Métropole de reprendre la procédure au stade de l'analyse des offres et rejeté ainsi le surplus de ses conclusions ;<br/>
<br/>
              2°) statuant en référé, d'annuler l'ensemble de la procédure ; <br/>
<br/>
              3°) de mettre à la charge de la communauté urbaine Marseille Provence Métropole le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la communauté urbaine Marseille Provence Métropole, à la SCP Rocheteau, Uzan-Sarano, avocat de la société communication et application de transmissions industrielles et système (Comatis) ;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la communauté urbaine Marseille Provence Métropole et de la société communication et application de transmissions industrielles et système (Comatis) sont dirigés contre la même ordonnance ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article L. 551-1 du code de justice administrative que le président du tribunal administratif ou le magistrat qu'il délègue peut être saisi, avant la conclusion d'un contrat de commande publique ou de délégation de service public, d'un manquement, par le pouvoir adjudicateur, à ses obligations de publicité et de mise en concurrence ; qu'aux termes de l'article L. 551-2 du même code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un avis d'appel public à la concurrence publié le 24 juillet 2012, la communauté urbaine Marseille Provence Métropole a lancé une procédure d'attribution d'un marché à bons de commande portant sur la fourniture d'un système de priorité aux feux pour des lignes de bus à Marseille ; que, par une décision du 18 janvier 2013, la communauté urbaine Marseille Provence Métropole a rejeté l'offre de la société Comatis, déclarée irrégulière au motif que la mention " sans objet " avait été inscrite dans le bordereau de prix unitaires joint à son offre au titre des prix 308, 311, 327, 328, 329, 330, 352, 353, 355, 802, 804, 805, 824, 825, 827, 828 et 841 ; que la communauté urbaine Marseille Provence Métropole demande l'annulation de l'ordonnance du juge des référés du tribunal administratif de Marseille du 15 février 2013, en ce que, saisi par la société Comatis, le juge a annulé la décision du 18 janvier 2013 rejetant l'offre de la société et enjoint à la communauté urbaine de reprendre la procédure au stade de l'analyse des offres ; que la société Comatis en demande également l'annulation en tant qu'elle n'a pas annulé l'intégralité de la procédure et enjoint seulement à la communauté urbaine de reprendre la procédure au stade de l'analyse des offres et qu'elle a ainsi rejeté le surplus de ses conclusions ; <br/>
<br/>
              Sur le pourvoi de la communauté urbaine Marseille Provence Métropole :<br/>
<br/>
              4. Considérant qu'aux termes du III de l'article 53 du code des marchés publics : " Les offres inappropriées, irrégulières et inacceptables sont éliminées. Les autres offres sont classées par ordre décroissant. (...) " ; qu'il résulte de ces dispositions que le pouvoir adjudicateur est tenu de rejeter les offres inappropriées, irrégulières et inacceptables; qu'en jugeant que la communauté urbaine Marseille Provence Métropole ne pouvait rejeter l'offre de la société Comatis comme irrégulière au motif que cette offre avait été préalablement analysée, notée puis classée par la commission d'appel d'offres, le juge des référés a commis une erreur de droit ; que la communauté urbaine Marseille Provence Métropole est, dès lors, fondée à demander l'annulation de l'ordonnance attaquée en ce qu'elle annule la décision du 18 janvier 2013 et lui enjoint de reprendre la procédure de passation au stade de l'analyse des offres ;<br/>
<br/>
              Sur le pourvoi de la société Comatis :<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis au juge des référés, qu'à l'appui de sa demande tendant à l'annulation complète de la procédure de passation du marché en cause, la société Comatis soutenait que les documents de la consultation étaient entachés de contradictions susceptibles d'induire en erreur les candidats et d'affecter le contenu de leurs offres ; qu'en se bornant, après avoir annulé la décision du 18 janvier 2013, à enjoindre à la communauté urbaine Marseille Provence Métropole de reprendre la procédure de passation au stade de l'analyse des offres, sans examiner ce moyen de la société Comatis, qui était susceptible d'entraîner l'annulation de la totalité de la procédure de passation, le juge des référés a méconnu l'étendue de son office ; qu'ainsi la société Comatis est fondée à demander l'annulation de l'ordonnance attaquée en tant qu'elle a rejeté le surplus de ses conclusions ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens des pourvois, que l'ordonnance attaquée doit être intégralement annulée ;<br/>
<br/>
              7. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société Comatis ;<br/>
<br/>
              8. Considérant qu'un candidat dont l'offre est irrégulière et doit être rejetée pour ce motif, n'est pas susceptible d'être lésé par les manquements qu'il invoque, sauf si cette irrégularité est le résultat de l'un de ces manquements ;<br/>
<br/>
              9. Considérant qu'il résulte de l'instruction que le cahier des clauses techniques particulières permettait aux candidats de choisir, pour élaborer leurs offres, entre plusieurs options techniques, notamment pour la fourniture du modem radio intégré au " boîtier MDPF ", des antennes, du relai temporisé pour le " calculateur MDPF " et du relai temporisé pour le modem radio ; que si la communauté urbaine Marseille Provence Métropole soutient que tous les prix devaient être renseignés quelles que fussent les options techniques proposées par les candidats, la société Comatis pouvait légitimement croire, en l'absence de toute stipulation contraire dans les documents de la consultation, qu'elle n'était pas tenue, compte tenu des options techniques offertes aux candidats, de renseigner dans le bordereau des prix unitaires le prix des prestations ou matériels qu'elle ne proposait pas dans le cadre de son offre, et pouvait, dès lors, se contenter d'indiquer que le prix de ces prestations et matériels était " sans objet " ; qu'ainsi la société Comatis est fondée à soutenir que les documents de la consultation étaient entachés d'une imprécision de nature à l'induire en erreur et que la communauté urbaine Marseille Provence Métropole ne pouvait, dès lors, regarder son offre comme irrégulière au motif que le prix de certaines prestations et matériels, jugé sans objet, n'était pas mentionné ;<br/>
<br/>
              10. Considérant qu'eu égard à la nature et à la portée du manquement constaté, qui affecte le règlement de la consultation et est susceptible d'avoir influencé les offres des candidats, il y a lieu d'annuler la procédure de passation contestée dans son intégralité ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Comatis qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, sur le fondement des mêmes dispositions, de mettre à la charge de la communauté urbaine Marseille Provence Métropole la somme de 4 500 euros à verser à la société Comatis au titre des frais exposés et non compris dans les dépens devant le juge des référés du tribunal administratif de Marseille et devant le Conseil d'Etat ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Marseille du 15 février 2013 est annulée.<br/>
Article 2 : La procédure de passation du marché ayant pour objet la fourniture d'un système de priorité aux feux pour des lignes de bus à haut niveau de service et des lignes de bus structurantes à Marseille est annulée.<br/>
Article 3 : La communauté urbaine Marseille Provence Métropole versera à la société (Comatis) une somme de 4 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la communauté urbaine Marseille Provence Métropole au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la communauté urbaine Marseille Provence Métropole, à la société communication et application de transmissions industrielles et système (Comatis).<br/>
Copie pour information en sera transmise à la société Capsys SAS.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
