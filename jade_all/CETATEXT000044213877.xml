<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044213877</ID>
<ANCIEN_ID>JG_L_2021_10_000000440741</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/38/CETATEXT000044213877.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 14/10/2021, 440741, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440741</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440741.20211014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 20 et 25 mai 2020 au secrétariat du contentieux du Conseil d'État, M. AH... AD..., M. V... T..., Mme Y... AI..., M. D... W..., M. R... G..., M. S... N..., Mme AG... C..., Mme AA... P..., M. I... A..., M. L... E..., Mme AB... U..., Mme H... F... et M. AK... demandent au Conseil d'État d'annuler pour excès de pouvoir l'article 2 de l'ordonnance n° 2020-463 du 22 avril 2020 adaptant l'état d'urgence sanitaire à la Nouvelle-Calédonie, à la Polynésie française et aux îles Wallis-et-Futuna, en tant qu'il s'applique à la Nouvelle-Calédonie.<br/>
<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu :<br/>
<br/>
       - la Constitution ;<br/>
       - l'accord sur la Nouvelle-Calédonie signé à Nouméa le 5 mai 1998 ; <br/>
       - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
       - le code de la santé publique ;<br/>
       - la loi n° 2020-290 du 23 mars 2020 ;<br/>
       - l'ordonnance n° 2020-463 du 22 avril 2020 ;<br/>
       - la décision n° 2020-869 QPC du 4 décembre 2020 du Conseil constitutionnel ;<br/>
       - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le législateur, par l'article 2 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a inséré au titre III du livre Ier de la troisième partie du code de la santé publique un chapitre Ier bis relatif à l'état d'urgence sanitaire. Aux termes de l'article L. 3131-12 de ce code, l'état d'urgence sanitaire peut être déclaré " en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-15 du même code dresse la liste des mesures de police que le Premier ministre peut prendre, lorsque l'état d'urgence sanitaire est déclaré, " aux seules fins de garantir la santé publique ", tandis qu'aux termes de l'article L. 3131-16 du code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le ministre chargé de la santé peut prescrire, par arrêté motivé, toute mesure réglementaire relative à l'organisation et au fonctionnement du dispositif de santé, à l'exception des mesures prévues à l'article L. 3131-15, visant à mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12. / Dans les mêmes conditions, le ministre chargé de la santé peut prescrire toute mesure individuelle nécessaire à l'application des mesures prescrites par le Premier ministre en application des 1° à 9° du I de l'article L. 3131-15 (...) ". L'article 4 de cette loi dispose que : " L'état d'urgence sanitaire entre en vigueur sur l'ensemble du territoire national ". <br/>
<br/>
              2. L'article 3 de la loi du 23 mars 2020 dispose, dans sa rédaction applicable au litige, que : " Dans les conditions prévues à l'article 38 de la Constitution, le Gouvernement est habilité à prendre par voie d'ordonnance, dans un délai de deux mois à compter de la publication de la présente loi, les mesures d'adaptation destinées à adapter le dispositif de l'état d'urgence sanitaire dans les collectivités régies par l'article 74 de la Constitution et en Nouvelle-Calédonie, dans le respect des compétences de ces collectivités. / Un projet de loi de ratification est déposé devant le Parlement dans un délai de trois mois à compter de la publication de l'ordonnance ". Sur le fondement de cette habilitation, l'article 2 de l'ordonnance du 22 avril 2020 attaquée insère au code de la santé publique un article L. 3841-2 qui prévoit que : " Le chapitre Ier bis du titre III du livre Ier de la troisième partie est applicable en Nouvelle-Calédonie et en Polynésie française jusqu'au 1er avril 2021, sous réserve des adaptations suivantes : / 1° Les références au département sont remplacées, selon le cas, par la référence à la Nouvelle-Calédonie ou par la référence à la Polynésie française ; / 2° Le premier alinéa de l'article L. 3131-17 est remplacé par les deux alinéas suivants : / Lorsque le Premier ministre ou le ministre chargé de la santé prennent des mesures mentionnées aux articles L. 3131-15 et L. 3131-16 et les rendent applicables à la Nouvelle-Calédonie ou à la Polynésie française, ils peuvent habiliter le haut-commissaire à les adapter en fonction des circonstances locales et à prendre toutes les mesures générales ou individuelles d'application de ces dispositions, lorsqu'elles relèvent de la compétence de l'Etat et après consultation du gouvernement de la collectivité. / Lorsqu'une des mesures mentionnées aux 1° à 9° de l'article L. 3131-15 ou à l'article L. 3131-16 doit s'appliquer dans un champ géographique qui n'excède pas la Nouvelle-Calédonie ou la Polynésie française, les autorités mentionnées aux mêmes articles peuvent habiliter le haut-commissaire à la décider lui-même, assortie des adaptations nécessaires s'il y a lieu et dans les mêmes conditions qu'au premier alinéa ". <br/>
<br/>
              3. En premier lieu. l'article 76 de la Constitution dispose que : " Les populations de la Nouvelle-Calédonie sont appelées à se prononcer avant le 31 décembre 1998 sur les dispositions de l'accord signé à Nouméa le 5 mai 1998 et publié le 27 mai 1998 au Journal officiel de la République française. ", tandis qu'aux termes de son article 77 : " Après approbation de l'accord lors de la consultation prévue à l'article 76, la loi organique, prise après avis de l'assemblée délibérante de la Nouvelle-Calédonie, détermine, pour assurer l'évolution de la Nouvelle-Calédonie dans le respect des orientations définies par cet accord et selon les modalités nécessaires à sa mise en œuvre : / - les compétences de l'Etat qui seront transférées, de façon définitive, aux institutions de la Nouvelle-Calédonie, l'échelonnement et les modalités de ces transferts, ainsi que la répartition des charges résultant de ceux-ci ; / - les règles d'organisation et de fonctionnement des institutions de la Nouvelle-Calédonie et notamment les conditions dans lesquelles certaines catégories d'actes de l'assemblée délibérante pourront être soumises avant publication au contrôle du Conseil constitutionnel ; / - les règles relatives à la citoyenneté, au régime électoral, à l'emploi et au statut civil coutumier ; / - les conditions et les délais dans lesquels les populations intéressées de la Nouvelle-Calédonie seront amenées à se prononcer sur l'accession à la pleine souveraineté (...) ". Le 3.3. de l'accord de Nouméa stipule quant à lui que : " La justice, l'ordre public, la défense et la monnaie (ainsi que le crédit et les changes), et les affaires étrangères (sous réserve des dispositions du 3.2.1) resteront de la compétence de l'Etat jusqu'à la nouvelle organisation politique résultant de la consultation des populations intéressées prévue au 5 ". Enfin, l'article 21 de la loi organique du 19 mars 1999 dispose que l'Etat est compétent dans les matières suivantes : " 1° (...) garantie des libertés publiques " tandis qu'aux termes de l'article 22 de la loi organique, la Nouvelle-Calédonie est compétente en matière de : " 4° Protection sociale, hygiène publique et santé, contrôle sanitaire aux frontières ".<br/>
<br/>
              4. Saisi par le Conseil d'Etat de trois questions prioritaires de constitutionnalité soulevées par M. AD... dans le cadre d'autres requêtes, enregistrées sous les n° 441059 et 442045, et portant notamment sur la conformité aux droits et libertés garantis par la Constitution de l'article L. 3841-2 du code de la santé publique, le Conseil constitutionnel a, dans sa décision n° 2020-869 QPC du 4 décembre 2020, déclaré ces dispositions conformes à la Constitution. Il a notamment relevé que, " si elles poursuivent un objectif de protection de la santé publique, ces mesures exceptionnelles, temporaires et limitées à la mesure strictement nécessaire pour répondre à une catastrophe sanitaire et à ses conséquences, se rattachent à la garantie des libertés publiques et ne relèvent donc pas de la compétence de la Nouvelle-Calédonie " et que " en étendant à la Nouvelle-Calédonie les mesures prévues par l'article L. 3131-16 du code de la santé publique permettant au ministre chargé de la santé ou au haut-commissaire de prescrire ou d'adapter, dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, " toute mesure réglementaire relative à l'organisation et au fonctionnement du dispositif de santé ", autre que celles prévues à l'article L. 3131-15, pour mettre fin à la catastrophe sanitaire, le législateur n'a visé que les mesures qui, parce qu'elles concernent l'ordre public ou les garanties des libertés publiques, relèvent de la compétence de l'État. Cette extension est donc sans incidence sur les compétences de la Nouvelle-Calédonie en matière de santé ". Il s'ensuit que les requérants ne sont, en tout état de cause, pas fondés à soutenir que les dispositions attaquées seraient contraires aux règles de répartition des compétences qui découlent des articles 76 et 77 de la Constitution et des articles 21 et 22 de la loi organique du 19 mars 1999. <br/>
<br/>
              5. En deuxième lieu, il découle de ce qui a été dit au point précédent que le moyen tiré de ce que le Gouvernement aurait excédé le périmètre de l'habilitation défini à l'article 3 de la loi du 20 mars 2020 cité au point 2 en empiétant sur les compétences de la Nouvelle-Calédonie n'est pas fondé. <br/>
<br/>
              6. En troisième lieu, les requérants ne sont pas fondés à soutenir que le 2° de l'article 2 de l'ordonnance méconnaîtrait l'objectif de valeur constitutionnelle de clarté et d'intelligibilité de la norme au motif qu'il autorise le pouvoir réglementaire à habiliter le représentant de l'Etat en Nouvelle-Calédonie à prendre des mesures générales ou individuelles d'application des articles L. 3131-15 et L. 3131-16 du code de la santé publique " lorsqu'elles relèvent de la compétence de l'Etat ", dès lors que cette mention se contente de rappeler la nécessité de respecter le partage des compétences entre l'Etat et la Nouvelle-Calédonie défini par la loi organique du 19 mars 1999. <br/>
<br/>
              7. Il résulte de tout ce qui précède que la requête de M. AD... et autres doit être rejetée. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
<br/>
Article 1er : La requête de M. AD... et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. AH... AD..., premier dénommé pour l'ensemble des requérants, au Premier ministre, au ministre des solidarités et de la santé, au ministre des outre-mer et au gouvernement de la Nouvelle-Calédonie.<br/>
              Délibéré à l'issue de la séance du 22 septembre 2021 où siégeaient : M. Christophe Chantepy, président de la section du contentieux, présidant ; M. X... Q..., M. Frédéric Aladjidi, présidents de chambre, Mme AC... J..., M. AE... K..., Mme B... AF..., M. M... O..., M. François Weil, conseillers d'Etat ; M. Réda Wadjinny-Green, auditeur-rapporteur. <br/>
<br/>
              Rendu le 14 octobre 2021<br/>
<br/>
<br/>
       Le Président : <br/>
       Signé : M. Christophe Chantepy<br/>
<br/>
       Le rapporteur : <br/>
       Signé : M. Réda Wadjinny-Green<br/>
<br/>
                                      Le secrétaire :<br/>
                                      Signé : Mme Z... AJ...<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
