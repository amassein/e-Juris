<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044331907</ID>
<ANCIEN_ID>JG_L_2021_11_000000442893</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/33/19/CETATEXT000044331907.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 15/11/2021, 442893, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442893</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:442893.20211115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... et Mme E... C... ont demandé au tribunal administratif de Toulouse de prononcer la décharge de la cotisation supplémentaire d'impôt sur le revenu à laquelle ils ont été assujettis au titre de l'année 2008, ainsi que des intérêts de retard et des pénalités correspondantes. <br/>
<br/>
              Par un jugement n° 1600225 du 24 octobre 2017, après avoir prononcé un non-lieu partiel à hauteur du montant des pénalités dégrevées en cours d'instance, le tribunal administratif de Toulouse a fait droit à la demande de M. B... et de Mme C... en prononçant la décharge des droits et majorations restant en litige d'un montant de 10 518 euros. <br/>
<br/>
              Par un arrêt n° 18BX00604 du 18 juin 2020, la cour administrative d'appel de Bordeaux a, sur appel du ministre de l'action et des comptes publics, annulé ce jugement et remis à la charge de M. B... et de Mme C... les suppléments d'impôt sur le revenu litigieux ainsi que les majorations correspondantes, dans la limite de la somme de 10 518 euros.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 18 août et 16 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... et Mme C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ; <br/>
              - le code de justice administrative ;  <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. B... et de Mme C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... et Mme C... ont imputé sur le montant de leur impôt sur le revenu dû au titre de l'année 2008, la réduction d'impôt prévue par l'article 199 undecies B du code général des impôts, à raison d'investissements réalisés en Guadeloupe ayant consisté, par l'intermédiaire de la société en participation (SEP) KJD Capital 21, laquelle était représentée par la société en nom collectif (SNC) KJD Capital, en l'acquisition et en la mise à disposition au profit d'un exploitant d'une plantation de quatre hectares de fleurs d'alpinias d'une part, et d'un hangar à usage agricole, d'un poulailler et de matériels d'autre part. A l'issue d'une vérification de comptabilité, cette réduction d'impôt a été remise en cause par l'administration fiscale au motif, notamment, que le caractère neuf de cet investissement n'était pas établi. Par un jugement du 24 octobre 2017, le tribunal administratif de Toulouse, après avoir prononcé un non-lieu à statuer à concurrence du dégrèvement des pénalités intervenu en cours d'instance, a déchargé les intéressés de la cotisation supplémentaire d'impôt sur le revenu, majorée des intérêts de retard, à laquelle ils avaient été assujettis au titre de l'année 2008. M. B... et Mme C... se pourvoient en cassation contre l'arrêt du 18 juin 2020 par lequel la cour administrative d'appel de Bordeaux a, sur appel du ministre de l'action et des comptes publics, annulé ce jugement et remis à leur charge le supplément d'imposition en litige ainsi que les majorations correspondantes, dans la limite de la somme de 10 518 euros.<br/>
<br/>
              2. En premier lieu, il ressort des termes de l'arrêt attaqué que la cour a jugé que les investissements dont se prévalaient M. B... et Mme C... n'entraient pas dans les prévisions de l'article 199 undecies B du code général des impôts, en relevant que le caractère neuf des investissements en cause, voire leur existence même, n'était pas démontré. La cour a, ce faisant, répondu de manière motivée à la contestation des intéressés en se fondant sur un des motifs qu'avait retenu l'administration pour établir le redressement. Par suite, alors même que la cour administrative d'appel n'a pas répondu à certains moyens qui étaient dirigés contre les autres motifs retenus par l'administration mais qu'elle n'avait pas elle-même repris pour fonder son arrêt, celui-ci ne méconnait pas l'exigence de motivation des jugements codifiée à l'article L. 9 du code de justice administrative. <br/>
<br/>
              3. En deuxième lieu, aux termes du I de l'article 199 undecies B du code général des impôts : " Les contribuables domiciliés en France au sens de l'article 4 B peuvent bénéficier d'une réduction d'impôt sur le revenu à raison des investissements productifs neufs qu'ils réalisent dans les départements d'outre-mer (...), dans le cadre d'une entreprise exerçant une activité agricole (...) ".<br/>
<br/>
              4. Par des décisions du 28 novembre 2001 et du 11 novembre 2003, la Commission européenne a déclaré l'aide prévue par l'article 199 undecies B du code général des impôts compatible avec les stipulations de l'article 87 du traité instituant la Communauté européenne, devenu l'article 107 du traité sur le fonctionnement de l'Union européenne, en relevant notamment que la France s'engageait à ce que, dans le secteur de la production, transformation et commercialisation de produits agricoles, tous les investissements contribuent à l'amélioration des conditions de production agricole, toute aide visant au simple remplacement des moyens étant à exclure. Ces décisions, dont la juridiction européenne compétente n'avait pas constaté l'invalidité, imposaient à l'administration de faire application de l'article 199 undecies B du code général des impôts en respectant les conditions dont la Commission avait ainsi assorti la déclaration de compatibilité de l'aide, notamment pour l'interprétation de la notion d'"'investissements productifs neufs'". En recherchant si les rejets de fleurs d'alpinias avaient été implantés sur une terre non précédemment cultivée ou exploitée pour une autre culture afin de vérifier si la plantation dont les requérants se prévalaient correspondait à un investissement productif neuf et non au simple remplacement d'une plantation existante, la cour n'a donc pas commis d'erreur de droit. <br/>
<br/>
              5. En troisième lieu, en estimant qu'aucune des pièces du dossier qui lui était soumis n'était de nature à établir le caractère neuf du hangar de stockage, du poulailler et des matériels acquis le 11 décembre 2008 par la SNC KJD Capital auprès de l'EARL Avipro, la cour s'est livrée à une appréciation souveraine exempte de dénaturation.   <br/>
<br/>
              6. Il résulte de ce qui précède que M. B... et Mme C... ne sont pas fondés à soutenir que c'est à tort que, par l'arrêt attaqué, la cour administrative d'appel de Bordeaux a rejeté leur requête. Leurs conclusions tendant au bénéfice de l'article L. 761-1 du code de justice administrative doivent, par conséquent, également être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... et de Mme C... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à Mme E... C..., ainsi qu'au ministre de l'économie, des finances et de la relance. <br/>
              Délibéré à l'issue de la séance du 28 octobre 2021 où siégeaient : M. Frédéric Aladjidi, président de chambre, présidant ; Mme Anne Egerszegi, conseillère d'Etat et M. Lionel Ferreira, maître des requêtes en service extraordinaire-rapporteur. <br/>
<br/>
              Rendu le 15 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Frédéric Aladjidi<br/>
 		Le rapporteur : <br/>
      Signé : M. Lionel Ferreira<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... F...<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
