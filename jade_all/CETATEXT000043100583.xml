<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043100583</ID>
<ANCIEN_ID>JG_L_2021_02_000000432038</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/10/05/CETATEXT000043100583.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 04/02/2021, 432038, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432038</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Dominique Agniau-Canel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432038.20210204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Casino de Nouméa a demandé au tribunal administratif de<br/>
Nouvelle-Calédonie de prononcer la réduction des sommes auxquelles elle a été assujettie au titre du prélèvement communal sur le produit des jeux pour les années 2012 à 2015 et d'ordonner, en conséquence, la restitution du trop-perçu. Par un jugement n° 1600382 du<br/>
30 mars 2017, le tribunal administratif de Nouvelle-Calédonie a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 17PA01829 du 28 mars 2019, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Casino de Nouméa contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 27 juin 2019, le 17 septembre 2019 et le 6 avril 2020 au secrétariat du contentieux du Conseil d'Etat, la société Casino de Nouméa demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du gouvernement de la Nouvelle-Calédonie le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
            Vu les autres pièces du dossier ;<br/>
<br/>
            Vu : <br/>
            - la loi organique n° 99-209 du 19 mars 1999 relative à la Nouvelle-Calédonie ;<br/>
            - le code des impôts de Nouvelle-Calédonie ;<br/>
             - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Agniau-Canel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Casino de Nouméa, au cabinet Briard, avocat du Gouvernement de la Nouvelle-Calédonie et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Nouméa ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société Casino de Nouméa, qui exploite un établissement de jeux de hasard sur le territoire de la commune de Nouméa, a demandé la réduction du prélèvement communal sur le produit des jeux auquel elle a été assujettie au titre des années 2012 à 2015 en application des dispositions de l'article 890 du code des impôts de Nouvelle-Calédonie au motif que ce prélèvement avait été assis à tort sur le produit brut des jeux. Elle se pourvoit en cassation contre l'arrêt en date du 28 mars 2019 par lequel la cour administrative d'appel de Paris a rejeté son appel contre le jugement du tribunal administratif de Nouvelle-Calédonie du 30 mars 2017 rejetant sa demande. <br/>
<br/>
              2. L'ancien article 890 du code des impôts de Nouvelle-Calédonie, applicable au litige avant son abrogation par la " loi du pays " du 31 décembre 2016 relative aux privilèges et hypothèques et portant diverses dispositions d'ordre fiscal, dispose que : " le cahier des charges, approuvé par le conseil municipal de la commune d'implantation d'un établissement de jeux de hasard régulièrement autorisé, peut comporter une clause instituant au profit de ladite commune un prélèvement sur le produit des jeux au maximum égal à 10% de la même base que le prélèvement opéré au profit de la Nouvelle-Calédonie avec abattement à la base de 30 % ". L'article 626 du même code précise que le prélèvement opéré au profit de la Nouvelle Calédonie est établi sur le " produit net des jeux d'argent pratiqués " qui s'entend  " en ce qui concerne l'ensemble des recettes, à l'exception de celles procurées par les junket tours et du Texas hold'em Poker, du produit brut des jeux, augmenté du total des pourboires reçus et diminué des salaires, charges sociales et rémunérations autres que celles versées aux membres du conseil d'administration ; / en ce qui concerne les recettes provenant des junket-tours et du Texas hold'em Poker, du produit brut des jeux, diminué de l'ensemble des frais spécialement engagés pour la réalisation de ces opérations ".  Enfin, l'article 647 de ce code prévoit que " (...) Le directeur responsable du casino (...) certifie le montant du produit net des jeux réalisé jusqu'à la dernière journée ainsi que le montant des prélèvements à verser au titre du mois considéré. ". <br/>
<br/>
              3. Il résulte des dispositions du code des impôts de la Nouvelle-Calédonie citées au point 2 que le prélèvement opéré sur le produit des jeux au profit de la commune où l'établissement de jeux de hasard est implanté a la même assiette que celui qui est opéré au profit de la Nouvelle-Calédonie. Il est par suite établi sur la base du produit net des jeux réalisé par l'établissement tel que défini par l'article 626 du code des impôts de Nouvelle-Calédonie. Il s'ensuit qu'en jugeant que ce prélèvement était assis sur le produit brut des jeux au motif qu'il était soumis à un abattement forfaitaire de 30 %, la cour administrative d'appel de Paris a commis une erreur de droit. La société requérante est, dès lors, fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Nouvelle-Calédonie la somme de 3 000 euros à verser à la société Casino de Nouméa au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à la charge de la société Casino de Nouméa qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 28 mars 2019 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : La Nouvelle-Calédonie versera à la société Casino de Nouméa la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par la Nouvelle-Calédonie et la commune de Nouméa au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Casino de Nouméa, à la Nouvelle-Calédonie et à la commune de Nouméa. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
