<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029103378</ID>
<ANCIEN_ID>JG_L_2014_06_000000376113</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/10/33/CETATEXT000029103378.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 18/06/2014, 376113, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376113</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2014:376113.20140618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1201826 du 11 février 2014, enregistré le 7 mars 2014 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Pau, avant de statuer sur la demande de la SCI Mounou, M.B..., la SARL Colomes Immobilier, la SARL Beaupré et M.A..., tendant à l'annulation de l'arrêté du 1er août 2012 par lequel le maire de Billère (Pyrénées-Atlantiques) a accordé à l'office 64 de l'habitat un permis de construire portant sur un ensemble de sept logements sur un terrain sis chemin Latéral, a décidé, par application de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si les dispositions des articles L. 600-1-2, L. 600-1-3, L. 600-5 et L. 600-7 du code de l'urbanisme, issues de l'ordonnance n° 2013-638 du 18 juillet 2013 relative au contentieux de l'urbanisme, sont ou non applicables aux instances introduites avant son entrée en vigueur ; <br/>
<br/>
              Vu les observations, enregistrées le 24 mars 2014, présentées par la SCI Mounou et autres ; <br/>
<br/>
              Vu les observations, enregistrées le 19 mai 2014, présentées par le ministre de l'écologie, du développement durable et de l'énergie ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu la loi n° 2013-569 du 1er juillet 2013 ; <br/>
<br/>
              Vu l'ordonnance n° 2013-638 du 18 juillet 2013 ; <br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
      Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de Mme Clémence Olsina, auditeur, <br/>
- les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT :<br/>
<br/>
              1. Conformément aux dispositions de son article 5, l'ordonnance du 18 juillet 2013 relative au contentieux de l'urbanisme est entrée en vigueur le 19 août 2013, un mois après sa publication au Journal officiel. Cette ordonnance ne contient aucune disposition précisant ses modalités d'application aux instances juridictionnelles en cours à la date de son entrée en vigueur. <br/>
<br/>
              2. L'article L. 600-1-2 du code de l'urbanisme, créé par cette ordonnance, dispose que : " Une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association n'est recevable à former un recours pour excès de pouvoir contre un permis de construire, de démolir ou d'aménager que si la construction, l'aménagement ou les travaux sont de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien qu'elle détient ou occupe régulièrement ou pour lequel elle bénéficie d'une promesse de vente, de bail, ou d'un contrat préliminaire mentionné à l'article L. 261-15 du code de la construction et de l'habitation ". L'article L. 600-1-3 du même code, créé par la même ordonnance, dispose que : " Sauf pour le requérant à justifier de circonstances particulières, l'intérêt pour agir contre un permis de construire, de démolir ou d'aménager s'apprécie à la date d'affichage en mairie de la demande du pétitionnaire ". <br/>
<br/>
              3. S'agissant de dispositions nouvelles qui affectent la substance du droit de former un recours pour excès de pouvoir contre une décision administrative, elles sont, en l'absence de dispositions contraires expresses, applicables aux recours formés contre les décisions intervenues après leur entrée en vigueur.  <br/>
<br/>
              4. L'article L. 600-5 du code de l'urbanisme, dans sa version issue de la même ordonnance, dispose que : " Le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager, estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice n'affectant qu'une partie du projet peut être régularisé par un permis modificatif, peut limiter à cette partie la portée de l'annulation qu'il prononce et, le cas échéant, fixer le délai dans lequel le titulaire du permis pourra en demander la régularisation ". L'article L. 600-7 du même code, créé par la même ordonnance, dispose que : " Lorsque le droit de former un recours pour excès de pouvoir contre un permis de construire, de démolir ou d'aménager est mis en oeuvre dans des conditions qui excèdent la défense des intérêts légitimes du requérant et qui causent un préjudice excessif au bénéficiaire du permis, celui-ci peut demander, par un mémoire distinct, au juge administratif saisi du recours de condamner l'auteur de celui-ci à lui allouer des dommages et intérêts. La demande peut être présentée pour la première fois en appel. / Lorsqu'une association régulièrement déclarée et ayant pour objet principal la protection de l'environnement au sens de l'article L. 141-1 du code de l'environnement est l'auteur du recours, elle est présumée agir dans les limites de la défense de ses intérêts légitimes ". <br/>
<br/>
              5. Ces nouvelles dispositions, qui instituent des règles de procédure concernant exclusivement les pouvoirs du juge administratif en matière de contentieux de l'urbanisme, sont, en l'absence de dispositions expresses contraires, d'application immédiate aux instances en cours, quelle que soit la date à laquelle est intervenue la décision administrative contestée. Elles peuvent être appliquées pour la première fois en appel.<br/>
<br/>
              6. Le présent avis sera notifié au tribunal administratif de Pau, à la SCI Mounou, à M.B..., à la SARL Colomes Immobilier, à la SARL Beaupré, à M. A..., à la commune de Billère, à l'office 64 de l'habitat et à la ministre du logement et de l'égalité des territoires. <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES EN MATIÈRE D'URBANISME ISSUES DE L'ORDONNANCE DU 18 JUILLET 2013 - 1) DISPOSITIONS RELATIVES À L'APPRÉCIATION DE L'INTÉRÊT POUR AGIR (ART. L. 600-1-2 ET ART. L. 600-1-3 DU CODE DE L'URBANISME) - APPLICATION AUX SEULS RECOURS FORMÉS CONTRE DES DÉCISIONS INTERVENUES APRÈS LEUR ENTRÉE EN VIGUEUR - EXISTENCE [RJ1] - 2) DISPOSITIONS RELATIVES AUX POUVOIRS DU JUGE (ART. L. 600-5 ET L. 600-7 DU CODE DE L'URBANISME) - APPLICATION IMMÉDIATE AUX INSTANCES EN COURS, Y COMPRIS POUR LA PREMIÈRE FOIS EN APPEL - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. INTÉRÊT À AGIR. - ENTRÉE EN VIGUEUR DES DISPOSITIONS DES ARTICLES L. 600-1-2 (CRITÈRES D'APPRÉCIATION DE L'INTÉRÊT POUR AGIR) ET L. 600-1-3 (DATE D'APPRÉCIATION DE L'INTÉRÊT POUR AGIR) ISSUES DE L'ORDONNANCE DU 18 JUILLET 2013 - APPLICATION AUX SEULS RECOURS FORMÉS CONTRE DES DÉCISIONS INTERVENUES APRÈS LEUR ENTRÉE EN VIGUEUR - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - ENTRÉE EN VIGUEUR DES DISPOSITIONS DES ARTICLES L. 600-5 (ANNULATION PARTIELLE) ET L. 600-7 (ALLOCATION DE DOMMAGES ET INTÉRÊTS POUR RECOURS ABUSIF) ISSUES DE L'ORDONNANCE DU 18 JUILLET 2013 - APPLICATION IMMÉDIATE AUX INSTANCES EN COURS, Y COMPRIS POUR LA PREMIÈRE FOIS EN APPEL - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-08-01 1) Les dispositions nouvelles, issues de l'ordonnance n° 2013-638 du 18 juillet 2013, de l'article L. 600-1-2 du code de l'urbanisme, qui subordonnent la reconnaissance d'un intérêt pour agir contre certaines autorisations d'urbanisme au profit d'une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association à la condition que le projet soit de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien de cette personne, d'une part, et celles de l'article L. 600-1-3 du même code, qui imposent d'apprécier l'intérêt pour agir contre une telle autorisation, sauf circonstance particulière, à la date d'affichage en mairie de la demande du pétitionnaire, d'autre part, affectent la substance du droit de former un recours pour excès de pouvoir contre une décision administrative et sont dès lors, en l'absence de dispositions contraires expresses, applicables aux seuls recours formés contre les décisions intervenues après leur entrée en vigueur....  ,,2) Les dispositions, dans leur rédaction issue de la même ordonnance, de l'article L. 600-5 du code de l'urbanisme, relatives à la possibilité pour le juge, en présence d'un vice n'affectant qu'une partie de la construction, de ne prononcer qu'une annulation partielle et de surseoir à statuer afin d'en permettre la régularisation, d'une part, et les dispositions nouvelles de l'article L. 600-7 du même code, qui permettent au juge d'allouer des dommages et intérêts au bénéficiaire de l'autorisation d'urbanisme en cas de recours abusif, d'autre part, instituent des règles de procédure concernant exclusivement les pouvoirs du juge administratif en matière de contentieux de l'urbanisme et sont dès lors, en l'absence de dispositions expresses contraires, d'application immédiate aux instances en cours, quelle que soit la date à laquelle est intervenue la décision administrative contestée. Elles peuvent être appliquées pour la première fois en appel.</ANA>
<ANA ID="9B"> 68-06-01-02 Les dispositions nouvelles, issues de l'ordonnance n° 2013-638 du 18 juillet 2013, de l'article L. 600-1-2 du code de l'urbanisme, qui subordonnent la reconnaissance d'un intérêt pour agir contre certaines autorisations d'urbanisme au profit d'une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association à la condition que le projet soit de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance de son bien, d'une part, et celles de l'article L. 600-1-3 du même code, qui imposent d'apprécier l'intérêt pour agir contre une telle autorisation, sauf circonstance particulière, à la date d'affichage en mairie de la demande du pétitionnaire, d'autre part, affectent la substance du droit de former un recours pour excès de pouvoir contre une décision administrative et sont dès lors, en l'absence de dispositions contraires expresses, applicables aux recours formés contre les décisions intervenues après leur entrée en vigueur.</ANA>
<ANA ID="9C"> 68-06-04 Les dispositions, dans leur rédaction issue de l'ordonnance n° 2013-638 du 18 juillet 2013, de l'article L. 600-5 du code de l'urbanisme, relatives à la possibilité pour le juge, en présence d'un vice n'affectant qu'une partie de la construction, de ne prononcer qu'une annulation partielle et de surseoir à statuer afin d'en permettre la régularisation, d'une part, et les dispositions nouvelles de l'article L. 600-7 issues de cette même ordonnance, qui permettent au juge d'allouer des dommages et intérêts au bénéficiaire de l'autorisation d'urbanisme en cas de recours abusif, d'autre part, instituent des règles de procédure concernant exclusivement les pouvoirs du juge administratif en matière de contentieux de l'urbanisme et sont dès lors, en l'absence de dispositions expresses contraires, d'application immédiate aux instances en cours, quelle que soit la date à laquelle est intervenue la décision administrative contestée. Elles peuvent être appliquées pour la première fois en appel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 11 juillet 2008, Association des amis des paysages bourganiauds, n° 313386, T. pp. 845-970.,,[RJ2] Rappr., s'agissant des dispositions de l'article L. 600-5-1 du code de l'urbanisme issues de l'ordonnance n° 2013-638 du 18 juillet 2013, CE, 18 juin 2014, Société Batimalo et autre, n° 376760, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
