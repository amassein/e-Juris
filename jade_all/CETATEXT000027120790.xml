<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027120790</ID>
<ANCIEN_ID>JG_L_2013_02_000000354994</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/12/07/CETATEXT000027120790.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 27/02/2013, 354994, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354994</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Maryline Saleix</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354994.20130227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 19 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics, de la réforme de l'Etat, porte-parole du Gouvernement ; il demande au Conseil d'Etat d'annuler l'arrêt n° 09PA06734 du 24 novembre 2011 par lequel la cour administrative d'appel de Paris, statuant sur la requête de l'Etablissement Poudix tendant à l'annulation de l'article 2 du jugement n° 0421607, 0421612 du 5 octobre 2009 par lequel le tribunal administratif de Paris a rejeté le surplus de ses demandes tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle sur cet impôt ainsi que des rappels de retenue à la source auxquels il a été assujetti au titre des années 1998 à 2000, a déchargé le contribuable des impositions restant en litige ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 février 2013, présentée pour l'Etablissement Poudix ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maryline Saleix, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les observations de Me Le Prado, avocat de l'Etablissement Poudix,<br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Le Prado, avocat de l'Etablissement Poudix ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'Etablissement Poudix, anstalt constitué le 30 juillet 1973 à Vaduz au Liechtenstein, a acquis le 22 juillet 1974 la propriété d'un hôtel particulier situé à Paris, dans le 16ème arrondissement ; qu'il a fait l'objet d'une vérification de comptabilité portant sur les années 1997 à 1999 et d'un contrôle sur pièces pour l'année 2000 ; que l'administration fiscale l'a assujetti à l'impôt sur les sociétés et à la contribution additionnelle à cet impôt au titre des années 1998 à 2000, sur une base d'imposition égale à la valeur locative de l'immeuble, lequel était pendant ces années mis gratuitement à la disposition de tiers ; que le ministre du budget, des comptes publics, de la réforme de l'Etat, porte-parole du Gouvernement se pourvoit en cassation contre l'arrêt du 24 novembre 2011 par lequel la cour administrative d'appel de Paris a réformé le jugement du 5 octobre 2009 du tribunal administratif de Paris et déchargé l'établissement des impositions restant en litige ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 206 du code général des impôts : " 1. Sous réserve des dispositions des articles 8 ter, 239 bis AA et 1655 ter, sont passibles de l'impôt sur les sociétés, quel que soit leur objet, les sociétés anonymes, les sociétés en commandite par actions, les sociétés à responsabilité limitée n'ayant pas opté pour le régime fiscal des sociétés de personnes dans les conditions prévues à l'article 3 IV du décret n° 55-594 du 20 mai 1955 modifié, les sociétés coopératives et leurs unions ainsi que, sous réserve des dispositions des 6° et 6° bis du 1 de l'article 207, les établissements publics, les organismes de l'Etat jouissant de l'autonomie financière, les organismes des départements et des communes et toutes autres personnes morales se livrant à une exploitation ou à des opérations de caractère lucratif " ; que la gestion d'un patrimoine immobilier peut constituer une opération à caractère lucratif, même si elle n'a pas de caractère commercial ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que d'après les termes de l'article 2 de ses statuts, l'Etablissement Poudix a pour objet " le placement et la gestion de biens de toutes sortes, y compris les biens immobiliers, la prise et la détention de participations dans les entreprises ou d'autres droits ; l'exercice d'une activité commerciale est à exclure dans tous les cas " ; que, selon l'article 5 des mêmes statuts, dont l'administration avait reproduit la teneur dans les notifications de redressement du 26 septembre 2001, le capital social et les produits en découlant, de même que d'éventuels bénéfices nets réalisés par l'établissement, échoiront aux bénéficiaires désignés par son fondateur ; <br/>
<br/>
              4. Considérant que la cour a relevé que l'Etablissement Poudix était une personne morale constituée sous la forme d'un anstalt selon le droit du Lichtenstein qui ne pouvait être regardé comme soumis à l'impôt sur les sociétés du seul fait de sa forme sociale et que l'article 2 de ses statuts excluait qu'il ait une activité commerciale ; qu'en déduisant de ces seules constatations que la mise à disposition de tiers, personnes physiques, à titre gratuit, du bien immobilier dont il était propriétaire n'était pas constitutif d'une activité lucrative et que l'Etablissement Poudix n'était par suite pas passible de l'impôt sur les sociétés en France, sans rechercher si l'Etablissement Poudix apportait des éléments, relatifs notamment à l'identité des bénéficiaires de son activité, de nature à justifier du but non lucratif de celle-ci, la cour a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner le second moyen du pourvoi, le ministre du budget, des comptes publics, de la réforme de l'Etat, porte-parole du Gouvernement est fondé à demander l'annulation de l'arrêt qu'il attaque ;  <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas dans la présente instance, la partie perdante, le paiement de la somme demandée par l'Etablissement Poudix ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 24 novembre 2011 de la cour administrative d'appel de Paris est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : Les conclusions de l'Etablissement Poudix présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à l'Etablissement Poudix.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
