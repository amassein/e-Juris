<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034879200</ID>
<ANCIEN_ID>JG_L_2017_06_000000396175</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/87/92/CETATEXT000034879200.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 07/06/2017, 396175, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396175</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396175.20170607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 janvier 2016 et 30 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche née du silence gardé sur sa demande du 16 octobre 2015 tendant à ce que soit prises les dispositions nécessaires à l'examen de sa demande d'obtention du diplôme d'expertise comptable par la voie de la validation des acquis de l'expérience ;<br/>
<br/>
              2°) d'enjoindre à la ministre de publier ces dispositions.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code du travail, notamment son article L. 6111-1 ;<br/>
              - la loi n° 2002-73 du 17 janvier 2002 ;<br/>
              - le décret n° 2002-615 du 26 avril 2002 ; <br/>
              - le décret n° 2012-432 du 30 mars 2012 ;<br/>
              - l'arrêté du 28 mars 2014 relatif aux épreuves du diplôme d'expertise comptable ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur, <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M. B...a saisi le directeur du service interacadémique des examens et concours d'une demande tendant à l'obtention du diplôme d'expertise comptable par la voie de la validation des acquis de l'expérience ; que, sa demande ayant été rejetée comme irrecevable au motif de l'absence des textes réglementaires nécessaires à l'entrée en vigueur de la procédure de validation des acquis de l'expérience pour le diplôme d'expertise comptable, M. B...a saisi le ministre chargé de l'éducation d'une demande tendant à ce que soient prises les mesures d'application permettant l'examen de sa demande d'obtention de ce diplôme ; qu'il demande l'annulation pour excès de pouvoir de la décision implicite de rejet née du silence gardé par la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche :<br/>
<br/>
              Sur la fin de non-recevoir soulevée par la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche :<br/>
<br/>
              2. Considérant que la communication, par la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, d'un simple projet d'arrêté relatif à l'obtention du diplôme d'expertise comptable par voie de la validation des acquis de l'expérience n'est pas de nature à priver le présent litige de son objet : que la fin de non-recevoir tirée de ce qu'il n'y aurait plus lieu de statuer sur la requête de M. B...doit, par suite, être écartée ; <br/>
<br/>
              Sur l'accès au diplôme d'expertise comptable par la validation des acquis de l'expérience :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes du quatrième alinéa de l'article L. 6111-1 du code du travail : " (...) toute personne engagée dans la vie active est en droit de faire valider les acquis de son expérience, notamment professionnelle ou liée à l'exercice de responsabilités syndicales " ; qu'aux termes de l'article L. 6412-1 du même code : " La validation des acquis de l'expérience est régie par les articles L. 335-5, L. 335-6, L. 613-3 et L. 613-4 du code de l'éducation " ; qu'aux termes de l'article L. 335-5 du code de l'éducation : "  I- Les diplômes ou les titres à finalité professionnelle sont obtenus par les voies scolaire et universitaire, par l'apprentissage, par la formation professionnelle continue ou, en tout ou en partie, par la validation des acquis de l'expérience. (...) / (...) Un décret en Conseil d'Etat détermine (...) les conditions dans lesquelles il peut être dérogé au I, pour des raisons tenant à la nature des diplômes ou titres en cause ou aux conditions d'exercice des professions auxquelles ils permettent d'accéder " ; qu'il résulte de ces dispositions que le législateur a prévu un droit à obtenir par la voie de la validation des acquis de l'expérience l'ensemble des diplômes et titres à finalité professionnelle, à l'exception de ceux pour lesquels un décret en Conseil d'Etat prévoit une dérogation pour des raisons tenant à leur nature ou aux conditions d'exercice des professions auxquelles ils permettent d'accéder ; que l'article R. 335-11 du code de l'éducation prévoit ainsi que les dérogations, prises individuellement pour chaque diplôme, font l'objet d'un arrêté conjoint des ministres concernés, pris après avis de la Commission nationale de la certification professionnelle ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 335-6 du code de l'éducation : " I. - Les diplômes et titres à finalité professionnelle délivrés au nom de l'Etat sont créés par décret et organisés par arrêté des ministres compétents (...) / II. - Il est créé un répertoire national des certifications professionnelles. Les diplômes et les titres à finalité professionnelle y sont classés par domaine d'activité et par niveau (...) ;  " ;  que le diplôme d'expertise comptable, qui est inscrit au répertoire national des certifications professionnelles, fait partie des diplômes et des titres à finalité professionnelle ;<br/>
<br/>
              5. Considérant qu'aucun arrêté n'a été adopté dans les conditions prévues par les dispositions de l'article R. 335-11 du code de l'éducation en vue de prévoir une dérogation au droit d'obtenir le diplôme d'expertise comptable par la voie de la validation des acquis de l'expérience ; que l'article 66 du décret du 30 mars 2012 relatif à l'activité d'expertise comptable dispose, au contraire, que : " Le diplôme d'expertise comptable est également délivré aux candidats dans le cadre de la procédure de validation des acquis de l'expérience " ; qu'il résulte, par suite, de ce qui a été dit aux points 2 et 3 que le diplôme d'expertise comptable doit pouvoir être obtenu par la voie de la validation des acquis de l'expérience ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              6. Considérant que l'exercice du pouvoir réglementaire comporte non seulement le droit mais aussi l'obligation de prendre dans un délai raisonnable les mesures qu'implique nécessairement l'application de la loi, hors le cas où le respect d'engagements internationaux de la France y ferait obstacle ; que, dès lors, le ministre chargé de l'éducation devait tirer toutes les conséquences nécessaires de la loi en rendant possible dans un délai raisonnable l'obtention de ce diplôme par la voie de la validation des acquis de l'expérience ; qu'à la date du 16 décembre 2015 à laquelle est née la décision de refus implicite de la ministre, elle avait disposé d'un temps suffisant pour s'acquitter de cette obligation ;<br/>
<br/>
              7. Considérant que les dispositions des articles R. 335-5 à R. 335-11 du code de l'éducation, issues du décret du 26 avril 2002 relatif à la validation des acquis de l'expérience et prises pour l'application des dispositions citées ci-dessus de l'article L. 335-5 du même code, fixent les modalités de validation des acquis de l'expérience professionnelle en prévoyant notamment les règles de composition du jury et ses attributions, ainsi que le contenu du dossier de demande de validation des acquis de l'expérience ; que l'article R. 335-7 dispose, à ce dernier titre, que : "  Les candidats adressent leur demande de validation des acquis de l'expérience à l'autorité ou à l'organisme qui délivre le diplôme, le titre ou le certificat de qualification, dans les délais et les conditions qu'il a préalablement fixés et rendus publics. / Un candidat ne peut déposer qu'une seule demande pendant la même année civile et pour le même diplôme, titre ou certificat de qualification. Pour des diplômes ou titres différents, il ne peut déposer plus de trois demandes au cours de la même année civile. Ces obligations, et l'engagement sur l'honneur du candidat à les respecter, doivent figurer sur chaque formulaire de candidature à une validation d'acquis. / La demande de validation des acquis de l'expérience précise le diplôme, le titre ou le certificat de qualification postulé, ainsi que le statut de la personne au moment de cette demande. Elle est accompagnée d'un dossier constitué par le candidat dont le contenu est fixé par l'autorité ou l'organisme délivrant le diplôme, le titre ou le certificat. Ce dossier comprend les documents rendant compte des expériences acquises dans les différentes activités salariées, non salariées ou bénévoles exercées par le candidat et leur durée, en relation avec la certification recherchée, ainsi que les attestations des formations suivies et des diplômes obtenus antérieurement " ;<br/>
<br/>
              8. Considérant qu'il résulte de ces dispositions que, contrairement à ce qui a été opposé à M. B...lors du dépôt de sa demande de validation des acquis de son expérience professionnelle, et alors même que l'arrêté du 28 mars 2014 relatif aux épreuves du diplôme d'expertise comptable, pris sur le fondement des dispositions citées ci-dessus de l'article L. 335-6 du code de l'éducation, n'a prévu, s'agissant de l'acquisition du diplôme par cette voie, ni les délais et conditions de transmission des dossiers ni les précisions quant à leur contenu, il n'était pas manifestement impossible d'examiner sa demande ; que s'il appartenait, par suite, à l'administration de procéder à l'examen du dossier de M.B..., le ministre n'était en revanche, pour le même motif, pas tenu de prendre les dispositions dont M. B...lui a demandé l'édiction ; <br/>
<br/>
              9. Considérant, par suite, que le moyen tiré de ce que le refus litigieux méconnaîtrait le principe d'égalité en ce que le diplôme d'expertise comptable ne pourrait pas être acquis par la voie de la validation des acquis de l'expérience professionnelle à la différence du diplôme de comptabilité et de gestion et du diplôme supérieur de comptabilité et de gestion doit, en tout état de cause, être écarté ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la requête de M. B... doit être rejetée, y compris, par voie de conséquence, ses conclusions tendant à ce qu'il soit enjoint à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche de publier les dispositions dont il lui a demandé l'adoption ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
