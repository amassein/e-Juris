<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034322695</ID>
<ANCIEN_ID>JG_L_2017_03_000000403257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/32/26/CETATEXT000034322695.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 29/03/2017, 403257</TITRE>
<DATE_DEC>2017-03-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403257.20170329</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de la Réunion, d'une part, de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, la décision du directeur régional de l'Office national des forêts du 24 mai 2016 refusant le renouvellement de la concession au titre de laquelle elle était autorisée à occuper un terrain sur le site de l'Anse des cascades à Sainte-Rose et à y exploiter un restaurant et, d'autre part, d'ordonner la poursuite des relations contractuelles. Par une ordonnance n° 1600771 du 24 août 2016, le juge des référés a fait droit à sa demande en suspendant la décision et en ordonnant la reprise des relations contractuelles à titre provisoire. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et trois nouveaux mémoires, enregistrés les 6 septembre, 20 septembre, 28 novembre et 21 décembre 2016 et le 18 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, l'Office national des forêts demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de Mme B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu<br/>
- le code général de la propriété des personnes publiques,<br/>
- le code forestier ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de l'Office National Des Forets et à la SCP Richard, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de la Réunion que le préfet de la Réunion et l'Office national des forêts, d'une part, et MmeB..., d'autre part, ont conclu une convention autorisant cette dernière à occuper, pour la période du 1er juillet 2007 au 30 juin 2016, un terrain de 675 mètres carrés situé au lieu-dit " l'Anse des cascades " dans la forêt de Sainte-Rose en vue de l'exploitation d'un établissement de restauration. L'article 4 de cette convention excluait expressément toute possibilité de tacite reconduction, tandis que son article 5 stipulait que " le renouvellement éventuel devra être sollicité par la titulaire au moins six mois avant la date d'expiration de la convention. Le défaut de présentation de cette demande dans le délai imparti vaut renonciation au renouvellement ". Par un courrier du 12 décembre 2015, Mme B...a sollicité le renouvellement de la convention en exécution de son article 5. Le 24 mai 2016, l'Office national des forêts a refusé d'y faire droit. Mme B...a saisi le tribunal administratif de la Réunion d'une demande d'annulation de cette décision. Elle a, en outre, présenté, sur le fondement de l'article L. 521-1 du code de justice administrative, une demande tendant, d'une part, à la suspension de cette décision et, d'autre part, à la poursuite des relations contractuelles. L'Office national des forêts se pourvoit en cassation contre l'ordonnance du 24 août 2016 par laquelle le juge des référés du tribunal administratif de la Réunion, après avoir estimé que la convention était un contrat administratif compte tenu des clauses qu'il contenait, a prononcé la suspension de cette décision et lui a enjoint de reprendre les relations contractuelles avec Mme B...à titre provisoire.<br/>
<br/>
              2. Le terme du contrat de Mme B...était fixé au 30 juin 2016. Si le juge du contrat était compétent pour connaître de la contestation par Mme B...de la validité du refus de l'Office national des Forêts de renouveler la convention, la demande de l'intéressée tendant à la suspension de la décision de refus de renouvellement a été présentée au juge des référés le 1er juillet. Cette demande était dès lors, en tout état de cause, dépourvue d'objet dès son introduction et n'était par suite pas recevable. En accueillant la demande de suspension dont il était saisi sans opposer cette irrecevabilité, qui ressortait des pièces du dossier qui lui était soumis, le juge des référés a entaché son ordonnance d'irrégularité. Celle-ci doit être annulée, sans qu'il soit besoin d'examiner les moyens soulevés par l'Office national des forêts à l'appui de son pourvoi.<br/>
<br/>
              3. Aux termes de l'article L. 821-2 du code de justice administrative, le Conseil d'Etat, s'il prononce l'annulation d'une décision d'une juridiction administrative statuant en dernier ressort, peut " régler l'affaire au fond si l'intérêt d'une bonne administration de la justice le justifie ". Dans les circonstances de l'espèce, il y a lieu de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              4. Il résulte de ce qui a été dit ci-dessus que les demandes présentées par Mme B...sur le fondement de l'article L. 521-1 du code de justice administrative sont, en tout état de cause,  irrecevables et doivent être rejetées.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme que l'Office national des forêts demande au titre de l'article L. 761 1 du code de justice administrative. Les dispositions du même article font par ailleurs obstacle à ce que la somme demandée au même titre par Mme B...soit mise à la charge de l'Office national des forêts, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1600771 du 24 août 2016 du juge des référés du tribunal administratif de la Réunion est annulée.<br/>
Article 2 : La demande présentée par Mme B...devant le juge des référés du tribunal administratif de la Réunion est rejetée.<br/>
Article 3 : Les conclusions présentées par l'Office national des forêts et Mme B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Office national des forêts et à Mme A...B....<br/>
Copie en sera adressée au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. COMPÉTENCE. - CONTESTATION PAR LE TITULAIRE D'UN CONTRAT ADMINISTRATIF D'UNE DÉCISION REJETANT SA DEMANDE DE RENOUVELLEMENT DU CONTRAT - COMPÉTENCE DU JUGE DU CONTRAT - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 39-08-005 Le juge du contrat est compétent pour connaître de la contestation par le titulaire d'un contrat administratif de la validité d'une décision rejetant la demande de renouvellement du contrat qu'il a présentée en application de l'une de ses clauses.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 4 mars 1981, Commune d'Azereix, n°s 13545; 17522, T. pp. 615-820-867.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
