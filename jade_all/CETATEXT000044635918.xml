<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044635918</ID>
<ANCIEN_ID>JG_L_2021_12_000000438492</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/63/59/CETATEXT000044635918.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 29/12/2021, 438492</TITRE>
<DATE_DEC>2021-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438492</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438492.20211229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. F... L... et le groupement agricole d'exploitation en commun (GAEC) de la Frête ont demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision du 20 mars 2015 par laquelle le préfet du Nord a autorisé la société civile d'exploitation agricole (SCEA) H... Père et Fille à exploiter 43,7529 hectares de terres sur le territoire des communes de Aibes, Ferrière-la-Petite, Obrechies et Quiévelon, ainsi que la décision implicite de rejet de leur recours gracieux. Par un jugement n° 1509522 du 28 juin 2018, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 18DA01773 du 10 décembre 2019, la cour administrative d'appel de Douai a rejeté l'appel formé par M. L... et le GAEC de la Frête contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 février et 29 avril 2020 et le 31 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. L... et le GAEC de la Frête demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de la SCEA H... Père et Fille et de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. F... L... et du GAEC de la Frête et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la SCEA H... Père et Fille.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'en application d'un bail conclu en 2006, le groupement agricole d'exploitation en commun (GAEC) de la Frête, dont M. L... est l'un des associés, exploitait des terres appartenant à M. et Mme H..., dans les communes de Ferrière-la-Petite et de Quiévelon (Nord). En 2014, afin de permettre l'installation de leur fille I..., M. et Mme H... ont décidé de reprendre leurs terres et ont, en conséquence, délivré son congé au GAEC de la Frête. Le GAEC de la Frête et M. L... ont contesté ce congé devant le tribunal paritaire des baux ruraux de Maubeuge.<br/>
<br/>
              2. Il ressort également des pièces du dossier qu'à la suite de cette reprise, le préfet du Nord a, par un arrêté du 5 février 2015, autorisé Mme I... H... à exploiter les parcelles antérieurement données à bail au GAEC de la Frête, puis, par un arrêté du 20 mars 2015, a autorisé la société civile d'exploitation agricole (SCEA) H... Père et Fille à réunir les exploitations individuelles de M. H... et de sa fille. Le GAEC de la Frête et M. L..., qui n'avaient pas attaqué l'arrêté du 5 février 2015, ont en revanche demandé au tribunal administratif de Lille d'annuler l'arrêté du 20 mars 2015.<br/>
<br/>
              3. Enfin, il ressort des pièces du dossier que, par un jugement du 21 mars 2016, le tribunal paritaire des baux ruraux de Maubeuge, saisi ainsi qu'il a été dit au point 1, a sursis à statuer sur la demande du GAEC de la Frête et de M. L... dans l'attente de la décision définitive à rendre par les juridictions administratives et que, par un jugement du 28 juin 2018, le tribunal administratif de Lille a rejeté la demande d'annulation présentée par le GAEC de la Frête et M. L... au motif qu'ils n'avaient pas d'intérêt leur donnant qualité pour agir contre l'arrêté préfectoral du 20 mars 2015 autorisant M. H... et sa fille à regrouper leurs terres.<br/>
<br/>
              4. Le GAEC de la Frête et M. L... se pourvoient en cassation contre l'arrêt du 10 décembre 2019 par lequel la cour administrative d'appel de Douai a rejeté leur appel dirigé contre ce jugement du 28 juin 2018 du tribunal administratif de Lille.<br/>
<br/>
              5. Il résulte des énonciations de l'arrêt attaqué que pour juger, comme l'avait fait le tribunal administratif, que M. L... et le GAEC de la Frête ne justifiaient pas d'un intérêt leur donnant qualité pour agir contre l'autorisation préfectorale délivrée le 20 mars 2015 à la SCEA H... Père et fille, la cour administrative d'appel s'est fondée, en premier lieu, sur ce que les requérants n'avaient pas déposé de candidature concurrente, en deuxième lieu, sur ce qu'ils n'avaient pas contesté l'autorisation délivrée à Mme I... H... le 5 février 2015 et, enfin, sur ce qu'ils ne pouvaient utilement se prévaloir, en raison du principe de l'indépendance de la législation du contrôle des structures des exploitations agricoles et de celle des baux ruraux, de ce que la validation de leur congé était toujours pendante devant le tribunal paritaire des baux ruraux.<br/>
<br/>
              6. Le preneur en place justifie d'un intérêt lui donnant qualité à agir contre l'autorisation donnée à un autre exploitant d'exploiter les parcelles qu'il loue, même s'il ne s'est pas porté candidat pour obtenir l'autorisation d'exploiter ces terres en application des dispositions des articles L. 331-1 et suivants du code rural et de la pêche maritime. Pour l'application de cette règle, le preneur auquel il a été donné congé mais dont la contestation du congé est pendante devant le juge compétent doit être regardé comme ayant le même intérêt pour agir contre une autorisation d'exploiter donnée à un nouvel exploitant.<br/>
<br/>
              7. Par suite, en jugeant, par les motifs exposés au point 5, que le GAEC de la Frête et M. L... n'avaient pas d'intérêt pour agir contre l'autorisation d'exploiter délivrée à la SCEA H... Père et Fille, alors qu'il ressortait des pièces du dossier qui lui était soumis que l'instance ouverte par ceux-ci devant le tribunal paritaire des baux ruraux était encore pendante devant ce tribunal, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. Est en effet sans incidence sur l'intérêt pour agir reconnu au preneur en place la circonstance qu'il n'aurait pas déposé une demande concurrente ou qu'il n'aurait pas contesté une précédente autorisation préfectorale portant sur les mêmes parcelles.<br/>
<br/>
              8. M. L... et le GAEC de la Frête sont, par suite, fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. L... et du GAEC de la Frête, qui ne sont pas la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCEA H... Père et Fille la somme de 1 500 euros à verser à M. L... et la somme de 1 500 euros à verser au GAEC de la Frête, au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 10 décembre 2019 de la cour administrative d'appel de Douai est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : Les conclusions de la SCEA Gillard Père et Fille tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La SCEA H... Père et Fille versera à M. L... et au GAEC de la Frête la somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. F... L..., premier requérant dénommé, et à la société civile d'exploitation agricole H... Père et Fille.<br/>
		Copie en sera adressée au ministre de l'agriculture et de l'alimentation. <br/>
              Délibéré à l'issue de la séance du 17 décembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. B... J..., M. Fabien Raynaud, présidents de chambre ; M. P... C..., Mme E... O..., M. D... M..., M. A... N... et Mme Bénédicte Fauvarque-Cosson, conseillers d'Etat et Mme Pearl Nguyên Duy, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 29 décembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Pearl Nguyên Duy<br/>
                 La secrétaire :<br/>
                 Signé : Mme K... G...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-03-01-06 AGRICULTURE ET FORÊTS. - EXPLOITATIONS AGRICOLES. - CUMULS ET CONTRÔLE DES STRUCTURES. - CUMULS D'EXPLOITATIONS. - CONTENTIEUX. - AUTORISATION D'EXPLOITER DES PARCELLES SUR DES TERRES - INTÉRÊT À AGIR DU PRENEUR EN PLACE - 1) PRINCIPE - EXISTENCE [RJ1] - 2) CAS DU PRENEUR AYANT REÇU CONGÉ - EXISTENCE, TANT QUE LA CONTESTATION DU CONGÉ EST PENDANTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04-02-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - INTÉRÊT POUR AGIR. - EXISTENCE D'UN INTÉRÊT. - INTÉRÊT LIÉ À UNE QUALITÉ PARTICULIÈRE. - AUTORISATION D'EXPLOITER DES PARCELLES SUR DES TERRES - INTÉRÊT À AGIR DU PRENEUR EN PLACE - 1) PRINCIPE - EXISTENCE [RJ1] - 2) CAS DU PRENEUR AYANT REÇU CONGÉ - EXISTENCE, TANT QUE LA CONTESTATION DU CONGÉ EST PENDANTE.
</SCT>
<ANA ID="9A"> 03-03-03-01-06 1) Le preneur en place justifie d'un intérêt lui donnant qualité à agir contre l'autorisation donnée à un autre exploitant d'exploiter les parcelles qu'il loue, même s'il ne s'est pas porté candidat pour obtenir l'autorisation d'exploiter ces terres en application des articles L. 331-1 et suivants du code rural et de la pêche maritime (CRPM). ......2) Pour l'application de cette règle, le preneur auquel il a été donné congé mais dont la contestation du congé est pendante devant le juge compétent doit être regardé comme ayant le même intérêt pour agir contre une autorisation d'exploiter donnée à un nouvel exploitant.</ANA>
<ANA ID="9B"> 54-01-04-02-01 1) Le preneur en place justifie d'un intérêt lui donnant qualité à agir contre l'autorisation donnée à un autre exploitant d'exploiter les parcelles qu'il loue, même s'il ne s'est pas porté candidat pour obtenir l'autorisation d'exploiter ces terres en application des articles L. 331-1 et suivants du code rural et de la pêche maritime (CRPM). ......2) Pour l'application de cette règle, le preneur auquel il a été donné congé mais dont la contestation du congé est pendante devant le juge compétent doit être regardé comme ayant le même intérêt pour agir contre une autorisation d'exploiter donnée à un nouvel exploitant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du propriétaire des terres, CE, 5 février 2020, M. Herbain, n° 419790, T. pp. 590- 887 ; s'agissant de l'exploitant d'autres parcelles des mêmes terres, CE, 5 février 2020, Ministre de l'agriculture et de l'alimentation c/ GAEC Coulangheon Frères, n° 418970, T. pp. 590-887.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
