<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031212529</ID>
<ANCIEN_ID>JG_L_2015_09_000000389494</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/21/25/CETATEXT000031212529.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 23/09/2015, 389494, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389494</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:389494.20150923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Office public d'aménagement et de construction (OPAC) de la Savoie a demandé au tribunal administratif de Grenoble, en premier lieu, de condamner solidairement M. B... A..., la société Artélia Ville et Transport, nouvelle dénomination de la société Sogreah, venant aux droits de la société Etudes et Projets, la commune de Chambéry et la société Eurovia, venant aux droits de la société Jean-Lefebvre Sud-Est, à lui verser les sommes de 2 684 535 euros, 92 614,99 euros et 55 288,31 euros en indemnisation des désordres affectant les voies et réseaux divers de la zone d'aménagement concertée (ZAC) du Covet à Chambéry, ainsi qu'une somme de 150 000 euros en réparation de l'atteinte portée à sa réputation par lesdits désordres, en deuxième lieu, d'assortir ces condamnations des intérêts de droit capitalisés et, en troisième lieu, de mettre à la charge des mêmes défendeurs les frais et honoraires d'expertise.<br/>
<br/>
              Par un jugement n° 1005904 du 26 août 2013, le tribunal administratif de Grenoble a, en premier lieu, condamné la sociétéA..., la commune de Chambéry et la société Eurovia à verser à l'OPAC de la Savoie la somme de 653 439,61 euros, avec intérêts capitalisés, en indemnisation des désordres de la première tranche de travaux, en deuxième lieu, condamné solidairement la société A...et la commune de Chambéry à verser à l'OPAC la somme de 618 122,89 euros, avec intérêts capitalisés, en indemnisation des désordres des dernières tranches de travaux, en troisième lieu, condamné la société A...et la société Artélia Ville et Transport à garantir, chacune en ce qui la concernait, la société Eurovia à hauteur de 40 % du montant de la condamnation prononcée à son encontre, en quatrième lieu, condamné la commune de Chambéry à garantir la société Eurovia à hauteur de 10 % du montant de la même condamnation, en cinquième lieu, rejeté les conclusions d'appel en garantie formées par la société A...contre la société Artélia Ville et Transport et contre la société MAF et, en dernier lieu, mis les frais et honoraires d'expertise à la charge de la sociétéA..., de la commune de Chambéry et de la société Eurovia.<br/>
<br/>
              La société A...et la commune de Chambéry ont relevé appel de ce jugement.<br/>
<br/>
              Par un arrêt n°s 13LY02646, 13LY02817, 14LY00258 du 12 février 2015, la cour administrative d'appel de Lyon a, en premier lieu, porté à 1 098 360,60 euros la somme de 653 439,61 euros que la sociétéA..., la commune de Chambéry et la société Eurovia ont été solidairement condamnées à verser à l'OPAC de la Savoie par le jugement du 26 août 2013 du tribunal administratif de Grenoble, en deuxième lieu, ramené à 559 649,79 euros la somme de 618 122,89 euros que la société A...et la commune de Chambéry ont été solidairement condamnées à verser à l'OPAC de la Savoie par ce même jugement et, en dernier lieu, rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 avril et 15 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la société Artélia Ville et Transport demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge séparément ou conjointement de l'OPAC de la Savoie, de la commune de Chambéry, de la société Eurovia et de la société A...le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de la société Artélia Ville et Transport ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ; <br/>
<br/>
              2. Considérant que, pour demander l'annulation de l'arrêt qu'elle attaque, la société Artélia Ville et Transport soutient que la cour administrative d'appel de Lyon a entaché son arrêt d'une insuffisance de motivation et d'une omission à statuer en ne se prononçant pas sur le partage de responsabilité effectué en première instance entre les participants à l'exécution de la première tranche de travaux ; que la cour a commis une erreur de droit en jugeant que l'OPAC de la Savoie devait être exonéré de toute responsabilité en ce qui concernait la première tranche de travaux, alors qu'il était pourtant averti des problèmes de stabilité du sol ; que la cour a commis une erreur de droit et dénaturé les pièces du dossier en jugeant que le coût des mesures de confortement du tréfonds devait être mis à la charge des maîtres d'oeuvre ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt en tant qu'il a porté le montant de la garantie due au titre de la première tranche de travaux de 431 020 euros à 868 528 euros ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société Artélia Ville et Transport qui sont dirigées contre l'arrêt attaqué en tant qu'il a porté sur le montant de la garantie due par la société requérante au titre de la première tranche de travaux de la somme de 431 020 euros à la somme de 868 528 euros sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Artélia Ville et Transport n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société Artélia Ville et Transport.<br/>
Copie en sera adressée pour information à l'OPAC de la Savoie, à la commune de Chambéry, à la société A...et à la société Eurovia Alpes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
