<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043867913</ID>
<ANCIEN_ID>JG_L_2021_07_000000446979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/86/79/CETATEXT000043867913.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 12/07/2021, 446979, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DESCORPS-DECLÈRE</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois De Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2021:446979.20210712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. Sous le n°446979, par une requête, enregistrée le 27 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 1er octobre 2020, par laquelle le ministre de l'intérieur lui a refusé l'accès aux données susceptibles de la concerner figurant dans le traitement automatisé de données à caractère personnel dénommé fichier des personnes recherchées ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de procéder à l'effacement des données personnelles la concernant figurant dans ce fichier dans un délai de deux mois à compter de la notification du jugement à intervenir sous astreinte de 50 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              II. Sous le n°449218, par une requête, enregistrée le 1er février 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision, révélée par le courrier de la présidente de la Commission nationale de l'informatique et des libertés (CNIL) du 16 décembre 2020, par laquelle le ministre de l'intérieur lui a refusé l'accès aux données susceptibles de la concerner figurant dans le traitement automatisé de données à caractère personnel CRISTINA mis en oeuvre par la direction générale de la sécurité intérieure (DGSI) ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de communiquer les actes autorisant la création du fichier CRISTINA ;<br/>
<br/>
              3°) de l'informer de l'existence d'informations la concernant le cas échéant dans le fichier CRISTINA ;<br/>
<br/>
              4°) d'enjoindre à l'Etat d'effacer les données personnelles susceptibles de la concerner dans le fichier CRISTINA ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - la loi n° 78-17 du 6 janvier 1978, modifiée notamment par la loi n° 2018-693 du 20 juin 2018 et l'ordonnance n° 2018-1125 du 12 décembre 2018 ; <br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le décret n° 2010-569 du 28 mai 2010 ;<br/>
              - le décret n° 2019-536 du 29 mai 2019<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, d'une part, Mme B... et Me C..., son avocat, et d'autre part, le ministre de l'intérieur et la Commission nationale de l'informatique et des libertés, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, conseiller d'Etat,<br/>
<br/>
              - et, hors la présence des parties, les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes enregistrées au secrétariat du contentieux du Conseil d'Etat sous les n°s 446979, 449218 concernent la même requérante et présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. En vertu de l'article 31 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et intéressant la sûreté de l'Etat, la défense ou la sécurité publique sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé de la Commission nationale de l'informatique et des libertés (CNIL), publié avec l'arrêté autorisant le traitement. Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 6 de la même loi doivent être autorisés par décret en Conseil d'Etat pris après avis motivé de la Commission, publié avec ce décret. Un décret en Conseil d'Etat peut dispenser de publication l'acte réglementaire autorisant la mise en oeuvre de ces traitements. Le sens de l'avis émis par la CNIL est alors publié avec ce décret.<br/>
<br/>
              3. L'article L. 841-2 du code de la sécurité intérieure prévoit que le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre du droit d'accès aux traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et intéressant la sûreté de l'Etat ou la défense, dont la liste est fixée par décret en Conseil d'Etat. En vertu de l'article R. 841-2 du même code, figurent notamment au nombre de ces traitements le fichier CRISTINA et le fichier des personnes recherchées (FPR) pour les seules données intéressant la sûreté de l'Etat mentionnées au 8° du III de l'article 2 du décret du 28 mai 2010 susvisé.<br/>
<br/>
              3. L'article L. 773-8 du code de justice administrative dispose que, lorsqu'elle traite des requêtes relatives à la mise en oeuvre du droit d'accès mentionné au point 2, " la formation de jugement se fonde sur les éléments contenus, le cas échéant, dans le traitement sans les révéler ni révéler si le requérant figure ou non dans le traitement. Toutefois, lorsqu'elle constate que le traitement ou la partie de traitement faisant l'objet du litige comporte des données à caractère personnel le concernant qui sont inexactes, incomplètes, équivoques ou périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite, elle en informe le requérant, sans faire état d'aucun élément protégé par le secret de la défense nationale. Elle peut ordonner que ces données soient, selon les cas, rectifiées, mises à jour ou effacées. Saisie de conclusions en ce sens, elle peut indemniser le requérant ". L'article R. 773-20 du même code précise que : " Le défendeur indique au Conseil d'Etat, au moment du dépôt de ses mémoires et pièces, les passages de ses productions et, le cas échéant, de celles de la Commission nationale de contrôle des techniques de renseignement, qui sont protégés par le secret de la défense nationale. /Les mémoires et les pièces jointes produits par le défendeur et, le cas échéant, par la Commission nationale de contrôle des techniques de renseignement sont communiqués au requérant, à l'exception des passages des mémoires et des pièces qui, soit comportent des informations protégées par le secret de la défense nationale, soit confirment ou infirment la mise en oeuvre d'une technique de renseignement à l'égard du requérant, soit divulguent des éléments contenus dans le traitement de données, soit révèlent que le requérant figure ou ne figure pas dans le traitement. /Lorsqu'une intervention est formée, le président de la formation spécialisée ordonne, s'il y a lieu, que le mémoire soit communiqué aux parties, et à la Commission nationale de contrôle des techniques de renseignement, dans les mêmes conditions et sous les mêmes réserves que celles mentionnées à l'alinéa précédent ".<br/>
<br/>
              4. Il ressort des pièces du dossier que Mme B... a saisi la CNIL afin de pouvoir accéder aux données susceptibles de la concerner figurant dans le fichier CRISTINA et dans le FPR. La Commission a désigné, en application de l'article 41 de la loi du 6 janvier 1978, alors applicable, un membre pour mener toutes investigations utiles et faire procéder, le cas échéant, aux modifications nécessaires. Par deux lettres du 1er octobre 2020 et du 16 décembre 2020, la présidente de la Commission a informé Mme B... qu'il avait été procédé à l'ensemble des vérifications demandées et que la procédure était terminée, sans lui apporter d'autres informations. Mme B... demande l'annulation du refus, révélé par ces courriers, du ministre de l'intérieur de lui donner accès aux données susceptibles de la concerner et figurant dans le fichier litigieux et d'enjoindre au ministre de les effacer.<br/>
<br/>
              5. Le ministre de l'intérieur a communiqué au Conseil d'Etat, dans les conditions prévues à l'article R. 773-20 du code de justice administrative, les éléments susceptibles d'être relatifs à la situation de l'intéressée. Le ministre a, en outre, communiqué l'acte réglementaire, non publié, créant le fichier CRISTINA.<br/>
<br/>
              6. Il appartient à la formation spécialisée, créée par l'article L. 773-2 du code de justice administrative précité, saisie de conclusions dirigées contre le refus de communiquer les données relatives à une personne qui allègue être mentionnée dans un fichier figurant à l'article R. 841-2 du code de la sécurité intérieure, de vérifier, au vu des éléments qui lui ont été communiqués hors la procédure contradictoire, si le requérant figure ou non dans le fichier litigieux. Dans l'affirmative, il lui appartient d'apprécier si les données y figurant sont pertinentes au regard des finalités poursuivies par ce fichier, adéquates et proportionnées. Pour ce faire, elle peut relever d'office tout moyen ainsi que le prévoit l'article L. 773-5 du code de justice administrative. Lorsqu'il apparaît soit que le requérant n'est pas mentionné dans le fichier litigieux soit que les données à caractère personnel le concernant qui y figurent ne sont entachées d'aucune illégalité, la formation de jugement rejette les conclusions du requérant sans autre précision. Dans le cas où des informations relatives au requérant figurent dans le fichier litigieux et apparaissent entachées d'illégalité soit que les données à caractère personnel le concernant sont inexactes, incomplètes, équivoques ou périmées soit que leur collecte, leur utilisation, leur communication ou leur consultation est interdite, elle en informe le requérant sans faire état d'aucun élément protégé par le secret de la défense nationale. Cette circonstance, le cas échéant relevée d'office par le juge dans les conditions prévues à l'article R. 773-21 du code de justice administrative, implique nécessairement que l'autorité gestionnaire du fichier rétablisse la légalité en effaçant ou en rectifiant, dans la mesure du nécessaire, les données illégales. Dans pareil cas, doit être annulée la décision implicite refusant de procéder à un tel effacement ou à une telle rectification.<br/>
<br/>
              7. La formation spécialisée a procédé à l'examen de l'acte réglementaire autorisant la création du fichier litigieux ainsi que des éléments fournis par le ministre et par la CNIL, laquelle a effectué les diligences qui lui incombent dans le respect des règles de compétence et de procédure applicables. Il résulte de cet examen, qui s'est déroulé selon les modalités décrites au point précédent, qui n'ont révélé aucune illégalité, notamment au regard du droit à la protection de la vie privée et familiale garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de la présomption d'innocence, que les conclusions de Mme B..., y compris ses conclusions à fin d'injonction et d'application de l'article L. 761-1 du code de justice administrative, doivent être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les requêtes de Mme B... sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à Mme A... B... et au ministre de l'intérieur.<br/>
Copie en sera adressée à la Commission nationale de l'informatique et des libertés.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
