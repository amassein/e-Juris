<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028253841</ID>
<ANCIEN_ID>JG_L_2013_11_000000334215</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/25/38/CETATEXT000028253841.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 26/11/2013, 334215, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334215</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON, CAPRON ; SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2013:334215.20131126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, avec les pièces qui y sont visées, la décision du 28 novembre 2011 par laquelle le Conseil d'Etat, statuant au contentieux sur la requête enregistrée sous le numéro 334215 présentée par la société coopérative agricole Ukl-Arrée et tendant à l'annulation, pour excès de pouvoir, d'une part, de la décision implicite du 29 août 2009 par laquelle a été réputée acceptée la demande du comité interprofessionnel de la dinde française (CIDEF) tendant à l'extension de l'avenant du 5 novembre 2008 complétant l'accord interprofessionnel de la dinde française du 18 octobre 2007 et maintenant le montant de la cotisation interprofessionnelle à 14 euros pour 1000 dindonneaux pour l'année 2009, d'autre part, de l'avis publié au Journal officiel du 30 septembre 2009 relatif à la décision tacite d'extension de cet avenant, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir si l'article 107 du traité sur le fonctionnement de l'Union européenne, lu à la lumière de l'arrêt du 15 juillet 2004 Pearle BV e. a. (C-345/02), doit être interprété en ce sens que la décision d'une autorité nationale étendant à l'ensemble des professionnels d'une filière un accord qui, comme l'accord conclu au sein du comité interprofessionnel de la dinde française (CIDEF), institue une cotisation dans le cadre d'une organisation interprofessionnelle reconnue par l'autorité nationale et la rend ainsi obligatoire, en vue de permettre la mise en oeuvre d'actions de communication, de promotion, de relations extérieures, d'assurance qualité, de recherche, de défense des intérêts du secteur, ainsi que l'acquisition d'études et de panels de consommateurs, est, eu égard à la nature des actions en cause, aux modalités de leur financement et aux conditions de leur mise en oeuvre, relative à une aide d'Etat ;<br/>
<br/>
              Vu l'arrêt du 30 mai 2013 par lequel la Cour de justice de l'Union européenne s'est prononcée sur cette question ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la Société Cooperative Agricole Ukl-Arrée et à la SCP Capron, Capron, avocat du comité interprofessionnel de la dinde française ;  <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l' article L. 632-1 du code rural, dans sa rédaction applicable à la date de la décision d'extension attaquée : " I. Les groupements constitués à leur initiative par les organisations professionnelles les plus représentatives de la production agricole et, selon les cas, de la transformation, de la commercialisation et de la distribution peuvent faire l'objet d'une reconnaissance en qualité d'organisations interprofessionnelles par l'autorité administrative compétente (...), soit au niveau national, soit au niveau d'une zone de production, par produit ou groupe de produits déterminés (...) " ; que selon l'article L. 632-3 du même code, dans sa rédaction applicable en l'espèce : " Les accords conclus dans le cadre d'une organisation interprofessionnelle reconnue peuvent être étendus, pour une durée déterminée, en tout ou partie, par l'autorité administrative compétente lorsqu'ils tendent, par des contrats types, des conventions de campagne et des actions communes ou visant un intérêt commun conformes à l'intérêt général et compatibles avec les règles de la politique agricole commune, à favoriser notamment : /1° La connaissance de l'offre et de la demande ; / 2° L'adaptation et la régularisation de l'offre ; / 3° La mise en oeuvre, sous le contrôle de l'Etat, de règles de mise en marché, de prix et de conditions de paiement (...) ; / 4° La qualité des produits (...) ; / 5° Les relations interprofessionnelles dans le secteur intéressé (...) ; / 6° L'information relative aux filières et aux produits ainsi que leur promotion sur les marchés intérieur et extérieurs ; / 7° Les démarches collectives visant à lutter contre les risques et aléas liés à la production, à la transformation, à la commercialisation et à la distribution (...) ; / 8° La lutte contre les organismes nuisibles (...) ; / 9° Le développement des valorisations non alimentaires des produits ; / 10° La participation aux actions internationales de développement ; (...) " ; que l'article L. 632-6 du même code précise que : " Les organisations interprofessionnelles reconnues (...) sont habilitées à prélever, sur tous les membres des professions les constituant, des cotisations résultant des accords étendus selon la procédure fixée aux articles L. 632-3 et L. 632-4 et qui, nonobstant leur caractère obligatoire, demeurent des créances de droit privé)(... " ;<br/>
<br/>
<br/>
              2. Considérant que, par un accord interprofessionnel adopté le 18 octobre 2007, le comité interprofessionnel de la dinde française (CIDEF), qui a été reconnu comme organisation interprofessionnelle dans le secteur de la viande de dinde par un arrêté interministériel du 24 juin 1976, a instauré une cotisation interprofessionnelle prélevée sur chacun des membres des professions représentées au sein de cette organisation interprofessionnelle ; que, par un avenant conclu le même jour complétant cet accord, il a fixé le montant de cette cotisation à 14 euros pour 1 000 dindonneaux pour l'année 2008 ; que, par deux arrêtés du 13 mars 2008, le ministre de l'économie, des finances et de l'emploi et le ministre de l'agriculture et de la pêche ont étendu l'accord interprofessionnel du 18 octobre 2007 pour une durée de trois ans et l'avenant du 18 octobre 2007 pour une durée d'un an ; que, par un nouvel avenant à l'accord interprofessionnel du 18 octobre 2007, conclu le 5 novembre 2008, le CIDEF a décidé de maintenir au même montant la cotisation interprofessionnelle pour l'année 2009 ; que la société coopérative agricole Ukl-Arrée, productrice de dindes, doit être regardée comme demandant l'annulation de la décision tacite d'extension de l'avenant du 5 novembre 2008 ;<br/>
<br/>
              3. Considérant que, dans l'arrêt du 30 mai 2013 par lequel elle s'est prononcée sur la question dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que la décision d'une autorité nationale étendant à l'ensemble des professionnels d'une filière agricole un accord qui institue une cotisation dans le cadre d'une organisation interprofessionnelle reconnue par l'autorité nationale et la rend ainsi obligatoire en vue de permettre la mise en oeuvre d'actions de communication, de promotion, de relations extérieures, d'assurance qualité, de recherche et de défense des intérêts du secteur concerné ne constitue pas un élément d'une aide d'Etat, au motif, d'une part, que le mécanisme d'une telle cotisation n'implique aucun transfert direct ou indirect de ressources d'Etat, d'autre part, que les autorités nationales ne peuvent effectivement utiliser les ressources provenant d'une telle cotisation, enfin, que les actions menées par l'organisation interprofessionnelle ne sont pas imputables à l'Etat ; <br/>
<br/>
              4. Considérant qu'il suit de là que la décision attaquée, qui étend, en application des dispositions des articles L. 632-3 et L. 632-4 du code rural, un accord instituant des cotisations dans le cadre d'une organisation interprofessionnelle agricole, ne saurait être regardé comme étant relative à une aide d'Etat, dès lors que les ressources collectées grâce aux cotisations et les actions financées par ces ressources ne se traduisent par aucune dépense supplémentaire ou atténuation de recettes pour l'Etat, d'autres collectivités publiques ou des personnes agissant pour leur compte et que les actions financées par les cotisations sont établies et mises en oeuvre de façon autonome par le CIDEF, sans être soumises à un contrôle autre que de régularité et de conformité à la loi et sans que le produit des cotisations soit jamais mis à la disposition des autorités publiques ; que, par suite, le moyen tiré de ce que cette décision aurait dû faire l'objet d'une notification préalable à la Commission européenne en application de l'article 88 du traité instituant la Communauté européenne, devenu l'article 108 du traité sur le fonctionnement de l'Union européenne, ne peut qu'être écarté ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la société coopérative agricole Ukl-Arrée n'est pas fondée à demander l'annulation de la décision d'extension de l'avenant du 5 novembre 2008 ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société la somme de 3 000 euros à verser au CIDEF au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                  --------------<br/>
Article 1er : La requête de la société coopérative agricole Ukl-Arrée est rejetée.<br/>
<br/>
Article 2 : La société coopérative agricole Ukl-Arrée versera au comité interprofessionnel de la dinde française la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société coopérative agricole Ukl-Arrée, au ministre de l'agriculture, de l'agroalimentaire et de la forêt et au comité interprofessionnel de la dinde française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
