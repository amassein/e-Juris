<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033789067</ID>
<ANCIEN_ID>JG_L_2016_12_000000403563</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/90/CETATEXT000033789067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 28/12/2016, 403563</TITRE>
<DATE_DEC>2016-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403563</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Philippe Mochon</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2016:403563.20161228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un jugement n° 1601980 du 13 septembre 2016, enregistré le 16 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif d'Amiens, avant de statuer sur la demande de Mme A...B...tendant à l'annulation pour excès de pouvoir de l'arrêté du 31 mai 2016 par lequel le préfet de l'Oise lui a refusé la délivrance d'un titre de séjour, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé son pays de destination, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question suivante : <br/>
<br/>
              " Les stipulations de l'accord conclu entre la France et le Cap-Vert se bornent-elles, concernant le titre de séjour " salarié ", à dresser une liste de métiers pour lesquels la situation de l'emploi en France ne peut être opposée aux ressortissants cap-verdiens occupant ces métiers, permettant ainsi l'application des dispositions prévues par la législation française relative au séjour des étrangers, en particulier de l'article L. 313-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, aux ressortissants cap-verdiens n'occupant pas ces métiers, ou les stipulations de l'accord régissent-elles de manière exclusive la délivrance des titres de séjour temporaires aux ressortissants cap-verdiens, seuls pouvant bénéficier des dispositions de droit commun ceux de ces ressortissants occupant l'un des métiers énumérés en annexe II de l'accord et dépassant le contingent annuel fixé par l'article 3.2.3 de l'accord ' "<br/>
<br/>
<br/>
     	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'accord du 24 novembre 2008 entre le Gouvernement de la République française et le Gouvernement de la République du Cap-Vert relatif à la gestion concertée des flux migratoires et au développement solidaire ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Jean-Philippe Mochon, conseiller d'Etat, <br/>
- les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT :<br/>
<br/>
<br/>
              1. Les dispositions du code de l'entrée et du séjour des étrangers et du droit d'asile, s'appliquent, ainsi que le rappellent les dispositions de l'article L. 111-2 de ce code, " sous réserve des conventions internationales ".<br/>
<br/>
              2. L'article L. 313-10 du même code dispose, dans sa rédaction applicable au litige dont est saisi le tribunal administratif d'Amiens, que : " La carte de séjour temporaire autorisant l'exercice d'une activité professionnelle est délivrée : / 1° A l'étranger titulaire d'un contrat de travail visé conformément aux dispositions de l'article L. 341-2 du code du travail. / Pour l'exercice d'une activité professionnelle salariée dans un métier et une zone géographique caractérisés par des difficultés de recrutement et figurant sur une liste établie au plan national par l'autorité administrative, après consultation des organisations syndicales d'employeurs et de salariés représentatives, l'étranger se voit délivrer cette carte sans que lui soit opposable la situation de l'emploi sur le fondement du même article L. 341-2. / La carte porte la mention "salarié" lorsque l'activité est exercée pour une durée supérieure ou égale à douze mois. Elle porte la mention "travailleur temporaire" lorsque l'activité est exercée pour une durée déterminée inférieure à douze mois ".<br/>
<br/>
              3. L'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " La carte de séjour temporaire mentionnée à l'article L. 313-11 ou la carte de séjour temporaire mentionnée au 1° de l'article L. 313-10 peut être délivrée, sauf si sa présence constitue une menace pour l'ordre public, à l'étranger ne vivant pas en état de polygamie dont l'admission au séjour répond à des considérations humanitaires ou se justifie au regard des motifs exceptionnels qu'il fait valoir, sans que soit opposable la condition prévue à l'article L. 311-7 ". <br/>
<br/>
              4. En ce qui concerne les ressortissants cap-verdiens, l'accord du 24 novembre 2008 entre la France et le Cap-Vert relatif à la gestion concertée des flux migratoires et au développement solidaire traite, dans son point 3.2, de l'immigration pour motifs professionnels. Les articles 3.2.1 et 3.2.2 sont consacrés respectivement aux échanges de jeunes professionnels, dans la limite d'un contingent de 100 jeunes professionnels admis chaque année, et, d'autre part, à la carte " compétences et talents ", dans la limite de 100 cartes susceptibles d'être délivrés chaque année par la France. L'accord stipule dans son article 3.2.3, relatif au titre de séjour portant la mention " salarié ", qu'un titre d'une durée d'un an renouvelable " est délivré à un ressortissant cap-verdien en vue de l'exercice, sur l'ensemble du territoire métropolitain de la France, de l'un des métiers énumérés en annexe II au présent accord sur présentation d'un contrat de travail visé par l'autorité française compétente sans que soit prise en compte la situation de l'emploi. (...) Pour faciliter la formation professionnelle, l'accueil et l'insertion en France des intéressés, le nombre de titres de séjour temporaires mentionnés au premier alinéa du présent paragraphe susceptibles d'être délivrés chaque année par la France à des ressortissants du Cap-Vert est limité à 500 ". L'article 3.2.4 du même accord stipule que : " Les ressortissants cap-verdiens, qui ne pourraient bénéficier des dispositions prévues aux paragraphes 3.2.1 à 3.2.3 pour la seule raison d'un dépassement des contingents indiqués dans ces paragraphes, pourront toutefois bénéficier des dispositions de droit commun prévues par la législation française en matière d'immigration professionnelle ".<br/>
<br/>
              5. Ces stipulations de l'accord entre la France et le Cap-Vert se bornent, en ce qui concerne l'admission au séjour des ressortissants cap-verdiens en qualité de salarié, à fixer les conditions dans lesquelles ces ressortissants peuvent bénéficier, dans la limite d'un contingent annuel, de la délivrance d'un titre de séjour en cette qualité sans se voir opposer la situation de l'emploi en France, et à préciser les conditions d'application des dispositions de droit commun en matière d'immigration professionnelle en cas de dépassement de ce contingent. Si elles ouvrent la possibilité pour les ressortissants cap-verdiens qui rempliraient l'ensemble des conditions posées par les articles 3.2.1 à 3.2.3, en cas de dépassement du contingent annuel, de bénéficier de l'application des dispositions de la législation nationale, elles n'ont pas pour objet ni pour effet de régir entièrement la situation des ressortissants cap-verdiens pour l'accès au séjour en qualité de salarié.<br/>
<br/>
              6. Par suite, les stipulations de l'accord entre la France et le Cap-Vert du 24 novembre 2008 n'excluent pas l'application de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile relatives à l'admission exceptionnelle au séjour aux ressortissants cap-verdiens demandant un titre de séjour en qualité de salarié et ne remplissant pas les conditions posées par l'article 3.2.3 de l'accord, notamment au regard de la liste des emplois énumérés dans l'accord.<br/>
<br/>
<br/>
<br/>
<br/>
              7. Le présent avis sera notifié au tribunal administratif d'Amiens, à Mme A...B...et au ministre de l'intérieur.<br/>
              Il sera publié au Journal officiel de la République française. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. TEXTES APPLICABLES. CONVENTIONS INTERNATIONALES. - ACCORD ENTRE LA FRANCE ET LE CAP-VERT DU 24 NOVEMBRE 2008 - STIPULATIONS NE RÉGISSANT PAS ENTIÈREMENT L'ACCÈS AU SÉJOUR EN QUALITÉ DE SALARIÉ - CONSÉQUENCE - APPLICATION DE L'ARTICLE L. 313-14 DU CESEDA - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 335-01-01-02 Les articles 3.2.1 à 3.2.4 de l'accord entre la France et le Cap-Vert se bornent, en ce qui concerne l'admission au séjour des ressortissants cap-verdiens en qualité de salarié, à fixer les conditions dans lesquelles ces ressortissants peuvent bénéficier, dans la limite d'un contingent annuel, de la délivrance d'un titre de séjour en cette qualité sans se voir opposer la situation de l'emploi en France, et à préciser les conditions d'application des dispositions de droit commun en matière d'immigration professionnelle en cas de dépassement de ce contingent. S'ils ouvrent la possibilité pour les ressortissants cap-verdiens qui rempliraient l'ensemble des conditions posées par les articles 3.2.1 à 3.2.3, en cas de dépassement du contingent annuel, de bénéficier de l'application des dispositions de la législation nationale, ils n'ont pas pour objet ni pour effet de régir entièrement la situation des ressortissants cap-verdiens pour l'accès au séjour en qualité de salarié.,,,Par suite, les stipulations de l'accord entre la France et le Cap-Vert du 24 novembre 2008 n'excluent pas l'application de l'article L. 313-14 du code de l'entrée et du séjour des étrangers en France relatives à l'admission exceptionnelle au séjour aux ressortissants cap-verdiens demandant un titre de séjour en qualité de salarié et ne remplissant pas les conditions posées par l'article 3.2.3 de l'accord, notamment au regard de la liste des emplois énumérés dans l'accord.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour le Sénégal, CE, 9 novembre 2015, M. Yatabaré, n° 391429, T. p. 705 ; pour le Mali, CE, avis, 7 mai 2013, M. Dembélé, n° 366481, T. p. 630 ; pour le Bénin, CE, avis, 5 juillet 2013, M. Houeto, n° 367908, T. p. 630. Comp., pour l'accord franco-algérien du 27 décembre 1968, CE, Avis, 22 mars 2010, Djilali Saou, n° 333679, p. 83 ; pour l'accord franco-tunisien du 17 mars 1988, CE, 2 mars 2012, M. Lahouel, n° 355208, T. p. 793 ; pour l'accord franco-marocain du 9 octobre 1987, CE, 31 janvier 2014, Ministre de l'intérieur c/ M. Nassiri, n° 367306, T. p. 698.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
