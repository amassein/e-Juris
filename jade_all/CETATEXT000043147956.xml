<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043147956</ID>
<ANCIEN_ID>JG_L_2021_02_000000439435</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/14/79/CETATEXT000043147956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 16/02/2021, 439435, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439435</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439435.20210216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              La société Catalogne Informations a demandé à la cour administrative d'appel de Paris, d'une part, d'annuler pour excès de pouvoir la décision du 26 septembre 2018 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a rejeté le recours qu'elle a formé contre la décision du 13 mars 2018 par laquelle le comité territorial de l'audiovisuel de Toulouse a rejeté sa demande tendant à la modification du code " PS " attribué au service qu'elle exploite, d'autre part, d'enjoindre au CSA de réexaminer ce recours. Par un arrêt n° 18PA03841 du 6 février 2020, la cour administrative d'appel a fait droit à sa demande.<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 10 mars et 2 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, le CSA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société Catalogne Informations ;<br/>
<br/>
              3°) de mettre à la charge de la société Catalogne Informations la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de Mme A... B..., rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel et à la SCP Lyon-Caen, Thiriez, avocat de la société Catalogne Informations.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 26 septembre 2018, le Conseil supérieur de l'audiovisuel (CSA) a rejeté le recours formé par la société Catalogne Informations, qui exploite le service de radio de catégorie B dénommé " 100 % Pays Catalan ", contre la décision du 13 mars 2018 par laquelle le comité territorial de l'audiovisuel de Toulouse a rejeté sa demande tendant à la modification du code " PS " attribué à ce service, correspondant à l'affichage du nom de programme de ce service de radio. Le CSA demande l'annulation de l'arrêt du 6 février 2020 par lequel la cour administrative d'appel de Paris, sur demande de la société Catalogne Informations, a annulé cette décision et lui a enjoint de réexaminer le recours de la société. <br/>
<br/>
              2. Aux termes du dernier alinéa de l'article 4 de la loi du 30 septembre 1986 relative à la liberté de communication : " Le Conseil supérieur de l'audiovisuel ne peut délibérer que si quatre au moins de ses membres sont présents. Il délibère à la majorité des membres présents. (...) ". <br/>
<br/>
              3. Ni ces dispositions ni aucune autre disposition législative ou réglementaire ni aucun principe général n'impose que les procès-verbaux des réunions du collège du CSA portent mention de sa composition ou de ce que ses délibérations ont été approuvées à la majorité des membres présents.<br/>
<br/>
              4. Il ressort des termes de l'arrêt attaqué que la cour a déduit l'illégalité de la décision en litige de la circonstance que ni le procès-verbal de la réunion du collège plénier du CSA du 26 septembre 2018 ni aucun autre élément produit par le CSA en défense ne permettaient d'établir qu'elle avait été adoptée conformément aux règles de majorité applicables. En statuant ainsi, alors que, d'une part, il est constant que le procès-verbal de la réunion du collège plénier porte mention de ce que le projet de délibération rejetant le recours formé par la société Catalogne Informations a été approuvé, et que, d'autre part, il résulte de ce qui a été dit au point précédent que, en l'absence de tout élément de preuve contraire, les mentions du procès-verbal d'adoption d'une décision établissent la régularité de la procédure suivie au regard des dispositions du dernier alinéa de l'article 4 de la loi du 30 septembre 1986, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, le CSA est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Catalogne Information la somme de 1 500 euros à verser au CSA au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mise à la charge du CSA, qui n'est pas la partie perdante dans la présente instance, la somme que demande, au même titre, la société Catalogne Informations.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 6 février 2020 est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : La société Catalogne Informations versera au CSA la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par la société Catalogne Informations au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au Conseil supérieur de l'audiovisuel et à la société Catalogne Informations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
