<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030459165</ID>
<ANCIEN_ID>JG_L_2015_04_000000372435</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/45/91/CETATEXT000030459165.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 03/04/2015, 372435, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372435</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:372435.20150403</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société civile de construction vente Lisieux Développement, dont le siège est 123 rue du Château, à Boulogne-Billancourt (92100) ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision 25 juin 2013 par laquelle la Commission nationale d'aménagement commercial, statuant sur les recours, d'une part, des associations CAP Lisieux et Lisieux au Coeur et, d'autre part, de la SAS Bricorama France dirigés contre la décision du 6 février 2013 de la commission départementale d'aménagement commercial du Calvados autorisant la société requérante à procéder, à Glos (Calvados), à la création d'un ensemble commercial dénommé " L'Ellipse " au sein de la zone d'activités commerciales " Les Hauts de Glos " d'une surface de vente totale de 17 543 m², composé d'une cellule spécialisée en alimentaire d'une surface de vente de 435 m², de sept cellules spécialisées dans l'équipement de la maison d'une surface totale de vente de 11 540 m² et de dix cellules spécialisées dans l'équipement de la personne d'une surface globale de vente de 5 568 m², lui a refusé l'autorisation sollicitée ;<br/>
<br/>
              2°) d'enjoindre à la commission nationale de réexaminer sa demande d'autorisation dans un délai de quatre mois à compter de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 8 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le Traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu la décision du 16 juillet 2014 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société civile de construction vente Lisieux Développement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée :<br/>
<br/>
              1. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              2. Considérant, par ailleurs, qu'aux termes de l'article 10 de la directive du 12 décembre 2006 relative aux services dans le marché intérieur : " 3. Les conditions d'octroi de l'autorisation pour un nouvel établissement ne doivent pas faire double emploi avec les exigences et les contrôles équivalents ou essentiellement comparables en raison de leur finalité, auxquels est déjà soumis le prestataire dans un autre Etat membre ou dans le même Etat membre. (...) ", qu'aux termes de l'article 13 de la même directive : " 1. Les procédures et formalités d'autorisation doivent être claires, rendues publiques à l'avance et propres à garantir aux parties concernées que leur demande sera traitée avec objectivité et impartialité. " ; qu'aux termes de l'article 14 de la même directive : " Les Etats membres ne subordonnent pas l'accès à une activité de services ou son exercice sur leur territoire au respect de l'une des exigences suivantes : (...) 5) l'application au cas par cas d'un test économique consistant à subordonner l'octroi de l'autorisation à la preuve de l'existence d'un besoin économique ou d'une demande du marché, à évaluer les effets économiques potentiels ou actuels de l'activité ou à évaluer l'adéquation de l'activité avec les objectifs de programmation économique fixés par l'autorité compétente (...) " ;<br/>
<br/>
              3. Considérant qu'à l'appui de son moyen tiré de ce que les dispositions nationales mentionnées au point 1. ci-dessus méconnaîtraient tant les dispositions rappelées ci-dessus de la directive du 12 décembre 2006 que le principe de liberté d'établissement énoncé à l'article 49 du Traité sur le fonctionnement de l'Union européenne, la société requérante se borne à faire valoir qu'elles feraient " double emploi " avec d'autres contrôles auxquels les porteurs de projets d'aménagement commercial sont soumis, que les critères qu'elles instaurent seraient insuffisamment clairs et objectifs, et qu'elles conduiraient à instituer un test économique incompatible avec l'article 14 de la directive ; que, cependant, la procédure que les dispositions nationales rappelées ci-dessus organisent ne saurait faire " double emploi " avec d'autres procédures administratives, telles que le permis de construire, certaines autorisations spécifiques à la protection de l'environnement ou les règles régissant les établissements accueillant du public, qui ont une finalité différente ; que ces mêmes dispositions édictent des critères clairs et objectifs ; qu'elles n'instituent pas de " test économique " ; que par suite, et sans qu'il soit besoin de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, les moyens tirés de la méconnaissance du droit de l'Union européenne doivent être écartés ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que le projet en cause, situé à Glos (Calvados), se trouve à proximité d'équipements publics et d'une zone d'habitation ayant vocation à se développer à court et moyen terme, et qu'il est inséré dans une zone d'aménagement concerté ayant vocation à participer au développement économique de l'intercommunalité ; que, par suite, en retenant, pour refuser à la société requérante l'autorisation qu'elle sollicitait, un motif tiré de ce que ce projet aurait, par son éloignement du centre-ville de Glos, favorisé l'étalement urbain et entraîné une consommation excessive de foncier, la Commission nationale d'aménagement commercial a commis une erreur d'appréciation ; <br/>
<br/>
              5. Considérant, d'une part, qu'il ne résulte pas de l'instruction que la commission nationale aurait pris la même décision si elle ne s'était fondée que sur ses autres motifs ;<br/>
<br/>
              6. Considérant, d'autre part, que si la SAS Bricorama France et l'association Cap Lisieux demandent que d'autres motifs soient substitués au motif erroné mentionné au point 4. ci-dessus, une telle substitution de motifs ne peut être demandée au juge de l'excès de pouvoir que par l'administration auteur de la décision attaquée, laquelle s'est abstenue de produire à l'instance ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin ni de statuer sur la fin de non-recevoir opposée par la société requérante au mémoire produit par l'association Cap Lisieux ni d'examiner les autres moyens de sa requête, que la société civile de construction vente Lisieux Développement est fondée à demander l'annulation de la décision de la Commission nationale d'aménagement commercial qu'elle attaque ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction : <br/>
<br/>
              8. Considérant que la présente décision implique nécessairement que la commission nationale procède à un nouvel examen de la demande dont elle se trouve à nouveau saisie ; qu'il y a lieu d'enjoindre un tel réexamen dans un délai de quatre mois à compter de la notification de la présente décision ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la société civile de construction vente Lisieux Développement, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société civile de construction vente Lisieux Développement de la somme de 2 000 euros au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la Commission nationale d'aménagement commercial du 25 juin 2013 est annulée.<br/>
<br/>
Article 2 : Il est enjoint à la Commission nationale d'aménagement commercial de réexaminer la demande de la société civile de construction vente Lisieux Développement dans un délai de quatre mois à compter de la notification de la présente décision. <br/>
<br/>
Article 3 : L'Etat versera à la société civile de construction vente Lisieux Développement la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la SAS Bricorama France et de l'association Cap Lisieux présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société civile de construction vente Lisieux Développement, à la SAS Bricorama France, à l'association CAP Lisieux et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
