<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374858</ID>
<ANCIEN_ID>JG_L_2016_04_000000398181</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/48/CETATEXT000032374858.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 01/04/2016, 398181, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398181</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:398181.20160401</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'arrêté du 14 septembre 2015, modifié par l'arrêté du 4 novembre 2015, par lequel le ministre de l'intérieur l'a assigné à résidence sur le territoire de la commune de Le Blanc (Indre) ; d'ordonner son assignation à résidence à son domicile à Paray-Vieille-Poste dans le département de l'Essonne, en application des dispositions de l'article L. 523-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ou, à défaut, son assignation à résidence dans les départements de l'Essonne ou du Val-de-Marne ou, à défaut, en Île-de-France ; d'ordonner, à défaut, une réduction du nombre de pointages à la gendarmerie de une fois à six fois par semaine ou, à défaut, une seule fois par jour et, à défaut, d'ordonner au ministre de l'intérieur de réexaminer sa situation.<br/>
<br/>
              Par une ordonnance n° 1602676 du 5 mars 2016, le juge des référés du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 22 et 29 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de suspendre les effets de l'arrêté portant assignation à résidence du 4 novembre 2015 ;<br/>
<br/>
              3°) à titre subsidiaire, d'ordonner  son assignation à résidence à Paray-Vieille-Poste dans le département de l'Essonne en application de l'article L. 523-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; à défaut d'ordonner l'assignation à résidence dans le département de l'Essonne ou dans le Val-de-Marne, ou à défaut dans tout autre lieu en Île-de-France ; <br/>
<br/>
              4°) d'ordonner, à défaut, une réduction du nombre de pointages à la gendarmerie de une fois à six fois par semaine ou, à défaut, une seule fois par jour ;<br/>
<br/>
              5°) à défaut, d'ordonner au ministre de l'intérieur de réexaminer sa situation ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - l'urgence au sens des dispositions de l'article L. 521-2 du code de justice administrative est présumée en matière d'expulsion ; elle est établie en l'espèce eu égard à la nature de la mesure d'assignation à résidence et à ses modalités définies par l'arrêté contesté compte-tenu de la gravité de ses pathologies et de son handicap ; en outre, l'Etat ne prend pas en charge la totalité de ses repas ;<br/>
              - l'arrêté contesté porte une atteinte grave et manifestement illégale à son droit à la vie, son droit à la vie privée et familiale et constitue un traitement dégradant et inhumain au sens de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors qu'il est gravement malade, que son état de santé ne lui permet pas de pointer deux fois par jours à la gendarmerie, que son état de santé s'est dégradé depuis le début de son assignation à résidence, qu'il est assigné à résidence loin de son domicile familial et de l'hôpital qui le suit régulièrement pour sa pathologie ;<br/>
              - la mesure d'assignation à résidence n'est pas nécessaire ni proportionnée au but qu'elle poursuit dès lors que le ministre de l'intérieur ne fait valoir aucun élément circonstancié et particulier pour désigner le lieu où il est assigné à résidence et porter à deux fois par jour le nombre de pointage à la gendarmerie.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 25 mars 2016, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que l'urgence n'est pas caractérisée et que les moyens soulevés par le requérant ne sont pas fondés.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale sur les droits des enfants et notamment son article 3.1 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A..., d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 29 mars 2016 à 11 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Lesourd, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              - M.A... ;<br/>
<br/>
              - la représentante de M.A... ;<br/>
<br/>
              - la représentante du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; que l'usage par le juge des référés des pouvoirs qu'il tient de cet article est ainsi subordonnée au caractère grave et manifeste de l'illégalité à l'origine d'une atteinte à une liberté fondamentale ;<br/>
<br/>
              2. Considérant que M.A..., ressortissant égyptien, né le 7 juin 1952, a fait l'objet, le 22 décembre 2014 d'un arrêté d'expulsion en application de l'article L. 521-3 du code de l'entrée et du séjour des étrangers et du droit d'asile et fixant l'Egypte comme pays de destination ; que par un arrêté du 22 juillet 2015, le ministre de l'intérieur l'a astreint à résider dans la commune de La Châtre, l'obligeant à se présenter 3 fois par jour à 9 heures 15, 15 heures 15, et 17 heures 45 à la gendarmerie de La Châtre et le contraignant à demeurer dans le lieu où il réside tous les jours de 21 heures à 7 heures ; que, par un arrêté du 21 août 2015, le ministre de l'intérieur a abrogé cet arrêté et l'a astreint à résider dans la commune de Châteauroux ; que le 4 septembre 2015, le ministre de l'intérieur a abrogé cet arrêté, a astreint M. A... à résider dans la commune de La Châtre et l'a contraint à se présenter 3 fois par jour à 9 heures 15, 15 heures 15 et 17 heures 45 à la gendarmerie de La Châtre ainsi qu'à demeurer dans le lieu où il réside tous les jours de 21 heures à 7 heures ; que, par un arrêté du 14 septembre 2015, notifié le 16 septembre 2015, le ministre de l'intérieur a abrogé cet arrêté et l'a assigné à résider dans la commune de Le Blanc (Indre) l'obligeant à se présenter 3 fois par jour à 9 heures 15, 15 heures 15, et 17 heures 45 à la gendarmerie de la commune et le contraignant à demeurer dans le lieu où il réside tous les jours de 21 heures à 7 heures ; que, par un arrêté du 4 novembre 2015, le ministre de l'intérieur a modifié l'arrêté du 14 septembre 2015 en réduisant les pointages à deux par jour ; que, par une requête enregistrée le 30 octobre 2015, M. A... a demandé au juge des référés du tribunal de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, la suspension de l'arrêté du 14 septembre 2015 ; que, par une ordonnance du 16 novembre 2016, le juge des référés du tribunal administratif de Paris a rejeté sa demande ; que, par une requête enregistrée le 19 février 2016, M. A...a demandé au juge des référés du tribunal administratif de Paris la suspension de l'exécution de l'arrêté du 14 septembre 2015 modifié par l'arrêté du 4 novembre 2015 ; qu'il relève appel de l'ordonnance du 5 mars 2016 par laquelle le juge des référés a rejeté sa demande ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 523-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " l'étranger qui fait l'objet d'un arrêté d'expulsion et qui justifie être dans l'impossibilité de quitter le territoire français en établissant qu'il ne peut ni regagner son pays d'origine ni se rendre dans aucun autre pays peut faire l'objet d'une mesure d'assignation à résidence dans les conditions prévues à l'article L. 561-1. Les dispositions de l'article L. 624-4 sont applicables. (...)" ; qu'aux termes de l'article L. 561-1 du même code : " (...) L'étranger astreint à résider dans les lieux qui lui sont fixés par l'autorité administrative doit se présenter périodiquement aux services de police ou aux unités de gendarmerie. L'étranger qui fait l'objet d'un arrêté d'expulsion ou d'une interdiction judiciaire ou administrative du territoire prononcés en tout point du territoire de la République peut, quel que soit l'endroit où il se trouve, être astreint à résider dans des lieux choisis par l'autorité administrative dans l'ensemble du territoire de la République. L'autorité administrative peut prescrire à l'étranger la remise de son passeport ou de tout document justificatif de son identité dans les conditions prévues à l'article L. 611-2. Si l'étranger présente une menace d'une particulière gravité pour l'ordre public, l'autorité administrative peut le faire conduire par les services de police ou de gendarmerie jusqu'aux lieux d'assignation. Le non-respect des prescriptions liées à l'assignation à résidence est sanctionné dans les conditions prévues à l'article L. 624-4. " ;<br/>
<br/>
              4. Considérant que M. A...soutient que les modalités d'exécution de l'arrêté l'astreignant à résider dans la commune de Le Blanc portent atteinte à son droit  à ne pas subir de traitement inhumain, ainsi qu'à son droit à mener une vie privée et familiale normale, tels que garantis par les articles 2, 3 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en ce qu'il l'oblige à résider dans une commune éloignée de son lieu de résidence habituelle, que la fréquence des pointages et les déplacements qu'ils impliquent ont pour effet d'aggraver son état de santé ; qu'il produit deux certificats médicaux à cet égard établissant qu'il ne peut marcher plus de 100 mètres et qu'il se déplace en fauteuil roulant ; que le choix du lieu d'assignation à résidence n'est pas justifié par des motifs d'ordre public, l'empêche de recevoir l'aide de sa famille et méconnaît l'intérêt supérieur de ses enfants ;<br/>
<br/>
              5. Considérant, en premier lieu, que, pour assigner M. A...dans une commune située à près de 300 kilomètres de la commune de Paray-Vieille-Poste, lieu où résident son épouse ainsi qu'un de ses enfants, le ministre de l'intérieur s'est fondé sur le danger que M. A...présente pour l'ordre public, en raison notamment de ses prêches antisémites ; que si M. A...fait valoir que la commission d'expulsion des étrangers a rendu le 11 juillet 2014 un avis défavorable à son expulsion, il ressort toutefois de l'instruction, et notamment d'une note blanche précise et circonstanciée versée au débat contradictoire, que M.A..., en qualité d'imam, a prononcé de manière répétée, pendant plusieurs années et au moins jusqu'en novembre 2013, des prêches violents et antisémites dans plusieurs mosquées d'Île-de-France et qu'il dispose d'une réelle autorité sur les membres de sa communauté ; que, dans ces conditions, il n'apparaît pas, en l'état de l'instruction, qu'en prononçant l'assignation à résidence de M. A..., et en la maintenant jusqu'à ce jour, le ministre de l'intérieur ait porté une atteinte grave et manifestement illégale à sa liberté d'aller et venir ou à son droit au respect de sa vie familiale ainsi qu'à l'intérêt supérieur de ses enfants garanti par l'article 3 de la convention internationale relative aux droits de l'enfant du 26 janvier 1990, rien ne s'opposant à ce que sa famille le rejoigne ;<br/>
<br/>
              6. Considérant, en second lieu, que si M. A...est atteint de pathologies pour lesquelles il a été hospitalisé à plusieurs reprises, y compris postérieurement à l'ordonnance attaquée, il ressort toutefois de l'instruction qu'il dispose sur le territoire de la commune où il est assigné d'un accès assuré aux services hospitaliers et médicaux nécessaires au traitement des affections dont il souffre ; que le ministre de l'intérieur s'est engagé à ce que tous les sauf-conduits nécessaires à son suivi médical soient délivrés, même en urgence ; qu'il n'apparaît pas non plus, en l'état de l'instruction, que M. A...soit confronté à de réelles difficultés pour se rendre à la gendarmerie où il doit effectuer les pointages, laquelle est distante de 430 mètres de l'hôtel où il est assigné et qui est desservie par les transports en commun, dont une navette gratuite assurant le trajet trois fois par jour ; que dans ces conditions, il n'apparaît pas non plus que les modalités de son assignation à résidence porteraient une atteinte grave et manifestement illégale au droit au respect de sa vie privée ou le soumettraient à un traitement inhumain ou dégradant ;<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande ; que ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, par voie de conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
