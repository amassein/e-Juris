<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027042756</ID>
<ANCIEN_ID>JG_L_2013_02_000000363955</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/04/27/CETATEXT000027042756.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 06/02/2013, 363955, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363955</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:363955.20130206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1201066 du 14 novembre 2012, enregistrée le 19 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle la présidente du tribunal administratif de Caen, avant de statuer sur la demande de M. A...tendant à l'annulation de l'arrêté du préfet de la Manche du 27 mars 2012 portant approbation du tracé de détail et établissement de servitudes d'appui, de passage, d'élagage et d'abattage d'arbres au profit de la société Réseau de Transport d'Electricité pour permettre la construction de la ligne électrique aérienne à très haute tension de 400 000 volts dite " Cotentin-Maine ", a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 323-4, L. 323-5, L. 323-6 et L. 323-7 du code de l'énergie ;<br/>
<br/>
              Vu les mémoires, enregistrés les 21 mai, 7 octobre et 5 novembre 2012 au greffe du tribunal administratif de Caen, présentés par M. B...A..., demeurant..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu les articles L. 323-4, L. 323-5, L. 323-6 et L. 323-7 du code de l'énergie ;<br/>
<br/>
              Vu l'ordonnance n° 2011-504 du 9 mai 2011 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 61-1 de la Constitution : " Lorsque, à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation qui se prononce dans un délai déterminé " ; qu'aux termes de l'article 38 de la Constitution : " Le Gouvernement peut, pour l'exécution de son programme, demander au Parlement l'autorisation de prendre par ordonnances, pendant un délai limité, des mesures qui sont normalement du domaine de la loi. / Les ordonnances sont prises en Conseil des ministres après avis du Conseil d'Etat. Elles entrent en vigueur dès leur publication mais deviennent caduques si le projet de loi de ratification n'est pas déposé devant le Parlement avant la date fixée par la loi d'habilitation. Elles ne peuvent être ratifiées que de manière expresse. / A l'expiration du délai mentionné au premier alinéa du présent article, les ordonnances ne peuvent plus être modifiées que par la loi dans les matières qui sont du domaine législatif " ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions précitées de l'article 38 de la Constitution qu'une ordonnance prise sur son fondement conserve, aussi longtemps que le Parlement ne l'a pas ratifiée expressément le caractère d'un acte administratif ; <br/>
<br/>
              3. Considérant que les dispositions des articles L. 323-4, L. 323-5, L. 323-6 et L. 323-7 du code de l'énergie sont issues de l'ordonnance du 9 mai 2011 portant codification de la partie législative du code de l'énergie ; qu'à la date de la présente décision, cette ordonnance n'a pas été ratifiée dans les conditions prévues par l'article 38 de la Constitution ; que, par suite, les dispositions contestées présentent le caractère d'un acte réglementaire et ne sont pas au nombre des dispositions législatives mentionnées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance du 7 novembre 1958 ; qu'elles ne sont ainsi pas susceptibles de faire l'objet d'un renvoi au Conseil constitutionnel en application de l'article 61-1 de la Constitution ;<br/>
<br/>
              4. Considérant que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, la question de la conformité à la Constitution d'une disposition législative, le Conseil d'Etat est régulièrement saisi et se prononce sur le renvoi de la question prioritaire de constitutionnalité telle qu'elle a été soulevée dans le mémoire distinct produit devant la juridiction qui la lui a transmise, dans la limite des dispositions dont la conformité à la Constitution a fait l'objet de la transmission ; que, par suite, il n'y a pas lieu pour le Conseil d'Etat de se prononcer sur le renvoi au Conseil constitutionnel de la question soulevée par M. A...en tant qu'elle concerne les articles 12 et 12 bis de la loi du 15 juin 1906 sur les distributions d'énergie et l'article 35 de la loi du 8 avril 1946 sur la nationalisation de l'électricité et du gaz, d'ailleurs abrogés par l'ordonnance du 9 mai 2011 précitée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Caen, non plus que la question de la conformité aux droits et garanties protégés par la Constitution des articles 12 et 12 bis de la loi du 15 juin 1906 et de l'article 35 de la loi du 8 avril 1946.<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., au Premier ministre, à la ministre de l'écologie, du développement durable et de l'énergie et à la société Réseau de Transport d'Electricité (RTE).<br/>
Copie en sera adressée au Conseil constitutionnel et au tribunal administratif de Caen.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
