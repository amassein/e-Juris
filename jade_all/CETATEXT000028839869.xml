<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028839869</ID>
<ANCIEN_ID>JG_L_2014_04_000000375088</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/98/CETATEXT000028839869.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 09/04/2014, 375088, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375088</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:375088.20140409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1312283 du 28 octobre 2013, enregistrée le 31 janvier 2014 au secrétariat du contentieux du Conseil d'État, par laquelle le président de la première chambre du tribunal administratif de Montreuil, avant qu'il soit statué sur la demande de la SA Orange tendant à obtenir la réduction des intérêts de retard mis à sa charge par un avis de mise en recouvrement du 23 août 2013, a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'État la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 209 du livre des procédures fiscales et 1727 du code général des impôts ;<br/>
<br/>
              Vu le mémoire, enregistré le 20 décembre 2013 au greffe du tribunal administratif de Montreuil, présenté par la SA Orange, dont le siège est 78, rue Olivier de Serres à Paris (75505 cedex 15), représentée par son président directeur général, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de la SA Orange ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'État a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'il résulte de l'article L. 209 du livre des procédures fiscales que " lorsque le tribunal administratif rejette totalement ou partiellement la demande d'un contribuable tendant à obtenir l'annulation ou la réduction d'une imposition établie en matière d'impôts directs à la suite d'une rectification ou d'une taxation d'office, les cotisations ou fractions de cotisations maintenues à la charge du contribuable et pour lesquelles celui-ci avait présenté une réclamation assortie d'une demande de sursis de paiement donnent lieu au paiement d'intérêts moratoires au taux de l'intérêt de retard prévu à l'article 1727 du code général des impôts. Ces intérêts moratoires ne sont pas dus sur les cotisations ou fractions de cotisations d'impôts soumises à l'intérêt de retard mentionné à l'article 1727 du code général des impôts. / (...) Sur demande justifiée du contribuable, le montant des intérêts moratoires est réduit du montant des frais éventuellement engagés pour la constitution des garanties propres à assurer le recouvrement des impôts contestés " ; qu'aux termes de l'article 1727 du code général des impôts : " I. Toute créance de nature fiscale, dont l'établissement ou le recouvrement incombe aux administrations fiscales, qui n'a pas été acquittée dans le délai légal donne lieu au versement d'un intérêt de retard. (...) / IV. (...) 2. L'intérêt de retard cesse d'être décompté lorsque la majoration prévue à l'article 1730 est applicable (...) " ; qu'aux termes de l'article 1730 du même code : " 1. Donne lieu à l'application d'une majoration de 10 % tout retard dans le paiement des sommes dues au titre de l'impôt sur le revenu, des contributions sociales recouvrées comme en matière d'impôt sur le revenu, de la taxe d'habitation, des taxes foncières sur les propriétés bâties et non bâties, des impositions recouvrées comme les impositions précitées et de l'impôt de solidarité sur la fortune (...) " ;<br/>
<br/>
              3. Considérant que les dispositions précitées de l'article L. 209 du livre des procédures fiscales prévoient la possibilité d'imputer les frais de constitution de garanties exposés en application de l'article L. 277 du même livre sur les intérêts moratoires dus par les contribuables soumis à la majoration prévue par l'article 1730 du code général des impôts, mais non sur l'intérêt de retard prévu par l'article 1727 du même code ; que ces dispositions doivent être regardées comme applicables au litige dont est saisi le tribunal administratif de Montreuil, dans le cadre duquel la société requérante revendique la faculté d'imputer sur l'intérêt de retard qui lui a été réclamé, après le rejet par le tribunal administratif de Montreuil de sa demande de décharge des cotisations supplémentaires d'impôt sur les sociétés qui lui ont été assignées, les frais engagés pour constituer les garanties nécessaires au sursis de paiement qu'elle avait sollicité ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce que, compte tenu de la différence de traitement, mentionnée ci-dessus, qu'elles prévoient, elles portent atteinte aux droits et libertés garantis par la Constitution, notamment aux principes d'égalité devant la loi et d'égalité devant les charges publiques, soulève une question sérieuse ; que tel n'est pas le cas, en revanche, pour les dispositions de l'article 1727 du code général des impôts, qui se bornent à définir les conditions dans lesquelles est versé, lorsqu'une créance de nature fiscale n'a pas été acquittée dans le délai légal, un intérêt de retard ; qu'ainsi, il y a seulement lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité relative à l'article L. 209 du livre des procédures fiscales ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 209 du livre des procédures fiscales est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution de l'article 1727 du code général des impôts.<br/>
Article 3 : La présente décision sera notifiée à la SA Orange et au ministre des finances et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, ainsi qu'au tribunal administratif de Montreuil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
