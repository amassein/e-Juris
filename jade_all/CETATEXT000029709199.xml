<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709199</ID>
<ANCIEN_ID>JG_L_2014_11_000000382352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/91/CETATEXT000029709199.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 05/11/2014, 382352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382352.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire, enregistrés les 4 juillet et 16 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, le département de la Savoie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-272 du 27 février 2014 portant délimitation des cantons dans le département de la Savoie ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution du 4 octobre 1958 ;<br/>
              - le code électoral ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - le décret n° 2012-1479 du 27 décembre 2012 ;<br/>
              - le décret n° 2013-938 du 18 octobre 2013 ;<br/>
              - la décision n° 380636 du 12 juin 2014 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel les questions prioritaires de constitutionnalité relatives aux articles L. 3113-1 et L. 3113-2 du code général des collectivités territoriales et L. 191-1 du code électoral ;<br/>
              - l'ordonnance n° 382352 du 18 juillet 2014 par laquelle le président de la 3ème sous-section de la section du contentieux du Conseil d'Etat n'a pas renvoyé au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par le département de la Savoie ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants ". Aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. / (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général ".<br/>
<br/>
              2. Compte tenu de la réduction du nombre des cantons résultant de l'application de l'article L. 191-1 du code électoral, le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de la Savoie. <br/>
<br/>
              Sur la compétence du Premier ministre et la procédure d'élaboration du décret attaqué : <br/>
<br/>
              3. En premier lieu, il résulte des termes mêmes des dispositions législatives précitées qu'il appartenait au Premier ministre de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons. Les autres critiques soulevées par le requérant à l'appui de son moyen tiré de l'incompétence du Premier ministre pour prendre le décret attaqué reviennent à remettre en cause la conformité aux droits et libertés garantis par la Constitution des dispositions législatives précitées. Les questions prioritaires de constitutionnalité posées à cet effet n'ont pas été renvoyées au Conseil constitutionnel par la décision n° 380636 et l'ordonnance n° 382352 du Conseil d'Etat statuant au contentieux visées ci-dessus. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ". Le décret attaqué n'appelle aucune mesure d'exécution que le ministre de la justice serait compétent pour signer ou contresigner. Il n'avait donc pas à être contresigné par ce ministre.<br/>
<br/>
              5. En troisième lieu, aucune disposition législative ou réglementaire n'imposait de procéder, préalablement à l'intervention du décret attaqué, à la consultation des communes, des établissements publics de coopération intercommunale, non plus qu'à la consultation individuelle des élus indépendamment de la consultation du conseil général requise par l'article L. 3113-2 du code général des collectivités territoriales. La circonstance, à la supposer établie, que l'avis de certains élus ait été recueilli n'a pas eu pour effet d'instaurer une procédure de consultation dont l'administration aurait été ensuite tenue de respecter les règles. Le département requérant ne peut, à cet égard et en tout état de cause, utilement se prévaloir des termes de la circulaire du ministre de l'intérieur du 12 avril 2013 relative à la méthodologie du redécoupage cantonal en vue de la mise en oeuvre du scrutin binominal majoritaire aux élections départementales, laquelle est dépourvue de caractère réglementaire.  <br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              6. En premier lieu, aux termes de l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable à la date du décret attaqué : " (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2  du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) ". Il est constant que les nouveaux cantons du département de la Savoie ont été délimités sur la base des données authentifiées par le décret du 27 décembre 2012. Par suite, le moyen tiré de ce que les données retenues pour le redécoupage des cantons de ce département ne correspondraient pas à la réalité démographique ne peut qu'être écarté. <br/>
<br/>
              7. En deuxième lieu, il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que la délimitation des cantons doit être effectuée sur des bases essentiellement démographiques. Il en découle qu'elle doit être effectuée en prenant en considération non le nombre des électeurs mais le chiffre de la population. De plus, ne peut être utilement invoqué le moyen tiré de ce que le décret n'aurait pas procédé à un rééquilibrage des écarts de population par canton d'un département à un autre dès lors que l'objet du décret est de procéder à une élection au sein de chaque département. Les autres critiques soulevées par le requérant à l'appui du moyen tiré de la méconnaissance du principe d'égalité devant le suffrage reviennent à remettre en cause la conformité aux droits et libertés garantis par la Constitution des dispositions législatives précitées. Les questions prioritaires de constitutionnalité posées à cet effet n'ont pas été renvoyées au Conseil constitutionnel par la décision n° 380636 et l'ordonnance n° 382352 du Conseil d'Etat statuant au contentieux visées ci-dessus. <br/>
<br/>
              8. En troisième lieu, la circonstance que le décret attaqué se borne à identifier, pour chaque canton, un " bureau centralisateur " sans mentionner les chefs-lieux de canton est, en tout état de cause, sans influence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales dans le département de la Savoie.<br/>
<br/>
              9. En quatrième lieu, ni l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposaient au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les périmètres des arrondissements, des circonscriptions législatives ou des circonscriptions judiciaires ou avec les îlots regroupés pour l'information statistique (IRIS) définis par l'Institut national de la statistique et des études économiques. En tout état de cause, le moyen tiré de ce que la nouvelle carte cantonale ne respecterait pas les limites des IRIS " dans tous les départements dans lesquels la frontière des cantons traverse les villes " n'est pas assorti des précisions permettant d'en apprécié le bien-fondé. <br/>
<br/>
              10. En cinquième lieu, si le requérant soutient que le décret méconnaît la spécificité territoriale, géographique et historique, de la Maurienne et critique les choix opérés pour les cantons du Pont-de-Beauvoisin et de Saint-Pierre d'Albigny, il n'apporte aucun élément de nature à établir que les tracés contestés seraient entachés d'une erreur manifeste d'appréciation. En outre, il n'est, en tout état de cause, pas allégué que la délimitation des cantons du département de la Savoie n'aurait pas été opérée sur des bases essentiellement démographiques. <br/>
<br/>
              11. Il résulte de tout ce qui précède que le département de la Savoie n'est pas fondé à demander l'annulation pour excès de pouvoir du décret attaqué. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du département de la Savoie est rejetée.<br/>
Article 2 : La présente décision sera notifiée au département de la Savoie, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
