<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043099658</ID>
<ANCIEN_ID>JG_L_2021_02_000000413226</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/96/CETATEXT000043099658.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 04/02/2021, 413226, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413226</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:413226.20210204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 5 décembre 2018, le Conseil d'Etat, statuant au contentieux, a sursis à statuer sur la requête, enregistrée sous le n° 413226, présentée par l'association française des usagers des banques, tendant à l'annulation pour excès de pouvoir du décret n° 2017-1099 du 14 juin 2017 fixant la durée pendant laquelle le prêteur peut imposer à l'emprunteur la domiciliation de ses salaires ou revenus assimilés sur un compte de paiement, jusqu'à ce que la Cour de Justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
<br/>
              1°) Les dispositions du a) du paragraphe 2 de l'article 12 de la directive 2014/17/UE du 4 février 2014 sur les contrats de crédit aux consommateurs relatifs aux biens immobiliers à usage résidentiel, compte tenu notamment de la finalité qu'elles assignent au compte de paiement ou d'épargne dont elles autorisent l'ouverture ou la tenue, ou les dispositions du paragraphe 3 du même article autorisent-elles, d'une part, le prêteur à imposer à l'emprunteur, en contrepartie d'un avantage individualisé, la domiciliation de l'ensemble de ses revenus salariaux ou assimilés sur un compte de paiement pendant une durée fixée par le contrat de prêt, quels que soient le montant, les échéances et la durée du prêt, d'autre part, à ce que la durée ainsi fixée puisse atteindre dix ans ou, si elle est inférieure, la durée du contrat '<br/>
<br/>
              2°) L'article 45 de la directive 2007/64/CE du 13 novembre 2007, alors applicable et repris désormais à l'article 55 de la directive (UE) 2015/2366 du 25 novembre 2015, et les articles 9 à 14 de la directive 2014/92/UE du 23 juillet 2014, relatifs à la facilitation de la mobilité bancaire et aux frais de clôture d'un compte de paiement, s'opposent-ils à ce que la clôture d'un compte ouvert par l'emprunteur auprès du prêteur pour y domicilier ses revenus en contrepartie d'un avantage individualisé dans le cadre d'un contrat de crédit entraîne, si elle a lieu avant l'expiration de la période fixée dans ce contrat, la perte de cet avantage, y compris plus d'un an après l'ouverture du compte et, d'autre part, si ces mêmes dispositions s'opposent à ce que la durée de cette période puisse atteindre dix ans ou la durée totale du crédit '<br/>
<br/>
              Par un arrêt n° C-778/18 du 15 octobre 2020, la Cour de justice de l'Union européenne s'est prononcée sur ces questions.<br/>
<br/>
              Les parties ont été invitées à indiquer au Conseil d'Etat quelles seraient les conséquences d'une annulation rétroactive du décret n° 2017-1099 du 14 juin 2017 fixant la durée pendant laquelle le prêteur peut imposer à l'emprunteur la domiciliation de ses salaires ou revenus assimilés sur un compte de paiement.<br/>
<br/>
              Par un nouveau mémoire, enregistré 21 décembre 2020, le ministre de l'économie, des finances et de la relance conclut au rejet de la requête et, à titre subsidiaire, si l'annulation du décret devait être prononcée, à ce que les effets de cette annulation soient différés. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2007/64/CE du Parlement européen et du Conseil du 13 novembre 2007 ;<br/>
              - la directive 2014/17/UE du Parlement européen et du Conseil du 4 février 2014 ;<br/>
              - la directive 2014/92/UE du Parlement européen et du Conseil du 23 juillet 2014 ;<br/>
              - la directive (UE) 2015/2366 du Parlement européen et du Conseil du 25 novembre 2015 ;<br/>
              - le code monétaire et financier ;<br/>
              - le code de la consommation ;<br/>
              - l'ordonnance n° 2017-1090 du 1er juin 2017 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association française des usagers des banques ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 12 de la directive 2014/17/UE du Parlement européen et du Conseil du 4 février 2014 sur les contrats de crédit aux consommateurs relatifs aux biens immobiliers à usage résidentiel : " 1. Les États membres autorisent la vente groupée mais interdisent la vente liée. / 2. Nonobstant le paragraphe 1, les États membres peuvent prévoir que les prêteurs puissent demander au consommateur, à un membre de sa famille ou à un de ses proches: / a) d'ouvrir ou de tenir un compte de paiement ou d'épargne dont la seule finalité est d'accumuler un capital pour assurer le remboursement du principal et des intérêts du prêt, de mettre en commun des ressources aux fins de l'obtention du crédit ou de fournir au prêteur des garanties supplémentaires en cas de défaut de paiement ; / b) d'acquérir ou de conserver un produit d'investissement ou un produit de retraite privé, l'objectif étant que ce produit qui vise avant tout à procurer un revenu à l'investisseur pendant sa retraite serve également à fournir au prêteur une garantie supplémentaire en cas de défaut de paiement ou à accumuler un capital pour assurer le remboursement ou le service du prêt ou mettre en commun des ressources aux fins de l'obtention du crédit ; / c) de conclure un contrat de crédit distinct en relation avec un contrat de crédit à risque partagé afin d'obtenir le crédit. / 3. Nonobstant le paragraphe 1, les États membres peuvent également autoriser les ventes liées lorsque le prêteur peut prouver à son autorité compétente que, en prenant dûment en compte la disponibilité et le prix des produits en question proposés sur le marché, les produits ou catégories de produits liés offerts dans des conditions similaires qui ne sont pas proposés séparément présentent des avantages évidents pour le consommateur. Le présent paragraphe s'applique uniquement aux produits qui sont commercialisés après le 20 mars 2014. / 4. Les États membres peuvent également autoriser les prêteurs à exiger du consommateur qu'il souscrive une police d'assurance appropriée en rapport avec le contrat de crédit. Dans ce cas, les États membres veillent à ce que le prêteur accepte la police d'assurance établie par un prestataire différent du prestataire préconisé par le prêteur si la police en question présente un niveau de garanties équivalent à celui de la police proposée par le prêteur ".<br/>
<br/>
              2. Par l'arrêt C-778/18 du 15 octobre 2020, la Cour de justice de l'Union européenne s'est prononcée sur les questions préjudicielles que lui avait renvoyées le Conseil d'Etat, saisi d'un recours pour excès de pouvoir formé contre le décret du 14 juin 2017 fixant la durée pendant laquelle le prêteur peut imposer à l'emprunteur la domiciliation de ses salaires ou revenus assimilés sur un compte de paiement. La Cour de justice a dit pour droit que le a) du paragraphe 2 de l'article 12 de la directive 2014/17/UE, précédemment cité, doit être interprété en ce sens qu'il s'oppose à une réglementation nationale qui autorise le prêteur à imposer à l'emprunteur, lors de la conclusion d'un contrat de crédit relatif aux biens immobiliers à usage résidentiel, en contrepartie d'un avantage individualisé, la domiciliation de l'ensemble de ses revenus salariaux ou assimilés sur un compte de paiement ouvert auprès de ce prêteur, indépendamment du montant, des échéances et de la durée du prêt, mais qu'il ne s'oppose pas à une réglementation nationale selon laquelle la durée de domiciliation imposée, lorsque celle-ci ne porte pas sur l'ensemble des revenus salariaux de l'emprunteur, peut atteindre dix ans ou, si elle est inférieure, la durée du contrat de crédit concerné. <br/>
<br/>
              3. La Cour de justice a, par ailleurs, aussi dit pour droit que la notion de " frais ", au sens de l'article 45, paragraphe 2, de la directive 2007/64/CE du Parlement européen et du Conseil concernant les services de paiement dans le marché intérieur du 13 novembre 2007 ainsi que de l'article 12, paragraphe 3, de la directive 2014/92/UE du Parlement européen et du Conseil du 23 juillet 2014 sur la comparabilité des frais liés aux comptes de paiement, le changement de compte de paiement et l'accès à un compte de paiement assorti de prestations de base, doit être interprétée en ce sens qu'elle n'englobe pas la perte d'un avantage individualisé offert par le prêteur à l'emprunteur en contrepartie de l'ouverture d'un compte auprès de ce prêteur pour y domicilier ses revenus dans le cadre d'un contrat de crédit, causée par la clôture de ce compte. <br/>
<br/>
              4. Aux termes de l'article L. 312-1-2 du code monétaire et financier : " I. - 1. Est interdite la vente ou offre de vente de produits ou de prestations de services groupés sauf lorsque les produits ou prestations de services inclus dans l'offre groupée peuvent être achetés individuellement ou lorsqu'ils sont indissociables (...) ". L'article 1er de l'ordonnance du 1er juin 2017 relative aux offres de prêt immobilier conditionnées à la domiciliation des salaires ou revenus assimilés de l'emprunteur sur un compte de paiement, prise sur le fondement de ces dispositions, a inséré dans le code de la consommation un nouvel article L. 313-25-1 qui dispose, dans sa version alors vigueur, que : " Le prêteur peut conditionner l'offre de prêt mentionnée à l'article L. 313-24 à la domiciliation par l'emprunteur de ses salaires ou revenus assimilés sur un compte de paiement mentionné à l'article L. 314-1 du code monétaire et financier, sous réserve pour ce prêteur de faire bénéficier en contrepartie l'emprunteur d'un avantage individualisé. / Cette condition ne peut être imposée à l'emprunteur au-delà d'une durée maximale fixée par décret en Conseil d'Etat. Au terme du délai prévu par le contrat de crédit, l'avantage individualisé est acquis à l'emprunteur jusqu'à la fin du prêt. / Si, avant le terme de ce délai, l'emprunteur cesse de satisfaire à la condition de domiciliation susmentionnée, le prêteur peut mettre fin, pour les échéances restant à courir jusqu'au terme du prêt, à l'avantage individualisé mentionné au premier alinéa, et appliquer les conditions, de taux ou autres, mentionnées au 10° de l'article L. 313-25.  (...) ". Aux termes du I de l'article L. 314-1 du code monétaire et financier : " Est un compte de paiement, un compte détenu au nom d'une ou de plusieurs personnes, utilisé aux fins de l'exécution d'opérations de paiement ". Le décret attaqué a inséré dans le code de la consommation un nouvel article R. 313-21-1 qui prévoit que : " La durée maximale de domiciliation des salaires ou revenus assimilés mentionnée à l'article L. 313-25-1 est fixée à dix ans suivant la conclusion du contrat de crédit, ou le cas échéant, de l'avenant au contrat de crédit initial. / Cette durée ne peut en tout état de cause excéder celle du contrat de crédit. " <br/>
<br/>
              5. Il résulte des termes mêmes de l'article L. 313-25-1 du code de la consommation, alors en vigueur, que ce dernier permet aux établissements de crédit de conditionner l'octroi d'un avantage individualisé, dans le cadre d'un contrat de crédit proposé à un emprunteur relatif à un bien immobilier, à l'engagement de domicilier, pendant une période déterminée, l'ensemble des salaires ou revenus assimilés dans cet établissement, indépendamment du montant, des échéances et de la durée d'un prêt, et non uniquement la seule partie des salaires ou des revenus assimilés de l'emprunteur correspondant à ce qui est nécessaire pour rembourser le prêt, obtenir le crédit ou de fournir au prêteur des garanties supplémentaires en cas de défaut de paiement. Il doit ainsi être regardé comme une vente liée, au sens de la directive 2014/17/UE du Parlement européen et du Conseil du 4 février 2014, prohibée par le a) du paragraphe 2 de l'article 12 de cette directive. Il en résulte que l'ensemble des dispositions de l'article L. 313-25-1 du code de la consommation, qui définissent un seul et même dispositif et sont indivisibles, ne sont pas compatibles avec les objectifs de la directive. <br/>
<br/>
              6. Il résulte de ce qui précède que le décret attaqué du 14 juin 2017, qui a été pris en application du deuxième alinéa de l'article L. 313-25-1 du code de la consommation pour fixer la durée maximale de domiciliation obligatoire des salaires ou revenus assimilés, est dépourvu de base légale et doit, pour ce motif, être annulé. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'association française des usagers des banques au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le décret n° 2017-1099 du 14 juin 2017 est annulé. <br/>
Article 2 : L'Etat versera à l'association française des usagers des banques la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à l'association française des usagers des banques, au Premier ministre et au ministre de l'économie, des finances et de la relance.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
