<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033222576</ID>
<ANCIEN_ID>JG_L_2016_10_000000392351</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/22/25/CETATEXT000033222576.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/10/2016, 392351, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392351</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP ODENT, POULET ; SCP BOUTET, HOURDEAUX ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392351.20161007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Lyonnaise des eaux France a demandé au tribunal administratif de Bordeaux de condamner solidairement les sociétés Sobebo, Urbaine de Travaux, Sagebat et Axa France Iard, d'une part, à lui verser la somme de 3 430 367,20 euros TTC au titre des désordres relevés sur le collecteur Lajaunie du réseau d'assainissement de la communauté urbaine de Bordeaux et, d'autre part, à lui rembourser les frais d'expertise avancés à hauteur de 311 940,66 euros TTC. Par un jugement n° 0901696 du 27 novembre 2012, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13BX00246 du 8 juin 2015, la cour administrative d'appel de Bordeaux a rejeté son appel dirigé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 août et 4 novembre 2015 et le 8 février 2016 au secrétariat du contentieux du Conseil d'État, la société Lyonnaise des eaux France demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société Sobebo, de la société Urbaine de travaux, du Centre d'expertise du bâtiment et des travaux publics (CEBTP), de la société Axa France Iard et des sociétés Sagebat et Sagena la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Lyonnaise des eaux France, à la SCP Hémery, Thomas-Raquin, avocat de la société Sobebo, à la SCP Boutet, Hourdeaux, avocat de la société Urbaine de Travaux et de la société Axa France Iardard, à la SCP Odent, Poulet, avocat de la société SMA courtage et de la société SMA SA et à la SCP Foussard, Froger, avocat de Bordeaux métropole ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, pour la rénovation du collecteur du réseau d'assainissement de la communauté urbaine de Bordeaux situé entre la station Saint-Emilion et le quai de Brazza à Bordeaux, la communauté urbaine de Bordeaux (CUB) a confié une mission de maîtrise d'oeuvre à la société Lyonnaise des eaux France, exploitante du réseau d'assainissement dans le cadre d'un contrat d'affermage conclu le 22 décembre 1992 ; que l'exécution des travaux a, quant à elle, été confiée à un groupement d'entreprises constitué entre la société Sobebo et la société Urbaine de travaux par un marché conclu le 17 mai 1994 ; qu'à la suite de l'apparition de désordres affectant la coque en polypropylène mise en place à l'intérieur de la canalisation, la société Lyonnaise des eaux France a recherché devant la juridiction administrative la responsabilité des deux entrepreneurs ainsi que celle des sociétés Sagebat et Sagena, aux droits des quelles sont venues les sociétés SMA SA et SMA courtage, et de la société Axa France Iard, en leur qualité d'assureurs des entreprises de travaux, et demandé leur condamnation solidaire au versement d'une indemnité de 3 430 367,20 euros TTC et au paiement de frais d'un montant de 311 940,66 euros TTC engagés dans le cadre de l'expertise ordonnée par le juge des référés le 20 février 2004 ; que ses demandes ont été rejetées par le tribunal administratif de Bordeaux, par un jugement du 27 novembre 2012, ainsi que, sur appel de la société Lyonnaise des eaux France, par la cour administrative d'appel de Bordeaux, par un arrêt du 8 juin 2015 contre lequel la société se pourvoit en cassation ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que la société Lyonnaise des eaux France soutient que le principe d'impartialité a été méconnu dès lors que le président de la formation de jugement statuant en appel, M.A..., a été détaché de 2005 à 2008 auprès de la communauté urbaine de Bordeaux dans le cadre d'emplois des administrateurs territoriaux et y a exercé durant cette période les fonctions d'inspecteur général des services de la communauté urbaine ;<br/>
<br/>
              3. Considérant, toutefois, que, d'une part, il ressort des pièces du dossier soumis au Conseil d'Etat que les faits à l'origine de l'instance litigieuse, à laquelle la CUB n'a au demeurant jamais été partie, sont largement antérieurs à l'arrivée de M. A...au sein des services de la communauté urbaine ; que durant la période où il y a exercé les fonctions d'inspecteur général des services, la nature des tâches qui lui étaient confiées en cette qualité, qui incluaient en particulier la mission de renégocier les contrats d'affermage des services de distribution d'eau et de l'assainissement passés par la communauté urbaine, ne l'ont pas mis en situation de connaître du contentieux opposant la société Lyonnaise des eaux France aux constructeurs quant aux désordres affectant le collecteur Lajaunie dont la rénovation avait été entreprise et qui faisait l'objet, à la même époque, d'une expertise dont les conclusions n'ont été remises qu'après son départ ; que, dès lors, ce magistrat ne peut être regardé comme ayant déjà eu, dans le cadre de ses fonctions administratives antérieures, à connaître des faits objets du présent litige dans des conditions telles qu'il aurait déjà pris partie sur l'issue du contentieux qui lui a été soumis dans le cadre de ses fonctions juridictionnelles ;<br/>
<br/>
              4. Considérant, d'autre part, qu'il ressort également des pièces du dossier que, si le magistrat dont l'impartialité est critiquée a conduit, dans le cadre des relations entre la collectivité qui l'employait et son délégataire de service public, des négociations qui ont abouti à une révision générale des conditions financières d'exécution du contrat d'affermage et si la modification de ces contrats a été largement relayée par voie de presse, le présent litige n'a pas de rapport avec la question de l'exécution et de la renégociation des contrats de délégation entre la Lyonnaise des eaux France et la communauté urbaine de Bordeaux dont il était chargé ; que, compte tenu, en outre, du délai écoulé entre la fin des fonctions de ce magistrat à la communauté urbaine et la date à laquelle la cour a statué, ces seules circonstances ne sont pas, en l'absence de tout autre élément et contrairement à ce qui est soutenu, de nature à révéler un parti pris de M. A... à l'égard de la société requérante et ne constituent pas une raison sérieuse de mettre en doute son indépendance et son impartialité, exigences requises de tout juge ; que, dans ces conditions, le moyen tiré de ce que la composition de la formation de jugement aurait, en l'espèce, méconnu le principe d'impartialité doit être écarté ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              En ce qui concerne la garantie décennale :<br/>
<br/>
              5. Considérant que c'est par une appréciation souveraine exempte de dénaturation que la cour administrative d'appel de Bordeaux a constaté que les désordres apparus après la réception des travaux étaient de même nature que ceux qui avaient été constatés pendant les travaux ; que, de même, la cour a souverainement relevé que si des réparations avaient déjà été effectuées sur le collecteur avant la réception des travaux, les doutes quant aux solutions techniques employées, qui étaient connus du maître d'oeuvre, ne permettaient pas de regarder ces réparations comme définitives ni même pérennes ; qu'elle a ainsi pu en déduire, sans commettre d'erreur de droit, que les désordres en cause avaient le caractère d'un vice apparent ; qu'enfin, en estimant que la société requérante pouvait appréhender les conséquences dommageables des désordres en cause, la cour, qui a notamment tenu compte des conclusions des études qui avaient été menées sur les causes des cloques et bombements observés sur la coque du collecteur, n'a ni commis d'erreur de droit ni dénaturé les pièces du dossier ;<br/>
<br/>
              En ce qui concerne la responsabilité contractuelle :<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Lyonnaise des eaux France a recherché la responsabilité contractuelle des sociétés Urbaine de travaux et Sobebo au titre de l'article 9-6 de l'additif au cahier des clauses administratives particulières du marché de travaux conclu par la communauté urbaine de Bordeaux, qui a prévu une garantie de dix ans des constructeurs pour l'étanchéité de la coque en polypropylène ;<br/>
<br/>
              7. Considérant qu'aux termes de l'article 26 du contrat d'affermage liant la société Lyonnaise des eaux France à la CUB : " Le fermier est autorisé, soit directement, soit par l'intermédiaire de la collectivité, à exercer les recours ouverts à celle-ci vis-à-vis des entrepreneurs et fournisseurs par la législation en vigueur " ; qu'il est loisible au maître de l'ouvrage de transférer à un tiers au contrat, y compris au maître d'oeuvre, son droit d'agir en justice sur le fondement de la garantie contractuelle dont il dispose contre les entrepreneurs de travaux auxquels il est lié ; qu'il résulte des termes mêmes de l'article 26 du contrat d'affermage que la commune intention des parties était de permettre à la société Lyonnaise des eaux France d'exercer les actions en justice appartenant initialement au délégant, également maître de l'ouvrage ; que, par suite, en estimant que les stipulations citées ci-dessus excluaient du champ des recours ouverts par la législation en vigueur l'action tendant à rechercher la responsabilité contractuelle du groupement d'entreprises en charge des travaux au motif que celle-ci, à la différence de la garantie décennale ou de la garantie de parfait achèvement qui sont prévues par le code civil, ne trouve son fondement que dans des stipulations contractuelles, la cour administrative d'appel de Bordeaux a dénaturé les pièces du dossier qui lui était soumis ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que, dès lors, son arrêt doit être annulé en tant qu'il se prononce sur la responsabilité contractuelle des entrepreneurs ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société Lyonnaise des eaux France, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge, d'une part de la société Urbaine de travaux et de la société Axa France Iard, d'autre part, des sociétés SMA SA et SMA courtage et, enfin, de la société Sobebo, la somme de 1 000 euros chacune à verser à la société Lyonnaise des eaux France, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 8 juin 2015 est annulé en tant qu'il se prononce sur la responsabilité contractuelle des entrepreneurs.<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La société Urbaine de travaux et la société Axa France Iard, d'une part, les sociétés SMA SA et SMA courtage, d'autre part, et, enfin, la société Sobebo, verseront chacune une somme de 1 000 euros à la société Lyonnaise des eaux France au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Urbaine de travaux, de la société Axa France Iard, des sociétés SMA SA et SMA courtage et de la société Sobebo ainsi que le surplus des conclusions du pourvoi de la société Lyonnaise des eaux France sont rejetés.<br/>
Article 5 : La présente décision sera notifiée à la société Lyonnaise des eaux France, à la société Sobebo, à la société Urbaine de travaux, à la société Axa France Iard, à la société SMA courtage, à la société SMA SA et au centre d'expertise du bâtiment et des travaux publics.<br/>
Copie en sera adressée à la communauté urbaine de Bordeaux - Bordeaux Métropole.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
