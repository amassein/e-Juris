<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043751438</ID>
<ANCIEN_ID>JG_L_2021_07_000000436551</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/14/CETATEXT000043751438.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 01/07/2021, 436551, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436551</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Prévoteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:436551.20210701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B... A... ont demandé au tribunal administratif de Lyon la décharge des cotisations supplémentaires d'impôt sur le revenu et de prélèvements sociaux auxquelles ils ont été assujettis au titre des années 2012 et 2013. Par un jugement n° 1602873 du 21 juin 2018, le tribunal administratif de Lyon a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 18LY03011 du 17 octobre 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. et Mme A... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 9 décembre 2019, le 5 mars 2020 et le 19 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat.<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Prévoteau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. et Mme B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A... ont acquis, le 8 décembre 2011 un lot dans un immeuble édifié au XVIème siècle situé dans le périmètre du secteur sauvegardé de Nîmes. Ils ont constitué avec les autres copropriétaires une association foncière urbaine libre pour la réalisation globale des travaux de restauration de cet immeuble à laquelle ils ont réglé, sur appel de fonds en 2011 et 2012, la somme de 104 152,75 euros correspondant à leur quote-part. Ils ont déduit la somme de 69 782,34 euros de leurs revenus fonciers au titre de 2012 et imputé le déficit foncier en résultant sur leur revenu global de 2012 et 2013. L'administration fiscale a remis en cause la déduction de cette somme correspondant au montant des travaux réalisés en 2012 et a rehaussé leurs revenus fonciers des années 2012 et 2013. Par un jugement du 21 juin 2018, le tribunal administratif de Lyon a rejeté leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2012 et 2013 à la suite de ces rectifications. Ils demandent l'annulation de l'arrêt du 17 octobre 2019 de la cour administrative d'appel de Lyon qui a rejeté leur appel contre ce jugement.<br/>
<br/>
              2. Aux termes du I de l'article 31 du code général des impôts : " Les charges de la propriété déductibles pour la détermination du revenu net comprennent :/ 1° Pour les propriétés urbaines :/ a) Les dépenses de réparation et d'entretien (...) /b) Les dépenses d'amélioration afférentes aux locaux d'habitation, à l'exclusion des frais correspondant à des travaux de construction, de reconstruction ou d'agrandissement (...) ". Au sens de ces dispositions, doivent être regardés comme des travaux de reconstruction ceux qui comportent la création de nouveaux locaux d'habitation, ou qui ont pour effet d'apporter une modification importante au gros oeuvre, ainsi que les travaux d'aménagement interne qui, par leur importance, équivalent à des travaux de reconstruction, et, comme des travaux d'agrandissement, ceux qui ont pour effet d'accroître le volume ou la surface habitable des locaux existants.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond et notamment de la note établie le 10 septembre 2019 par l'architecte du patrimoine en charge de l'opération que les travaux de restauration et de réhabilitation entrepris dans l'ensemble immobilier en litige se sont adaptés aux volumes existants et n'ont pas affecté de manière importante le gros oeuvre et que le local situé au rez-de-chaussée et occupé par la mairie n'a pas été intégré dans l'opération de restauration. Il ressort également des pièces du dossier que la transformation en appartement du local commercial, situé au rez-de-chaussée et anciennement occupé par une association, était techniquement et fonctionnellement indépendante des travaux entrepris dans les parties privatives et communes du reste de l'immeuble. Par suite, en jugeant que la réfection de la toiture de l'immeuble, la reprise partielle de la charpente, le remplacement des plafonds en plâtre, la restauration des parquets, la réfection des dallages et des réseaux, sur chacun des quatre niveaux, équivalaient à des travaux de reconstruction, alors que ces travaux, qui n'ont pas affecté de manière importante le gros oeuvre, étaient dissociables de ceux ayant conduit à l'augmentation de la surface habitable au rez-de-chaussée, la cour a inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. et Mme A... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et Mme A..., au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 17 octobre 2019 de la cour administrative d'appel de Lyon est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera à M. et Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. et Mme B... A... ainsi qu'au ministre de l'économie, des finances et de la relance.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
