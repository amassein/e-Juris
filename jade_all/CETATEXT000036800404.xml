<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036800404</ID>
<ANCIEN_ID>JG_L_2018_04_000000413436</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/80/04/CETATEXT000036800404.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 13/04/2018, 413436, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413436</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413436.20180413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de la Guadeloupe d'annuler la décision du 25 août 2016 par laquelle le préfet de la Guadeloupe a refusé de procéder à l'échange de son permis de conduire syrien contre un permis français. Par un jugement n° 1600990 du 15 juin 2017, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par une ordonnance n° 17BX02730 du 11 août 2017, le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, sur le fondement de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 9 août 2017 au greffe de la cour, par lequel le préfet de la Guadeloupe demande l'annulation de ce jugement et qui a été enregistré au secrétariat de la section du contentieux du Conseil d'Etat sous le n° 413436. <br/>
<br/>
              Par un mémoire enregistré le 13 octobre 2017 au secrétariat de la section du contentieux du Conseil d'Etat sous le n° 415015, le ministre de l'intérieur a régularisé le pourvoi du préfet de la Guadeloupe. Il conclut en outre à ce que, réglant l'affaire au fond, le Conseil d'Etat rejette la demande de M.A.... <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le pourvoi enregistré sous le n° 415015 constitue en réalité une régularisation du pourvoi enregistré sous le n° 413436 ; que, par suite, ce pourvoi doit être rayé des registres du secrétariat du contentieux du Conseil d'Etat et joint au pourvoi enregistré sous le n° 413436 ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., ressortissant syrien admis le 7 octobre 2014 au bénéfice de la protection subsidiaire, s'est vu délivrer le 7 janvier 2015 le récépissé de demande de titre de séjour prévu à l'article R. 742-6 du code de  l'entrée et du séjour des étrangers et du droit d'asile alors en vigueur ; qu'il a demandé le 14 juin 2016 au préfet de la Guadeloupe de procéder à l'échange de son permis de conduire syrien contre un permis français ; que le préfet de la Guadeloupe a rejeté sa demande par une décision du 25 août 2016, au motif qu'elle avait été présentée après l'expiration du délai d'un an suivant le début de validité de son premier titre de séjour, prévu à l'article 4 de l'arrêté du 12 janvier 2012 susvisé ; que le ministre de l'intérieur demande l'annulation du jugement du 15 juin 2017 par lequel le tribunal administratif de la Guadeloupe, statuant sur la demande de M. A..., a annulé cette décision ;<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article R. 222-3 du code de la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire " ; qu'aux termes du II de l'article 4 de l'arrêté du 12 janvier 2012 susvisé : " Pour les ressortissants étrangers non-ressortissants de l'Union européenne, la date d'acquisition de la résidence normale est celle du début de validité du premier titre de séjour " ; qu'aux termes du II de l'article 11 du même arrêté, le délai d'un an dans lequel un étranger qui s'est vu reconnaître la qualité de réfugié peut demander l'échange de son permis de conduire court " à compter de la date de début de validité du titre de séjour provisoire " ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article R. 742-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction en vigueur à la date des faits : " L'étranger auquel la qualité de réfugié est reconnue par l'Office français de protection des réfugiés et apatrides ou la Cour nationale du droit d'asile est admis à souscrire une demande de délivrance de carte de résident dans les conditions prévues à l'article R. 314-2. / Dans un délai de huit jours à compter de sa demande, il est mis en possession d'un récépissé de la demande de titre de séjour qui vaut autorisation de séjour d'une durée de validité de trois mois renouvelable et qui porte la mention " reconnu réfugié ". / Ce récépissé confère à son titulaire le droit d'exercer la profession de son choix dans les conditions prévues à l'article L. 314-4. " ; qu'aux termes de l'article R. 742-6 du même code alors en vigueur : " L'étranger qui s'est vu accorder le bénéfice de la protection subsidiaire par l'Office français de protection des réfugiés et apatrides ou la Cour nationale du droit d'asile est admis à souscrire une demande de délivrance de carte de séjour temporaire dans les conditions prévues à l'article R. 313-1. / Dans un délai de huit jours à compter de sa demande, il est mis en possession d'un récépissé de demande de titre de séjour qui vaut autorisation de séjour d'une durée de validité de trois mois renouvelable. / Ce récépissé confère à son titulaire le droit d'exercer la profession de son choix dans les conditions prévues à l'article L. 314-4. / Le bénéficiaire de la protection subsidiaire est ensuite mis en possession de la carte de séjour temporaire prévue à l'article L. 313-13. " ;  <br/>
<br/>
              5. Considérant que les dispositions citées au point 3 fixent, pour les réfugiés, à la date de délivrance du titre de séjour provisoire établi à la suite de la reconnaissance de leur qualité de réfugié le point de départ du délai d'un an qui leur est imparti pour demander l'échange d'un permis délivré par un Etat n'appartenant ni à l'Union européenne ni à l'Espace économique européen ; que ces dispositions doivent être regardées comme également applicables à la situation des bénéficiaires de la protection subsidiaire qui, selon des dispositions analogues à celles qui valent pour les réfugiés, se voient délivrer après l'attribution de cette protection un titre de séjour provisoire dans l'attente de la délivrance d'une carte de séjour temporaire ; <br/>
<br/>
              6. Considérant par suite qu'en jugeant que, compte tenu du caractère très provisoire du récépissé délivré le 7 janvier 2015 à M.A..., sa délivrance ne pouvait être regardée comme valant acquisition de la résidence normale au sens des dispositions précitées et n'avait pu faire courir ce délai, le tribunal administratif de la Guadeloupe a commis une erreur de droit qui justifie l'annulation de son jugement ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui a été dit que le délai d'un an imparti à M. A... pour demander l'échange de son permis de conduire syrien a commencé à courir le 7 janvier 2015 ; que le moyen tiré de ce que le préfet se serait illégalement fondé, dans sa décision de refus du 25 août 2016, sur ce que ce délai était expiré lorsque l'intéressé a présenté sa demande d'échange le 14 juin 2016 doit, dès lors, être écarté ; que les conclusions de M. A... aux fins d'annulation de cette décision doivent être rejetées, ainsi que, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi enregistré sous le n° 415015 est rayé des registres du secrétariat du contentieux du Conseil d'Etat pour être joint au pourvoi n° 413436.<br/>
<br/>
Article 2 : Le jugement du 15 juin 2017 du tribunal administratif de la Guadeloupe est annulé.<br/>
<br/>
Article 3 : Les conclusions présentées par M. A...devant le tribunal administratif de la Guadeloupe sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur, à M. B... A...et au préfet de la Guadeloupe.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
