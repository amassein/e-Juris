<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027499110</ID>
<ANCIEN_ID>JG_L_2013_06_000000352653</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/49/91/CETATEXT000027499110.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 03/06/2013, 352653, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352653</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; HAAS</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:352653.20130603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 septembre et 13 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0800128 du 12 juillet 2011 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa demande tendant, d'une part, à l'annulation de la décision du 4 décembre 2007 par laquelle le maire de Rethel l'a autorisée à récupérer, à raison de dix heures hebdomadaires le mardi et le mercredi, sur la période allant du 11 décembre 2007 au 19 février 2008, les 103 heures supplémentaires qu'elle a effectuées au cours de l'année 2004, et, d'autre part, à la condamnation de la commune de Rethel à lui verser la somme de 1 316 euros en rémunération de ces heures supplémentaires et la somme de 1 000 euros au titre de son préjudice moral ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Rethel la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 91-875 du 6 septembre 1991 ;<br/>
<br/>
              Vu le décret n° 2002-60 du 14 janvier 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée avant et après les conclusions à la SCP Didier, Pinet, avocat de Mme A... et à Me Haas, avocat de la ville de Rethel ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que MmeA..., adjointe technique territoriale employée par la commune de Rethel (Ardennes), se pourvoit en cassation contre le jugement du 12 juillet 2011 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa demande tendant, d'une part, à l'annulation de la décision du 4 décembre 2007 du maire de la commune l'autorisant à récupérer, sous forme de repos compensateurs, les heures supplémentaires effectuées au cours de l'année 2004, et, d'autre part, à la condamnation de la commune de Rethel à lui verser la somme de 1 316 euros en rémunération de ces heures supplémentaires et la somme de 1 000 euros au titre du préjudice moral qu'elle estime avoir subi ;<br/>
<br/>
              2. Considérant, en premier lieu, que le jugement attaqué comporte le visa du code de justice administrative ; que, par suite, le moyen tiré de ce qu'il méconnaîtrait les dispositions de l'article R. 741-2 du code de justice administrative, faute de viser les dispositions du 2° de l'article R. 222-13 de ce code qui autorisent le jugement du litige par un magistrat statuant seul, doit être écarté ;<br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes du premier alinéa de l'article 88 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " L'assemblée délibérante de chaque collectivité territoriale ou le conseil d'administration d'un établissement public local fixe, par ailleurs, les régimes indemnitaires dans la limite de ceux dont bénéficient les différents services de l'Etat. [...] " ; qu'aux termes de l'article 1er du décret du 6 septembre 1991 pris pour l'application du premier alinéa de l'article 88 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le régime indemnitaire fixé par les assemblées délibérantes des collectivités territoriales et les conseils d'administration des établissements publics locaux pour les différentes catégories de fonctionnaires territoriaux ne doit pas être plus favorable que celui dont bénéficient les fonctionnaires de l'Etat exerçant des fonctions équivalentes. [...] " ; que l'article 2 du même décret dispose : " L'assemblée délibérante de la collectivité ou le conseil d'administration de l'établissement fixe, dans les limites prévues à l'article 1er, la nature, les conditions d'attribution et le taux moyen des indemnités applicables aux fonctionnaires de ces collectivités ou établissements. [...] " ;<br/>
<br/>
              4. Considérant que, pour rejeter la demande de MmeA..., le tribunal, après avoir cité les dispositions des articles 3 et 7 du décret du 14 janvier 2002 relatif aux indemnités horaires pour travaux supplémentaires selon lesquelles les heures supplémentaires accomplies peuvent être indemnisées à défaut de repos compensateur, a jugé qu'il ressortait des pièces du dossier que le conseil municipal de Rethel, seul compétent pour fixer le régime des indemnités horaires pour travaux supplémentaires de ses agents, n'avait pris aucune délibération tendant à indemniser les heures supplémentaires effectuées par les agents de la commune ; qu'en recherchant ainsi si les dispositions du décret du 14 janvier 2002 concernant le régime indemnitaire pour travaux supplémentaires applicable à la fonction publique d'Etat avaient été, conformément aux dispositions de l'article 88 de la loi du 26 janvier 1984, rendues applicables aux fonctionnaires de la commune, le tribunal n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation du jugement attaqué ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Rethel qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Rethel au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par la commune de Rethel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présence décision sera notifiée à Mme B...A...et à la commune de Rethel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
