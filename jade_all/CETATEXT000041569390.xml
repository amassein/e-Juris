<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569390</ID>
<ANCIEN_ID>JG_L_2020_02_000000421949</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/93/CETATEXT000041569390.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 12/02/2020, 421949</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421949</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:421949.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Le Toit parisien a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 16 mars 2016 par laquelle le maire de Paris lui a refusé la délivrance d'une attestation de permis de construire tacite et a précisé que sa demande de permis de construire n° 07511114V0040 avait fait l'objet d'un refus tacite de permis de construire le 26 septembre 2015. <br/>
<br/>
              Par un jugement n° 1607464 du 9 mars 2017, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA01548 du 4 mai 2018, la cour administrative d'appel de Paris a annulé ce jugement ainsi que la décision du 16 mars 2016 par laquelle le maire de Paris a refusé à la société Le Toit parisien la délivrance d'un certificat de permis de construire tacite.  <br/>
<br/>
              Par un pourvoi, enregistré le 3 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la Ville de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel et les conclusions de première instance de la société Le Toit parisien ;<br/>
<br/>
<br/>
<br/>
              3°) de mettre à la charge de la société Le Toit parisien la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la Ville de Paris et à la SCP Didier, Pinet, avocat de la société Le Toit parisien ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 4 novembre 2014, la société Le Toit parisien a déposé une demande de permis de construire portant sur la démolition de deux bâtiments, la surélévation d'un bâtiment existant et la construction d'un nouveau bâtiment sur une parcelle située au 40-42 rue de la Folie Regnault dans le 11ème arrondissement de Paris. La Ville de Paris a demandé au pétitionnaire de produire des pièces supplémentaires par deux courriers à la suite desquels de nouvelles pièces ont été déposées. La société Le Toit parisien a alors demandé à la Ville de Paris de lui délivrer une attestation de permis tacite. Par lettre du 16 mars 2016, la Ville de Paris a rejeté cette demande en l'informant que sa demande de permis de construire avait fait l'objet d'une décision implicite de rejet. Par jugement du 9 mars 2017, le tribunal administratif de Paris a rejeté la demande de la société Le Toit parisien tendant à l'annulation du refus de lui délivrer une attestation de permis tacite. La Ville de Paris se pourvoit contre l'arrêt du 4 mai 2018 par lequel la cour administrative d'appel de Paris, saisie par la société Le Toit parisien, a annulé ce jugement et la décision du 16 mars 2016 refusant de lui délivrer un permis de construire tacite.<br/>
<br/>
              2. Aux termes de l'article L. 451-1 du code de l'urbanisme : " Lorsque la démolition est nécessaire à une opération de construction ou d'aménagement, la demande de permis de construire (...) peut porter à la fois sur la démolition et sur la construction (...). Dans ce cas, le permis de construire (...) autorise la démolition ". L'article R. 424-1 du code de l'urbanisme prévoit que : " A défaut de notification d'une décision expresse dans le délai d'instruction déterminé comme il est dit à la section IV du chapitre III ci-dessus, le silence gardé par l'autorité compétente vaut, selon les cas : / (...) b) Permis de construire, permis d'aménager ou permis de démolir tacite. (...) ". Toutefois, aux termes de l'article R. 424-2 du même code : " Par exception au b de l'article R. 424-1, le défaut de notification d'une décision expresse dans le délai d'instruction vaut décision implicite de rejet dans les cas suivants : (...) / i) Lorsque le projet porte sur une démolition soumise à permis en site inscrit ".<br/>
<br/>
              3. Il résulte de ces dispositions que le défaut de notification d'une décision expresse dans le délai d'instruction vaut décision implicite de rejet lorsque la demande de permis de construire porte sur une démolition soumise à permis en site inscrit, y compris lorsque cette demande porte également sur une construction. Par suite, en jugeant que les dispositions de l'article R. 424-2 du code de l'urbanisme ne visent que les demandes de permis ou les déclarations préalables portant uniquement sur des travaux de démolition et en en déduisant que le projet de permis de construire litigieux, s'il comportait des démolitions en site inscrit nécessitant l'accord de l'architecte des bâtiments de France, n'était pas un projet " portant sur une démolition " au sens du i) de l'article R. 424-2 du code de l'urbanisme, la cour a commis une erreur de droit. Il s'ensuit, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la Ville de Paris est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la Ville de Paris, qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Le Toit parisien la somme de 3 000 euros à verser à la Ville de Paris au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 4 mai 2018 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La société Le Toit parisien versera à la Ville de Paris une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Le Toit parisien au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la Ville de Paris et à la société Le Toit parisien. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">41-01-05-04 MONUMENTS ET SITES. MONUMENTS HISTORIQUES. MESURES APPLICABLES AUX IMMEUBLES SITUÉS DANS LE CHAMP DE VISIBILITÉ D'UN ÉDIFICE CLASSÉ OU INSCRIT. PERMIS DE DÉMOLIR. - SILENCE VALANT DÉCISION IMPLICITE DE REJET - APPLICATION À UN PROJET PORTANT SUR UNE DÉMOLITION SOUMISE À PERMIS EN SITE INSCRIT - ABSENCE (ART. R. 424-2 DU CODE DE L'URBANISME), Y COMPRIS LORSQUE LA DEMANDE PORTE ÉGALEMENT SUR UNE CONSTRUCTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - REFUS DE DÉLIVRANCE D'UNE ATTESTATION DE PERMIS DE CONSTRUIRE TACITE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. - SILENCE VALANT DÉCISION IMPLICITE DE REJET - APPLICATION À UN PROJET PORTANT SUR UNE DÉMOLITION SOUMISE À PERMIS EN SITE INSCRIT - ABSENCE (ART. R. 424-2 DU CODE DE L'URBANISME), Y COMPRIS LORSQUE LA DEMANDE PORTE ÉGALEMENT SUR UNE CONSTRUCTION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-03-025-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. OCTROI DU PERMIS. PERMIS TACITE. - REFUS DE DÉLIVRANCE D'UNE ATTESTATION DE PERMIS DE CONSTRUIRE TACITE - DÉCISION SUSCEPTIBLE DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">68-06-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉCISION FAISANT GRIEF. - REFUS DE DÉLIVRANCE D'UNE ATTESTATION DE PERMIS DE CONSTRUIRE TACITE [RJ1].
</SCT>
<ANA ID="9A"> 41-01-05-04 Il résulte des articles L. 451-1, R. 424-1 et R. 424-2 du code de l'urbanisme que le défaut de notification d'une décision expresse dans le délai d'instruction vaut décision implicite de rejet lorsque la demande de permis de construire porte sur une démolition soumise à permis en site inscrit, y compris lorsque cette demande porte également sur une construction.,,,En jugeant que les dispositions de l'article R. 424-2 du code de l'urbanisme ne visent que les demandes de permis ou les déclarations préalables portant uniquement sur des travaux de démolition et en en déduisant que le projet de permis de construire litigieux, s'il comportait des démolitions en site inscrit nécessitant l'accord de l'architecte des bâtiments de France, n'était pas un projet portant sur une démolition au sens du i) de l'article R. 424-2 du code de l'urbanisme, la cour administrative d'appel a commis une erreur de droit.</ANA>
<ANA ID="9B"> 54-01-01-01 Le refus de délivrance d'une attestation de permis de construire tacite constitue un acte susceptible de faire l'objet d'un recours pour excès de pouvoir (sol. impl.).</ANA>
<ANA ID="9C"> 68-03-02 Il résulte des articles L. 451-1, R. 424-1 et R. 424-2 du code de l'urbanisme que le défaut de notification d'une décision expresse dans le délai d'instruction vaut décision implicite de rejet lorsque la demande de permis de construire porte sur une démolition soumise à permis en site inscrit, y compris lorsque cette demande porte également sur une construction.,,,En jugeant que les dispositions de l'article R. 424-2 du code de l'urbanisme ne visent que les demandes de permis ou les déclarations préalables portant uniquement sur des travaux de démolition et en en déduisant que le projet de permis de construire litigieux, s'il comportait des démolitions en site inscrit nécessitant l'accord de l'architecte des bâtiments de France, n'était pas un projet portant sur une démolition au sens du i) de l'article R. 424-2 du code de l'urbanisme, la cour administrative d'appel a commis une erreur de droit.</ANA>
<ANA ID="9D"> 68-03-025-02-01 Le refus de délivrance d'une attestation de permis de construire tacite constitue un acte susceptible de faire l'objet d'un recours pour excès de pouvoir (sol. impl.).</ANA>
<ANA ID="9E"> 68-06-01-01 Le refus de délivrance d'une attestation de permis de construire tacite constitue un acte susceptible de faire l'objet d'un recours pour excès de pouvoir (sol. impl.).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 juillet 1993, Société Les nouveaux constructeurs ouest, n° 129263, T. pp. 936-1107-1115. Comp., s'agissant du recours contre un certificat de non opposition à un projet ayant fait l'objet d'une déclaration préalable, qui doit être regardé comme dirigé contre l'autorisation dont l'existence a été révélée par l'affichage du certificat, CE, 12 décembre 2012, SCEA Pochon et GFA Pochon, n° 339220, T. pp. 935-1025.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
