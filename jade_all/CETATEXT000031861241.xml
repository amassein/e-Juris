<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861241</ID>
<ANCIEN_ID>JG_L_2015_12_000000382579</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/12/CETATEXT000031861241.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 23/12/2015, 382579, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382579</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET, HOURDEAUX ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382579.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La caisse primaire centrale d'assurance maladie des Bouches-du-Rhône a porté plainte contre Mme B...A..., masseur-kinésithérapeute devant la section des assurances sociales de la chambre disciplinaire de Provence-Alpes-Côte-d'Azur et Corse de l'ordre des médecins. Par décision du 2 octobre 2013, cette section a prononcé à l'encontre de Mme A...la sanction d'interdiction du droit de donner des soins aux assurés sociaux pendant six mois dont trois mois avec sursis et l'a condamnée à verser à la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône la somme de 35 773 euros. <br/>
<br/>
              Par une décision n° 5088 du 13 mai 2014, la section des assurances sociales du Conseil national de l'ordre des médecins a, sur appel de MmeA..., annulé la décision de première instance en tant qu'elle condamnait Mme A...à verser la somme de 35 773 euros et a rejeté le surplus de ses conclusions. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision en tant qu'elle annule la décision ayant condamné Mme A...à lui verser la somme de 35 773 euros ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la section des assurances sociales du conseil national de l'ordre des médecins ; <br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 2 800 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale, notamment son article L. 145-2 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il relève de l'office du juge du contrôle technique de déterminer, parmi celles qu'énumère la loi, la ou les sanctions qu'il entend infliger à un professionnel de santé ; que les dispositions du 4° de l'article L. 145-2 du code de la sécurité sociale mentionnent, parmi les sanctions susceptibles d'être prononcées par la section des assurances sociales du Conseil national de l'ordre des médecins, le reversement aux organismes de sécurité sociale du trop-remboursé aux assurés sociaux ; qu'aucune disposition législative ou réglementaire ne soumet le prononcé de cette sanction à la condition qu'elle ait été demandée par l'organisme concerné ni, par conséquent, à la condition que les éléments permettant d'établir le montant du trop-remboursé soient spontanément portés à la connaissance du juge par cet organisme, y compris dans le cas où celui-ci aurait demandé une telle sanction ; qu'il relève de l'office du juge du contrôle technique, s'il envisage d'infliger à un professionnel de santé le remboursement du trop-remboursé, de déterminer lui-même son montant, en faisant, le cas échéant, usage de ses pouvoirs d'instruction ;<br/>
<br/>
              2. Considérant qu'il résulte de ce qui précède qu'il incombait à la section des assurances sociales du Conseil national de l'ordre des médecins, si elle estimait insuffisants les éléments produits par la caisse primaire d'assurance maladie des Bouches-du-Rhône à l'appui de sa plainte pour justifier de son préjudice financier, de faire usage de ses pouvoirs d'instruction ou de déterminer elle-même le montant dont le remboursement devait être mis à la charge de Mme A... ; qu'en jugeant qu'elle ne pouvait prononcer la sanction prévue par le 4° de l'article L. 145-2 du code de la sécurité sociale au seul motif que les éléments versés au dossier ne lui permettaient pas de fixer avec exactitude la somme correspondant au trop remboursé par la sécurité sociale, elle a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, sa décision doit être annulée dans la mesure où elle statue sur la demande de remboursement de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...la somme de 1 500 euros à verser à la caisse primaire centrale d'assurance-maladie des Bouches-du-Rhône, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				---------------<br/>
<br/>
Article 1er : La décision de la section des assurances sociales du Conseil national de l'ordre des médecins du 13 mai 2014 est annulée en tant qu'elle statue sur la demande de remboursement de la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la section des assurances sociales du Conseil national de l'ordre des médecins.<br/>
<br/>
Article 3 : Mme A...versera une somme de 1 500 euros à la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la caisse primaire centrale d'assurance maladie des Bouches-du-Rhône, à Mme B...A...et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
