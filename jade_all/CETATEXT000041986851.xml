<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041986851</ID>
<ANCIEN_ID>JG_L_2020_06_000000425417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/68/CETATEXT000041986851.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 10/06/2020, 425417</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425417.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'association Les Riverains du port et Mme A... C... épouse B... ont demandé au tribunal administratif de Nantes d'annuler l'arrêté du 4 juillet 2018 de la ministre auprès du ministre d'Etat, ministre de la transition écologique et solidaire, chargée des transports, autorisant la création d'une hélistation par la commune de l'Île d'Yeu.<br/>
<br/>
              Par une ordonnance n° 1808676 en date du 4 octobre 2018, le président du tribunal administratif de Nantes a, par application de l'article R. 351-3 du code de justice administrative, transmis la demande de l'association Les Riverains du port et de Mme B... au tribunal administratif de Paris.<br/>
<br/>
              Par une ordonnance n° 1817612 du 14 novembre 2018, le président du tribunal administratif de Paris a transmis cette demande au président de la section du contentieux du Conseil d'Etat, par application des dispositions de l'article R. 351-6 du code de justice administrative. <br/>
<br/>
              Par cette demande et deux mémoires en réplique, enregistrés les 30 juillet et 28 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Les Riverains du Port et Mme B... demandent :<br/>
<br/>
              1°) l'annulation pour excès de pouvoir de l'arrêté du 4 juillet 2018 ;<br/>
<br/>
              2°) que soit mise à la charge de l'Etat, pour chacun des requérants, une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des transports ;<br/>
              - le code de l'aviation civile ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de l'association Les Riverains du Port et de Mme B... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du ministre de la transition écologique et solidaire ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 6312-2 du code des transports dispose : " Les aérodromes non ouverts à la circulation aérienne publique comprennent : 1° Les aérodromes réservés à l'usage d'administrations de l'Etat ; / 2° Les aérodromes à usage restreint, autres que les aérodromes à l'usage d'administrations de l'Etat ; / 3° Les aérodromes à usage privé. / Les conditions de leur création et de leur mise en service sont fixées par voie réglementaire ". L'article D. 231-1 du code de l'aviation civile, applicable aux " aérodromes pour hélicoptères " selon l'article D. 211-1 de ce code, prévoit que la décision de créer un aérodrome à usage restreint, soumise à enquête technique, est prise par arrêté ministériel ou préfectoral et sa mise en service autorisée par arrêté conjoint des ministres dont ils dépendent et du ministre chargé de l'aviation civile. Si la décision autorisant la mise en service d'un tel aérodrome revêt un caractère réglementaire, il n'en va pas de même de celle, préalable, ayant pour seul objet d'en autoriser la création.<br/>
<br/>
              2. Par l'arrêté du 4 juillet 2018 dont les requérants demandent l'annulation pour excès de pouvoir, le ministre chargé des transports a autorisé la création, par la commune de l'Ile-d'Yeu (Vendée), de l'hélistation Port-Joinville, destinée à être agréée à usage restreint. Cet arrêté ne revêtant pas un caractère réglementaire, la demande d'annulation ne soulève pas un litige dont il appartient au Conseil d'État de connaître en premier ressort en vertu de l'article R. 311-1 du code de justice administrative.<br/>
<br/>
              3. Aux termes de l'article R. 312-7 du code de justice administrative : " Les litiges relatifs aux déclarations d'utilité publique, au domaine public, aux affectations d'immeubles, au remembrement, à l'urbanisme et à l'habitation, au permis de construire, d'aménager ou de démolir, au classement des monuments et des sites et, de manière générale, aux décisions concernant des immeubles relèvent de la compétence du tribunal administratif dans le ressort duquel se trouvent les immeubles faisant l'objet du litige. (...) ".<br/>
<br/>
              4. La demande de l'association Les Riverains du port et de Mme B... doit être regardée comme soulevant un litige relatif à une décision concernant un immeuble, au sens des dispositions de l'article R. 312-7 du code de justice administrative. Elle relève, par suite, de la compétence en premier ressort du tribunal administratif de Nantes, dans le ressort duquel se trouve le terrain d'assiette de l'hélistation faisant l'objet du litige. <br/>
<br/>
              5. Il résulte de ce qui précède qu'il y a lieu d'attribuer le jugement de la demande au tribunal administratif de Nantes.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la demande de l'association Les Riverains du port et de Mme B... est attribué au tribunal administratif de Nantes.<br/>
Article 2 : La présente décision sera notifiée à l'association Les Riverains du Port, à Mme A... B..., à la commune de l'Ile d'Yeu, à la ministre de la transition écologique et solidaire, au président du tribunal administratif de Paris et au président du tribunal administratif de Nantes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. NE PRÉSENTENT PAS CE CARACTÈRE. - ARRÊTÉ PORTANT CRÉATION D'UN AÉRODROME À USAGE RESTREINT (ART. D. 231-1 DU CAC) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - RECOURS DIRIGÉ CONTRE L'ARRÊTÉ DU MINISTRE CHARGÉ DES TRANSPORTS AUTORISANT LA CRÉATION D'UN AÉRODROME À USAGE RESTREINT (ART. D. 231-1 DU CODE DE L'AVIATION CIVILE) - LITIGE RELATIF À UN IMMEUBLE AU SENS DE L'ARTICLE R. 312-7 DU CJA.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">17-05-02-04 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. ACTES RÉGLEMENTAIRES DES MINISTRES. - ARRÊTÉ PORTANT CRÉATION D'UN AÉRODROME À USAGE RESTREINT (ART. D. 231-1 DU CAC) - EXCLUSION [RJ1] [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">65-03-04 TRANSPORTS. TRANSPORTS AÉRIENS. AÉROPORTS. - RECOURS CONTRE UN ARRÊTÉ PORTANT CRÉATION D'UN AÉRODROME À USAGE RESTREINT (ART. D. 231-1 DU CAC) - 1) ACTE NE REVÊTANT PAS UN CARACTÈRE RÉGLEMENTAIRE [RJ1] - CONSÉQUENCE - COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT - ABSENCE [RJ2] - 2) LITIGE RELATIF À UN IMMEUBLE AU SENS DE L'ARTICLE R. 312-7 DU CJA - INCLUSION - CONSÉQUENCE - COMPÉTENCE DU TRIBUNAL ADMINISTRATIF DANS LE RESSORT DUQUEL SE SITUE LE LOGEMENT EN CAUSE.
</SCT>
<ANA ID="9A"> 01-01-06-01-02 L'article D. 231-1 du code de l'aviation civile (CAC) prévoit que la décision de créer un aérodrome à usage restreint, soumise à enquête technique, est prise par arrêté ministériel ou préfectoral et sa mise en service autorisée par arrêté conjoint des ministres dont il dépend et du ministre chargé de l'aviation civile. Si la décision autorisant la mise en service d'un tel aérodrome revêt un caractère réglementaire, il n'en va pas de même de celle, préalable, ayant pour seul objet d'en autoriser la création.</ANA>
<ANA ID="9B"> 17-05-01-02 La demande tendant à l'annulation d'un arrêté du ministre chargé des transports, pris sur le fondement de l'article D. 231-1 du code de l'aviation civile (CAC), autorisant la création d'un aérodrome à usage restreint doit être regardée comme soulevant un litige relatif à une décision concernant un immeuble, au sens des dispositions de l'article R. 312-7 du code de justice administrative (CJA).,,,Elle relève, par suite, de la compétence en premier ressort du tribunal administratif dans le ressort duquel est situé l'aérodrome en cause.</ANA>
<ANA ID="9C"> 17-05-02-04 L'article D. 231-1 du code de l'aviation civile (CAC) prévoit que la décision de créer un aérodrome à usage restreint, soumise à enquête technique, est prise par arrêté ministériel ou préfectoral et sa mise en service autorisée par arrêté conjoint des ministres dont il dépend et du ministre chargé de l'aviation civile. Si la décision autorisant la mise en service d'un tel aérodrome revêt un caractère réglementaire, il n'en va pas de même de celle, préalable, ayant pour seul objet d'en autoriser la création. Par suite, la demande tendant à l'annulation d'un arrêté du ministre chargé des transports autorisant la création d'un aérodrome à usage restreint ne soulève pas un litige dont il appartient au Conseil d'État de connaître en premier ressort en vertu de l'article R. 311-1 du code de justice administrative.</ANA>
<ANA ID="9D"> 65-03-04 1) L'article D. 231-1 du code de l'aviation civile (CAC) prévoit que la décision de créer un aérodrome à usage restreint, soumise à enquête technique, est prise par arrêté ministériel ou préfectoral et sa mise en service autorisée par arrêté conjoint des ministres dont il dépend et du ministre chargé de l'aviation civile. Si la décision autorisant la mise en service d'un tel aérodrome revêt un caractère réglementaire, il n'en va pas de même de celle, préalable, ayant pour seul objet d'en autoriser la création. Par suite, la demande tendant à l'annulation d'un arrêté du ministre chargé des transports autorisant la création d'un aérodrome à usage restreint ne soulève pas un litige dont il appartient au Conseil d'État de connaître en premier ressort en vertu de l'article R. 311-1 du code de justice administrative.,,,2) Un telle demande doit être regardée comme soulevant un litige relatif à une décision concernant un immeuble, au sens des dispositions de l'article R. 312-7 du code de justice administrative. Elle relève, par suite, de la compétence en premier ressort du tribunal administratif dans le ressort duquel est situé le logement en cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant d'un acte portant à la fois création et agrément d'un aérodrome, CE, 29 juillet 1998, Compagnie financière du Sud-Est, n° 193372, T. pp. 675-831-1195., ,[RJ2] Comp., s'agissant de la compétence du Conseil d'Etat pour statuer, en raison de leur connexité, sur deux recours dirigés, d'une part, contre l'arrêté ministériel portant création d'un aérodrome, d'autre part, contre l'arrêté ministériel portant agrément et fixation de ses conditions d'utilisation, CE, 28 avril 2014, Commune de Baons-le-Comte, n°s 373193 373194, T. pp. 822-883.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
