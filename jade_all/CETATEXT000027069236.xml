<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027069236</ID>
<ANCIEN_ID>JG_L_2013_02_000000350780</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/92/CETATEXT000027069236.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 15/02/2013, 350780, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350780</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ANCEL, COUTURIER-HELLER, MEIER-BOURDEAU ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:350780.20130215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 juillet et 10 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Assemblée des chambres françaises de commerce et d'industrie (ACFCI), dont le siège est 46, avenue de la Grande Armée, CS 50071 à Paris Cedex 17 (75858), représentée par son président ; l'ACFCI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0905149/5-1 du 5 mai 2011 par lequel le tribunal administratif de Paris, faisant droit à la demande de M. A... B..., a annulé la décision du 5 novembre 2008 par laquelle le directeur général adjoint de l'ACFCI a rejeté sa demande tendant au paiement de la somme de 3 097,61 euros dont il est fait mention sur son bulletin de paye du mois de juillet 2008 et l'a renvoyé devant l'ACFCI pour qu'il soit procédé à la liquidation de l'indemnité correspondant à l'écart de rémunération résultant de l'application de la valeur du point national, assortie des intérêts légaux à compter du 4 novembre 2008 et de la capitalisation des intérêts à compter du 4 juin 2010 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 52-1311 du 10 décembre 1952 ;<br/>
<br/>
              Vu l'arrêté du secrétaire d'Etat aux petites et moyennes entreprises, au commerce et à l'artisanat et du secrétaire d'Etat à l'industrie du 25 juillet 1997 relatif au statut du personnel de l'assemblée des chambres françaises de commerce et d'industrie, des chambres régionales de commerce et d'industrie, des chambres de commerce et d'industrie et des groupements interconsulaires ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Richard, avocat de l'Assemblée  des chambres françaises de commerce et d'industrie et de la SCP Ancel, Couturier-Heller, Meier-Bourdeau, avocat de M. A...B...,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Richard, avocat de l'Assemblée des chambres françaises de commerce et d'industrie et à la SCP Ancel, Couturier-Heller, Meier-Bourdeau, avocat de M. A...B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'en vertu de l'article 15 du statut du personnel des établissements du réseau des chambres de commerce et d'industrie, la rémunération mensuelle indiciaire des agents titulaires et stagiaires est calculée en multipliant la somme de l'indice de qualification, de l'indice des résultats professionnels individuels et de l'indice d'expérience, par la valeur du point national ; qu'afin de mettre un terme au versement, dans certains établissements du réseau, dont l'assemblée des chambres françaises de commerce et d'industrie (ACFCI), de rémunérations fondées sur une valeur du point supérieure à la valeur du point national, les compagnies consulaires ont engagé des négociations avec les organisations syndicales pour harmoniser la valeur du point au niveau national, tout en maintenant inchangé le niveau de rémunération des agents ; que, le 6 mai 2008, la commission paritaire locale de l'ACFCI a approuvé un accord, annexé à l'article 15 du règlement intérieur et entré en vigueur le 1er juillet 2008, précisant les modalités de compensation de l'harmonisation de la valeur du point national pour les agents de l'ACFCI ; que, le 10 juin 2008, la commission paritaire nationale des établissements du réseau des chambres de commerce et d'industrie a approuvé un accord, annexé à l'article 53 du statut, obligeant tous les établissements du réseau à appliquer la valeur du point national au plus tard le 1er janvier 2009 et fixant les modalités de la compensation de l'harmonisation de la valeur du point national pour les agents de tous les établissements concernés ; que l'ACFCI se pourvoit en cassation contre le jugement du 5 mai 2011 par lequel le tribunal administratif de Paris a annulé la décision du 5 novembre 2008 par laquelle son directeur général adjoint a rejeté la demande de M. A... B...tendant à ce que la compensation de l'harmonisation de la valeur du point national prenne pour lui la forme, non d'un abondement de 28,10 jours de son compte épargne-temps, mais d'un virement de 3 097,61 euros sur son compte bancaire ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 53 du statut du personnel des établissements du réseau des chambres de commerce et d'industrie, dans sa version postérieure au 10 juin 2008 : " La mise en vigueur du présent statut ne pourra, en aucun cas, être la cause ou l'occasion ni d'une diminution des rémunérations, ni d'une compression du personnel en fonctions. / La valeur du point national s'impose à tous les établissements du réseau. Les compagnies consulaires [...] doivent se mettre en conformité au plus tard pour le 1er janvier 2009. Les modalités de retour à la norme nationale seront déterminées au niveau local dans le respect des dispositions statutaires (voir annexe). " ; qu'aux termes de l'article 3 de l'annexe à cet article 53 : " Chaque compagnie consulaire doit s'approprier les présentes dispositions en fonction de ses spécificités et effectuer une transposition dans le cadre d'un accord local adopté en commission paritaire locale. " ; qu'aux termes de l'article 7 de la même annexe : " Les partenaires sociaux retiennent plusieurs types de modalités de retour à la valeur nationale du point. [...] Il appartient à chaque compagnie consulaire concernée de déterminer, en commission paritaire locale, l'une des solutions ci-après présentées en fonction du contexte local et de l'adapter dans le strict respect des dispositions statutaires. " ; qu'en vertu de ce même article, une première solution consistait en une indemnité différentielle, dont il était prévu qu'elle puisse être soit versée sur le compte épargne-temps de l'agent et convertie en jours (option 1), soit virée sur un compte bancaire moyennant une décote de 2,5% par année séparant l'agent de l'âge de 60 ans (option 2), et une seconde solution consistait à combler chaque mois l'écart de rémunération ; qu'aux termes de l'article 54-1 du même statut : " L'ouverture d'un compte épargne-temps est accordée aux agents titulaires qui en font la demande dans les conditions fixées par l'accord annexé au présent statut. " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées de l'article 53 du statut du personnel des établissements du réseau des chambres de commerce et d'industrie et de son annexe que, pour compenser les effets pour chaque agent de l'harmonisation de la valeur du point national, chacune des compagnies consulaires appliquant une valeur du point supérieure à la valeur du point national avait non seulement le choix entre le versement d'une indemnité différentielle et le comblement mensuel de l'écart de rémunération, mais également toute latitude pour adapter la solution retenue, et notamment, en cas de choix en faveur d'une indemnité différentielle, pour arrêter  les modalités de son versement sous forme soit de l'abondement du compte épargne-temps de l'agent, qui pouvait donc, dans cette hypothèse, être ouvert à l'initiative de la compagnie consulaire, soit d'un virement bancaire ; que, dès lors, en jugeant qu'il résultait des dispositions précitées que les compagnies consulaires qui avaient choisi de compenser les effets de l'harmonisation de la valeur du point national par le versement d'une indemnité différentielle étaient tenues de proposer aux agents le choix entre l'abondement de leur compte épargne-temps et un virement bancaire et ne pouvaient rendre obligatoire l'abondement de leur compte épargne-temps, qui ne pouvait être ouvert qu'à la demande des agents concernés, le tribunal administratif a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'ACFCI est fondée à demander l'annulation du jugement attaqué ;<br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'ACFCI au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'ACFCI, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 5 mai 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : Le surplus des conclusions du pourvoi de l'ACFCI est rejeté.<br/>
Article 4 : Les conclusions de M. B... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'Assemblée des chambres françaises de commerce et d'industrie et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
