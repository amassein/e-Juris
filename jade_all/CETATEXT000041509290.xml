<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041509290</ID>
<ANCIEN_ID>JG_L_2020_01_000000426346</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/50/92/CETATEXT000041509290.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 30/01/2020, 426346</TITRE>
<DATE_DEC>2020-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426346</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; HAAS ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426346.20200130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux requêtes distinctes, M. B... C..., M. E... D..., Mme A... D... et Mme F... G... ont demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir, d'une part, l'arrêté du 1er décembre 2016 par lequel le maire de Chaponnay a délivré un permis de construire à la société Villa Cité 4 en vue de l'édification d'un ensemble immobilier de 20 logements et, d'autre part, l'arrêté du 2 mars 2018 par lequel le maire de Chaponnay a délivré un permis de construire modificatif à cette même société. Par un jugement n°s 1700763 et 1803319 du 11 octobre 2018, le tribunal administratif de Lyon a rejeté leurs demandes. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 décembre 2018 et 18 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. C... et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Chaponnay et de la société Villa Cité 4 la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B... C..., de M. E... D..., de Mme A... D... et de Mme F... G..., à Me Haas, avocat de la commune de Chaponnay et à la SCP Delamarre, Jéhannin, avocat de la SCI Villa Cité 4  ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Dans sa rédaction applicable au litige, l'article R. 611-7-1 du code de justice administrative dispose que : " Lorsque l'affaire est en état d'être jugée, le président de la formation de jugement ou, au Conseil d'Etat, le président de la chambre chargée de l'instruction peut, sans clore l'instruction, fixer par ordonnance la date à compter de laquelle les parties ne peuvent plus invoquer de moyens nouveaux ". Il résulte de ces dispositions que si le président d'une formation de jugement d'un tribunal administratif, lorsqu'il considère qu'une affaire est en état d'être jugée, peut fixer par ordonnance, dans le cadre de l'instance et avant la clôture de l'instruction, une date à compter de laquelle les parties ne peuvent plus invoquer de moyens nouveaux, une telle faculté n'est possible qu'après l'expiration du délai donné aux requérants pour répliquer au premier mémoire en défense. <br/>
<br/>
              2. Il ressort des pièces de la procédure que, par une ordonnance du 25 octobre 2017, le président de la formation de jugement du tribunal administratif de Lyon a fixé au 30 novembre 2017 le délai au-delà duquel les parties ne pouvaient plus invoquer de moyens nouveaux, alors que ni la commune Chaponnay, ni la société Villa Cité 4 n'avaient encore produit de mémoire en défense. Il résulte de ce qui a été dit au point 1 qu'en l'absence de production d'écritures en défense, une telle ordonnance ne pouvait être adoptée sans méconnaître les dispositions précitées de l'article R. 611-7-1. Par suite, en écartant comme irrecevable le moyen invoqué pour la première fois par les requérants dans un mémoire enregistré postérieurement à l'expiration du délai imparti par l'ordonnance du 25 octobre 2017, le tribunal a entaché son jugement d'erreur de droit. <br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. C... et autres sont fondés à demander l'annulation du jugement qu'ils attaquent. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit à leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. C... et autres qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 11 octobre 2018 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon.<br/>
Article 3 :  Les conclusions présentées par M. B... C... et autres, la commune de Chaponnay et la société Villa Cité 4 au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. B... C... et autres, à la commune de Chaponnay et à la société Villa Cité 4.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. - ORDONNANCE DU PRÉSIDENT D'UNE FORMATION DE JUGEMENT FIXANT UNE DATE À COMPTER DE LAQUELLE LES PARTIES NE PEUVENT PLUS INVOQUER DE MOYENS NOUVEAUX (ART. R. 611-7-1 DU CJA) - CONDITION QUE L'AFFAIRE SOIT EN ÉTAT D'ÊTRE JUGÉE - PORTÉE - EXIGENCE QUE LE DÉLAI DONNÉ POUR RÉPLIQUER AU PREMIER MÉMOIRE EN DÉFENSE SOIT EXPIRÉ [RJ1].
</SCT>
<ANA ID="9A"> 54-04-01 Il résulte de l'article R. 611-7-1 du code de justice administrative (CJA) que si le président d'une formation de jugement d'un tribunal administratif, lorsqu'il considère qu'une affaire est en état d'être jugée, peut fixer par ordonnance, dans le cadre de l'instance et avant la clôture de l'instruction, une date à compter de laquelle les parties ne peuvent plus invoquer de moyens nouveaux, une telle faculté n'est possible qu'après l'expiration du délai donné aux requérants pour répliquer au premier mémoire en défense.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la possibilité de prononcer un non-lieu en l'état, CE, 25 novembre 1955,,, T. p. 777.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
