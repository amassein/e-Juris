<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728707</ID>
<ANCIEN_ID>JG_L_2019_12_000000422131</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/87/CETATEXT000039728707.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 31/12/2019, 422131, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422131</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422131.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 10 juillet 2018 et 13 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération des acteurs de la solidarité, la Fédération des établissements hospitaliers et d'aide à la personne, la Croix-Rouge française, l'association Nexem et l'Union nationale interfédérale des oeuvres et organismes privés non lucratifs sanitaires et sociaux demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de la cohésion des territoires et du ministre de l'action et des comptes publics du 2 mai 2018 fixant les tarifs plafonds prévus au deuxième alinéa de l'article L. 314-4 du code de l'action sociale et des familles applicable aux établissements mentionnés au 8° du I de l'article L. 312-1 du même code au titre de l'année 2018 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2017-1837 du 30 décembre 2017 ;<br/>
              - le décret n° 2017-1075 du 24 mai 2017 ;<br/>
              - le décret n°2005-850 du 27 juillet 2005 ;<br/>
              - l'arrêté du 25 janvier 2010 portant organisation de la direction générale de la cohésion sociale en services, en sous-directions et en bureaux ;<br/>
              - l'arrêté du 12 mars 2018 fixant le modèle du tableau d'analyse de l'activité et des coûts de l'enquête nationale de coûts applicable au secteur de l'accueil, de l'hébergement et de l'insertion prévue aux articles L. 345-1 et L. 322-8-1 du code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte du deuxième alinéa de l'article L. 314-4 du code de l'action sociale et des familles que le montant total annuel des dépenses prises en compte pour le calcul des dotations globales de fonctionnement des établissements et services mentionnés au 8° du I de l'article L. 312-1 du même code est réparti en dotations régionales limitatives, dont le montant " est fixé par le ministre chargé de l'action sociale, en fonction des besoins de la population, des priorités définies au niveau national en matière de politique médico-sociale, en tenant compte de l'activité et des coûts moyens des établissements et services et d'un objectif de réduction progressive des inégalités dans l'allocation des ressources entre régions. A cet effet, un arrêté interministériel fixe, annuellement, les tarifs plafonds ou les règles de calcul desdits tarifs plafonds (...), ainsi que les règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds ". Pour l'application de ces dispositions, un arrêté du ministre de la cohésion des territoires et du ministre de l'action et des comptes publics du 2 mai 2018, dont les associations requérantes demandent l'annulation pour excès de pouvoir, a fixé les tarifs plafonds applicables pour l'année 2018 aux centres d'hébergement et de réinsertion sociale, qui ont pour mission, en vertu du 8° du I de l'article L. 312-1 du code de l'action sociale et des familles, d'assurer l'accueil, notamment dans les situations d'urgence, le soutien ou l'accompagnement social, l'adaptation à la vie active ou l'insertion sociale et professionnelle des personnes ou des familles en difficulté ou en situation de détresse.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. En premier lieu, aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / 2° Les chefs de service (...). / Cette délégation s'exerce sous l'autorité du ou des ministres et secrétaires d'Etat dont relèvent les agents, ainsi que, le cas échéant, de leur supérieur hiérarchique immédiat (...) ". Il résulte des articles 3 et 4 de l'arrêté du 25 janvier 2010 portant organisation de la direction générale de la cohésion sociale en services, en sous-directions et en bureaux que le service des politiques sociales et médico-sociales pilote le " dispositif d'accueil d'hébergement et de l'insertion des personnes sans abri ou mal logées ". Il suit de là que, contrairement à ce que soutiennent les requérantes, le chef de ce service était au nombre des personnes ayant, du fait de leur nomination, compétence pour signer l'arrêté attaqué au nom du ministre de la cohésion des territoires qui, en vertu du décret du 24 mai 2017 relatif à ses attributions, " élabore et met en oeuvre la politique en faveur du logement et de l'hébergement des populations en situation d'exclusion " et  dispose à cette fin de la direction générale de la cohésion sociale. Le moyen tiré de ce que l'arrêté attaqué serait entaché d'incompétence doit, par suite, être écarté.<br/>
<br/>
              3. En second lieu, il résulte des termes mêmes de l'article L. 314-4 du code de l'action sociale et des familles cité ci-dessus que les auteurs de l'arrêté attaqué étaient compétents, ainsi qu'ils l'ont fait à l'article 2 de l'arrêté litigieux, pour fixer au titre de l'année 2018 les tarifs plafonds dans la limite desquels le représentant de l'Etat dans le département détermine, en sa qualité d'autorité de tarification, le tarif des prestations de chaque établissement. En vertu des mêmes dispositions, ils étaient également compétents pour déterminer, ainsi qu'ils l'ont fait à l'article 3 de l'arrêté litigieux, les conditions, relevant des " règles permettant de ramener les tarifs pratiqués au niveau des tarifs plafonds ", dans lesquelles les nouveaux tarifs plafonds s'appliquent aux établissements présentant un coût de fonctionnement brut à la place supérieur à ces tarifs plafonds. Si l'article 3 de l'arrêté litigieux prévoit également que l'autorité compétente de l'Etat peut procéder à une tarification d'office de l'établissement en l'absence de transmission des données prévues à l'article L. 345-1 du code de l'action sociale et des familles, il se borne sur ce point à rappeler la règle prévue par ces mêmes dispositions. Enfin, les dispositions de l'article L. 314-8 du même code, qui confient de manière générale à un décret en Conseil d'Etat le soin de déterminer les modalités de fixation des tarifs des centres d'hébergement et de réinsertion sociale, ne faisaient nullement obstacle à ce que l'arrêté attaqué, pris sur le fondement de dispositions législatives distinctes, détermine des règles ayant pour effet d'encadrer le pouvoir d'appréciation de l'autorité de tarification dans la fixation des tarifs de ces mêmes établissements. Par suite, le moyen tiré de ce que l'arrêté attaqué aurait institué une nouvelle procédure de tarification et serait entaché d'incompétence doit être écarté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              En ce qui concerne la fixation, à l'article 2 de l'arrêté attaqué, des tarifs plafonds :<br/>
<br/>
              4. Il résulte des dispositions précitées de l'article L. 314-4 du code de l'action sociale et des familles que la fixation des tarifs plafonds applicables aux centres d'hébergement et de réinsertion sociale doit, comme la détermination des dotations régionales limitatives mentionnées au même article, être arrêtée en fonction des besoins de la population et des priorités définies au niveau national en matière de politique médico-sociale, en tenant compte de l'activité et des coûts moyens de ces structures ainsi que d'un objectif de réduction progressive des inégalités dans l'allocation des ressources entre régions.<br/>
<br/>
              5. En vertu de l'article L. 345-1 du code de l'action sociale et des familles, dans sa rédaction issue de l'article 128 de la loi du 30 décembre 2017 de finances pour 2018, les centres d'hébergement et de réinsertion sociale remplissent chaque année une enquête nationale de coûts relative au secteur de l'accueil, de l'hébergement et de l'insertion, pour le recueil des données relatives à l'année précédente. Par un arrêté du 12 mars 2018, le ministre de la cohésion des territoires a prévu que cette enquête serait fondée, comme l'étude réalisée annuellement auprès des établissements volontaires à laquelle elle se substitue, sur le classement de chaque établissement dans douze groupes homogènes d'activité et de missions identifiés en fonction des types d'hébergement proposés et des missions assurées. L'arrêté attaqué fixe les tarifs plafonds applicables aux centres d'hébergement et de réinsertion sociale par place et par groupe homogène d'activité et de missions.<br/>
<br/>
              6. Ni les dispositions de l'article L. 314-4 du code de l'action sociale et des familles, ni aucune autre disposition législative ou réglementaire, ne faisaient obstacle à ce que les tarifs plafonds soient différenciés en fonction des missions assurées par les établissements et du type d'hébergement qu'ils proposent. Cette différenciation des tarifs plafonds vise à mieux prendre en compte les charges supportées par les établissements et services dans l'allocation des ressources de l'Etat. Il ne ressort pas des pièces du dossier que le recours aux groupes homogènes d'activité et de missions identifiés par l'arrêté du 12 mars 2018 mentionné au point 5 aurait été manifestement impropre à satisfaire à l'objectif ainsi poursuivi.<br/>
<br/>
              7. Il ressort des pièces du dossier que les tarifs plafonds ont été fixés par l'article 2 de l'arrêté attaqué pour 2018 au vu des résultats d'une étude nationale de coûts dont il n'est pas contesté par les requérantes qu'elle a été réalisée sur la base des données de l'exercice 2015 portant sur l'activité de 672 centres d'hébergement et de réinsertion sociale sur un total de 797. La circonstance que la réalisation de cette étude n'était prévue par aucune disposition législative ou réglementaire et que l'étude n'aurait pas fait l'objet d'une publication était sans incidence sur la possibilité de se fonder sur ses résultats. L'échantillon retenu par l'étude était suffisant pour permettre aux ministres d'appréhender les coûts moyens des établissements et services et d'identifier les facteurs expliquant les écarts à la moyenne. Si les associations requérantes font valoir que les valeurs extrêmes n'ont pas été exclues du calcul de la moyenne des coûts, elles ne précisent pas en quoi leur prise en considération a pu altérer la pertinence de la moyenne constatée, alors que le ministre indique sans être sérieusement contredit sur ce point que les données qui se rapportaient à des circonstances exceptionnelles, ou qui sont apparues comme aberrantes ou comme manifestant des erreurs de remplissage ont été écartées. Le moyen tiré de ce que les ministres ne se seraient pas fondés sur des données de coûts fiables et auraient, pour ce motif, méconnu les dispositions de l'article L. 314-4 du code de l'action sociale et des familles doit, par suite, être écarté.<br/>
<br/>
              8. Si les associations requérantes font valoir que les coûts, en particulier immobiliers, supportés par les centres d'hébergement et de réinsertion sociale sont hétérogènes, il ne ressort pas des pièces du dossier que, eu égard à la finalité de l'institution de tarifs plafonds, qui correspondent au coût maximal théorique de fonctionnement par place que l'Etat entend prendre en charge, et à l'objectif de réduction progressive des inégalités dans l'allocation des ressources entre régions fixé par l'article L. 314-4 du code de l'action sociale et des familles, les ministres auraient, en adoptant des tarifs plafonds uniformes pour l'ensemble du territoire, sous la seule réserve de majorations dans les collectivités d'outre-mer, commis une erreur manifeste d'appréciation.<br/>
<br/>
              9. Les tarifs plafonds ont été fixés au niveau des coûts moyens constatés en 2015 majorés de 5 %. Si les requérantes font valoir que plus de la moitié des centres d'hébergement et de réinsertion sociale seront régis par ces tarifs plafonds, il résulte de l'article 3 de l'arrêté attaqué que le financement accordé en 2017 est en principe maintenu en 2018 au même niveau diminué du quart de l'écart avec le financement qui résulterait de l'application des tarifs plafonds. Compte tenu de l'objectif de rapprochement des tarifs pratiqués au niveau des tarifs plafonds et de réduction progressive des inégalités dans l'allocation des ressources entre régions, il ne ressort pas des pièces du dossier qu'en fixant les tarifs  plafonds aux niveaux prévus par l'arrêté les ministres auraient commis une erreur manifeste d'appréciation ni qu'ils auraient insuffisamment tenu compte des besoins de la population et des priorités nationales, comme l'exige l'article L. 314-4 du code de l'action sociale et des familles.<br/>
<br/>
              En ce qui concerne la fixation, à l'article 3, des règles applicables aux établissements présentant un coût de fonctionnement brut à la place supérieur à ces tarifs plafonds :<br/>
<br/>
              10. Par son article 3, l'arrêté attaqué prévoit que les centres d'hébergement et de réinsertion sociale dont le coût de fonctionnement brut à la place constaté au 31 décembre 2017 est supérieur aux tarifs plafonds prévus pour l'année 2018 perçoivent pour cet exercice un financement égal à celui accordé en 2017 diminué du quart de l'écart entre ce financement et celui qui résulterait de l'application des tarifs plafonds. C'est par des dispositions suffisamment claires et précises que ce même article permet à l'autorité de tarification d'appliquer un taux d'effort budgétaire supérieur dans le cadre de la procédure prévue à l'article L. 314-7 du code de l'action sociale et des familles afin de tenir compte des moyennes observées sur son territoire et des écarts à ces moyennes pour des établissements dont l'activité est comparable, sans que les abattements sur les charges opérés dans ce cadre puissent aboutir à un coût brut à la place inférieur au tarif plafond applicable. Par suite, les dispositions de cet article ne méconnaissent, pas davantage que celles du reste de l'arrêté, l'objectif de valeur constitutionnelle d'intelligibilité du droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation de l'arrêté qu'elles attaquent.<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la Fédération des acteurs de la solidarité et des autres associations requérantes est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée, pour l'ensemble des requérantes, à la Fédération des acteurs de la solidarité, première dénommée, à la ministre des solidarités et de la santé et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
