<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045084099</ID>
<ANCIEN_ID>JG_L_2022_01_000000458102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/08/40/CETATEXT000045084099.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 28/01/2022, 458102, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>458102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>Mme Ségolène Cavaliere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2022:458102.20220128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire enregistré le 12 novembre 2021, Mme I... B... demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de son pourvoi dirigé contre l'ordonnance n° 211370 du 15 octobre 2021 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande, présentée sur le fondement de l'article L. 521-1 du code de justice administrative, de suspension de l'exécution de la décision du 15 septembre 2021 par laquelle le directeur de l'établissement public de santé Erasme l'a suspendue de ses fonctions à compter du 15 septembre et jusqu'à la production d'un justificatif de vaccination ou de contre-indication à la vaccination, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 12 et 14 de la loi du 5 août 2021 relative à la gestion de la crise sanitaire.<br/>
<br/>
              Par un mémoire, enregistré le 6 décembre 2021, l'établissement public de santé Erasme conclut qu'il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité au Conseil constitutionnel. Il soutient que la question n'est ni nouvelle ni sérieuse.<br/>
<br/>
              Par un mémoire, enregistré le 11 janvier 2022, le ministre des solidarités et de la santé conclut qu'il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité au Conseil constitutionnel. Il soutient que la question n'est ni nouvelle ni sérieuse.<br/>
<br/>
              Le mémoire a été communiqué au Premier ministre, qui n'a pas produit de mémoire.<br/>
<br/>
<br/>
              Vu Les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021, notamment ses articles 12 et 14 ;<br/>
              - la décision du Conseil constitutionnel n°2015-458 QPC du 20 mars 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ségolène Cavaliere, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - Les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après Les conclusions, à la SCP Richard, avocat de Mme B... et au cabinet Rousseau et Tapie, avocat de l'établissement public de santé Erasme.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans Les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. L'article 12 de la loi du 5 août 2021 relative à la crise sanitaire instaure une obligation de vaccination dans Les termes suivants : " I. - Doivent être vaccinés, sauf contre-indication médicale reconnue, contre la covid-19 : / 1° Les personnes exerçant leur activité dans : / a) Les établissements de santé mentionnés à l'article L. 6111-1 du code de la santé publique ainsi que Les hôpitaux des armées mentionnés à l'article L. 6147-7 du même code ; / b) Les centres de santé mentionnés à l'article L. 6323-1 dudit code ; / c) Les maisons de santé mentionnées à l'article L. 6323-3 du même code ; / d) Les centres et équipes mobiles de soins mentionnés à l'article L. 6325-1 du même code ; / e) Les centres médicaux et équipes de soins mobiles du service de santé des armées mentionnés à l'article L. 6326-1 du même code ; / f) Les dispositifs d'appui à la coordination des parcours de santé complexes mentionnés aux II et III de l'article 23 de la loi n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé ; / g) Les centres de lutte contre la tuberculose mentionnés à l'article L. 3112-2 du code de la santé publique ; / h) Les centres gratuits d'information, de dépistage et de diagnostic mentionnés à l'article L. 3121-2 du même code ; / i) Les services de médecine préventive et de promotion de la santé mentionnés à l'article L. 831-1 du code de l'éducation ; / j) Les services de prévention et de santé au travail mentionnés à l'article-L.-4622-1 du code du travail et Les services de prévention et de santé au travail interentreprises définis à l'article L. 4622-7 du même code ; / k) Les établissements et services sociaux et médico-sociaux mentionnés aux 2°, 3°, 5°, 6°, 7°, 9° et 12° du I de l'article L. 312-1 du code de l'action sociale et des familles, à l'exception des travailleurs handicapés accompagnés dans le cadre d'un contrat de soutien et d'aide par le travail mentionné au dernier alinéa de l'article L. 311-4 du même code ; / l) Les établissements mentionnés à l'article L. 633-1 du code de la construction et de l'habitation, qui ne relèvent pas des établissements sociaux et médico-sociaux mentionnés aux 6° et 7° du I de l'article L. 312-1 du code de l'action sociale et des familles, destinés à l'accueil des personnes âgées ou handicapées ; / m) Les résidences-services destinées à l'accueil des personnes âgées ou handicapées mentionnées à l'article L. 631-13 du code de la construction et de l'habitation ; / n) Les habitats inclusifs mentionnés à l'article L. 281-1 du code de l'action sociale et des familles ; / 2° Les professionnels de santé mentionnés à la quatrième partie du code de la santé publique, lorsqu'ils ne relèvent pas du 1° du présent I ; / 3° Les personnes, lorsqu'elles ne relèvent pas des 1° ou 2° du présent I, faisant usage : / a) Du titre de psychologue mentionné à l'article 44 de la loi n° 85-772 du 25 juillet 1985 portant diverses dispositions d'ordre social ; / b) Du titre d'ostéopathe ou de chiropracteur mentionné à l'article 75 de la loi n° 2002-303 du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé ; / c) Du titre de psychothérapeute mentionné à l'article 52 de la loi n° 2004-806 du 9 août 2004 relative à la politique de santé publique ; / 4° Les étudiants ou élèves des établissements préparant à l'exercice des professions mentionnées aux 2° et 3° du présent I ainsi que Les personnes travaillant dans Les mêmes locaux que Les professionnels mentionnés au 2° ou que Les personnes mentionnées au 3° ; / 5° Les professionnels employés par un particulier employeur mentionné à l'article L. 7221-1 du code du travail, effectuant des interventions au domicile des personnes attributaires des allocations définies aux articles L. 232-1 et L. 245-1 du code de l'action sociale et des familles ; / 6° Les sapeurs-pompiers et Les marins-pompiers des services d'incendie et de secours, Les pilotes et personnels navigants de la sécurité civile assurant la prise en charge de victimes, Les militaires des unités investies à titre permanent de missions de sécurité civile mentionnés au premier alinéa de l'article L. 721-2 du code de la sécurité intérieure ainsi que Les membres des associations agréées de sécurité civile mentionnées à l'article L. 725-3 du même code participant, à la demande de l'autorité de police compétente ou lors du déclenchement du plan Orsec, aux opérations de secours et à l'encadrement des bénévoles dans le cadre des actions de soutien aux populations ou qui contribuent à la mise en place des dispositifs de sécurité civile dans le cadre de rassemblements de personnes ; / 7° Les personnes exerçant l'activité de transport sanitaire mentionnée à l'article L. 6312-1 du code de la santé publique ainsi que celles assurant Les transports pris en charge sur prescription médicale mentionnés à l'article L. 322-5 du code de la sécurité sociale ; / 8° Les prestataires de services et Les distributeurs de matériels mentionnés à l'article L. 5232-3 du code de la santé publique. /  II. - Un décret, pris après avis de la Haute Autorité de santé, détermine Les conditions de vaccination contre la covid-19 des personnes mentionnées au I du présent article. Il précise Les différents schémas vaccinaux et, pour chacun d'entre eux, le nombre de doses requises. / Ce décret fixe Les éléments permettant d'établir un certificat de statut vaccinal pour Les personnes mentionnées au même I et Les modalités de présentation de ce certificat sous une forme ne permettant d'identifier que la nature de celui-ci et la satisfaction aux critères requis. Il détermine également Les éléments permettant d'établir le résultat d'un examen de dépistage virologique ne concluant pas à une contamination par la covid-19 et le certificat de rétablissement à la suite d'une contamination par la covid-19. / III. - Le I ne s'applique pas aux personnes chargées de l'exécution d'une tâche ponctuelle au sein des locaux dans lesquels Les personnes mentionnées aux 1°, 2°, 3° et 4° du même I exercent ou travaillent. /IV. - Un décret, pris après avis de la Haute Autorité de santé, peut, compte tenu de l'évolution de la situation épidémiologique et des connaissances médicales et scientifiques, suspendre, pour tout ou partie des catégories de personnes mentionnées au I, l'obligation prévue au même I ".<br/>
<br/>
              3. Aux termes de l'article 14 de cette même loi : " I. - A compter du lendemain de la publication de la présente loi et jusqu'au 14 septembre 2021 inclus, Les personnes mentionnées au I de l'article 12 ne peuvent plus exercer leur activité si elles n'ont pas présenté Les documents mentionnés au I de l'article 13 ou, à défaut, le justificatif de l'administration des doses de vaccins requises par le décret mentionné au II de l'article 12 ou le résultat, pour sa durée de validité, de l'examen de dépistage virologique ne concluant pas à une contamination par la covid-19 prévu par le même décret B. - A compter du 15 septembre 2021, Les personnes mentionnées au I de l'article 12 ne peuvent plus exercer leur activité si elles n'ont pas présenté Les documents mentionnés au I de l'article 13 ou, à défaut, le justificatif de l'administration des doses de vaccins requises par le décret mentionné au II de l'article 12. / Par dérogation au premier alinéa du présent B, à compter du 15 septembre 2021 et jusqu'au 15 octobre 2021 inclus, sont autorisées à exercer leur activité Les personnes mentionnées au I de l'article 12 qui, dans le cadre d'un schéma vaccinal comprenant plusieurs doses, justifient de l'administration d'au moins une des doses requises par le décret mentionné au II du même article 12, sous réserve de présenter le résultat, pour sa durée de validité, de l'examen de dépistage virologique ne concluant pas à une contamination par la covid-19 prévu par le même décret. / II. - Lorsque l'employeur constate qu'un salarié ne peut plus exercer son activité en application du I du présent article, il l'informe sans délai des conséquences qu'emporte cette interdiction d'exercer sur son emploi ainsi que des moyens de régulariser sa situation. Le salarié qui fait l'objet d'une interdiction d'exercer peut utiliser, avec l'accord de son employeur, des jours de repos conventionnels ou des jours de congés payés. A défaut, son contrat de travail est suspendu. La suspension mentionnée au premier alinéa du présent II, qui s'accompagne de l'interruption du versement de la rémunération, prend fin dès que le salarié remplit Les conditions nécessaires à l'exercice de son activité prévues au I. Elle ne peut être assimilée à une période de travail effectif pour la détermination de la durée des congés payés ainsi que pour Les droits légaux ou conventionnels acquis par le salarié au titre de son ancienneté. Pendant cette suspension, le salarié conserve le bénéfice des garanties de protection sociale complémentaire auxquelles il a souscrit. / La dernière phrase du deuxième alinéa du présent II est d'ordre public. / Lorsque le contrat à durée déterminée d'un salarié est suspendu en application du premier alinéa du présent II, le contrat prend fin au terme prévu si ce dernier intervient au cours de la période de suspension. / III. - Lorsque l'employeur constate qu'un agent public ne peut plus exercer son activité en application du I, il l'informe sans délai des conséquences qu'emporte cette interdiction d'exercer sur son emploi ainsi que des moyens de régulariser sa situation. L'agent public qui fait l'objet d'une interdiction d'exercer peut utiliser, avec l'accord de son employeur, des jours de congés payés. A défaut, il est suspendu de ses fonctions ou de son contrat de travail. / La suspension mentionnée au premier alinéa du présent III, qui s'accompagne de l'interruption du versement de la rémunération, prend fin dès que l'agent public remplit Les conditions nécessaires à l'exercice de son activité prévues au I. Elle ne peut être assimilée à une période de travail effectif pour la détermination de la durée des congés payés ainsi que pour Les droits acquis par l'agent public au titre de son ancienneté. Pendant cette suspension, l'agent public conserve le bénéfice des garanties de protection sociale complémentaire auxquelles il a souscrit. / La dernière phrase du deuxième alinéa du présent III est d'ordre public. / Lorsque le contrat à durée déterminée d'un agent public non titulaire est suspendu en application du premier alinéa du présent III, le contrat prend fin au terme prévu si ce dernier intervient au cours de la période de suspension V. - Les agences régionales de santé vérifient que Les personnes mentionnées aux 2° et 3° du I de l'article 12 qui ne leur ont pas adressé Les documents mentionnés au I de l'article 13 ne méconnaissent pas l'interdiction d'exercer leur activité prévue au I du présent article. / V. - Lorsque l'employeur ou l'agence régionale de santé constate qu'un professionnel de santé ne peut plus exercer son activité en application du présent article depuis plus de trente jours, il en informe, le cas échéant, le conseil national de l'ordre dont il relève ".<br/>
<br/>
              4. A l'appui de son pourvoi dirigé contre l'ordonnance n° 211370 du 15 octobre 2021 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande, présentée sur le fondement de l'article L. 521-1 du code de justice administrative, de suspension de l'exécution de la décision du 15 septembre 2021 par laquelle le directeur de l'établissement public de santé Erasme l'a suspendue de ses fonctions à compter du 15 septembre et jusqu'à la production d'un justificatif de vaccination ou de contre-indication à la vaccination,  Mme B... demande au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 12 et 14 de la loi du 5 août 2021 relative à la gestion de la crise sanitaire. Les dispositions de l'article 12, en tant qu'elles visent, au a) du 1° de son I, Les établissements de santé mentionnés à l'article L. 6111-1 du code de la santé publique, ainsi que Les dispositions du B du I, du III, du IV et du V de l'article 14, sont applicables au litige, au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 cité ci-dessus.<br/>
<br/>
              Sur l'article 12 :<br/>
<br/>
              En ce qui concerne le droit à la protection de la santé, l'inviolabilité du corps humain et le droit à la dignité de la personne humaine :<br/>
<br/>
              5. Ainsi que l'a jugé le Conseil constitutionnel dans sa décision n°2015-458 QPC du 20 mars 2015, il est loisible au législateur de définir une politique de vaccination afin de protéger la santé individuelle et collective, ainsi que de modifier Les dispositions relatives à cette politique de vaccination pour tenir compte de l'évolution des données scientifiques, médicales et épidémiologiques. Le droit à la protection de la santé garanti par le Préambule de la Constitution de 1946 n'impose pas de rechercher si l'objectif de protection de la santé que s'est assigné le législateur aurait pu être atteint par d'autres voies, dès lors que Les modalités retenues par la loi ne sont pas manifestement inappropriées à l'objectif visé. <br/>
<br/>
              6. En adoptant, pour l'ensemble des personnes exerçant leur activité dans Les établissements de santé mentionnés à l'article L. 6111-1 du code de la santé publique, à l'exception de celles y effectuant une tâche ponctuelle, le principe d'une obligation vaccinale à compter du 15 septembre 2021, le législateur a entendu, dans un contexte de progression rapide de l'épidémie de Covid-19 accompagné de l'émergence de nouveaux variants et compte tenu d'un niveau encore incomplet de la couverture vaccinale de certains professionnels de santé, garantir le bon fonctionnement des services hospitaliers publics grâce à la protection offerte par Les vaccins disponibles et protéger, par l'effet de la moindre transmission du virus par Les personnes vaccinées, la santé des malades qui y étaient hospitalisés.<br/>
<br/>
              7. Cette obligation vaccinale ne s'impose pas, en vertu de l'article 13 de la même loi du 5 août 2021, aux personnes qui présentent un certificat médical de contre-indication ainsi que, pendant la durée de sa validité, aux personnes disposant d'un certificat de rétablissement. Par ailleurs l'article contesté donne compétence, en son IV, au pouvoir réglementaire, compte tenu de l'évolution de la situation épidémiologique et des connaissances médicales et scientifiques et après avis de la Haute autorité de santé, pour suspendre cette obligation pour tout ou partie des catégories de personnes qu'elle concerne. Enfin, il ressort des pièces du dossier que la vaccination contre la Covid-19, dont l'efficacité au regard des deux objectifs rappelés au point 6. est établie en l'état des connaissances scientifiques, n'est susceptible de provoquer, sauf dans des cas très rares, que des effets indésirables mineurs et temporaires. Dans ces conditions, la requérante n'est pas fondée à soutenir que Les dispositions qu'elle conteste, qui sont justifiées par une exigence de santé publique et ne sont pas manifestement inappropriées à l'objectif qu'elles poursuivent, portent atteinte à l'exigence constitutionnelle de protection de la santé garantie par le Préambule de la Constitution de 1946, à l'inviolabilité du corps humain et au principe constitutionnel de respect de la dignité de la personne humaine.<br/>
<br/>
              En ce qui concerne la méconnaissance du principe d'égalité :<br/>
<br/>
              8. D'une part, Les dispositions contestées s'appliquant de manière identique à l'ensemble des personnes qui exercent leur activité professionnelle au sein des établissements de santé, qu'elles fassent ou non partie du personnel soignant, la requérante ne saurait utilement soutenir qu'elles méconnaissent, pour ce motif, le principe d'égalité garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789. Au surplus, compte tenu de la profession d'orthophoniste exercée par Mme B..., Les dispositions attaquées ne sont, en tant qu'elles s'appliquent à des personnels hospitaliers non soignants, pas applicables au présent litige.<br/>
<br/>
              9. D'autre part, la circonstance que Les dispositions contestées font peser sur Les personnes exerçant une activité au sein des établissements de santé une obligation vaccinale qui n'est pas imposée, notamment, aux personnels des établissements scolaires et universitaires, constitue, compte tenu des missions des établissements de santé et de la vulnérabilité des patients qui y sont admis, une différence de traitement en rapport avec cette différence de situation, qui n'est pas manifestement disproportionnée au regard de l'objectif poursuivi.<br/>
<br/>
              Sur l'article 14 :<br/>
<br/>
              10. Aux termes du cinquième alinéa du Préambule de 1946 : " Chacun a le devoir de travailler et le droit d'obtenir un emploi (...) ".<br/>
<br/>
              11. Les dispositions contestées ne portent par elles-mêmes aucune atteinte au droit à l'emploi, notamment pour des personnes qui n'étaient pas encore employées dans un établissement public de santé à la date d'entrée en vigueur de la loi. S'agissant des personnes qui y étaient employées à cette date et qui refusent de se soumettre, en dehors des motifs prévus par la loi, à l'obligation vaccinale, elles prévoient non pas la rupture de leur contrat de travail ou la cessation de leurs fonctions, mais la suspension du contrat de travail ou des fonctions exercées jusqu'à ce que l'agent produise Les justificatifs requis. <br/>
<br/>
              12. Dans ces conditions, contrairement à ce que soutient la requérante, Les dispositions attaquées ont opéré une conciliation qui n'est pas manifestement déséquilibrée entre Les exigences constitutionnelles qui découlent du droit à l'emploi et du droit à la protection de la santé, rappelé ci-dessus.<br/>
<br/>
              13. Il résulte de ce tout ce qui précède que la question prioritaire de constitutionnalité, qui n'est pas nouvelle, ne présente pas de caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Mme B.... <br/>
Article 2 : La présente décision sera notifiée à Mme I... B.... <br/>
Copie en sera adressée à l'établissement public de santé Erasme, au Premier ministre, au ministre des solidarités et de la santé et au Conseil constitutionnel. <br/>
              Délibéré à l'issue de la séance du 17 janvier 2022 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; M. C... H..., M. Fabien Raynaud, présidents de chambre ; M. M... D..., Mme G... L..., M. F... K..., M. Cyril Roger-Lacan, conseillers d'Etat ; Mme Flavie Le Tallec, maître des requêtes en service extraordinaire et Mme Ségolène Cavaliere, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 28 janvier 2022.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Ségolène Cavaliere<br/>
                 Le secrétaire :<br/>
                 Signé : M. E... J...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
