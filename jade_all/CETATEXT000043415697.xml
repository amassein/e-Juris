<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043415697</ID>
<ANCIEN_ID>JG_L_2021_04_000000445595</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/41/56/CETATEXT000043415697.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/04/2021, 445595, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445595</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445595.20210422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... a demandé au tribunal administratif de Dijon d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 en vue de l'élection des conseillers municipaux de la commune de Vézannes (Yonne). <br/>
<br/>
              Par un jugement n°s 2000818, 2000965 du 22 septembre 2020, le tribunal administratif de Dijon, après avoir joint cette protestation et le déféré du préfet de l'Yonne tendant à la proclamation de l'élection d'une candidate à l'issue du premier tour, a prononcé un non-lieu à statuer sur le déféré du préfet puis rejeté la protestation de M. A....<br/>
<br/>
              Par une requête enregistrée le 23 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il statue sur sa protestation ;<br/>
<br/>
              2°) de faire droit à sa protestation ;<br/>
<br/>
              3°) de mettre à la charge de M. D... la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du premier tour du scrutin des élections municipales qui s'est déroulé le 15 mars 2020 dans la commune de Vézannes (Yonne), commune de moins de 1 000 habitants, M. D..., maire sortant, et les six autres candidats qui avaient présenté leur candidature de façon groupée ont chacun obtenu au moins 20 voix, soit un nombre supérieur à la majorité absolue des suffrages exprimés et au quart des électeurs inscrits. M. A..., qui a exercé les fonctions de maire de la commune entre 2008 et 2014, ainsi que M. E... avec lequel il présentait sa candidature, n'ont rassemblé respectivement que 13 et 16 voix. A la suite d'une omission, seuls six des candidats arrivés en tête ont été proclamés élus à l'issue du premier tour, la candidate arrivée septième ayant néanmoins, à l'issue du second tour, été élue au titre du dernier siège à pourvoir. M. A... relève appel du jugement du 22 septembre 2020 par lequel le tribunal administratif de Dijon, après avoir prononcé un non-lieu sur le déféré du préfet de l'Yonne tendant à ce que soit proclamée élue la candidate arrivée en septième position à l'issue du premier tour, a rejeté la protestation de M. A... tendant à l'annulation des opérations électorales.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite (...) ".<br/>
<br/>
              3. Si M. A... soutient que la publication, dans le journal " L'Yonne Républicaine ", d'un article en date du 8 février 2020 faisant état de la candidature du maire sortant serait constitutive d'une publicité commerciale en méconnaissance des dispositions de l'article L. 52-1 du code électoral, ce grief, qui n'est pas d'ordre public, est nouveau en appel et, par suite, irrecevable. En tout état de cause, un tel article ne peut être regardé comme une publicité commerciale au sens de ces dispositions. <br/>
<br/>
              4. En second lieu, aux termes de l'article L. 48-2 du code électoral : " Il est interdit à tout candidat de porter à la connaissance du public un élément nouveau de polémique électorale à un moment tel que ses adversaires n'aient pas la possibilité d'y répondre utilement avant la fin de la campagne électorale. "<br/>
<br/>
              5. Il résulte de l'instruction qu'un tract de la liste " Pour Vézannes " conduite par M. D..., distribué le samedi 7 mars 2020 et présentant sur deux pages les " principaux points de réalisation du mandat ", indique que " les Finances ont été assainies alors que nous partions d'une situation catastrophique à la limite de la mise sous tutelle ". Si cette mention, qui n'excédait pas les limites de la polémique électorale, pouvait être regardée comme un élément nouveau de celle-ci, M. A... a pu y répondre utilement en diffusant, dès le lendemain, un tract défendant le bilan de son propre mandat et contestant sa responsabilité dans la situation financière actuelle de la commune. Par suite, la diffusion de ce tract ne peut être regardée comme constitutive d'une manoeuvre de nature à altérer la sincérité du scrutin.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. A... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Dijon a rejeté sa protestation.<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. D... et les autres défendeurs au titre des dispositions de l'article L. 761 1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. D..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : Les conclusions présentées par M. D..., premier défenseur dénommé, au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. C... A..., à M. B... D..., premier défenseur dénommé et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
