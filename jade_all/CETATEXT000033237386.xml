<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033237386</ID>
<ANCIEN_ID>JG_L_2016_10_000000391092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/23/73/CETATEXT000033237386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 12/10/2016, 391092</TITRE>
<DATE_DEC>2016-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Marie-Françoise Guilhemsans</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:391092.20161012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              L'association Saint-Priest Environnement, M. F...A..., Mme E...D...et Mme B...C...ont demandé au tribunal administratif de Limoges d'annuler pour excès de pouvoir l'arrêté du 1er juillet 2011, par lequel le préfet de la Creuse a accordé à la société WPD Energie 21 Limousin un permis de construire trois éoliennes sur le territoire de la commune du Chauchet et un permis de construire deux éoliennes sur le territoire de la commune de Saint-Priest, ainsi que la décision du 21 septembre 2011 rejetant leur recours gracieux contre cet arrêté. Par un jugement n° 1101832 du 3 octobre 2013, le tribunal administratif de Limoges a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 13BX03243 du 16 avril 2015, la cour administrative d'appel de Bordeaux, sur appel de l'association Saint-Priest Environnement et de M F...A..., a annulé ce jugement ainsi que l'arrêté du 1er juillet 2011. <br/>
<br/>
              1° Sous le n° 391092, par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'État les 16 juin et 15 septembre 2015, la société WPD Energie 21 Limousin demande au Conseil d'État :<br/>
<br/>
              - d'annuler cet arrêt ;<br/>
<br/>
              - de mettre à la charge de l'association Saint-Priest Environnement et autres la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 391155, par un pourvoi sommaire et un mémoire complémentaire enregistrés au secrétariat du contentieux du Conseil d'Etat les 18 juin et 18 septembre 2015, le ministre du logement, de l'égalité des territoires et de la ruralité demande au Conseil d'Etat d'annuler le même arrêt de la cour administrative d'appel de Bordeaux. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Françoise Guilhemsans, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la société WPD Energie 21 Limousin et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de l'association Saint-Priest Environnement et de M.A....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2009, présentée par l'association Saint-Priest Environnement et M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois susvisés sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour qu'il y soit statué par une même décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 1er juillet 2011, le préfet de la Creuse a délivré à la société WPD Energie 21 Limousin des permis de construire en vue de l'édification de trois éoliennes sur le territoire de la commune du Chauche et de deux éoliennes sur le territoire de la commune de Saint-Priest ; qu'en revanche, par un autre arrêté du même jour, le préfet a refusé de délivrer à la société les autres permis demandés en vue, notamment, de la construction d'un poste de livraison sur le territoire de la commune de Tardes ; que par un jugement en date du 3 octobre 2013, le tribunal administratif de Limoges a rejeté la demande formée par l'Association Saint-Priest Environnement et autres et tendant à l'annulation des permis de construire en date du 1er juillet 2011 ; que la société WPD Energie 21 Limousin et le ministre du logement, de l'égalité des territoires et de la ruralité se pourvoient en cassation contre l'arrêt en date du 16 avril 2015 par lequel la cour administrative d'appel de Bordeaux a, sur la requête de l'association et de M F...A..., annulé ce jugement et ces permis ; <br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article L. 421-6 du code de l'urbanisme : " Le permis de construire ne peut être accordé que si les travaux projetés sont conformes aux normes de fond résultant des dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords " ; qu'il résulte de ces dispositions qu'une construction constituée de plusieurs éléments formant, en raison des liens physiques ou fonctionnels entre eux, un ensemble immobilier unique, doit en principe faire l'objet d'un seul permis de construire ; <br/>
<br/>
              4. Considérant, d'autre part, que le permis de construire a pour seul objet de s'assurer de la conformité des travaux qu'il autorise avec la législation et la réglementation d'urbanisme ; qu'il suit de là que, lorsque deux constructions sont distinctes, la seule circonstance que l'une ne pourrait fonctionner ou être exploitée sans l'autre, au regard de considérations d'ordre technique ou économique et non au regard des règles d'urbanisme, ne suffit pas à caractériser un ensemble immobilier unique;<br/>
<br/>
              5. Considérant que pour estimer que le préfet ne pouvait autoriser la construction des cinq éoliennes alors qu'il refusait par ailleurs le permis de construire le poste de livraison indispensable à leur fonctionnement, la cour s'est fondée sur la circonstance que si un aérogénérateur et un poste de livraison sont des constructions distinctes, elles ne présentent pas le caractère de constructions divisibles mais sont au contraire fonctionnellement liées entre elles ; qu'il résulte de ce qui a été dit au point précédent qu'en se fondant sur l'existence d'un lien fonctionnel de nature technique et économique entre ces constructions distinctes, au demeurant éloignées, pour en déduire qu'elles constituaient un ensemble immobilier unique devant faire l'objet d'un même permis de construire, la cour a commis une erreur de droit ; que dès lors, l'arrêt attaqué doit être annulé ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association " Saint-Priest Environnement " et de Monsieur A...la somme globale de 3 000 euros à verser à la société WPD énergie 21 Limousin au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce que la somme demandée par l'association Saint-Priest Environnement et M. A...soit mise à la charge de cette société et de l'Etat, qui ne sont pas, dans la présente instance, les parties perdantes ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 16 avril 2015 de la cour administrative d'appel de Bordeaux est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : L'association Saint-Priest Environnement et M. A...verseront à la société WPD Energie 21 Limousin une somme globale de 3 000 euros au titre des dispositions de l'article L.761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions de l'association Saint-Priest Environnement et de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la Société WPD Energie 21 Limousin, à la ministre du logement et de l'habitat durable, à l'association Saint-Priest Environnement et à M. F...  A.... <br/>
      Copie sera adressée à Mme B...C...et à Mme E...D.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03 Urbanisme et aménagement du territoire. Permis de construire.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
