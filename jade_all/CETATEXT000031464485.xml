<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464485</ID>
<ANCIEN_ID>JG_L_2015_11_000000388922</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464485.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 09/11/2015, 388922, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388922</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388922.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association " Assistance familiale " a demandé au tribunal administratif de Marseille d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de la décision du 4 février 2015 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, retirant la décision implicite de rejet du recours hiérarchique formé à l'encontre de la décision du 10 juin 2014 de l'inspecteur du travail de la 14ème section de l'Unité territoriale des Bouches-du-Rhône ayant autorisé l'association " Assistance familiale " à licencier Mme A...B..., a annulé cette décision et refusé ce licenciement. Par une ordonnance n° 1501350 du 5 mars 2015, le juge des référés de ce tribunal a ordonné la suspension de la décision du 4 février 2015.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 mars, 7 avril et 17 août 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter les conclusions aux fins de suspension de la décision du 4 février 2015 ;<br/>
<br/>
              3°) de mettre à la charge de l'association " Assistance familiale " la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme B...et à la SCP Spinosi, Sureau, avocat de l'association " Assistance familiale " ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2.  Considérant, d'une part, que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              3. Considérant que le premier aliéna de l'article L. 2422-1 du code du travail prévoit qu'en cas d'annulation de la décision de l'inspecteur du travail autorisant le licenciement d'un salarié protégé mentionné par cet article, " le salarié concerné a le droit, s'il le demande dans un délai de deux mois à compter de la notification de la décision, d'être réintégré dans son emploi ou dans un emploi équivalent " ; qu'aux termes du premier alinéa de l'article L. 2422-4 du même code : " Lorsque l'annulation d'une décision d'autorisation est devenue définitive, le salarié investi d'un des mandats mentionnés à l'article L. 2422-1 a droit au paiement d'une indemnité correspondant à la totalité du préjudice subi au cours de la période écoulée entre son licenciement et sa réintégration, s'il en a formulé la demande dans le délai de deux mois à compter de la notification de la décision " ;<br/>
<br/>
              4. Considérant que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Marseille a jugé, sans entacher sa décision d'insuffisance de motivation et en se fondant sur des éléments dont il ressort des pièces du dossier qui lui était soumis que Mme B... en avait eu connaissance, que l'exécution de la décision contestée du 4 février 2015 par laquelle le ministre chargé du travail a refusé son licenciement signifiait sa réintégration immédiate par l'association " Assistance familiale " qui l'avait licenciée et pouvait ainsi, par ses incidences financières, préjudicier de manière grave et immédiate aux intérêts de cette association ; que contrairement à ce que soutient Mme B..., le juge des référés ne s'est pas fondé sur la circonstance que l'association était, en cas de réintégration, tenue de lui verser l'indemnité prévue par l'article L. 2422-4 du code du travail ; que c'est par suite sans erreur de droit et sans dénaturer les pièces du dossier qui lui était soumis que le juge des référés a jugé que la condition d'urgence à laquelle est subordonné le prononcé de la mesure de suspension était remplie ;<br/>
<br/>
              5.  Considérant, d'autre part, que le juge des référés du tribunal administratif de Marseille a relevé, par une appréciation souveraine des pièces du dossier qui lui était soumis, et sans entacher sa décision de dénaturation, qu'à plusieurs reprises, Mme B...a proféré des menaces et tenu des propos particulièrement violents à l'égard notamment du directeur de l'association ; qu'il a pu, par suite, sans erreur de droit, juger que le moyen tiré de ce que le ministre chargé du travail avait commis une erreur d'appréciation en estimant que le comportement de Mme B...n'était pas constitutif d'une faute justifiant son licenciement, était de nature à créer un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              6.  Considérant qu'il résulte de tout ce qui précède que Mme B... n'est pas fondée à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Marseille ; que son pourvoi doit être rejeté ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association " Assistance familiale " qui n'est pas la partie perdante dans la présente instance ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de Mme B...à ce même  titre ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : Les conclusions présentées par l'association " Assistance familiale " au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à l'association " Assistance familiale " et à Mme A...B....<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
