<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815803</ID>
<ANCIEN_ID>JG_L_2019_07_000000418247</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815803.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 24/07/2019, 418247, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418247</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418247.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) E-Motors a demandé au tribunal administratif de Châlons-en-Champagne de prononcer la décharge des cotisations de taxe sur les surfaces commerciales qu'elle a acquittées au titre des années 2008 à 2011 au titre de son établissement situé à Lavau (Aube). Par un jugement n° 1201962 du 24 mai 2016, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt nos 16NC01605, 16NC01631 du 1er février 2018, enregistré le 15 février 2018 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Nancy a, d'une part, transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 25 juillet 2016 au greffe de cette cour, présenté par la société E-Motors contre ce jugement en tant qu'il concerne l'année 2011 et, d'autre part, rejeté l'appel formé par la société contre ce jugement en tant qu'il concerne l'année 2010.<br/>
<br/>
              Par ce pourvoi et un mémoire complémentaire, enregistré le 27 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la société E-Motors demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 24 mai 2016 en tant qu'il se prononce sur l'imposition due au titre de l'année 2011 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ortscheidt, avocat de la société E-Motors ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société E-Motors qui exerce une activité de vente de véhicules neufs et d'occasion, de réparation automobile et de vente de pièces détachées au sein d'un établissement situé à Lavau (Aube), a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration fiscale l'a assujettie à des rappels de taxe sur les surfaces commerciales au titre des années 2010 et 2011. Elle se pourvoit en cassation contre le jugement du 26 mai 2016 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa demande en tant qu'elle tendait à la décharge des impositions dues au titre de l'année 2011.<br/>
<br/>
              2. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable à l'année d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse 400 mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe, et celle visée à l'article L. 720-5 du code de commerce, s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement, et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente (...)  / Si ces établissements, à l'exception de ceux dont l'activité principale est la vente ou la réparation de véhicules automobiles, ont également une activité de vente au détail de carburants, l'assiette de la taxe comprend en outre une surface calculée forfaitairement en fonction du nombre de position de ravitaillement dans la limite de 70 mètres carrés par position de ravitaillement. Le décret prévu à l'article 20 fixe la surface forfaitaire par emplacement à un montant compris entre 35 et 70 mètres carrés. / Pour les établissements dont le chiffre d'affaires au mètre carré est inférieur à 3 000 euros, le taux de cette taxe est de 5,74 euros au mètre carré de surface définie au troisième alinéa. Pour les établissements dont le chiffre d'affaires au mètre carré est supérieur à 12 000 euros, le taux est fixé à 34, 12 euros. / A l'exclusion des établissements qui ont pour activité principale la vente ou la réparation de véhicules automobiles, les taux mentionnés à l'alinéa précédent sont respectivement portés à 8,32 euros ou 35,70 euros (...). Un décret prévoira, par rapport aux taux ci-dessus, des réductions pour les professions dont l'exercice requiert des superficies de vente anormalement élevées ou, en fonction de leur chiffre d'affaires au mètre carré, pour les établissements dont la surface des locaux de vente destinés à la vente au détail est comprise entre 400 et 600 mètres carrés ". Aux termes de l'article 3 du décret du 26 janvier 1995 : " A. - La réduction de taux prévue au troisième alinéa de l'article 3 de la loi du 13 juillet 1972 susvisée en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées est fixée à 30 p. 100 en ce qui concerne la vente exclusive des marchandises énumérées ci-après (...) - véhicules automobiles ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond et notamment d'un courrier en date du 17 mars 2010 émis par la société d'assurance de la société E-Motors, qu'en raison de malfaçons ayant entaché la dalle en ciment du premier étage du bâtiment dans lequel celle-ci exerçait son activité, l'accès à cet étage était interdit au public et que cette interdiction a perduré au moins jusqu'en 2014, ainsi qu'en atteste le procès-verbal de constat d'huissier établi le 26 mars 2014 à la demande de la société à laquelle les malfaçons étaient reprochées. Il ressort également des pièces du dossier et notamment de ce même procès-verbal, que la partie du premier étage initialement destinée à l'exposition de véhicules proposés à la vente a été affectée, comme l'autre partie, à l'entreposage de véhicules en attente de livraison. Dans ces conditions, en jugeant que, pour l'année en litige, les surfaces de cet étage devaient être qualifiées de surfaces de vente au sens de l'article 3 de la loi du 12 juillet 1972 cité au point 2 et devaient être prises en compte dans l'assiette de la taxe, au motif que ces véhicules étaient exposés dans une perspective de promotion des ventes, dès lors que ce premier étage avait été aménagé pour que les véhicules puissent être vus du hall d'exposition situé au rez-de-chaussée, alors que ces surfaces, inaccessibles au public pour des raisons de sécurité, ne pouvaient être regardées comme des espaces affectés à l'exposition des marchandises proposées à la vente, le tribunal administratif de Châlons-en-Champagne a inexactement qualifié les faits qui lui étaient soumis et commis une erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société est fondée à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              5. Il résulte de ce qui a été dit au point 3 ci-dessus que la surface du premier étage du bâtiment dans lequel la société E-Motors exerce son activité n'est pas à prendre en compte pour la détermination de l'assiette de la taxe sur les surfaces commerciales au titre de l'année 2011. Dès lors qu'il résulte de l'instruction que la surface d'exposition-vente du rez-de-chaussée s'élève à 352,80 m2, soit une surface inférieure au seuil d'assujettissement de la taxe fixé au premier alinéa de l'article 3 de la loi du 13 juillet 1972 cité au point 2, la société est fondée à demander la décharge de la taxe à laquelle elle a été assujettie au titre de l'année 2011 et des pénalités correspondantes. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, pour l'ensemble de la procédure, la somme de 4 000 euros à verser à la société E-Motors, au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 24 mai 2016 du tribunal administratif de Châlons-en-Champagne est annulé.<br/>
<br/>
Article 2 : La société E-Motors est déchargée de la taxe sur les surfaces commerciales à laquelle elle a été assujettie au titre de l'année 2011 et des pénalités correspondantes. <br/>
<br/>
Article 3 : L'Etat versera à la société E-Motors la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société E-Motors et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
