<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032408999</ID>
<ANCIEN_ID>JG_L_2016_04_000000387468</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/89/CETATEXT000032408999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 15/04/2016, 387468, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387468</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387468.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association démocratie et transparence à l'université de Lyon a demandé au tribunal administratif de Lyon d'annuler les élections qui se sont tenues en mai et juin 2013 pour la désignation des représentants des collèges nos 4, 5 et 6 au conseil d'administration de l'établissement public de coopération scientifique " Université de Lyon " ainsi que les arrêtés des 5 février et 14 mai 2013 organisant ces élections. Par un jugement n° 1304482 du 8 octobre 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13LY03260 du 27 novembre 2014, la cour administrative d'appel de Lyon a rejeté l'appel formé par l'association démocratie et transparence à l'université de Lyon contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et trois mémoires en réplique, enregistrés les 27 janvier, 27 avril et 22 octobre 2015 et les 14 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, l'association démocratie et transparence à l'université de Lyon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'université de Lyon la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code de la recherche ;<br/>
              - la loi n° 2013-660 du 22 juillet 2013 ; <br/>
              - le décret n° 2007-386 du 21 mars 2007 ;<br/>
              - le décret n° 2015-127 du 5 février 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'association démocratie et transparence à l'université de Lyon et à la SCP Matuchansky, Vexliard, Poupot, avocat de la communauté d'universités et établissements " Université de Lyon " ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le requérant a demandé au tribunal administratif de Lyon l'annulation des élections des représentants des enseignants-chercheurs, des autres personnels et des étudiants au conseil d'administration de l'établissement public de coopération scientifique " Université de Lyon " qui se sont déroulées entre le 13 mai et le 11 juin 2013, ainsi que des arrêtés des 5 février et 14 mai 2013 qui en fixaient l'organisation ; que, par un jugement du 8 octobre 2013, le tribunal a rejeté leur demande ; qu'ils se pourvoient en cassation contre l'arrêt du 27 novembre 2014 par lequel la cour administrative d'appel de Lyon a rejeté leur requête tendant à l'annulation de ce jugement ;<br/>
<br/>
              2. Considérant que, postérieurement à l'introduction du pourvoi, le décret du 5 février 2015 portant approbation des statuts de la communauté d'universités et établissements " Université de Lyon ", pris sur le fondement des dispositions de la loi du 22 juillet 2013 relative à l'enseignement supérieur et à la recherche, a abrogé le décret du 21 mars 2007 portant création de l'établissement public de coopération scientifique " Université de Lyon " ; que cette abrogation est devenue définitive ; qu'il a d'ailleurs été procédé, en juin 2015, à l'élection générale des représentants des enseignants-chercheurs, des autres personnels et des étudiants au conseil d'administration de la nouvelle communauté d'universités et établissements ; qu'ainsi, le pourvoi de l'association démocratie et transparence à l'université de Lyon a perdu son objet en cours d'instance ; qu'il n'y a, dès lors, plus lieu d'y statuer ; <br/>
<br/>
              3. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions des parties présentées sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      --------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de l'association démocratie et transparence à l'université de Lyon tendant à l'annulation de l'arrêt du 27 novembre 2014 de la cour administrative d'appel de Lyon.<br/>
Article 2 : Les conclusions de l'association démocratie et transparence à l'université de Lyon et de la communauté d'universités et établissements "Université de Lyon " présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'association démocratie et transparence à l'université de Lyon et à la communauté d'universités et établissements " Université de Lyon ".<br/>
Copie en sera adressée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
