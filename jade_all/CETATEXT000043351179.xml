<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043351179</ID>
<ANCIEN_ID>JG_L_2021_04_000000432993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/11/CETATEXT000043351179.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 07/04/2021, 432993, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Agnès Pic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:432993.20210407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A... B... et la Mutuelle des architectes français ont demandé au tribunal administratif de Montpellier de condamner la commune d'Estavar à leur verser la somme de 76 777,25 euros, assortie des intérêts au taux légal à compter du 30 juin 1999, correspondant aux  sommes que M. B... a été condamné à verser à la société civile immobilière Estavar 1200 par le tribunal de grande instance de Perpignan en réparation du préjudice résultant de l'illégalité du permis de construire délivré à cette société le 15 avril 1991, ainsi que du plan d'occupation des sols de la commune approuvé le 7 mars 1987. Par un jugement n° 1504733 du 26 septembre 2017, le tribunal administratif de Montpellier a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 17MA04424 du 28 mai 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 juillet et 25 octobre 2019 et le 24 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;   <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel en condamnant la commune d'Estavar à lui verser la somme de 71 274,58 euros, assortie des intérêts au taux légal à compter du 30 juin 1999 et de la capitalisation des intérêts ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L.761-1 du code de justice administrative.  <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 mars 2021, présentée par la commune d'Estavar ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Pic, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boulloche, avocat de M. B... et à la SCP Buk-Lament - Robillot, avocat de la commune d'Estavar ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le maire d'Estavar a délivré le 15 avril 1991 à la société civile immobilière Estavar 1 200 le permis de construire un ensemble immobilier de soixante-six logements en cinq tranches, qui a été annulé par un arrêt de la cour administrative d'appel de Marseille du 5 décembre 2002. Par deux arrêts des 21 novembre 2013 et 9 novembre 2017, la cour d'appel de Montpellier a confirmé la condamnation, au titre du manquement à son devoir de conseil, de M. B..., maître d'oeuvre, à payer à la société Estavar 1 200 une somme, ramenée à 71 274,58 euros, correspondant à la fraction des honoraires qu'elle lui a versés et à la quote-part de la taxe locale d'équipement qu'elle a acquittée pour la partie du programme qui n'a pu être réalisée. M. B... et la Mutuelle de architectes français ont demandé au tribunal administratif de Montpellier, qui a rejeté leur demande par un jugement du 26 septembre 2017, de condamner la commune d'Estavar à leur verser une somme équivalente, en mettant en cause la responsabilité de la commune du fait de l'illégalité de son plan d'occupation des sols approuvé le 7 mars 1987 et du permis de construire délivré le 15 avril 1991 à la société Estavar 1200. M. B... se pourvoit en cassation contre l'arrêt du 28 mai 2019 par lequel la cour administrative d'appel de Marseille a rejeté son appel contre ce jugement. <br/>
<br/>
              2. Lorsque l'auteur d'un dommage, condamné, comme en l'espèce, par le juge judiciaire à en indemniser la victime, saisit la juridiction administrative d'un recours en vue de faire supporter la charge de la réparation par la collectivité publique co-auteur de ce dommage, sa demande, quel que soit le fondement de sa responsabilité retenu par le juge judiciaire, n'a pas le caractère d'une action récursoire par laquelle il ferait valoir des droits propres à l'encontre de cette collectivité mais celui d'une action subrogatoire fondée sur les droits de la victime à l'égard de cette collectivité. Ainsi subrogé, il peut utilement se prévaloir des fautes que la collectivité publique aurait commises à son encontre ou à l'égard de la victime et qui ont concouru à la réalisation du dommage, sans toutefois avoir plus de droits que cette victime. En outre, eu égard à l'objet d'une telle action, qui vise à assurer la répartition de la charge de la réparation du dommage entre ses co-auteurs, sa propre faute lui est également opposable. <br/>
<br/>
              3. La cour administrative d'appel de Marseille a jugé qu'à supposer établie l'illégalité du classement, par le plan d'occupation des sols de la commune, du terrain d'assiette du projet autorisé par le permis de construire illégal, le préjudice que M. B... soutenait avoir subi du fait de cette faute et de celle résultant de la délivrance du permis en cause, constitué par sa condamnation à rembourser à la société Estavar 1 200 les honoraires versés et la part de la taxe locale d'équipement acquittée au titre des tranches non réalisées du projet de construction conçu par ses soins, n'avait pu résulter que des stipulations du contrat conclu avec cette société lui confiant une mission complète de maîtrise d'oeuvre et de ses conditions d'exécution et ne pouvait découler directement des illégalités susceptibles d'avoir été commises par la commune. Il résulte de ce qui a été dit au point précédent que, dès lors que M. B... recherchait la responsabilité de la commune en sa qualité de co-auteur du dommage subi par la société Estavar que le juge judiciaire l'avait condamné à réparer, il devait être regardé comme exerçant une action subrogatoire fondée sur les droits de la victime à l'égard de cette collectivité. A ce titre, il pouvait utilement se prévaloir de la faute commise par cette commune en délivrant un permis de construire illégal et, le cas échéant, en adoptant un plan d'occupation des sols illégal, en faisant valoir que cette faute avait également concouru directement à la réalisation du dommage, la propre faute commise par M. B... étant seulement susceptible d'atténuer la responsabilité de la commune. M. B... est dès lors fondé à soutenir qu'en jugeant le contraire, la cour a commis une erreur de droit et inexactement qualifié les faits de l'espèce. <br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. B... est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B..., qui n'est pas la partie perdante, non plus qu'à la charge de l'Etat, qui n'est pas partie à la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 28 mai 2019 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions des parties présentées au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et à la commune d'Estavar.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
