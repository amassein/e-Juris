<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041958758</ID>
<ANCIEN_ID>JG_L_2020_05_000000440815</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/95/87/CETATEXT000041958758.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 28/05/2020, 440815, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440815</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440815.20200528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 24 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du IV de l'article 1er de l'ordonnance n° 2020-313 du 25 mars 2020 relative aux adaptations des règles d'organisation et de fonctionnement des établissements sociaux et médico-sociaux en tant qu'il prévoit le maintien du niveau de facturation des services d'aide et d'accompagnement à domicile (SAAD) intervenant au profit des bénéficiaires de l'allocation personnalisée d'autonomie (APA) sur la base de l'activité prévue sans tenir compte de la sous-activité ou des fermetures temporaires résultant de l'épidémie de covid-19 ;<br/>
<br/>
              2°) d'enjoindre que l'allocation personnalisée d'autonomie soit directement versée à ses bénéficiaires.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - il justifie d'un intérêt lui donnant qualité pour agir en sa qualité de propriétaire et président de la société YouTime, de la marque YouTime et de l'application YouTime, destinées à permettre aux bénéficiaires de l'allocation personnalisée d'autonomie de faire remplacer leur service d'aide et d'accompagnement à domicile en cas de défaillance de celui-ci ;<br/>
              - la condition d'urgence est remplie en ce que l'ordonnance attaquée porte une atteinte grave et immédiate, d'une part, à ses intérêts financiers et aux intérêts économiques de YouTime et, d'autre part, aux intérêts et au respect de la dignité des bénéficiaires de l'allocation personnalisée d'autonomie, qui ne sont pas mis en mesure de bénéficier du remplacement d'un service d'aide et d'accompagnement à domicile défaillant ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ;<br/>
              - l'allocation personnalisée d'autonomie doit être versée directement à ses bénéficiaires, qui doivent pouvoir choisir leurs intervenants, et non au service d'aide et d'accompagnement à domicile.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le premier alinéa du IV de l'ordonnance du 25 mars 2020 relative aux adaptations des règles d'organisation et de fonctionnement des établissements sociaux et médico-sociaux prévoit qu'en cas de sous-activité ou de fermeture temporaire résultant de l'épidémie de covid-19, le niveau de financement des établissements et services sociaux et médico-sociaux, au sens du code de l'action sociale et des familles, n'est pas modifié et que, pour la partie de financement des établissements et services sociaux et médico-sociaux qui ne relève pas de dotation ou de forfait global, la facturation est établie à terme mensuel échu sur la base de l'activité qui aurait prévalu en l'absence de sous-activité ou de fermeture temporaire résultant de l'épidémie de covid-19.<br/>
<br/>
              3. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés se prononce.<br/>
              4. Pour caractériser l'urgence qu'il y aurait à suspendre l'exécution des dispositions du IV de l'ordonnance du 25 mars 2020 mentionnées au point 2 en tant qu'elles prévoient le maintien du niveau de facturation des services d'aide et d'accompagnement à domicile intervenant au profit des bénéficiaires de l'allocation personnalisée d'autonomie sur la base de l'activité qui aurait prévalu en l'absence de sous-activité ou de fermeture temporaire résultant de l'épidémie de covid-19, le requérant se borne à faire valoir que ces dispositions, en ce qu'elles ne prévoient pas de remplacer le versement de l'allocation personnalisée d'autonomie au service d'aide et d'accompagnement à domicile qui n'assure pas les heures de prestations prévues pendant l'épidémie de covid-19 par un versement au bénéficiaire de cette allocation, afin que celui-ci puissent remplacer son prestataire, porte une atteinte grave et immédiate, d'une part, à ses intérêts financiers et aux intérêts économiques de YouTime, société, marque et application dont il est propriétaire et président et, d'autre part, aux intérêts et au respect de la dignité des bénéficiaires de l'allocation personnalisée d'autonomie. Le requérant n'assortit ces allégations d'aucun élément précis ni d'aucune pièce de nature à établir la consistance ou l'étendue des intérêts financiers ou économiques qu'il invoque, pas davantage qu'il n'étaye ses affirmations sur l'importance du nombre d'heures d'aide et d'accompagnement à domicile non réalisées par les services d'aide et d'accompagnement à domicile depuis le début de l'épidémie de covid-19 et sur les effets produits à ce titre par les dispositions qu'il critique. Il n'établit ainsi pas que ces dispositions porteraient à ses intérêts ou aux intérêts qu'il entend défendre une atteinte suffisamment grave et immédiate pour caractériser une urgence justifiant que l'exécution en soit suspendue sans attendre le jugement de la requête au fond.<br/>
<br/>
              5. Il y a lieu par suite de rejeter la requête selon la procédure prévue par l'article L. 522-3 du code de justice administrative<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
