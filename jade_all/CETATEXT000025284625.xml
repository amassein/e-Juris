<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025284625</ID>
<ANCIEN_ID>JG_L_2012_02_000000355137</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/28/46/CETATEXT000025284625.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 02/02/2012, 355137</TITRE>
<DATE_DEC>2012-02-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355137</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Stéphanie Gargoullaud</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:355137.20120202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 22 décembre 2011 au secrétariat du contentieux du Conseil d'État, présenté par Mme Marine A demeurant ... en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; Mme A demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation de la décision par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation de l'article 7 du décret n° 2001-213 du 8 mars 2001, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du dernier alinéa du I de l'article 3 de la loi du 6 novembre 1962 relative à l'élection du Président de la République au suffrage universel ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 4, 6 et 61-1 ; <br/>
<br/>
              Vu la loi n° 62-1292 du 6 novembre 1962 modifiée notamment par la loi organique n° 76-528 du 18 juin 1976 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le décret n° 2001-213 du 8 mars 2001 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Gargoullaud, chargée des fonctions de Maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'État (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'en vertu de l'article 6 de la Constitution les modalités de l'élection du Président de la République sont fixées par une loi organique ; qu'il résulte de l'article 3 de la loi du 6 novembre 1962 relative à l'élection du Président de la République au suffrage universel, modifiée notamment par la loi organique du 18 juin 1976, que la liste des candidats à cette élection est établie par le Conseil constitutionnel au vu des présentations qui lui sont adressées par au moins cinq cents citoyens titulaires de l'un des mandats électifs énumérés par cet article ; que selon le dernier alinéa du I de ce même article, tel qu'il résulte de la loi organique du 18 juin 1976, le nom et la qualité des citoyens qui ont proposé les candidats inscrits sur la liste établie par le Conseil constitutionnel sont rendus publics par ce dernier huit jours au moins avant le premier tour de scrutin, dans la limite du nombre requis pour la validité de la candidature ; <br/>
<br/>
              Considérant que les dispositions du dernier alinéa du I de l'article 3 de la loi du 6 novembre 1962 résultant de la loi organique du 18 juin 1976, qui soulèvent une question non dénuée de rapport avec les termes du litige, doivent être regardées comme étant applicables au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ; que si le Conseil constitutionnel, dans les motifs et le dispositif de sa décision n° 76-65 DC du 14 juin 1976, a déclaré ces dispositions conformes à la Constitution, les changements ayant affecté la vie politique et l'organisation institutionnelle du pays depuis cette date justifient que la conformité à la Constitution du dernier alinéa du I de l'article 3 de la loi du 6 novembre 1962 puisse être à nouveau examinée par le Conseil constitutionnel ; qu'enfin, un des moyens invoqués par Mme A est tiré du dernier alinéa de l'article 4 de la Constitution, résultant de la loi constitutionnelle du 23 juillet 2008, aux termes duquel : " La loi garantit les expressions pluralistes des opinions et la participation équitable des partis et groupements politiques à la vie démocratique de la Nation "  ; que ce moyen présente le caractère d'une question nouvelle au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ; <br/>
<br/>
              Considérant qu'il y a lieu, dans ces conditions, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité relative au dernier alinéa du I de l'article 3 de la loi du 6 novembre 1962 résultant de la loi organique du 18 juin 1976 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution du dernier alinéa du I de l'article 3 de la loi du 6 novembre 1962 modifiée est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur la requête de Mme A jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme Marine A et au Premier ministre. <br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-02-04 PROCÉDURE. - RÈGLE DE PUBLICITÉ DES « PARRAINAGES » DES CANDIDATS À L'ÉLECTION PRÉSIDENTIELLE - CIRCONSTANCES DE FAIT NOUVELLES - EXISTENCE - CHANGEMENTS AYANT AFFECTÉ LA VIE POLITIQUE ET L'ORGANISATION INSTITUTIONNELLE DU PAYS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-01 PROCÉDURE. - QUESTION NOUVELLE - QUESTION DE LA CONFORMITÉ AU DERNIER ALINÉA DE L'ARTICLE 4 DE LA CONSTITUTION.
</SCT>
<ANA ID="9A"> 54-10-05-02-04 Les changements ayant affecté la vie politique et l'organisation institutionnelle du pays depuis l'examen par le Conseil constitutionnel, par décision n°  76-65 DC du 14 juin 1976, du dernier alinéa du I de l'article 3 de la loi n° 62-1292 du 6 novembre 1962 (règle de publicité des « parrainages » des candidats à l'élection présidentielle) constituent des circonstances de fait nouvelles de nature à justifier un nouvel examen de sa conformité à la Constitution.</ANA>
<ANA ID="9B"> 54-10-05-04-01 Le moyen tiré de la non conformité d'une disposition législative au dernier alinéa de l'article 4 de la Constitution, résultant de la loi constitutionnelle du 23 juillet 2008, aux termes duquel : « La loi garantit les expressions pluralistes des opinions et la participation équitable des partis et groupements politiques à la vie démocratique de la Nation »  présente le caractère d'une question nouvelle au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. Cons. Const., 21 février 2012, n° 2012-233 QPC.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
