<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041869421</ID>
<ANCIEN_ID>JG_L_2020_05_000000440213</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/86/94/CETATEXT000041869421.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 08/05/2020, 440213, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440213</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440213.20200508</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 avril et 5 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la société Innov'sa demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) de suspendre l'exécution de l'arrêté du 10 mars 2020 portant modification des modalités de prise en charge des véhicules destinés au transport passif des personnes handicapées inscrits au titre IV de la liste des produits et prestations prévue à l'article L. 165-1 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que l'arrêté attaqué interdit la vente des fauteuils poussettes, qui représente 58 % de son activité, à compter du 15 juin 2020 et risque ainsi de conduire à sa liquidation ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté attaqué ;<br/>
              - il est entaché d'incompétence, dès lors que l'adjoint à la sous-directrice de la politique des produits de santé et de la qualité des pratiques de soin n'a aucune compétence pour signer un tel arrêté et que le sous-directeur du financement du système de soins, dont la sous-direction n'a pas été créée par décret, ne pouvait en outre signer légalement un arrêté au nom de deux ministres différents quand la loi prévoit l'intervention de ces deux ministres ;<br/>
              - il a été pris au terme d'une procédure irrégulière, dès lors que, en premier lieu, il n'est pas établi que tous les membres de la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé (CNEDiMTS) aient été régulièrement convoqués, en application des articles R. 165-18 et R. 165-20 du code de la sécurité sociale, aux séances des 5 novembre 2019 et 28 janvier 2020, que, en deuxième lieu, il n'est pas établi que le quorum de quatorze membres présents ait été atteint et que, en troisième lieu, les avis des 19 novembre 2019 et 11 février 2020 de la CNEDiMTS méconnaissent les dispositions de l'article R. 165-11 du code de la sécurité sociale, en ce qu'ils concluent sans justification sérieuse à la nécessité d'exclure la prise en charge des sièges coquilles et des appareils de soutien partiel de la tête ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation, en ce qu'il modifie les conditions de fabrication des structures rigides de manière injustifiée, en exigeant que la structure soit composée de parties démontables ou que le revêtement capitonné soit distinct pour les différentes parties, même non démontables ;<br/>
              - il est entaché d'une erreur de fait, en ce que la prohibition de toute prise en charge cumulée du véhicule pour handicapé physique (VHP) à pousser pour personne de plus de seize ans avec un dispositif de soutien de la tête ne peut être motivée par l'existence de dispositifs de soutien de la tête dans les tarifs de base des poussettes pour adultes, puisque de tels dispositifs n'y sont pas prévus ;<br/>
              - l'arrêté attaqué est entaché d'une erreur manifeste d'appréciation, en ce qu'il exclut l'adjonction d'un dispositif de soutien de la tête pour les VHP destinés aux personnes de 16 ans alors même qu'un dispositif de maintien de la tête est indispensable dans certaines situations ;<br/>
              - l'arrêté instaure une rupture d'égalité dès lors que ni le critère de l'âge ni le mode de propulsion du fauteuil roulant ne peuvent justifier l'interdiction ou non d'un dispositif d'appui-tête.<br/>
<br/>
              Par un mémoire en défense, enregistré le 30 avril 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et qu'il n'existe aucun doute sérieux sur la légalité de l'arrêté du 10 mars 2020.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 5 mai 2020 à 20 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Par l'arrêté du 10 mars 2020 portant modification des modalités de prise en charge des véhicules destinés au transport passif des personnes handicapées inscrits au titre IV de la liste des produits et prestations prévue à l'article L. 165-1 du code de la sécurité sociale, publié au journal officiel du 14 mars 2020, le ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont modifié le paragraphe 1 " Généralités " de la section A " Poussettes, fauteuils roulants à pousser et châssis roulants destinés au transport passif des personnes handicapées " du chapitre 2 " Véhicules divers " du titre IV de la liste des produits et prestations prévue à l'article L. 165-1 du code de la sécurité sociale. La société Innov'sa demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cet arrêté.<br/>
<br/>
              Sur les dispositions applicables :<br/>
<br/>
              3. Aux termes de l'article L. 165-1 du code de sécurité sociale, dans sa rédaction applicable à la date de l'acte attaqué : " Le remboursement par l'assurance maladie des dispositifs médicaux à usage individuel (...) est subordonné à leur inscription sur une liste établie après avis d'une commission de la Haute Autorité de santé mentionnée à l'article L. 161-37. L'inscription est effectuée soit par la description générique de tout ou partie du produit concerné, soit sous forme de marque ou de nom commercial. L'inscription sur la liste peut elle-même être subordonnée au respect de spécifications techniques, d'indications thérapeutiques ou diagnostiques et de conditions particulières de prescription et d'utilisation. (...) ".<br/>
<br/>
              4. Aux termes du premier alinéa de l'article R. 165-1 du code de la sécurité sociale : " Les produits et prestations mentionnés à l'article L. 165-1 ne peuvent être remboursés par l'assurance maladie, sur prescription médicale ou sur prescription par un infirmier exerçant en pratique avancée dans les conditions prévues à l'article R. 4301-3 du code de la santé publique (...) que s'ils figurent sur une liste établie par arrêté du ministre chargé de la sécurité sociale et du ministre chargé de la santé après avis de la commission spécialisée de la Haute Autorité de santé mentionnée à l'article L. 165-1 du présent code et dénommée "Commission nationale d'évaluation des dispositifs médicaux et des technologies de santé" ". <br/>
<br/>
              5. Par l'arrêté du 17 octobre 2017 portant modification des modalités de prise en charge des " sièges coquilles de série " au titre Ier de la liste prévue à l'article L. 165-1 du code de la sécurité sociale, dans sa rédaction issue de l'arrêté du 15 juillet 2019, le ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont restreint, dans l'intérêt de la santé publique, les indications de prise en charge des sièges coquilles de série aux " Patients âgés ayant une impossibilité de se maintenir en position assise sans un système de soutien et n'ayant pas d'autonomie de déplacement (...) ". Aux termes des dispositions issues de l'arrêté contesté, ne peuvent pas faire l'objet d'une prise en charge au titre des " Poussettes, fauteuils roulants à pousser et châssis roulants destinés au transport passif des personnes handicapées " du chapitre 2 " Véhicules divers " du titre IV de la liste des produits et prestations (LPP) prévue à l' article L. 165-1 du code de la sécurité sociale, " les véhicules comprenant une même structure rigide : / - dont le siège, le dossier, les accoudoirs ou maintiens latéraux sont non démontables en 4 parties (notamment avec fixations non réutilisables) ; / - ou dont le revêtement capitonné n'est pas propre à chacune des parties : siège, dossier, accoudoirs ou maintiens latéraux ". Par ailleurs, aux termes du même arrêté, " La prise en charge du code 4263950 " VHP, POUSSETTE OU FAUTEUIL, &gt; OU = 16 ANS, DOSSIER OU DOSSIER ET SIEGE INCLINABLES " ne peut se cumuler avec le code 1211489 d'un appareil de soutien partiel de la tête ".<br/>
<br/>
              Sur la condition d'urgence :<br/>
<br/>
              6. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porterait atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre.<br/>
<br/>
              7. L'activité de la société est, de fait de la nature des produits qu'elle commercialise, très dépendante des conditions de prise en charge par l'assurance maladie. Elle établit, par les pièces comptables qu'elle produit, que son chiffre d'affaires est passé de 35 millions d'euros en 2017, année au cours de laquelle elle commercialisait principalement des sièges coquilles de série, à 14 millions environ en 2018, avant de remonter à 24 millions d'euros en 2019, du fait d'une réorientation de sa production vers les poussettes pour handicapés, dont elle indique qu'elles représentent désormais 58 % de son activité. Elle soutient, sans être sérieusement contredite, que l'application de l'arrêté qu'elle conteste l'empêcherait de continuer à commercialiser ce produit, ce qui pourrait conduire à sa liquidation. Dès lors, la condition d'urgence doit être regardée comme remplie. <br/>
<br/>
              Sur les moyens soulevés par la société Innov'sa :<br/>
<br/>
              8. La société soutient, d'une part, que l'arrêté contesté est entaché d'une erreur manifeste d'appréciation en ce qu'il modifie les conditions de fabrication des véhicules destinés au transport passif des handicapés en exigeant, sans justification pertinente, que le siège, le dossier, les accoudoirs ou maintiens latéraux des structures rigides soient démontables en quatre parties ou que le revêtement capitonné soit propre à chacune de ces parties et, d'autre part, qu'il est entaché d'erreur de fait et d'erreur manifeste d'appréciation en ce qu'il exclut le cumul de la prise en charge d'un véhicule pour handicapé physique avec celle d'un appareil de soutien partiel de la tête, alors que les poussettes et fauteuils à pousser pour handicapés de plus de 16 ans ne comportent d'appui-tête ni dans le modèle de base, ni en option.<br/>
<br/>
              9. Le ministre des solidarités et de la santé soutient en défense que l'arrêté du 10 mars 2020 a pour objet de d'éviter que les sièges coquilles de série ne soient remboursés par l'assurance maladie en tant que véhicules pour handicapés physiques inscrits au titre IV de la liste des produits et prestations, alors que leurs indications de prise en charge ont été restreintes, dans l'intérêt de la santé publique, par l'effet des arrêtés des 17 octobre 2017 et 15 juillet 2019, qui ont modifié à cette fin le titre I de la liste des produits et prestations. Toutefois, en dépit de cette explication, les moyens soulevés par la société Innov'sa sont de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de l'arrêté contesté, d'une part, en tant que, tout en autorisant les structures rigides, il exige qu'elles soient démontables ou que leur revêtement ne soit pas commun à leurs différentes composantes, d'autre part, en tant qu'il exclut le cumul de la prise en charge d'un dispositif de soutien de la tête avec celle d'un véhicule pour handicapé physique, sans que, dans l'un et l'autre cas, les modifications ainsi apportées aux spécifications des véhicules ne soient justifiées par le service attendu de leur usage.<br/>
<br/>
              10. Il résulte de ce qui précède que la société Innov'sa est fondée à demander la suspension de l'exécution de l'arrêté du 10 mars 2020. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Innov'sa au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'exécution de l'arrêté du 10 mars 2020 portant modification des modalités de prise en charge des véhicules destinés au transport passif des personnes handicapées inscrits au titre IV de la liste des produits et prestations prévue à l'article L. 165-1 du code de la sécurité sociale est suspendue.<br/>
Article 2 : L'Etat versera à la société Innov'sa la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la société Innov'sa, au ministre des solidarités et de la santé et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
