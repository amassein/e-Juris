<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445566</ID>
<ANCIEN_ID>JG_L_2015_03_000000366813</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/55/CETATEXT000030445566.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 23/03/2015, 366813, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366813</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Action en astreinte</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:366813.20150323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 0903167 du 30 novembre 2011, le tribunal administratif de Bordeaux :<br/>
              - a annulé la décision implicite par laquelle le maire de la commune de Saint-Eutrope-de-Born avait rejeté la demande du 6 juillet 2009 de Mme D...tendant au retrait de la croix implantée à l'entrée du village ;<br/>
              - a enjoint au maire de procéder à ce retrait dans un délai de trois mois à compter de la notification de ce jugement.<br/>
<br/>
              Par une requête, enregistrée le 14 janvier 2013 au greffe du tribunal administratif de Bordeaux, Mme D...demande :<br/>
<br/>
              1°) que la commune de Saint-Eutrope-de-Born soit condamnée à une astreinte de 500 euros par jour en vue d'assurer l'exécution de ce jugement ;<br/>
<br/>
              2°) que soit mise à la charge de cette commune la somme de 2 316,65 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. " ; qu'aux termes de l'article L. 911-4 du même code : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu la décision d'en assurer l'exécution. / (...) Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte. / Le tribunal administratif ou la cour administrative d'appel peut renvoyer la demande d'exécution au Conseil d'Etat " ; <br/>
<br/>
              2. Considérant, d'une part, qu'il résulte de ces dispositions qu'en l'absence de définition, par le jugement ou l'arrêt dont l'exécution lui est demandée, des mesures qu'implique nécessairement cette décision, il appartient au juge saisi sur le fondement de l'article L. 911-4 du code de justice administrative d'y procéder lui-même en tenant compte des situations de droit et de fait existant à la date de sa décision ; que, si la décision faisant l'objet de la demande d'exécution prescrit déjà de telles mesures en application de l'article L. 911-1 du même code, il peut, dans l'hypothèse où elles seraient entachées d'une obscurité ou d'une ambigüité, en préciser la portée ; que, le cas échéant, il lui appartient aussi d'en édicter de nouvelles en se plaçant, de même, à la date de sa décision, sans toutefois pouvoir remettre en cause celles qui ont précédemment été prescrites ni méconnaître l'autorité qui s'attache aux motifs qui sont le soutien nécessaire du dispositif de la décision juridictionnelle dont l'exécution lui est demandée ; qu'en particulier, la rectification des erreurs de droit ou de fait dont serait entachée la décision en cause ne peut procéder que de l'exercice, dans les délais fixés par les dispositions applicables, des voies de recours ouvertes contre cette décision ;<br/>
<br/>
              3. Considérant, d'autre part, qu'il appartient au juge saisi sur le fondement de l'article L. 911-4 d'apprécier l'opportunité de compléter les mesures déjà prescrites ou qu'il prescrit lui-même par la fixation d'un délai d'exécution et le prononcé d'une astreinte suivi, le cas échéant, de la liquidation de celle-ci, en tenant compte tant des circonstances de droit et de fait existant à la date de sa décision que des diligences déjà accomplies par les parties tenues de procéder à l'exécution de la chose jugée ainsi que de celles qui sont encore susceptibles de l'être ;<br/>
<br/>
              4. Considérant que, par un jugement du 30 novembre 2011, devenu définitif faute que les parties en aient relevé appel, le tribunal administratif de Bordeaux, après avoir relevé que le talus communal situé en bordure de voie publique, à l'entrée du village de Saint-Eutrope-de-Born, sur lequel avait été installé un Christ en croix était un emplacement public, a annulé le refus du maire de cette commune de retirer cette croix et lui a enjoint d'y procéder dans un délai de trois mois à compter de la notification de ce jugement ; <br/>
<br/>
              5. Considérant que, s'il est loisible à la personne qui prétendrait être propriétaire du terrain sur lequel est implantée la croix litigieuse de former tierce opposition contre ce jugement, le maire de Saint-Eutrope-de-Born ne peut utilement, pour établir qu'il n'est pas en mesure de l'exécuter, se borner à invoquer une telle circonstance, dont il ne soutient pas qu'elle résulterait d'une mutation de propriété postérieure au jugement du 30 novembre 2011, ni à produire un arrêté d'alignement signé par lui le 4 juin 2013 et qui se borne à constater les limites de la voie publique, dès lors que, ce faisant, il remet nécessairement en cause le bien-fondé de la mesure d'exécution définitivement prononcée par le tribunal administratif ainsi que le motif qui en constitue le soutien nécessaire ; <br/>
<br/>
              6. Considérant qu'il ne résulte pas de l'instruction que le maire aurait, comme il pouvait le faire au titre des diligences lui incombant pour répondre à la demande d'exécution du jugement en cause, averti la personne dont il affirme qu'elle serait propriétaire du terrain situé en bordure de la voie publique de son intention d'exécuter ce jugement afin de la mettre à même, si elle s'y croit fondée, de former un recours en tierce opposition devant le tribunal administratif ou de saisir le juge civil d'une action tendant à faire reconnaître son droit de propriété ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le maire de Saint-Eutrope-de-Born n'établit pas avoir accompli toutes les diligences utiles qui lui incombaient en vue de l'exécution du jugement du 30 novembre 2011 du tribunal administratif de Bordeaux ; qu'il y a lieu, par suite, de prononcer contre la commune, à défaut pour elle de justifier avoir procédé à ces diligences dans un délai de deux mois à compter de la notification de la présente décision, une astreinte de 100 euros par mois jusqu'à la date à laquelle elle y aura procédé ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Une astreinte est prononcée à l'encontre de la commune de Saint-Eutrope-de-Born, si elle ne justifie pas avoir, dans les deux mois suivant la notification de la présente décision, accompli toutes diligences utiles à l'exécution du jugement du 30 novembre 2011 du tribunal administratif de Bordeaux dans les conditions définies dans les motifs de la présente décision et jusqu'à la date de cette exécution. Le taux de cette astreinte est fixé à 100 euros par mois, à compter de l'expiration du délai de deux mois suivant la notification de la présente décision.<br/>
Article 2 : La commune de Saint-Eutrope-de-Born communiquera au secrétariat du contentieux du Conseil d'État copie des actes justifiant des mesures prises pour exécuter le jugement du 30 novembre 2011 du tribunal administratif de Bordeaux.<br/>
Article 3 : La présente décision sera notifiée à Mme B...D...et à la commune de Saint-Eutrope-de-Born. <br/>
Copie en sera adressée, pour information, à M. C...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-07 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. - DEMANDE D'EXÉCUTION D'UNE DÉCISION JURIDICTIONNELLE (ART. L. 911-4 DU CJA) - POUVOIRS DU JUGE - 1) PRINCIPE - A) I) ABSENCE DE DÉFINITION PRÉALABLE DES MESURES D'EXÉCUTION - OBLIGATION DE DÉFINIR DE TELLES MESURES - II) MESURES D'EXÉCUTION DÉJÀ DÉFINIES - POSSIBILITÉ DE PRÉCISER LA PORTÉE DES MESURES D'EXÉCUTION ET DE LES COMPLÉTER - EXISTENCE - POSSIBILITÉ DE LES REMETTRE EN CAUSE OU DE REMETTRE EN CAUSE LA DÉCISION JURIDICTIONNELLE DONT L'EXÉCUTION EST DEMANDÉE - ABSENCE [RJ1] - B) APPRÉCIATION DE L'OPPORTUNITÉ DE COMPLÉTER LES MESURES D'EXÉCUTION PAR LA FIXATION D'UN DÉLAI D'EXÉCUTION ET LE PRONONCÉ D'UNE ASTREINTE - PRISE EN COMPTE DES CIRCONSTANCES EXISTANT À LA DATE DE LA DÉCISION ET DES DILIGENCES DÉJÀ ACCOMPLIES PAR LES PARTIES AINSI QUE DE CELLES QUI SONT ENCORE SUSCEPTIBLES DE L'ÊTRE - 2) ESPÈCE - JUGEMENT AYANT ENJOINT À UN MAIRE DE  RETIRER UNE CROIX AU MOTIF QU'ELLE ÉTAIT IMPLANTÉE SUR UN EMPLACEMENT PUBLIC  - POSSIBILITÉ POUR LE MAIRE DE SOUTENIR QUE LE TERRAIN D'ASSIETTE DE LA CROIX APPARTIENT À UN PROPRIÉTAIRE PRIVÉ - ABSENCE - POSSIBILITÉ POUR CET ÉVENTUEL PROPRIÉTAIRE DE FORMER TIERCE OPPOSITION - EXISTENCE - OBLIGATION POUR LE MAIRE D'AVERTIR CET ÉVENTUEL PROPRIÉTAIRE DE SON INTENTION D'EXÉCUTER LE JUGEMENT - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-06-07 1) a) i) Il résulte des dispositions des articles L. 911-1 et L. 911-4 du code de justice administrative (CJA) qu'en l'absence de définition, par le jugement ou l'arrêt dont l'exécution lui est demandée, des mesures qu'implique nécessairement cette décision, il appartient au juge saisi sur le fondement de l'article L. 911-4 du CJA d'y procéder lui-même en tenant compte des situations de droit et de fait existant à la date de sa décision.... ,,ii) Si la décision faisant l'objet de la demande d'exécution prescrit déjà de telles mesures en application de l'article L. 911-1 du même code, il peut, dans l'hypothèse où elles seraient entachées d'une obscurité ou d'une ambigüité, en préciser la portée. Le cas échéant, il lui appartient aussi d'en édicter de nouvelles en se plaçant, de même, à la date de sa décision, sans toutefois pouvoir remettre en cause celles qui ont précédemment été prescrites ni méconnaître l'autorité qui s'attache aux motifs qui sont le soutien nécessaire du dispositif de la décision juridictionnelle dont l'exécution lui est demandée. En particulier, la rectification des erreurs de droit ou de fait dont serait entachée la décision en cause ne peut procéder que de l'exercice, dans les délais fixés par les dispositions applicables, des voies de recours ouvertes contre cette décision.,,,b) Il appartient au juge saisi sur le fondement de l'article L. 911-4 d'apprécier l'opportunité de compléter les mesures déjà prescrites ou qu'il prescrit lui-même par la fixation d'un délai d'exécution et le prononcé d'une astreinte suivi, le cas échéant, de la liquidation de celle-ci, en tenant compte tant des circonstances de droit et de fait existant à la date de sa décision que des diligences déjà accomplies par les parties tenues de procéder à l'exécution de la chose jugée ainsi que de celles qui sont encore susceptibles de l'être.,,,2) En l'espèce, jugement, devenu définitif faute que les parties en aient relevé appel, par lequel le tribunal administratif, après avoir relevé que le talus communal situé en bordure de voie publique, sur lequel avait été installé un Christ en croix était un emplacement public, a annulé le refus du maire de la commune de retirer cette croix et lui a enjoint d'y procéder dans un délai de trois mois à compter de la notification de ce jugement.,,,S'il est loisible à la personne qui prétendrait être propriétaire du terrain sur lequel est implantée la croix litigieuse de former tierce opposition contre ce jugement, le maire de la commune ne peut utilement, pour établir qu'il n'est pas en mesure de l'exécuter, se borner à invoquer une telle circonstance, dont il ne soutient pas qu'elle résulterait d'une mutation de propriété postérieure au jugement, ni à produire un arrêté d'alignement signé par lui postérieurement au jugement et qui se borne à constater les limites de la voie publique, dès lors que, ce faisant, il remet en effet nécessairement en cause le bien-fondé de la mesure d'exécution définitivement prononcée par le tribunal administratif ainsi que le motif qui en constitue le soutien nécessaire.,,,Il ne résulte pas de l'instruction que le maire aurait, comme il pouvait le faire au titre des diligences lui incombant pour répondre à la demande d'exécution du jugement en cause, averti la personne dont il affirme qu'elle serait propriétaire du terrain situé en bordure de la voie publique de son intention d'exécuter ce jugement afin de la mettre à même, si elle s'y croit fondée, de former un recours en tierce opposition devant le tribunal administratif ou de saisir le juge civil d'une action tendant à faire reconnaître son droit de propriété. Par suite, le maire n'établit pas avoir accompli toutes les diligences utiles qui lui incombaient en vue de l'exécution du jugement. Prononcé d'une astreinte.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 mai 2004, Magnat, n° 250730, T. pp. 838-841 ; CE, 23 novembre 2005, Société Eiffage TP, n° 271329, p. 524 ; CE, 29 juin 2011, SCI La Lauzière et autres, n°327080 327256 327332, p. 308.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
