<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043672626</ID>
<ANCIEN_ID>JG_L_2021_06_000000440064</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/26/CETATEXT000043672626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/06/2021, 440064</TITRE>
<DATE_DEC>2021-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440064</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440064.20210616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E... B...-D... a demandé au tribunal administratif de Nice d'annuler la décision du 13 juillet 2017 par laquelle la caisse d'allocations familiales des Alpes-Maritimes a décidé la récupération d'un indu de revenu de solidarité active d'un montant de 12 099,14 euros pour la période de mars 2013 à mars 2017, d'annuler la décision du 12 septembre 2017 par laquelle le président du conseil départemental des Alpes-Maritimes lui a infligé une amende administrative d'un montant de 400 euros et d'annuler le titre exécutoire émis le 11 octobre 2017 pour recouvrer cette amende administrative.<br/>
<br/>
              Par un jugement n°s 1703212, 1704871, 1705242 du 20 février 2020, le tribunal administratif de Nice a rejeté ces demandes.<br/>
<br/>
              Par une ordonnance n° 20MA01541 du 14 avril 2020, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 3 avril 2020 au greffe de cette cour, présenté par Mme B...-D....<br/>
<br/>
              Par ce pourvoi et un nouveau mémoire, enregistré le 28 septembre 2020, Mme B...-D... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Piwnica, Molinié, son avocat, au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... C..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de Mme B...-D... et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat du département des Alpes-Maritimes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après un contrôle de la situation de Mme B...-D..., la caisse d'allocations familiales des Alpes-Maritimes a décidé, le 13 juillet 2017, de récupérer un indu de revenu de solidarité active d'un montant de 12 099,14 euros pour la période du 1er avril 2013 au 13 juillet 2017, en raison de son omission de déclarer certaines des ressources de son foyer. Par une décision du 12 septembre 2017, le département des Alpes-Maritimes a prononcé à l'encontre de Mme B...-D... une amende administrative d'un montant de 400 euros au motif de ces omissions déclaratives et a émis le 11 octobre 2017 un avis de somme à payer valant titre exécutoire en vue de son recouvrement. Mme B...-D... a contesté devant le tribunal administratif de Nice la décision de la caisse d'allocations familiales du 13 juillet 2017 relative à l'indu de revenu de solidarité active, celle du président du conseil départemental du 12 septembre 2017 relative à l'amende administrative ainsi que le titre exécutoire émis le 11 octobre 2017. Par un jugement du 20 février 2020, contre lequel Mme B...-D... se pourvoit en cassation, le tribunal a rejeté comme irrecevables les conclusions de celle-ci dirigées contre la décision de récupération de l'indu de revenu de solidarité active au motif qu'elle n'avait pas exercé un recours administratif préalable et a rejeté comme infondé le surplus de ses conclusions.<br/>
<br/>
              2. D'une part, le premier alinéa de l'article L. 262-47 du code de l'action sociale et des familles prévoit que : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil départemental (...) ". Selon le premier alinéa de l'article R. 262-88 du même code : " Le recours administratif préalable mentionné à l'article L. 262-47 est adressé par le bénéficiaire au président du conseil départemental dans un délai de deux mois à compter de la notification de la décision contestée (...) ". Ce recours administratif préalable obligatoire constitue une demande au sens de l'article L. 112-1 du code des relations entre le public et l'administration, aux termes duquel : " Toute personne tenue de respecter une date limite ou un délai pour présenter une demande (...) peut satisfaire à cette obligation au plus tard à la date prescrite au moyen d'un envoi de correspondance, le cachet apposé par les prestataires de services postaux autorisés au titre de l'article L. 3 du code des postes et des communications électroniques faisant foi ". D'autre part, sauf texte contraire, le respect du délai de recours devant une juridiction administrative s'apprécie lors de l'enregistrement du recours au greffe de la juridiction.<br/>
<br/>
              3. L'institution d'un recours administratif, préalable obligatoire à la saisine du juge, vise à laisser à l'autorité compétente pour en connaître le soin d'arrêter définitivement la position de l'administration. Pour autant, dès lors que le recours administratif obligatoire a été adressé à l'administration préalablement au dépôt de la demande contentieuse, la circonstance que cette dernière demande ait été présentée de façon prématurée, avant que l'autorité administrative ait statué sur le recours administratif, ne permet pas au juge administratif de la rejeter comme irrecevable si, à la date à laquelle il statue, est intervenue une décision, expresse ou implicite, se prononçant sur le recours administratif. Il appartient alors au juge administratif, statuant après que l'autorité compétente a définitivement arrêté sa position, de regarder les conclusions dirigées formellement contre la décision initiale comme tendant à l'annulation de la décision, née de l'exercice du recours administratif préalable, qui s'y est substituée. <br/>
<br/>
              4. En l'espèce, le tribunal administratif, après avoir relevé que le recours administratif préalable, exigé par l'article L. 262-47 du code de l'action sociale et des familles, avait été exercé par Mme B...-D... de telle sorte qu'il avait été reçu par le département des Alpes-Maritimes le jour de l'enregistrement de sa demande contentieuse au greffe du tribunal, a rejeté cette dernière demande comme irrecevable au motif qu'elle avait été déposée sans que l'autorité administrative ait pu arrêter définitivement sa position. En statuant ainsi, alors qu'à la date à laquelle il statuait le caractère prématuré de la demande contentieuse avait été couvert par le rejet du recours administratif, le tribunal administratif a commis une erreur de droit. Mme B...-D... est, par suite, fondée à demander pour ce motif l'annulation du jugement qu'elle attaque, en tant qu'il statue sur ces conclusions relatives à la récupération de l'indu de revenu de solidarité active.<br/>
<br/>
              5. Ainsi annulé en tant qu'il statue sur l'indu de revenu de solidarité active, le jugement attaqué doit également être annulé, en conséquence et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, en tant qu'il statue sur le bien-fondé de l'amende administrative prononcée le 12 septembre 2017 sur le fondement de l'article L. 262-52 du code de l'action sociale et des familles, motivée par les omissions déclaratives ayant abouti au versement de cet indu, ainsi que sur le titre exécutoire émis le 11 octobre 2017 pour son recouvrement. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle, en tout état de cause, à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas partie à la procédure, au titre de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nice du 20 février 2020 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nice.<br/>
Article 3 : Les conclusions présentées au titre de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme E... B...-D... et au département des Alpes-Maritimes. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - RAPO - DEMANDE CONTENTIEUSE PRÉMATURÉE - CONDITIONS DE RECEVABILITÉ [RJ1] - 1) RAPO AYANT PRÉCÉDÉ LA DEMANDE CONTENTIEUSE [RJ2]  - 2) ADMINISTRATION AYANT PRIS SA DÉCISION AVANT QUE LE JUGE AIT STATUÉ.
</SCT>
<ANA ID="9A"> 54-01-02-01 L'institution d'un recours administratif, préalable obligatoire (RAPO) à la saisine du juge, vise à laisser à l'autorité compétente pour en connaître le soin d'arrêter définitivement la position de l'administration.,,,Pour autant, 1) dès lors que le RAPO a été adressé à l'administration préalablement au dépôt de la demande contentieuse, la circonstance que cette dernière demande ait été présentée de façon prématurée, avant que l'autorité administrative ait statué sur le recours administratif, ne permet pas au juge administratif de la rejeter comme irrecevable si, 2) à la date à laquelle il statue, est intervenue une décision, expresse ou implicite, se prononçant sur le recours administratif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant de la réclamation d'assiette, CE, Section, 4 janvier 1974, Sieur X., n° 87418, p. 3. Rappr., s'agissant de la saisine de la CADA, CE, 12 février 1988,,, n° 62332, T. pp. 798-944., ,[RJ2] Comp., dans le cas particulier où la requête prématurée a été complétée par des conclusions qui auraient pu faire l'objet d'un recours distinct et recevable, CE, 4 novembre 2015, M. et Mme,, n° 384241, T. pp. 554-791 ; s'agissant de l'obligation de faire naître une décision administrative préalable lorsque la demande tendant au versement d'une somme d'argent (art. R. 421-1 du CJA), CE, Section, avis, 27 mars 2019, Consorts,, n° 426472, p. 95.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
