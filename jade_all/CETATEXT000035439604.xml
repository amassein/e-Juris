<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035439604</ID>
<ANCIEN_ID>JG_L_2017_08_000000394167</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/43/96/CETATEXT000035439604.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 10/08/2017, 394167, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-08-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394167</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:394167.20170810</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Batipro a demandé au tribunal administratif de La Réunion de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2010 et 2011, à raison de ses locaux commerciaux situés 9, avenue Jean Jaurès dans la commune de Saint-Benoît (La Réunion). Par un jugement n° 1200179 du 20 juillet 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 octobre 2015 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société Batipro demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2014-1655 du 29 décembre 2014 ;  <br/>
              - la décision n° 2015-525 QPC du Conseil constitutionnel du 2 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de la société Batipro.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société Batipro a été assujettie, au titre des années 2010 et 2011, à la taxe foncière sur les propriétés bâties à raison des locaux commerciaux dont elle est propriétaire dans la commune de Saint-Benoît (La Réunion). Elle se pourvoit en cassation contre le jugement du 20 juillet 2015 par lequel le tribunal administratif de La Réunion a rejeté ses conclusions tendant à la décharge des impositions en litige. <br/>
<br/>
              2. Aux termes de l'article 1498 du code général des impôts : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ; / 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; / 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du fond que la société Batipro contestait le choix de l'administration, pour faire application des dispositions précitées, de retenir comme terme de comparaison le local de référence n° 71 du procès-verbal complémentaire de la commune de Saint-Benoît au motif que sa valeur avait été déterminée par comparaison avec celle du local de référence n° 11, alors que celui-ci n'existait plus à la date des années d'imposition litigieuses.<br/>
<br/>
              4. Pour écarter le moyen tiré de l'irrégularité du choix du local de référence retenu par l'administration, le tribunal administratif s'est fondé sur les dispositions du III de l'article 32 de la loi du 29 décembre 2014 de finances rectificative pour 2014 aux termes duquel : " Sous réserve des décisions de justice passées en force de chose jugée, pour la détermination de la valeur locative des locaux mentionnés à l'article 1496 du code général des impôts et de ceux évalués en application du 2° de l'article 1498 du même code, sont validées les évaluations réalisées avant le 1er janvier 2015 en tant que leur légalité serait contestée au motif que, selon le cas, le local de référence ou le local-type ayant servi de terme de comparaison, soit directement, soit indirectement, a été détruit ou a changé de consistance, d'affectation ou de caractéristiques physiques ". <br/>
<br/>
              5. Toutefois, le Conseil constitutionnel a déclaré ces dispositions contraires à la Constitution par sa décision n° 2015-525 QPC du 2 mars 2016, dont le point 12 précise que cette déclaration d'inconstitutionnalité prend effet à compter de la date de la publication de la décision et qu'elle peut être invoquée dans toutes les instances introduites à cette date et non jugées définitivement. Par suite, le jugement du tribunal administratif est entaché d'une erreur de droit. Il résulte de ce qui précède que la société Batipro est fondée, sans qu'il soit besoin d'examiner les moyens de son pourvoi, à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Batipro de la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>                           D E C I D E :<br/>
                                          --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de La Réunion du 20 juillet 2015 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de La Réunion. <br/>
<br/>
Article 3: L'Etat versera à la société Batipro une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Batipro et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
