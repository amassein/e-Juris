<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038466958</ID>
<ANCIEN_ID>JG_L_2019_05_000000429722</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/46/69/CETATEXT000038466958.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 07/05/2019, 429722, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429722</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP PIWNICA, MOLINIE ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429722.20190507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
             Vu la procédure suivante :<br/>
<br/>
             Par une requête, un mémoire en réplique et un nouveau mémoire enregistrés les 12 avril, 2 et 3 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la société Aéroports de la Côte d'Azur demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision n° 1904-D1 du 3 avril 2019 de l'Autorité de supervision indépendante des redevances aéroportuaires relative à la fixation des tarifs de redevances aéroportuaires et de leurs modulations applicables sur les aérodromes de Nice-Côte d'Azur et Cannes-Mandelieu.<br/>
<br/>
<br/>
<br/>
             Elle soutient que : <br/>
             - le Conseil d'Etat est compétent, en vertu du 2° de l'article R. 311-1 du code de justice administrative, dès lors que la décision contestée émane d'une autorité à compétence nationale ;<br/>
             - la condition d'urgence est remplie dès lors que, d'une part, la décision contestée, en ce qu'elle bouleverse son équilibre économique, social et financier, préjudicie de manière grave et immédiate aux intérêts de la société requérante et, d'autre part, l'exécution de la décision contestée entraînerait des conséquences difficilement réversibles pour la société Aéroports de la Côte d'Azur ;<br/>
             - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
             - en effet, en premier lieu, l'Autorité de supervision indépendante des redevances aéroportuaires n'avait pas compétence pour adopter, sur le fondement des dispositions de l'article R. 224-3-4, la décision contestée, dès lors que ses décisions de refus d'homologation du 12 décembre 2018 et du 21 janvier 2019 sont illégales ;<br/>
             - en deuxième lieu, la décision contestée est entachée d'une erreur de droit, en ce qu'elle fixe la date d'entrée en vigueur des tarifs de redevances aéroportuaires au 15 mai 2019 ;<br/>
             - en troisième lieu, la décision contestée est entachée, d'une part, d'une erreur de droit en ce que la règle de l'évolution modérée des tarifs de redevances aéroportuaires, prévue par les dispositions de l'article R. 224-3-4 du code de l'aviation civile, doit être appréciée au regard du tarif des redevances précédemment en vigueur et non au regard du niveau auquel ces tarifs auraient dû régulièrement être fixés et, d'autre part, d'une erreur manifeste d'appréciation, en ce que la baisse immédiate de 33,4 % par rapport au précédent tarif appliqué méconnaît cette exigence ;<br/>
             - en quatrième lieu, la décision contestée méconnaît l'exigence de juste rémunération des capitaux investis prévue par les dispositions de l'article R. 224-3-1 du code de l'aviation civile ;<br/>
             - en dernier lieu, la décision contestée méconnaît le principe de proportionnalité entre le produit global du tarif de la redevance et le coût des services rendus, prévu par les dispositions de l'article L. 6325-1 du code des transports.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 29 avril 2019, l'Autorité de supervision indépendante des redevances aéroportuaires conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la société Aéroports de la Côte d'Azur ne sont pas fondés.<br/>
<br/>
<br/>
              Par une intervention, enregistrée le 30 avril 2019, le syndicat des compagnies aériennes autonomes (SCARA) demande au Conseil d'Etat de rejeter la requête de la société Aéroports de la Côte d'Azur.<br/>
<br/>
              Par une intervention, enregistrée le 3 mai 2019, la chambre syndicale du transport aérien (CSTA) demande au Conseil d'Etat de rejeter la requête de la société Aéroports de la Côte d'Azur. <br/>
<br/>
              Par une intervention, enregistrée le 6 mai 2019, l'International Air Transport Association (IATA) demande au Conseil d'Etat de rejeter la requête de la société Aéroports de la Côte d'Azur et de mettre à la charge de cette société la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Aéroports de la Côte d'Azur, d'autre part, l'Autorité de supervision indépendante des redevances aéroportuaires, le ministre d'Etat, ministre de la transition écologique et solidaire, le syndicat des compagnies aériennes autonomes, la chambre syndicale du transport aérien et l'International Air Transport Association ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 6 mai 2019 à 14 heures, au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Froger, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Aéroports de la Côte d'Azur ;<br/>
<br/>
              - les représentants de la société Aéroports de la Côte d'Azur ;<br/>
<br/>
              - les représentants de l'Autorité de supervision indépendante des redevances aéroportuaires ;<br/>
<br/>
              - les représentants du ministre d'Etat, ministre de la transition écologique et solidaire ;<br/>
<br/>
              - Me Périer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'International Air Transport Association ;<br/>
<br/>
              - les représentants du syndicat des compagnies aériennes autonomes ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 6 mai 2019 à 20 heures ; <br/>
<br/>
<br/>
              Vu le nouveau mémoire, enregistrée le 6 mai 2019 avant la clôture de l'instruction, présenté par la société Aéroports de la Côte d'Azur ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2009/12/CE du Parlement européen et du Conseil du 11 mars 2009 ;<br/>
              - le code de l'aviation civile ;<br/>
              - le code des transports ;<br/>
              - le décret n° 2016-825 du 23 juin 2016 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le syndicat des compagnies aériennes autonomes (SCARA), la chambre syndicale du transport aérien (CSTA) et l'International Air Transport Association (IATA). justifient, chacun, d'un intérêt suffisant au maintien de la décision attaquée. Leurs interventions respectives sont, par suite, recevables.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              3. Par une décision du 3 avril 2019, l'Autorité de supervision indépendante des redevances aéroportuaires a fixé de manière unilatérale les tarifs des redevances aéroportuaires applicables sur les aéroports de Nice-côte d'Azur et Cannes-Mandelieu à compter du 15 mai 2019, sur le fondement des dispositions du IV de l'article R. 224-3-4 du code de l'aviation civile. Par la présente requête, la société Aéroport de la Côte d'Azur demande la suspension de cette décision.<br/>
<br/>
              4. Les moyens invoqués par la société Aéroports de la Côte d'Azur à l'appui de sa demande de suspension et tirés de ce que l'Autorité de supervision indépendante des redevances aéroportuaires n'aurait pas eu compétence pour fixer les tarifs litigieux en raison de l'illégalité des refus d'homologation qu'elle a opposés les 12 décembre 2018 et 21 janvier 2019 aux tarifs qui lui avaient été notifiés par la société Aéroport de la Côte d'Azur, de ce que la décision attaquée ne pourrait pas légalement prendre effet au 15 mai 2019, de ce qu'elle méconnaîtrait, en premier lieu, la règle d'évolution modérée des tarifs fixée à l'article R. 224-3-4 du code de l'aviation civile, en deuxième lieu, le droit de l'exploitant à recevoir une juste rémunération des capitaux investis et, en troisième lieu, la règle générale de proportionnalité ne paraissent pas, en l'état de l'instruction, propres à créer un doute sérieux sur la légalité de la décision contestée. Dès lors, l'une des conditions posées par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par la société Aéroports de la Côte d'Azur tendant à ce que soit suspendue la décision du 3 avril 2019 de l'Autorité de supervision indépendante des redevances aéroportuaires doit être rejetée. <br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Aéroports de la Côte d'Azur la somme que l'International Air Transport Association (IATA) demande en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Les interventions du syndicat des compagnies aériennes autonomes (SCARA), de la chambre syndicale du transport aérien (CSTA) et de l'International Air Transport Association (IATA) sont admises.<br/>
Article 2 : La requête de la société Aéroports de la Côte d'Azur est rejetée.<br/>
Article 3 : Les conclusions de l'International Air Transport Association (IATA), présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative, sont rejetées. <br/>
Article 4 : La présente ordonnance sera notifiée à la société Aéroports de la Côte d'Azur et à l'Autorité de supervision indépendante des redevances aéroportuaire.<br/>
Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire, au syndicat des compagnies aériennes autonomes (SCARA), à la chambre syndicale du transport aérien (CSTA) et à l'International Air Transport Association (IATA).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
