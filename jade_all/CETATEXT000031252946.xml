<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031252946</ID>
<ANCIEN_ID>J3_L_2015_09_000001301624</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/25/29/CETATEXT000031252946.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de BORDEAUX, 6ème chambre (formation à 3), 28/09/2015, 13BX01624, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-28</DATE_DEC>
<JURIDICTION>CAA de BORDEAUX</JURIDICTION>
<NUMERO>13BX01624</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre (formation à 3)</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. LARROUMEC</PRESIDENT>
<AVOCATS>SELARL A. GUÉRIN &amp; J. DELAS</AVOCATS>
<RAPPORTEUR>M. Antoine  BEC</RAPPORTEUR>
<COMMISSAIRE_GVT>M. BENTOLILA</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       Le syndicat national des praticiens de la Mutualité sociale agricole (SNPMA) a demandé au tribunal administratif de Bordeaux d'annuler la décision du ministre du travail du 6 octobre 2010 fixant la répartition des personnels entre collèges pour l'élection au comité d'entreprise de la caisse de la Mutualité sociale agricole de la Gironde.<br/>
<br/>
       Par un jugement n° 1100164 du 25 avril 2013 le tribunal administratif de Bordeaux a rejeté leur demande comme irrecevable. <br/>
<br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       Par une requête enregistrée le 14 juin 2013 et un mémoire complémentaire enregistré le 11 juillet 2013, le syndicat national des praticiens de la Mutualité sociale agricole (SNPMA), dont le siège est 1 allée de Managua à Dinard (35800), représenté par Me Guérin, demande à la cour : <br/>
<br/>
       1°) d'annuler le jugement du tribunal administratif de Bordeaux du 25 avril 2013 ; <br/>
<br/>
       2°) d'annuler la décision contestée ; <br/>
       3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
-----------------------------------------------------------------------------------------------------------------<br/>
<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
<br/>
       Vu :<br/>
       - le code du travail ; <br/>
       - le code de justice administrative.<br/>
<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M Antoine Bec, président-assesseur, <br/>
       - les conclusions de M. Pierre Bentolila, rapporteur public ;<br/>
       - et les observations de Me Guérin, avocat du syndicat national des praticiens de la mutualité sociale agricole.<br/>
<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. A la demande de la Mutualité sociale agricole (MSA) de la Gironde, la directrice-adjointe du travail de l'unité territoriale de la Gironde de la Direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) Aquitaine a, par une décision en date du 19 avril 2010, procédé à la répartition du personnel entre les collèges électoraux pour l'élection des membres du comité d'entreprise de la caisse de la MSA de la Gironde. Le syndicat national des praticiens de la Mutualité sociale agricole (SNPMA) ayant formé un recours hiérarchique contre cette décision devant le ministre en charge du travail, le ministre du travail, de la solidarité et de la fonction publique a, par une décision du 6 octobre 2010, annulé la décision du 19 avril 2010 de la directrice-adjointe du travail et a de nouveau fixé la répartition des personnels entre collèges électoraux en vue de l'élection des membres du comité d'entreprise de la MSA de la Gironde. <br/>
       Le syndicat national des praticiens de la Mutualité sociale agricole relève appel du jugement du 25 avril 2013 par lequel le tribunal administratif de Bordeaux a rejeté sa demande tendant à l'annulation de cette décision.<br/>
<br/>
<br/>
       Sur la régularité du jugement attaqué :<br/>
<br/>
       2. Le tribunal administratif a considéré que la demande présentée par le syndicat national des praticiens de la Mutualité sociale agricole (SNPMA) était irrecevable au motif que l'intervention d'un accord préélectoral conclu le 19 octobre 2010 entre les organisations syndicales CFDT et FO et la direction de la caisse de la Mutualité sociale agricole de la Gironde avait eu pour effet de rendre caduque la décision du 6 octobre 2010 du ministre du travail ; <br/>
<br/>
<br/>
       3. Selon les articles L. 2314-11 et L. 2324-13 du code du travail relatifs aux élections des délégués du personnel, lorsque l'accord entre l'employeur et les organisations syndicales sur la répartition du personnel dans les collèges électoraux et la répartition des sièges entre les différentes catégories de personnel ne peut être obtenu, l'autorité administrative procède à cette répartition entre les collèges électoraux.<br/>
<br/>
<br/>
       4. En vertu des articles L. 2314-25 et L. 2324-23 du code du travail, les contestations relatives à l'électorat et à la régularité des opérations électorales sont de la compétence du tribunal d'instance statuant en premier et dernier ressort.<br/>
<br/>
<br/>
       5. Les décisions prises par l'autorité administrative pour procéder à la répartition du personnel dans les collèges électoraux en vue de la désignation des délégués du personnel et des représentants du personnel au comité d'entreprise constituent des actes préparatoires aux élections professionnelles en vue desquelles elles ont été édictées. Par suite, à compter de la date des élections professionnelles, dont il n'incombe qu'à l'autorité judiciaire d'apprécier la validité, ces actes administratifs ne sont plus susceptibles de faire l'objet d'un recours pour excès de pouvoir devant le juge administratif, mais uniquement d'un recours en appréciation de la légalité sur renvoi du juge judiciaire dans le cadre de la contestation éventuelle du résultat du scrutin.<br/>
<br/>
<br/>
       6. Les élections professionnelles au sein de la caisse de la MSA de la Gironde en vue de la désignation des délégués du personnel et des représentants du personnel au comité d'entreprise ayant eu lieu le 14 décembre 2010, la requête du syndicat national des praticiens de la Mutualité sociale agricole, enregistrée le 5 novembre 2010 auprès du Tribunal administratif de Paris, était ainsi recevable. En revanche, à la date du 25 avril 2013 à laquelle le tribunal administratif de Bordeaux a statué, sa requête dirigée contre la décision de l'administration en date du 6 octobre 2010, édictée en vue des seules élections, avait perdu son objet, du fait de l'intervention de ces élections.<br/>
<br/>
<br/>
       7. Il résulte de ce qui précède que c'est à tort que, par le jugement attaqué, les premiers juges ont rejeté comme irrecevable la demande d'annulation présentée par le Syndicat national des praticiens de la mutualité sociale agricole.<br/>
<br/>
<br/>
       8. Considérant qu'il y a lieu, dès lors, de statuer par voie d'évocation, et, sans qu'il soit besoin d'examiner les moyens invoqués par le Syndicat national des praticiens de la mutualité sociale agricole, de constater qu'il n'y a plus lieu de statuer sur les conclusions à fin d'annulation de la décision du ministre du travail  du 6 octobre 2010 présentées par celui-ci devant le tribunal administratif de Bordeaux . <br/>
<br/>
<br/>
       Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
<br/>
       9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que le Syndicat national des praticiens de la mutualité sociale agricole demande au titre des frais exposés et non compris dans les dépens.<br/>
DECIDE :<br/>
Article 1er : Le jugement n° 1100164 du tribunal administratif de Bordeaux du 25 avril 2013 est annulé.<br/>
Article 2 : Il n'y a pas lieu de statuer sur la demande présentée par le syndicat national des praticiens de la Mutualité sociale agricole (SNPMA) devant le tribunal administratif de Bordeaux.<br/>
Article 3 : Les conclusions du syndicat national des praticiens de la Mutualité sociale agricole (SNPMA) présentées en appel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 13BX01624<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-04-01-02 Travail et emploi. Institutions représentatives du personnel. Comités d'entreprise. Organisation des élections.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
