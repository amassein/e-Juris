<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031173259</ID>
<ANCIEN_ID>JG_L_2015_09_000000391245</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/17/32/CETATEXT000031173259.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 14/09/2015, 391245, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391245</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:391245.20150914</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé à la cour administrative d'appel de Bordeaux d'annuler le jugement n° 1400514 du 30 décembre 2014 par lequel le tribunal administratif de Fort-de-France l'a condamné pour contravention de grande voirie à remettre en l'état initial la parcelle sur laquelle il a réalisé des travaux de rénovation de sa maison après un cyclone et à payer une amende de 1 500 euros pour avoir occupé illégalement le domaine public. A l'appui de cette requête, il a produit un mémoire, enregistré le 30 avril 2015 au greffe de la cour administrative d'appel, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, par lequel il soulève une question prioritaire de constitutionnalité. <br/>
<br/>
              Par une ordonnance n° 15BX00695 du 19 juin 2015, enregistrée le 23 juin 2015 au secrétariat du contentieux du Conseil d'Etat, le président de la première chambre de cette cour, avant qu'il soit statué sur la requête de M.B..., a transmis au Conseil d'Etat, en application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 5111-3 du code général de la propriété des personnes publiques.<br/>
<br/>
              Dans la question ainsi transmise et dans un mémoire en réplique, enregistré le 16 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...soutient que cet article méconnaît, d'une part, l'étendue de la compétence du législateur prévue à l'article 34 de la Constitution en ce qu'il renvoie à l'autorité réglementaire le soin de fixer les conditions d'application de la prescription acquisitive trentenaire prévue par les dispositions des articles 2262 et 2265 du code civil, alors en vigueur, sur le domaine privé de l'Etat lorsque les cinquante pas géométriques en faisaient partie entre 1955 et 1986 et, d'autre part, les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen relatifs à la protection du droit de propriété en ce qu'il exclut rétroactivement le département de la Martinique du champ d'application de cette prescription durant cette même période.  <br/>
<br/>
              Par un mémoire, enregistré le 7 juillet 2015, la ministre de l'écologie, du développement durable et de l'énergie soutient que les conditions posées par l'article 23-5 de l'ordonnance du 7 novembre 1958 ne sont pas remplies, la disposition contestée n'étant pas applicable au litige et la question soulevée ne présentant pas, en tout état de cause, un caractère sérieux. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code civil ;<br/>
              - le code du domaine de l'Etat ;<br/>
              - le code général de la propriété des personnes publiques, notamment son article L. 5111-3 ; <br/>
              - la loi n° 55-349 du 2 avril 1955 ;<br/>
              - la loi n° 86-2 du 3 janvier 1986 ;<br/>
              - le décret n° 55-885 du 30 juin 1955 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 5111-1 du code général de la propriété des personnes publiques, qui reprend des dispositions de l'article L. 87 du code du domaine de l'Etat dans leur rédaction issue de l'article 37 de la loi du 3 janvier 1986 relative à l'aménagement, la protection et la mise en valeur du littoral, la zone dite " des cinquante pas géométriques " définie selon les modalités prévues par l'article L. 5111-2 du même code, dans les départements de la Martinique, de la Guadeloupe, de la Guyane et de la Réunion, fait partie du domaine maritime public de l'Etat ; qu'aux termes de l'article L. 5111-3 de ce code, qui reprend des dispositions des articles L. 87 et L. 88 du code du domaine de l'Etat dans leur rédaction issue des articles 37 et 38 de la même loi : "  Les dispositions de l'article L. 5111-1 s'appliquent sous réserve des droits des tiers à la date du 5 janvier 1986. Les droits des tiers résultent : / 1° Soit de titres reconnus valides par la commission prévue par les dispositions de l'article 10 du décret n° 55-885 du 30 juin 1955 ; / 2° Soit de ventes ou de promesses de vente consenties par l'Etat postérieurement à la publication de ce décret et antérieurement à la date du 5 janvier 1986 ; / 3° Soit, dans le département de La Réunion, des éventuelles prescriptions acquises à la date du 3 janvier 1986 " ;<br/>
<br/>
              3. Considérant que M. B...soutient que les dispositions de l'article L. 5111-3 méconnaissent, d'une part, les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen relatifs à la protection du droit de propriété en ce qu'elles excluent le département de la Martinique du champ de la prescription acquisitive trentenaire qui s'appliquait, en vertu des articles 2262 et 2265 du code civil alors en vigueur, au domaine privé de l'Etat, auquel appartenait la zone dite " des cinquante pas géométriques " entre le 30 juin 1955 et la date d'entrée en vigueur de la loi du 3 janvier 1986, et, d'autre part, l'étendue de la compétence du législateur prévue à l'article 34 de la Constitution en ce qu'elles renvoient à l'autorité réglementaire le soin de fixer les règles de prescription dans cette zone durant la même période ;<br/>
<br/>
              4. Considérant, d'une part, que la loi du 2 avril 1955 a habilité le Gouvernement à " adopter toutes mesures tendant à favoriser la mise en valeur des régions qui souffrent de sous-emploi ou d'un développement économique insuffisant, notamment en favorisant la reconversion agricole, l'implantation de nouvelles industries ou l'expansion du tourisme " ; que le décret du 30 juin 1955, conformément à cette habilitation, a prononcé le déclassement du domaine public maritime et l'incorporation au domaine privé de l'Etat, dans les départements d'outre-mer, de la zone dite " des cinquante pas géométriques " en vue d'une utilisation de cette zone plus avantageuse pour l'économie de ces départements ; que l'article 37 de la loi du 3 janvier 1986 a réincorporé cette même zone au domaine public maritime ; qu'ainsi, cette zone a appartenu au domaine privé de l'Etat du 30 juin 1955 au 5 janvier 1986, soit une période supérieure à la période de trente ans prévue, pour l'application de la prescription acquisitive en dehors du cas de l'acquisition de bonne foi et par juste titre d'un immeuble, aux articles 2262 et 2265 du code civil dans leur rédaction applicable à la période ; que, conformément à l'objet de l'habilitation législative mentionnée plus haut, le Gouvernement a légalement, par l'article 10 du décret du 30 juin 1955, soumis, à l'occasion de cette incorporation de l'ensemble de cette zone au domaine privé de l'Etat, les titres de propriété et les titres conférant d'autres droits réels dans cette zone à une procédure de vérification par des commissions ayant un caractère juridictionnel aux fins de vérifier, notamment, si de tels droits trouvaient leur fondement, avant 1955, dans des titres de propriété délivrés à l'origine par l'Etat, qui seul avait pu procéder à la cession à un tiers d'un terrain faisant partie d'une telle zone, et institué un délai de forclusion pour saisir ces commissions et, par l'article 5 du même décret, prévu que dans les départements de la Martinique, de la Guadeloupe et de la Guyane, la prescription acquisitive trentenaire ne pourrait commencer à courir au profit des occupants de terrains de la zone des cinquante pas géométriques qu'à partir de la date de clôture des opérations de délimitation de cette zone, laquelle devait être fixée par un arrêté ministériel ; que l'absence d'intervention d'un tel arrêté a conduit à maintenir le caractère imprescriptible de la zone " des cinquante pas géométriques " malgré son rattachement au domaine privé de l'Etat ; que, par suite, le moyen tiré de ce que l'article L. 5111-3 du code général de la propriété des personnes publiques porterait atteinte au droit de propriété protégé par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen en ce qu'il constate l'exclusion de ces départements du champ d'application de la prescription acquisitive susceptible de s'exercer à raison de cette période du 30 juin 1955 au 3 janvier 1986 ne présente pas de caractère sérieux ;<br/>
<br/>
              5. Considérant, d'autre part, qu'en se bornant à réserver les droits des tiers résultant des éventuelles prescriptions acquises en matière de propriété à la date du 3 janvier 1986 dans le seul département de la Réunion, le législateur n'a pas entendu renvoyer à l'autorité réglementaire le soin de fixer les règles de la prescription acquisitive s'agissant des autres départements ; que, dès lors, le moyen tiré de ce que le législateur aurait méconnu l'étendue de la sa compétence et porté ainsi atteinte au droit de propriété en procédant à un tel renvoi ne présente pas non plus de caractère sérieux ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle, ne présente pas de caractère sérieux ; qu'ainsi, il n'y a pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                           --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution de l'article L. 5111-3 du code général de la propriété des personnes publiques.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
Copie en sera adressée, pour information, au Conseil constitutionnel, au Premier ministre et à la cour administrative d'appel de Bordeaux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
