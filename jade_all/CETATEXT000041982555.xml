<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041982555</ID>
<ANCIEN_ID>JG_L_2020_06_000000427441</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/25/CETATEXT000041982555.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 09/06/2020, 427441, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427441</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427441.20200609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Hays France a demandé au tribunal administratif de Paris la restitution de crédits d'impôt recherche s'élevant à 678 581 euros et à 824 837 euros, respectivement au titre des années 2011 et 2012. <br/>
<br/>
              Par un jugement du 29 novembre 2017, le tribunal administratif a rejeté ses demandes. <br/>
<br/>
              Par un arrêt n° 18PA00276 du 29 novembre 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Hays France contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 janvier,12 avril et 19 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Hays France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Hays France ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Hays France est la société mère d'un groupe fiscalement intégré incluant les sociétés Hays Pharm (HP), Hays Pharma Consulting (HPC) et Hays Pharma Services (HPS). La société HP, qui a la qualité d'organisme privé de recherche agréé, au sens du d bis) du II de l'article 244 quater B du code général des impôts, a confié aux sociétés HPC et HPS le soin de réaliser des travaux de recherche dans le domaine pharmaceutique, qui lui avaient été commandés par des entreprises extérieures au groupe. Les sociétés HPC et HPS ont estimé qu'elles avaient droit, à raison des dépenses de recherche qu'elles ont engagées à cette occasion et qu'elles ont facturées à la société HP, laquelle les a elle-même refacturées aux donneurs d'ordres extérieurs, à des crédits d'impôt recherche, s'élevant, au total, pour les deux sociétés, à 678 581 euros pour l'année 2011 et à 824 837 euros pour l'année 2012. En application des dispositions du b du 1 de l'article 223 O du code général des impôts, la société Hays France s'est substituée aux sociétés HPC et HPS pour demander la restitution de ces crédits d'impôt à l'administration fiscale. Celle-ci ayant rejeté sa demande, la société Hays France a porté le litige devant le tribunal administratif de Paris. Elle demande l'annulation de l'arrêt du 29 novembre 2018 par lequel la cour administrative d'appel de Paris, confirmant le jugement du tribunal administratif du 29 novembre 2017, a rejeté cette demande.<br/>
<br/>
              2. Aux termes de l'article 244 quater B du code général des impôts, dans sa rédaction applicable au litige : I. Les entreprises industrielles et commerciales ou agricoles imposées d'après leur bénéfice réel (...) peuvent bénéficier d'un crédit d'impôt au titre des dépenses de recherche qu'elles exposent au cours de l'année. Le taux du crédit d'impôt est de 30 % pour la fraction des dépenses de recherche inférieure ou égale à 100 millions d'euros et de 5 % pour la fraction des dépenses de recherche supérieure à ce montant (...) / II. Les dépenses de recherche ouvrant droit au crédit d'impôt sont : / a) Les dotations aux amortissements des immobilisations, créées ou acquises à l'état neuf et affectées directement à la réalisation d'opérations de recherche scientifique et technique (...) / b) Les dépenses de personnel afférentes aux chercheurs et techniciens de recherche directement et exclusivement affectés à ces opérations (...) / b bis) Les rémunérations supplémentaires et justes prix mentionnés aux 1 et 2 de l'article L. 611-7 du code de la propriété intellectuelle, au profit des salariés auteurs d'une invention résultant d'opérations de recherche ; / c) Les autres dépenses de fonctionnement exposées dans les mêmes opérations (...) / d) Les dépenses exposées pour la réalisation d'opérations de même nature confiées à : / 1° des organismes de recherche publics ; (...) / d bis) Les dépenses exposées pour la réalisation d'opérations de même nature confiées à des organismes de recherche privés agréés par le ministre chargé de la recherche, ou à des experts scientifiques ou techniques agréés dans les mêmes conditions (...) /d ter) Les dépenses mentionnées aux d et d bis entrent dans la base de calcul du crédit d'impôt recherche dans la limite globale de deux millions d'euros par an. Cette limite est portée à 10 millions d'euros pour les dépenses de recherche correspondant à des opérations confiées aux organismes mentionnés aux d et d bis, à la condition qu'il n'existe pas de lien de dépendance au sens des deuxième à quatrième alinéas du 12 de l'article 39 entre l'entreprise qui bénéficie du crédit d'impôt et ces organismes. /Le plafond de 10 millions d'euros mentionné au premier alinéa est majoré de 2 millions d'euros à raison des dépenses correspondant aux opérations confiées aux organismes mentionnés au d (...) / III. Les subventions publiques reçues par les entreprises à raison des opérations ouvrant droit au crédit d'impôt sont déduites des bases de calcul de ce crédit, qu'elles soient définitivement acquises par elles ou remboursables. Il en est de même des sommes reçues par les organismes ou experts désignés au d et au d bis du II, pour le calcul de leur propre crédit d'impôt. Lorsque ces subventions sont remboursables, elles sont ajoutées aux bases de calcul du crédit d'impôt de l'année au cours de laquelle elles sont remboursées à l'organisme qui les a versées (...) ".<br/>
<br/>
              3. Il résulte des dispositions précitées de l'article 244 quater B que seule l'entreprise qui expose les dépenses de recherche visées au II, qu'elle effectue les opérations de recherche en cause pour son propre compte ou pour le compte d'une entreprise qui lui en confie la réalisation, est, en principe, en droit de bénéficier à ce titre du crédit d'impôt qu'elles instituent. Il n'en va autrement que dans le cas prévu au d) bis du II, les dépenses exposées par l'organisme agréé au titre des opérations de recherche dont la réalisation lui est confiée entrant alors, dans les limites par ailleurs prévues par les dispositions suivantes du II, dans l'assiette du crédit d'impôt susceptible d'être demandé par l'entreprise qui lui a confié ces opérations de recherche, et étant déduites, en vertu des dispositions du III, de l'assiette du crédit d'impôt susceptible d'être demandé par l'organisme agréé. Toutefois, en l'état du droit résultant de ces dispositions, la circonstance qu'une entreprise effectue des opérations de recherche pour le compte d'un organisme agréé au titre du d) bis du II n'est pas en soi de nature à la priver du bénéfice du crédit d'impôt recherche, sauf à ce que l'administration, si elle s'y croit fondée, invoque sa participation à un montage constitutif d'une fraude à la loi. <br/>
<br/>
              4. Il résulte de ce qui précède qu'en se fondant, pour exclure tout droit des sociétés HPS et HPC au bénéfice d'un crédit d'impôt au titre des opérations de recherche qu'elles avaient effectuées, et qui leur avaient été confiées par la société HP, organisme agréé auquel la réalisation de ces travaux avait été demandée par des sociétés tierces, sur le seul motif que les sociétés HPS et HPC n'avaient pas la qualité de donneur d'ordre, la cour a commis une erreur de droit. La société Hays France est pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3000 euros à verser à la société Hays France au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 29 novembre 2018 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à la société Hays France au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Hays France et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
