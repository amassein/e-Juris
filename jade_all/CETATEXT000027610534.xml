<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027610534</ID>
<ANCIEN_ID>JG_L_2013_06_000000363279</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/61/05/CETATEXT000027610534.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 24/06/2013, 363279</TITRE>
<DATE_DEC>2013-06-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363279</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Renaud Jaune</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:363279.20130624</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 5 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'agence régionale de santé de Haute-Normandie, dont le siège est 31 rue Malouet BP 2061 à Rouen (76040 cedex) ; l'agence régionale de santé de Haute-Normandie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11DA01107 du 31 juillet 2012 par lequel la cour administrative d'appel de Douai a, sur la requête de la fédération nationale Interco CFDT, annulé le jugement n°1101038 du 12 mai 2011 par lequel le tribunal administratif de Rouen a rejeté sa demande tendant à l'annulation de la décision du directeur général de  l'agence régionale de santé de Haute-Normandie du 28 mars 2011 rejetant son recours tendant à la rectification des résultats des élections au comité d'agence du 15 mars 2011, a annulé l'attribution d'un siège à l'UNSA et attribué un siège à l'organisation SUD travail affaires sociales au sein du collège des fonctionnaires et agents publics du comité d'agence ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la fédération nationale Interco CFDT ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Renaud Jaune, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado Didier, avocat de l'Agence régionale de santé de Haute-Normandie et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la Fédération nationale Interco CFDT ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que des élections  en vue de désigner les représentants du personnel au  sein du comité d'agence de l'agence régionale de santé de Haute-Normandie se sont tenues le 15 mars 2011 ; qu'à l'issue du scrutin, le  directeur général de l'agence a constaté que, au sein du collège des fonctionnaires et agents publics, la confédération française démocratique du travail (CFDT) avait recueilli 38 % des suffrages, la confédération générale du travail (CGT) 23 %, le syndicat solidaires, unitaires, démocratiques (SUD) 13 % et l'union nationale des syndicats autonomes (UNSA) 26 % ; qu'il a, par suite, attribué deux sièges à la CFDT, un siège à la CGT et un siège à l'UNSA ; que, saisie de l'appel interjeté par la fédération nationale Interco CFDT contre le jugement du 21 mai 2011 par lequel le tribunal administratif de Rouen a rejeté sa demande tendant à ce que soit modifiée la répartition des sièges par attribution du siège attribué à l'UNSA au syndicat SUD travail - affaires sociales, la cour administrative d'appel de Douai, par un arrêt du 31 juillet 2012, contre lequel l'agence régionale de santé de Haute-Normandie se pourvoit en cassation, a jugé que la liste " UNSA " devait être regardée comme une liste commune à ce syndicat et à d'autres syndicats et a annulé l'attribution du siège à l'UNSA pour l'attribuer à SUD travail - affaires sociales ; <br/>
<br/>
              2. Considérant que les comités d'agence institués dans chaque agence régionale de santé en application de l'article L. 1432-11 du code de la santé publique comprennent le directeur général de l'agence, ou son représentant, et des représentants élus du personnel ; qu'en vertu des dispositions de l'article R. 1432-78 du même code, les personnels sont répartis entre deux collèges, le premier  comprenant les fonctionnaires et agents de droit public, le second, les personnels de droit privé ; qu'en vertu de l'article R. 1432-86 du même code, les représentants du personnel sont élus au scrutin de liste par collèges sur des listes établies par les organisations syndicales ; qu'aux termes de l'article R. 1432-121 du même code : " Pour l'appréciation de la représentativité des organisations syndicales : " 1° Le pourcentage des voix exprimées aux élections aux comités d'agence en faveur des organisations mentionnées aux articles L. 2122-1 à L. 2122-3 du code du travail s'apprécie au niveau de chacun des deux collèges ou sous-collèges pour les organisations syndicales mentionnées à l'article L. 2122-2 du même code " ; <br/>
<br/>
              3. Considérant que l'article L. 2122-3 du code du travail, qui fixe une règle de répartition des suffrages entre organisations syndicales ayant établi une liste commune pour des élections professionnelles, n'est pas applicable à l'appréciation, pour la répartition du nombre de sièges au sein du comité d'agence des agences régionales de santé, du nombre de voix obtenues par les syndicats représentant les agents de droit public ; qu'ainsi, en jugeant que le directeur général de l'agence régionale de santé de Haute-Normandie avait méconnu les dispositions de l'article L. 2122-3 du code du travail en ne répartissant pas entre les trois organisations syndicales figurant sur la liste présentée par l'UNSA les votes exprimés en faveur de cette dernière, la cour a commis une erreur de droit ; que l'agence régionale de santé est dès lors fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction, d'une part, que les bulletins de vote de la liste UNSA portaient uniquement la mention de l'UNSA et, d'autre part, que si la profession de foi de cette liste faisait état d'une union intersyndicale, elle comportait une liste de candidats qui ne précisait pas leur affiliation ; qu'il en résulte que cette liste visait à recueillir des voix au seul profit de l'UNSA et ne pouvait être regardée comme une liste commune à l'UNSA, au syndicat des médecins inspecteurs de santé publique (SMISP) et au syndicat des pharmaciens inspecteurs de santé publique (SPHISP) ; que, par suite, compte tenu des résultats obtenus et contrairement à ce que soutient la fédération requérante, un siège devait être attribué à l'UNSA et non au syndicat SUD travail - affaires sociales ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la fédération nationale Interco CFDT n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif a rejeté sa demande tendant à ce que soit modifiée la répartition des sièges par attribution du siège attribué à l'UNSA au syndicat SUD travail - affaires sociales ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'agence régionale de santé de Haute-Normandie, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que la fédération nationale Interco CFDT demande à ce titre ; <br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : L'arrêt du 31 juillet 2012 de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 : La requête de la fédération nationale Interco CFDT devant la cour administrative d'appel de Douai ainsi que ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à l'agence régionale de santé de Haute-Normandie, à la fédération nationale Interco CFDT, à la confédération générale du travail (CGT), à l'organisation solidaires, unitaires et démocratiques (SUD), à l'union nationale des syndicats autonomes (UNSA), au syndicat des médecins inspecteurs de santé publique (SMISP), au syndicat des pharmaciens inspecteurs de santé publique (SPHISP) et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-06-02 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS TECHNIQUES PARITAIRES. COMPOSITION. - COMITÉS D'AGENCE DES ARS - REPRÉSENTATION DU PERSONNEL - COLLÈGE COMPRENANT LES AGENTS DE DROIT PUBLIC - RÉPARTITION DU NOMBRE DE SIÈGES ENTRE ORGANISATIONS SYNDICALES AYANT ÉTABLI UNE LISTE COMMUNE - APPLICATION DE L'ARTICLE L. 2122-3 DU CODE DU TRAVAIL - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-09-02-02 SANTÉ PUBLIQUE. ADMINISTRATION DE LA SANTÉ. - COMITÉ D'AGENCE - REPRÉSENTATION DU PERSONNEL - COLLÈGE COMPRENANT LES AGENTS DE DROIT PUBLIC - RÉPARTITION DU NOMBRE DE SIÈGES ENTRE ORGANISATIONS SYNDICALES AYANT ÉTABLI UNE LISTE COMMUNE - APPLICATION DE L'ARTICLE L. 2122-3 DU CODE DU TRAVAIL - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 36-07-06-02 L'article L. 2122-3 du code du travail, qui fixe une règle de répartition des suffrages entre organisations syndicales ayant établi une liste commune pour des élections professionnelles, n'est pas applicable à l'appréciation, pour la répartition du nombre de sièges au sein des comités d'agence des agences régionales de santé (ARS), du nombre de voix obtenues par les syndicats représentant les agents de droit public.</ANA>
<ANA ID="9B"> 61-09-02-02 L'article L. 2122-3 du code du travail, qui fixe une règle de répartition des suffrages entre organisations syndicales ayant établi une liste commune pour des élections professionnelles, n'est pas applicable à l'appréciation, pour la répartition du nombre de sièges au sein des comités d'agence des agences régionales de santé, du nombre de voix obtenues par les syndicats représentant les agents de droit public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 15 mai 2012, Fédération nationale CGT des personnels des organismes sociaux et Fédération interco CFDT, n°s 340106 343618, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
