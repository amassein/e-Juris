<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029604147</ID>
<ANCIEN_ID>JG_L_2014_10_000000365936</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/41/CETATEXT000029604147.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 15/10/2014, 365936</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365936</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365936.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés sous le n° 365936 les 11 février 2013, 13 mai 2013, 25 mars 2014 et 10 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, le Comité des artistes auteurs plasticiens et le Syndicat national des sculpteurs et plasticiens " artistes visuels " demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 25 janvier 2013 du ministre de la culture et de la communication fixant la composition du conseil de gestion de la section particulière des artistes auteurs au sein du fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs ;  <br/>
<br/>
              2°) de mettre à la charge de l'Etat la  somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés sous le n° 365940 les 11 février 2013, 13 mai 2013 et 25 mars 2014 au secrétariat du contentieux du Conseil d'Etat, les mêmes requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2012-1370 du 7 décembre 2012 relatif à la formation professionnelle tout au long de la vie des artistes auteurs et au financement de l'action sociale, en modulant dans le temps les effets de l'annulation du décret ;  <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu :<br/>
              - les autres pièces des dossiers ;<br/>
              - la Constitution ; <br/>
              - le code de la sécurité sociale ;  <br/>
              - le code du travail ; <br/>
              - la loi n° 2011-1978 du 28 décembre 2011 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;  <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du Comité des artistes auteurs plasticiens et du Syndicat des sculpteurs et plasticiens " artistes visuels ".<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 89 de la loi du 28 décembre 2011 a introduit dans le code du travail les articles L. 6331-65 à L. 6331-68, qui mettent à la charge des artistes auteurs et des diffuseurs le versement de contributions destinées au financement d'actions de formation continue au bénéfice de ces artistes et prévoient la faculté, pour les sociétés de versement de droits d'auteurs, de contribuer volontairement à ce financement ; que ces contributions sont recouvrées, en vertu des ces dispositions, par les organismes, mentionnés aux articles L. 382-4 et  L. 382-5 du code de la sécurité sociale, agréés pour le recouvrement des cotisations sociales des artistes auteurs ; que selon les mêmes dispositions législatives, les contributions sont ensuite reversées à l'organisme paritaire collecteur agréé (OPCA) au titre de la formation professionnelle des artistes salariés, qui est le fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS), et gérées au sein de ce dernier dans une " section particulière " dont " un décret en Conseil d'Etat détermine les modalités d'organisation et de fonctionnement " ; <br/>
<br/>
              2. Considérant que, pour l'application de ces dispositions, est intervenu le décret du 7 décembre 2012 relatif à la formation professionnelle tout au long de la vie des artistes auteurs et au financement de l'action sociale, qui détermine notamment, à l'article R. 331-64 du code du travail, les modalités d'organisation et de fonctionnement de la section particulière créée pour la gestion des nouvelles contributions ; que, par ailleurs, en raison de l'absence de signature, dans un délai d'un mois, de l'accord auquel renvoie le nouvel article R. 6331-64 du code du travail, le ministre de la culture et de la communication a, par un arrêté du 25 janvier 2013, et en application de l'article 4 du décret du 7 décembre 2012, fixé la composition du conseil de gestion de la section particulière de l'AFDAS ; que par les requêtes visées ci-dessus, qu'il y a lieu de joindre dès lors qu'elles présentent à juger des questions semblables, le Comité des artistes auteurs plasticiens et le Syndicat national des sculpteurs et plasticiens " artistes visuels " doivent être regardés comme demandant l'annulation pour excès de pouvoir, d'une part, de l'article 2 du décret du 7 décembre 2012 en tant qu'il introduit l'article R. 6331-64 dans le code du travail, ainsi que de l'article 4 de ce décret et, d'autre part, de l'arrêté du 25 janvier 2013 ; <br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de la culture et de la communication : <br/>
<br/>
              3. Considérant que selon l'article 13 des statuts du Comité des artistes auteurs plasticiens, le conseil d'administration de cette association " décide, le cas échéant, des actions en justice " ; qu'il ressort des pièces du dossier que, par une délibération du 27 janvier 2013, le conseil d'administration de l'association a décidé d'engager des recours dirigés contre le décret du 7 décembre 2012 et l'arrêté du 25 janvier 2013 ; que le président de l'association représente celle-ci en justice, selon l'article 14 des mêmes statuts ; que, par suite, la fin de non-recevoir tirée de ce que le signataire des requêtes n'aurait pas qualité pour agir doit être écartée ; <br/>
<br/>
              Sur la requête tendant à l'annulation de certaines dispositions du décret du 7 décembre 2012 : <br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 6331-64 du code du travail, issu de l'article 2 du décret attaqué : " I. Il est créé au sein de l'organisme paritaire collecteur agréé mentionné à l'article L. 6331-55 une section particulière chargée de gérer les contributions mentionnées à l'article L. 6331-65 du présent code. / II. Le conseil d'administration de l'organisme paritaire collecteur agréé arrête, sur proposition du conseil de gestion de la section mentionnée au I, les services et actions de formation susceptibles d'êtres financés, les priorités, les critères et les conditions de prise en charge des demandes de formation présentées par les artistes auteurs. A défaut de proposition, le conseil d'administration délibère valablement sur ces questions. / III. Le conseil de gestion de la section mentionnée au I est composé de représentants : / 1° Des organisations professionnelles représentant les artistes auteurs ; / 2° Des organisations professionnelles représentant les diffuseurs ; / 3° Des représentants des sociétés d'auteurs contribuant au financement. (...) / IV.  La répartition en nombre de sièges entre les organisations professionnelles représentant les artistes auteurs, d'une part, et des diffuseurs, d'autre part, est déterminée en fonction du montant de leur contribution respective. Un accord entre les organisations professionnelles des artistes auteurs, des diffuseurs et les sociétés d'auteurs détermine le nombre de sièges de représentants et la durée de leur mandat ainsi que la répartition en nombre de sièges au sein des trois collèges " ; <br/>
<br/>
              5. Considérant, en premier lieu, que le législateur a prévu, à l'article L. 6331-68 du code du travail, que les contributions seraient gérées au sein de l'organisme paritaire collecteur agréé au titre des contributions versées au titre des salariés intermittents du spectacle, dans une section particulière ; que l'article R. 6331-64 du même code ne permet au conseil d'administration de cet organisme d'arrêter les décisions relatives à la formation professionnelle des artistes auteurs que sur proposition du conseil de gestion de la section particulière, sauf en cas de carence de ce dernier ; qu'en donnant, dans ces conditions, compétence au conseil d'administration pour adopter les décisions prises en matière de formation professionnelle des artistes auteurs, le décret attaqué ne méconnaît pas les dispositions législatives mentionnées ci-dessus ; qu'il n'est pas davantage entaché, sur ce point, d'erreur manifeste d'appréciation, le conseil de gestion de la section particulière étant composé de représentants des organisations professionnelles représentant les artistes auteurs et les diffuseurs, ainsi que de représentants des sociétés d'auteurs ; <br/>
<br/>
              6. Considérant, en second lieu, que l'article L. 6331-68 du code du travail a confié à un décret en Conseil d'Etat le soin de déterminer les modalités d'organisation et de fonctionnement de la section particulière de l'organisme paritaire collecteur agréé au sein de laquelle sont gérées les contributions au financement de la formation continue des artistes auteurs ; que l'article R. 6331-64 du code du travail se borne, d'une part, à prévoir la création d'un " conseil de gestion " composé de représentants des organisations professionnelles représentant les artistes auteurs, de représentants des organisations professionnelles représentant les diffuseurs et de représentants des sociétés d'auteurs contribuant au financement et, d'autre part, à indiquer que la répartition des sièges entre les représentants des organisations professionnelles d'artistes auteurs et les représentants des organisations de diffuseurs est fonction des montants respectifs de leurs contributions ; qu'il ne détermine pas le critère de répartition des sièges entre ces deux premières catégories et celle des représentants des sociétés d'auteurs, ni celui de la répartition des sièges au sein de chacun des trois collèges ;  qu'il renvoie à un accord entre les trois catégories de financeurs, sans préciser les conditions de validité de cet accord, la détermination du nombre de sièges des représentants, la durée de leur mandat ainsi que la répartition en nombre de sièges au sein des trois collèges ; que, dans ces conditions, le renvoi à un accord pour préciser la composition du conseil de gestion de la section particulière, auquel procède le IV de l'article R. 6331-64 introduit dans le code du travail par l'article 2 du décret attaqué, est entaché d'illégalité  ; <br/>
<br/>
              7. Considérant que l'article 4 du décret attaqué prévoit l'intervention d'un arrêté du ministre chargé de la culture, à défaut de signature de l'accord mentionné au IV de l'article R. 6331-64 du code du travail dans le mois qui suit la publication du décret ; que ses dispositions ne sont pas divisibles de celles du IV de l'article R. 6331-64 du code du travail, issu de l'article 2  du décret ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête dirigés contre les mêmes dispositions, le Comité des artistes auteurs plasticiens et le Syndicat national des sculpteurs et plasticiens " artistes visuels " sont fondés à demander l'annulation des seuls articles 2, en tant qu'il introduit le IV l'article R. 6331-64 dans le code du travail, et 4 du décret attaqué ;<br/>
<br/>
              Sur la requête tendant à l'annulation de l'arrêté du 25 janvier 2013 du ministre de la culture et de la communication, et sans qu'il soit besoin d'examiner les autres moyens : <br/>
<br/>
              9. Considérant que l'arrêté du 25 janvier 2013 a été pris pour l'application de l'article 4 du décret du 7 décembre 2012 ; que, par suite, l'annulation de cet article entraîne, par voie de conséquence, celle de l'arrêté du 25 janvier 2013 ;  <br/>
<br/>
              Sur les conséquences de l'annulation de certaines dispositions du décret du 7 décembre 2012 ainsi que de l'arrêté du 25 janvier 2013 : <br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier, en particulier des réponses des parties à la mesure d'instruction ordonnée par la première sous-section chargée de l'instruction de l'affaire, que l'annulation rétroactive des dispositions, mentionnées ci-dessus, du décret du 7 décembre 2012, ainsi que de l'arrêté du 25 janvier 2013 aurait pour effet de rendre illégaux de nombreux actes pris sur le fondement de ces dispositions en matière de formation des artistes auteurs, par la section particulière du fonds d'assurance formation des secteurs de la culture, de la communication et des loisirs (AFDAS) ; qu'au regard des effets manifestement excessifs qu'emporterait une telle annulation rétroactive, il y a lieu de prévoir que l'annulation prononcée par la présente décision ne prendra effet que le 1er janvier 2015 et que, sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur leur fondement, les effets produits par les dispositions attaquées antérieurement à leur annulation seront réputés définitifs ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement au Comité des artistes auteurs plasticiens et au Syndicat national des sculpteurs et plasticiens " artistes visuels " d'une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du décret du 7 décembre 2012, en tant qu'il introduit le IV de l'article R. 6331-64 dans le code du travail, l'article 4 de ce décret, ainsi que l'arrêté du 25 janvier 2013 du ministre de la culture et de la communication sont annulés. Cette annulation prendra effet le 1er janvier 2015.<br/>
Article 2 : Sous réserve des actions contentieuses engagées à la date de la présente décision contre des actes pris sur leur fondement, les effets produits par le IV de l'article R. 6331-64 du code du travail, l'article 4 du décret du 7 décembre 2012, ainsi que par l'arrêté du 25 janvier 2013, antérieurement à leur annulation, sont réputés définitifs.<br/>
Article 3 : L'Etat versera au Comité des artistes auteurs plasticiens et au Syndicat national des sculpteurs et plasticiens " artistes visuels " une somme de 1 5000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus de la requête du Comité des artistes auteurs plasticiens et du Syndicat national des sculpteurs et plasticiens " artistes visuels " dirigée contre le décret du 7 décembre 2012 est rejeté.<br/>
Article 5 : La présente décision sera notifiée au Comité des artistes auteurs plasticiens, au Syndicat national des sculpteurs et plasticiens " artistes visuels ", au Premier ministre, à la ministre de la culture et de la communication et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MESURES À PRENDRE PAR DÉCRET. - DISPOSITION LÉGISLATIVE RENVOYANT AU DÉCRET EN CONSEIL D'ETAT LE SOIN DE FIXER LES MODALITÉS D'ORGANISATION ET DE FONCTIONNEMENT DE LA SECTION PARTICULIÈRE CRÉÉE, AU SEIN DE L'OPCA DES ARTISTES SALARIÉS, POUR LA FORMATION CONTINUE DES ARTISTES AUTEURS - DÉCRET PRÉVOYANT L'EXISTENCE D'UN CONSEIL DE GESTION DONT IL ARRÊTE LES CATÉGORIES DE MEMBRES, MAIS RENVOYANT À UN ACCORD ENTRE ORGANISATIONS REPRÉSENTATIVES DES ARTISTES AUTEURS, DES DIFFUSEURS ET DES SOCIÉTÉS D'AUTEURS LE SOIN DE PRÉCISER LE NOMBRE DE CES MEMBRES, LA DURÉE DE LEUR MANDAT ET LA RÉPARTITION DES SIÈGES - POSSIBILITÉ D'UN TEL RENVOI DANS SON PRINCIPE - EXISTENCE - LÉGALITÉ DU RENVOI EN L'ESPÈCE - ABSENCE, FAUTE POUR LE DÉCRET DE PRÉCISER LES CONDITIONS DE VALIDITÉ DE CET ACCORD.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-09-04 TRAVAIL ET EMPLOI. FORMATION PROFESSIONNELLE. PARTICIPATION DES EMPLOYEURS AU DÉVELOPPEMENT DE LA FORMATION PROFESSIONNELLE CONTINUE. - DISPOSITION LÉGISLATIVE RENVOYANT AU DÉCRET EN CONSEIL D'ETAT LE SOIN DE FIXER LES MODALITÉS D'ORGANISATION ET DE FONCTIONNEMENT DE LA SECTION PARTICULIÈRE CRÉÉE, AU SEIN DE L'OPCA DES ARTISTES SALARIÉS, POUR LA FORMATION CONTINUE DES ARTISTES AUTEURS - DÉCRET PRÉVOYANT L'EXISTENCE D'UN CONSEIL DE GESTION DONT IL ARRÊTE LES CATÉGORIES DE MEMBRES, MAIS RENVOYANT À UN ACCORD ENTRE ORGANISATIONS REPRÉSENTATIVES DES ARTISTES AUTEURS, DES DIFFUSEURS ET DES SOCIÉTÉS D'AUTEURS LE SOIN DE PRÉCISER LE NOMBRE DE CES MEMBRES, LA DURÉE DE LEUR MANDAT ET LA RÉPARTITION DES SIÈGES - POSSIBILITÉ D'UN TEL RENVOI DANS SON PRINCIPE - EXISTENCE - LÉGALITÉ DU RENVOI EN L'ESPÈCE - ABSENCE, FAUTE POUR LE DÉCRET DE PRÉCISER LES CONDITIONS DE VALIDITÉ DE CET ACCORD.
</SCT>
<ANA ID="9A"> 01-02-02-02 L'article L. 6331-68 du code du travail a confié à un décret en Conseil d'Etat le soin de déterminer les modalités d'organisation et de fonctionnement de la section particulière de l'organisme paritaire collecteur agréé (OPCA) des artistes salariés au sein de laquelle sont gérées les contributions au financement de la formation continue des artistes auteurs.,,,L'article R. 6331-64 du code du travail, pris pour l'application de ces dispositions, se borne, d'une part, à prévoir la création d'un  conseil de gestion  composé de représentants des organisations professionnelles représentant les artistes auteurs, de celles représentant les diffuseurs et de représentants des sociétés d'auteurs contribuant au financement et, d'autre part, à indiquer que la répartition des sièges entre les représentants des organisations professionnelles d'artistes auteurs et les représentants des organisations de diffuseurs est fonction des montants respectifs de leurs contributions. En revanche, il ne détermine pas le critère de répartition des sièges entre ces deux premières catégories et celle des représentants des sociétés d'auteurs, ni celui de la répartition des sièges au sein de chacun des trois collèges, et renvoie à un accord entre les trois catégories de financeurs, sans préciser les conditions de validité de cet accord, la détermination du nombre de sièges des représentants, la durée de leur mandat ainsi que la répartition en nombre de sièges au sein des trois collèges.,,,Dans ces conditions, le renvoi à un accord pour préciser la composition du conseil de gestion de la section particulière, auquel procède le IV de l'article R. 6331-64 introduit dans le code du travail par l'article 2 du décret attaqué, est entaché d'illégalité.</ANA>
<ANA ID="9B"> 66-09-04 L'article L. 6331-68 du code du travail a confié à un décret en Conseil d'Etat le soin de déterminer les modalités d'organisation et de fonctionnement de la section particulière de l'organisme paritaire collecteur agréé (OPCA) des artistes salariés au sein de laquelle sont gérées les contributions au financement de la formation continue des artistes auteurs.,,,L'article R. 6331-64 du code du travail, pris pour l'application de ces dispositions, se borne, d'une part, à prévoir la création d'un  conseil de gestion  composé de représentants des organisations professionnelles représentant les artistes auteurs, de celles représentant les diffuseurs et de représentants des sociétés d'auteurs contribuant au financement et, d'autre part, à indiquer que la répartition des sièges entre les représentants des organisations professionnelles d'artistes auteurs et les représentants des organisations de diffuseurs est fonction des montants respectifs de leurs contributions. En revanche, il ne détermine pas le critère de répartition des sièges entre ces deux premières catégories et celle des représentants des sociétés d'auteurs, ni celui de la répartition des sièges au sein de chacun des trois collèges, et renvoie à un accord entre les trois catégories de financeurs, sans préciser les conditions de validité de cet accord, la détermination du nombre de sièges des représentants, la durée de leur mandat ainsi que la répartition en nombre de sièges au sein des trois collèges.,,,Dans ces conditions, le renvoi à un accord pour préciser la composition du conseil de gestion de la section particulière, auquel procède le IV de l'article R. 6331-64 introduit dans le code du travail par l'article 2 du décret attaqué, est entaché d'illégalité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
