<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044254929</ID>
<ANCIEN_ID>JG_L_2021_10_000000457294</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/25/49/CETATEXT000044254929.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 25/10/2021, 457294, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457294</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:457294.20211025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 6 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat Action et Démocratie demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du 3° et du 10° de l'article 1er du décret n° 2021-1059 du 7 août 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de la requête ; <br/>
              - la requête est recevable dès lors qu'il justifie d'un intérêt à agir et que son président a qualité pour agir ;<br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, les personnels enseignants, administratifs et psychologues de l'éducation nationale soumis à l'obligation vaccinale avaient jusqu'au 15 septembre 2021 pour présenter un certificat médical de contre-indication et que, d'autre part, les motifs de contre-indication non prévus par le décret litigieux ne sont pas retenus par le médecin de prévention du rectorat de l'académie et ne peuvent être inscrits dans le formulaire Cerfa destiné à cet effet ; <br/>
              - les dispositions contestées méconnaissent la loi n° 2021-1040 du 5 août 2021, pour l'application de laquelle le décret a été pris, en ce qu'elles fixent limitativement les cas de contre-indications médicales faisant obstacle à la vaccination contre la Covid-19 ;<br/>
              - ces dispositions sont constitutives d'un abus de pouvoir dès lors que le texte de la loi ne mentionne aucun décret déterminant les cas de contre-indications médicales reconnus ;<br/>
              - le 1° du I de l'annexe 2 du décret contesté méconnaît le principe de précaution en ce qu'il limite les contre-indications à celles qui sont mentionnées au point 4.3 du résumé des caractéristiques du produit, sans tenir compte du risque d'interaction entre les substances médicamenteuses résultant de l'administration du vaccin à des personnes prenant d'autres médicaments ; <br/>
              - les dispositions contestées constituent une discrimination manifeste de la part d'une autorité publique fondée sur l'état de santé de la personne et une atteinte au droit à la protection de la santé ; <br/>
              - le décret, en ce qu'il fixe a priori et de manière définitive les contre-indications médicales qui permettent de considérer que l'obligation vaccinale est satisfaite pour les personnes qui y sont soumises, méconnaît l'avis de la Haute Autorité de santé du 4 août 2021, qui indique la nécessité d'actualiser une telle liste en fonction de la position des autorités compétentes en matière de pharmacovigilance. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. L'article 12 de la loi du 5 août 2021 relative à la gestion de la crise sanitaire prévoit que doivent être vaccinées contre la covid-19, sauf contre-indication médicale reconnue, les personnes dont le I établit la liste. Le II dispose qu'un décret, pris après avis de la Haute Autorité de santé, " détermine les conditions de vaccination contre la covid-19 des personnes mentionnées au I ", lequel, notamment, " fixe les éléments permettant d'établir un certificat de statut vaccinal pour les personnes mentionnées au même I et les modalités de présentation de ce certificat sous une forme ne permettant d'identifier que la nature de celui-ci et la satisfaction aux critères requis ". <br/>
<br/>
              3. L'article 1er du décret du 7 août 2021 modifiant le décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire, pris pour l'application de l'article 12 de la loi du 5 août 2021 présenté au point précédent, a introduit dans le décret du 1er juin 2021 un article 2-4 disposant que " les cas de contre-indication médicale faisant obstacle à la vaccination contre la covid-19 et permettant la délivrance du document pouvant être présenté dans les cas prévus au 2° du A du II de l'article 1er de la loi du 31 mai 2021 susvisée sont mentionnés à l'annexe 2 du présent décret " et que " l'attestation de contre-indication médicale est remise à la personne concernée par un médecin ". Le 10° de cet article 1er insère l'annexe 2, qui prévoit que : " I.- Les cas de contre-indication médicale faisant obstacle à la vaccination contre la covid-19 mentionnés à l'article 2-4 sont : 1° Les contre-indications inscrites dans le résumé des caractéristiques du produit (RCP) : - antécédent d'allergie documentée (avis allergologue) à un des composants du vaccin en particulier polyéthylène-glycols et par risque d'allergie croisée aux polysorbates ; - réaction anaphylaxique au moins de grade 2 (atteinte au moins de 2 organes) à une première injection d'un vaccin contre le COVID posée après expertise allergologique ; - personnes ayant déjà présenté des épisodes de syndrome de fuite capillaire (contre-indication commune au vaccin Vaxzevria et au vaccin Janssen). "<br/>
<br/>
              4. Dès lors que l'article 12 de la loi du 5 août 2021 a renvoyé au pouvoir réglementaire la détermination des conditions de vaccination contre la covid-19 des personnes soumises à l'obligation de vaccination, le pouvoir réglementaire a légalement pu fixer une liste limitative des contre-indications médicales faisant obstacle à la vaccination contre la covid-19, sans la laisser à l'appréciation de chaque médecin, cette liste ayant fait l'objet de l'avis de la Haute Autorité de santé du 4 août 2021 et reposant sur des éléments objectifs. Par suite, les moyens tirés de ce que les dispositions mentionnées au point précédent méconnaîtraient la loi du 5 août 2021 et l'avis de la Haute autorité de santé et, en tout état de cause, constitueraient un " abus de pouvoir " et une discrimination manifeste fondée sur l'état de santé de la personne et méconnaîtraient le principe de précaution et celui de droit à la santé, ne sont pas de nature, en l'état de l'instruction, à créer un doute sérieux sur leur légalité. <br/>
<br/>
              5. Il résulte de ce qui précède qu'il est manifeste qu'est mal fondée la demande introduite par le syndicat Action et Démocratie, sans qu'il soit besoin de statuer sur l'autre condition posée par l'article L. 521-1 du code de justice administrative. Il y a donc lieu de rejeter les conclusions présentées par ce dernier, y compris celles introduites au titre de l'article L. 761-1 du code de justice administrative, par la procédure prévue à l'article L. 522-3 de ce même code. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête du syndicat Action et Démocratie est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée au syndicat Action et Démocratie.<br/>
Fait à Paris, le 25 octobre 2021.<br/>
    Signé : Damien Botteghi<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
