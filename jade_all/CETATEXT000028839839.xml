<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028839839</ID>
<ANCIEN_ID>JG_L_2014_04_000000364192</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/98/CETATEXT000028839839.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 09/04/2014, 364192</TITRE>
<DATE_DEC>2014-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364192</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364192.20140409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 30 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'Association des centres distributeurs Edouard Leclerc, dont le siège est situé 26, quai Marcel Boyer à Ivry-sur-Seine (94200) ; l'Association des centres distributeurs Edouard Leclerc demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision n° 12-DCC-125 de l'Autorité de la concurrence du 27 août 2012 relative à la prise de contrôle conjoint de 28 magasins de commerce de détail à dominante alimentaire par l'Union des coopérateurs d'Alsace et l'Association des centres distributeurs E. Leclerc en tant qu'elle comporterait une décision la déclarant en situation de contrôle conjoint de 43 sociétés d'exploitation de magasins Leclerc et en situation de contrôle exclusif de la société Val Expansion ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 mars 2014, présentée pour l'Association des centres distributeurs Edouard Leclerc ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de l'Association Des Centres Distributeurs E. Leclerc et à la SCP Baraduc, Duhamel, Rameix, avocat de la Autorité de la concurrence ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que l'Union des coopérateurs d'Alsace, la société Val Expansion et la Société coopérative d'approvisionnement d'Alsace ont notifié le 22 juin 2012 à l'Autorité de la concurrence un protocole d'accord portant sur l'acquisition, par des filiales de la société Val Expansion, de 60 % du capital et des droits de vote de la société Foncière Hypercoop et de 33,88 % du capital et des droits de vote de la société Hypercoop ; que, d'une part, s'agissant des sociétés acquéreuses, le capital de la société Val Expansion est détenu par 50 sociétés d'exploitation de magasins bénéficiant de l'enseigne Leclerc en Alsace et en Moselle et celui de la Société coopérative d'approvisionnement d'Alsace, qui est la centrale d'achat régionale des magasins exploités sous l'enseigne Leclerc, est notamment détenu par 43 des 50 sociétés actionnaires de la société Val Expansion ; que, d'autre part, s'agissant des sociétés cibles, la société Foncière Hypercoop détenait les actifs immobiliers et la société Hypercoop exploitait six hypermarchés et 22 supermarchés sous l'enseigne Leclerc en Alsace et en Moselle ; que, par une décision du 27 août 2012, l'Autorité de la concurrence a autorisé cette opération de concentration ; que l'Association des centres distributeurs Edouard Leclerc (ACDLec), qui n'était pas partie au protocole d'accord, mais qui avait été invitée par le service instructeur de l'Autorité de la concurrence à notifier conjointement avec les parties à ce protocole l'opération de concentration, demande l'annulation pour excès de pouvoir d'une décision autonome, distincte de la décision d'autorisation, que révèleraient, selon elle, les motifs de cette dernière et qui consisterait en une prise de position de l'Autorité de la concurrence la déclarant en situation de contrôle conjoint de 43 sociétés d'exploitation de magasins exploités sous l'enseigne Leclerc et en situation de contrôle exclusif de la société Val Expansion ;<br/>
<br/>
              2. Considérant que les appréciations que l'Autorité de la concurrence porte, dans les motifs de la décision par laquelle elle statue sur la demande d'autorisation d'une opération de concentration, sur l'exercice, par des personnes physiques ou morales autres que les parties notifiantes, d'un contrôle sur ces mêmes parties afin, s'il y a lieu, de tenir compte, dans l'analyse des effets anticoncurrentiels de l'opération sur les marchés pertinents qu'elle a identifiés, de l'activité de l'ensemble des personnes concernées par l'opération ne sont pas détachables du dispositif de cette décision, dont elles constituent le soutien ; qu'ainsi, les appréciations de l'Autorité de la concurrence selon lesquelles l'Association des centres distributeurs Edouard Leclerc se trouverait en situation de contrôle conjoint de 43 sociétés d'exploitation de magasins exploités sous l'enseigne Leclerc et de contrôle exclusif de la société Val Expansion ne sauraient être regardées comme constituant une décision distincte de la décision par laquelle l'Autorité de la concurrence a autorisé l'opération de concentration et qui serait susceptible de faire l'objet d'un recours pour excès de pouvoir ; que la requête de l'Association des centres distributeurs Edouard Leclerc est irrecevable et doit, par suite, être rejetée ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Association des centres distributeurs Edouard Leclerc la somme de 3 000 euros, à verser à l'Autorité de la concurrence au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association des centres distributeurs Edouard Leclerc est rejetée.<br/>
Article 2 : L'Association des centres distributeurs Edouard Leclerc versera à l'Autorité de la concurrence une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'Association des centres distributeurs Edouard Leclerc et à l'Autorité de la concurrence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-05-005 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. DÉFENSE DE LA CONCURRENCE. AUTORITÉ DE LA CONCURRENCE. - APPRÉCIATIONS PORTÉES PAR L'AUTORITÉ DE LA CONCURRENCE SUR LE CONTRÔLE EXERCÉ PAR UNE ENTREPRISE AUTRE QUE LES PARTIES NOTIFIANTES DANS LES MOTIFS D'UNE DÉCISION D'AUTORISATION DE CONCENTRATION - DÉCISION SUSCEPTIBLE DE RECOURS - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - APPRÉCIATIONS PORTÉES PAR L'AUTORITÉ DE LA CONCURRENCE SUR LE CONTRÔLE EXERCÉ PAR UNE ENTREPRISE AUTRE QUE LES PARTIES NOTIFIANTES DANS LES MOTIFS D'UNE DÉCISION D'AUTORISATION DE CONCENTRATION [RJ1].
</SCT>
<ANA ID="9A"> 14-05-005 Les appréciations que l'Autorité de la concurrence porte, dans les motifs de la décision par laquelle elle statue sur la demande d'autorisation d'une opération de concentration, sur l'exercice, par des personnes physiques ou morales autres que les parties notifiantes, d'un contrôle sur ces mêmes parties afin, s'il y a lieu, de tenir compte, dans l'analyse des effets anticoncurrentiels de l'opération sur les marchés pertinents qu'elle a identifiés, de l'activité de l'ensemble des personnes concernées par l'opération, ne sont pas détachables du dispositif de cette décision, dont elles constituent le soutien. Elles ne sauraient être regardées comme constituant une décision distincte de la décision par laquelle l'Autorité de la concurrence a autorisé l'opération de concentration et qui serait susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-02 Les appréciations que l'Autorité de la concurrence porte, dans les motifs de la décision par laquelle elle statue sur la demande d'autorisation d'une opération de concentration, sur l'exercice, par des personnes physiques ou morales autres que les parties notifiantes, d'un contrôle sur ces mêmes parties afin, s'il y a lieu, de tenir compte, dans l'analyse des effets anticoncurrentiels de l'opération sur les marchés pertinents qu'elle a identifiés, de l'activité de l'ensemble des personnes concernées par l'opération, ne sont pas détachables du dispositif de cette décision, dont elles constituent le soutien. Elles ne sauraient être regardées comme constituant une décision distincte de la décision par laquelle l'Autorité de la concurrence a autorisé l'opération de concentration et qui serait susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 11 octobre 2012, Société Casino Guichard-Perrachon, n° 357193, p. 361 ; CE, 1er octobre 2012, Société ITM Entreprises et autres, n°s 346378 346444, p. 359.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
