<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806229</ID>
<ANCIEN_ID>JG_L_2021_12_000000450005</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806229.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/12/2021, 450005, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450005</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SARL MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450005.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... A... a porté plainte contre Mme F... D... devant la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des médecins. Par une décision du 27 septembre 2019, la chambre disciplinaire de première instance a prononcé à l'encontre de Mme D... la sanction de l'interdiction d'exercer la médecine pendant une durée d'un mois, assortie du sursis.<br/>
<br/>
              Par une décision du 22 décembre 2020, la chambre disciplinaire nationale de l'ordre des médecins a, sur appel de Mme D..., annulé cette décision et rejeté la plainte de Mme A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 22 février, 25 mai, 8 octobre et 24 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme D... ;<br/>
<br/>
              3°) de mettre à la charge de Mme D... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de Mme A... et à la SCP Richard, avocat de Mme D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... a porté plainte contre Mme D..., médecin spécialiste, qualifié en médecine générale, devant la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des médecins. Par une décision du 27 septembre 2019, la chambre disciplinaire de première instance a infligé à Mme D... la sanction de l'interdiction d'exercer la médecine pendant une durée d'un mois, assortie du sursis. Mme A... se pourvoit en cassation contre la décision du 22 décembre 2020 par laquelle la chambre disciplinaire nationale de l'ordre des médecins a annulé cette décision et rejeté sa plainte.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'appui de la plainte qu'elle avait formée contre Mme D... et transmise par le conseil départemental de la ville de Paris de l'ordre des médecins à la chambre disciplinaire de première instance, Mme A... avait notamment formulé des griefs, d'ailleurs repris dans le mémoire en défense qu'elle a produit en appel, tirés de la méconnaissance par Mme D... de ses obligations déontologiques d'élaborer son diagnostic avec le plus grand soin, en y consacrant le temps nécessaire et d'assurer à ses patients des soins consciencieux et dévoués, résultant des articles R. 4127-32 et R. 4127-33 du code de la santé publique. Or la chambre disciplinaire nationale de l'ordre des médecins ne se prononce dans la décision attaquée que sur deux griefs parmi ceux soulevés, respectivement tirés de la méconnaissance de l'obligation d'assurer la continuité des soins dispensés aux malades, résultant de l'article R. 4127-47 du code de la santé publique et de la méconnaissance des dispositions de l'article R. 4127-3 du code de la santé publique. Par suite, Mme A... est fondée à soutenir que cette décision est insuffisamment motivée. <br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, que Mme A... est fondée à demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins qu'elle attaque.<br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des médecins du 22 décembre 2020 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la chambre disciplinaire nationale de l'ordre des médecins.<br/>
Article 3 : Les conclusions de Mme A... et de Mme D... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme C... A... et à Mme F... D....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins. <br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Fabienne Lambolez, conseillère d'Etat et M. Edouard Solier, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
 		Le rapporteur : <br/>
      Signé : M. Edouard Solier<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
