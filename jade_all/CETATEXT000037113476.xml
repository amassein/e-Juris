<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037113476</ID>
<ANCIEN_ID>JG_L_2018_06_000000393881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/11/34/CETATEXT000037113476.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 27/06/2018, 393881, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:393881.20180627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 31 mai 2016, le Conseil d'Etat, statuant au contentieux sur le pourvoi de M. B...tendant à l'annulation de l'arrêt du 28 mai 2015 par lequel la cour administrative d'appel de Versailles a annulé le jugement du tribunal administratif de Montreuil du 8 juin 2012 et a remis à sa charge la cotisation supplémentaire d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 2007 ainsi que les majorations correspondantes, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions de savoir :<br/>
<br/>
              1° Si les dispositions de l'article 8 de la directive du 23 juillet 1990 doivent être interprétées en ce sens qu'elles interdisent, dans le cas d'une opération d'échange de titres entrant dans le champ de la directive, un mécanisme de report d'imposition prévoyant que, par dérogation à la règle selon laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, une plus-value d'échange est constatée et liquidée à l'occasion de l'opération d'échange de titres et est imposée l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange.<br/>
<br/>
              2° Si les dispositions de l'article 8 de la directive du 23 juillet 1990 doivent être interprétées en ce sens qu'elles interdisent, dans le cas d'une opération d'échange de titres entrant dans le champ de la directive, que la plus-value d'échange de titres, à la supposer imposable, soit taxée par l'Etat de la résidence du contribuable au moment de l'opération d'échange, alors que celui-ci, à la date de la cession des titres reçus à l'occasion de cet échange à laquelle la plus-value d'échange est effectivement imposée, a transféré son domicile fiscal dans un autre Etat membre.<br/>
<br/>
              Par un arrêt n° C-327/16 et C-421/16 du 22 mars 2018, la Cour de justice de l'Union européenne s'est prononcée sur ces questions.<br/>
<br/>
              Par un mémoire, enregistré le 16 avril 2018, le ministre de l'action et des comptes publics maintient ses conclusions.<br/>
<br/>
              Par un mémoire, enregistré le 25 mai 2018, M. B...a indiqué maintenir ses conclusions. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 31 mai 2016 ;<br/>
              Vu :<br/>
              - le Traité instituant la Communauté européenne ;<br/>
              - le Traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 90/434/CE du Conseil du 23 juillet 1990 ;<br/>
              - la convention entre la France et la Belgique, tendant à éviter les doubles impositions et à établir les règles d'assistance administrative et juridique réciproque en matière d'impôts sur les revenus signée à Bruxelles le 10 mars 1964 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 99-1172 du 30 décembre 1999 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 22 mars 2018, Marc B...c/ Ministre des finances et des comptes publics (C-327/16) et Ministre des finances et des comptes publics c/ Marc Lassus (C-421/16) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de M. B...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 juin 2018, présentée par M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Dans l'arrêt du 22 mars 2018 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que :<br/>
<br/>
              - l'article 8 de la directive 90/434/CEE du Conseil, du 23 juillet 1990, concernant le régime fiscal commun applicable aux fusions, scissions, apports d'actifs et échanges d'actions intéressant des sociétés d'États membres différents, telle que modifiée par l'acte relatif aux conditions d'adhésion du Royaume de Norvège, de la République d'Autriche, de la République de Finlande et du Royaume de Suède, adapté par la décision 95/1/CE, Euratom, CECA du Conseil de l'Union européenne, du 1er janvier 1995, doit être interprété en ce sens qu'il ne s'oppose pas à une législation d'un État membre en vertu de laquelle la plus-value issue d'une opération d'échange de titres relevant de cette directive est constatée à l'occasion de cette opération, mais son imposition est reportée jusqu'à l'année au cours de laquelle intervient l'évènement mettant fin à ce report d'imposition, en l'occurrence la cession des titres reçus en échange ; <br/>
<br/>
              - l'article 8 de la directive 90/434, telle que modifiée par l'acte relatif aux conditions d'adhésion du Royaume de Norvège, de la République d'Autriche, de la République de Finlande et du Royaume de Suède, adapté par la décision 95/1, doit être interprété en ce sens qu'il ne s'oppose pas à une législation d'un État membre qui prévoit l'imposition de la plus-value afférente à une opération d'échange de titres, placée en report d'imposition, lors de la cession ultérieure des titres reçus en échange, alors même que cette cession ne relève pas de la compétence fiscale de cet État membre.<br/>
<br/>
              2. La cour administrative d'appel de Versailles n'a pas commis d'erreur de droit en jugeant que les dispositions du II de l'article 92 B et du I ter de l'article 160 du code général des impôts instituent un mécanisme de report d'imposition dont le seul effet est de permettre, par dérogation à la règle suivant laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, de constater et de liquider la plus-value d'échange l'année de sa réalisation et de l'imposer l'année au cours de laquelle intervient l'événement qui met fin à ce report d'imposition, qui peut notamment être la cession des titres reçus au moment de l'échange. Cette règle de report est conforme au droit de l'Union, ainsi que l'a jugé la Cour de justice de l'Union européenne, que la cession ultérieure des titres interviennent en France ou dans un  autre Etat de l'Union.<br/>
<br/>
              3. En second lieu, l'article 18 de la convention fiscale conclue entre la France et la Belgique du 10 mars 1964, laquelle ne comporte aucune disposition relative aux gains en capital, stipule que " Dans la mesure où les articles précédents de la présente convention n'en disposent pas autrement, les revenus des résidents de l'un des États contractants ne sont imposables que dans cet État ". Pour rejeter les conclusions de M.B..., la cour a jugé que ces stipulations ne faisaient pas obstacle à ce que l'Etat d'imposition de la plus-value résultant de l'échange de titres soit celui dont le contribuable était résident au moment du fait générateur que constitue sa réalisation. Dans la mesure, d'une part, où les gains tirés de l'échange de titre réalisé en 1996 doivent être regardés comme des revenus au sens de ces stipulations et où, d'autre part, il ressort des pièces du dossier soumis aux juges du fond que la résidence de M. B... se situait en France à la date à laquelle la plus-value d'échange a été réalisée, c'est sans commettre d'erreur de droit que la cour a jugé que la convention fiscale ne faisait pas obstacle à ce que M. B... soit assujetti, à raison de cette plus-value d'échange, à une cotisation supplémentaire d'impôt sur le revenu assortie des majorations correspondantes au titre de l'année 2007.<br/>
<br/>
              4. Il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
