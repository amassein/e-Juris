<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028987564</ID>
<ANCIEN_ID>JG_L_2014_05_000000359876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/98/75/CETATEXT000028987564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 26/05/2014, 359876, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359876.20140526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Bastia d'annuler pour excès de pouvoir la décision du 5 avril 2011 par laquelle le directeur général des finances publiques a refusé de prendre en compte son ancienneté dans le secteur privé de 1973 à 1981, ainsi que de la décision implicite de rejet de son recours gracieux du 30 mai 2011, et d'enjoindre au ministre chargé du budget de prendre en compte cette ancienneté, dans le délai d'un mois à compter de la notification du jugement, sous astreinte de 150 euros par jour de retard.<br/>
<br/>
              Par un jugement n° 1100698 du 29 mars 2012, le tribunal administratif de Bastia a rejeté sa demande.<br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er juin 2012, 10 juillet 2012 et 11 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler ce jugement n° 1100698 du tribunal administratif de Bastia du 29 mars 2012 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que :<br/>
              - le jugement a été rendu par un magistrat incompétent, faute de publication de la décision le désignant ;<br/>
              - en estimant que le ministre du budget avait pris en compte les services effectués dans le secteur privé entre 1973 et 1981, le tribunal administratif a dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              Par un mémoire en défense, enregistré le 14 août 2013, le ministre de l'économie et des finances conclut au rejet du pourvoi.<br/>
<br/>
              Il soutient que les moyens du pourvoi ne sont pas fondés.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le décret n° 2005-1228 du 29 septembre 2005 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes du II de l'article 5 du décret du 29 septembre 2005 relatif à l'organisation des carrières des fonctionnaires de catégorie C : " Les personnes nommées fonctionnaires dans un grade de catégorie C doté des échelles de rémunération 3, 4 ou 5 qui ont, ou qui avaient eu auparavant, la qualité d'agent de droit privé d'une administration, ou qui travaillent ou ont travaillé en qualité de salarié dans le secteur privé ou associatif, sont classées avec une reprise d'ancienneté de travail égale à la moitié de sa durée, le cas échéant après calcul de conversion en équivalent temps plein. Ce classement est opéré sur la base de la durée moyenne de chacun des échelons du grade dans lequel ils sont intégrés ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge du fond que MmeB..., recrutée dans la fonction publique de l'Etat à compter du 1er février 2002 puis nommée agent de recouvrement du Trésor et reclassée comme agent administratif du Trésor public de 1e classe, a demandé au directeur général des finances publiques de prendre en considération, pour son reclassement dans ces deux derniers corps, sa durée de travail en qualité de salariée dans le secteur privé, en se prévalant d'au moins 68 trimestres d'assurance au régime général, hors périodes de chômage, entre 1973 et 2002. Par la décision contestée du 5 avril 2011, confirmée sur recours gracieux, le directeur général des finances publiques lui a fait savoir que les services privés accomplis antérieurement à sa nomination dans le corps des agents de recouvrement du Trésor ne pouvaient être pris en considération que partiellement, en l'absence de production de contrats de travail, certificats de travail ou bulletins de salaire permettant de calculer l'équivalent temps plein de certaines des périodes travaillées. La décision mentionnait une durée de services privés rappelés à hauteur de la moitié de la durée de 9 ans, 5 mois et 2 jours. Par suite, en déduisant de cette mention qu'avaient été pris en compte les services effectués par Mme B... dans le secteur privé " pendant 9 ans 5 mois et 2 jours, entre 1973 et 1981 ", le tribunal administratif de Bastia a dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que Mme B...est fondée à demander l'annulation du jugement qu'elle attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 500 euros qu'elle demande au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Bastia du 29 mars 2012 est annulé.<br/>
Article 2 : L'Etat versera à Mme B...la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
