<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143049</ID>
<ANCIEN_ID>JG_L_2020_06_000000420703</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/30/CETATEXT000042143049.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 10/06/2020, 420703, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420703</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; CABINET MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Eric Thiers</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:420703.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Grenoble de condamner la maison de l'intercommunalité de Haute-Tarentaise à lui verser la somme de 5 622 euros avec intérêts au taux légal correspondant à la rémunération de durées de trajet entre les divers sites de l'école de musique intercommunale au sein de laquelle il a enseigné entre 2010 et 2013. Par un jugement n° 1303237 du 10 novembre 2015, le tribunal administratif a condamné l'établissement public de coopération intercommunale à lui verser une somme de 2 896,02 euros avec intérêts au taux légal à compter du 21 juin 2013.<br/>
<br/>
              Par un arrêt n° 16LY00090 du 5 décembre 2017, la cour administrative d'appel de Lyon a, sur appel de la maison de l'intercommunalité de Haute-Tarentaise et sur appel incident de M. B..., réformé ce jugement en ramenant à 186,84 euros le montant de la somme à verser à M. B....  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 mai et 20 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la maison de l'intercommunalité de Haute-Tarentaise ;<br/>
<br/>
              3°) de mettre à la charge de la maison de l'intercommunalité de Haute-Tarentaise la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Thiers, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M. B... et à la SCP Coutard, Munier-Apaire, avocat de la maison de l'intercommunalite de Haute-tarentaise ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un jugement du 10 novembre 2015, le tribunal administratif de Grenoble a condamné la maison de l'intercommunalité de Haute-Tarentaise à verser à M. B... la somme de 2 896,02 euros avec intérêts au taux légal, correspondant à la rémunération des durées de trajets accomplis par ce dernier entre les divers sites de l'école de musique intercommunale au sein de laquelle il a enseigné entre 2010 et 2013. Par un arrêt du 5 décembre 2017 contre lequel M. B... se pourvoit en cassation, la cour administrative d'appel de Lyon a réformé ce jugement en ramenant à 186,84 euros la somme à verser au demandeur par la maison de l'intercommunalité de Haute-Tarentaise.     <br/>
<br/>
              		2. Aux termes de l'article 7-1 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les règles relatives à la définition, à la durée et à l'aménagement du temps de travail des agents des collectivités territoriales et des établissements publics mentionnés au premier alinéa de l'article 2 sont fixées par la collectivité ou l'établissement, dans les limites applicables aux agents de l'Etat, en tenant compte de la spécificité des missions exercées par ces collectivités ou établissements (...) ". Le premier alinéa de l'article 2 de cette loi prévoit que " Les dispositions de la présente loi s'appliquent aux personnes qui, régies par le titre Ier du statut général des fonctionnaires de l'Etat et des collectivités territoriales, ont été nommées dans un emploi permanent et titularisées dans un grade de la hiérarchie administrative des communes, des départements, des régions ou des établissements publics en relevant, à l'exception des agents comptables des caisses de crédit municipal ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que M. B... était employé par la maison de l'intercommunalité de Haute-Tarentaise entre 2010 et 2013 en qualité d'agent non titulaire, sous couvert d'un contrat à durée indéterminée. Par suite, en faisant application à sa situation des dispositions citées au point 2, applicables aux seuls fonctionnaires de la fonction publique territoriale, la cour administrative d'appel de Lyon a commis une erreur de droit. M. B... est donc fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de son arrêt.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la maison de l'intercommunalité de Haute-Tarentaise la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B... qui, dans la présente instance, n'est pas la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 5 décembre 2017 est annulé.<br/>
Article 2 : Le jugement de l'affaire est renvoyé à la cour administrative d'appel de Lyon. <br/>
Article 3 : La maison de l'intercommunalité de Haute-Tarentaise versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la maison de l'intercommunalité de Haute-Tarentaise présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A... B... et à la maison de l'intercommunalité de Haute-Tarentaise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
