<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036506395</ID>
<ANCIEN_ID>JG_L_2018_01_000000398671</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/50/63/CETATEXT000036506395.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 17/01/2018, 398671, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-01-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398671</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:398671.20180117</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 23 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, le syndicat secondaire Le Signal demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 14BX03289 de la cour administrative d'appel de Bordeaux du 9 février 2016, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 561-1 du code de l'environnement. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la Constitution, notamment son préambule et son article 61-1 ;<br/>
              -	le code de l'environnement ;<br/>
              -	la loi n° 95-101 du 2 février 1995 ;<br/>
              -	la loi n° 2010-788 du 12 juillet 2010 ; <br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat du syndicat secondaire Le Signal.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'État (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              2. Considérant qu'en vertu du premier alinéa de l'article L. 561-1 du code de l'environnement, dans sa rédaction applicable au litige : " Sans préjudice des dispositions prévues au 5° de l'article L. 2212-2 et à l'article L. 2212-4 du code général des collectivités territoriales, lorsqu'un risque prévisible de mouvements de terrain, ou d'affaissements de terrain dus à une cavité souterraine ou à une marnière, d'avalanches, de crues torrentielles ou à montée rapide ou de submersion marine menace gravement des vies humaines, l'Etat peut déclarer d'utilité publique l'expropriation par lui-même, les communes ou leurs groupements, des biens exposés à ce risque, dans les conditions prévues par le code de l'expropriation pour cause d'utilité publique et sous réserve que les moyens de sauvegarde et de protection des populations s'avèrent plus coûteux que les indemnités d'expropriation. " ; que le syndicat secondaire Le Signal soutient que ces dispositions, si elles ont pour portée d'exclure de leur champ d'application les risques liés à l'érosion côtière, méconnaissent le principe d'égalité devant la loi et le droit de propriété garantis respectivement par les articles 6 et 17 de la Déclaration des droits de l'homme et du citoyen, ainsi que le principe de prévention garanti par l'article 3 de la Charte de l'environnement ; <br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées de l'article L. 561-1 du code de l'environnement, éclairées notamment par les travaux préparatoires de la loi du 2 février 1995 relative au renforcement de la protection de l'environnement et de la loi du 12 juillet 2010 portant engagement national pour l'environnement dont elles sont issues, que le législateur n'a pas entendu étendre le régime d'expropriation qu'elles instituent aux risques liés à l'érosion côtière, lesquels ne sont assimilables ni aux risques de submersion marine, ni, par eux-mêmes, aux risques de mouvements de terrain, mentionnés dans cet article ; <br/>
<br/>
              4. Considérant que la cour administrative d'appel de Bordeaux, par l'arrêt attaqué, a confirmé le rejet de la demande du Syndicat des copropriétaires de l'immeuble " Le Signal ", situé boulevard du Front de mer à Soulac-sur-Mer, tendant à l'annulation de la décision implicite du préfet de la Gironde refusant d'engager la procédure d'expropriation prévue par les dispositions précitées de l'article L. 561-1 du code de l'environnement en raison du risque d'effondrement de cet immeuble consécutivement à un phénomène d'érosion côtière ; que ces dispositions doivent par suite être regardées comme applicables au présent litige ; qu'elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que la question de leur conformité aux droits et libertés garantis par la Constitution, notamment aux principes d'égalité et au droit de propriété garantis respectivement par les articles 6 et 17 de la Déclaration des droits de l'homme et du citoyen, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du premier alinéa de l'article L. 561-1 du code de l'environnement est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur le pourvoi du syndicat secondaire Le Signal.<br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat secondaire Le Signal et au ministre d'Etat, ministre de la transition écologique et solidaire. <br/>
Copie en sera adressée à la commune de Soulac-sur-Mer, à la communauté de communes de la Pointe du Médoc et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
