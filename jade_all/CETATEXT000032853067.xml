<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032853067</ID>
<ANCIEN_ID>JG_L_2016_07_000000394846</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/85/30/CETATEXT000032853067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 06/07/2016, 394846, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394846</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:394846.20160706</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E...F...et M. B...D..., candidats aux élections qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Bastia 3 en vue de l'élection des conseillers départementaux du département de la Haute-Corse, ont, notamment, demandé au tribunal administratif de Bastia d'annuler ces opérations électorales, à l'issue desquelles ont été élus Mme G...A...et M. B...C.... Par un déféré, le préfet de la Haute-Corse a demandé au tribunal d'annuler la feuille de proclamation annexée au procès-verbal afférent au scrutin des opérations électorales qui se sont tenues le 22 mars 2015. Par un jugement nos 1500273, 1500303 du 23 octobre 2015, le tribunal administratif de Bastia a annulé ces opérations électorales. <br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 26 novembre et 24 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...et M. C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la protestation de Mme F...et de M. D...et valider les opérations électorales qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Bastia 3 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de Mme F...et M.D..., conjointement ou séparément, le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de Mme G...A...et de M. B...C...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'à l'issue du second tour du scrutin organisé les 22 et 29 mars 2015 en vue de l'élection du binôme de conseillers départementaux du canton de Bastia 3 (Haute-Corse), Mme A...et M. C...ont été proclamés élus ; que, par un jugement du 23 octobre 2015, le tribunal administratif de Bastia, faisant droit à la protestation de Mme F... et M.D..., a annulé ces opérations électorales au motif que le nombre de 159 suffrages irréguliers à retirer du total de voix obtenu par le binôme élu excédait l'écart de 137 le séparant du total de voix obtenu par l'autre binôme présent au second tour, composé de Mme F...et de M.D... ; que Mme A...et M. C... relèvent appel de ce jugement ;  <br/>
<br/>
              2. Considérant qu'aux termes du troisième alinéa de l'article L.  62-1 du code électoral : " Le vote de chaque électeur est constaté par sa signature apposée à l'encre en face de son nom sur la liste d'émargement " ; qu'il résulte de ces dispositions, destinées à assurer la sincérité des opérations électorales, que seule la signature personnelle, à l'encre, d'un électeur est de nature à apporter la preuve de sa participation au scrutin, sauf cas d'impossibilité dûment reporté sur la liste d'émargement ; qu'ainsi, la constatation d'une signature qui présente des différences manifestes entre les deux tours de scrutin sans qu'il soit fait mention d'un vote par procuration sur la liste d'émargement ne peut être regardée comme garantissant l'authenticité du vote ;<br/>
<br/>
              3. Considérant que le juge de l'élection ne vérifie la régularité que des seuls émargement précisément contestés devant lui ; que, devant le tribunal administratif, 174 émargements étaient expressément contestés ; qu'il ressort de l'examen des listes d'émargement et des attestations produites, devant les premiers juges, par les requérants que, sur les 159 signatures regardées, par le tribunal administratif, comme présentant des différences manifestes entre les deux tours de scrutin, seules 141 présentent effectivement de telles différences ; que, d'une part, les signatures correspondant aux suffrages des électeurs enregistrés sous les numéros 567 dans le bureau de vote n° 16, 780 dans le bureau de vote n° 18 et 861 dans le bureau de vote n° 22 n'étaient pas expressément contestées ; que, d'autre part, il résulte de l'instruction que les attestations fournies permettent de regarder comme régulièrement émis les suffrages exprimés par les électeurs ayant voté sous les numéros 349 dans le bureau de vote n° 15, 611 dans le bureau de vote n° 16 et 272 dans le bureau de vote n° 20 ; qu'en outre, les signatures correspondant aux votes numéros 225, 295, 642, 675, 555, 671 et 780 dans le bureau de vote n° 16, numéros 34, 166 et 444 dans le bureau de vote n° 17, numéros 103, 104 et 528 dans le bureau de vote n° 20, numéros 219 dans le bureau de vote n° 21 et numéros 2, 378, 73 dans le bureau de vote n° 22 ne sont pas manifestement différentes entre les deux tours ; qu'en revanche, les signatures numéros 157 et 450 dans le bureau de vote n° 15, et numéros 136, 170 et 356 dans le bureau de vote n° 21 présentent de telles différences ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'il convient de retrancher 141 suffrages du total des voix obtenues par Mme A...et M. C...au second tour de l'élection du binôme de conseillers départementaux dans le canton de Bastia 3 ; que ce nombre est supérieur à la différence de voix entre les deux binômes lors du second tour ; que, dans ces conditions, les requérants ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Bastia a annulé ces opérations électorales ; que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de Mme A...et de M. C...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme G...A..., M. B... C..., Mme E...F...et M. B...D.... <br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
