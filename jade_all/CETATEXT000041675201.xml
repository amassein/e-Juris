<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041675201</ID>
<ANCIEN_ID>JG_L_2020_03_000000426104</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/67/52/CETATEXT000041675201.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 02/03/2020, 426104, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426104</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426104.20200302</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 23 janvier 2017 par laquelle l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile.<br/>
<br/>
              Par une décision n° 17013802 du 5 octobre 2018, le Cour nationale du droit d'asile a annulé la décision de l'Office français de protection des réfugiés et apatrides et lui a accordé le bénéfice de la protection subsidiaire.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 décembre 2018 et 28 février 2019 au secrétariat du contentieux du Conseil d'Etat, l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de renvoyer l'affaire à la Cour nationale du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides et au Cabinet Briard, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : a) La peine de mort ou une exécution ; b) La torture ou des peines ou traitements inhumains ou dégradants ; c) S'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international ". Selon l'article L. 712-2 du même code : " La protection subsidiaire n'est pas accordée à une personne s'il existe des raisons sérieuses de penser : (...) / d) Que son activité sur le territoire constitue une menace grave pour l'ordre public, la sécurité publique ou la sûreté de l'Etat (...) ".<br/>
<br/>
              2. En premier lieu, en relevant que le conflit armé en Syrie atteignait un niveau si élevé de violence aveugle qu'il ne permettait pas aux autorités d'offrir une protection à un civil qui y serait renvoyé et en en déduisant que M. B... courrait, s'il y était renvoyé, un risque réel de subir une menace grave au sens du c) de l'article L. 712-1 précité, la Cour nationale du droit d'asile, devant laquelle la qualité de civil de M. B... n'était pas contestée, a suffisamment motivé sa décision et a porté sur les faits qui lui étaient soumis une appréciation souveraine, exempte de dénaturation.<br/>
<br/>
              3. En second lieu, en jugeant qu'en dépit de la sympathie exprimée par M. B..., notamment sur internet, à l'égard de groupes armés opposés au régime syrien parmi lesquels certains sont djihadistes, les pièces au dossier qui lui était soumis ne permettaient pas de caractériser de raisons sérieuses de penser que son activité sur le territoire français constituait une menace grave pour l'ordre public, la sécurité publique ou la sûreté de l'Etat au sens du d de l'article L. 712-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, compte tenu notamment de l'absence d'aucun signalement ou mesure de surveillance décidée à son encontre, la Cour nationale du droit d'asile, qui n'a pas exclu par principe que l'expression d'opinions personnelles puisse être prise en compte pour caractériser l'existence de telles raisons sérieuses, n'a pas commis d'erreur de droit ni inexactement qualifié les faits qui lui était soumis.<br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi de l'Office français de protection des réfugiés et apatrides doit être rejeté. M. B... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que le Cabinet Briard, avocat de M. B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre la somme de 3 000 euros à la charge de l'Office français de protection des réfugiés et apatrides. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Le pourvoi de l'Office français de protection des réfugiés et apatrides est rejeté.<br/>
Article 2 : L'Office français de protection des réfugiés et apatrides versera la somme de 3 000 euros à la SARL Cabinet Briard, avocat de M. B..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
