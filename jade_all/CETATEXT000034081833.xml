<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081833</ID>
<ANCIEN_ID>JG_L_2017_02_000000390139</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081833.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 24/02/2017, 390139, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390139</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE DE LA BURGADE ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:390139.20170224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Limeux a demandé au tribunal administratif d'Amiens de condamner solidairement le Groupement Agricole d'Exercice en Commun (GAEC)A..., MM. B...et C...A...et la société Tereos France - Union de coopératives agricoles, à lui verser une somme de 20 846,20 euros en réparation des dommages occasionnés au chemin communal n° 303, dit " chemin des Pluettes ", en application de l'article L. 141-9 du code de la voirie routière et, dans l'hypothèse où le tribunal ne s'estimerait pas suffisamment informé, d'ordonner toute mesure d'expertise utile. Par un jugement avant dire droit n° 1102498 du 26 novembre 2013, le tribunal administratif d'Amiens a ordonné une expertise afin de rassembler tous les éléments permettant d'appréhender la consistance et le coût des détériorations imputables à un usage anormal de cette voie.<br/>
<br/>
              Par un arrêt n° 14DA00316 du 31 mars 2015, la cour administrative d'appel de Douai a rejeté l'appel formé par la société Tereos France - Union de coopératives agricoles contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 mai 2015, 6 août 2015 et 19 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la société Tereos France - Union de coopératives agricoles demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Limeux la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de la voirie routière, notamment son article L. 141-9 ; <br/>
              - le code rural et de la pêche maritime, notamment son article L.161-8 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange de la Burgade, avocat de la société Tereos France - Union de cooperatives agricoles et à la SCP Hémery, Thomas-Raquin, avocat de la commune de Limeux.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 31 janvier 2017, présentée par la société Tereos France - Union de coopératives agricoles.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 141-9 du code de la voirie routière : " Toutes les fois qu'une voie communale entretenue à l'état de viabilité est habituellement ou temporairement soit empruntée par des véhicules dont la circulation entraîne des détériorations anormales, soit dégradée par des exploitations de mines, de carrières, de forêts ou de toute autre entreprise, il peut être imposé aux entrepreneurs ou propriétaires des contributions spéciales, dont la quotité est proportionnée à la dégradation causée. / Ces contributions peuvent être acquittées en argent ou en prestation en nature et faire l'objet d'un abonnement. / A défaut d'accord amiable, elles sont fixées annuellement sur la demande des communes par les tribunaux administratifs, après expertise, et recouvrées comme en matière d'impôts directs  ". Aux termes de l'article L. 161-8 du code rural et de la pêche maritime : " Des contributions spéciales peuvent, dans les conditions prévues pour les voies communales par l'article L. 141-9 du code de la voirie routière, être imposées par la commune (...) aux propriétaires ou entrepreneurs responsables des dégradations apportées aux chemins ruraux ". Il résulte de ces dispositions que les communes qui entendent imposer aux entrepreneurs ou propriétaires des contributions spéciales sont tenues de rechercher au préalable un accord amiable avec les intéressés. Cette prescription devant être conciliée avec le principe du règlement annuel de ces contributions, posé par les mêmes dispositions, les demandes de règlement pour lesquelles l'administration justifie qu'elle a engagé, avant l'expiration de l'année suivant celle où se sont produites les dégradations en cause, des pourparlers en vue d'aboutir à un accord amiable avec l'entrepreneur ou le propriétaire, ne sont recevables devant les tribunaux administratifs que si elles ont été présentées avant l'expiration de l'année civile suivant celle à partir de laquelle la tentative d'accord amiable doit être regardée comme ayant définitivement échoué.<br/>
<br/>
              2. En premier lieu, il résulte de ces mêmes dispositions que si elles prévoient la possibilité, pour les entrepreneurs ou les propriétaires, de demander à acquitter les contributions spéciales qui leur sont imposées sous forme de prestations en nature, une telle possibilité ne constitue ni une garantie pour les intéressés, ni un choix laissé à leur entière convenance et  suppose un accord de la commune. Par suite, la commune n'est pas tenue, au cours des pourparlers qu'elle engage en vue de rechercher un accord amiable avec l'entrepreneur ou le propriétaire, de mettre celui-ci en demeure d'indiquer s'il préfère acquitter la contribution en argent ou sous forme de  prestations en nature. Ainsi, la société n'est pas fondée à soutenir que la cour aurait entaché son arrêt d'erreur de droit en jugeant que la commune de Limeux était recevable à saisir le tribunal administratif sans avoir recherché si elle avait préalablement mis en demeure l'entrepreneur ou le propriétaire d'exprimer sa préférence pour une réparation en argent ou en nature, dès lors qu'une telle mise en demeure n'était pas nécessaire pour que la commune puisse être regardée comme ayant effectivement tenté d'aboutir à un accord amiable.<br/>
<br/>
              3. En deuxième lieu, il ressort des pièces du dossier soumis aux juges du fond que la société Tereos France - Union de coopératives agricoles soulevait, dans ses écritures devant le tribunal administratif d'Amiens, une fin de non-recevoir tirée du défaut de qualité pour agir de la commune de Limeux, du fait du transfert à la communauté de communes de la région d'Hallencourt de sa compétence en matière de voirie. Toutefois, la commune de Limeux faisait valoir, en défense, que le chemin communal n° 303 dit " des Pluettes ", n'était pas au nombre  des voies d'intérêt communautaire dont la gestion avait été transférée. En en déduisant qu'il y avait lieu d'écarter la fin de  non-recevoir soulevée par la société Tereos France - Union de coopératives agricoles, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              4. En troisième lieu, en relevant, d'une part, que la commune de Limeux avait fait dresser un procès-verbal d'huissier le 28 décembre 2009 constatant les dommages occasionnés au chemin dit " des Pluettes " lors de l'opération d'enlèvement d'un dépôt de betteraves à sucre le 25 décembre 2009 sur la parcelle de MM. A...et, d'autre part, que si ce document faisait apparaître un lien entre les dégradations observées et l'utilisation du chemin par la société, l'état du dossier ne permettait pas de confirmer l'état de ce chemin et les modalités de son entretien et en ordonnant, en conséquence, une expertise afin de déterminer les causes des dégradations constatées, la cour n'a pas jugé qu'un lien de causalité était établi entre les dommages occasionnés au chemin des Pluettes et le passage des poids lourds de la société. Ainsi, le moyen tiré de ce que la cour aurait insuffisamment motivé son arrêt, faute d'expliquer en quoi le procès-verbal d'huissier du 28 décembre 2009 permettait d'attester d'un tel lien de causalité, doit être écarté.<br/>
<br/>
              5. En quatrième lieu, sont notamment redevables de la contribution spéciale prévue par les dispositions de l'article L. 141-9 du code de la voirie routière,  les entrepreneurs ayant utilisé des véhicules dont le passage sur la voie communale a entraîné une détérioration anormale. Par suite, en jugeant que la société ne pouvait utilement se prévaloir de ce qu'elle n'était ni propriétaire, ni gardienne des véhicules lourds utilisés et en relevant que la société ne contestait pas avoir contracté avec MM. A... afin d'assurer, sous sa propre responsabilité, la prestation d'enlèvement des betteraves de la parcelle appartenant à ces derniers pour en déduire qu'elle était redevable de la contribution litigieuse, la cour n'a, contrairement à ce que soutient la société requérante, commis ni erreur de droit, ni erreur de qualification juridique. <br/>
<br/>
              6. Il résulte de ce qui précède que le pourvoi de la société Tereos France doit être rejeté.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Tereos France - Union de coopératives agricoles la somme de 3 000 euros à verser à la commune de Limeux, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions  font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Limeux qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Tereos France - Union de coopératives agricoles est rejeté.<br/>
<br/>
Article 2 : La société Tereos France - Union de coopératives agricoles versera une somme de 3 000 euros à la commune de Limeux au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Tereos France - Union de coopératives agricoles et à la commune de Limeux.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
