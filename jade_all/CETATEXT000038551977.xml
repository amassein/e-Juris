<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038551977</ID>
<ANCIEN_ID>JG_L_2019_06_000000423435</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/55/19/CETATEXT000038551977.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 04/06/2019, 423435, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423435</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423435.20190604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              La société anonyme (SA) Clinique de l'Alliance a demandé au tribunal administratif d'Orléans de prononcer le remboursement de crédits de taxe sur la valeur ajoutée de 50 636 et 32 238 euros au titre, respectivement, des périodes correspondant aux exercices clos en 2013 et 2014, et la réduction correspondante des cotisations de taxe sur les salaires auxquelles elle a été assujettie au titre des années 2013 et 2014, à concurrence, respectivement, de 35 582 euros et 39 189 euros. Par un jugement n° 1504172 du 17 janvier 2017, ce tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17NT00974 du 29 juin 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé  par cette société contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 21 août 2018, 19 novembre 2018 et 9 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la socièté Clinique de l'Alliance demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2006/112/CE du Conseil du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - les arrêts de la Cour de justice de l'Union européenne C- 366/12 du 13 mars 2014, C-88/09 du 11 février 2010 et C-111/05 du 29 mars 2007;<br/>
              - l'arrêté du 4 avril 2005 pris en application de l'article L. 162-22-7 du code de la sécurité sociale et fixant la liste des spécialités pharmaceutiques prises en charge par l'assurance maladie en sus des prestations d'hospitalisation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP WAQUET, FARGE, HAZAN, avocat de la Société Clinique De L'alliance ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 mai 2019, présentée par la société Clinique de l'Alliance ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société anonyme (SA) Clinique de l'Alliance, établissement hospitalier privé à but lucratif, a formé des réclamations en matière de taxe sur la valeur ajoutée et de taxe sur les salaires au titre des années 2013 et 2014 au motif que les livraisons de médicaments cytostatiques dans le cadre de traitements ambulatoires contre le cancer avaient été exonérées à tort de taxe sur la valeur ajoutée. L'administration fiscale a rejeté ces réclamations. La société Clinique de l'Alliance a alors demandé au tribunal administratif d'Orléans de prononcer le remboursement de crédits de taxe sur la valeur ajoutée dont elle estimait disposer à l'expiration des périodes correspondant aux exercices clos en 2013 et 2014, en conséquence de la majoration des droits à déduction en résultant, et la réduction correspondante des cotisations de taxe sur les salaires auxquelles elle a été assujettie au titre des années 2013 et 2014. Elle se pourvoit en cassation contre l'arrêt du 29 juin 2018 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'elle avait formé contre le jugement du 17 janvier 2017 par lequel le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 132 de la directive du 28 novembre 2006 relative au système commun de taxe sur la valeur ajoutée, applicable à compter du 1er  janvier 2007, qui reprend l'article 13 A de la sixième directive du 17 mai 1977 : " 1. Sans préjudice d'autres dispositions communautaires, les Etats membres exonèrent: (...) b) l'hospitalisation et les soins médicaux, ainsi que les opérations qui leur sont étroitement liées, assurés par des organismes de droit public ou, dans des conditions sociales comparables à celles qui valent pour ces derniers, par des établissements hospitaliers, des centres de soins médicaux et de diagnostic et d'autres établissements de même nature dûment reconnus ; ; / c) les prestations de soins à la personne effectuées dans le cadre de l'exercice des professions médicales et paramédicales telles qu'elles sont définies par l'État membre concerné (...) ". Aux termes de l'article 256 du code général des impôts, dans sa rédaction applicable au litige : " I. Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel. (...) ". Aux termes de l'article 261 du même code, dans sa rédaction applicable à la période d'imposition en litige : " Sont exonérés de la taxe sur la valeur ajoutée : (...) 4. (...) / 1° Les soins dispensés aux personnes par les membres de professions médicales et paramédicales réglementées (...) / 1° bis Les frais d'hospitalisation et de traitement, y compris les frais de mise à disposition d'une chambre individuelle, dans les établissements de santé privés titulaires de l'autorisation mentionnée à l'article L. 712-8 du code de santé publique ". <br/>
<br/>
              3. Il découle de l'arrêt Finanzamt Dortmund-West c. Klinikum Dortmund GmbH de la Cour de justice de l'Union européenne du 13 mars 2014 qu'une livraison de biens de médicaments cytostatiques, prescrits et administrés à un patient dans le cadre d'un traitement ambulatoire contre le cancer par des médecins exerçant à titre indépendant au sein d'un hôpital ou d'un établissement de soins de santé privé, ne relève pas du champ d'application des dispositions du 1° bis de l'article 261 du code général des impôts, qui assurent la transposition de celles de l'article 13, A, paragraphe 1, sous b, de la sixième directive du 17 mai 1977, concernant exclusivement les soins médicaux délivrés au cours d'une hospitalisation et les opérations qui leur sont étroitement liées. Cette livraison ne peut être exonérée de la taxe sur la valeur ajoutée qu'en vertu des dispositions du 1° du 4 de l'article 261 du code général des impôts, prises pour assurer la transposition de l'article 13, A, paragraphe 1, sous c, sous la condition d'être matériellement et économiquement indissociable de la prestation de soins médicaux principale.<br/>
<br/>
              4. Il résulte de ce qui précède qu'en jugeant que les livraisons de médicaments cytostatiques étaient exonérées de taxe sur la valeur ajoutée en application des dispositions du 1° bis du 4 de l'article 261 du code général des impôts, interprétées à la lumière du b du paragraphe 1 de l'article 132 de la directive 2006/112/CE, et non en application du 1° du 4 du même article, la cour administrative d'appel a commis une erreur de droit. Dès lors, la société Clinique de l'Alliance est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur les conclusions tendant à la restitution des crédits de taxe sur la valeur ajoutée au titre des périodes correspondant aux exercices clos en 2013 et 2014 :<br/>
<br/>
              En ce qui concerne la loi fiscale :<br/>
<br/>
              6. Ainsi qu'il a été dit au point 3, l'exonération de taxe sur la valeur ajoutée de la livraison de médicaments cytostatiques prescrits par un médecin exerçant à titre indépendant dans un établissement de santé privé titulaire de l'autorisation mentionnée à l'article L. 6122-1 du code de la santé publique, sur le fondement du 1° du 4 de l'article 261 du code général des impôts, est subordonnée à la condition que cette opération soit matériellement et économiquement indissociable de la prestation de soins médicaux principale. <br/>
<br/>
              7. D'une part, d'un point de vue matériel, l'Institut national du cancer est chargé, en application de l'article L. 1415-2 du code de la santé publique, dans sa version applicable aux impositions en litige, de coordonner les actions de lutte contre le cancer, notamment en définissant, au titre du 2° de cet article, des référentiels de bonnes pratiques et de prise en charge en cancérologie ainsi que des critères d'agrément des établissements et des professionnels de santé pratiquant la cancérologie. En application de ces dispositions et de l'article D. 1415-1-9 de ce code, le conseil d'administration de l'Institut a fixé, par un avis publié le 16 juin 2008, les critères d'agrément auxquels les établissements pratiquant la cancérologie doivent satisfaire conformément aux dispositions du 3° de l'article R. 6123-88 du même code, dans sa rédaction alors applicable. Selon ces critères, l'oncologue détermine pour chaque patient, en concertation avec l'équipe médicale, un programme personnalisé de soins comprenant un protocole d'administration des médicaments. Ce programme fixe le calendrier prévisionnel des consultations, des examens médicaux, de l'administration des médicaments cytostatiques, ainsi que les modalités de surveillance et de prise en charge des effets secondaires en cours de traitement. Les médicaments, préparés par la pharmacie à usage intérieur de l'établissement, sont exclusivement administrés au patient par l'équipe médicale, sur prescription de l'oncologue et sous le contrôle étroit de ce dernier. Par suite, la livraison au patient des médicaments cytostatiques, qui ne constitue pas une fin en soi pour ce dernier, doit être regardée comme indispensable à la réalisation de la prestation de soins par l'oncologue. <br/>
<br/>
              8. Il s'ensuit que la prestation de l'oncologue exerçant à titre libéral dans un établissement de soins privé, d'une part, et la délivrance de médicaments cytostatiques par la pharmacie de l'établissement de santé, d'autre part, sont matériellement indissociables dans le cadre du traitement du cancer par chimiothérapie.<br/>
<br/>
              9. D'autre part, d'un point de vue économique, il résulte d'une jurisprudence constante de la Cour de justice de l'Union européenne, et notamment de ses arrêts Aktiebolaget NN du 29 mars 2007 et Graphic procédé du 11 février 2010 que, pour apprécier si une livraison de biens et une prestation de services constituent une prestation unique, il convient de se placer du point de vue du consommateur moyen.<br/>
<br/>
               10. En vertu des dispositions de l'arrêté du 4 avril 2005 pris en application de l'article L. 162-22-7 du code de la sécurité sociale, les médicaments cytostatiques dispensés dans un établissement de santé privé titulaire de l'autorisation mentionnée à l'article L. 6122-1 du code de la santé publique font l'objet d'une prise en charge directe par l'assurance maladie. En application du 3° de l'article L. 322-3 du code de la sécurité sociale alors applicable, aujourd'hui repris au 4° de l'article L. 160-14 de ce code, la participation de l'assuré à la prise en charge des frais de santé est supprimée, dans le cadre d'un forfait global de soins, lorsque le bénéficiaire a été reconnu atteint d'une tumeur maligne ou d'une affection maligne du tissu lymphatique ou hématopoïétique. Aux termes de ces différentes dispositions, le patient pris en charge dans le cadre d'un traitement ambulatoire contre le cancer par un médecin oncologue exerçant à titre libéral dans le cadre d'un établissement de santé privé à caractère lucratif ne supporte pas le coût des médicaments cytostatiques qui lui sont administrés dans le cadre des soins de chimiothérapie. <br/>
<br/>
              11. Il en résulte en tout état de cause que, du point de vue du patient, la prestation de l'oncologue exerçant à titre libéral dans un établissement de soins privé et la délivrance de médicaments cytostatiques par la pharmacie de l'établissement de santé sont économiquement indissociables dans le cadre du traitement du cancer par chimiothérapie, sans que la société requérante puisse utilement se prévaloir ni de l'existence de volets distinguant les frais d'hospitalisation et ceux liés aux médicaments dans le bordereau de facturation établi en vertu de l'article R. 161-40 du code de la sécurité sociale, qui au demeurant ne constitue pas une facture pour ce dernier, ni de l'inscription des médicaments onéreux sur une liste spécifique, ces règles de facturation et de prise en charge des dépenses hospitalières ne concernant, en l'absence de toute facturation des médicaments cytostatiques au patient, que les seules relations entre les établissements de santé et les organismes de sécurité sociale. <br/>
<br/>
              12. Il résulte de ce qui précède que le traitement du cancer par chimiothérapie forme objectivement, pour le patient moyen, une prestation unique, dont la décomposition entre la livraison de médicament cytostatique et la prestation médicale, étroitement liées d'un point de vue matériel et économique, revêtirait un caractère artificiel. <br/>
<br/>
              En ce qui concerne les commentaires administratifs :<br/>
<br/>
              13. La société Clinique de l'Alliance n'est pas fondée à se prévaloir des énonciations des points 460 à 480 de l'instruction administrative BOI-TVA-CHMAP-10-30-50-50, publiée au Bulletin officiel des finances publiques (BOFiP) - Impôts le 20 novembre 2013, qui concerne les ventes de médicaments ou autres produits réalisées par les médecins propharmaciens et dans les prévisions de laquelle elle n'entre pas. Elle ne peut pas davantage, pour le même motif, se prévaloir des énonciations des paragraphes 20 et 70 des commentaires administratifs publiés au BOFiP le 4 février 2015 sous la référence BOI-TVA-CHAMP-30-10-20-20, qui portent sur les prestations de services et de ventes non liées à l'hospitalisation, qui ne résultent pas d'une prescription médicale ainsi que les actes de médecine et de chirurgie esthétiques qui ne poursuivent pas une finalité thérapeutique. Enfin, elle n'est pas fondée, en tout état de cause, en l'absence de tout rehaussement d'imposition, à se prévaloir de la note de la direction générale des finances publiques du mois de septembre 2013. <br/>
<br/>
              14. Il résulte de tout ce qui précède que la société requérante n'est pas fondée à se plaindre de ce que le tribunal administratif a jugé que les livraisons de médicaments cytostatiques prescrits par des médecins exerçant à titre libéral au sein d'un établissement privé de soins doivent être exonérées de taxe sur la valeur ajoutée et a rejeté ses demandes de restitution de taxe sur la valeur ajoutée au titre des périodes correspondant aux exercices clos en 2013 et 2014.<br/>
<br/>
              Sur les conclusions tendant à la restitution de la taxe sur les salaires acquittées au titre des années 2013 et 2014 :<br/>
<br/>
              15. Aux termes du 3 de l'article 51 de l'annexe III au code général des impôts : " (...) L'assiette de la taxe est constituée par une partie des rémunérations versées, déterminée en appliquant à l'ensemble des rémunérations (...) le rapport existant, au titre de l'année civile précédant celle du paiement desdites rémunérations, entre le chiffre d'affaires qui n'a pas été passible de la taxe sur la valeur ajoutée et le chiffre d'affaires total ". Il résulte de ces dispositions que la taxe sur les salaires afférente aux rémunérations des personnels est établie en appliquant à ces rémunérations le rapport existant entre le chiffre d'affaires de la société requérante qui n'a pas été passible de la taxe sur la valeur ajoutée et le chiffre d'affaires total. Il résulte de ce qui a été dit précédemment qu'il n'y a pas lieu, pour la détermination de l'assiette de la taxe sur les salaires due par la société, de modifier le rapport existant entre le chiffre d'affaires de la société Pôle Santé Léonard-de-Vinci qui n'a pas été passible de la taxe sur la valeur ajoutée et le chiffre d'affaires total de cette société. La société requérante n'est, en outre, pas fondée, en l'absence de tout rehaussement d'imposition, à se prévaloir sur le fondement des dispositions de l'article L. 80 A du livre des procédures fiscales, des prévisions de la doctrine administrative exprimée par l'instruction BOI TPS-TS-20-30 du 22 janvier 2014 et du rescrit n° 2010/31 (FP) du 11 mai 2010.<br/>
<br/>
              16. Il en résulte que la société Clinique de l'Alliance n'est pas fondée à soutenir que c'est à tort que le tribunal administratif a rejeté sa demande tendant à la réduction de ses cotisations de taxe sur les salaires au titre des années 2013 et 2014. <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande à ce titre la société Clinique de l'Alliance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                       --------------<br/>
<br/>
            Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 29 juin 2018 est annulé. <br/>
<br/>
Article 2 : La requête présentée par la société Clinique de L'Alliance devant cette cour est rejetée. <br/>
Article 3 : Les conclusions de la société Clinique de l'Alliance tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative devant le Conseil d'Etat et la cour administrative de Nantes sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Clinique de l'Alliance et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
