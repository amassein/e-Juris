<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029225097</ID>
<ANCIEN_ID>JG_L_2014_07_000000358500</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/22/50/CETATEXT000029225097.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 11/07/2014, 358500, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358500</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:358500.20140711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 358500, la requête sommaire et le mémoire complémentaire, enregistrés les 13 avril et 13 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération générale des fonctionnaires Force ouvrière, dont le siège est situé 46, rue des Petites écuries à Paris (75010), représentée par son secrétaire général ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2012-225 du 16 février 2012 relatif au Conseil supérieur de la fonction publique de l'Etat ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 358646, la requête sommaire et le mémoire complémentaire, enregistrés les 18 avril et 18 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération autonome des fonctionnaires, dont le siège est situé 4, rue de Trévise à Paris (75009), représentée par son secrétaire général ; la fédération requérante demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2012-225 du 16 février 2012 relatif au Conseil supérieur de la fonction publique de l'Etat ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 89-406 du 20 juin 1989 ;<br/>
<br/>
              Vu le décret n° 2011-184 du 15 février 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de la Fédération générale des fonctionnaires Force ouvrière et à la SCP Delaporte, Briard, Trichet, avocat de la Fédération autonome des fonctionnaires ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article 17 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, un décret en Conseil d'Etat détermine la compétence, la composition, l'organisation et le fonctionnement du Conseil supérieur de la fonction publique de l'Etat ; que les requêtes de la Fédération générale des fonctionnaires Force ouvrière et de la Fédération autonome des fonctionnaires sont dirigées contre le même décret, pris sur le fondement de ces dispositions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort de l'examen des pièces versées au dossier n° 358500 par le ministre chargé de la fonction publique que les dispositions du 8° de l'article 5 ainsi que celles de l'article 37 du décret attaqué ne diffèrent pas à la fois du projet soumis par le Gouvernement au Conseil d'Etat et du texte adopté par ce dernier ; que si la Fédération générale des fonctionnaires Force ouvrière soutient en outre que le décret aurait été signé avant la réception de l'avis du Conseil d'Etat, rendu par la section de l'administration dans sa séance du 21 décembre 2011, ses allégations sont en tout état de cause dénuées de toute précision ; que, par suite, les fédérations requérantes ne sont pas fondées à soutenir que les règles qui gouvernent l'examen des projets de décret par le Conseil d'Etat auraient été méconnues ;<br/>
<br/>
              3. Considérant, en second lieu, qu'aucune disposition ne faisait obligation au Premier ministre, avant de signer le décret attaqué, de diligenter une enquête de représentativité des organisations syndicales intéressées ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne les moyens dirigés contre l'article 5 :<br/>
<br/>
              4. Considérant, d'une part, qu'aux termes des deux premiers alinéas de l'article 13 de la loi du 11 janvier 1984 : " Le Conseil supérieur de la fonction publique de l'Etat connaît de toute question d'ordre général concernant la fonction publique de l'Etat dont il est saisi. Il est l'organe supérieur de recours en matière disciplinaire, en matière d'avancement et en matière de licenciement pour insuffisance professionnelle. / Le Conseil supérieur comprend des représentants de l'administration et des représentants des organisations syndicales de fonctionnaires. Seuls ces derniers sont appelés à prendre part aux votes " ; que le dernier alinéa du même article, qui dispose que les sièges sont répartis entre les organisations syndicales proportionnellement au nombre des voix obtenues par chaque organisation lors des dernières élections aux comités techniques, prévoit qu'un décret en Conseil d'Etat fixe, pour les organismes qui ne sont pas soumis aux dispositions de l'article 15 relatif aux comités techniques, " les modalités de prise en compte des voix des fonctionnaires et des agents non titulaires qui en relèvent " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'en vertu de l'article L. 442-5 du code de l'éducation, les établissements d'enseignement privés du premier et du second degré peuvent demander à passer avec l'Etat un " contrat d'association à l'enseignement public ", s'ils répondent à un besoin scolaire reconnu, l'enseignement étant alors confié soit à des maîtres de l'enseignement public, soit à des maîtres liés à l'Etat par contrat, qui ont la qualité d'agent public ; qu'aux termes du premier alinéa de l'article L. 442-12 du même code : " Les établissements d'enseignement privés du premier degré peuvent passer avec l'Etat un contrat simple suivant lequel les maîtres agréés reçoivent de l'Etat leur rémunération qui est déterminée compte tenu notamment de leurs diplômes et des rémunérations en vigueur dans l'enseignement public " ; que, contrairement aux maîtres contractuels qui enseignent dans des établissements sous contrat d'association, les maîtres agréés, qui enseignent dans des établissements ayant passé un contrat simple avec l'Etat, sont des salariés des organismes de gestion de ces établissements, même si leur rémunération est prise en charge par l'Etat ; qu'enfin, l'article L. 813-8 du code rural et de la pêche maritime prévoit que les personnels enseignants et de documentation des établissements d'enseignement agricole privés sous contrat dont les formations sont dispensées dans certaines conditions sont " liés par un contrat de droit public à l'Etat, qui les rémunère directement par référence aux échelles indiciaires des corps équivalents de la fonction publique exerçant des fonctions comparables et ayant les mêmes niveaux de formation " ; <br/>
<br/>
              6. Considérant que le décret attaqué prévoit à son article 5 que le Conseil supérieur de la fonction publique de l'Etat comprend, outre les représentants de l'administration, vingt membres désignés par les organisations syndicales de fonctionnaires proportionnellement au nombre de voix obtenues lors des dernières élections pour la désignation de représentants du personnel aux comités techniques ministériels et à divers autres organismes, parmi lesquels les " commissions consultatives mixtes des maîtres des établissements d'enseignement privés sous contrat et des enseignants des établissements d'enseignement agricole privés sous contrat " ; qu'en vertu des articles R. 914 5 et R. 914-8 du code de l'éducation, dans leur rédaction applicable à la date d'adoption du décret attaqué, les commissions consultatives mixtes départementales et académiques comprennent cinq représentants des maîtres, contractuels ou agréés, de l'enseignement privé primaire ou secondaire et de l'enseignement technique privé ainsi que des maîtres agréés des classes spécialisées fonctionnant dans des établissements spécialisés sous contrat accueillant des enfants et adolescents handicapés, élus par leurs collègues ; qu'en vertu de l'article 55 du décret du 20 juin 1989 relatif aux contrats liant l'Etat et les personnels enseignants et de documentation des établissements mentionnés à l'article L. 813-8 du code rural, la commission consultative mixte instituée auprès du ministre chargé de l'agriculture comprend cinq représentants des enseignants contractuels des établissements de l'enseignement agricole privés relevant de l'article L. 813-8 du code rural et de la pêche maritime, élus par leurs collègues ;<br/>
<br/>
              7. Considérant, en premier lieu, qu'en vertu du troisième alinéa de l'article L. 442-5 du code de l'éducation, issu de la loi du 5 janvier 2005 relative à la situation des maîtres des établissements d'enseignement privés sous contrat, les personnels enseignants des établissements d'enseignement privés du premier et du second degré sous contrat d'association avec l'Etat, malgré l'absence de contrat de travail avec l'établissement, " sont électeurs et éligibles pour les élections des délégués du personnel et les élections au comité d'hygiène, de sécurité et des conditions de travail et au comité d'entreprise. Ils bénéficient de ces institutions dans les conditions prévues par le code du travail " ; que, contrairement à ce que soutient la Fédération générale des fonctionnaires Force ouvrière, le décret attaqué, en ce qu'il prévoit que les résultats des élections aux commissions consultatives mixtes sont pris en considération pour la répartition des sièges au Conseil supérieur de la fonction publique de l'Etat, ne méconnaît aucunement ces dispositions ; <br/>
<br/>
              8. Considérant, en deuxième lieu, qu'aux termes du premier alinéa du I de l'article 18 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " Sont électeurs pour la désignation des représentants du personnel au sein du comité technique tous les agents exerçant leurs fonctions, dans le périmètre du département ministériel, de la direction, du service ou de l'établissement public au titre duquel le comité est institué " ; qu'il résulte de ces dispositions que les maîtres contractuels ou agréés de l'enseignement privé, qui exercent leurs fonctions dans des établissements d'enseignement privés liés à l'Etat par contrat, ne sont pas électeurs au comité technique ministériel et aux comités techniques des services déconcentrés du ministère chargé de l'éducation nationale ; que, par suite, la Fédération générale des fonctionnaires Force ouvrière n'est pas fondée à soutenir que le vote des maîtres, contractuels ou agréés, de l'enseignement privé serait pris en compte deux fois pour la répartition des sièges au Conseil supérieur de la fonction publique de l'Etat, au titre des élections aux comités techniques et au titre des élections aux commissions consultatives mixtes ; que le moyen tiré de la méconnaissance du principe d'égalité ne peut, dès lors, qu'être écarté ;<br/>
<br/>
              9. Considérant, en revanche, que, pour définir les règles de répartition des sièges entre organisations syndicales en application de l'article 13 de la loi du 11 janvier 1984, le pouvoir réglementaire devait seulement prendre en compte les voix de l'ensemble des fonctionnaires et des agents non titulaires à l'égard desquels le Conseil supérieur de la fonction publique de l'Etat exerce sa compétence ; que, ainsi qu'il a été dit au point 5, les maîtres agréés, qui enseignent dans des établissements ayant passé un contrat simple avec l'Etat sur le fondement de l'article L. 442-12 du code de l'éducation, sont des salariés des organismes de gestion de ces établissements ; que, par suite, et alors même que l'article L. 914-1 du code de l'éducation a posé un principe de parité entre les maîtres titulaires de l'enseignement public et les maîtres contractuels ou agréés justifiant du même niveau de formation, le décret attaqué ne pouvait légalement prévoir la prise en compte des suffrages des maîtres agréés aux élections des commissions consultatives mixtes pour déterminer la répartition des sièges au Conseil supérieur de la fonction publique de l'Etat, qui n'a pas compétence pour connaître de questions relatives aux salariés d'employeurs de droit privé ; que, par suite, la Fédération générale des fonctionnaires Force ouvrière est fondée à soutenir que le décret est entaché d'illégalité dans cette mesure ; que cette disposition est divisible des autres dispositions du décret ;<br/>
<br/>
              En ce qui concerne les moyens dirigés contre l'article 37 :<br/>
<br/>
              10. Considérant qu'ainsi qu'il a été dit ci-dessus, il résulte des dispositions du dernier alinéa de l'article 13 de la loi du 11 janvier 1984, dans sa rédaction résultant de la loi du 5 juillet 2010 relative à la rénovation du dialogue social et comportant diverses dispositions relatives à la fonction publique, que les sièges au Conseil supérieur de la fonction publique de l'Etat sont répartis entre les organisations syndicales proportionnellement au nombre des voix obtenues par chaque organisation lors des dernières élections aux comités techniques ; qu'aux termes de l'article 30 de la loi du 5 juillet 2010 : " Jusqu'au terme d'une période transitoire qui s'achève au premier renouvellement de l'instance qui suit le 31 décembre 2013, les sièges attribués aux organisations syndicales représentatives de fonctionnaires pour la composition du Conseil supérieur de la fonction publique de l'Etat sont attribués conformément aux règles suivantes : / 1° Les sièges sont répartis entre les organisations syndicales proportionnellement au nombre des voix qu'elles ont obtenues aux élections ou consultations du personnel organisées pour la désignation des représentants du personnel aux comités techniques et aux organismes consultatifs permettant d'assurer la représentation des personnels de l'Etat en vertu de dispositions législatives spéciales ; / 2° Toute organisation syndicale justifiant au sein de la fonction publique de l'Etat d'une influence réelle, caractérisée par son activité, son expérience et son implantation professionnelle et géographique, dispose au moins d'un siège (...) " ; <br/>
<br/>
              11. Considérant que les dispositions des deux premiers alinéas du I de l'article 37 du décret attaqué se bornent à mettre en oeuvre l'article 30 de la loi du 5 juillet 2010 pour les renouvellements du Conseil supérieur de la fonction publique de l'Etat intervenant jusqu'au 31 décembre 2013 ; que, par suite, la Fédération autonome des fonctionnaires ne peut utilement soutenir que ces dispositions définiraient de façon excessivement restrictive les organisations syndicales appelées à désigner un membre au sein du conseil supérieur et méconnaîtraient les principes de liberté syndicale et de non-discrimination entre organisations syndicales légalement constituées ; qu'elle n'est, par ailleurs, pas fondée à soutenir qu'en prévoyant la prise en compte des suffrages ayant servi de référence pour la composition des instances renouvelées en 2010 et 2011, le troisième alinéa du I du même article, qui ne dispose que pour l'avenir, serait entaché d'une rétroactivité illégale ou encore méconnaîtrait l'exigence de clarté et de loyauté des consultations électorales et le principe de sécurité juridique ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que les fédérations requérantes sont fondées à demander l'annulation du décret qu'elles attaquent en tant seulement qu'il n'exclut pas, au 8° de son article 5, la prise en compte des suffrages exprimés par les maîtres agréés lors des dernières élections pour la désignation de représentants du personnel aux commissions consultatives mixtes des maîtres des établissements d'enseignement privés sous contrat ; <br/>
<br/>
              Sur les conséquences de l'illégalité des dispositions du 8° de l'article 5 : <br/>
<br/>
              13. Considérant que, compte tenu des effets excessifs qu'emporterait une annulation rétroactive des dispositions entachées d'illégalité, en raison du nombre et de l'importance des textes examinés par le Conseil supérieur de la fonction publique de l'Etat et du risque de mise en cause de la situation des nombreux fonctionnaires concernés, il y a lieu de prévoir que l'annulation prononcée par la présente décision ne prendra effet qu'à compter du 1er janvier 2015 et que, sous réserve des actions contentieuses engagées à la date de la présente décision, les effets produits par le 8° de l'article 5 du décret attaqué antérieurement à son annulation seront réputés définitifs ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 000 euros à verser à chacune des deux  fédérations requérantes ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret du 16 février 2012 relatif au Conseil supérieur de la fonction publique de l'Etat est annulé en tant qu'il n'exclut pas, au 8° de son article 5, la prise en compte des suffrages exprimés par les maîtres agréés lors des élections pour la désignation de représentants du personnel aux commissions consultatives mixtes des maîtres des établissements d'enseignement privés sous contrat. Cette annulation prendra effet le 1er janvier 2015.<br/>
Article 2 : Sous réserve des actions contentieuses engagées à la date de la présente décision, les effets produits par ces dispositions antérieurement à leur annulation sont réputés définitifs.<br/>
Article 3 : L'Etat versera une somme de 1 000 euros à la Fédération générale des fonctionnaires Force ouvrière et à la Fédération autonome des fonctionnaires au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des requêtes de la Fédération générale des fonctionnaires Force ouvrière et de la Fédération autonome des fonctionnaires est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la Fédération générale des fonctionnaires Force ouvrière, à la Fédération autonome des fonctionnaires, au Premier ministre et à la ministre de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
