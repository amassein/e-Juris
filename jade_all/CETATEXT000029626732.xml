<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626732</ID>
<ANCIEN_ID>JG_L_2014_10_000000372494</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626732.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 24/10/2014, 372494, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372494</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:372494.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure  <br/>
<br/>
              L'EURL La Croix Paqueray a demandé au tribunal administratif de Caen le rétablissement de son résultat déficitaire au titre de l'exercice clos en 2006, réduit par l'administration fiscale d'un montant de 236 000 euros. Par un jugement n° 1100809 du 17 juillet 2012, le tribunal administratif de Caen a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 12NT02608 du 25 juillet 2013, la cour administrative d'appel de Nantes, faisant partiellement droit à l'appel de la société, a fixé le résultat déficitaire de celle-ci au titre de l'exercice clos en 2006 au montant de 73 144 euros, réformé le jugement en ce sens et rejeté le surplus des conclusions de la requête. <br/>
<br/>
Procédure devant le Conseil d'Etat : <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 septembre et 30 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, l'EURL La Croix Paqueray demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 3 de l'arrêt n° 12NT02608 du 25 juillet 2013 de la cour administrative d'appel de Nantes ; <br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              L'EURL La Croix Paqueray soutient que la cour administrative d'appel de Nantes : <br/>
              - a dénaturé les termes de la réponse aux observations du contribuable en date du 28 octobre 2009 en relevant qu'il ressortait de ce document que l'administration fiscale avait pratiqué une décote sur le prix de cession de deux biens immobiliers de référence situés à Granville ; <br/>
<br/>
              - a commis une erreur de droit et méconnu le principe du contradictoire en validant une méthode d'évaluation par " décote " arbitraire et non chiffrée ;<br/>
<br/>
              - a insuffisamment motivé son arrêt, méconnu les règles relatives à la charge de la preuve, commis une erreur de droit et par suite inexactement qualifié les faits qui lui étaient soumis  en écartant par principe le moyen tiré de ce que le coût d'acquisition de la maison en 2005 constituait un élément de comparaison pertinent ; <br/>
<br/>
              - a insuffisamment motivé son arrêt, commis une erreur de droit et par suite inexactement qualifié les faits qui lui étaient soumis en écartant par principe les attestations établies par des agences immobilières ;<br/>
<br/>
              - a dénaturé le rapport d'expertise en indiquant qu'il ne résultait pas de celui-ci que son auteur aurait pris comme termes de comparaison des cessions concernant des biens de même ancienneté et de même situation par rapport au rivage ;<br/>
<br/>
              - a entaché son arrêt d'une contradiction de motifs en affirmant que les biens de référence utilisés par l'expert n'étaient pas pertinents alors que l'expertise démontrait que l'unique bien de référence retenu par l'administration fiscale sur la commune de Charolle n'était pas non plus pertinent ;<br/>
<br/>
              - a dénaturé ses écritures ainsi que les pièces du dossier et insuffisamment motivé son arrêt en écartant le facteur de moins-value consistant en la présence, en bordure de la propriété, d'un immeuble collectif de deux étages au motif que certains des immeubles pris comme références étaient eux-mêmes situés en milieu urbain ;<br/>
<br/>
              - a commis une erreur de droit en faisant peser sur elle la charge de la preuve de la surface exacte du bien en litige ;<br/>
<br/>
              - a dénaturé les pièces du dossier en estimant qu'elle ne démontrait pas que la mention de la construction accessoire de 54 m2 aurait été rajoutée par l'administration sur la déclaration souscrite par M. et MmeA..., postérieurement au dépôt de cette déclaration ;<br/>
<br/>
              - a insuffisamment motivé son arrêt et dénaturé les termes du litige en validant les superficies retenues pour le rez-de-chaussée et le premier étage de la villa, sans se prononcer sur l'erreur de métrage ressortant du rapport d'expertise ;<br/>
<br/>
              - a commis une erreur de droit et par suite inexactement qualifié les faits qui lui étaient soumis en jugeant qu'elle n'était pas fondée à se prévaloir de la jurisprudence observée en matière de titres non cotés ;<br/>
<br/>
              - a insuffisamment motivé son arrêt en ne répondant pas aux moyens tirés d'une part, de ce que les difficultés de l'entreprise étaient dues au retard dans la délivrance des autorisations administratives préalables à l'engagement des travaux sur l'immeuble collectif et que dans l'attente de ces autorisations, il avait été procédé à la réfaction de la villa et d'autre part, que la vente de la villa à un tiers était apparue comme hasardeuse à raison de la taille de la maison et de la proximité de l'immeuble collectif ;<br/>
<br/>
              - a privé sa décision de motifs et par suite inexactement qualifié l'intention libérale en ne prenant pas en compte la circonstance qu'elle avait retenu un prix de cession se situant dans la fourchette haute des estimations que M. A...avait pris la peine de solliciter avant la vente. <br/>
<br/>
              Par un mémoire en défense, enregistré le 12 juin 2014, le ministre des finances et des comptes publics conclut au rejet du pourvoi. Il soutient que les moyens soulevés par la requérante ne sont pas fondés.<br/>
<br/>
<br/>
              Vu : <br/>
<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Société Eurl La Croix Paqueray.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'EURL La Croix Paqueray, dont M. et Mme A...détiennent 100% des parts par l'intermédiaire de la société holding SA R2J, exerce une activité de marchand de biens. La société a acquis en 2005 à Carolles (Manche) un ensemble immobilier constitué d'un terrain sur lequel sont édifiés une grande maison ancienne, dénommée " Villa Jeanne d'Arc ", une maison de gardien ainsi qu'un bâtiment de trois étages d'une surface de 300 m2 au sol ayant abrité des colonies de vacances et destiné à être transformé en appartements. Par acte notarié du 3 août 2006, l'EURL La Croix Paqueray a cédé à M. et Mme A...la villa et la maison de gardien au prix de 600 000 euros. A l'issue d'une vérification de comptabilité de l'EURL portant sur la période du 1er janvier 2006 au 30 septembre 2008, l'administration fiscale a réintégré dans le résultat de l'entreprise la somme de 236 000 euros correspondant, selon elle, à la minoration du prix de cession des biens immobiliers vendus à M. et Mme A..., estimant que cette opération était constitutive d'un acte anormal de gestion. La société ayant déclaré un résultat déficitaire au titre de l'exercice clos en 2006, ce rehaussement a seulement conduit à la réduction, à due concurrence, du déficit reportable déclaré. Sa réclamation contestant le rehaussement ayant été rejetée le 14 février 2011, la société a porté le litige devant le tribunal administratif de Caen qui, par un jugement du 17 juillet 2012, a rejeté sa demande tendant au rétablissement du résultat déficitaire initialement déclaré. Faisant partiellement droit à l'appel de la société contre ce jugement, la cour administrative d'appel de Nantes a par un arrêt du 25 juillet 2013, fixé le résultat déficitaire de celle-ci au titre de l'exercice clos en 2006 à 73 144 euros, réformé le jugement en ce sens et rejeté le surplus des conclusions de la requête. L'EURL La Croix Paqueray demande l'annulation de cet arrêt en tant qu'il lui est défavorable. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, pour évaluer la superficie de la villa cédée par l'EURL La Croix Paqueray à M. et MmeA..., l'administration fiscale s'est fondée sur une déclaration relative aux impôts locaux souscrite par M. et Mme A...le 12 décembre 2006. L'EURL La Croix Paqueray a contesté la superficie ainsi retenue en se prévalant d'une expertise réalisée à l'initiative de M. A... et enregistrée au greffe de la cour administrative d'appel de Nantes le 16 janvier 2013. Au cours de l'instruction, le ministre a seulement admis une légère réduction de la surface du deuxième étage de la villa. <br/>
<br/>
              3. En se bornant, pour répondre à la contestation développée par l'EURL La Croix Paqueray sur la superficie de la villa, à entériner la correction de surface admise par le ministre pour le deuxième étage de la villa, sans se prononcer sur l'évaluation de la surface des rez-de-chaussée et premier étage, que la requérante contestait en se prévalant d'une expertise et qui était déterminante pour l'estimation de la valeur vénale de la villa, la cour a insuffisamment motivé son arrêt. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la requérante est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il a rejeté le surplus des conclusions de sa requête. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'EURL La Croix Paqueray au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêt de la cour administrative d'appel de Nantes du 25 juillet 2013 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée à l'article 1er, à la cour administrative d'appel de Nantes. <br/>
<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à l'EURL La Croix Paqueray au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à l'EURL La Croix Paqueray et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
