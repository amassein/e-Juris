<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288066</ID>
<ANCIEN_ID>JG_L_2013_04_000000364165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288066.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 08/04/2013, 364165, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Chadelat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364165.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 28 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B...A..., demeurant à...; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 20 septembre 2012 accordant son extradition aux autorités britanniques ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              Vu la convention établie sur la base de l'article K 3 du traité sur l'Union européenne, relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996 ;<br/>
<br/>
              Vu la décision-cadre du Conseil de l'Union européenne du 13 juin 2002 relative au mandat d'arrêt européen et aux procédures de remise aux Etats membres ; <br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Chadelat, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par le décret attaqué, le Premier ministre a accordé aux autorités britanniques, en application de la convention européenne d'extradition et de la convention relative à l'extradition entre les Etats membres de l'Union européenne signée à Dublin le 27 septembre 1996 et sur le fondement d'un mandat d'arrêt décerné le 3 juillet 2000 par un juge à la cour d'assises de Norwich, l'extradition de M. B...A...aux fins de poursuites de faits qualifiés en droit français de viols et d'agressions sexuelles, pour certains sur mineurs de 15 ans, commis entre 1972 et 1976 ; <br/>
<br/>
              Sur le cadre juridique applicable : <br/>
<br/>
              2.	Considérant qu'il résulte de l'article 31 de la décision-cadre du Conseil de l'Union européenne du 13 juin 2002 relative aux mandats d'arrêt européen et aux procédures de remise aux Etats membres que le mandat d'arrêt européen, dont le régime est fixé par les dispositions de la décision-cadre, se substitue à partir du 1er janvier 2004 aux conventions internationales relatives à l'extradition entre les Etats membres ; qu'il en est notamment ainsi de la convention européenne d'extradition du 13 décembre 1957 et de la convention relative à l'extradition entre les Etats membres de l'Union européenne signée à Dublin le 27 septembre 1996 ; <br/>
<br/>
              3.	Considérant, toutefois, que l'article 32 de la décision-cadre autorise chaque Etat membre à déclarer, au moment de l'adoption de celle-ci, qu'il continuera, en tant qu'Etat membre d'exécution, à traiter par la procédure d'extradition applicable avant le 1er janvier 2004 les demandes relatives à des faits commis avant une date qu'il indique et qui ne peut être postérieure au 7 août 2002 ; que la France a opté pour la faculté ouverte par l'article 32 et a déclaré qu'en tant qu'Etat d'exécution, elle continuera à traiter les demandes d'extradition relatives à des faits commis avant le 1er novembre 1993 selon la procédure applicable avant le 1er janvier 2004 ; qu'il en résulte que la demande de remise formée par les autorités britanniques pour des faits commis avant le 1er novembre 1993 est demeurée régie par la convention européenne d'extradition et la convention relative à l'extradition entre les Etats membres de l'Union européenne signée à Dublin le 27 septembre 1996 ;<br/>
<br/>
              Sur les moyens contestant les vices propres du décret attaqué :<br/>
<br/>
              4.	Considérant, d'une part, qu'il ressort des mentions de l'ampliation du décret attaqué figurant au dossier et certifiée conforme par le secrétaire général du Gouvernement que le décret a été signé par le Premier ministre et contresigné par la garde des sceaux, ministre de la justice ; que l'ampliation notifiée à M. A...n'avait pas à être revêtue de ces signatures ; <br/>
<br/>
              5.	Considérant, d'autre part, que le décret attaqué comporte l'énoncé des considérations de fait et de droit qui en constituent le fondement ; qu'il satisfait ainsi à l'exigence de motivation prévue par l'article 3 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ; <br/>
<br/>
              Sur les moyens relatifs à l'arrestation provisoire et à l'avis de la chambre de l'instruction :<br/>
<br/>
              6.	Considérant, en premier lieu, que si M. A...soutient que le mandat d'arrêt européen initialement décerné contre lui ne pouvait valoir ordre d'arrestation provisoire, que le procès-verbal d'interpellation fait inexactement référence à un mandat d'arrêt européen du 3 juillet 2000, que les conditions de son arrestation provisoire auraient méconnu l'article 696-23 du code de procédure pénale, il n'appartient pas au Conseil d'Etat de se prononcer sur les conditions de cette arrestation, dont l'appréciation relève de la seule compétence des juridictions judiciaires ; <br/>
<br/>
              7.	Considérant, en deuxième lieu, qu'il n'appartient pas au Conseil d'Etat, saisi d'un recours contre un décret d'extradition, d'examiner les moyens de forme ou de procédure mettant en cause la régularité de l'avis émis par la chambre de l'instruction ; <br/>
<br/>
              8.	Considérant, en troisième lieu, que les moyens tirés de la méconnaissance des stipulations des articles 3 et 6 du troisième protocole, en date du 10 novembre 2010, additionnel à la convention européenne d'extradition, qui n'a pas été ratifié par la France, sont en tout état de cause inopérants ; <br/>
<br/>
              Sur les moyens relatifs à la prescription de l'action publique :<br/>
<br/>
              9.	Considérant que les conventions d'extradition sont des lois de procédure qui, sauf stipulation contraire, sont applicables immédiatement aux faits survenus avant leur entrée en vigueur ; qu'il en est ainsi, notamment, des conditions qu'elles fixent quant à la prescription de l'action publique ou de la peine ; <br/>
<br/>
              10.	Considérant que la convention signée à Dublin le 27 septembre 1996, relative à l'extradition entre les Etats membres de l'Union européenne, dont les stipulations sont applicables en vertu du paragraphe 5 de son article 18 aux demandes d'extradition présentées postérieurement à son entrée en vigueur, s'est substituée, entre ces Etats, à compter du 1er juillet 2005, date de cette entrée en vigueur, à la convention européenne d'extradition du 13 décembre 1957 ; <br/>
<br/>
              11.	Considérant qu'il résulte de ce qui précède que les stipulations de la convention de Dublin ont vocation à régir la demande d'extradition de M. A...présentée par les autorités britanniques le 12 juin 2012 ; qu'en particulier, sont applicables les stipulations du paragraphe 1 de son article 8 aux termes desquelles : " L'extradition ne peut être refusée au motif qu'il y a prescription de l'action ou de la peine selon la législation de l'Etat membre requis ", qui se sont substituées à celles de l'article 10 de la convention européenne d'extradition du 13 décembre 1957, selon lesquelles la prescription ne devait être acquise ni selon la législation de l'Etat requérant ni selon celle de l'Etat requis ; <br/>
<br/>
              12.	Considérant que les faits constitutifs de viols et d'agressions sexuelles sur mineurs pour la poursuite desquels l'extradition de M. A...a été demandée sont imprescriptibles en droit britannique ; que la circonstance qu'il n'existe pas, en droit britannique, de prescription de l'action publique pour ces faits ne saurait être regardée comme contraire à l'ordre public français ; <br/>
<br/>
              Sur le moyen relatif à la qualification pénale des faits incriminés :<br/>
<br/>
              13.	Considérant, d'une part, que le respect de la règle de la double incrimination par la législation de l'Etat requérant et par celle de l'Etat requis, résultant de l'article 2 de la convention européenne d'extradition, n'implique pas que la qualification pénale des faits doive être identique dans ces deux législations ; que si les faits reprochés à M. A...reçoivent, en droit britannique, la qualification de sodomie et attentat à la pudeur contre un homme, prévus et réprimés par les articles 12 et 15 de la loi de 1956 sur les délits sexuels, ils reçoivent, en droit français, la qualification de viols et agressions sexuelles sur mineur de quinze ans et de viols et agressions sexuelles, prévus et réprimés par les articles 222-22, 222-23, 222-24, 222-27 et 222-29 du code pénal ; que, par suite, le moyen tiré de la méconnaissance de la règle de la double incrimination doit être écarté ;<br/>
<br/>
              14.	Considérant, d'autre part et en tout état de cause, que la demande d'extradition ne repose pas, contrairement à ce que soutient M.A..., sur des considérations discriminatoires tenant à son orientation sexuelle ; <br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              15.	Considérant, en premier lieu, qu'en admettant même qu'une condamnation à la peine de réclusion criminelle à perpétuité ne donne que rarement et tardivement lieu à un aménagement de peine ou à une libération conditionnelle, l'extradition d'une personne exposée à une peine incompressible de réclusion perpétuelle n'est pas en soi contraire à l'ordre public français ; <br/>
<br/>
              16.	Considérant, en deuxième lieu, que la circonstance que les faits à l'origine de l'extradition de M. A...sont anciens n'est pas de nature à établir que les conditions dans lesquelles il viendrait à être jugé pour ces faits, en cas d'exécution du décret attaqué, le priveraient du droit à un procès équitable alors que le système juridictionnel de l'Etat requérant offre des garanties conformes aux principes généraux des droits de la défense ; que, par suite, le moyen tiré de la violation du premier alinéa des réserves et déclarations émises par la France lors de la ratification de la convention européenne d'extradition doit être écarté ; <br/>
<br/>
              17.	Considérant, en troisième lieu, qu'aux termes du second alinéa de ces réserves et déclarations, l'extradition peut être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée ; que si M. A...soutient que l'exécution du décret attaqué aurait des conséquences d'une gravité exceptionnelle sur son état de santé, il ne ressort nullement des éléments versés au dossier que l'état de santé de l'intéressé serait de nature à faire obstacle à son extradition ; que le moyen tiré de ce que le Premier ministre aurait commis une erreur manifeste d'appréciation en refusant de mettre en oeuvre la possibilité de refuser l'extradition résultant des réserves et déclarations émises par la France ne peut, par suite, qu'être écarté ;<br/>
<br/>
              18.	Considérant, en quatrième lieu, que si une décision d'extradition est susceptible de porter atteinte, au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au droit au respect de la vie familiale, cette mesure trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, tant le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France que l'exécution, par les mêmes personnes, des condamnations pénales prononcées contre elles à l'étranger pour de tels crimes ou délits ; que ni l'ancienneté de la présence en France de M.A..., ni la circonstance que son épouse est âgée et serait atteinte d'une pathologie invalidante et que son fils souffrirait de troubles de la compréhension ne sont de nature à faire obstacle, dans l'intérêt de l'ordre public, à l'exécution de son extradition ; que, dès lors, le moyen tiré de la méconnaissance de l'article 8 de la convention doit être écarté ; <br/>
<br/>
              19.	Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du décret du 20 septembre 2012 accordant son extradition aux autorités britanniques ;<br/>
<br/>
              Sur les conclusions de M. A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              20.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, verse à M. A...la somme qu'il demande au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
