<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018314379</ID>
<ANCIEN_ID>JG_L_2008_02_000000266755</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/31/43/CETATEXT000018314379.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 22/02/2008, 266755</TITRE>
<DATE_DEC>2008-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>266755</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET ; SCP TIFFREAU ; SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>M. Alban de Nervaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Casas Didier</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 avril 2004 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. Etienne B, domicilié ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 12 février 2004 par lequel la cour administrative d'appel de Douai a rejeté son appel dirigé contre le jugement du 15 novembre 2001 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de la délibération du 21 décembre 1999 par laquelle le conseil de la communauté urbaine de Lyon (COURLY) a autorisé le président de celle-ci à conclure une transaction avec la société concessionnaire du boulevard périphérique Nord de Lyon (SCBPNL), à l'annulation de la décision de signer cette transaction, subsidiairement à l'annulation de cette transaction elle-même, et à ce qu'il soit enjoint à la communauté urbaine de Lyon de solliciter du juge du contrat la résiliation de cette transaction ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du tribunal administratif de Lyon du 15 novembre 2001 et de faire droit à ses demandes devant ce tribunal ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la directive 89/440/CEE du Conseil du 18 juillet 1989 modifiant la directive 71/305/CEE portant coordination des procédures de passation des marchés publics de travaux ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alban de Nervaux, Auditeur,  <br/>
<br/>
              - les observations de la SCP Tiffreau, avocat de M. B, de la SCP Delaporte, Briard, Trichet, avocat de la société concessionnaire du boulevard périphérique Nord de Lyon et de la SCP Vier, Barthélemy, Matuchansky, avocat de la communauté urbaine de Lyon, <br/>
<br/>
              - les conclusions de M. Didier Casas, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que, par une décision en date du 6 février 1998, le Conseil d'Etat statuant au contentieux a annulé comme prises sur la base de règles nationales incompatibles avec les objectifs de la directive 89/440/CEE du 18 juillet 1989 portant coordination des procédures de passation des marchés publics de travaux, la délibération en date du 18 juillet 1991 par laquelle le conseil de la communauté urbaine de Lyon avait autorisé son président à signer avec la société concessionnaire du boulevard périphérique Nord de Lyon (S.C.B.P.N.L.) la convention de réalisation et d'exploitation du  tronçon nord du boulevard périphérique de l'agglomération lyonnaise ainsi que la décision de signer, le 19 juillet 1991, cette convention ; que par décision du 27 février 1998, le président de la communauté urbaine a prononcé la résiliation de cette convention ; qu'après que la communauté urbaine a pris possession de l'ouvrage, le 6 mars 1998, pour l'exploiter en régie directe, une procédure de conciliation a été engagée sous la conduite du président du tribunal administratif de Lyon, sur le fondement de l'article L. 3 du code des tribunaux administratifs et des cours administratives d'appel, alors applicable, afin de permettre un règlement amiable des conséquences financières de la décision du Conseil d'Etat ; qu'à l'issue de cette procédure, le conseil de la communauté urbaine de Lyon a approuvé, le 21 décembre 1999, les termes de la transaction à laquelle étaient parvenues les parties et autorisé son président à signer la convention de transaction, aux termes de laquelle la SCBPNL renonçait à toute demande sur un terrain quasi-délictuel, seules étant indemnisées les dépenses utiles exposées par elle ; que M. B, contribuable local, se pourvoit en cassation contre l'arrêt du 12 février 2004 de la cour administrative d'appel de Douai confirmant le jugement en date du 15 novembre 2001 par lequel le tribunal administratif de Lyon a rejeté sa demande dirigée contre la délibération du 21 décembre 1999 ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              Considérant que l'entrepreneur dont le contrat est entaché de nullité peut prétendre, sur un terrain quasi-contractuel, au remboursement de celles de ses dépenses qui ont été utiles à la collectivité envers laquelle il s'était engagé ; que les fautes éventuellement commises par l'intéressé antérieurement à la signature du contrat sont sans incidence sur son droit à indemnisation au titre de l'enrichissement sans cause de la collectivité, sauf si le contrat a été obtenu dans des conditions de nature à vicier le consentement de l'administration, ce qui fait obstacle à l'exercice d'une telle action ; que dans le cas où la nullité du contrat résulte d'une faute de l'administration, l'entrepreneur peut en outre, sous réserve du partage de responsabilités découlant le cas échéant de ses propres fautes, prétendre à la réparation du dommage imputable à la faute de l'administration ; qu'il peut à ce titre demander le paiement du bénéfice dont il a été privé par la nullité du contrat, si toutefois l'indemnité à laquelle il a droit sur un terrain quasi-contractuel ne lui assure pas déjà une rémunération supérieure à celle que l'exécution du contrat lui aurait procurée ; <br/>
<br/>
              Considérant qu'en écartant comme inopérant le moyen tiré de ce que le contrat de concession du 19 juillet 1991 avait été conclu à la suite de manoeuvres dolosives, imputables à la S.C.B.P.N.L. et constitutives d'un vice du consentement de nature à faire obstacle à ce que soit engagée, dans le cadre de la convention de transaction, la responsabilité de la communauté urbaine sur le fondement de l'enrichissement sans cause, la cour administrative d'appel a commis une erreur de droit ; que son arrêt doit, pour ce motif, être annulé ; <br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              Considérant, en premier lieu, que la circonstance que le président du tribunal administratif de Lyon, qui n'était pas membre de la formation collégiale ayant rendu le jugement attaqué, a participé à la mission de conciliation, ne suffit pas à faire naître un doute objectivement justifié sur l'impartialité de cette formation de jugement et à faire obstacle à ce qu'elle soit regardée comme  un tribunal indépendant et impartial  au sens de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Considérant, en deuxième lieu, que si le rapport d'expertise et ses annexes n'ont pu, compte tenu de leur volume, être notifiés à M. B, il ne ressort pas des pièces du dossier que le requérant ait été empêché d'exercer le droit d'accès aux pièces jointes qu'il tient de l'article R. 611-5 du code de justice administrative ; qu'en outre, ainsi qu'il en convient d'ailleurs, il a pu prendre connaissance du rapport d'expertise en sa qualité d'élu et réaliser toutes photocopies utiles ; que, dans ces conditions, le caractère contradictoire de la procédure n'a pas été méconnu ;<br/>
<br/>
              Considérant, en troisième lieu, que la circonstance que l'expédition du jugement du tribunal administratif de Lyon du 15 novembre 2001 comportait seulement l'analyse des conclusions des parties et ne faisait pas apparaître celle des moyens invoqués n'est pas de nature à entacher d'irrégularité le jugement attaqué dont la minute comporte le visa de l'ensemble des conclusions et moyens dont était saisi le tribunal ; que cette  circonstance n'apporte pas au droit à un procès équitable une atteinte contraire aux stipulations de l'article 6 §1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Sur la compétence de la communauté urbaine de Lyon pour conclure la transaction :<br/>
<br/>
              Considérant que la faculté dont dispose le contribuable d'une communauté urbaine, en vertu de l'article L. 5211-58 du code général des collectivités territoriales, d'exercer avec l'autorisation du tribunal administratif  les actions qu' il croit appartenir à la collectivité publique ne fait pas obstacle à l'exercice par le président de cette collectivité du pouvoir qu'il tient des dispositions combinées des articles L. 2122-21 7° et L. 2122-24 du même code de conclure, sur habilitation du conseil de communauté, une transaction ayant pour objet de régler définitivement un litige, et ce alors même qu'une telle transaction est opposable au contribuable local ;<br/>
<br/>
              Sur les vices qui auraient entaché le contrat de concession :<br/>
<br/>
              Considérant, d'une part,  que si M. B fait valoir que l'article 6 de la convention de concession signée en juillet 1991 est entaché d'une condition potestative contraire aux principes dégagés par l'article 1174 du code civil et que la société concessionnaire du boulevard périphérique Nord de Lyon n'ignorait pas l'existence de la directive du 18 juillet 1989 et savait que la communauté urbaine n'avait pas fait appel à la concurrence, il résulte de ce qui a été dit ci-dessus que les fautes ainsi imputées à la SCBPNL sont, en tout état de cause, sans incidence sur son droit au remboursement, sur un terrain quasi-contractuel,  de celles de ses dépenses qui ont été utiles à la communauté urbaine de Lyon ;<br/>
<br/>
              Considérant, d'autre part, qu'il ne ressort pas des pièces du dossier que le choix de la S.C.B.P.N.L par le conseil de la communauté urbaine  de Lyon ait été déterminé par les avantages financiers dont auraient bénéficié certains de ses membres de la part des sociétés Bouygues  et Dumez, actionnaires de la SCPBNL ; que doit ainsi être écarté le moyen tiré de que ces sociétés se seraient rendues coupables de manoeuvres frauduleuses, entachant de dol le contrat de concession conclu le 19 juillet 1991 et de nature à faire obstacle à ce que soit engagée, dans le cadre de la transaction litigieuse, la responsabilité de la communauté urbaine sur le fondement de l'enrichissement sans cause ; <br/>
<br/>
              En ce qui concerne l'évaluation des travaux utiles à la collectivité publique :<br/>
<br/>
              Considérant, en premier lieu, qu'après la résiliation du contrat de concession, la communauté urbaine a décidé de reprendre la gestion de l'ouvrage en régie directe ; qu'il s'en est suivi l'incorporation immédiate de celui-ci dans son patrimoine et la nécessité corrélative d'indemniser la société concessionnaire ; que les contrats, factures et toutes pièces utiles à la justification des sommes réclamées par la S.C.B.P.N.L ont été mises par celle-ci à la disposition des experts, des parties et de la mission de conciliation ; que le collège d' experts, dont il n'est pas démontré que les conditions de nomination auraient manqué à l'impartialité, ne s'est pas contenté d'entériner les factures établies par les constructeurs  sans la moindre discussion de la valeur réelle des travaux , mais s'est attaché à vérifier le caractère normal des montants, notamment en les comparant avec le coût d'ouvrages analogues ; que la réalité physique des travaux effectués a été contrôlée à l'occasion de nombreuses réunions techniques ; que des abattements significatifs ont été effectués en particulier sur les frais de financement ; qu' ainsi le manque de sérieux allégué du travail des experts, tant sur le plan technique que financier, dont résulterait une évaluation erronée des travaux utiles à la communauté urbaine, ne peut être regardé comme établi ;<br/>
<br/>
              Considérant, en deuxième lieu, que, conformément à la volonté des parties, donnent lieu en l'espèce à indemnisation les dépenses exposées  pour la construction de l'ouvrage qui ont été utiles à la collectivité publique ; que la circonstance que l'ouvrage ne serait pas rentable n'est pas de nature à faire regarder l'enrichissement de la collectivité comme devant être évalué à un montant inférieur à celui des dépenses exposées pour sa construction ; que c'est à bon droit qu'ont été comptées au nombre des dépenses utiles les dépenses engagées pour la construction de l'ouvrage dénommé  deuxième tube de Caluire  alors même qu'il n'était pas terminé à la date de la décision du Conseil d'Etat mentionnée ci-dessus dès lors que la communauté urbaine, qui avait consenti à sa réalisation, en a pris possession et a assumé son coût d'achèvement ; qu'il en va de même des locaux administratifs et techniques destinés à la S.C.B.P.N.L qui ont été construits avec l'assentiment de la communauté urbaine et dont certains sont aujourd'hui utilisés par les services de la régie du périphérique ;<br/>
<br/>
              Considérant, en troisième lieu, que le requérant n'établit pas le caractère erroné ou excessif du taux de révision de 5 % appliqué au coût des travaux par les rapports d'experts ; <br/>
<br/>
              Considérant, en dernier lieu, qu'il ressort des pièces du dossier que les difficultés techniques auxquelles ont été confrontés les constructeurs lors du creusement du tube nord du tunnel, en raison de la présence d'éperons rocheux dans la zone dite de transition et de l'abrasivité anormale des matériaux dans la zone alluvionnaire, ont revêtu un caractère exceptionnel ; que les documents et études communiqués aux constructeurs, pourtant établis à partir de sondages géologiques réalisés en nombre suffisant et dans les règles de l'art, ne permettaient pas de les déceler ; que les travaux supplémentaires ainsi rendus nécessaires, sans lesquels la construction de l'ouvrage n'aurait pu être poursuivie, ont été utiles à la collectivité publique ; qu'en acceptant de verser à ce titre la somme de 120 200 000 francs hors taxes (18 324 371,87 euros), au demeurant inférieure à celle retenue par les experts, la communauté urbaine de Lyon n'a pas méconnu l'obligation qui lui est faite de ne pas payer une somme qu'elle ne doit pas ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède qu'en acceptant de verser à la S.C.B.P.N.L, pour solde de tous comptes, la somme de 1 704 123 068 francs (259 791 886,97 euros) en règlement de toutes les conséquences financières de l'arrêt du Conseil d'Etat du 6 février 1998 et de la résiliation du contrat de concession, la communauté urbaine de Lyon ne peut être regardée comme ayant consenti à la société concessionnaire une libéralité et n'a méconnu aucune autre règle d'ordre public  ; que, par suite, M. B n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif  de Lyon a rejeté sa demande dirigée contre la délibération du 21 décembre 1999 ; <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède qu'il n'y a pas lieu de faire droit aux conclusions de M. B présentées sur le fondement de l'article L. 911-1 du code de justice administrative ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de M. B une somme de 3 000 euros au titre des frais exposés par la communauté urbaine de Lyon devant le Conseil d'Etat et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de faire application de ces mêmes dispositions et de mettre une somme de 1 500 euros à la charge de M. B au titre des frais de même nature exposés par la société concessionnaire du boulevard périphérique Nord de Lyon devant la cour administrative d'appel de Douai ; <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai en date du 12 février 2004 est annulé.<br/>
Article 2 : La requête présentée par M. B devant la cour administrative d'appel de Douai est rejetée.<br/>
Article 3 : M. B versera respectivement une somme de 3 000 euros à la communauté urbaine de Lyon et une somme de 1 500 euros à la société concessionnaire du boulevard périphérique Nord de Lyon au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. Etienne B, à la communauté urbaine de Lyon et à la société concessionnaire du boulevard périphérique Nord de Lyon.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05-01-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÉMUNÉRATION DU CO-CONTRACTANT. INDEMNITÉS. - ENRICHISSEMENT SANS CAUSE - A) INOPÉRANCE DE LA FAUTE DE L'APPAUVRI - B) EXCEPTION - CAS DE DOL DE NATURE À VICIER LE CONSENTEMENT DE L'ADMINISTRATION.
</SCT>
<ANA ID="9A"> 39-05-01-02 a) La faute de l'entrepreneur appauvri est sans incidence sur son droit à indemnisation au titre de l'enrichissement sans cause de la collectivité. b) Il doit cependant être fait une exception à ce principe, dans les cas où la faute du quasi-cocontractant est, par sa gravité, assimilable à une fraude ou à un dol de nature à vicier le consentement de l'administration, faisant ainsi obstacle à l'exercice d'une action fondée sur l'enrichissement sans cause.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
