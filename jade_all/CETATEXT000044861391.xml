<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044861391</ID>
<ANCIEN_ID>JG_L_2021_12_000000449731</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/86/13/CETATEXT000044861391.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/12/2021, 449731, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449731</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449731.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E... C... a demandé au tribunal administratif de Nancy d'annuler les opérations électorales qui se sont déroulées le 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Laxou (Meurthe-et-Moselle). Par un jugement du 18 janvier 2021, le tribunal a rejeté sa protestation.<br/>
<br/>
              Par une requête enregistrée le 15 février 2021 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif ;<br/>
<br/>
              2°) de faire droit à sa protestation ;<br/>
<br/>
              3°) d'enjoindre à M. D... B..., maire de la commune de Laxou, de convoquer aux urnes les électeurs dans les trois mois suivant la date à laquelle l'annulation de l'élection sera devenue définitive en application des dispositions de l'article L. 251 du code électoral ;<br/>
<br/>
              4°) de mettre à la charge de M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-760 du 22 juin 2020 ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le décret n° 2020-571 du 14 mai 2020 ;<br/>
              - la décision n° 2020-849 QPC du 17 juin 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP L. Poulet, Odent, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 28 juin 2020 pour l'élection des conseillers municipaux et communautaires dans la commune de Laxou (Meurthe-et-Moselle), la liste " Vivons Laxou ", conduite par M. D... B..., a obtenu 1 756 voix, et la liste " Avec vous pour Laxou ", conduite par Mme E... C..., maire sortante, a obtenu 1 723 voix. Mme C... relève appel du jugement du 18 janvier 2021 par lequel le tribunal administratif de Nancy a rejeté sa protestation contre le scrutin.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. Il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral que, par dérogation aux dispositions de l'article R. 611-1 du code de justice administrative, les tribunaux administratifs ne sont pas tenus d'ordonner la communication des mémoires en défense des conseillers municipaux dont l'élection est contestée aux auteurs des protestations, ni des autres mémoires ultérieurement enregistrés et qu'il appartient seulement aux parties, si elles le jugent utile, de prendre connaissance de ces défenses et mémoires ultérieurs au greffe du tribunal administratif. Ainsi le défaut de communication de ces mémoires n'entache pas la décision juridictionnelle d'irrégularité, même s'ils contiennent des éléments nouveaux. Mme C... ne saurait, dès lors, utilement se prévaloir de ce que le mémoire produit par M. B... le 21 décembre 2020 ne lui avait pas été transmis par Télérecours à la suite de sa demande formulée le 23 décembre 2020 ni en déduire que le tribunal aurait méconnu le principe du contradictoire. <br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              En ce qui concerne les griefs relatifs à des faits antérieurs au premier tour de scrutin :<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 51 du code électoral, dans sa rédaction applicable au scrutin : " Pendant la durée de la période électorale, dans chaque commune, des emplacements spéciaux sont réservés par l'autorité municipale pour l'apposition des affiches électorales. / Dans chacun de ces emplacements, une surface égale est attribuée à chaque candidat, chaque binôme de candidats ou à chaque liste de candidats. / Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, tout affichage relatif à l'élection, même par affiches timbrées, est interdit en dehors de cet emplacement ou sur l'emplacement réservé aux autres candidats, ainsi qu'en dehors des panneaux d'affichage d'expression libre lorsqu'il en existe ".<br/>
<br/>
              4. S'il résulte de l'instruction que l'affiche de M. B... a été apposée sur la vitrine d'un commerce de la commune en méconnaissance des dispositions de l'article L. 51 du code électoral et que son programme complet a été déposé sur le comptoir de ce même commerce, cette circonstance, pour regrettable qu'elle soit, est restée isolée et ne peut être regardée, à elle seule, comme ayant pu altérer la sincérité du scrutin. <br/>
<br/>
              5. En deuxième lieu, aux termes de l'article 52-1 du code électoral : " (...) / A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. (...) ". Il résulte de l'instruction que des cartes de vœux au nom de M. B..., dont la présentation reproduit celle de la première page d'un quotidien de la presse régionale, ont été diffusées sur sa page " Facebook " et que des affiches par lesquelles il présentait ses vœux à la population ont été apposées sur des emplacements d'expression libre en janvier 2020. Toutefois, eu égard à leur contenu, ces publications ne sauraient être regardées, contrairement à ce que soutient Mme C..., comme présentant le caractère d'une campagne de promotion publicitaire des réalisations ou de la gestion de la collectivité, prohibée par les dispositions de l'article L. 52 1 du code électoral et de nature à altérer la sincérité du scrutin.<br/>
<br/>
              6. En troisième lieu, si la diffusion sur le compte " Facebook " de M. B..., parmi les photographies illustrant un message intitulé " vernissage de l'exposition de Françoise à l'hôpital central de Nancy : les agents du CHU ont du talent ... Bravo l'artiste ! ", de la photographie d'une plaque de bois sur laquelle étaient écrits des propos injurieux à l'encontre de Mme C... dépasse les limites de la polémique électorale, il ne résulte pas de l'instruction que cette publication, pour regrettable qu'elle soit et qui n'a été visible que quelques heures, ait eu une influence sur l'issue du scrutin.<br/>
<br/>
              En ce qui concerne les griefs relatifs à des faits intervenus entre les deux tours :<br/>
<br/>
              7. En quatrième lieu, si Mme C... soutient que M. B... aurait pu, en raison de son mandat de député et à la différence des autres candidats, se déplacer sans restriction sur tout le territoire national et en particulier à Laxou, malgré l'interdiction de déplacement en vigueur pendant l'état d'urgence sanitaire, elle n'assortit pas ce grief des précisions permettant d'en apprécier le bien-fondé. <br/>
<br/>
              8. En cinquième lieu, aux termes du deuxième alinéa de l'article L. 52-8 du code électoral, dans sa rédaction applicable au scrutin : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. Les personnes morales, à l'exception des partis et groupements politiques ainsi que des établissements de crédit ou sociétés de financement ayant leur siège social dans un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, ne peuvent ni consentir des prêts à un candidat ni apporter leur garantie aux prêts octroyés aux partis et groupements politiques ".<br/>
<br/>
              9. Il résulte de l'instruction que M. B... a effectué deux commandes de masques en février et mars 2020, la première à hauteur de 1 500 masques obtenus auprès de l'association des maires de Meurthe-et-Moselle, pour un coût total de 1 107,75 euros financé par M. B... sur ses fonds personnels et non inclus à son compte de campagne, et la seconde de 1 000 masques achetés à une société privée, la société Booster Factory, pour un coût total de 900 euros  payé par le mandataire de M. B... et intégré à son compte de campagne, et qu'une partie de ces masques a été distribuée aux habitants de Laxou, bénéficiant aussi à d'autres communes. Toutefois, il ne résulte pas de l'instruction que cette distribution, qui avait pour objet de faciliter la sortie du premier confinement dans un contexte de pandémie liée au covid-19, se soit accompagnée d'une opération de propagande en faveur de la liste conduite par M. B... ni qu'elle aurait méconnu les dispositions de l'article L. 52-8 du code électoral.<br/>
<br/>
              10. En sixième lieu, il résulte de l'instruction que la lettre datée d'avril 2020 diffusée par M. B..., dans laquelle il fait état de la difficulté de le joindre à l'Assemblée nationale par voie postale et invite les administrés à privilégier le courriel et son téléphone portable, se borne à donner des informations pratiques aux administrés sans jamais faire état, même de façon détournée, des élections municipales. A ce titre, elle ne peut être regardée comme constitutive d'une utilisation des moyens de l'Assemblée nationale au profit de la campagne de M. B... pour les élections municipales en méconnaissance de l'article L. 52-8 du code électoral.<br/>
<br/>
              11. Il résulte de tout ce qui précède que Mme C... n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nancy a rejeté sa protestation.<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de Mme C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme E... C... et à M. D... B....<br/>
Copie en sera adressée au ministre de l'intérieur et à la Commission nationale des comptes des comptes de campagne et des financements politiques.<br/>
<br/>
              Délibéré à l'issue de la séance du 2 décembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Françoise Tomé, conseillère d'Etat-rapporteure. <br/>
<br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Françoise Tomé<br/>
<br/>
                 Le secrétaire :<br/>
                 Signé : M. F... A...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
