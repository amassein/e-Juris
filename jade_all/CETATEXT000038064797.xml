<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038064797</ID>
<ANCIEN_ID>JG_L_2019_01_000000426257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/06/47/CETATEXT000038064797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/01/2019, 426257, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:426257.20190123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 13 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, les associations One Voice, Sea Shepherd France, Le Biome, Centre Athenas et Wildlife Angel demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 8 octobre 2018 fixant les règles générales de détention d'animaux d'espèces non domestiques ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que : <br/>
              - leur recours est recevable ;<br/>
              - elles ont intérêt à agir contre l'arrêté litigieux ;<br/>
              - la condition d'urgence est remplie dès lors que l'arrêté litigieux porte une atteinte suffisamment grave et immédiate à la protection de la biodiversité et au bien-être animal, intérêts qu'elles entendent défendre, en ce qu'il réduit le degré de protection de nombreuses espèces sauvages ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté contesté a été pris au terme d'une procédure irrégulière dès lors que, d'une part, la synthèse des observations et propositions du public prévue à l'alinéa 7 de l'article L. 123-19-1 du code de l'environnement n'a pas été rendue publique et, d'autre part, la commission nationale consultative pour la faune sauvage captive n'a pas été consultée en vertu de l'article R. 413-2 du code de l'environnement ;<br/>
              - il a été pris en violation des articles L. 412-1 et suivants du code de l'environnement dès lors qu'il abroge le régime de l'élevage d'agrément et permet la détention et la reproduction d'animaux sauvages menacés d'extinction  sans aucune formalité ;<br/>
              - il a été pris en méconnaissance du principe de non-régression et du principe de prévention, tels qu'ils ressortent de l'article L. 110-1 du code de l'environnement, le passage d'un régime d'autorisation préfectorale préalable à celui d'une simple déclaration pour de nombreux animaux protégés au niveau européen constitue une diminution des garanties de la protection des animaux et de leur bien-être ;<br/>
              - il a été pris en violation de l'article R. 413-9 du code de l'environnement dès lors que le changement de régime relatif à la détention d'animaux d'espèces non domestiques porte atteinte à l'objectif de protection de la nature et des animaux ;  <br/>
              - il méconnaît les articles 5, 6 et 11 de la directive n° 2009/147/CE du 30 novembre 2009 concernant la conservation des oiseaux sauvages et du règlement (CE) n° 338/97 du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce ;<br/>
              - il ne soumet à aucune formalité la détention d'un nombre illimité d'animaux sauvages de jeune âge ;<br/>
              - il a été pris en violation des dispositions de l'article R. 431-30 du code de l'environnement qui prévoient un marquage inamovible des animaux détenus dans un établissement en ce qu'il envisage un marquage par transpondeur à radiofréquences, susceptible d'être enlevé ou modifié ;<br/>
              - il a été pris en violation des dispositions de l'article L. 413-7 du code de l'environnement dès lors que l'attestation de cession d'un animal vivant d'une espèce non domestique peut prendre la forme d'un simple ticket de caisse, ce qui rend la traçabilité des mouvements et l'identification des animaux cédés impossible ;<br/>
              - il ne comporte pas de définition objective du bien-être animal et des conditions nécessaires  pour assurer son respect en captivité.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 10 janvier 2019, le ministre d'Etat, ministre de la transition écologique et solidaire conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
              Par un mémoire, enregistré le 16 janvier 2019, l'association Wildlife Angel déclare se désister purement et simplement de l'instance.<br/>
<br/>
              Par un mémoire en réplique enregistré le 17 janvier 2019, les associations One Voice, Sea Shepherd France, le Biome et Centre Athenas reprennent les conclusions de leur requête par les mêmes moyens.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention sur le commerce international des espèces de faune et de flore sauvages menacées d'extinction signée à Washington le 3 mars 1973 ;<br/>
              - le règlement (CE) n° 338/97 du 9 décembre 1996 relatif à la protection des espèces de faune et de flore sauvages par le contrôle de leur commerce ;<br/>
              - la directive n° 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, les associations One Voice et autres et, d'autre part, le ministre d'Etat, ministre de la transition écologique et solidaire et le ministre de l'agriculture et de l'alimentation ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 17 janvier 2019 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants des associations One Voice et autres ;<br/>
<br/>
              - les représentants du ministre d'Etat, ministre de la transition écologique et solidaire ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Dans un mémoire enregistré au secrétariat du contentieux du Conseil d'Etat, l'association Wildlife Angel a déclaré se désister de sa requête. Son désistement est pur et simple et il y a lieu de lui en donner acte. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              3. La loi du 8 août 2016 pour la reconquête de la biodiversité, de la nature et des paysages a institué, aux côtés du régime d'autorisation préalable existant, un régime de déclaration préalable pour certaines actions concernant des espèces d'animaux non domestiques et de végétaux non cultivés, en raison de la faible gravité de leurs effets sur la conservation des espèces en cause. L'article  L. 412-1 du code de l'environnement, dans sa rédaction issue de cette loi, prévoit désormais que : " La production, le ramassage, la récolte, la capture, la détention, la cession à titre gratuit ou onéreux, à travers tout support, y compris numérique, l'utilisation, le transport, l'introduction quelle qu'en soit l'origine, l'importation sous tous régimes douaniers, l'exportation, la réexportation de tout ou partie d'animaux d'espèces non domestiques et de leurs produits ainsi que de tout ou partie de végétaux d'espèces non cultivées et de leurs produits, dont la liste est fixée par arrêtés conjoints du ministre chargé de l'environnement et, en tant que de besoin, du ou des ministres compétents, s'ils en font la demande, sont soumis, suivant la gravité de leurs effets sur l'état de conservation des espèces concernées et des risques qu'ils présentent pour la santé, la sécurité et la salubrité publiques, à déclaration ou à autorisation de l'autorité administrative délivrée dans les conditions et selon les modalités fixées par un décret en Conseil d'Etat. / ce décret précise également: / 1° les cas dans lesquels les récépissés de déclaration et les autorisations ne peuvent être délivrés qu'à des personnes préalablement habilitées par l'autorité administrative ; / (...). ". <br/>
<br/>
              4. Aux termes de l'article R. 412-1 du même code : " Les arrêtés prévus à l'article L. 412-1, pris par le ministre chargé de l'environnement, précisent les espèces ou les catégories de spécimens d'animaux non domestiques et de végétaux non cultivés concernés, les activités soumises à autorisation ou à déclaration (...) ". Aux termes de l'article R. 412-1-1 du même code : " Le ministre chargé de l'environnement fixe par arrêté : / 1° la forme des déclarations et des demandes d'autorisation ; / 2° les caractéristiques auxquelles doivent répondre les installation de production et de détention ainsi que les règles générales dans le respect desquelles doivent s'exercer les activités mentionnées à l'article L. 412-1 (...). ". Enfin, les articles R. 412-5 et R. 412-6 du même code prévoient que la déclaration doit être adressée à l'autorité compétente dans le territoire, qu'elle peut être transmise par téléservice et qu'elle donne lieu, selon les cas, soit, lorsqu'elle est incomplète, à un simple accusé de réception indiquant les pièces manquantes, soit, lorsqu'elle est complète, à un récépissé de déclaration qui indique la date à laquelle, en l'absence d'opposition, l'opération projetée pourra être entreprise ou bien l'absence d'opposition qui permet d'entreprendre cette opération sans délai. Il est en outre prévu que l'absence de réponse de l'administration pendant un mois vaut délivrance du récépissé. <br/>
<br/>
              5 Pour l'application de ces dispositions, est intervenu l'arrêté du 8 octobre 2018 fixant les règles générales de détention d'animaux d'espèces non domestiques. Dans son chapitre 1er, il prévoit des règles communes à la détention de ces animaux, en particulier des règles d'identification par marquage et enregistrement dans un fichier national, dont il résulte des échanges à l'audience qu'elles sont plus contraignantes que les règles antérieures dès lors que leur application est générale et qu'elles s'appliquent à un nombre nettement plus élevé d'espèces. Dans son chapitre 2, il fixe les règles applicables aux procédures préalables à la détention d'animaux d'espèces non domestiques ; selon ces dispositions, lorsque la détention d'animaux est faite dans un but lucratif ou de négoce, elle est soumise à un régime d'autorisation, en revanche, lorsque tel n'est pas le cas, la détermination de la procédure applicable est faite, selon l'espèce et le nombre de spécimens détenus, par référence à un tableau figurant en annexe II. Par la présente requête, les associations One Voice et autres demandent la suspension de cet arrêté sur le fondement de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              Sur la condition relative à l'urgence :<br/>
<br/>
              6. Pour justifier de l'urgence qu'il y aurait à suspendre les effets de cet arrêté, les associations requérantes soutiennent tout d'abord que l'introduction d'un régime de déclaration préalable et l'abandon du régime antérieur d'autorisation préfectorale des élevages d'agrément constitue un allègement des procédures qui aura pour effet de faire augmenter le nombre de personnes désireuses de détenir des animaux rares et que cette augmentation de la demande se traduira nécessairement par un accroissement des captures et des trafics d'espèces protégées. L'invocation générale d'effets indirects de l'arrêté à raison de l'organisation d'une procédure de déclaration ne saurait établir une urgence. D'une part, ainsi qu'il a été dit au point 3, c'est le législateur qui a prévu la mise en place d'un régime de déclaration. D'autre part, si le régime antérieur des élevages d'agrément fixé par l'arrêté du 10 août 2004, abrogé par l'arrêté litigieux, subordonnait l'ouverture de ces établissement à la délivrance d'une autorisation par le préfet, cette autorisation était, en l'absence d'autorisation ou de refus expresse, réputée accordée à l'expiration d'un délai de deux mois suivant la date du récépissé de dépôt d'une demande complète et il résulte de l'instruction, et en particulier des échanges à l'audience, que la délivrance d'une autorisation tacite était, de fait, le mode le plus fréquent d'autorisation. Enfin, ainsi qu'il a été dit au point 5, l'arrêté énonce dans son chapitre 1er des règles d'identification applicables à tous les spécimens, quel que soit le régime de détention, qui contribuent à renforcer la lutte contre les trafics. <br/>
<br/>
              7. Les associations requérantes soutiennent également pour justifier de l'urgence que, compte tenu des mentions du tableau annexé à l'arrêté, certains animaux particulièrement menacés pourraient, depuis l'entrée en vigueur de cet arrêté, être détenus sans aucune formalité alors que leur détention était jusqu'à l'intervention de ce dernier soumise à un régime particulièrement contraignant. S'agissant de certaines espèces qu'elles désignent dans la requête ou le mémoire en réplique, ces allégations sont soit inexactes soit non assorties de précisions permettant de s'assurer de la réalité et de la gravité d'une menace immédiate. En revanche, s'agissant des jeunes animaux, les associations font valoir que, dans la mesure où le tableau prévoit que les seuils qu'il fixe ne concernent que des animaux adultes, l'arrêté permettrait la détention sans aucune formalité de bébés animaux et de juvéniles appartenant à des espèces protégées et en particulier de jeunes félins dont la détention est à la mode. Le tableau annexé à l'arrêté reprend dans sa partie gauche la liste des espèces selon leur nom scientifique avec l'indication de leur nom vernaculaire et comporte dans sa partie droite sous l'intitulé : " Régime de détention en fonction des effectifs d'animaux adultes ", trois colonnes intitulées " Pas de formalité ", " Déclaration de détention " et " Certificat de capacité et autorisation d'ouverture ". Alors même que l'administration soutient qu'il s'agit d'une erreur de présentation et que, pour la troisième colonne, en ce qui concerne les animaux dont la détention nécessite une autorisation à partir d'un seul spécimen, l'esprit du texte est de soumettre à cette autorisation toute détention, y compris d'un animal n'ayant pas encore atteint l'âge adulte, la formulation retenue dans le tableau annexé à l'arrêté, qui est claire et ne résulte pas d'une simple erreur de plume, n'est pas dans ce sens. Cette formulation pourrait, dans un contexte où existent des réseaux de trafic d'animaux, être utilisée par ces réseaux pour proposer à la vente des bébés animaux en contournant les interdits formulés par d'autres textes et en portant atteinte à la conservation des espèces, au bien-être animal ou à la sécurité des personnes. Il existe donc, dans cette mesure, une situation d'urgence. <br/>
<br/>
              Sur la condition relative au  moyen de nature à créer un doute sérieux :<br/>
              8. Il résulte de l'article L. 412-1 du code de l'environnement rappelé ci-dessus que la détermination de la procédure préalable à la détention d'animaux non domestique doit être faite en tenant compte de la gravité des effets sur l'état de conservation des espèces concernées et des risques qu'ils présentent pour la santé, la sécurité et la salubrité publiques. Par suite, le moyen tiré de ce que, en se référant exclusivement à des effectifs d'animaux adultes en ce qui concerne les animaux dont la détention nécessite une autorisation à partir d'un seul spécimen, sans prévoir de formalité obligatoire préalable à la détention de jeunes animaux appartenant à des espèces protégées ou à des espèces dangereuses, l'arrêté attaqué méconnaîtrait l'article L. 412-1 du code de l'environnement est, en l'état de l'instruction, propre à faire naître un doute sérieux sur l'arrêté contesté en ce qu'il concerne les animaux non adultes. <br/>
              9. Il résulte de ce qui précède que les associations requérantes sont fondées à demander la suspension de l'arrêté du 8 octobre 2018 en tant qu'il prévoit que le seuil à partir duquel est exigé un certificat de capacité et une autorisation d'ouverture à partir d'un seul spécimen est déterminé en fonction des effectifs d'animaux adultes et qu'il abroge dans cette mesure les dispositions de l'arrêté du 10 août 2004 fixant les règles générales de fonctionnement des installations d'élevage d'agrément d'espèces non domestiques. <br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser aux associations requérantes prises ensemble sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il est donné acte du désistement de l'association Wildlife Angel.<br/>
Article 2 : L'exécution de l'arrêté du 8 octobre 2018 fixant les règles générales de détention d'animaux d'espèces non domestiques est suspendue en tant qu'il prévoit que le seuil à partir duquel est exigé un certificat de capacité et une autorisation d'ouverture à partir d'un seul spécimen est déterminé en fonction des effectifs d'animaux adultes et qu'il abroge dans cette mesure les dispositions de l'arrêté du 10 août 2004 fixant les règles générales de fonctionnement des installations d'élevage d'agrément d'espèces non domestiques.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros aux associations One Voice et autres sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions de la requête de l'association One Voice et autres est rejeté.<br/>
Article 5 : La présente ordonnance sera notifiée à l'association One Voice, première dénommée, pour l'ensemble des requérants, et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
Copie en sera adressée au ministre de l'agriculture et de l'alimentation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
