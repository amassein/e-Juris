<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043466306</ID>
<ANCIEN_ID>JG_L_2021_04_000000450435</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/46/63/CETATEXT000043466306.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/04/2021, 450435, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450435</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450435.20210430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le médecin-conseil, chef du service de l'échelon local du service médical du Cher et la caisse primaire d'assurance maladie du Cher ont porté plainte contre M. A... C... devant la section des assurances sociales de la chambre disciplinaire de première instance du Centre-Val de Loire de l'ordre des chirurgiens-dentistes. Par une décision du 12 avril 2018, la section des assurances sociales de la chambre disciplinaire de première instance a infligé à M. C... la sanction de l'interdiction de donner des soins aux assurés sociaux pendant trois ans et lui a ordonné de reverser la somme de 21 855,65 euros à la caisse primaire d'assurance maladie du Cher.<br/>
<br/>
              Par une décision du 15 juin 2020, la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes a, sur appel de M. C..., ramené la somme qu'il lui est ordonné de reverser à la caisse primaire d'assurance maladie du Cher à 21 104,10 euros, rejeté le surplus des conclusions de sa requête, et décidé que la sanction prendra effet au 1er septembre 2020, avec publication.<br/>
<br/>
              Par une requête, enregistrée le 8 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution de cette décision contre laquelle il s'est pourvu en cassation sous le n° 442638 ;<br/>
<br/>
              2°) de mettre solidairement à la charge de la caisse primaire d'assurance maladie du Cher et du médecin-conseil, chef du service de l'échelon local du service médical du Cher, la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ; <br/>
              - le code de la sécurité sociale ; <br/>
              - l'ordonnance n° 2020-1402 du 18 novembre 2020, notamment son article 2 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. C... et à la SCP Foussard, Froger, avocat du médecin-coseil, chef de l'échelon local du service médical du Cher ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle rendue en dernier ressort, l'infirmation de la solution retenue par les juges du fond ".<br/>
<br/>
              2. Aucun des moyens invoqués par M. C..., qui sont tirés, d'une part, de ce que la décision du 15 juin 2020 de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes dont il demande qu'il soit sursis à l'exécution est entachée, en premier lieu, d'insuffisance de motivation, en deuxième lieu, d'erreur de droit et de méconnaissance par la juridiction de son office en ce qu'elle juge qu'il a méconnu les dispositions de l'article R. 315-1-1 du code de la sécurité sociale, en troisième lieu, d'inexacte qualification juridique des faits en ce qu'elle juge qu'il a méconnu les obligations de la nomenclature générale des actes professionnels en surfacturant des actes à trente-sept reprises, en quatrième lieu, d'inexacte qualification juridique des faits en ce qu'elle juge qu'il a méconnu les dispositions de l'article R. 4127-240 du code de la santé publique et de l'article R. 161-40 du code de la sécurité sociale en facturant des actes par anticipation, en cinquième lieu, de dénaturation des pièces du dossier et d'inexactitude matérielle en ce qu'elle juge qu'il a réalisé des actes non conformes aux données acquises de la science, en sixième lieu, de dénaturation des pièces du dossier en ce qu'elle juge qu'il a effectué des actes sans nécessité médicale, en septième lieu, d'inexacte qualification juridique des faits en ce qu'elle retient que les faits constatés constituent des fautes, fraudes ou abus au sens des dispositions de l'article L. 145-1 du code de la sécurité sociale, en huitième lieu, d'inexacte qualification juridique des faits en ce qu'elle retient que les anomalies relevées par le service du contrôle médical constituent des abus d'honoraires et d'autre part, de ce que la sanction prononcée est hors de proportion avec les fautes reprochées et que, par suite, la décision en litige est entachée d'erreur de droit, n'est sérieux et de nature à justifier, outre l'annulation de la décision en cause, l'infirmation de la solution retenue par la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>
              3. L'une des conditions posées par l'article R. 821-5 du code de justice administrative n'étant pas remplie, les conclusions à fins de sursis à exécution de la décision du 15 juin 2020 de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes présentées par M. C... ne peuvent qu'être rejetées.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la caisse primaire d'assurance maladie du Cher et du médecin-conseil, chef du service de l'échelon local du service médical du Cher, qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... C..., au médecin-conseil, chef du service de l'échelon local du service médical du Cher et à la caisse primaire d'assurance maladie du Cher.<br/>
Copie en sera adressée au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
