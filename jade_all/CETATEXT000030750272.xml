<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030750272</ID>
<ANCIEN_ID>JG_L_2015_06_000000388430</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/02/CETATEXT000030750272.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 17/06/2015, 388430, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388430</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:388430.20150617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a transmis au tribunal administratif de Versailles, en application de l'article L. 52-15 du code électoral, la décision du 27 novembre 2014 par laquelle elle a rejeté le compte de campagne de M. A... B..., candidat aux élections municipales de Viroflay (Yvelines) organisées les 23 et 30 mars 2014. Par un jugement n° 1408443 du 3 février 2015, le tribunal administratif de Versailles a déclaré que le compte de campagne du candidat avait été rejeté à bon droit et a proclamé l'inéligibilité de M. B...pour une durée d'un an à compter de la date à laquelle le jugement sera devenu définitif.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 3 mars et 24 avril 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la saisine de la Commission nationale des comptes de campagne et des financements politiques.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, en premier lieu, qu'aux termes de l'article L. 52-4 du code électoral : " Tout candidat à une élection déclare un mandataire conformément aux articles L. 52-5 et L. 52-6 au plus tard à la date à laquelle sa candidature est enregistrée. (...) / Il règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique. Les dépenses antérieures à sa désignation payées directement par le candidat ou à son profit, ou par l'un des membres d'un binôme de candidats ou au profit de ce membre, font l'objet d'un remboursement par le mandataire et figurent dans son compte bancaire ou postal. (...) " ; que l'article L. 52-6 du même code précise qu'à cette fin " (...) Le mandataire financier est tenu d'ouvrir un compte bancaire ou postal unique retraçant la totalité de ses opérations financières. (...) / Les comptes du mandataire sont annexés au compte de campagne du candidat qui l'a désigné ou au compte de campagne du candidat tête de liste lorsque le candidat qui l'a désigné figure sur cette liste. " ; qu'enfin, l'article L. 52-12 du même code dispose que : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que les dépenses engagées directement par le candidat ou à son profit par un tiers antérieurement à la désignation du mandataire financier doivent être remboursées par ce dernier avant le dépôt à la Commission nationale des comptes de campagne et des financements politiques du compte de campagne du candidat, afin notamment que ce remboursement figure au compte bancaire ou postal annexé par le mandataire financier au compte de campagne ; que, toutefois, par dérogation à la formalité substantielle que constitue l'obligation de recourir à un mandataire pour toute dépense effectuée en vue de la campagne, le règlement direct de menues dépenses par le candidat peut être admis, à la double condition que leur montant apprécié à la lumière des dispositions de l'article L. 52-4 du code électoral, c'est-à-dire prenant en compte non seulement les dépenses intervenues après la désignation du mandataire financier mais aussi celles réglées avant cette désignation et qui n'auraient pas fait l'objet d'un remboursement par le mandataire, soit faible tant par rapport au total des dépenses du compte de campagne qu'au regard du plafond de dépenses autorisées fixé par l'article L. 52-11 du code électoral ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M. B...a réglé personnellement une dépense de campagne avant la désignation de son mandataire financier ; qu'il est constant que cette dépense, si elle figurait au compte de campagne du candidat, ne lui a été remboursée par son mandataire financier qu'après le dépôt à la Commission nationale des comptes de campagne et des financements politiques de son compte de campagne ; que M. B... et ses colistiers ont également réglé personnellement des dépenses de campagne postérieurement à la désignation du mandataire financier ; que le montant des dépenses ainsi réglées personnellement par le requérant et ses colistiers après la désignation du mandataire financier ou avant cette désignation et qui n'auraient pas fait l'objet d'un remboursement par le mandataire représente 40 % du total des dépenses du compte de campagne et 16 % du plafond des dépenses autorisées ; que, dans ces conditions, M. B...n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Versailles a jugé que la Commission nationale des comptes de campagne et des financements politiques avait à bon droit rejeté son compte de campagne ; <br/>
<br/>
              4. Considérant, en second lieu, que le troisième alinéa de l'article L. 118-3 du code électoral dispose que le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, " prononce (...) l'inéligibilité du candidat dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales " ; que, pour déterminer si un manquement est d'une particulière gravité au sens de ces dispositions, il incombe au juge de l'élection d'apprécier, d'une part, s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales, d'autre part, s'il présente un caractère délibéré ; qu'en cas de manquement aux dispositions de l'article L. 52-4 du code électoral, il incombe, en outre, au juge de tenir compte de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce ;<br/>
<br/>
              5. Considérant, qu'il résulte de l'instruction que les dépenses électorales réglées par M. B...avant la désignation de son mandataire financier et qui n'ont pas été remboursées par ce dernier avant le dépôt par le candidat de son compte de campagne, contrairement aux dispositions de l'article L. 52-4 du code électoral dont les prescriptions, dépourvues d'ambiguïté, présentent un caractère substantiel, se sont élevées à 3 800 euros, engagés pour l'organisation d'un sondage dont les résultats étaient au coeur de la campagne de l'intéressé ; qu'il n'a régularisé cette irrégularité qu'après l'engagement de la procédure contradictoire avec la commission nationale ; que si le compte de campagne de M. B...ne fait pas apparaître d'autres irrégularités de nature à justifier une déclaration d'inéligibilité, la dépense litigieuse, qui n'a pas été remboursée par le mandataire financier au candidat avant le dépôt de son compte de campagne, représente, ainsi qu'il a été dit, 40 % du montant des dépenses à caractère électoral et 16 % du plafond des dépenses ; que le montant global de ces dépenses, qui ne sont ni faibles par rapport au total des dépenses du compte de campagne, ni négligeables au regard du plafond de dépenses autorisées, n'est pas demeuré limité ; que, compte tenu de la nature et du montant de ces dépenses, les circonstances que le requérant a fait figurer ces dépenses dans son compte de campagne par souci de transparence et qu'il se présentait pour la première fois à une élection et n'avait pas le soutien d'un parti politique ne retirent pas à ce manquement son caractère de particulière gravité ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, compte tenu des montants en cause et  de l'ensemble des circonstances de l'espèce, M. B...doit être regardé comme ayant commis un manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales ; que, par suite, il n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles l'a déclaré inéligible et a proclamé élu le premier candidat suivant de la liste qu'il conduisait ; <br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
Copie en sera adressée, pour information, au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
