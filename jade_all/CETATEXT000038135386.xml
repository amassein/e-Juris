<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038135386</ID>
<ANCIEN_ID>JG_L_2019_02_000000409211</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/13/53/CETATEXT000038135386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 15/02/2019, 409211</TITRE>
<DATE_DEC>2019-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409211</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:409211.20190215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              La société par action simplifiée Danish Crown France a demandé au tribunal administratif d'Orléans d'annuler dix-sept titres de recettes émis le 26 avril 2011 à l'encontre de la société Ess-Food aux droits de laquelle elle vient par l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer) pour un montant global de 526 909,88 euros, incluant le reversement de restitutions à l'exportation à hauteur de 337 597,93 euros, des sanctions pécuniaires à hauteur de 168 799,06 euros et des majorations de 10 % à hauteur de 20 512,89 euros. Par un jugement n° 1103812 du 7 février 2013, le tribunal administratif d'Orléans a fait droit à sa demande et déchargé la société de l'obligation de payer les sommes en cause.<br/>
<br/>
              Par un arrêt n° 13NT00996 du 18 septembre 2014, la cour administrative d'appel de Nantes a rejeté l'appel que FranceAgriMer a formé contre ce jugement.<br/>
<br/>
              Par une décision n° 385935 du 17 mars 2016, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt du 18 septembre 2014 et renvoyé l'affaire devant la cour administrative d'appel de Nantes. <br/>
<br/>
              Par un arrêt n° 16NT00950 du 25 janvier 2017, la cour administrative d'appel de Nantes a annulé le jugement du tribunal administratif d'Orléans du 7 février 2013, en tant qu'il statue sur les titres de recettes n°s 2011000046 à 2011000051 et décharge la société Danish Crown France de son obligation de payer les sommes mises à sa charge par ces titres de recettes, annulé ces mêmes titres de recettes et déchargé la société Danish Crown France de son obligation de payer les sommes mises à sa charge par ceux-ci et rejeté le surplus des conclusions de la requête de FranceAgriMer.<br/>
<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 24 mars 2017 et le 22 juin 2017 au secrétariat du contentieux du Conseil d'Etat, FranceAgriMer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Danish Crown France la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE, EURATOM) n° 2988/95 du Conseil du 18 décembre 1995 ; <br/>
              - le règlement (CE) n° 800/1999 de la Commission du 15 avril 1999 ;<br/>
              - l'arrêt C-59/14 du 6 octobre 2015 de la Cour de justice de l'Union européenne ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de l'établissement public national des produits de l'agriculture et de la mer et à la SCP Waquet, Farge, Hazan, avocat de la société Danish Crown France ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Ess-Food a réalisé, entre le 4 janvier et le 10 novembre 2000, des exportations de viande bovine à destination de la Fédération de Russie et a bénéficié, à ce titre, de restitutions à l'exportation en application du règlement (CE) n° 800/1999 de la Commission du 15 avril 1999 portant modalités communes d'application du régime des restitutions à l'exportation pour les produits agricoles, dont certaines lui ont été versées sous forme d'avances. Un contrôle de l'administration des douanes, réalisé au cours des années 2003, 2004 et 2005, ayant conclu à l'invalidité de certaines preuves d'arrivée à destination des denrées exportées, l'établissement public national des produits de l'agriculture et de la mer (FranceAgriMer) a notifié à la société Ess-Food, par un courrier du 30 août 2011, dix-sept titres de recettes émis le 26 avril 2011 pour un montant global de 526 909,88 euros, par lesquels il lui a demandé le remboursement du montant des restitutions indument payées ou avancées, augmenté d'une sanction et d'une majoration. Par un jugement du 7 février 2013, le tribunal administratif d'Orléans a annulé ces dix-sept titres de recettes et déchargé la société Danish Crown France, laquelle est venue aux droits de la société Ess-Food, de son obligation de payer les sommes mises à sa charge par ces titres, au motif que les créances correspondantes étaient prescrites. L'appel formé contre ce jugement par FranceAgriMer a été rejeté par un arrêt de la cour administrative d'appel de Nantes du 18 septembre 2014. Par une décision du 17 mars 2016, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à cette cour. Par un arrêt du 25 janvier 2017, contre lequel FranceAgriMer se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé le jugement du tribunal administratif d'Orléans en tant qu'il a statué sur les titres de recettes n° 2011000046 à 2011000051, déchargé la société Danish Crown France de son obligation de payer les sommes mises à sa charge par ces titres de recettes et rejeté le surplus des conclusions de la requête de FranceAgriMer, tendant à l'annulation de ce jugement en tant qu'il statuait sur les titres de recettes n° 2011000052 à 2011000062, relatifs à des avances de restitutions à l'exportation. FranceAgriMer se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Il ressort des termes du pourvoi de FranceAgriMer que celui-ci ne conteste l'arrêt attaqué qu'en tant que ce dernier s'est prononcé sur les titres de recettes n° 2011000052 à 2011000062.<br/>
<br/>
              3. En premier lieu, aux termes de l'article 1.2 du règlement (CE, EURATOM) n° 2988/95 du Conseil du 18 décembre 1995 relatif à la protection des intérêts financiers des Communautés européennes : " Est constitutive d'une irrégularité toute violation d'une disposition du droit communautaire résultant d'un acte ou d'une omission d'un opérateur économique qui a ou aurait pour effet de porter préjudice au budget général des Communautés ou à des budgets gérés par celles-ci, soit par la diminution ou la suppression de recettes provenant des ressources propres perçues directement pour le compte des Communautés, soit par une dépense indue ". Selon l'article 3.1 du même règlement : " Le délai de prescription des poursuites est de quatre ans à partir de la réalisation de l'irrégularité visée à l'article 1er paragraphe 1. Toutefois, les réglementations sectorielles peuvent prévoir un délai inférieur qui ne saurait aller en deçà de trois ans. / (...) / La prescription des poursuites est interrompue par tout acte, porté à la connaissance de la personne en cause, émanant de l'autorité compétente et visant à l'instruction ou à la poursuite de l'irrégularité. Le délai de prescription court à nouveau à partir de chaque acte interruptif. / Toutefois, la prescription est acquise au plus tard le jour où un délai égal au double du délai de prescription arrive à expiration sans que l'autorité compétente ait prononcé une sanction, sauf dans les cas où la procédure administrative a été suspendue conformément à l'article 6 paragraphe 1 ". Selon l'article 24.1 du règlement n° 800/1999 de la Commission du 15 avril 1999 portant modalités communes d'application du régime des restitutions à l'exportation pour les produits agricoles : " Sur demande de l'exportateur, les Etats membres avancent tout ou partie du montant de la restitution, dès l'acceptation de la déclaration d'exportation, à condition que soit constituée une garantie dont le montant est égal au montant de cette avance, majoré de 10 % (...) ". Aux termes de l'article 49.2 du même règlement : " Le dossier pour le paiement de la restitution ou la libération de la garantie doit être déposé, sauf cas de force majeure, dans les douze mois suivant la date d'acceptation de la déclaration d'exportation (...) ". <br/>
<br/>
              4. Par un arrêt du 6 octobre 2015 Firma Ernst Kollmer Fleischimport und -export c/ Hauptzollamt Hamburg-Jonas (C-59/14), la Cour de justice de l'Union européenne, statuant sur renvoi préjudiciel, a dit pour droit que les articles 1.2 et 3.1, premier alinéa, du règlement n° 2988/95 du Conseil du 18 décembre 1995 doivent être interprétés en ce sens que, dans des circonstances où la violation d'une disposition du droit de l'Union n'a été détectée qu'après la réalisation d'un préjudice, le délai de prescription commence à courir à partir du moment où tant l'acte ou l'omission d'un opérateur économique constituant une violation du droit de l'Union que le préjudice porté au budget de l'Union ou aux budgets gérés par celle-ci sont survenus, et donc à compter de la plus tardive de ces deux dates. Par le même arrêt, la Cour de Justice a dit pour droit que l'article 1.2 du même règlement doit être interprété en ce sens que, dans de telles circonstances, le préjudice est réalisé dès que la décision d'octroyer définitivement la restitution à l'exportation à l'exportateur concerné a été prise, soit, en cas d'avance, au moment où les garanties correspondantes sont libérées.<br/>
<br/>
              5. L'article 49.2 du règlement n° 800/1999 de la Commission du 15 avril 1999 donne un délai de douze mois à l'opérateur à compter de l'acceptation de la déclaration d'exportation pour déposer le dossier, dont la teneur est précisée à l'article 16 du même règlement, justifiant de la réalisation régulière de l'opération donnant lieu à restitution, et permettant à l'administration de statuer sur l'octroi définitif de celle-ci. Toutefois, lorsque l'opérateur a obtenu avant cette limite le paiement de la restitution ou, en cas d'avance, la libération des garanties, la violation, le cas échéant, du droit de l'Union doit être regardée comme réalisée à la date à laquelle l'opérateur a transmis les documents justificatifs sur lesquels l'administration s'est fondée pour octroyer définitivement la restitution à l'exportation, quand bien même le caractère probant de ces documents et la régularité de l'opération concernée seraient, postérieurement à cette décision, remis en cause.<br/>
<br/>
              6. En jugeant, pour l'application de l'article 3 du règlement n° 2998/95, que la violation du droit de l'Union devait être regardée comme constituée au moment de la transmission par la société requérante des pièces justificatives exigées par l'article 16 du règlement n° 800/1999, d'une part, que le préjudice au budget de l'Union était constitué le 20 novembre 2001, date de la libération des dernières garanties, d'autre part, et que cette dernière date, étant la plus tardive des deux, constituait le point de départ de la prescription, la cour administrative d'appel n'a donc pas commis d'erreur de droit.<br/>
<br/>
              7. En second lieu, la cour, ayant jugé sans erreur de droit, comme il vient d'être dit, que la prescription était acquise à compter de novembre 2005, elle ne s'est pas prononcée sur l'éventuelle suspension du délai " butoir " fixé par le dernier alinéa de l'article 3.1 du règlement n° 2998/95. Le moyen tiré de l'erreur de droit qu'elle aurait commise en ne jugeant pas que ce délai avait été suspendu, du 8 décembre 2008 au 3 janvier 2011, par la procédure pénale engagée à l'encontre de la société Danish Crown France est donc sans influence sur le bien-fondé de l'arrêt attaqué. <br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de FranceAgriMer doit être rejeté.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de FranceAgriMer le versement d'une somme de 3 000 euros à la société Danish Crown France au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'il soit mis à la charge de la société Danish Crown France, qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par FranceAgriMer.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'établissement public national des produits de l'agriculture et de la mer est rejeté.<br/>
Article 2 : L'établissement public national des produits de l'agriculture et de la mer versera à la société Danish Crown France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'établissement public national des produits de l'agriculture et de la mer et à la société Danish Crown France. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-01 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. GÉNÉRALITÉS. - RESTITUTIONS À L'EXPORTATION - REMBOURSEMENT DES MONTANTS INDÛMENT PERÇUS - POINT DE DÉPART DU DÉLAI DE PRESCRIPTION (RÈGLEMENT (CE, EURATOM) N° 2988/95 DU CONSEIL DU 18 DÉCEMBRE 1995) - 1) PRINCIPE [RJ1] - DATE LA PLUS TARDIVE ENTRE CELLE DE LA VIOLATION DU DROIT DE L'UNION ET CELLE DE LA RÉALISATION DU PRÉJUDICE PORTÉ AU BUDGET DE L'UNION [RJ2] - PRÉJUDICE RÉALISÉ DÈS QUE LA DÉCISION D'OCTROYER DÉFINITIVEMENT LA RESTITUTION À L'EXPORTATION À L'EXPORTATEUR CONCERNÉ A ÉTÉ PRISE, SOIT, EN CAS D'AVANCE, AU MOMENT OÙ LES GARANTIES CORRESPONDANTES SONT LIBÉRÉES - 2) APPLICATION - DATE DE LA VIOLATION DU DROIT DE L'UE - CAS DANS LEQUEL L'OPÉRATEUR A OBTENU, AVANT L'EXPIRATION DU DÉLAI DE DOUZE MOIS DONT IL DISPOSE POUR DÉPOSER LE DOSSIER JUSTIFIANT DE LA RÉALISATION RÉGULIÈRE DE L'OPÉRATION DONNANT LIEU À RESTITUTION (ART. 49.2 DU RÈGLEMENT N° 800/1999), LE PAIEMENT DE LA RESTITUTION OU LA LIBÉRATION DES GARANTIES - DATE À LAQUELLE L'OPÉRATEUR A TRANSMIS LES DOCUMENTS JUSTIFICATIFS SUR LESQUELS L'ADMINISTRATION S'EST FONDÉE POUR OCTROYER DÉFINITIVEMENT LA RESTITUTION À L'EXPORTATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-14 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. POLITIQUE AGRICOLE COMMUNE. - RESTITUTIONS À L'EXPORTATION - REMBOURSEMENT DES MONTANTS INDÛMENT PERÇUS - POINT DE DÉPART DU DÉLAI DE PRESCRIPTION (RÈGLEMENT (CE, EURATOM) N° 2988/95 DU CONSEIL DU 18 DÉCEMBRE 1995) - 1) PRINCIPE [RJ1] - DATE LA PLUS TARDIVE ENTRE CELLE DE LA VIOLATION DU DROIT DE L'UNION ET CELLE DE LA RÉALISATION DU PRÉJUDICE PORTÉ AU BUDGET DE L'UNION [RJ2] - PRÉJUDICE RÉALISÉ DÈS QUE LA DÉCISION D'OCTROYER DÉFINITIVEMENT LA RESTITUTION À L'EXPORTATION À L'EXPORTATEUR CONCERNÉ A ÉTÉ PRISE, SOIT, EN CAS D'AVANCE, AU MOMENT OÙ LES GARANTIES CORRESPONDANTES SONT LIBÉRÉES - 2) APPLICATION - DATE DE LA VIOLATION DU DROIT DE L'UE - CAS DANS LEQUEL L'OPÉRATEUR A OBTENU, AVANT L'EXPIRATION DU DÉLAI DE DOUZE MOIS DONT IL DISPOSE POUR DÉPOSER LE DOSSIER JUSTIFIANT DE LA RÉALISATION RÉGULIÈRE DE L'OPÉRATION DONNANT LIEU À RESTITUTION (ART. 49.2 DU RÈGLEMENT N° 800/1999), LE PAIEMENT DE LA RESTITUTION OU LA LIBÉRATION DES GARANTIES - DATE À LAQUELLE L'OPÉRATEUR A TRANSMIS LES DOCUMENTS JUSTIFICATIFS SUR LESQUELS L'ADMINISTRATION S'EST FONDÉE POUR OCTROYER DÉFINITIVEMENT LA RESTITUTION À L'EXPORTATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-08 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. LITIGES RELATIFS AU VERSEMENT D`AIDES DE L'UNION EUROPÉENNE. - RESTITUTIONS À L'EXPORTATION - REMBOURSEMENT DES MONTANTS INDÛMENT PERÇUS - POINT DE DÉPART DU DÉLAI DE PRESCRIPTION (RÈGLEMENT (CE, EURATOM) N° 2988/95 DU CONSEIL DU 18 DÉCEMBRE 1995) - 1) PRINCIPE [RJ1] - DATE LA PLUS TARDIVE ENTRE CELLE DE LA VIOLATION DU DROIT DE L'UNION ET CELLE DE LA RÉALISATION DU PRÉJUDICE PORTÉ AU BUDGET DE L'UNION [RJ2] - PRÉJUDICE RÉALISÉ DÈS QUE LA DÉCISION D'OCTROYER DÉFINITIVEMENT LA RESTITUTION À L'EXPORTATION À L'EXPORTATEUR CONCERNÉ A ÉTÉ PRISE, SOIT, EN CAS D'AVANCE, AU MOMENT OÙ LES GARANTIES CORRESPONDANTES SONT LIBÉRÉES - 2) APPLICATION - DATE DE LA VIOLATION DU DROIT DE L'UE - CAS DANS LEQUEL L'OPÉRATEUR A OBTENU, AVANT L'EXPIRATION DU DÉLAI DE DOUZE MOIS DONT IL DISPOSE POUR DÉPOSER LE DOSSIER JUSTIFIANT DE LA RÉALISATION RÉGULIÈRE DE L'OPÉRATION DONNANT LIEU À RESTITUTION (ART. 49.2 DU RÈGLEMENT N° 800/1999), LE PAIEMENT DE LA RESTITUTION OU LA LIBÉRATION DES GARANTIES - DATE À LAQUELLE L'OPÉRATEUR A TRANSMIS LES DOCUMENTS JUSTIFICATIFS SUR LESQUELS L'ADMINISTRATION S'EST FONDÉE POUR OCTROYER DÉFINITIVEMENT LA RESTITUTION À L'EXPORTATION.
</SCT>
<ANA ID="9A"> 03-05-01 1) Par un arrêt du 6 octobre 2015 Firma Ernst Kollmer Fleischimport und -export c/ Hauptzollamt Hamburg-Jonas (C-59/14), la Cour de justice de l'Union européenne a dit pour droit que les articles 1.2 et 3.1, premier alinéa, du règlement n° 2988/95 du Conseil du 18 décembre 1995 doivent être interprétés en ce sens que, dans des circonstances où la violation d'une disposition du droit de l'Union n'a été détectée qu'après la réalisation d'un préjudice, le délai de prescription commence à courir à partir du moment où tant l'acte ou l'omission d'un opérateur économique constituant une violation du droit de l'Union que le préjudice porté au budget de l'Union ou aux budgets gérés par celle-ci sont survenus, et donc à compter de la plus tardive de ces deux dates. Par le même arrêt, la Cour de Justice a dit pour droit que l'article 1.2 du même règlement doit être interprété en ce sens que, dans de telles circonstances, le préjudice est réalisé dès que la décision d'octroyer définitivement la restitution à l'exportation à l'exportateur concerné a été prise, soit, en cas d'avance, au moment où les garanties correspondantes sont libérées.,,2) L'article 49.2 du règlement n° 800/1999 de la Commission du 15 avril 1999 donne un délai de douze mois à l'opérateur à compter de l'acceptation de la déclaration d'exportation pour déposer le dossier, dont la teneur est précisée à l'article 16 du même règlement, justifiant de la réalisation régulière de l'opération donnant lieu à restitution, et permettant à l'administration de statuer sur l'octroi définitif de celle-ci. Toutefois, lorsque l'opérateur a obtenu avant cette limite le paiement de la restitution ou, en cas d'avance, la libération des garanties, la violation, le cas échéant, du droit de l'Union doit être regardée comme réalisée à la date à laquelle l'opérateur a transmis les documents justificatifs sur lesquels l'administration s'est fondée pour octroyer définitivement la restitution à l'exportation, quand bien même le caractère probant de ces documents et la régularité de l'opération concernée seraient, postérieurement à cette décision, remis en cause.</ANA>
<ANA ID="9B"> 15-05-14 1) Par un arrêt du 6 octobre 2015 Firma Ernst Kollmer Fleischimport und -export c/ Hauptzollamt Hamburg-Jonas (C-59/14), la Cour de justice de l'Union européenne a dit pour droit que les articles 1.2 et 3.1, premier alinéa, du règlement n° 2988/95 du Conseil du 18 décembre 1995 doivent être interprétés en ce sens que, dans des circonstances où la violation d'une disposition du droit de l'Union n'a été détectée qu'après la réalisation d'un préjudice, le délai de prescription commence à courir à partir du moment où tant l'acte ou l'omission d'un opérateur économique constituant une violation du droit de l'Union que le préjudice porté au budget de l'Union ou aux budgets gérés par celle-ci sont survenus, et donc à compter de la plus tardive de ces deux dates. Par le même arrêt, la Cour de Justice a dit pour droit que l'article 1.2 du même règlement doit être interprété en ce sens que, dans de telles circonstances, le préjudice est réalisé dès que la décision d'octroyer définitivement la restitution à l'exportation à l'exportateur concerné a été prise, soit, en cas d'avance, au moment où les garanties correspondantes sont libérées.,,2) L'article 49.2 du règlement n° 800/1999 de la Commission du 15 avril 1999 donne un délai de douze mois à l'opérateur à compter de l'acceptation de la déclaration d'exportation pour déposer le dossier, dont la teneur est précisée à l'article 16 du même règlement, justifiant de la réalisation régulière de l'opération donnant lieu à restitution, et permettant à l'administration de statuer sur l'octroi définitif de celle-ci. Toutefois, lorsque l'opérateur a obtenu avant cette limite le paiement de la restitution ou, en cas d'avance, la libération des garanties, la violation, le cas échéant, du droit de l'Union doit être regardée comme réalisée à la date à laquelle l'opérateur a transmis les documents justificatifs sur lesquels l'administration s'est fondée pour octroyer définitivement la restitution à l'exportation, quand bien même le caractère probant de ces documents et la régularité de l'opération concernée seraient, postérieurement à cette décision, remis en cause.</ANA>
<ANA ID="9C"> 15-08 1) Par un arrêt du 6 octobre 2015 Firma Ernst Kollmer Fleischimport und -export c/ Hauptzollamt Hamburg-Jonas (C-59/14), la Cour de justice de l'Union européenne a dit pour droit que les articles 1.2 et 3.1, premier alinéa, du règlement n° 2988/95 du Conseil du 18 décembre 1995 doivent être interprétés en ce sens que, dans des circonstances où la violation d'une disposition du droit de l'Union n'a été détectée qu'après la réalisation d'un préjudice, le délai de prescription commence à courir à partir du moment où tant l'acte ou l'omission d'un opérateur économique constituant une violation du droit de l'Union que le préjudice porté au budget de l'Union ou aux budgets gérés par celle-ci sont survenus, et donc à compter de la plus tardive de ces deux dates. Par le même arrêt, la Cour de Justice a dit pour droit que l'article 1.2 du même règlement doit être interprété en ce sens que, dans de telles circonstances, le préjudice est réalisé dès que la décision d'octroyer définitivement la restitution à l'exportation à l'exportateur concerné a été prise, soit, en cas d'avance, au moment où les garanties correspondantes sont libérées.,,2) L'article 49.2 du règlement n° 800/1999 de la Commission du 15 avril 1999 donne un délai de douze mois à l'opérateur à compter de l'acceptation de la déclaration d'exportation pour déposer le dossier, dont la teneur est précisée à l'article 16 du même règlement, justifiant de la réalisation régulière de l'opération donnant lieu à restitution, et permettant à l'administration de statuer sur l'octroi définitif de celle-ci. Toutefois, lorsque l'opérateur a obtenu avant cette limite le paiement de la restitution ou, en cas d'avance, la libération des garanties, la violation, le cas échéant, du droit de l'Union doit être regardée comme réalisée à la date à laquelle l'opérateur a transmis les documents justificatifs sur lesquels l'administration s'est fondée pour octroyer définitivement la restitution à l'exportation, quand bien même le caractère probant de ces documents et la régularité de l'opération concernée seraient, postérieurement à cette décision, remis en cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CJUE, 6 octobre 2015,,, aff. C-59/14,,[RJ2] Ab. jur., sur ce point, CE, 2 avril 2015, Etablissement public national des produits de l'agriculture et de la mer (FranceAgrimer), n° 371042, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
