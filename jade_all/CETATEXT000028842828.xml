<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028842828</ID>
<ANCIEN_ID>JG_L_2014_04_000000352473</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/84/28/CETATEXT000028842828.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 11/04/2014, 352473, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352473</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:352473.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 352473, la requête, enregistrée le 7 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association "la Ligue des droits de l'homme", dont le siège est 138, rue Marcadet à Paris (75018), représentée par son président, par le Syndicat des avocats de France, dont le siège est 34, rue Saint-Lazare, à Paris (75009) représenté par sa présidente et par la section française de l'Observatoire international des prisons, dont le siège est 7 bis, rue Riquet à Paris (75019), représentée par sa présidente ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-817 du 6 juillet 2011 portant création d'un traitement de données à caractère personnel relatif à la gestion informatisée des détenus en établissement (GIDE) ;<br/>
<br/>
              2°) d'enjoindre, sur le fondement des dispositions des articles L. 911-1 et L. 911-3 du code de justice administrative, au directeur de l'administration pénitentiaire de faire procéder, dans un délai de quinze jours à compter de l'arrêt à intervenir, à la désinstallation du cahier électronique de liaison (CEL) et de détruire les données collectées pour son alimentation, sous astreinte de 400 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 352527, la requête sommaire et le mémoire complémentaire, enregistrés les 8 septembre et 8 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Conseil national de l'ordre des médecins, dont le siège est 180, boulevard Haussmann, à Paris (75009), représenté par son président ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
     ............................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la charte des droits fondamentaux de l'Union européenne ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 2009-1436 du 24 novembre 2009 ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978 ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la Ligue des droits de l'homme et autres et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de la Ligue des droits de l'homme, du Syndicat des avocats de France, de la section française de l'Observatoire international des prisons d'une part, et du Conseil national de l'ordre des médecins d'autre part, sont dirigées contre le même décret du 6 juillet 2011 portant création d'un traitement de données à caractère personnel relatif à la gestion informatisée des détenus en établissement (GIDE) ; qu'il y a lieu de les joindre pour statuer par une même décision ; <br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              En ce qui concerne la consultation du Conseil d'Etat :<br/>
<br/>
              2. Considérant qu'il ressort des pièces versées au dossier par le ministre que le décret attaqué ne contient aucune disposition qui différerait de celles du projet soumis par le Gouvernement au Conseil d'Etat ou du texte adopté par ce dernier ; que dès lors, les requérants ne sont pas fondés à soutenir que la consultation du Conseil d'Etat sur le décret attaqué aurait été entachée d'irrégularité ;<br/>
<br/>
              En ce qui concerne le contreseing du ministre chargé de la santé :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que s'agissant d'un acte réglementaire, les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures individuelles ou réglementaires que comporte nécessairement l'exécution de cet acte ; que le décret attaqué n'appelle aucune mesure de cette nature de la part du ministre chargé de la santé ; que, par suite, il a pu être légalement pris sans que ce ministre y appose son contreseing ;<br/>
<br/>
              En ce qui concerne la motivation de l'avis de la Commission nationale de l'informatique et des libertés :<br/>
<br/>
              4. Considérant qu'il n'est pas contesté que les dispositions de l'article 10 du décret attaqué aux termes desquelles " Le droit d'opposition prévu à l'article 38 de la loi du 6 janvier 1978 susvisé ne s'applique pas au présent traitement " ont été soumises à la Commission nationale de l'informatique et des libertés (CNIL) ; que par suite, la circonstance que dans son avis, public et motivé, rendu le 20 janvier 2011, elle ne se soit pas expressément prononcée sur les dispositions de cet article 10 est sans incidence sur la légalité de ce dernier ; <br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne le droit au respect de la vie privée :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; qu'aux termes de l'article 7 de la charte européenne des droits fondamentaux de l'Union européenne : " Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de ses communications " ; que l'article 5 de la convention du Conseil de l'Europe du 28 janvier 1981 pour la protection des personnes à l'égard du traitement automatisé des données à caractère personnel stipule que ces données sont : " (...) c) adéquates, pertinentes et non excessives par rapport aux finalités pour lesquelles elles sont enregistrées " ; qu'aux termes, enfin, de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : (...) 2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités (...) /3° Elles sont adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs " ;<br/>
<br/>
              6. Considérant qu'il résulte de l'ensemble de ces stipulations et dispositions que l'ingérence dans l'exercice du droit de toute personne au respect de sa vie privée que constituent la collection, la conservation et le traitement, par une autorité publique, d'informations personnelles nominatives ne peut être légalement autorisée que si elle répond à des finalités légitimes et que le choix, la collecte et le traitement des données sont effectués de manière adéquate et proportionnée au regard de ces finalités ; <br/>
<br/>
              7. Considérant, en premier lieu, que l'article 1er du décret attaqué dispose que le traitement GIDE, qui a pour finalités l'exécution des sentences pénales et des décisions de justice s'y rattachant, la gestion de la détention des personnes placées sous main de justice et écrouées ainsi que la sécurité des détenus et des personnels et la mise en oeuvre dans les meilleures conditions d'efficacité et de coordination de l'ensemble des actions relatives au parcours de la personne détenue, permet la prise en charge des personnes détenues ; que l'article 2 précise que celle-ci donne lieu à la tenue d'un cahier électronique de liaison (CEL) destiné à faciliter la mise en oeuvre du parcours de détention, la prévention des comportements à risques, la tenue de la commission pluridisciplinaire unique de l'établissement pénitentiaire ainsi que la gestion des requêtes, des audiences, des rendez-vous, des visites et du courrier des détenus ; que les finalités ainsi assignées au CEL déterminent de manière explicite la population visée, les conditions dans lesquelles les données peuvent être collectées et les objectifs poursuivis ; que la situation d'entière dépendance dans laquelle se trouvent les personnes détenues vis-à-vis de l'administration pénitentiaire, à laquelle elles doivent s'adresser y compris pour les actes de la vie courante, légitime dans son principe la collecte et le traitement de l'ensemble des informations recueillies au quotidien qui se rapportent à l'existence matérielle, au bien être, à la sécurité et à la santé des intéressés ; que, par suite, les finalités assignées par le décret attaqué au CEL sont déterminées, explicites et légitimes eu égard à la mission du service public pénitentiaire, telle que définie par la loi du 24 novembre 2009 pénitentiaire, qui fixe notamment pour obligation à l'administration de prévenir la récidive, de contribuer à l'insertion ou à la réinsertion des personnes détenues, et d'assurer la protection effective de leur intégrité physique, dans tous lieux collectifs et individuels ;<br/>
<br/>
              8. Considérant, en second lieu, que le 3° de l'article 4 du décret attaqué énumère limitativement les données à caractère personnel pouvant être enregistrées pour la prise en charge des personnes détenues et qui, par suite, ne peuvent être regardées comme illimitées ; qu'il prévoit notamment en premier lieu, au e) la collecte de données portant sur les relations du détenu avec autrui, en deuxième lieu, au h) la consignation des observations des personnels pénitentiaires, des agents de la protection judiciaire de la jeunesse, des membres de la commission pluridisciplinaire unique, des personnels de santé, de l'éducation nationale, ainsi que des agents des groupements privés chargés de missions de service public dans le cadre de la gestion déléguée, et en troisième lieu, pour évaluer le risque de suicide des détenus, aux 5 et 6 du j, distinguant les personnes majeures des personnes mineures, la collecte de données, formulées sous forme de questions auxquelles il est répondu au cours d'entretiens avec les intéressés, relatives aux facteurs familiaux, sociaux, économiques, sanitaires, à la situation judiciaire et pénitentiaire, au comportement de l'intéressé, à l'évaluation de l'urgence, aux moyens envisagés par le détenu pour le passage à l'acte et aux mesures à prendre ; que les données ainsi collectées par des personnes appartenant aux catégories limitativement énumérées à l'article 5 du décret attaqué, individuellement désignées et spécialement habilitées par le chef d'établissement, doivent être strictement nécessaires à la prise en charge des personnes détenues, finalité mentionnée au 2e de l'article 1er du décret attaqué ; que dès lors qu'elles ne peuvent être collectées qu'à cette condition, posée à son article 3, elles sont pertinentes et en adéquation avec les finalités du CEL ; qu'elles ne portent pas au droit des individus au respect de leur vie privée une atteinte disproportionnée par rapport aux objectifs de gestion de la détention des personnes placées sous main de justice et écrouées et de prévention des comportements à risques, qui sont au nombre de ces finalités ; <br/>
<br/>
              En ce qui concerne le fondement de la création du cahier électronique de liaison :<br/>
<br/>
              9. Considérant qu'aux termes de l'article 26 de la loi du 6 janvier 1978 : " I. - Sont autorisés par arrêté du ou des ministres compétents, pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat et : / 1° Qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique. / 2° Ou qui ont pour objet la prévention, la recherche, la constatation ou la poursuite des infractions pénales ou l'exécution des condamnations pénales ou des mesures de sûreté (...) / II. - Ceux de ces traitements qui portent sur des données mentionnées au I de l'article 8 sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la commission ; cet avis est publié avec le décret autorisant le traitement (...) " ; que c'est à bon droit que la création du CEL est fondée sur ces dispositions, l'une des finalités du CEL étant de faciliter la prévention des comportements à risques et la tenue de la commission pluridisciplinaire unique, chargée notamment de préparer les aménagements de peine ou la libération des personnes détenues ; <br/>
<br/>
              En ce qui concerne l'accès aux données :<br/>
<br/>
              10. Considérant qu'aux termes de l'article 34 de la loi du 6 janvier 1978 : " Le responsable du traitement est tenu de prendre toutes précautions utiles, au regard de la nature des données et des risques présentés par le traitement, pour préserver la sécurité des données et, notamment, empêcher qu'elles soient déformées, endommagées, ou que des tiers non autorisés y aient accès " ; que ces dispositions, qui sont relatives aux obligations du responsable du traitement dans le fonctionnement de ce dernier, ne peuvent être utilement invoquées à l'appui de conclusions dirigées contre l'acte portant création du traitement automatisé ; <br/>
<br/>
              En ce qui concerne le secret médical et la confidentialité du bilan de santé relatif aux addictions et du motif d'écrou :<br/>
<br/>
              11. Considérant qu'aux termes de l'article 45 de la loi du 24 novembre 2009 pénitentiaire : " L'administration pénitentiaire respecte le droit au secret médical des personnes détenues ainsi que le secret de la consultation, dans le respect des troisième et quatrième alinéas de l'article L. 6141-5 du code de la santé publique " ; que ces derniers alinéas disposent que dès lors qu'il existe un risque sérieux pour la sécurité des personnes, au sein des établissements publics de santé destinés à l'accueil des personnes incarcérées ou des établissements pénitentiaires, les personnels soignants ayant connaissance de ce risque sont tenus de le signaler dans les plus brefs délais au directeur de l'établissement en lui transmettant, dans le respect du secret médical, les informations utiles à la mise en oeuvre des mesures de protection ; qu'aux termes de l'article D. 382 du code de procédure pénale : " Les médecins (...) délivrent aux autorités pénitentiaires des attestations écrites contenant les renseignements strictement nécessaires à l'orientation du détenu ainsi qu'aux modifications ou aux aménagements du régime pénitentiaire que pourrait justifier son état de santé " ; <br/>
<br/>
              12. Considérant que peuvent seuls accéder aux données issues des entretiens des détenus avec les services médicaux, en application de l'article 5 du décret attaqué, d'une part, dans la limite du besoin d'en connaître, les personnels centraux et déconcentrés de la direction de l'administration pénitentiaire ainsi que les membres de la commission pluridisciplinaire unique et, d'autre part, après avoir été individuellement désignés et spécialement habilités par le chef d'établissement, les agents du service pénitentiaire d'insertion et de probation, l'encadrement des personnels de surveillance, les agents des services déconcentrés du secteur public de la protection judiciaire de la jeunesse et les personnels hospitaliers des unités de consultation et de soins ambulatoires, des services médicaux psychologiques régionaux, des unités hospitalières sécurisées interrégionales, des unités hospitalières spécialement aménagées et de l'établissement public de santé national de Fresnes ; que peuvent en être destinataires, en application de l'article 6, les juridictions étrangères et les gouvernements étrangers dans le cadre de la procédure d'extradition, d'un mandat d'arrêt européen ou d'une remise temporaire pour les consignes judiciaires, médicales ou pénitentiaires ;<br/>
<br/>
              13. Considérant, en premier lieu, que le moyen tiré de ce qu'un nombre excessif de personnes pourrait être habilité à accéder à ces informations doit être écarté, dès lors que l'accès est limité au besoin d'en connaître par les différents professionnels dans l'accomplissement de leurs missions ; que, de même, les conditions matérielles dans lesquelles il est allégué que l'accès à ces informations s'effectuerait dans la mise en oeuvre du traitement autorisé par le décret litigieux sont sans incidence sur la légalité de ce décret ;<br/>
<br/>
              14. Considérant, en deuxième lieu, que le CEL comporte, dans la rubrique j) intitulée " prise en charge pluridisciplinaire du détenu " une sous-rubrique j2) : " entretien avec les services médicaux, sous forme d'indication oui/non/ne se prononce pas : antécédents placement SMPR (services médicaux psychologiques régionaux), antécédents placement UMD (unités pour malades difficiles), antécédents d'hospitalisation d'office, nécessite un suivi somatique, suivi psychologique ou psychiatrique antérieur ou en cours, régime alimentaire particulier, grève de la faim ou de la soif, prescription d'une douche médicale, automutilations graves, fumeur, addictions, aptitude au sport, aptitude au travail ; " au regard de laquelle doivent être inscrites les mentions " oui/non/ne se prononce pas " ; que la collecte de ces données n'est pas de nature à entacher d'illégalité le décret attaqué, dès lors qu'elles ne comportent aucune motivation médicale et que les restrictions qu'elles apportent au secret médical et à la confidentialité du bilan de santé proposé à la personne détenue, lors de son incarcération, relatif à sa consommation de produits stupéfiants, d'alcool et de tabac en application de l'article 51 de la loi du 24 novembre 2009, sont nécessaires au respect de la double obligation de protection effective de l'intégrité des personnes détenues, comme des personnels pénitentiaires, et d'individualisation de leur régime de détention, posé par cette loi ; <br/>
<br/>
              15. Considérant, en troisième lieu, que la mention, dans la rubrique b) du 3° de l'article 4 du décret attaqué, des " faits/objets de la condamnation " ne méconnaît pas les dispositions de l'article 42 de cette même loi, qui imposent le dépôt au greffe de l'établissement pénitentiaire, par les détenus, de leurs documents personnels mentionnant leur motif d'écrou, dès lors que cette mention n'est pas accessible aux codétenus mais uniquement aux personnes habilités à accéder au CEL ;<br/>
<br/>
              En ce qui concerne le secret professionnel auquel sont soumis les membres du service pénitentiaire d'insertion et de probation :<br/>
<br/>
              16. Considérant qu'en application de l'article D. 581 du code de procédure pénale, les membres du service pénitentiaire d'insertion et de probation, qui sont tenus au secret professionnel, doivent fournir aux services de l'administration pénitentiaire, à chaque fois que la demande leur en est faite ou à leur initiative, les éléments permettant de mieux individualiser la situation des personnes placées sous main de justice ; que, dès lors, le décret attaqué ne méconnait pas ces dispositions en prévoyant, au 3 du j) du 3° de l'article 4, que le CEL comporte une rubrique " entretien avec le service pénitentiaire d'insertion ou de probation ou avec le service de la protection judiciaire de la jeunesse : sous la forme oui/non : mention de l'indigence, mention d'absence de soutien financier extérieur, nom du conseiller d'insertion et de probation référent, revenus, condamnations pécuniaires, travail au moment de l'incarcération, type de contrat, situation par rapport au logement et existence de logement stable, observations " ;<br/>
<br/>
              En ce qui concerne la dérogation au droit d'opposition :<br/>
<br/>
              17. Considérant qu'aux termes de l'article 38 de la loi du 6 janvier 1978 : " Toute personne physique a le droit de s'opposer, pour des motifs légitimes, à ce que des données à caractère personnel la concernant fassent l'objet d'un traitement. / (...) Les dispositions du premier alinéa ne s'appliquent pas (...) lorsque l'application de ces dispositions a été écartée par une disposition expresse de l'acte autorisant le traitement. " ; que si l'exercice, pour des motifs légitimes, d'un droit d'opposition à ce que des informations personnelles soient collectées au sein d'un traitement de données est au nombre des garanties de la protection de la vie privée devant régir de tels traitements, la suppression de ce droit d'opposition, pour un motif d'intérêt général et afin d'assurer l'effectivité des finalités du traitement en cause, sous le contrôle de la CNIL et du juge, ne méconnaît pas les exigences de la protection de la vie privée résultant de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que le deuxième alinéa de cet article réserve l'ingérence d'une autorité publique dans l'exercice de ce droit, prévue par la loi, à des mesures nécessaires, notamment, à " la défense de l'ordre ", " la protection de la santé " ou " la protection des droits et libertés d'autrui " si cette ingérence est proportionnée à la finalité poursuivie et que les garanties appropriées sont prévues par la loi ; <br/>
<br/>
              18. Considérant qu'en excluant l'application du droit d'opposition pour le traitement qu'il créé, l'article 10 du décret attaqué entend d'une part, concilier l'intérêt général qui s'attache au maintien de la sécurité et du bon ordre dans les établissements pénitentiaires, à la prévention de la récidive et à la protection effective de l'intégrité physique des personnes détenues, avec la protection de la vie privée et, d'autre part, assurer l'effectivité des finalités poursuivies par le traitement en cause, en ne permettant pas aux personnes détenues de s'opposer au recensement des informations destinées principalement à faciliter la mise en oeuvre de leur parcours de détention, la prévention des comportements à risques et la tenue de la commission pluridisciplinaire unique de l'établissement pénitentiaire ; que, dès lors, en recourant à la dérogation au droit d'opposition prévu par l'article 38 de la loi du 6 janvier 1978, le décret attaqué ne porte pas une atteinte disproportionnée au droit garanti par l'article 8 de la convention déjà citée, y compris relativement aux données qui relèvent de la nécessaire restriction apportée au secret médical ;<br/>
<br/>
              En ce qui concerne la durée de conservation des données :<br/>
<br/>
              19. Considérant que le dernier alinéa de l'article 1er du décret attaqué dispose que le traitement GIDE permet également la gestion des contentieux entre l'administration pénitentiaire et les personnes placées sous main de justice ou leurs ayants droits ; qu'aux termes de l'article 7 du décret attaqué : " Les informations et données à caractère personnel sont conservées pour une durée de deux ans à compter de la date de levée d'écrou " ; <br/>
<br/>
              20. Considérant que la durée de conservation de deux ans à compter de la date de la levée d'écrou n'excède pas ce qui est nécessaire, compte tenu de la finalité de gestion des contentieux entre l'administration pénitentiaire et les personnes placées sous main de justice ou leurs ayants droits, pour lesquelles les données sont collectées et traitées dans le traitement GIDE, dès lors que l'accès aux données ainsi conservées doit nécessairement être entendu comme étant réservé, dans la limite du besoin d'en connaitre, aux catégories de personnes limitativement énumérées au I de l'article 5 du décret attaqué, c'est-à-dire aux personnels centraux et déconcentrés de direction de l'administration pénitentiaire ainsi qu'aux membres de la commission pluridisciplinaire unique ; <br/>
<br/>
              21. Considérant qu'il résulte de l'ensemble de ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret attaqué ; que leurs conclusions à fin d'injonction ainsi que celles qu'ils présentent au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de la Ligue des droits de l'homme, du Syndicat national des avocats en France, de la section française de l'Observatoire international des prisons et du Conseil national de l'ordre des médecins sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à la Ligue des droits de l'homme, au Syndicat national des avocats de France, à la section française de l'Observatoire national des prisons, au Conseil national de l'ordre des médecins, au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
