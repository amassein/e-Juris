<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026719828</ID>
<ANCIEN_ID>JG_L_2012_12_000000347108</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/71/98/CETATEXT000026719828.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 03/12/2012, 347108, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347108</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:347108.20121203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 février et 30 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Etablissement public foncier de Normandie, dont le siège est au Carré Pasteur, 5 rue Montaigne à Rouen (76178) ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0900538 du 21 décembre 2010 par lequel le tribunal administratif de Rouen a rejeté sa demande tendant, en premier lieu, à la réduction de la cotisation de taxe foncière sur les propriétés bâties auxquelles il a été assujetti au titre de l'année 2008 à raison d'entrepôts situés 170 boulevard Jules Durand au Havre, en deuxième lieu, à ce que soit mis à la charge de l'Etat, sur le fondement de l'article L. 208 du livre des procédures fiscales, le paiement d'intérêts moratoires et, en troisième lieu, à ce qu'il soit exonéré de manière permanente de la taxe foncière sur les propriétés bâties au titre de ces locaux à compter du <br/>
1er janvier 2009 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le décret n° 68-376 du 26 avril 1968 ;<br/>
<br/>
              Vu le décret n° 2004-1149 du 28 octobre 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de l'Etablissement Public Foncier de Normandie ;<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de l'Etablissement Public Foncier de Normandie ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumises au juge du fond que l'Etablissement public foncier de Normandie a acquis, par acte du 12 avril 2007, des entrepôts situés 170 boulevard Jules Durand au Havre ; que cet établissement public a été assujetti à la taxe foncière sur les propriétés bâties à raison de ces entrepôts au titre de l'année 2008 ; qu'eu égard à l'argumentation qu'il développe, il doit être regardé comme formant un pourvoi en cassation contre le jugement du 21 décembre 2010 en tant seulement que le tribunal administratif de Rouen a rejeté sa demande tendant à la réduction  de cette imposition ;<br/>
<br/>
              2. Considérant que l'Etablissement public foncier de Normandie ne conteste pas le motif par lequel le tribunal a jugé qu'il ne pouvait obtenir satisfaction sur le terrain de la loi fiscale ; qu'il s'est prévalu devant les juges du fond, sur le fondement de l'article L. 80 A du livre des procédures fiscales, de la documentation administrative référencée 6 C-1211 à jour au 15 décembre 1988, et de l'instruction 6 C-2-99 du 3 septembre 1999, publiée au bulletin officiel des impôts le 9 septembre 1999, alors en vigueur ; que le tribunal a rappelé les termes de la documentation administrative étendant le bénéfice de l'exonération de la taxe foncière sur les propriétés bâties prévue au 1° de l'article 1382 du code général des impôts aux " immeubles appartenant : - (...) - aux établissements publics de la Basse-Seine et de la métropole lorraine (...) " ainsi que ceux de l'instruction du 3 septembre 1999 précisant, à la section 2, la portée de cette exonération, selon lesquels : " 2. Les modalités d'exonération des immeubles appartenant aux communes, aux départements, aux régions, ainsi qu'aux établissements publics fonciers ou d'aménagement énumérés au 1 ci-dessus sont inchangées " ; que le 1 de cette instruction mentionne l'établissement public d'aménagement de la Basse-Seine ; que le tribunal, qui n'était pas tenu de répondre à tous les arguments présentés par le requérant, n'a pas insuffisamment motivé son jugement en relevant, pour estimer que l'établissement public requérant n'entrait pas dans les prévisions de cette documentation et de cette instruction, que ces documents ne concernaient que l'établissement public foncier de la Basse-Seine ; <br/>
<br/>
              3. Considérant qu'il résulte de l'article 1er du décret du 26 avril 1968 portant création de l'Etablissement public de la Basse-Seine que cet établissement public d'aménagement, à caractère industriel et commercial, était habilité à exercer certaines missions dans les cantons des départements de la Seine-Maritime et de l'Eure dont la liste était annexée à ce décret ; que le décret du 28 octobre 2004 modifiant ce décret crée, sous le nom A...public foncier de Normandie, un établissement public d'aménagement à caractère industriel et commercial ; que si ce décret prévoit le remplacement de cette dénomination dans l'intitulé du décret du 26 avril 1968 et si l'établissement public ainsi créé assume sans évolution notable les mêmes missions que celles conférées à l'Etablissement public de la Basse-Seine, il résulte des termes mêmes de l'article 2 du décret du 28 octobre 2004 que le nouvel établissement public est habilité à exercer ses missions non seulement dans l'intégralité des départements de la Seine-Maritime et de l'Eure mais aussi dans les départements du Calvados, de la Manche et de l'Orne  ; que, par suite, compte tenu de cette extension de son ressort géographique d'intervention, l'Etablissement public foncier de Normandie ne peut être regardé comme s'étant substitué purement et simplement à l'Etablissement public de la Basse-Seine ; que, dès lors, en jugeant que le requérant n'entrait pas dans les prévisions de cette documentation et de cette instruction, et que, par suite, il n'était pas fondé à se prévaloir, sur le fondement de l'article L. 80 A du livre des procédures fiscales, des énonciations  qu'elles contenaient à propos de l'Etablissement public de la Basse-Seine, le tribunal n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée en défense, l'Etablissement public foncier de Normandie n'est pas fondé à demander l'annulation du jugement attaqué ; que, par suite, son pourvoi doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le pourvoi de l'Etablissement public foncier de Normandie est rejeté.<br/>
<br/>
Article 2 : La présence décision sera notifiée à l'Etablissement public foncier de Normandie et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
