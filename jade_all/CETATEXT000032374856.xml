<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374856</ID>
<ANCIEN_ID>JG_L_2016_04_000000396320</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/48/CETATEXT000032374856.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 06/04/2016, 396320, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396320</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:396320.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La ville de Paris, à l'appui de sa demande tendant à l'annulation pour excès de pouvoir des arrêtés nos 2015275-0003, 2015275-0004, 2015275-0005, 2015275-0006, 2015275-0007, 2015275-0008, 2015275-0009, 2015275-0010, 2015275-0011, 2015275-0012, 2015275-0013, 2015275-0014, 2015275-0015, 2015275-0016, 2015275-0017, 2015275-0018, 2015275-0019, 2015275-0020, 2015275-0021, 2015275-0022, 2015275-0023 et 2015275-0024 du préfet de la région d'Île-de-France, préfet de Paris, du 2 octobre 2015 fixant pour l'année 2015 des dérogations collectives au repos dominical dans plusieurs branches professionnelles, a produit un mémoire, enregistré le 19 octobre 2015 au greffe du tribunal administratif de Paris, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1517080 du 18 janvier 2016, enregistrée le 20 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la troisième chambre de la troisième section du tribunal administratif de Paris, avant qu'il soit statué sur la demande de la ville de Paris, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 3132-26 du code du travail et du III de l'article 257 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et dans un mémoire enregistré le 16 février 2016 au secrétariat du contentieux du Conseil d'Etat, la ville de Paris soutient que l'article L. 3132-26 du code du travail et le III de l'article 257 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, applicables au litige, méconnaissent le principe d'égalité entre collectivités territoriales, le principe de libre administration des collectivités territoriales, garanti par l'article 72 de la Constitution, et le principe de subsidiarité, garanti par le deuxième alinéa du même article.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - l'article L. 3132-26 du code du travail ;<br/>
              - le III de l'article 257 de la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que l'article L. 3132-26 du code du travail fixe les conditions dans lesquelles le maire d'une commune peut, après avis du conseil municipal, supprimer le repos hebdomadaire dans les établissements de commerce de détail certains dimanches, dans la limite d'un nombre de dimanches porté de cinq à douze par an par l'article 250 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques ; que le dernier alinéa de cet article L. 3132-26 dispose que : " A Paris, la décision mentionnée aux trois premiers alinéas est prise par le préfet de Paris " ; qu'aux termes du III de l'article 257 de la loi du 6 août 2015 : " L'article L. 3132-26 du code du travail, dans sa rédaction résultant de la présente loi, s'applique, pour la première fois, au titre de l'année suivant celle au cours de laquelle la présente loi est publiée. / Par dérogation à l'article L. 3132-26 du code du travail, dans sa rédaction antérieure à la présente loi, pour l'année au cours de laquelle la présente loi est publiée, le maire ou, à Paris, le préfet peut désigner neuf dimanches durant lesquels, dans les établissements de commerce de détail, le repos hebdomadaire est supprimé " ;<br/>
<br/>
              3. Considérant que par la question prioritaire de constitutionnalité qu'elle soulève, la ville de Paris conteste la constitutionnalité de ces dispositions en tant seulement que le dernier alinéa de l'article L. 3132-26 du code du travail, rappelé par la mention correspondante au second alinéa du III de l'article 257 de la loi du 6 août 2015, attribue au préfet la compétence pour prendre, à Paris, les décisions de dérogation au repos dominical ainsi prévues ;<br/>
<br/>
              4. Considérant que ces dispositions sont applicables au litige dont est saisi le tribunal administratif de Paris ; qu'elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, et notamment au principe d'égalité entre collectivités territoriales et au principe de libre administration des collectivités territoriales, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du quatrième alinéa de l'article L. 3132-26 du code du travail et des mots " ou, à Paris, le préfet " du second alinéa du III de l'article 257 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à la ville de Paris, au Premier ministre et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée au ministre de l'intérieur, au ministre de l'économie, de l'industrie et du numérique ainsi qu'au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
