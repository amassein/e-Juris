<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240898</ID>
<ANCIEN_ID>JG_L_2021_02_000000429521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/08/CETATEXT000043240898.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 12/02/2021, 429521, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:429521.20210212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Boischaut Marche Environnement, MM. Ballaire, Gibouret, Hussard, Kebabdjian, Laveau, Mathiault, Roblin et Tardif, Mmes C..., D... et H... et MM. et Mmes A..., G... et F... ont demandé au tribunal administratif d'Orléans d'annuler l'arrêté du préfet de la région Centre-Val de Loire du 4 février 2016 délivrant à la société Ferme éolienne de Ids une autorisation d'exploiter, au titre de la législation des installations classées pour la protection de l'environnement, un parc éolien comprenant six éoliennes et un poste de livraison sur le territoire des communes d'Ids-Saint-Roch et de Touchay, ainsi que l'arrêté de la préfète du Cher du 22 mars 2017 modifiant celui du 4 février 2016 pour autoriser la société Ferme éolienne d'Ids à déplacer deux éoliennes. Par un jugement n° 1601814 et 1701764 du 27 février 2018, le tribunal administratif a annulé les deux arrêtés litigieux.<br/>
<br/>
              Par un arrêt n° 18NT01762, 18NT01879 et 18NT01880 du 5 avril 2019, la cour administrative d'appel de Nantes a, sur l'appel formé par la société Ferme éolienne d'Ids, annulé ce jugement, réformé l'arrêté préfectoral du 4 février 2016 s'agissant de l'emplacement de l'éolienne E3 et rejeté le surplus de la demande de l'association Boischaut Marche Environnement et autres. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 8 avril et le 25 juin 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Boischaut Marche Environnement, MM. Ballaire, Gibouret, Kebabdjian, Laveau, Mathiault, Roblin et Tardif, Mmes C..., D... et H..., MM. et Mmes A..., G... et F... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Ferme éolienne d'Ids ;<br/>
<br/>
              3°) de mettre solidairement à la charge de la société Ferme éolienne d'Ids et de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme E... B..., conseillère d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de l'association Boischaut Marche Environnement et autres, et au Cabinet Briard, avocat de la société Ferme éolienne de Ids ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 4 février 2016, le préfet de la région Centre-Val de Loire a autorisé la Ferme éolienne d'Ids à exploiter un parc éolien de 6 éoliennes et un poste de livraison sur le territoire des communes d'Ids-Saint-Roch et de Touchay dans le Cher et que, par un arrêté du 22 mars 2017, la préfète du Cher a modifié cette autorisation par déplacement de 2 éoliennes. L'association Boischaut Marche Environnement et autres se pourvoient en cassation contre l'arrêt du 5 avril 2019 par lequel la cour administrative d'appel de Nantes a annulé le jugement du 27 février 2018 du tribunal administratif d'Orléans qui avait prononcé l'annulation des deux arrêtés litigieux, réformé l'arrêté du 4 février 2016 s'agissant de l'emplacement d'une éolienne et rejeté le surplus de la demande de l'association Boischaut Marche Environnement et autres.<br/>
<br/>
              2. Aux termes du paragraphe 1 de l'article 6 de la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement : " Les États membres prennent les mesures nécessaires pour que les autorités susceptibles d'être concernées par le projet, en raison de leurs responsabilités spécifiques en matière d'environnement, aient la possibilité de donner leur avis sur les informations fournies par le maître d'ouvrage et sur la demande d'autorisation. À cet effet, les États membres désignent les autorités à consulter, d'une manière générale ou au cas par cas. (...) ". L'article L. 122-1 du code de l'environnement, pris pour la transposition des articles 2 et 6 de cette directive, dispose, dans sa rédaction applicable en l'espèce, que : " I. - Les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui, par leur nature, leurs dimensions ou leur localisation sont susceptibles d'avoir des incidences notables sur l'environnement ou la santé humaine sont précédés d'une étude d'impact. (...) / III. - Dans le cas d'un projet relevant des catégories d'opérations soumises à étude d'impact, le dossier présentant le projet, comprenant l'étude d'impact et la demande d'autorisation, est transmis pour avis à l'autorité administrative de l'Etat compétente en matière d'environnement. (...). / IV.- La décision de l'autorité compétente qui autorise le pétitionnaire ou le maître d'ouvrage à réaliser le projet prend en considération l'étude d'impact, l'avis de l'autorité administrative de l'Etat compétente en matière d'environnement et le résultat de la consultation du public (...) ". En vertu du III de l'article R. 122-6 du même code, dans sa version issue du décret du 29 décembre 2011 portant réforme des études d'impact des projets de travaux, d'ouvrages ou d'aménagement, applicable au litige, l'autorité administrative de l'Etat compétente en matière d'environnement mentionnée à l'article L. 122-1, lorsqu'elle n'est ni le ministre chargé de l'environnement, dans les cas prévus au I de cet article, ni la formation compétente du Conseil général de l'environnement et du développement durable, dans les cas prévus au II de ce même article, est le préfet de la région sur le territoire de laquelle le projet de travaux, d'ouvrage ou d'aménagement doit être réalisé. <br/>
<br/>
              3. L'article 6 de la directive du 13 décembre 2011 a pour objet de garantir qu'une autorité compétente et objective en matière d'environnement soit en mesure de rendre un avis sur l'évaluation environnementale des projets susceptibles d'avoir des incidences notables sur l'environnement, avant leur approbation ou leur autorisation, afin de permettre la prise en compte de ces incidences. Eu égard à l'interprétation de l'article 6 de la directive du 27 juin 2001 donnée par la Cour de justice de l'Union européenne dans son arrêt rendu le 20 octobre 2011 dans l'affaire C-474/10, il résulte clairement des dispositions de l'article 6 de la directive du 13 décembre 2011 que, si elles ne font pas obstacle à ce que l'autorité publique compétente pour autoriser un projet soit en même temps chargée de la consultation en matière environnementale, elles imposent cependant que, dans une telle situation, une séparation fonctionnelle soit organisée au sein de cette autorité, de manière à ce que l'entité administrative concernée dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, et soit ainsi en mesure de remplir la mission de consultation qui lui est confiée en donnant un avis objectif sur le projet concerné.<br/>
<br/>
              4. Lorsque le préfet de région est l'autorité compétente pour autoriser le projet, en particulier lorsqu'il agit en sa qualité de préfet du département où se trouve le chef-lieu de la région, ou dans les cas où il est chargé de l'élaboration ou de la conduite du projet au niveau local, si la mission régionale d'autorité environnementale du Conseil général de l'environnement et du développement durable, définie par le décret du 2 octobre 2015 relatif au Conseil général de l'environnement et du développement durable et les articles R. 122-21 et R. 122-25 du code de l'environnement, peut être regardée comme disposant, à son égard, d'une autonomie réelle lui permettant de rendre un avis environnemental dans des conditions répondant aux exigences résultant de la directive, il n'en va pas de même des services placés sous son autorité hiérarchique, comme en particulier la direction régionale de l'environnement, de l'aménagement et du logement. <br/>
<br/>
              4. Par suite, en jugeant qu'il avait été répondu aux exigences de la directive au motif que l'avis de l'autorité environnementale, signé au nom du préfet de région, avait été préparé par un service de la direction régionale de l'environnement, de l'aménagement et du logement, distinct de celui qui a instruit l'autorisation elle-même, alors qu'il ressortait des pièces du dossier qui lui était soumis que ces deux services étaient placés sous l'autorité hiérarchique du préfet de région, signataire de la décision d'autorisation, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. Dès lors, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'association Boischaut Marche Environnement et autres sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'association Boischaut Marche Environnement et autres, au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces mêmes dispositions font obstacle à qu'une somme soit mise à la charge de l'association Boischaut Marche Environnement et autres, qui ne sont pas, en l'espèce, les parties perdantes. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 5 avril 2019 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
<br/>
Article 3 : L'Etat versera à l'association Boischaut Marche Environnement et autres une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les conclusions présentées au même titre par la société Ferme éolienne de Ids sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'association Boischaut Marche Environnement, première dénommée pour l'ensemble des requérants, à la ministre de la transition écologique et à la société Ferme éolienne d'Ids. <br/>
Copie en sera adressée à la Commune d'Ids-Saint-Roch.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
