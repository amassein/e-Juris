<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041989730</ID>
<ANCIEN_ID>JG_L_2020_06_000000440762</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/97/CETATEXT000041989730.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/06/2020, 440762, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440762</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440762.20200609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 mai 2020 au secrétariat du contentieux du Conseil d'Etat, la société Kinoux's camp demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des articles 3, 7, 10 et 27 du décret n° 2020-548 du 11 mai 2020 ;<br/>
<br/>
              2°) d'enjoindre à l'Etat, sans délai et sous astreinte de 150 euros par jour de retard à compter de l'ordonnance à intervenir, de prendre les mesures suivantes : <br/>
              - autoriser la réouverture administrative de son établissement ;<br/>
              - si la réouverture n'est pas possible, couvrir la marge bénéficiaire réalisée par l'établissement à la même époque les années précédentes ainsi que sa marge bénéficiaire nette afin de permettre sa survie économique jusqu'à sa réouverture ;<br/>
              - si la réouverture est possible mais insuffisamment rentable, couvrir la marge bénéficiaire manquante due aux conditions restrictives imposées par la pandémie et prendre en charge les frais exposés en vue d'instaurer les mesures barrières ;<br/>
              - si aucune des mesures sollicitées n'est possible, réexaminer la situation de l'établissement afin qu'une nouvelle décision soit prise à son égard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - elle justifie d'un intérêt à agir dès lors qu'il lui est interdit d'exercer son activité depuis le 14 mars 2020 et que cette interdiction est maintenue depuis le 11 mai 2020 sans qu'aucune date de réouverture ne soit juridiquement prévue ;<br/>
              - la condition d'urgence est satisfaite eu égard, en premier lieu, à l'existence avérée de la pandémie, en deuxième lieu, aux conséquences économiques et patrimoniales de la fermeture de l'établissement et de l'absence de date de réouverture, en troisième lieu, à la nécessité de préserver la vie des entreprises afin qu'elles contribuent au financement du système de santé et, en dernier lieu, à l'absence de motifs de nature à justifier le maintien de la fermeture de son établissement alors qu'il se situe dans un département classé en zone verte et que le risque pandémique n'est pas le même pour l'ensemble des personnes infectées ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
              - le décret attaqué méconnait l'objectif de valeur constitutionnelle de prévisibilité et de sécurité juridique en ce qu'il instaure des dispositions imprécises, confuses, évolutives et susceptibles de conduire à une méconnaissance de l'égalité devant la loi ;<br/>
             - il porte une atteinte disproportionnée à deux des composantes du droit de propriété dès lors qu'il rend la valeur patrimoniale de son établissement incertaine et la prive de l'usage de cet établissement, et ce bien au-delà de la période de confinement ;<br/>
              - il méconnaît les stipulations de l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors qu'il ne permet pas de préserver la vie économique des activités de camping interrompues depuis le 14 mars 2020 ; <br/>
              - il porte une atteinte manifestement excessive à la liberté d'entreprendre dès lors que, en premier lieu, la décision d'interdiction des activités économiques est disproportionnée au regard de l'objectif de sécurité sanitaire poursuivi, en particulier dans les départements classés en zone verte, en deuxième lieu, elle a été adoptée sans qu'aucune étude d'impact n'ait mesuré ses conséquences financières et, en dernier lieu, les compensations économiques sont soit insuffisantes soit inapplicables au secteur du camping ;<br/>
              - il méconnaît le principe de non-discrimination dès lors que les activités de camping ont été exclues sans motif d'intérêt général de la liste des activités pouvant continuer à recevoir du public ;<br/>
              - il méconnaît l'article 4 du pacte international relatif aux droits civils et politiques et l'article 30 de la Déclaration universelle des droits de l'homme dès lors que la France n'a pas signalé au secrétaire général de l'Organisation des Nations-Unies la promulgation de l'état d'urgence sanitaire et que les mesures édictées ne respectent pas le principe d'intangibilité des droits fondamentaux, le principe de temporalité et le principe de proportionnalité ;<br/>
              - il est entaché d'une erreur manifeste d'appréciation eu égard, en premier lieu, à l'existence de moyens plus efficaces pour lutter contre la pandémie, en deuxième lieu, à la possibilité d'une régionalisation et d'une spécialisation du confinement, en troisième lieu, aux dommages collatéraux causés par le confinement et les fermetures administratives et, en dernier lieu, à l'interdiction générale et absolue des activités ainsi qu'à la durée excessive de ces interdictions.<br/>
              Par un mémoire en défense, enregistré le 29 mai 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucun des moyens soulevés n'est propre, en l'état de l'instruction, à créer un doute sérieux quant à la légalité du décret attaqué.<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
             - le code de la santé publique ;<br/>
             - la loi n° 2020-290 du 23 mars 2020 ;<br/>
             - la loi n° 2020-546 du 11 mai 2020 ;<br/>
             - le décret n° 2020-548 du 11 mai 2020 ;<br/>
             - le décret n° 2020-663 du 31 mai 2020 ;<br/>
             - le code de justice administrative ;<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 9 juin 2020 à 12 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. La société Kinoux's camp, qui exploite un établissement de camping dans le département de la Dordogne, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution des articles 3, 7, 10 et 27 du décret n° 2020-548 du 11 mai 2020 et d'enjoindre en conséquence à l'Etat, sans délai et sous astreinte, d'autoriser la réouverture administrative de son établissement ou, à défaut de réouverture dans des conditions normales, de l'indemniser des frais exposés en vue d'instaurer les mesures barrières, des pertes de bénéfices subies par comparaison avec la période équivalente de l'année précédente et de sa marge bénéficiaire nette jusqu'à sa réouverture dans des conditions normales et, si aucune des mesures sollicitées n'est possible, de réexaminer la situation de son établissement afin qu'une nouvelle décision soit prise.<br/>
<br/>
              3. Toutefois, postérieurement à l'introduction de la requête, le décret du 11 mai 2020 a été abrogé par le décret n° 2020-663 du 31 mai 2020. <br/>
<br/>
              4. Par suite, les conclusions de la requête présentées par la société Kinoux's camp au titre de l'article L. 521-1 du code de justice administrative sont devenues sans objet. Il n'y a, dès lors, plus lieu d'y statuer.<br/>
<br/>
              5. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la société Kinoux's camp présentées au titre de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
Article 2 : Les conclusions présentées par la société Kinoux's camp au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à la société Kinoux's camp et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
