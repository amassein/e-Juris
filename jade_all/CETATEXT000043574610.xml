<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043574610</ID>
<ANCIEN_ID>JG_L_2021_05_000000452529</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/57/46/CETATEXT000043574610.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/05/2021, 452529, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452529</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452529.20210526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 12 et 25 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'avis n° 2021.0027/AC/SEESP du 8 avril 2021 du collège de la Haute Autorité de santé concernant le type de vaccin à utiliser pour la seconde dose du vaccin AstraZeneca (nouvellement appelé Vaxzevria) contre la Covid-19 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de ce litige ;<br/>
              - sa requête est recevable ;<br/>
              - la condition d'urgence est satisfaite eu égard, d'une part, au nombre de morts en France en raison de l'épidémie de covid-19 et, d'autre part, au fait qu'aucun centre de vaccination ni aucun médecin n'accepte de lui administrer une seconde dose du vaccin AstraZeneca alors que cette injection doit avoir lieu avant la fin du mois de mai 2021 pour rendre sa vaccination effective à la suite de la première injection de ce vaccin qu'il a reçue le 13 mars 2021 ;<br/>
              - il existe un doute sérieux quant à la légalité de l'avis litigieux ; <br/>
              - cet avis est entaché d'illégalité en l'absence de données acquises de la science permettant de valider la stratégie faisant intervenir deux plateformes vaccinales différentes ;<br/>
              - il méconnaît le principe d'égalité de traitement et d'accès aux soins en prévoyant que seules les personnes âgées de moins de 55 ans ne pourront avoir accès à une seconde dose du vaccin AstraZeneca alors qu'ils en ont reçu une première dose ;<br/>
              - il méconnaît le droit de recevoir les traitements et les soins les plus appropriés en application de l'article L. 1110-5 du code de la santé publique en prévoyant une stratégie vaccinale faisant intervenir deux plateformes vaccinales différentes pour les personnes âgées de moins de 55 ans alors que cette stratégie ne permet pas d'exclure de faire courir au patient des risques disproportionnés par rapport au bénéfice escompté ;<br/>
              - il méconnaît les dispositions des articles L. 1110-3 et L 1411-1 du code de la santé publique en prévoyant l'injection d'une deuxième dose d'un vaccin à ARN messager sans vérifier les effets secondaires associés aux autres vaccins alors qu'il ressort d'une enquête de l'Agence nationale de sécurité du médicament que le vaccin Pfizer est responsable du plus grand nombre d'effets secondaires et que l'on recense plus de décès avec le vaccin Pfizer qu'avec le vaccin AstraZeneca.<br/>
<br/>
              Par un mémoire en défense, enregistré le 20 mai 2021, la Haute Autorité de santé conclut au rejet de la requête. Elle soutient, à titre principal, que la requête dirigée contre un avis rendu à la demande du ministre de la santé est irrecevable et, à titre subsidiaire, que les moyens avancés par le requérant ne sont pas de nature à faire naître un doute sérieux quant à la légalité de la décision contestée.<br/>
<br/>
              Le ministre des solidarités et de la santé a présenté des observations, enregistrées le 20 mai 2021.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B..., et d'autre part, la Haute Autorité de santé, le ministre des solidarités et de la santé et le Premier ministre ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 21 mai 2021, à 11 heures : <br/>
<br/>
              - les représentants de la Haute Autorité de santé et du ministre des solidarités et de la santé ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction le 21 mai à 15 heures puis le 25 mai à 16 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Aux termes de l'article L. 3111-1 du code de la santé publique : " La politique de vaccination est élaborée par le ministre chargé de la santé qui fixe les conditions d'immunisation, énonce les recommandations nécessaires et rend public le calendrier des vaccinations après avis de la Haute Autorité de santé ". Aux termes de l'article L. 161-37 du code de la sécurité sociale : " La Haute Autorité de santé, autorité publique indépendante à caractère scientifique, est chargée de : (...) / 12° Participer à l'élaboration de la politique de vaccination et émettre des recommandations vaccinales, y compris, dans des situations d'urgence, à la demande du ministre chargé de la santé, en fonction des données épidémiologiques, d'études sur les bénéfices et risques de la vaccination et de l'absence de vaccination aux niveaux individuel et collectif et d'études médico-économiques ".<br/>
<br/>
              3. Il résulte de l'instruction que, par un courrier du 15 décembre 2020, le directeur général de la santé a demandé à la Haute Autorité de santé de procéder à l'instruction d'une recommandation vaccinale pour tout vaccin contre la Covid-19 acquis par l'Etat. Par une recommandation du 2 février 2021, le collège de la Haute Autorité de santé a estimé que le vaccin AstraZeneca, qui est un vaccin à vecteur viral, pouvait être utilisé dans le cadre de la stratégie de vaccination contre la Covid-19 en recommandant, dans l'attente de données complémentaires, de privilégier d'autres types de vaccins, les vaccins à ARN messager (ARNm), chez les personnes de plus de 65 ans. Par un avis du 1er mars 2021, rendu sur une saisine du directeur général de la santé, le collège de la Haute Autorité de santé a estimé que la place dans la stratégie vaccinale du vaccin AstraZeneca pouvait être élargie aux personnes âgées de plus de 65 ans. Par un avis du 19 mars 2021, le collège de la Haute Autorité de Santé a recommandé, au vu des données publiées par l'Agence européenne du médicament, de n'utiliser le vaccin AstraZeneca que pour les personnes âgées de 55 ans et plus. Par un avis du 8 avril 2021, le collège de la Haute Autorité de santé a recommandé d'utiliser les vaccins à ARNm pour l'administration de la deuxième dose chez les personnes de moins de 55 ans ayant reçu une première dose du vaccin AstraZeneca, nouvellement appelé Vaxzevria. M. B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cet avis du 8 avril 2021.<br/>
<br/>
              4. Il résulte de l'instruction que l'avis du 8 avril 2021 a été émis à l'intention du ministre chargé de la santé qui est compétent, en vertu de l'article L. 3111-1 du code de la santé publique, pour énoncer les recommandations en matière de politique de vaccination et qui avait sollicité la Haute autorisé de santé le 15 décembre 2020 en vue d'exercer cette compétence. S'appuyant sur cet avis, un message a été adressé par le directeur général de la santé aux professionnels de santé recommandant d'administrer une dose de vaccin à ARNm aux personnes de moins de 55 ans ayant reçu une première injonction avec le vaccin AstraZeneca. L'avis contesté par M. B... constitue ainsi en l'espèce un élément de la procédure d'élaboration de cette décision du directeur général de la santé. Le bien-fondé de la position prise par la Haute Autorité de santé peut être discuté à l'occasion d'un recours pour excès de pouvoir dirigé contre cette décision. En revanche, cet avis ne constitue pas un acte faisant grief et n'est donc pas susceptible de faire l'objet d'un recours pour excès de pouvoir. Il suit de là qu'en l'état de l'instruction le recours pour excès de pouvoir formé contre l'avis litigieux apparaît entaché d'une irrecevabilité insusceptible d'être couverte au cours de l'instance. Par suite, la demande de M. B... tendant à ce que l'exécution de cet avis soit suspendue doit être rejetée ainsi que, par voie de conséquence, les conclusions présentées au titre de l'article L. 61-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B..., à la Haute Autorité de santé et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
