<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717829</ID>
<ANCIEN_ID>JG_L_2014_03_000000345188</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717829.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 12/03/2014, 345188</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345188</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP LAUGIER, CASTON</AVOCATS>
<RAPPORTEUR>M. Florian Blazy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:345188.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 21 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'Office français de protection des réfugiés et apatrides (OFPRA), dont le siège est 201, rue Carnot, à Fontenay-sous-Bois (94136 Cedex), représenté par son directeur général ; l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat d'annuler la décision n° 09021543 du 14 octobre 2010 par laquelle la Cour nationale du droit d'asile a, d'une part, annulé la décision du 20 octobre 2009 de son directeur général rejetant la demande d'admission au statut de réfugié de Mme B...A..., d'autre part, reconnu la qualité de réfugié à l'intéressée ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ;<br/>
<br/>
              Vu la convention de l'Organisation de l'Unité africaine du 10 septembre 1969 régissant les aspects propres aux problèmes des réfugiés en Afrique :<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Blazy, Maître des Requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides et à la SCP Laugier, Caston, avocat de Mlle A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du 2° du A de l'article 1er de la convention de Genève du 28 juillet 1951, la qualité de réfugié est reconnue à toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ou qui, si elle n'a pas de nationalité et se trouve hors du pays dans lequel elle avait sa résidence habituelle, ne peut ou, en raison de ladite crainte, ne veut y retourner " ; qu'aux termes de l'article L. 711-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La qualité de réfugié est reconnue à toute personne persécutée en raison de son action en faveur de la liberté ainsi qu'à toute personne sur laquelle le haut-commissariat des Nations Unies pour les réfugiés exerce son mandat aux termes des articles 6 et 7 de son statut tel qu'adopté par l'Assemblée générale des Nations Unies le 14 décembre 1950 ou qui répond aux définitions de l'article 1er de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés. Ces personnes sont régies par les dispositions applicables aux réfugiés en vertu de la convention de Genève susmentionnée. " ; qu'un réfugié placé sous mandat du haut-commissariat des Nations Unies pour les réfugiés s'entend au sens de la loi et conformément au statut du Haut commissariat des Nations Unies pour les réfugiés d'une personne s'étant vu reconnaître par un Etat partie à la Convention de Genève la qualité de réfugié sur son fondement ; qu'une personne reconnue comme réfugiée sur le fondement d'une autre convention internationale, comme celle de l'Organisation de l'Unité africaine, ou placée par l'Assemblée générale de l'Organisation des Nations Unies sous mandat du Haut commissariat des Nations Unies pour les réfugiés n'est, en revanche, pas, de ce seul fait, un réfugié au sens et pour l'application des stipulations de la Convention de Genève ; qu'il appartient en conséquence à la France, dans ce cas, d'examiner la demande d'asile sans que la circonstance que l'intéressé soit susceptible de séjourner normalement dans un pays tiers, dispense de cet examen ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que MmeA..., ressortissante de la République démocratique du Congo, a fui son pays en août 2004 et s'est rendue au Zimbabwe, où la qualité de réfugié lui a été reconnue sur le fondement du 2 de l'article 1er de la convention de l'Organisation de l'Unité africaine ; que pour reconnaître à Mme A...la qualité de réfugiée sur le fondement de la convention de Genève, la Cour nationale du droit d'asile a estimé qu'en raison du fait qu'elle s'était vu reconnaître la qualité de réfugiée au Zimbabwe sur le fondement de la convention de l'Organisation de l'Unité africaine, c'est au regard des risques personnels qu'elle encourrait dans ce pays qu'elle devait examiner sa demande ; que si la Cour nationale du droit d'asile n'a pas commis d'erreur de droit en jugeant que cette qualité de réfugiée ne reposant pas sur la convention de Genève ne permettait pas de regarder l'intéressée comme bénéficiant de la qualité de réfugiée au sein de l'article L. 711-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, elle a, en revanche, méconnu les stipulations de la convention de Genève en examinant la situation de l'intéressée non, comme l'article 1 de la convention de Genève lui en faisait obligation, au regard du pays dont elle a la nationalité, mais de celui où elle résidait ; que, par suite, l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision du 14 octobre 2010 de la Cour nationale du droit d'asile ; que, par voie de conséquence, les conclusions présentées pour Mme A...sur le fondement des dispositions de l'article 37 de la loi du 10 juillet 1991 doivent être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 14 octobre 2010 de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : Les conclusions présentées pour Mme A...en application des dispositions de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-01 - RÉFUGIÉ AU SENS DE LA CONVENTION DE L'OUA - RÉFUGIÉ AU SENS DE LA CONVENTION DE GENÈVE - ABSENCE - CONSÉQUENCES - PRÉSENTATION EN FRANCE D'UNE PREMIÈRE DEMANDE D'ACCÈS AU STATUT DE RÉFUGIÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-02 - RÉFUGIÉ AU SENS DE LA CONVENTION DE L'OUA - RÉFUGIÉ AU SENS DE LA CONVENTION DE GENÈVE - ABSENCE - CONSÉQUENCES - PRÉSENTATION EN FRANCE D'UNE PREMIÈRE DEMANDE D'ACCÈS AU STATUT DE RÉFUGIÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">095-05 - RÉFUGIÉ AU SENS DE LA CONVENTION DE L'OUA - RÉFUGIÉ AU SENS DE LA CONVENTION DE GENÈVE - ABSENCE - CONSÉQUENCES - PRÉSENTATION EN FRANCE D'UNE PREMIÈRE DEMANDE D'ACCÈS AU STATUT DE RÉFUGIÉ.
</SCT>
<ANA ID="9A"> 095-01 Une personne reconnue comme réfugié sur le fondement de la convention de l'Organisation de l'Unité africaine (OUA) n'est pas titulaire du statut de réfugié au sens de la convention de Genève. Elle présente donc en France une première demande d'accès au statut de réfugié.</ANA>
<ANA ID="9B"> 095-02 Une personne reconnue comme réfugié sur le fondement de la convention de l'Organisation de l'Unité africaine (OUA) n'est pas titulaire du statut de réfugié au sens de la convention de Genève. Elle présente donc en France une première demande d'accès au statut de réfugié.</ANA>
<ANA ID="9C"> 095-05 Une personne reconnue comme réfugié sur le fondement de la convention de l'Organisation de l'Unité africaine (OUA) n'est pas titulaire du statut de réfugié au sens de la convention de Genève. Elle présente donc en France une première demande d'accès au statut de réfugié.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
