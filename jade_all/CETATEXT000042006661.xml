<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042006661</ID>
<ANCIEN_ID>JG_L_2020_06_000000418142</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/00/66/CETATEXT000042006661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 12/06/2020, 418142, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418142</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2020:418142.20200612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 14 février 2018 au secrétariat du contentieux du Conseil d'Etat, le Groupe d'information et de soutien des immigré.e.s (GISTI) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note d'actualité n° 17/2017 de la division de l'expertise en fraude documentaire et à l'identité de la direction centrale de la police aux frontières du 1er décembre 2017 relative aux " fraudes documentaires organisées en Guinée (Conakry) sur les actes d'état civil " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les documents de portée générale émanant d'autorités publiques, matérialisés ou non, tels que les circulaires, instructions, recommandations, notes, présentations ou interprétations du droit positif peuvent être déférés au juge de l'excès de pouvoir lorsqu'ils sont susceptibles d'avoir des effets notables sur les droits ou la situation d'autres personnes que les agents chargés, le cas échéant, de les mettre en oeuvre. Ont notamment de tels effets ceux de ces documents qui ont un caractère impératif ou présentent le caractère de lignes directrices. <br/>
<br/>
              2. Il appartient au juge d'examiner les vices susceptibles d'affecter la légalité du document en tenant compte de la nature et des caractéristiques de celui-ci ainsi que du pouvoir d'appréciation dont dispose l'autorité dont il émane. Le recours formé à son encontre doit être accueilli notamment s'il fixe une règle nouvelle entachée d'incompétence, si l'interprétation du droit positif qu'il comporte en méconnaît le sens et la portée ou s'il est pris en vue de la mise en oeuvre d'une règle contraire à une norme juridique supérieure.<br/>
<br/>
              3. La " note d'actualité " contestée, du 1er décembre 2017, émanant de la division de l'expertise en fraude documentaire et à l'identité de la direction centrale de la police aux frontières, vise à diffuser une information relative à l'existence d'une " fraude documentaire généralisée en Guinée (Conakry) sur les actes d'état civil et les jugements supplétifs " et préconise en conséquence, en particulier aux agents devant se prononcer sur la validité d'actes d'état civil étrangers, de formuler un avis défavorable pour toute analyse d'un acte de naissance guinéen. Eu égard aux effets notables qu'elle est susceptible d'emporter sur la situation des ressortissants guinéens dans leurs relations avec l'administration française, cette note peut faire l'objet d'un recours pour excès de pouvoir, contrairement à ce que soutient le ministre de l'intérieur. <br/>
<br/>
              4. En premier lieu et en tout état de cause, la note contestée entre dans les attributions de la division de l'expertise en fraude documentaire et à l'identité dont elle émane. Et, dès lors qu'elle ne revêt pas le caractère d'une décision, le moyen tiré de ce qu'elle méconnaîtrait les dispositions de l'article L. 212-1 du code des relations entre le public et l'administration, relatives à la signature des décisions et aux mentions relatives à leur auteur ne peut qu'être écarté. <br/>
<br/>
              5. En second lieu, l'article 47 du code civil dispose que : " Tout acte de l'état civil des Français et des étrangers fait en pays étranger et rédigé dans les formes usitées dans ce pays fait foi, sauf si d'autres actes ou pièces détenus, des données extérieures ou des éléments tirés de l'acte lui-même établissent, le cas échéant après toutes vérifications utiles, que cet acte est irrégulier, falsifié ou que les faits qui y sont déclarés ne correspondent pas à la réalité ". La note contestée préconise l'émission d'un avis défavorable pour toute analyse d'acte de naissance guinéen et en suggère à ses destinataires la formulation. Elle ne saurait toutefois être regardée comme interdisant à ceux-ci comme aux autres autorités administratives compétentes de procéder, comme elles y sont tenues, à l'examen au cas par cas des demandes émanant de ressortissants guinéens et d'y faire droit, le cas échéant, au regard des différentes pièces produites à leur soutien. Le moyen tiré de la méconnaissance des dispositions de l'article 47 du code civil doit donc être écarté.  <br/>
<br/>
              6. Il résulte de ce qui précède que le GISTI n'est pas fondé à demander l'annulation du document qu'il attaque. Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent par suite être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête du Groupe d'information et de soutien des immigré.e.s est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Groupe d'information et de soutien aux immigré.e.s et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. - DOCUMENTS DE PORTÉE GÉNÉRALE ÉMANANT D'AUTORITÉS PUBLIQUES TELS QUE LES CIRCULAIRES, INSTRUCTIONS, RECOMMANDATIONS, NOTES, PRÉSENTATIONS OU INTERPRÉTATIONS DU DROIT POSITIF - 1) RECEVABILITÉ DU RECOURS POUR EXCÈS DE POUVOIR - A) PRINCIPE - EXISTENCE, LORSQU'ILS SONT SUSCEPTIBLES D'AVOIR DES EFFETS NOTABLES [RJ1] SUR LES DROITS OU LA SITUATION D'AUTRES PERSONNES QUE LES AGENTS CHARGÉS DE LES METTRE EN &#140;UVRE [RJ2] - B) EXEMPLES - CIRCULAIRES IMPÉRATIVES [RJ3] - LIGNES DIRECTRICES [RJ4] - 2) LÉGALITÉ - A) MODALITÉS D'APPRÉCIATION [RJ5] - B) CAS D'ILLÉGALITÉ [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-01-05-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. DIRECTIVES ADMINISTRATIVES. - ACTES SUSCEPTIBLES DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE [RJ4].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. - OBLIGATION DE SIGNATURE ET DE MENTION DES PRÉNOM, NOM ET QUALITÉ DE L'AUTEUR DE LA DÉCISION (ART. L. 212-1 DU CRPA) - CHAMP D'APPLICATION - EXCLUSION - 1) ACTE NE PRÉSENTANT PAS LE CARACTÈRE D'UNE DÉCISION - 2) ILLUSTRATION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">26-01 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. - NOTE DE LA DEFDI PRÉCONISANT DE FORMULER UN AVIS DÉFAVORABLE POUR TOUTE ANALYSE DE LA VALIDITÉ D'UN ACTE DE NAISSANCE GUINÉEN - 1) ACTE SUSCEPTIBLE DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE, EU ÉGARD AUX EFFETS NOTABLES [RJ1] QU'ELLE EST SUSCEPTIBLE D'EMPORTER SUR LA SITUATION DES RESSORTISSANTS GUINÉENS DANS LEURS RELATIONS AVEC L'ADMINISTRATION FRANÇAISE - 2) CONFORMITÉ À L'ARTICLE 47 DU CODE CIVIL - EXISTENCE, DÈS LORS QU'ELLE N'INTERDIT PAS DE PROCÉDER À UN EXAMEN AU CAS PAR CAS DES DEMANDES ET, LE CAS ÉCHÉANT, D'Y FAIRE DROIT.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - DOCUMENTS DE PORTÉE GÉNÉRALE ÉMANANT D'AUTORITÉS PUBLIQUES TELS QUE LES CIRCULAIRES, INSTRUCTIONS, RECOMMANDATIONS, NOTES, PRÉSENTATIONS OU INTERPRÉTATIONS DU DROIT POSITIF - 1) PRINCIPE - A) INCLUSION, LORSQU'ILS SONT SUSCEPTIBLES D'AVOIR DES EFFETS NOTABLES [RJ1] SUR LES DROITS OU LA SITUATION D'AUTRES PERSONNES QUE LES AGENTS CHARGÉS DE LES METTRE EN &#140;UVRE [RJ2] - B) EXEMPLES - CIRCULAIRES IMPÉRATIVES [RJ3] - LIGNES DIRECTRICES [RJ4] - C) OFFICE DU JUGE [RJ5] [RJ3] - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 01-01-05-03 1) a) Les documents de portée générale émanant d'autorités publiques, matérialisés ou non, tels que les circulaires, instructions, recommandations, notes, présentations ou interprétations du droit positif peuvent être déférés au juge de l'excès de pouvoir lorsqu'ils sont susceptibles d'avoir des effets notables sur les droits ou la situation d'autres personnes que les agents chargés, le cas échéant, de les mettre en oeuvre.... ,,b) Ont notamment de tels effets ceux de ces documents qui ont un caractère impératif ou présentent le caractère de lignes directrices.,,,2) a) Il appartient au juge d'examiner les vices susceptibles d'affecter la légalité du document en tenant compte de la nature et des caractéristiques de celui-ci ainsi que du pouvoir d'appréciation dont dispose l'autorité dont il émane.... ,,b) Le recours formé à son encontre doit être accueilli notamment s'il fixe une règle nouvelle entachée d'incompétence, si l'interprétation du droit positif qu'il comporte en méconnaît le sens et la portée ou s'il est pris en vue de la mise en oeuvre d'une règle contraire à une norme juridique supérieure.</ANA>
<ANA ID="9B"> 01-01-05-03-03 Les lignes directrices émanant d'autorités publiques sont susceptibles d'avoir des effets notables sur les droits ou la situation d'autres personnes que les agents chargés, le cas échéant, de les mettre en oeuvre, et sont, par suite, susceptibles de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9C"> 01-03-01 1) Dès lors qu'un acte administratif ne revêt pas le caractère d'une décision, le moyen tiré de ce qu'il méconnaîtrait l'article L. 212-1 du code des relations entre le public et l'administration (CRPA), relatif à la signature des décisions et aux mentions relatives à leur auteur, ne peut qu'être écarté.,,,2) Il en va ainsi d'une note émanant de la division de l'expertise en fraude documentaire et à l'identité (DEFDI) de la direction centrale de la police aux frontières, visant à diffuser une information relative à l'existence d'une fraude documentaire généralisée en Guinée (Conakry) sur les actes d'état civil et les jugements supplétifs et préconisant en conséquence, en particulier aux agents devant se prononcer sur la validité d'actes d'état civil étrangers, de formuler un avis défavorable pour toute analyse d'un acte de naissance guinéen.</ANA>
<ANA ID="9D"> 26-01 Note émanant de la division de l'expertise en fraude documentaire et à l'identité (DEFDI) de la direction centrale de la police aux frontières, visant à diffuser une information relative à l'existence d'une fraude documentaire généralisée en Guinée (Conakry) sur les actes d'état civil et les jugements supplétifs et préconisant en conséquence, en particulier aux agents devant se prononcer sur la validité d'actes d'état civil étrangers, de formuler un avis défavorable pour toute analyse d'un acte de naissance guinéen.,,,1) Eu égard aux effets notables qu'elle est susceptible d'emporter sur la situation des ressortissants guinéens dans leurs relations avec l'administration française, cette note peut faire l'objet d'un recours pour excès de pouvoir.,,,2) Cette note préconise l'émission d'un avis défavorable pour toute analyse d'acte de naissance guinéen et en suggère à ses destinataires la formulation. Elle ne saurait toutefois être regardée comme interdisant à ceux-ci comme aux autres autorités administratives compétentes de procéder, comme elles y sont tenues, à l'examen au cas par cas des demandes émanant de ressortissants guinéens et d'y faire droit, le cas échéant, au regard des différentes pièces produites à leur soutien. Le moyen tiré de la méconnaissance de l'article 47 du code civil doit donc être écarté.</ANA>
<ANA ID="9E"> 54-01-01-01 1) a) Les documents de portée générale émanant d'autorités publiques, matérialisés ou non, tels que les circulaires, instructions, recommandations, notes, présentations ou interprétations du droit positif peuvent être déférés au juge de l'excès de pouvoir lorsqu'ils sont susceptibles d'avoir des effets notables sur les droits ou la situation d'autres personnes que les agents chargés, le cas échéant, de les mettre en oeuvre.... ,,b) Ont notamment de tels effets ceux de ces documents qui ont un caractère impératif ou présentent le caractère de lignes directrices.,,,c) Il appartient au juge d'examiner les vices susceptibles d'affecter la légalité du document en tenant compte de la nature et des caractéristiques de celui-ci ainsi que du pouvoir d'appréciation dont dispose l'autorité dont il émane. Le recours formé à son encontre doit être accueilli notamment s'il fixe une règle nouvelle entachée d'incompétence, si l'interprétation du droit positif qu'il comporte en méconnaît le sens et la portée ou s'il est pris en vue de la mise en oeuvre d'une règle contraire à une norme juridique supérieure.,,,2) Note émanant de la division de l'expertise en fraude documentaire et à l'identité (DEFDI) de la direction centrale de la police aux frontières, visant à diffuser une information relative à l'existence d'une fraude documentaire généralisée en Guinée (Conakry) sur les actes d'état civil et les jugements supplétifs et préconisant en conséquence, en particulier aux agents devant se prononcer sur la validité d'actes d'état civil étrangers, de formuler un avis défavorable pour toute analyse d'un acte de naissance guinéen.,,,Eu égard aux effets notables qu'elle est susceptible d'emporter sur la situation des ressortissants guinéens dans leurs relations avec l'administration française, cette note peut faire l'objet d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du critère de recevabilité d'un recours pour excès de pouvoir contre les actes de droit souple des autorités de régulation, CE, Assemblée, 21 mars 2016, Société NC Numericable, n° 390023, p. 88 ; CE, Assemblée, 21 mars 2016, Société Fairvesta International GMBH et autres, n°s 368082 368083 368084, p. 76 ; pour une application de ce critère s'agissant d'un acte de droit souple n'émanant pas d'une autorité de régulation, CE, Assemblée, 19 juillet 2019, Mme,, n° 426689, p. 326.,,[RJ2] Ab. jur., sur le caractère impératif comme critère exclusif de recevabilité d'un recours pour excès de pouvoir contre les circulaires et instructions interprétatives, CE, Section, 18 décembre 2002, Mme,, n° 233618, p. 463.,,[RJ3] Cf., en précisant, CE, Section, 18 décembre 2002, Mme,, n° 233618, p. 463.,,[RJ4] Ab. jur. CE, 3 mai 2004, Comité anti-amiante Jussieu et Association nationale de défense des victimes de l'amiante, n°s 254961 255376 258342, p. 193. Rappr., s'agissant des lignes directrices des autorités de régulation, CE, 13 décembre 2017, Société Bouygues Télécom et autres, n°s 401799 401830 401912, p. 356.,,[RJ5] Rappr., s'agissant des modalités d'appréciation de la légalité des actes de droit souple des autorités de régulation, CE, Assemblée, 21 mars 2016, Société NC Numericable, n° 390023, p. 88 ; CE, Assemblée, 21 mars 2016, Société Fairvesta International GMBH et autres, n°s 368082 368083 368084, p. 76 ; s'agissant des lignes directrices des autorités de régulation, CE, 13 décembre 2017, Société Bouygues Télécom et autres, n°s 401799 401830 401912, p. 356.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
