<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028280166</ID>
<ANCIEN_ID>JG_L_2013_12_000000344989</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/28/01/CETATEXT000028280166.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 06/12/2013, 344989, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344989</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BLONDEL</AVOCATS>
<RAPPORTEUR>M. Jean-Claude Hassan</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:344989.20131206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 14 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 07PA02657 du 9 décembre 2009 rectifié par l'arrêt n° 10PA00539 du 14 octobre 2010 par lequel la cour administrative d'appel de Paris, faisant droit à l'appel de l'association " Les témoins de Jéhovah ", a, d'une part, annulé le jugement du 28 mars 2007 par lequel le tribunal administratif de Paris a rejeté sa demande en décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des années 1993, 1994 et 1995 et, d'autre part, accordé à cette association la décharge de ces cotisations supplémentaires :<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Claude Hassan, Conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Blondel, avocat de l'association " Les témoins de Jéhovah " ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'association " Les témoins de Jéhovah " a souscrit auprès du Crédit lyonnais des certificats de dépôt en vue de placer à court terme ses excédents de trésorerie ; qu'elle a périodiquement rétrocédé ces certificats, la veille de leur échéance, à l'établissement émetteur, à un prix de cession dont l'administration fiscale a relevé qu'il était à chaque fois égal au montant nominal des titres majoré du montant de la rémunération prévue lors de leur souscription ; que l'association  a estimé qu'elle réalisait à l'occasion de ces cessions une plus-value de cession non imposable ; que dans le cadre de la vérification de sa comptabilité ayant porté sur les exercices 1993, 1994 et 1995, l'administration a, en revanche, regardé l'écart entre le montant nominal des titres, acquitté lors de leur souscription, et le prix de cession comme constitutif d'un revenu produit par des titres de créances négociables, imposable en tant que tel ; qu'elle a, en conséquence, assujetti l'association, au titre des trois exercices considérés, notamment à des cotisations d'impôt sur les sociétés au taux de 10 % prévu en faveur des associations sans but lucratif par l'article 219 bis du code général des impôts ; que le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement se pourvoit en cassation contre l'arrêt du 9 décembre 2009, par lequel la cour administrative d'appel de Paris a, d'une part, annulé le jugement du tribunal administratif de Paris du 28 mars 2007 rejetant la demande de l'association " Les témoins de Jéhovah " en décharge de ces impositions supplémentaires et d'autre part, dans l'article 2 du dispositif de sa décision, notamment accordé décharge à l'association " Les témoins de Jéhovah " des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des années 1993,1994 et 1995 ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du I de l'article 219 bis du code général des impôts : " Par dérogation aux dispositions de l'article 219, le taux de l'impôt sur les sociétés est fixé à 24 % en ce qui concerne les revenus visés au 5 de l'article 206, perçus par les établissements publics, associations et collectivités sans but lucratif. Toutefois ce taux est fixé à 10 % en ce qui concerne : a) Les produits des titres de créances mentionnés au 1° bis du III bis de l'article 125 A ; (...) " ; qu'aux termes du III bis de l'article 125 A du même code, dans sa rédaction applicable aux impositions en litige : " Le taux du prélèvement est fixé : / (...) 1° bis. A 15 % pour les produits des titres de créances négociables sur un marché réglementé en application d'une disposition législative particulière et non susceptibles d'être cotés " ; qu'il résulte de ces dispositions que sont imposables à l'impôt sur les sociétés au taux réduit de 10 % les revenus produits par les titres de créance négociables détenus par les associations sans but lucratif ; que ne sont, en revanche, pas imposables les gains réalisés par ces associations lors de la cession des titres ;  <br/>
<br/>
              3. Considérant, d'autre part, que les articles 4 et 5 du  règlement n° 92-03 du Comité de la réglementation bancaire et financière du 17 février 1992 modifié, relatif aux titres de créances négociables, pris sur habilitation du V de la loi n° 91-716 du 26 juillet 1991, disposent respectivement que : " Les certificats de dépôt (...) doivent avoir une échéance fixe, une durée initiale au moins égale à dix jours (...). Leur durée maximale ne doit pas dépasser un an. Le taux de rémunération doit être fixe. Il peut donner lieu à un intérêt précompté ", et que : " Les certificats de dépôt et les bons des institutions et des sociétés financières ne peuvent pas être remboursés par anticipation, sauf autorisation exceptionnelle donnée par la Banque de France. Ces titres ne peuvent pas être rachetés par les émetteurs, sauf dans les conditions et limites suivantes : les opérations de rachat ne peuvent porter que sur des titres ayant une durée restant à courir supérieure à un mois, les titres provenant de rachat ne peuvent être revendus par l'émetteur lorsqu'ils sont à moins de dix jours de leur échéance (...) " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les certificats de dépôts souscrits par l'association " Les Témoins de Jéhovah " auprès du Crédit Lyonnais, qui sont des titres de créance négociables, avaient des dates d'échéance comprises entre un et trois mois, et étaient assortis d'une rémunération fixée lors de leur souscription, qui variait entre 5 et 12 % de la valeur des titres, selon la durée de vie de ces derniers ; que cette rémunération était payable au souscripteur à la date d'échéance, en même temps que lui était remboursé l'exact montant de la valeur nominale des certificats, acquitté lors de leur souscription ; que, toutefois, les certificats souscrits ont été rétrocédés à l'émetteur la veille de leurs dates d'échéance ; que la fraction du produit de ces rétrocessions supérieure à la valeur nominale des certificats était strictement égale au montant de la rémunération initialement prévue ; que le ministre soutient que l'opération en litige ne constituait pas une cession de titres mais le remboursement anticipé d'un prêt générateur d'un revenu imposable ;<br/>
<br/>
              5. Considérant, en premier lieu, que pour estimer que les sommes perçues du Crédit Lyonnais par l'association " Les témoins de Jéhovah ", en sus de la valeur nominale des titres à l'occasion de leur rétrocession avant la date normale de l'échéance, constituaient pour cette dernière, de la même façon que si elle les avait perçues en contrepartie d'une cession sur le marché, non pas un revenu mais une plus-value, la cour a relevé à bon droit que la circonstance que l'écart entre le produit de ces rétrocessions et la valeur nominale des titres était strictement égal au montant de la rémunération initialement prévue s'expliquait par la proximité de la date d'échéance des titres et n'avait pas pour effet de conférer à cette fraction le caractère d'un produit imposable assimilé à un revenu, alors qu'il n'est pas contesté que le prix versé par le Crédit Lyonnais en échange des certificats de dépôts n'avait pas été fixé sur le marché ; qu'en en déduisant que la perception des sommes perçues par l'association constituait pour cette dernière un gain en capital non imposable, la cour n'a ni donné aux faits ainsi énoncés une qualification juridique erronée, ni commis d'erreur de droit ; qu'en second lieu, la circonstance que l'opération aurait méconnu les dispositions du règlement du Comité de la réglementation bancaire et financière du 17 février 1992 cité ci-dessus n'a pas eu pour conséquence de faire perdre à celle-ci son caractère de gain en capital non imposable pour lui conférer, comme le soutient à tort le ministre, celui d'un revenu produit par des titres de créance négociables ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par l'association " Les témoins de Jéhovah ", que le ministre, qui n'a à aucun moment soutenu que l'association se serait livrée à un montage constitutif d'abus de droit ou de fraude à la loi, n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme dont l'association " Les témoins de Jéhovah " demande le versement au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement est rejeté.<br/>
Article 2 : Les conclusions présentées par l'association " Les témoins de Jéhovah " au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article  3 : La présente décision sera notifiée au ministre de l'économie et des finances et à l'association " Les témoins de Jéhovah ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
