<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042614254</ID>
<ANCIEN_ID>JG_L_2020_12_000000446507</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/61/42/CETATEXT000042614254.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 02/12/2020, 446507, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446507</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446507.20201202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat pénitentiaire des surveillants non gradés (SPS) a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner au garde des sceaux, ministre de la justice, sous astreinte de 500 euros par jour de retard, de lui permettre de participer, au même titre et dans les mêmes conditions que les autres organisations syndicales, au processus de consultation des campagnes de mobilité. Par une ordonnance n° 2017502 du 2 novembre 2020, le juge des référés du tribunal administratif de Paris a enjoint au garde des sceaux, ministre de la justice, de permettre au syndicat requérant d'être associé aux réunions de consultation sur les mobilités actuellement en cours et de lui transmettre, à cet effet, les informations nécessaires relatives à la situation individuelle des agents concernés et de lui permettre de présenter des observations dans le cadre de cet examen.<br/>
<br/>
              Par une requête, enregistrée le 16 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, le garde des sceaux, ministre de la justice, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter les demandes présentées en première instance par le syndicat SPS.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance attaquée est entachée d'une erreur de droit dès lors que le juge des référés de première instance, d'une part, s'est fondé sur les dispositions de l'article 39 du décret du 29 novembre 2019 relatif aux lignes directrices de gestion et à l'évolution des attributions des commissions administratives paritaires, lesquelles ne sont pas applicables au litige et, d'autre part, lui a enjoint de transmettre au syndicat requérant des informations relatives à la situation individuelle d'agents alors qu'une telle transmission est subordonnée à l'accord préalable des intéressés ;<br/>
              - aucune atteinte grave et manifestement illégale à la liberté syndicale ou au principe d'égalité de traitement entre organisations syndicales ne peut être caractérisée, dès lors, d'une part, que dans le silence des textes, il était loisible à l'administration d'autoriser ou non la participation des organisations syndicales aux campagnes de mobilité en en définissant les conditions et, d'autre part, que le syndicat requérant ne dispose d'aucun siège au comité technique ministériel, lequel est compétent en matière de mobilité.  <br/>
<br/>
              Par un mémoire en défense, enregistré le 19 novembre 2020, le syndicat pénitentiaire des surveillants non gradés conclut au rejet de la requête et à ce qu'il soit mis à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Il soutient qu'aucun des moyens de la requête n'est fondé. <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le garde des sceaux, ministre de la justice, d'autre part, le syndicat pénitentiaire des surveillants non gradés ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 24 novembre 2020, à 14 heures 30 : <br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice ;<br/>
<br/>
              - Me A..., avocate au Conseil d'Etat et à la Cour de cassation, avocate du syndicat pénitentiaire des surveillants non gradés ;<br/>
<br/>
              - le représentant du syndicat pénitentiaire des surveillants non gradés ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 26 novembre 2020 à 16 heures.<br/>
<br/>
              Vu les deux nouveaux mémoires, enregistrés le 25 novembre 2020, présentés par le garde des sceaux, ministre de la justice, qui maintient ses conclusions et ses moyens ;<br/>
<br/>
              Vu les deux nouveaux mémoires, enregistrés les 25 et 26 novembre 2020, présentés par le syndicat pénitentiaire des surveillants non gradés, qui maintient ses conclusions et ses moyens ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2019-828 du 6 août 2019 ;<br/>
              - le décret n° 2019-1265 du 29 novembre 2019 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Il résulte de l'instruction que le syndicat pénitentiaire des surveillants non gradés (SPS) a demandé au garde des sceaux, ministre de la justice, de lui transmettre les informations, y compris individuelles, relatives aux campagnes de mobilité des surveillants de l'administration pénitentiaire et de l'autoriser à participer aux réunions de consultation des organisations syndicales relatives aux demandes de mutation de ces agents. Cette demande a fait l'objet d'une décision implicite de rejet. Une nouvelle campagne de mobilité a été lancée le 23 octobre 2020. Saisi par le syndicat SPS sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Paris a, par l'ordonnance attaquée, enjoint au garde des sceaux, ministre de la justice, de permettre à ce syndicat d'être associé aux réunions de consultation sur la mobilité actuellement en cours, de lui transmettre, à cet effet, les informations nécessaires relatives à la situation individuelle des agents concernés et de lui permettre de présenter des observations dans le cadre de cet examen. <br/>
<br/>
              3. Aux termes du premier alinéa de l'article 8 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Le droit syndical est garanti aux fonctionnaires. Les intéressés peuvent librement créer des organisations syndicales, y adhérer et y exercer des mandats. Ces organisations peuvent ester en justice ".<br/>
<br/>
              4. Aux termes de l'article 14 bis de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Les agents peuvent choisir un représentant désigné par l'organisation syndicale représentative de leur choix pour les assister dans l'exercice des recours administratifs contre les décisions individuelles défavorables prises au titre des articles 26, 58 et 60. A leur demande, les éléments relatifs à leur situation individuelle au regard de la réglementation en vigueur et des lignes directrices de gestion leur sont communiqués ". Aux termes de l'article 60 de la même loi, dans sa rédaction applicable aux mutations prenant effet à compter du 1er janvier 2020 en application du VI de l'article 94 de la loi du 6 août 2019 de transformation de la fonction publique : " I. - L'autorité compétente procède aux mutations des fonctionnaires en tenant compte des besoins du service. / (...) IV. - Les décisions de mutation tiennent compte, dans des conditions prévues par décret en Conseil d'Etat, des lignes directrices de gestion en matière de mobilité prévues à l'article 18 de la présente loi. / (...) ".<br/>
<br/>
              5. Aux termes de l'article 30 du décret du 29 novembre 2019 relatif aux lignes directrices de gestion et à l'évolution des attributions des commissions administratives paritaires : " Sont représentatives, au sens de l'article 14 bis de la loi du 11 janvier 1984 susvisée, les organisations syndicales disposant d'au moins un siège au sein du comité social d'administration ministériel ou au sein de tout autre comité social d'administration dont relève l'agent ". Aux termes de l'article 39 du même décret : " I. - Jusqu'au renouvellement général des instances de la fonction publique et par dérogation à l'article 4, les lignes directrices de gestion doivent avoir été soumises pour avis, avant leur adoption ou leur révision, au comité technique ministériel (...). / V. - Jusqu'au renouvellement général des instances de la fonction publique, la représentativité des organisations syndicales est appréciée en fonction des résultats obtenus aux dernières élections : / 1° Au comité technique ministériel ou tout autre comité technique dont relève l'agent, pour l'application de l'article 30 ; (...) ".<br/>
<br/>
              6. Il résulte, d'une part, des dispositions de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, telles qu'elles ont été modifiées par la loi du 6 août 2019 de transformation de la fonction publique, que les commissions administratives paritaires ne sont plus compétentes pour émettre des avis concernant les demandes de mutation des agents. Il résulte, d'autre part, des dispositions des articles 30 et 39 du décret du 29 novembre 2019 citées au point précédent que, jusqu'au renouvellement général des instances de la fonction publique, sont regardées comme représentatives, pour assister les agents dans l'exercice de leurs recours individuels en matière de mutation, les organisations syndicales disposant d'au moins un siège au sein du comité technique ministériel ou de tout autre comité technique dont relève l'agent.<br/>
<br/>
              7. Il résulte de l'instruction qu'alors même qu'il n'y était pas tenu par les textes en vigueur, le garde des sceaux, ministre de la justice, a décidé de communiquer à trois organisations syndicales un document comportant la liste des candidats à chaque poste de surveillant ouvert à la mutation et de les inviter à des réunions leur permettant de présenter leurs observations sur ces candidatures avant que l'administration ne prenne les décisions de mutation. Les représentants du ministre ont affirmé à l'audience que, compte tenu de la suppression du rôle des commissions administratives paritaires en matière de mutation, le ministre avait décidé, à titre transitoire, de consulter de manière informelle les organisations syndicales représentées au comité technique ministériel dès lors que celui-ci avait émis un avis sur les lignes directrices de gestion en matière de mobilité mentionnées à l'article 60 de la loi du 11 janvier 1984.<br/>
<br/>
              8. Toutefois, ainsi qu'il a été dit au point 6, jusqu'au prochain renouvellement général des instances de la fonction publique, sont regardées comme représentatives pour assister les agents dans l'exercice de leurs recours individuels en matière de mutation les organisations syndicales disposant d'au moins un siège au sein du comité technique ministériel ou de tout autre comité technique dont relève l'agent. Or, il est constant que le syndicat SPS dispose d'un siège au comité technique de l'administration pénitentiaire. Par suite, en réservant le bénéfice des informations individuelles relatives à la mutation des surveillants à une partie seulement des organisations syndicales membres du comité technique ministériel ou du comité technique de l'administration pénitentiaire, le garde des sceaux, ministre de la justice, a porté une atteinte grave et manifestement illégale à la liberté syndicale et au principe de non-discrimination entre organisations syndicales, qui ont le caractère de libertés fondamentales au sens de l'article L. 521-2 du code de justice administrative.  <br/>
<br/>
              9. Il résulte de ce qui précède que le garde des sceaux, ministre de la justice, qui n'a pas sérieusement contesté que la condition d'urgence était remplie, n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris lui a enjoint de permettre au syndicat SPS d'être consulté, dans les mêmes conditions que les trois autres organisations syndicales, sur la campagne de mobilité des surveillants en cours. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du syndicat SPS tendant à ce que l'Etat verse une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : La requête du garde des sceaux, ministre de la justice, est rejetée. <br/>
Article 2 : L'Etat versera au syndicat pénitentiaire des surveillants non gradés la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée au garde des sceaux, ministre de la justice, et au syndicat pénitentiaire des surveillants non gradés. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
