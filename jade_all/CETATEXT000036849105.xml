<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036849105</ID>
<ANCIEN_ID>JG_L_2018_04_000000419537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/91/CETATEXT000036849105.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/04/2018, 419537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:419537.20180413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, outre de l'admettre provisoirement au bénéfice de l'aide juridictionnelle, d'une part, de suspendre l'exécution de la décision du 10 janvier 2018 par laquelle le président du conseil départemental de Seine-et-Marne a mis fin à sa prise en charge au titre de l'aide sociale à l'enfance, d'autre part d'enjoindre au président du conseil départemental de Seine-et-Marne de lui assurer une solution d'hébergement comportant le logement dans une structure adaptée à sa situation et la prise en charge de ses besoins alimentaires quotidiens et de mettre en place à son bénéfice une prise en charge éducative lui permettant d'accéder à un emploi ou une formation. Par une ordonnance n° 1801886 du 21 mars 2018, le juge des référés du tribunal administratif de Melun, après avoir admis l'intéressé à titre provisoire à l'aide juridictionnelle, a enjoint au président du conseil départemental de Seine-et-Marne de proposer à M. B...un accompagnement comportant l'accès à une solution de logement et de prise en charge de ses besoins alimentaires et sanitaires, moyennant éventuellement une participation financière de M. B... tenant compte de ses revenus, afin de lui permettre la poursuite de sa scolarité, dans un délai de huit jours à compter de la notification de cette ordonnance et a rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par une requête enregistrée le 4 avril 2018 au secrétariat du contentieux du Conseil d'Etat, le département de Seine-et-Marne demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter la demande présentée par M. B...devant le juge des référés du tribunal administratif de Melun.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la demande présentée par M. B...devant le juge des référés du tribunal administratif de Melun est irrecevable dès lors que la demande de prise en charge l'intéressée n'a pas été rejetée sans faire l'objet d'un examen ;<br/>
              - c'est à tort que le juge des référés du tribunal administratif de Melun a regardé la condition d'urgence comme satisfaite alors que M. B...n'est pas dépourvu de revenus et ne présente pas de difficulté d'insertion ;<br/>
              - c'est à tort qu'il a estimée caractérisée une atteinte grave et manifestement illégale à une liberté fondamentale alors, d'une part, que l'accompagnement prévu au dernier alinéa de l'article L. 222-5 du code de l'action sociale et des familles est réservé aux jeunes de moins de vingt et un ans qui éprouvent des difficultés d'insertion, ce qui n'est pas le cas de M. B..., lequel ne peut faire valoir une atteinte à son droit à l'éducation compte tenu de son manque d'assiduité scolaire et, d'autre part, que le département doit prendre en charge davantage de mineurs et jeunes majeurs qu'il ne dispose de places.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2016-297 du 14 mars 2016<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
      - le code de justice administrative ;		<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. En vertu de l'article L. 221-1 du code de l'action sociale et des familles, " le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / 1° Apporter un soutien matériel, éducatif et psychologique tant aux mineurs (...) confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social, qu'aux mineurs émancipés et majeurs de moins de vingt et un ans confrontés à des difficultés familiales, sociales et éducatives susceptibles de compromettre gravement leur équilibre ; / (...) 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation, (...) ; / (...)/ 7° Veiller à la stabilité du parcours de l'enfant confié et à l'adaptation de son statut sur le long terme ; (...) ". L'article L. 222-5 du code de l'action sociale et des familles détermine les personnes relevant, sur décision du président du conseil départemental, d'une prise en charge par le service de l'aide sociale à l'enfance, parmi lesquelles les mineurs mentionnés aux 1° à 3° de cet article. En vertu du sixième alinéa de cet article, cette prise en charge peut être ouverte, à titre temporaire, en faveur des " majeurs âgés de moins de vingt et un ans qui éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants ". La loi du 14 mars 2016, ci-dessus visée, a complété l'article L. 222-5 du même code par un septième alinéa imposant qu'un accompagnement soit proposé, au-delà du terme de leur prise en charge par le service de l'aide sociale à l'enfance, notamment, aux jeunes majeurs mentionnés au sixième alinéa, pour leur permettre de terminer l'année scolaire ou universitaire engagée. L'article L. 222-5-1 du même code, inséré par la même loi, prévoit qu' " un entretien est organisé par le président du conseil départemental avec tout mineur accueilli au titre des 1°, 2° ou 3° de l'article L. 222-5, un an avant sa majorité, pour faire un bilan de son parcours et envisager les conditions de son accompagnement vers l'autonomie. Dans le cadre du projet pour l'enfant, un projet d'accès à l'autonomie est élaboré par le président du conseil départemental avec le mineur. Il y associe les institutions et organismes concourant à construire une réponse globale adaptée à ses besoins en matière éducative, sociale, de santé, de logement, de formation, d'emploi et de ressources. "<br/>
<br/>
              3. Il résulte de ces dispositions, notamment telles qu'elles ont été complétées par la loi du 14 mars 2016, que si le président du conseil départemental dispose, sous le contrôle du juge, d'un pouvoir d'appréciation pour décider de la prise en charge par le service chargé de l'aide sociale à l'enfance, qu'il n'est pas tenu d'accorder ou de maintenir, d'un jeune majeur de moins de vingt et un ans éprouvant des difficultés d'insertion sociale, il lui incombe en revanche d'assurer l'accompagnement vers l'autonomie des mineurs pris en charge par ce service lorsqu'ils parviennent à la majorité et notamment, à ce titre, de proposer à ceux d'entre eux qui éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants toute mesure, adaptée à leurs besoins en matière éducative, sociale, de santé, de logement, de formation, d'emploi et de ressources, propre à leur permettre de terminer l'année scolaire ou universitaire engagée. <br/>
<br/>
              4. Le département de Seine-et-Marne relève appel de l'ordonnance du 21 mars 2018 par laquelle le juge des référés du tribunal administratif de Melun, saisi d'une demande présentée par M. A...B...sur le fondement de l'article L. 521-2 du code de justice administrative, lui a enjoint de proposer à l'intéressé, dans les huit jours de la notification de cette ordonnance, un accompagnement comportant l'accès à une solution de logement et de prise en charge de ses besoins alimentaires et sanitaires, moyennant éventuellement une participation financière de M. B...tenant compte de ses revenus, afin de lui permettre la poursuite de sa scolarité.<br/>
<br/>
              5. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de Melun que M. A...B..., ressortissant malien né le 26 décembre 1999, entré en France en 2015, a fait l'objet le 26 août 2015 d'une mesure de placement au service de l'aide sociale à l'enfance de Seine-et-Marne par le procureur de la République près le tribunal de grande instance de Meaux puis par le juge des enfants, renouvelée jusqu'à l'ouverture à son bénéfice le 28 septembre 2016 d'une mesure de tutelle, alors confiée au conseil départemental de Seine-et-Marne. Le 10 janvier 2018, le président du conseil départemental de Seine-et-Marne a rejeté la demande formée par M. B...à sa majorité, le 26 décembre 2017, tendant à ce que sa prise en charge se poursuive par un contrat jeune majeur jusqu'à la fin, en juin 2018, de sa formation en alternance.<br/>
<br/>
              6. Il résulte également de l'instruction, notamment des rapports de l'équipe éducative ayant assuré sa prise en charge et de l'assistante sociale du service de l'aide sociale à l'enfance ainsi que de ses bulletins scolaires, que si M.B..., arrivé seul en France, est unanimement décrit comme respectueux, sérieux, actif dans ses démarches et comme s'étant bien intégré dans les différentes structures, tant de l'aide sociale à l'enfance que scolaires, au sein desquelles il a été placé, il éprouve des difficultés pour mener des démarches à l'extérieur sans l'accompagnement d'un adulte et pour prendre en charge seul le suivi de sa scolarité. Dans ces conditions et eu égard à son absence de soutien familial et au caractère limité des ressources qu'il tire de son apprentissage, M. B...est au nombre des jeunes majeurs auxquels il incombait au président du conseil départemental de proposer, au-delà du terme de sa prise en charge par le service de l'aide sociale à l'enfance, un accompagnement adapté à ses besoins et propre à lui permettre de terminer l'année scolaire engagée. Si cet accompagnement pouvait revêtir toute forme utile et n'impliquait pas par lui-même une prise en charge de l'intéressé par le service de l'aide sociale à l'enfance au titre du contrat jeune majeur qu'il avait sollicité, il résulte de l'instruction que M.B..., alors qu'il n'apparaît ni avoir bénéficié avant sa majorité de l'entretien prévu à l'article L. 222-5-1 du code de l'action sociale et des familles pour faire un bilan de son parcours et envisager les conditions de son accompagnement vers l'autonomie, ni avoir été préparé à l'arrêt de sa prise en charge, dont la poursuite jusqu'à la fin de l'année scolaire était proposée par les services de l'aide sociale à l'enfance, sans d'ailleurs que la décision du 10 janvier 2018 fasse apparaître le motif du refus qui lui a en définitive été opposé, ne s'est vu proposer aucun accompagnement à l'issue de sa prise en charge.<br/>
<br/>
              7. Il résulte enfin de l'instruction que cette carence caractérisée du département a en l'espèce eu pour conséquence que M. B...s'est retrouvé, sans avoir été mis en mesure de prévenir cette situation, dépourvu d'hébergement, n'accédant qu'irrégulièrement à un hébergement d'urgence, isolé sur le territoire français et privé de tout suivi, alors qu'il ne dispose que des ressources limitées que lui procure sa formation en alternance, dont la poursuite devient surplus incertaine compte tenu de son besoin d'être accompagné pour obtenir le renouvellement de l'autorisation de travail provisoire ayant permis la conclusion de son contrat d'apprentissage, et ainsi confronté à des difficultés susceptibles de compromettre gravement l'équilibre auquel sa prise en charge pendant sa minorité avait contribué et de mettre en danger sa santé, sa sécurité et sa moralité.<br/>
<br/>
              8. Le département de Seine-et-Marne, qui ne conteste pas utilement la recevabilité de la demande présentée par M. B...au juge des référés du tribunal administratif de Melun et l'urgence retenue par ce dernier en se bornant à faire valoir le pouvoir d'appréciation dont il dispose pour accorder aux jeunes majeurs le bénéfice des capacités limitées de prise en charge par le service de l'aide sociale à l'enfance et qui, par ailleurs, ne discute pas sérieusement la motivation de l'intéressé dans le suivi de sa formation et la gravité de sa situation depuis la décision du 10 janvier 2018, n'allègue pas que ces capacités limités faisaient obstacle à ce qu'il propose à M. B...un accompagnement adapté en vue qu'il accède, à l'issue de sa prise à charge, à des conditions, notamment d'hébergement et d'alimentation, propres à lui permettre d'achever l'année scolaire et d'obtenir le diplôme préparé. Dans ces conditions, il est manifeste que le département requérant n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif a jugé que, dans les circonstance particulières de l'espèce, sa carence caractérisée portait une atteinte grave et manifestement illégale à une liberté fondamentale justifiant qu'il soit enjoint au président du conseil départemental de Seine-et-Marne de proposer, dans les huit jours, à M. B...un accompagnement comportant l'accès à une solution de logement et de prise en charge de ses besoins alimentaires et sanitaires, moyennant éventuellement une participation financière de M. B... tenant compte de ses revenus, afin de lui permettre la poursuite de sa scolarité jusqu'à la fin de l'année scolaire. <br/>
<br/>
              9. Par suite, le département de Seine-et-Marne n'étant manifestement pas fondé à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Melun, il y a lieu de rejeter son appel en faisant application de l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du conseil départemental de Seine-et-Marne est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au président du conseil départemental de Seine et-Marne.<br/>
Copie en sera adressée à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
