<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026199001</ID>
<ANCIEN_ID>JG_L_2012_07_000000351159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/19/90/CETATEXT000026199001.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 11/07/2012, 351159</TITRE>
<DATE_DEC>2012-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:351159.20120711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 25 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la SARL Média Place Partners, dont le siège est situé 21 rue Kléber à Issy-les-Moulineaux (92130), représentée par ses dirigeants en exercice ; la société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 16 mars 2010 par laquelle le Conseil supérieur de l'audiovisuel (CSA) a refusé de conclure avec elle une convention en vue de la diffusion du service " Edonys TV " par les réseaux de communication électronique n'utilisant pas des fréquences assignées par le CSA ;<br/>
<br/>
              2°) d'enjoindre au CSA de conclure avec elle une convention dans un délai d'un mois à compter de la notification de la décision à intervenir, sous astreinte de 1 000 euros par semaine de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, Auditeur,  <br/>
<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en vertu des dispositions de l'article 33-1 de la loi du 30 septembre 1986 relative à la liberté de communication, et sous réserve des exceptions qu'elles prévoient, les services de radio et de télévision ne peuvent être diffusés par les réseaux n'utilisant pas des fréquences assignées par le Conseil supérieur de l'audiovisuel (CSA) qu'après qu'a été conclue avec lui une convention définissant les obligations particulières à ces services ;<br/>
<br/>
              2. Considérant que, le 1er septembre 2009, la SARL Média Place Partners a saisi le CSA, sur le fondement de ces dispositions, d'une demande tendant à la conclusion d'une convention en vue de la diffusion, sur les réseaux n'utilisant pas des fréquences assignées par le conseil supérieur, du programme de télévision " Edonys TV " consacré au vin et à la viticulture ; que, le 22 février 2010, la société Maya Média Groupe a saisi le CSA d'une demande tendant à la conclusion d'une convention en vue de la diffusion du programme " Deovino ", également consacré au vin et à la viticulture ; que, par une délibération du 16 mars 2010, le conseil supérieur a rejeté la demande de la SARL Média Place Partners et différé l'examen de la demande de la société Maya Média Groupe ; que le projet de cette société, repris par la société Deovino, a fait l'objet d'une décision favorable le 31 mai 2011 ; que la SARL Média Place Partners demande l'annulation du refus qui lui a été opposé ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              3. Considérant qu'aux termes du cinquième alinéa de l'article 5 de la loi du 30 septembre 1986 : " Pendant la durée de leurs fonctions et durant un an à compter de la cessation de leurs fonctions, les membres du conseil sont tenus de s'abstenir de toute prise de position publique sur les questions dont le conseil a ou a eu à connaître ou qui sont susceptibles de lui être soumises dans l'exercice de sa mission " ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que Mme Laborde, membre du CSA et rapporteur chargé de l'examen des demandes de conventionnement relative aux services " Edonys TV " et " Deovino ", a, par des propos tenus à un journaliste et repris le 10 mars 2010 sur le site internet de France Télévisions, manifesté une opinion défavorable au premier projet et favorable au second ; que ces appréciations, portées sur les demandes qu'elle était chargée d'examiner et exprimées publiquement en méconnaissance des dispositions législatives précitées, ont été de nature à mettre en cause la nécessaire garantie d'impartialité de l'intéressée et à entacher par suite d'irrégularité la délibération du Conseil supérieur de l'audiovisuel du 16 mars 2010, à laquelle elle a participé ; que la SARL Média Place Partners est fondée à demander, pour ce motif, l'annulation du refus qui lui a été opposé ; <br/>
<br/>
              5. Considérant que l'annulation, par la présente décision, du refus du CSA de conclure avec la requérante une convention en vue de la diffusion du service " Edonys TV " n'implique pas nécessairement que le conseil conclue cette convention mais seulement qu'il se prononce à nouveau sur la demande de la requérante ; que, dès lors, il y a lieu pour le Conseil d'Etat d'ordonner au Conseil supérieur de l'audiovisuel de réexaminer la demande de la SARL Média Place Partners dans un délai de trois mois à compter de la notification de la présente décision ; qu'il n'y pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SARL Média Place Partners au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 16 mars 2010 du Conseil supérieur de l'audiovisuel est annulée. <br/>
<br/>
Article 2 : Il est enjoint au Conseil supérieur de l'audiovisuel de prendre les mesures nécessaires au réexamen, dans le délai de trois mois à compter de la notification de la présente décision, de la demande de la SARL Média Place Partners tendant à la conclusion d'une convention en vue de la diffusion du service " Edonys TV " par des réseaux de communication électronique n'utilisant pas les fréquences assignées par le CSA. <br/>
<br/>
Article 3 : L'Etat versera à la SARL Média Place Partners la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SARL Média Place Partners et au Conseil supérieur de l'audiovisuel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPE D'IMPARTIALITÉ - CHAMP D'APPLICATION - DÉLIBÉRATIONS DU CSA [RJ1] - RAPPORTEUR AYANT MANIFESTÉ PUBLIQUEMENT UNE OPINION SUR LA SUITE À DONNER À UNE DEMANDE DE CONVENTIONNEMENT RELATIVE À UN SERVICE DE TÉLÉVISION ALORS QU'IL EST CHARGÉ DE SON EXAMEN - MÉCONNAISSANCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">52-045 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. - DÉLIBÉRATIONS DU CSA - PRINCIPE D'IMPARTIALITÉ [RJ1] - MÉCONNAISSANCE - EXISTENCE - RAPPORTEUR AYANT MANIFESTÉ PUBLIQUEMENT UNE OPINION SUR LA SUITE À DONNER À UNE DEMANDE DE CONVENTIONNEMENT RELATIVE À UN SERVICE DE TÉLÉVISION ALORS QU'IL EST CHARGÉ DE SON EXAMEN.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - PRINCIPE D'IMPARTIALITÉ [RJ1] - MÉCONNAISSANCE - EXISTENCE - RAPPORTEUR AYANT MANIFESTÉ PUBLIQUEMENT UNE OPINION SUR LA SUITE À DONNER À UNE DEMANDE DE CONVENTIONNEMENT RELATIVE À UN SERVICE DE TÉLÉVISION ALORS QU'IL EST CHARGÉ DE SON EXAMEN.
</SCT>
<ANA ID="9A"> 01-04-03 Membre du Conseil supérieur de l'audiovisuel (CSA), rapporteur chargé de l'examen des demandes de conventionnement relatives à deux services de télévision, ayant manifesté, par des propos tenus à un journaliste et repris sur le site internet d'une chaîne d'information, une opinion défavorable au premier projet et favorable au second. Ces appréciations, portées sur les demandes qu'il était chargé d'examiner et exprimées publiquement en méconnaissance des dispositions du cinquième alinéa de l'article 5 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication, ont été de nature à mettre en cause la nécessaire garantie d'impartialité de l'intéressé et à entacher par suite d'irrégularité la délibération du CSA sur le premier projet, à laquelle il a participé. Par suite, annulation du refus opposé à ce projet.</ANA>
<ANA ID="9B"> 52-045 Membre du Conseil supérieur de l'audiovisuel (CSA), rapporteur chargé de l'examen des demandes de conventionnement relatives à deux services de télévision, ayant manifesté, par des propos tenus à un journaliste et repris sur le site internet d'une chaîne d'information, une opinion défavorable au premier projet et favorable au second. Ces appréciations, portées sur les demandes qu'il était chargé d'examiner et exprimées publiquement en méconnaissance des dispositions du cinquième alinéa de l'article 5 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication, ont été de nature à mettre en cause la nécessaire garantie d'impartialité de l'intéressé et à entacher par suite d'irrégularité la délibération du CSA sur le premier projet, à laquelle il a participé. Par suite, annulation du refus opposé à ce projet.</ANA>
<ANA ID="9C"> 56-01 Membre du Conseil supérieur de l'audiovisuel (CSA), rapporteur chargé de l'examen des demandes de conventionnement relatives à deux services de télévision, ayant manifesté, par des propos tenus à un journaliste et repris sur le site internet d'une chaîne d'information, une opinion défavorable au premier projet et favorable au second. Ces appréciations, portées sur les demandes qu'il était chargé d'examiner et exprimées publiquement en méconnaissance des dispositions du cinquième alinéa de l'article 5 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication, ont été de nature à mettre en cause la nécessaire garantie d'impartialité de l'intéressé et à entacher par suite d'irrégularité la délibération du CSA sur le premier projet, à laquelle il a participé. Par suite, annulation du refus opposé à ce projet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 30 décembre 2010, Société Métropole Télévision (M6), n° 338273, p. 544.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
