<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041903249</ID>
<ANCIEN_ID>JG_L_2020_05_000000440538</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/90/32/CETATEXT000041903249.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/05/2020, 440538, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440538</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440538.20200518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Toulon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, d'annuler la décision en date du 11 février 2020 par laquelle le paiement de sa solde a été suspendu et d'enjoindre à la ministre des armées de lui restituer la rémunération dont il a été privé du fait de cette décision lors de la période allant du 22 janvier au 30 avril 2020, en deuxième lieu, de suspendre les effets de cette décision, en troisième lieu, d'enjoindre à la ministre des armées de se prononcer sur l'attribution d'un nouveau congé de longue durée pour maladie, en quatrième lieu, de lui enjoindre de faire cesser les faits de harcèlement moral dont il est victime et de lui accorder le bénéfice de la protection fonctionnelle, enfin, en cinquième lieu, de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Par une ordonnance n° 200186 du 24 avril 2020, le juge des référés du tribunal administratif de Toulon a rejeté cette demande.<br/>
<br/>
              Par une ordonnance du 11 mai 2020, enregistrée le 12 mai 2020 au secrétariat du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au juge des référés du Conseil d'Etat la requête d'appel par laquelle M. B... demande :<br/>
<br/>
              1°) d'annuler l'ordonnance du 24 avril 2020 ;<br/>
<br/>
              2°) d'annuler la décision en date du 11 février par laquelle le paiement de sa solde a été suspendu ;<br/>
<br/>
              3°) à titre principal, de faire droit à ses conclusions aux fins d'injonction de première instance, à titre subsidiaire qu'il soit enjoint à la ministre des armées de lui octroyer une aide sociale d'urgence d'un montant de 6 000 euros pour lui permettre de faire face aux besoins de sa famille dans le cadre de la crise sanitaire liée à l'épidémie de COVID 19 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie, car du fait de la suspension du paiement de sa solde, il se trouve, avec sa famille, dans une situation de grande précarité, compte tenu des charges incompressibles qui sont les siennes, comme l'atteste le fait que son compte bancaire est débiteur, à hauteur d'un montant qui dépasse celui de son découvert autorisé ;<br/>
              - il est porté atteinte à plusieurs de ses droits fondamentaux, notamment celui de mener une vie familiale normale, le droit au travail, le droit au respect de la dignité de la personne humaine, le droit ne pas être soumis au harcèlement moral, le droit à la santé et le droit à ne pas être soumis à des traitements inhumains et dégradants ;<br/>
              - la décision de suspendre le paiement de sa solde a été prise en méconnaissance de la circulaire interministérielle du 30 janvier 1989 relative à la protection sociale des fonctionnaires et stagiaires de l'Etat, faute que cette décision ait été précédée d'une mise en demeure comme le requiert cette circulaire ;<br/>
              - cette décision est intervenue au motif qu'il ne se serait indûment pas rendu, le 22 janvier 2020, à une convocation à un examen de santé préalable à la prolongation de son congé de longue durée pour maladie, alors qu'il avait apporté les justificatifs nécessaires, notamment des certificats médicaux indiquant qu'il ne pouvait se rendre, pour des motifs de santé, ni dans un lieu trop éloigné de son domicile, ni dans une enceinte militaire ;<br/>
              - il n'a pas été informé sans délai des motifs de cette décision, en méconnaissance des dispositions de l'article L. 211-2 du code des relations du public avec l'administration ;<br/>
              - il est victime de faits de harcèlement moral depuis plusieurs années, dont la décision de suspendre sa solde est la dernière illustration.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais. ". Il résulte de ces dispositions que le juge des référés ne peut, sans excéder sa compétence, prononcer l'annulation d'une décision administrative. Il suit de là que M. B... n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Toulon a rejeté comme ne relevant pas de son office ses demandes tendant à l'annulation de la décision du 11 février 2020 par laquelle le paiement de sa solde a été suspendue et à la restitution des sommes qui, du fait de l'intervention de cette décision, ne lui ont pas été versées au titre de la période allant du 22 janvier au 30 avril 2020.<br/>
<br/>
              3. En second lieu, pour rejeter les demandes de M. B... tendant à ce qu'il soit enjoint à la ministre des armées de suspendre l'exécution de la décision ayant interrompu le versement de sa solde, de réexaminer sa demande de congé de maladie de longue durée et de mettre fin à la situation de harcèlement moral dont il se prévaut, le juge des référés du tribunal administratif de Toulon s'est fondé sur le fait que les éléments mis en avant par celui-ci n'établiraient pas l'urgence qu'il y aurait à prendre de telles injonctions et sur l'absence d'identification claire, dans sa demande, de mesures de nature à mettre fin au harcèlement moral qu'il subirait, que ce juge a estimé, au demeurant, non établi.<br/>
<br/>
              4. D'une part, il ressort tant des pièces du dossier de première instance que de la requête d'appel de M. B... que, pour établir la situation d'urgence dont il se prévaut et l'atteinte qui en résulterait à son droit à mener une vie familiale normale, celui-ci se borne à faire état des charges qui seraient les siennes chaque mois, sans les établir, et du montant du solde débiteur de son compte bancaire à la date du 21 avril 2020, sans fournir d'autre justificatif des difficultés financières qu'il allègue, d'autre part, il ressort des mêmes pièces que le harcèlement moral dont il fait état durerait depuis plusieurs années et a déjà donné lieu à diverses procédures contentieuses. Il suit de là que M. B... n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Toulon a considéré qu'il ne justifiait pas de l'urgence qu'il y aurait à prendre, sur le fondement de l'article L. 521-2 du code de justice administrative, les mesures qu'il demande.<br/>
<br/>
              5. Il n'y a pas lieu, pour les motifs énoncés au point 4 ci-dessus, de faire droit aux conclusions subsidiaires de M. B....<br/>
<br/>
              6. Il résulte de tout ce qui précède qu'il est manifeste que l'appel de M. B... ne peut être accueilli. Sa requête, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative, ne peut dès lors qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
