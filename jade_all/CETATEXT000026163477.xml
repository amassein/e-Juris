<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026163477</ID>
<ANCIEN_ID>JG_L_2012_07_000000359157</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/16/34/CETATEXT000026163477.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 09/07/2012, 359157, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359157</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:359157.20120709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le mémoire, enregistré le 4 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par l'union d'économie sociale du logement (UESL), dont le siège est 66, avenue du Maine à Paris Cedex 14 (75682), en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; l'UESL demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation du décret n° 2012-353 du 12 mars 2012 relatif aux enveloppes minimales et maximales des emplois de la participation des employeurs à l'effort de construction, de l'arrêté du 12 mars 2012 relatif à l'échéancier de versement des subventions de l'Union d'économie sociale du logement à l'Agence nationale pour la rénovation urbaine pour la mise en oeuvre du programme national de rénovation urbaine, et de l'arrêté du 12 mars 2012 relatif à l'échéancier de versement des subventions de l'Union d'économie sociale du logement à l'Agence nationale de l'habitat pour le soutien à l'amélioration du parc privé, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 313-3 du code de la construction et de l'habitation ;  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu l'article L. 313-3 du code de la construction et de l'habitation ;<br/>
<br/>
              Vu la loi n° 2009-323 du 25 mars 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié avocat du ministre de l'écologie, du développement durable et de l'énergie ;<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié avocat du ministre de l'écologie, du développement durable et de l'énergie ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              2. Considérant qu'il résulte de l'article L. 313-1 du code de la construction et de l'habitation que les employeurs assujettis à la taxe sur les salaires et occupant au moins vingt salariés, à l'exception de l'Etat, des collectivités territoriales et de leurs établissements publics administratifs et de ceux qui relèvent du régime agricole au regard des lois sur la sécurité sociale, doivent consacrer des sommes représentant 0,45 % au moins du montant des rémunérations versées par eux au cours de l'exercice écoulé au financement d'actions dans le domaine du logement ; qu'en vertu du même article, cette obligation prend la forme d'un versement à un organisme agréé par le ministre chargé du logement, l'employeur pouvant s'en libérer en investissant directement en faveur du logement de ses salariés ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 313-3 du même code, dans sa rédaction issue de la loi du 25 mars 2009 de mobilisation pour le logement et la lutte contre l'exclusion, les ressources de la participation des employeurs à l'effort de construction " (...) sont consacrées aux catégories d'emplois suivantes : (...) d) A la mise en oeuvre du programme national de rénovation urbaine ; / e) A la mise en oeuvre du programme national de requalification des quartiers anciens dégradés ainsi qu'au soutien à l'amélioration du parc privé ; / (...) Ces interventions peuvent prendre la forme de prêts, d'avances sur travaux, de prises de participation, d'octrois de garantie ou de subventions à des personnes physiques ou morales, à des opérateurs de l'Etat ou à des associations agréées par l'Etat. / Les ressources consacrées aux catégories d'emplois visées aux b, c, d et e donnent lieu à des contreparties qui peuvent prendre la forme de droits de réservation portant sur des logements locatifs, dans les conditions prévues par l'article L. 313-26. / (...) Pour chaque catégorie d'emplois, la nature des emplois correspondants et leurs règles d'utilisation sont fixées par décret en Conseil d'Etat, pris après concertation avec les représentants des organisations syndicales et patronales membres de l'Union d'économie sociale du logement. La répartition des ressources de la participation des employeurs à l'effort de construction entre chacune des catégories d'emplois mentionnées au présent article est fixée par un document de programmation établi pour une durée de trois ans par les ministres chargés du logement et du budget après concertation avec les représentants des organisations syndicales et patronales membres de l'Union d'économie sociale du logement (...) Les enveloppes minimales et maximales consacrées annuellement à chaque emploi ou catégorie d'emplois sont fixées pour une durée de trois ans par décret pris après concertation avec les représentants des organisations syndicales et patronales membres de l'Union d'économie sociale du logement (...) " ;<br/>
<br/>
              4. Considérant, en premier lieu, que l'Union d'économie sociale du logement (UESL), qui est une société anonyme coopérative chargée notamment, aux termes du troisième alinéa de l'article L. 313-19 du code de la construction et de l'habitation, d'assurer la mise en oeuvre des politiques nationales d'emploi des ressources issues de la participation des employeurs à l'effort de construction, soutient que les dispositions de l'article L. 313-3 de ce code portent atteinte au droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen en ce qu'elles autorisent le pouvoir réglementaire à fixer unilatéralement la répartition de ces ressources entre les catégories d'emplois qu'elles prévoient ; que, toutefois, les organismes chargés de collecter les fonds de la participation des employeurs à l'effort de construction ne détiennent ces fonds que du fait de la loi et sous les conditions qu'elle fixe ; que, dès lors, les dispositions en cause, qui déterminent l'emploi des fonds en tenant notamment compte de l'intérêt général qui s'attache à la cohérence et à l'efficacité des politiques nationales de rénovation urbaine et d'amélioration de l'habitat, ne sauraient être regardées comme contraires aux normes constitutionnelles garantissant le droit de propriété ; <br/>
<br/>
              5. Considérant, en second lieu, que l'UESL soutient que les dispositions de l'article L. 313-3 du code de la construction et de l'habitation sont contraires au principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen en ce qu'elles mettraient le financement d'actions nationales en faveur du logement à la charge des seules entreprises assujetties à la participation des employeurs à l'effort de construction prévue par l'article L. 313-1 du même code ; que les employeurs soumis à l'obligation de participer à l'effort de construction peuvent toutefois choisir de s'acquitter de cette obligation en investissant directement en faveur du logement de leurs propres salariés ; que les ressources collectées par les organismes agréés qui sont affectées aux catégories d'emplois prévues aux d et e de l'article L. 313-3 donnent lieu à des contreparties pouvant prendre la forme de droits de réservation portant sur des logements locatifs susceptibles de bénéficier à des salariés ; qu'ainsi, les dispositions contestées ne peuvent, en tout état de cause, être regardées comme créant une rupture caractérisée de l'égalité devant les charges publiques ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 313-3 du code de la construction et de l'habitation porte atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'Union d'économie sociale du logement.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Union d'économie sociale du logement et à la ministre de l'égalité des territoires et du logement. <br/>
		Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
