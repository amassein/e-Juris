<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028323661</ID>
<ANCIEN_ID>JG_L_2013_12_000000343688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/32/36/CETATEXT000028323661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 06/12/2013, 343688</TITRE>
<DATE_DEC>2013-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:343688.20131206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 5 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. A... B..., demeurant..., et pour le conseil coutumier de l'aire Djubea-Kapone, dont le siège est BP 146 à Nouméa (98845), représenté par son président ; M. B...et le conseil coutumier de l'aire Djubea-Kapone demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07PA01595 du 7 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté leur requête tendant à l'annulation du jugement n ° 0600267 du 21 février 2007 du tribunal administratif de Nouvelle-Calédonie rejetant leur demande tendant à l'annulation pour excès de pouvoir de la décision du 17 novembre 2005 par laquelle le sénat coutumier de Nouvelle-Calédonie a refusé de constater la désignation de M. B... comme chef de la tribu de Comagna ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la Nouvelle-Calédonie la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi organique n° 99-209 du 19 mars 1999 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, Auditeur, <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier - Bourdeau, Lecuyer, avocat de M. B...et du conseil coutumier de l'aire Djubea-Kapone ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, le 24 mai 2004, un palabre s'est tenu  en vue de la désignation du chef de la tribu de Comagna dans le district coutumier de l'île des Pins, appartenant à l'aire coutumière Djubea-Kapone en Nouvelle-Calédonie ; qu'un procès-verbal constatant la désignation de M. A...B...comme chef de la tribu de Comagna a été transmis à l'issue de ce palabre, par le président du gouvernement de la Nouvelle-Calédonie, au président du sénat coutumier aux fins de constatation de cette désignation en application de l'article 141 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie ; que cette désignation a toutefois été contestée devant le sénat coutumier, le 31 mai 2004, par quatre clans de la tribu ; que, par une délibération du 17 novembre 2005, le sénat coutumier a refusé de constater la désignation de M. B...en qualité de chef de la tribu de Comagna ; que M. B...et le conseil coutumier de l'aire Djuba-Kapone se pourvoient en cassation contre l'arrêt du 7 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté leur appel contre le jugement du tribunal administratif de Nouvelle-Calédonie du 21 février 2007 rejetant leur recours pour excès de pouvoir contre cette délibération ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 2 de la loi organique du 19 mars 1999 : " Les institutions de la Nouvelle-Calédonie comprennent le congrès, le gouvernement, le sénat coutumier, le conseil économique et social et les conseils coutumiers (...) " ; que selon l'article 141 de cette loi : " Le sénat coutumier constate la désignation des autorités coutumières et la notifie au président du gouvernement qui en assure la publication au Journal officiel de la Nouvelle-Calédonie. Cette désignation est également notifiée au haut-commissaire et aux présidents des assemblées de province " ; qu'aux termes du II de l'article 150 de la même loi : " En cas de litige sur l'interprétation d'un procès-verbal de palabre coutumier, les parties saisissent le conseil coutumier, qui rend sa décision dans un délai maximum de trois mois " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le sénat coutumier, saisi d'une demande en ce sens, est tenu de constater la désignation des autorités coutumières dès lors que le procès-verbal de palabre les désignant n'est pas entaché d'une irrégularité d'une gravité telle qu'il devrait être regardé comme inexistant ; que, par suite, alors qu'il ne ressortait pas des pièces du dossier qui lui était soumis et qu'il n'était d'ailleurs nullement allégué que le procès verbal du palabre qui s'était tenu le 24 mai 2004 pour la désignation du chef de la tribu de Comagna était entaché d'une telle irrégularité, la cour administrative d'appel de Paris a commis une erreur de droit en retenant que le sénat coutumier n'était pas tenu de constater la désignation de M. B... ; qu'il suit de là, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, que M. B...et le conseil coutumier de l'aire Djubea-Kapone sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Nouvelle-Calédonie la somme de 1 500 euros chacun à verser à M. B...et au conseil coutumier de l'aire Djubea-Kapone au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 7 juin 2010 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : La Nouvelle-Calédonie versera à M. B...et au conseil coutumier de l'aire Djubea - Kapone la somme de 1 500 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au conseil coutumier de l'aire Djubea-Kapone et au sénat coutumier de la Nouvelle-Calédonie. <br/>
Copie pour information en sera adressée au ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - CONTESTATION DE LA DÉLIBÉRATION DU SÉNAT COUTUMIER DE NOUVELLE-CALÉDONIE REFUSANT DE CONSTATER LA DÉSIGNATION D'UN CHEF DE TRIBU - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE - EXISTENCE (SOL. IMPL.).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. DROIT APPLICABLE. STATUTS. NOUVELLE-CALÉDONIE. - AUTORITÉS COUTUMIÈRES - DÉSIGNATION D'UN CHEF DE TRIBU - DÉLIBÉRATION DU SÉNAT COUTUMIER REFUSANT DE CONSTATER LA DÉSIGNATION - 1) COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE POUR CONNAÎTRE DE LA CONTESTATION DE CETTE DÉLIBÉRATION - EXISTENCE (SOL. IMPL.) - 2) DÉCISION SUSCEPTIBLE DE RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE (SOL. IMPL.) - 3) COMPÉTENCE LIÉE DU SÉNAT COUTUMIER POUR CONSTATER LA DÉSIGNATION DES AUTORITÉS COUTUMIÈRES - EXISTENCE - RÉSERVE - INEXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - DÉLIBÉRATION DU SÉNAT COUTUMIER DE NOUVELLE-CALÉDONIE REFUSANT DE CONSTATER LA DÉSIGNATION D'UN CHEF DE TRIBU (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 17-03 La juridiction administrative est compétente pour connaître de la contestation de la délibération par laquelle le sénat coutumier de Nouvelle-Calédonie refuse de constater la désignation d'un chef de tribu.</ANA>
<ANA ID="9B"> 46-01-02-01 1) La juridiction administrative est compétente pour connaître de la contestation de la délibération par laquelle le sénat coutumier de Nouvelle-Calédonie refuse de constater la désignation d'un chef de tribu.,,,2) La délibération par laquelle le sénat coutumier de Nouvelle-Calédonie refuse de constater la désignation d'un chef de tribu est une décision susceptible de recours pour excès de pouvoir.,,,3) Il résulte des dispositions de l'article 141 et du II de l'article 150 de la loi n° 99-209 du 19 mars 1999 relative à la Nouvelle-Calédonie que le sénat coutumier, saisi d'une demande en ce sens, est tenu de constater la désignation des autorités coutumières dès lors que le procès-verbal de palabre les désignant n'est pas entaché d'une irrégularité d'une gravité telle qu'il devrait être regardé comme inexistant.</ANA>
<ANA ID="9C"> 54-01-01-01 La délibération par laquelle le sénat coutumier de Nouvelle-Calédonie refuse de constater la désignation d'un chef de tribu est une décision susceptible de recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
