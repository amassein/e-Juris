<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043358773</ID>
<ANCIEN_ID>JG_L_2021_04_000000436264</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/87/CETATEXT000043358773.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/04/2021, 436264</TITRE>
<DATE_DEC>2021-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436264</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436264.20210408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... C..., épouse D... a demandé au tribunal administratif de Nantes d'annuler la décision du 13 novembre 2015 par laquelle le ministre de l'intérieur a rejeté sa demande de naturalisation ainsi que la décision implicite de rejet de son recours gracieux formé le 18 janvier 2016.<br/>
<br/>
              Par un jugement n° 1605290 du 6 décembre 2018, le tribunal administratif de Nantes a annulé ces décisions.<br/>
<br/>
              Par un arrêt n° 19NT00485 du 26 septembre 2019, la cour administrative d'appel de Nantes a, sur appel du ministre de l'intérieur, annulé ce jugement et rejeté la demande de Mme C....<br/>
<br/>
              Par un pourvoi sommaire, et un mémoire complémentaire, enregistrés le 26 novembre 2019 et le 26 février 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par une décision en date du 13 novembre 2015 le ministre de l'intérieur a rejeté la demande de naturalisation que Mme C... avait présentée sur le fondement de l'article 21-15 du code civil. Le tribunal administratif de Nantes a annulé cette décision par un jugement du 6 décembre 2018. Mme C... se pourvoit en cassation contre l'arrêt du 26 septembre 2019 par lequel, sur appel du ministre de l'intérieur, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 21-15 du code civil : " L'acquisition de la nationalité française par décision de l'autorité publique résulte d'une naturalisation accordée par décret à la demande de l'étranger ".<br/>
<br/>
              3. L'autorité administrative dispose, en matière de naturalisation ou de réintégration dans la nationalité française, d'un large pouvoir d'appréciation. Elle peut, dans l'exercice de ce pouvoir, prendre en considération les liens particuliers du demandeur avec un tiers, notamment le conjoint. Elle peut, à cet égard, rejeter une demande de naturalisation si elle estime, notamment, que de tels liens sont susceptibles d'affecter l'intérêt que présenterait pour le pays l'octroi de la nationalité française au demandeur.<br/>
<br/>
              4. En premier lieu, pour accueillir l'appel du ministre et rejeter la demande d'annulation de l'intéressée, les juges d'appel ont relevé que le ministre s'était fondé sur la circonstance que son mari, M. A... D... avait, en qualité de ministre du plan du Rwanda, directement et publiquement incité à commettre le génocide de 1994, faits dont i1 a été reconnu coupable et pour lesquels il a été condamné à une peine de trente ans d'emprisonnement par un arrêt du Tribunal Pénal International pour le Rwanda du 18 décembre 2014. Ils ont constaté que Mme C... était toujours mariée avec M. D... et continuait à entretenir des relations avec lui. En jugeant que le ministre avait pu, sans illégalité, rejeter la demande de l'intéressée en raison de l'existence de son mariage avec. M. D..., après avoir estimé, au vu de constatations souveraines exemptes de dénaturation des faits, que de tels liens étaient, dans les circonstances de l'espèce, susceptibles de justifier ce refus, les juges d'appel n'ont commis aucune erreur de droit. <br/>
<br/>
              5. En second lieu, en jugeant que la décision litigieuse n'ayant pour effet ni d'empêcher le mariage des intéressés, ni de leur imposer le divorce ne méconnaissait pas les stipulations de l'article 12 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui garantit le droit au mariage, les juges d'appel n'ont commis aucune erreur de droit.   <br/>
<br/>
              6. Il résulte de ce qui précède que Mme C... n'est pas est fondée à demander l'annulation de l'arrêt attaqué. Son pourvoi doit, par suite, être rejeté.   <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B... C... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme C... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-01-01-03 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. NATIONALITÉ. ACQUISITION DE LA NATIONALITÉ. NATURALISATION. - POSSIBILITÉ POUR L'ADMINISTRATION DE REJETER UNE DEMANDE NATURALISATION LORSQUE LES LIENS PARTICULIERS DU DEMANDEUR AVEC UN TIERS SONT SUSCEPTIBLES D'AFFECTER L'INTÉRÊT QUE PRÉSENTERAIT POUR LE PAYS L'OCTROI DE LA NATIONALITÉ FRANÇAISE - EXISTENCE - ILLUSTRATION.
</SCT>
<ANA ID="9A"> 26-01-01-01-03 L'autorité administrative dispose, en matière de naturalisation ou de réintégration dans la nationalité française, d'un large pouvoir d'appréciation. Elle peut, dans l'exercice de ce pouvoir, prendre en considération les liens particuliers du demandeur avec un tiers, notamment le conjoint. Elle peut, à cet égard, rejeter une demande de naturalisation si elle estime, notamment, que de tels liens sont susceptibles d'affecter l'intérêt que présenterait pour le pays l'octroi de la nationalité française au demandeur.,,,Ministre s'étant fondé, pour rejeter une demande de naturalisation, sur la circonstance que le mari de la demandeuse, avec qui elle continuait à entretenir des liens, avait, en qualité de ministre du plan du Rwanda, directement et publiquement incité à commettre le génocide de 1994, faits dont il a été reconnu coupable et pour lesquels il a été condamné à une peine de trente ans d'emprisonnement par un arrêt du Tribunal Pénal International pour le Rwanda du 18 décembre 2014.... ,,Le ministre a pu, sans illégalité, rejeter la demande de l'intéressée en raison de l'existence de ce mariage, de tels liens étant, dans les circonstances de l'espèce, de nature à justifier ce refus.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
