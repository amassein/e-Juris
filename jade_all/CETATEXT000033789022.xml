<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033789022</ID>
<ANCIEN_ID>JG_L_2016_12_000000397049</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/90/CETATEXT000033789022.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 30/12/2016, 397049, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397049</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Jean-Philippe Mochon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:397049.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
      La société Les vents du Catésis a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir les arrêtés du 6 décembre 2011 par lesquels le préfet de la région Nord-Pas-de-Calais a refusé de lui délivrer cinq permis de construire des éoliennes sur le territoire des communes de Bazuel et de Catillon-sur-Sambre (Nord). Par un jugement nos 1104197, 1201409 du 27 mai 2014, le tribunal administratif de Lille a rejeté sa demande.<br/>
      Par un arrêt n° 14DA01343 du 10 décembre 2015, la cour administrative d'appel de Douai a annulé le jugement du tribunal administratif de Lille ainsi que les arrêtés du préfet de la région Nord-Pas-de-Calais.<br/>
      Par un pourvoi et un mémoire complémentaire, enregistrés les 16 février et 10 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la ministre du logement et de l'habitat durable demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
      	Vu les autres pièces du dossier ;<br/>
      	Vu :<br/>
      	- le code de l'urbanisme ;<br/>
      	- le code de l'environnement ;<br/>
      	- le code de justice administrative ;<br/>
<br/>
<br/>
      	Après avoir entendu en séance publique :<br/>
<br/>
      	- le rapport de M. Jean-Philippe Mochon, conseiller d'Etat,  <br/>
<br/>
      	- les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
      	La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la société Les vents du Catésis.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par cinq arrêtés du 6 décembre 2011, le préfet de la région Nord-Pas-de-Calais a refusé à la société Les vents du Catésis la délivrance de permis de construire pour cinq éoliennes sur le territoire des communes de Bazuel et Catillon-sur-Sambre (Nord) ; que la ministre du logement et de l'habitat durable se pourvoit en cassation contre l'arrêt du 10 décembre 2015 par lequel la cour administrative d'appel de Douai a annulé ces arrêtés ainsi que le jugement du tribunal administratif de Lille qui avait rejeté la demande dirigée contre ces arrêtés par la société Les vents du Catésis ;<br/>
<br/>
              	2. Considérant qu'aux termes de l'article R. 111-2 du code de l'urbanisme dans sa rédaction applicable : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature à porter atteinte à la salubrité ou à la sécurité publique du fait de sa situation, de ses caractéristiques, de son importance ou de son implantation à proximité d'autres installations " ;<br/>
<br/>
              	3. Considérant que la cour administrative d'appel a jugé que le préfet du Nord, en estimant que le projet de parc éolien entraînerait pour le fonctionnement du radar météorologique d'Avesnes-sur-Helpe des conséquences de nature à établir l'existence d'un risque pour la sécurité et la salubrité publique, avait méconnu les dispositions citées ci-dessus de l'article R. 111-2 du code de l'urbanisme ;<br/>
<br/>
              	4. Considérant que la cour, pour porter cette appréciation, s'est fondée, après avoir rappelé les caractéristiques du radar météorologique et sa distance avec le projet d'éoliennes, tant sur les degrés de probabilité et de gravité du risque de perturbations temporaires induites par le projet d'éoliennes sur le fonctionnement du radar météorologique que sur la circonstance qu'une altération momentanée des données de ce radar n'aggraverait pas les risques liés au risque de crue des cours d'eau et enfin sur l'absence de conséquence significative de pertes ponctuelles de données au niveau du radar d'Avesnes sur la sécurité des agglomérations couvertes par le système " Arome " auquel celui-ci participe ; <br/>
<br/>
              	5. Considérant que la cour administrative d'appel a ainsi regardé le risque induit par le projet de parc éolien en matière de perturbation des données relevées par le radar météorologique d'Avesnes comme temporaire et momentané ; que, cependant, il ne ressort pas des pièces du dossier soumis aux juges du fond que ce risque de perturbation, quelles que soient les évaluations de sa portée, soit limité dans le temps ; qu'au contraire, il ressort des pièces du dossier du dossier soumis aux juges du fond que ce risque de perturbation sur la qualité des données relevées par le radar météorologique, induit par l'implantation physique des éoliennes dans cette zone de couverture, revêt un caractère permanent ; que, par suite, la cour administrative d'appel a entaché son arrêt de dénaturation des faits de l'espèce ; <br/>
<br/>
              	6. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la ministre du logement et de l'habitat durable est fondée à demander l'annulation de l'arrêt du 10 décembre 2015 de la cour administrative d'appel de Douai ; que, par suite, les conclusions présentées par la société Les vents du Catésis au titre de l'article L. 761-1 du code de justice administrative, ne peuvent qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 10 décembre 2015 de la cour administrative d'appel de Douai est annulé. <br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Douai.<br/>
Article 3 : Les conclusions présentées par la société Les vents du Catésis au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre du logement et de l'habitat durable. Copie en sera adressée pour information à la société Les vents du Catésis.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
