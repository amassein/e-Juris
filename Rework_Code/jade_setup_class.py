# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 13:42:47 2021

@author: Marc
"""
install_src="./Jade_dependency/"
import re

class Jade_cod:
    cod_text = []
    cod_text_tk=[]
    def __init__(self):
        list_cod = open(install_src+'jade_codes.txt','r', encoding="utf-8")
        cod_lines = list_cod.readlines()
        list_cod.close()
        for c in cod_lines:
            self.cod_text.append(c.strip())
            self.cod_text_tk.append('COD')
        self.cod_par=list(map(lambda x, y:(x,y), self.cod_text, self.cod_text_tk))
    def show(self):
        print(self.cod_text)

class Jade_pattern:
    pattern_re = []
    pattern_text = []
    def __init__(self):
        list_pat = open(install_src+'jade_patterns.txt','r', encoding="utf-8")
        pat_lines = list_pat.readlines()
        list_pat.close()
        for t in pat_lines:
            self.pattern_re.append(t.split(' ')[0].strip())
            self.pattern_text.append(t.split(' ')[1].strip())
        self.pat_par=list(map(lambda x, y:(x,y), self.pattern_re, self.pattern_text))
    def show(self):
        print(self.pattern_text)
        print(self.pattern_re)
        
        
class Jade_rewrite:
    rw_text = []
    rw_text_repl=[]
    def __init__(self):
        list_rw = open(install_src+'jade_rewrite.txt','r', encoding="utf-8")
        rw_lines = list_rw.readlines()
        list_rw.close()
        for c in rw_lines:
            txtsp=c.split(' ',1)
            txt=txtsp[0]
            txt_s=txtsp[1]
            self.rw_text.append(txt)
            self.rw_text_repl.append(txt_s)
        self.rw_par=list(map(lambda x, y:(x,y), self.rw_text, self.rw_text_repl))



class Jade_tag:
    tag_text = []
    tag_tag = []
    def __init__(self):
        list_tag = open(install_src+'/jade_tags.txt','r', encoding="utf-8")
        tag_lines = list_tag.readlines()
        list_tag.close()
        for t in tag_lines:
            self.tag_text.append(t.split(' ')[0].strip())
            self.tag_tag.append(t.split(' ')[1].strip())
    def show(self):
        print(self.tag_text)
        print(self.tag_tag)

class Jade_art:
    art_re = []
    art_text = []
    def __init__(self):
        list_art = open(install_src+'jade_articles.txt','r', encoding="utf-8")
        art_lines = list_art.readlines()
        list_art.close()
        for t in art_lines:
            self.art_re.append(t.split(' ')[0].strip())
            self.art_text.append(t.split(' ')[1].strip())
        self.art_par=list(map(lambda x, y:(x,y), self.art_re, self.art_text))
    def show(self):
        print(self.art_text)
        print(self.art_re)