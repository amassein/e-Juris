import Librairie_ejuris as Le
import numpy as np
import pickle

dico = {}

for i, simi in enumerate(Le.indexSOM):
    #print(i, len(Le.indexSOM))
    simi[i] = 0
    temp = list(np.where(simi > 0.95)[0])
    if len(temp) > 0:
        stock = []
        doc1 = Le.get_ref_considSOM(i)[0]
        for j in temp:
            if Le.get_ref_considSOM(j)[0] == doc1:
                stock.append(j)
        if len(stock) > 0:
            dico[i] = stock
            print(f"\r{i}, {dico[i]}", end = "")

pickle.dump(dico, open("./Result_temp/Dico_simi.xp","wb"))