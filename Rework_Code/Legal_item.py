import Librairie_ejuris as Le
import pandas as pd
import pickle
import numpy as np
from gensim import corpora
from sklearn.feature_extraction.text import CountVectorizer
import time
import sys


def Xport_legalitem_visa():
    
    stock = {}
    count = 0
    
    doc_list = Le.corpus.fileids()
    f = open("./Result_temp/no_visa.txt", "w")
    for i, doc in enumerate(doc_list):
        if doc.endswith('.xml'):
            stock[doc] = [doc]
            visa,_ = Le.get_sub_visa(doc, print=False)
            legal_item = Le.get_tokenize_juri_items(visa)
            if i != 0:
                print(f"\r{count}, {i} {(count/i)*100}", end="")
            if legal_item == ['No Visa']:
                f.write(doc+'\n')
                continue
            else:
                count += 1
                stock[doc].extend(legal_item)
    f.close()
    pickle.dump(stock, open("./Result_temp/legal_item_cod.xport", "wb"))
    return()


def legalitem_to_tab(picklepath, Export = False, ONLYCOD = True):
    
    stock_item = []
    
    legal = Le.load_pickle_list(picklepath)
    [stock_item.append(item[doc])  for item in legal for doc in item.keys()]
    index = [item[0] for item in stock_item]
    legal = [item[1:] for item in stock_item]
    
    Import = False
    if Export == True:
        LegalDictionary = corpora.Dictionary(legal)  # initialize a Dictionary
        LegalCorpus = [LegalDictionary.doc2bow(item) for item in legal]
        LegalDictionary.save('./Result_temp/legal_Dico.dict')
        corpora.MmCorpus.serialize('./Result_temp/legal_Corpus.mm', LegalCorpus)
    
    elif Import == True:
        LegalDictionary = corpora.Dictionary.load('./Result_temp/legal_Dico.dict')
        LegalCorpus = corpora.MmCorpus('./Result_temp/legal_Corpus.mm')
    
#    print(LegalCorpus[1])
    legal_text = legal
    if ONLYCOD:
        for i in range(len(legal_text)):
            legal_text[i] = [word for word in legal_text[i] if word.startswith("cod_")]
    
    legal_text = [" ".join(sent) for sent in legal]
    vectorizer = CountVectorizer().fit(legal_text)
    mat = vectorizer.transform(legal_text)
    features = vectorizer.get_feature_names()
    print(len(features))
    
    ONLYCOD_TAB = True
    
    if ONLYCOD_TAB:
        mat = mat.toarray()
        mat = pd.DataFrame(mat)
        mat.columns = features
        mat.index = index
        print(mat.shape, "\n", index[:2], "\n", mat.head(2))
    
        #mat.to_csv("./Result_temp/legal.csv")
        mat.to_parquet("./Result_temp/legal_cod.parquet.gzip",compression='gzip')
    

def load_legal_tab(parquetpath):
    
    mat = pd.read_parquet(parquetpath)
    print(mat.tail(2), '\n', mat.shape)
            
    

##############
#    MAIN    #
##############


#Xport_legalitem_visa()
legalitem_to_tab("./Result_temp/legal_item.xport")
#load_legal_tab("./Result_temp/legal.parquet.gzip")