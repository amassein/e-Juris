#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 14 10:01:28 2022

@author: aljoscha
"""

from gensim import corpora, models, similarities, downloader
import numpy as np
from gensim.models.word2vec import Word2Vec
import gensim.downloader as api
import re, nltk
import random
import pandas as pd
import matplotlib.pyplot as plt
import Librairie_ejuris as Le
import plotly.express as px
import pickle
#import plotly.io as pio
#pio.renderers.default='browser'

import plotly.io as pio
pio.renderers.default='svg'


import os
from nltk.corpus import XMLCorpusReader

def run_test_id(nb, rand_nb, nb_doc, ids):
    
    stock_id = 0
    stock_cos = 0
        
    
    for i in range(nb):
        
        rand = Le.rand_consid(rand_nb, nb_doc)
        #print(rand)
        vec_bow = Le.dictionarySOM.doc2bow(rand)
   #    print([dictionary[wd] for wd,_ in vec_bow])

        sims=Le.indexSOM[vec_bow]
        sims= sorted(enumerate(sims), key=lambda item: -item[1])
        
        idx, cos = sims[0]
        
        if rand_nb == 0 and i == 0:
            ids = idx
            if len(vec_bow) <= 20:
                return("void", "void")
            
        if idx == ids:
            stock_id +=1
            
            if i == 0:
                stock_cos += cos
            elif i != 0:
                stock_cos += cos
                stock_cos /= 2
        
    stock_id = stock_id/nb*100
    
    ret = [ids, stock_cos, stock_id, rand_nb, nb_doc, len(Le.corpusALL[nb_doc])]
    return(ret, ids)

def mini_cos(nb_cpSOM):

    nb = 10#int(input("Combien de tentative ? "))
#    nb_doc = 483691#798322#int(input("Quel considérant vouslez-vous tester ? "))
    ids = []
    iteration = 10
    test = []
    stop = Le.find_jurisprudence(nb_cpSOM, 0.45)
    
    simi = Le.find_similariteSOM(nb_cpSOM, 0.2)
    simi = [int(idx) for idx,cos in simi]
    
    for j, sim in enumerate(simi):
        if j % 100 == 0:
            print(j, len(simi)-j, sim)
            rand_nb = 0
            for i in range(iteration):
                ret, ids = run_test_id(nb, rand_nb, sim, ids)
                if ids not in stop:
                    break;
#                elif rand_nb == 0:
#                    rand_nb += 50
#                    continue;
                else:
                    print(ret)
                    test.append(ret)
                    rand_nb += 25
        
    test = pd.DataFrame(test)
    test.columns = ['CP', 'COS', 'Percent', 'Rand_nb', 'Consid', 'Longueur_Consid']
    print(test)
    pickle.dump(test, open('./tab_cos_long.xp', 'wb'))
    
    
def figure_to_DASH(pickle):
    
    test = Le.load_pickle(pickle)

    figrand_perc = px.line(test, x='Rand_nb', y='Percent', color='Consid')
    
    figrand_cos = px.line(test, x='Rand_nb', y='COS', color='Consid')
    
    testgroup = test.groupby('Consid')['Longueur_Consid', 'Percent'].mean()
    figlong = px.scatter(testgroup, x='Longueur_Consid', y='Percent', size='Longueur_Consid', color=testgroup.index, hover_name=testgroup.index)
    
#    fig = px.line_3d(test, x="Rand_nb", y="COS", z="Percent", color='Consid')
#    fig.show()

    group = test.groupby('Consid')['COS','Percent'].mean()
    figscatter = px.scatter(group, x="COS", y='Percent', size='COS', hover_name=group.index)
    
    
    return(test, figrand_perc, figrand_cos, figscatter, figlong)


def mix_cpSOM(nb_SOM1, nb_SOM2):
    
    embed1 = Le.corpusSOM[nb_SOM1]
    embed2 = Le.corpusSOM[nb_SOM2]
        
    tab = [[] for i in range(len(embed1))]
    temp = []
    rand = 0
    scoreSOM2 = 0
    scoreSOM1 = 0
    
    if len(embed1) <= len(embed2):
        lenght = len(embed1)
    else:
        lenght = len(embed2)
    
    for i in range(lenght):
        cp1 = [Le.dictionarySOM[wd] for wd,nb in embed1]
        cp1 = Le.dictionarySOM.doc2bow(cp1)
        sims1 = Le.indexSOM[cp1]
        sims1= sorted(enumerate(sims1), key=lambda item: -item[1])
                
        for doc_pos, doc_score in sims1:
            if doc_pos == nb_SOM2:
                scoreSOM2 = doc_score
            elif doc_pos == nb_SOM1:
                scoreSOM1 = doc_score
            if doc_pos in sims1[0]:
#                print(doc_pos, doc_score)
                pass
        
        tab[i] = [i, scoreSOM2, scoreSOM1, Le.dictionarySOM[embed2[rand][0]], rand, round(i/len(embed1)*100,0)]

        rand = random.randint(0,lenght-1)
        while rand in temp:
            rand = random.randint(0,lenght-1)
        embed1[rand] = embed2[rand]
        temp.append(rand)
        
    
    tab = pd.DataFrame(tab)
    tab.columns = ["id", f"cos SOM_{nb_SOM2}", f"cos SOM_{nb_SOM1}", "mot", "N°Rand", "%"]
#    print(tab)
    mean2 = np.mean(tab[f"cos SOM_{nb_SOM2}"])
    print("MEAN COS_SOM2 : ", mean2)
    mean1 = np.mean(tab[f"cos SOM_{nb_SOM1}"])
    print("MEAN COS_SOM1 : ", mean1)
    return(tab, nb_SOM1, nb_SOM2)


def plot(tab, nb_1, nb_2):
    
    Percent = True
    if Percent:
        tab.index = tab["%"]
        label = "%"
    else:
        label = "Nb"
    #fig = px.line(tab, x=f"cos SOM_{nb_2}", y=f"cos SOM_{nb_1}")
    fig = tab[[f"cos SOM_{nb_2}", f"cos SOM_{nb_1}"]].plot(kind="line",
                                                           labels=dict(index="%", value="COS", variable="CP")
                )
    return(fig)
   
    
####### MAIN

mini_cos(70607)
    