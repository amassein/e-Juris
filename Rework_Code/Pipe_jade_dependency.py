import jade_index_all as INDALL
import jade_index as INDSOM
import jade_mining_somm as MINSOM
import Jade_mining2 as MINALL
import Librairie_ejuris as Le
import os

print("\n : MINING ALL\n")
MINALL.launch_min_all()
print("\n : INDEXING ALL\n")
INDALL.launch_ind_all()
print("\n : MINING SOM\n")
MINSOM.launch_min_som()
print("\n : INDEXING SOM\n")
INDSOM.launch_ind_som()
print("\n : SOM2ALL & ALL2SOM")
Le.ALL2SOM()
Le.SOM2ALL()