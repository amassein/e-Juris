# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 16:26:49 2021

@author: Aljoscha
"""

# -*- coding: utf-8 -*-
# Importing necessary library

import numpy as np
import pandas as pd
import nltk
import re
import os
import codecs
from sklearn import feature_extraction
import pickle

import xml.etree.ElementTree as ET
from nltk.corpus import XMLCorpusReader
from gensim import corpora, models, similarities, downloader
import jade_setup_class
import logging  # Setting up the loggings to monitor gensim
import Librairie_ejuris as Le
logging.basicConfig(format="%(levelname)s - %(asctime)s: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)

stopwords = nltk.corpus.stopwords.words('french')
stopwords.append('considérant')
stopwords.append('qu')

codes = jade_setup_class.Jade_cod()
patt = jade_setup_class.Jade_pattern()
rw_list = jade_setup_class.Jade_rewrite()


def merge_token(match_obj,tk):
    repl=""
    txt=match_obj.group().strip()
    txt=re.sub(r'\s{1,20}'," ",txt)
    #on supprime les citations"
    if tk=="QOT":
        #pour les sommaires on n'enlève pas les citations
        repl=txt
    elif tk=="PCJA":
        #on doit enlever les 2 derniers caractères qui sont du token suivant puis les restituer
        last=txt[len(txt)-2:len(txt)]
        txt=txt[0:len(txt)-2].strip()
        
        txt=re.sub(",","",txt)
        pcja=txt.split(" ")
        for p in pcja:
            repl+="PCJA_"+p.strip()+" "
        repl+=last
        repl=txt
    else:
        repl=tk+"_"+re.sub(" ","_",txt)
        repl=re.sub("'","_",repl)+" "
    return repl
    
def tokenize_entities(codes, patt, rw_list, text):
    #il faut évite les blancs en début pour les regex
    text=text.strip()
    #on reprend certaines expressions ex: CGI pour code général des impôts
    for rw, rw_rep in rw_list.rw_par:
        raw_s = r'{0}'.format(rw)
        text=re.sub(raw_s,rw_rep,text,flags=re.I)
    for cod, cod_tk in codes.cod_par:
        text=re.sub(cod,lambda m: merge_token(m,cod_tk),text,flags=re.I)
    for pat, pat_tk in patt.pat_par:
        raw_s = r'{0}'.format(pat)
        text=re.sub(raw_s,lambda m: merge_token(m,pat_tk),text,flags=re.I)
    return text


def tokenize_only(text):

    text=tokenize_entities(codes, patt, rw_list,text)
    text = text.replace("'"," ")
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
    filtered_tokens_stop = [w for w in filtered_tokens if not w in stopwords]

    
    return filtered_tokens_stop


def get_considerant_part(doc):
    
    consid_stock = []
    consid_id = []
    nb_consid = 1
    xmldoc = Le.corpus.xml(doc)
    
    for elt in xmldoc.iter('CONTENU'):
        if elt.text is not None:
            read, doc = Le.format_consid(doc)
            result=re.search('Considérant(.*?)DECIDE',read,re.MULTILINE)
            if result is not None: 
                motif="Considérant "+result.group(1)
                considerantraw = motif.split('#')
                considerantraw = [consid for consid in considerantraw if consid != '' and consid != ' ']

                for consid in considerantraw:
                    if len(consid.strip())>0:
                        consid_stock.append(consid.lower())
                        consid_id.append(doc+"_"+str(nb_consid))
                        nb_consid += 1

    return(consid_stock, consid_id)

corpusdir = "../jade_all/" # Directory of corpus.

def read_corpus():
    
    corpusdir = "../jade_all/"
    corpus = XMLCorpusReader(corpusdir, '.*')
    doc_list = corpus.fileids()
    
    i=0

    cases=[]
    cases_id=[]
    for doc in doc_list:
        if doc.endswith('.xml'):
            i+=1
            if i>100000000:
                break
            if i%1000==0:
                print(f"\r{i}/{len(doc_list)}", end="")
            consid, consid_id=get_considerant_part(doc)
            for c, csd_id in zip(consid, consid_id):
                csd = tokenize_only(c)
                cases.append(csd)
                cases_id.append(csd_id)
            
    return cases, cases_id

def launch_min_all():
    
    cases, cases_id=read_corpus()
    pickle.dump(cases_id, open("./Jade_dependency/jadecorpus_all2.id", 'wb'))
    pickle.dump(cases, open("./Jade_dependency/jadecorpus_all2.cs", 'wb'))
    cp = open("./Jade_dependency/jadecorpus_all2.cs", "rb")
    cases=pickle.load(cp)
    cp.close()
    dictionary = corpora.Dictionary(cases,  prune_at=4000000)
    dictionary.save('./Jade_dependency/jade_all2.dict')  # store the dictionary, for future reference
    dictionary=corpora.Dictionary.load('./Jade_dependency/jade_all2.dict')

    for c in range(len(cases)):
        cpbow=dictionary.doc2bow(cases[c])           
        cases[c]=cpbow
        if c%1000==0:
               print(f"\r{c}", end="")

    corpora.MmCorpus.serialize('Jade_dependency/jadecorpus_all2.mm', cases)  
