# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 16:26:49 2021

@author: Marc Clément
"""
from gensim import corpora, models, similarities, downloader
import numpy as np
from gensim.models.word2vec import Word2Vec
from multiprocessing import cpu_count
import gensim.downloader as api
import jade_setup_class
import re, nltk
import random


import os
from nltk.corpus import XMLCorpusReader


stopwords = nltk.corpus.stopwords.words('french')
stopwords.append('considérant')
stopwords.append('qu')
#stopwords.append('cette')
codes = jade_setup_class.Jade_cod()
patt = jade_setup_class.Jade_pattern()


def merge_token(match_obj,tk):
    #on supprime les citations
    if tk!="QOT":
        repl=tk+"_"+re.sub(" ","_",match_obj.group().strip())
        repl=re.sub("'","_",repl)+" "
    else:
        repl=" "
    return repl
    
def tokenize_entities(text):
    for cod, cod_tk in codes.cod_par:
        text=re.sub(cod,lambda m: merge_token(m,cod_tk),text,flags=re.I)
    for pat, pat_tk in patt.pat_par:
        text=re.sub(pat,lambda m: merge_token(m,pat_tk),text,flags=re.I)
    return text


def tokenize_only(text):

    text=tokenize_entities(text)
    text = text.replace("'"," ")
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
    filtered_tokens_stop = [w for w in filtered_tokens if not w in stopwords]

    
    return filtered_tokens_stop

def ex():
    dictionary = corpora.Dictionary.load('./Jade_dependency/jade2_somCE.dict')

    corpus = corpora.MmCorpus('./Jade_dependency/jadecorpus_somCE2.mm')

    #test1 = "Considérant que ces dispositions énoncent, s'agissant des irrégularités commises lors de la consultation d'un organisme, une règle qui s'inspire du principe selon lequel, si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ; que l'application de ce principe n'est pas exclue en cas d'omission d'une procédure obligatoire, à condition qu'une telle omission n'ait pas pour effet d'affecter la compétence de l'auteur de l'acte ;"
    #test =  "Il résulte de ces dispositions que la fonction d' assesseur de bureau de vote qui , en vertu de l' article R. 44 du code électoral , peut être confiée par le maire à des membres du conseil municipal , compte parmi les fonctions qui leur sont dévolues par les lois au sens de l' article L. 2121 - 5 du code général des collectivités territoriales . Un membre du conseil municipal ne peut se soustraire à cette obligation que s' il est en mesure , sous le contrôle du juge administratif , de présenter une excuse valable."

    #test=tokenize_only(test1)

    #for doc in corpus:
    #    print([[dictionary[id],freq] for id, freq in doc])
    #    print(doc)

    #tfidf = models.TfidfModel(corpus, smartirs='ntc')

    # Show the TF-IDF weights
    #for doc in tfidf[corpus]:
    #    print([[dictionary[id], np.around(freq, decimals=2)] for id, freq in doc])



    #vec_bow = dictionary.doc2bow(test)
    #print([dictionary[wd] for wd,_ in vec_bow])

    #print(vec_lsi)
    index= similarities.Similarity('Jade_index_all/jade_index_som2', corpus, num_features=len(dictionary))
    index.save("Jade_index_all/jade_index_som2.idx")
    #index=similarities.Similarity.load("jade_index_som2.idx")
    #sims=index[vec_bow]
    #sims= sorted(enumerate(sims), key=lambda item: -item[1])
    #print(sims)
    #for doc_position, doc_score in sims[:5]:
    #    print(doc_score)
    #    consid = corpus[doc_position]
    #    print(consid)
    #    print([dictionary[wd] for wd,_ in consid if dictionary[wd] in test])

    
def launch_ind_som():
    
    dictionary = corpora.Dictionary.load('./Jade_dependency/jade2_somCE.dict')
    corpus = corpora.MmCorpus('./Jade_dependency/jadecorpus_somCE2.mm')    
    
    index= similarities.Similarity('Jade_index_all/jade_index_som2', corpus, num_features=len(dictionary))
    index.save("Jade_index_all/jade_index_som2.idx")