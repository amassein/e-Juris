# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 16:26:49 2021

@author: Marc Clément
"""

# -*- coding: utf-8 -*-
# Importing necessary library

import numpy as np
import pandas as pd
import nltk
import re
import os
import codecs
from sklearn import feature_extraction

from nltk.corpus import XMLCorpusReader
from gensim import corpora, models, similarities, downloader
import pickle


import shutil
import jade_setup_class

codes = jade_setup_class.Jade_cod()
patt = jade_setup_class.Jade_pattern()
rw_list = jade_setup_class.Jade_rewrite()

corpusdir = "../jade_all/" # Directory of corpus.
jade_lebon_dir = "../jade_lebon/" # Directory of corpus.
corpus = XMLCorpusReader(corpusdir, '.*')

stopwords = nltk.corpus.stopwords.words('french')
stopwords.append('considérant')
stopwords.append('qu')
#stopwords.append('cette')



def merge_token(match_obj,tk):
    repl=""
    txt=match_obj.group().strip()
    txt=re.sub(r'\s{1,20}'," ",txt)
    #on supprime les citations"
    if tk=="QOT":
        #pour les sommaires on n'enlève pas les citations
        repl=txt
    elif tk=="PCJA":
        #on doit enlever les 2 derniers caractères qui sont du token suivant puis les restituer
        last=txt[len(txt)-2:len(txt)]
        txt=txt[0:len(txt)-2].strip()
        
        txt=re.sub(",","",txt)
        pcja=txt.split(" ")
        for p in pcja:
            repl+="PCJA_"+p.strip()+" "
        repl+=last
        repl=txt
    else:
        repl=tk+"_"+re.sub(" ","_",txt)
        repl=re.sub("'","_",repl)+" "
    return repl
    
def tokenize_entities(codes, patt, rw_list, text):
    #il faut évite les blancs en début pour les regex
    text=text.strip()
    #on reprend certaines expressions ex: CGI pour code général des impôts
    for rw, rw_rep in rw_list.rw_par:
        raw_s = r'{0}'.format(rw)
        text=re.sub(raw_s,rw_rep,text,flags=re.I)
    for cod, cod_tk in codes.cod_par:
        text=re.sub(cod,lambda m: merge_token(m,cod_tk),text,flags=re.I)
    for pat, pat_tk in patt.pat_par:
        raw_s = r'{0}'.format(pat)
        text=re.sub(raw_s,lambda m: merge_token(m,pat_tk),text,flags=re.I)
    return text


def tokenize_only(text):

    text=tokenize_entities(codes, patt, rw_list,text)
    text = text.replace("'"," ")
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
    filtered_tokens_stop = [w for w in filtered_tokens if not w in stopwords]

    
    return filtered_tokens_stop


def get_sommaire(textxml,doc):
    sommaire = []
    idx=[]
    lebon ="C"
    for elt in textxml.iter('PUBLI_RECUEIL'):
        lebon = elt.text
    nbsom=0
    if (lebon == "A") or (lebon == "B"):
        for elt2 in textxml.iter('ANA'):
            nbsom+=1
            if elt2.text is not None:
                sommaire.append(elt2.text.lower()) #Ajout .lower()
                idx.append(doc+"_"+str(nbsom))
    return sommaire, idx


def launch_min_som():

    i=0
    doc_list = corpus.fileids()
    #totalvocab_stemmed = []
    #totalvocab_tokenized = []
    cases_som=[]
    cases_som_id=[]
    for doc in doc_list:
        if doc.endswith('.xml'):
            i+=1
            if i%1000==0:
                print(i)
            xmldoc =  corpus.xml(doc)

            for juri in xmldoc.iter('JURIDICTION'):

                if (juri.text[0:7] == "Conseil"):
                    print(i)
                    case_txt, case_id=get_sommaire(xmldoc,doc)
                    if len(case_txt)>0:
                        #shutil.copy(corpusdir+doc,jade_lebon_dir+doc)
                        for c, csd_id in zip(case_txt, case_id):
                            cases_som.append(tokenize_only(c))
                            cases_som_id.append(csd_id)
        else:
            print('Incorrect File')

    #print('I went there')

    pickle.dump(cases_som_id, open("./Jade_dependency/jadecorpus2_som.id", 'wb'))
    pickle.dump(cases_som, open("./Jade_dependency/jadecorpus2_som.cs", 'wb'))
    dictionary = corpora.Dictionary(cases_som)
    dictionary.save('Jade_dependency/jade2_somCE.dict')  # store the dictionary, for future reference
    corpusbow = [dictionary.doc2bow(c) for c in cases_som]
    corpora.MmCorpus.serialize('Jade_dependency/jadecorpus_somCE2.mm', corpusbow)