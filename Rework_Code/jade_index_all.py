# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 16:26:49 2021

@author: Marc Clément
"""
from gensim import corpora, models, similarities, downloader
import numpy as np
from gensim.models.word2vec import Word2Vec
#from multiprocessing import cpu_count
import gensim.downloader as api
#import logging  # Setting up the loggings to monitor gensim
#logging.basicConfig(format="%(levelname)s - %(asctime)s: %(message)s", datefmt= '%H:%M:%S', level=logging.INFO)

def ex():

    dictionary = corpora.Dictionary.load('./Jade_dependency/jade_all2.dict')

    corpus = corpora.MmCorpus('./Jade_dependency/jadecorpus_all2.mm')

    #test = "Si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de cette décision ou s'il a privé les intéressés d'une garantie. Il en résulte qu'une décision créatrice de droits, entachée d'un vice qui n'a pas été susceptible d'exercer une influence sur le sens de cette décision et qui n'a pas privé les intéressés d'une garantie, ne peut être tenue pour illégale et ne peut, en conséquence, être retirée ou abrogée par l'administration de sa propre initiative ou sur la demande d'un tiers, même dans le délai de quatre mois suivant la prise de cette décision"
    #test = test.replace("'"," ")

    #for doc in corpus:
    #    print([[dictionary[id],freq] for id, freq in doc])
    #    print(doc)

    #tfidf = models.TfidfModel(corpus, smartirs='ntc')

    # Show the TF-IDF weights
    #for doc in tfidf[corpus]:
    #    print([[dictionary[id], np.around(freq, decimals=2)] for id, freq in doc])


    #vec_bow = dictionary.doc2bow(test.lower().split())
    #print(vec_bow)
    #print([dictionary[wd] for wd,_ in vec_bow])


    index = similarities.Similarity('./Jade_index_all/jade_index_all2', corpus, num_features=len(dictionary))
    index.save("./Jade_index_all/jade_index_all2.idx")
    #index=similarities.Similarity.load("./jade_index_all2.idx")
    #sims=index[vec_bow]
    #sims = sorted(enumerate(sims), key=lambda item: -item[1])

    #temp = np.mean([score for _,score in sims[:30]])

    #for doc_position, doc_score in sims:
    #    if doc_score > temp:
    #        print(doc_score)
    #        consid = corpus[doc_position]
        #print(consid)
    #        print(doc_position)
    #        print([dictionary[wd] for wd,_ in consid if dictionary[wd] in test])

def launch_ind_all():
    
    dictionary = corpora.Dictionary.load('./Jade_dependency/jade_all2.dict')
    corpus = corpora.MmCorpus('./Jade_dependency/jadecorpus_all2.mm')

    index = similarities.Similarity('./Jade_index_all/jade_index_all2', corpus, num_features=len(dictionary))
    index.save("./Jade_index_all/jade_index_all2.idx")