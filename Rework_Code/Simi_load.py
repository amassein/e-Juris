import Librairie_ejuris as Le
import pandas as pd
import numpy as np
import pickle
import os
import pyarrow as pa
import pyarrow.dataset as ds
import pyarrow.compute as pc

def export_tab(data):
    for i, stack in enumerate(data):
        tab = pd.DataFrame(stack)
        df=df.append(tab,ignore_index=True)
        print(f"\r{i} / {count}.", end="")

##### EXPORT DE TAB VERS CSV

def read_export_to_tab_simi(picklepath):
    
    df = pd.DataFrame()
    data = []
    count = 0
    
    with open(picklepath, 'rb') as picklefile:
        try:
            while True:
                data = pickle.load(picklefile)
                tab = pd.DataFrame(data)
                df=df.append(tab,ignore_index=True)
                count += 1
                print(f"\r{count}. lenght : {len(df)}", end="")
                if len(df) >= 1500000:
                    df.columns = ["NB_Consid", "COS", "NB_CP", "XMLFILE"] #
                    print(f"\r refresh", end="")
                    df.to_parquet(f"./Result_temp/export_tab_without_juris/tab_{count}_without_juris.parquet.gzip", compression='gzip')
                    df = pd.DataFrame()
                    
        except EOFError:
            df.columns = ["NB_Consid", "COS", "NB_CP", "XMLFILE"] #
            df.to_parquet(f"./Result_temp/export_tab_without_juris/tab_{count}_without_juris.parquet.gzip", compression='gzip')
            df = pd.DataFrame()
            print("\n")
            pass
    
    picklefile.close()
    
    return(df)

def read_export_to_tab_simi2(picklepath):
    
    df = pd.DataFrame()
    KG = True

    data = []
    count = 0
    
    count = 0
    with open(picklepath, 'rb') as picklefile:
        try:
            while True:
                data = pickle.load(picklefile)
                tab = pd.DataFrame(data)
                df=df.append(tab,ignore_index=True)
                count += 1
                print(f"\r{count}. lenght : {len(df)}", end="")
                if len(df) >= 1500000:
                    df.columns = ["NB_Consid", "COS", "NB_CP", "Proxi_Juris","SOM_FOUND", "XMLFILE"] #
                    print(f"\r refresh", end="")
                    df.to_parquet(f"./Result_temp/export_tab/tab_{count}.parquet.gzip", compression='gzip')
                    df = pd.DataFrame()
                    
        except EOFError:
            df.columns = ["NB_Consid", "COS", "NB_CP", "Proxi_Juris","SOM_FOUND", "XMLFILE"] #
            df.to_parquet(f"./Result_temp/export_tab/tab_{count}.parquet.gzip", compression='gzip')
            df = pd.DataFrame()
            print("\n")
            pass
    
    picklefile.close()
            
    #df.to_csv(f"./Result_temp/tab_para{len(data)}.csv")
    #df.to_parquet(f"./Result_temp/export_tab/tab_para{count}.parquet.gzip", compression='gzip')
    return(df)

def nb_item(picklepath):
    
    count = 0
    
    with open(picklepath, 'rb') as picklefile:
        try:
            while True:
                pickle.load(picklefile)
                count+=1
                print(f"\r{count}", end="")
        except EOFError:
            pass
    picklefile.close()
    print("\n",count)

def load_tabs():
    
    count = 0
    docfile = "./Result_temp/export_tab3"
    dataset = ds.dataset(docfile, format="parquet")
    print(dataset.files)
    print(dataset.schema.to_string(show_field_metadata=False))
    for batch in dataset.to_batches(columns=["NB_CP"]):
        count += pc.count(batch.column("NB_CP")).as_py()
    print(count)

def export_some_tabs():
    
    docfile = "./Result_temp/export_tab2"
    dataset = ds.dataset(docfile, format="parquet")
    df = dataset.to_table()
    dataset = []
#    for table_chunk in dataset.to_batches():
#        tab = pc.value_counts(table_chunk["NB_CP"])
    df = df.to_pandas()
    df3 = df.groupby("NB_CP").count()
    print(df3.shape)
    df3.to_parquet("./Result_temp/Result_tab/big_tab_group_without_juris.parquet.gzip", compression="gzip")

#    df2 = df.iloc[:,:3]
#    df2 = df2.append(df.XMLFILE)
#    print(df2.head())
#    df2.to_parquet("./Result_temp/Result_tab/big_tab_light.parquet.gzip")
#    print(df)

def parquet_to_df():
    
    docfile = "./Result_temp/export_tab3"
    dataset = ds.dataset(docfile, format="parquet")
    df = dataset.to_table().to_pandas()
    print(df.head())
    
    
######### MAIN

read_export_to_tab_simi("./Result_temp/futur_tab_without_juris2.xport")
#read_export_to_tab_simi2("./Result_temp/futur_tab.xport")
#load_tabs()
#nb_item("./Result_temp/futur_tab.xport")
export_some_tabs()
#parquet_to_df()