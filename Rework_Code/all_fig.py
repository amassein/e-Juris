#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 11:18:43 2022

@author: Aljoscha
"""

import dash
from dash import dcc
from dash import html
from dash import dash_table
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
import re
import numpy as np
import pandas as pd
import string
import re
import nltk
import numpy as np
import gensim
import matplotlib.pyplot as plt

pd.options.plotting.backend = 'plotly'

meta = pd.read_parquet("./Result_temp/metadata_recod.parquet.gzip")
meta.DATE_DEC = meta.DATE_DEC.astype('int')
meta = meta.loc[(meta.DATE_DEC >= 1965) & (meta.DATE_DEC < 2999)]
del meta['TITRE'], meta['NUMERO']
df = pd.read_parquet("./Result_temp/tab_without_juris.parquet.gzip")
merged = pd.read_parquet("./Result_temp/Result_tab/merge_tab.parquet.gzip")
merged.DATE_DEC = merged.DATE_DEC.astype('int')
merged = merged.loc[(merged.DATE_DEC >= 1965) & (merged.DATE_DEC < 2999)]
del merged['TITRE'], merged['NUMERO']
cp = pd.read_parquet("./Result_temp/Result_tab/merge_tab_SOM.parquet.gzip")
pcja = pd.read_parquet("./code_pcja.parquet.gzip")
pcja_descri = pd.read_parquet("./Result_temp/pcja_with_descri_1_level.parquet.gzip")
legal = pd.read_parquet("./Result_temp/legal_cod.parquet.gzip")

group_codes= pd.merge(df, legal, left_on='XMLFILE', right_on=legal.index)
del group_codes['COS'], group_codes['XMLFILE'], group_codes['NB_Consid']
group_codes = group_codes.groupby('NB_CP').sum()
del group_codes['cod_code_de_justice_administrative']

# Nombre de considérant par Jurisprudence

temp = merged.copy()
temp.NB_CP = temp.NB_CP.astype('str')
df_count = temp.NB_CP.value_counts()[:50]
fig_count_CP = df_count.plot.bar()

# Nombre de jurisprudence par code pcja
stock = []
for i, item in enumerate(pcja_descri.Description_x):
    stock.append(str(pcja_descri.loc[i].pcja)+'-'+str(item))
pcja_descri['Description'] = stock
group_pcja = pcja_descri.groupby('1st_level').count().sort_values(by="CP", ascending=True)

fig_pcja1 = group_pcja.CP.plot(kind="bar", title="Distribution des jurisprudence par code PCJA Premier niveau")

group_pcja2 = pd.merge(df, pcja_descri, left_on="NB_CP", right_on="CP").groupby("1st_level").count().sort_values("NB_Consid",ascending=True)
fig_pcja2 = group_pcja2.CP.plot(kind="bar", title="Distribution des contentieux pondérée sur le nombre de considérant par jurisprudence - Premier niveau")

mergure = pd.merge(merged, pcja_descri[['CP', '1st_level', 'PCJA_0', 'Description', 'pcja']], left_on="NB_CP", right_on="CP")
sort = pd.crosstab(mergure.DATE_DEC, mergure['1st_level']).sum().sort_values(ascending=False)[:10].index
group = pd.crosstab(mergure.DATE_DEC, mergure['1st_level'], normalize='index')
group = group[sort]
fig_pcja3 = group.plot(kind="line", title="% des identifiants PCJA associés à un considérant par année")

# Codes

def get_cod_by_consid(nb_consid):
    
    temp2 = group_codes.loc[group_codes.index == nb_consid]
    
    for col in group_codes.columns:
        tot = group_codes[col].sum()
        group_codes[col] = group_codes[col].apply(lambda x: x / tot)*100
        
    #return(group)
    #fig, axes = plt.subplots(1, 2, figsize=(15, 8))
    plt.figure(figsize=(10, 6))
    temp = group_codes.loc[group_codes.index == nb_consid]
    condition = temp >= np.mean(np.mean(temp))
    temp = temp[condition]
    temp.index = temp.index.astype('str')
    temp = temp.dropna(axis=1)
    temp = temp.transpose().sort_values(by=str(nb_consid),ascending=True)
    fig = temp.plot.barh()
    
    plt.figure(figsize=(10,6))
    condition = temp2 >= np.mean(np.mean(temp2))
    temp2 = temp2[condition]
    temp2.index = temp2.index.astype('str')
    temp2 = temp2.dropna(axis=1)
    temp2 = temp2.transpose().sort_values(by=str(nb_consid),ascending=True)
    fig2 = temp2.plot.barh()

    widget = html.Div(children=[
                             dcc.Graph(
                                 id='graph_cp',
                                 figure=fig,
                                 style={'display': 'flex'}
                                 ),
                             html.Br(),
                             dcc.Graph(
                                 id='graph_cp2',
                                 figure=fig2,
                                 style={'display': 'flex'}
                                 )],style={'display': 'flex', 'flex-direction': 'row'})
    return(widget)