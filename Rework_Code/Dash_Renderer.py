#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 09:05:03 2022

@author: Aljoscha
"""

####### DASH RENDERER

import dash
from dash import dcc
from dash import html
from dash import dash_table
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
from skimage import io
from dash.dependencies import Input, Output
import re
import numpy as np
import pandas as pd
import string
import re
import nltk
import numpy as np
import gensim
import matplotlib.pyplot as plt
import Librairie_ejuris as Le
import Opti_cos_CP as opti
import Find_juris as Fj

pd.options.plotting.backend = "plotly"


def simple_graph(fig):
    return dcc.Graph(
        id='example-graph-7',
        figure=fig
        ),


def main():
    
    ### STYLESHEET
    
    app = dash.Dash(__name__, 
                    title="E-Juris_projet", 
                    external_stylesheets=[dbc.themes.SPACELAB])


    ### VAR & GRAPH

    df1, figrand_perc, figrand_cos, __, _ = opti.figure_to_DASH('./tab_cos.xp')
    df1_long, figrand_perc_long, figrand_cos_long, fig_test_long, figlong = opti.figure_to_DASH("./tab_cos_long.xp") 
    
    tab, nb1, nb2 = opti.mix_cpSOM(3995, 43)
    fig_mix_cp = opti.plot(tab, nb1, nb2)
    
    tab = Le.to_tab("./Result_temp/futur_tab.xport")
    df = tab.iloc[:,0:14]
    df.columns = ["Consid", "Cos", "CP", "proxi", 'proxi2' ,"xmlfile", "titre", "year", "JURIDICTION", "CODE", "_", "Chambres", "TYPE_REC", "Classement"]
    df2 = df.drop(['proxi', 'proxi2'], axis=1)
    test = df[['CP', 'year', 'JURIDICTION']]
    group = test.groupby(['year'])['JURIDICTION'].value_counts().unstack()
    fig_tab = group.plot()
    
    ### COMPONENTS
    
    
    ### LAYOUT
    
    app.layout = dbc.Container([
      html.Div(children=[html.H1("Dashboard"),
                         html.Br(),
                         html.H2('Travail sur le cosinus pour le repérage des similarités'),
                         html.P('Suite au premier travaux réalisé par Marc Clément, nous avons poursuivi les travaux que permettait la '
                                "reconnaissance de considérant par rapport aux considérants de principe que l'on a retrouvé dans les sommaires "
                                "des arrêts du Conseil d'Etat."),
                         html.P('Nous avons donc dans un premier temps testé les variations du cosinus pour voir dans quelle mesure nos bag-of-words '
                                "sont efficaces. Pour ce faire, nous avons ajouté des mots au considérant qui 'match' avec un considérant de principe "
                                "pour voir si la variation du cosinus influe sur la reconnaissance du considérant de principe"),
                         html.P("Pour cet exemple, nous avons pris des considérants aléatoirement (modulo 50), avec un cos > 0.2 à l'origine. "
                                "Notre programme sélectionne ensuite les considérants affiliés au CP choisi, ainsi que les CP proches (find_jurisprudence())."
                                "Enfin, pour chaque considérant trouvé, nous ajoutons des mots et vérifions que le considérant de principe retrouvé en premier"
                                "correspond au 'paquet' de CP, c'est-à-dire à une 'jurisprudence'."),
                         html.Br(),
                         html.H3('Evolution du cosinus et du pourcentage avec ajout de mot alétoire'),
                         html.P("Ces premiers graphiques nous montre l'évolution du pourcentage de reconnaissance du considérant de principe et du cosinus"
                                "en fonction de l'ajout de mot."),
                         
                         html.Div(children=[
                             dcc.Graph(
                                 id='example-graph-2',
                                 figure=figrand_perc,
                                 style={'display': 'flex'}
                                 ),
        
#            html.H3('Rand_nb/COS'),
#            html.P("Ce graphique, à lire avec le précédent, nous montre l'évolution du cosinus avec l'ajout de mot aléatoire."),
                            dcc.Graph(
                                id='example-graph-1',
                                figure=figrand_cos,
                                style={'display': 'flex'})
                            ], style={'display': 'flex', 'flex-direction': 'row'}),
                         
                         html.Label('Sélectionner le considérant'),
                         dcc.Dropdown(options=df1['Consid'].unique(), value=df1['Consid'].loc[0], id='input_val'),
                         html.Div(id='output_consid'),
                         
                         html.Br(),
                         html.Div(children=[
                             html.H3('Test (modulo 2 = Long)'),
                             dcc.Graph(
                                 id='example-graph-3',
                                 figure=fig_test_long,
                                 #style={'display': 'flex'}
                                 ),                            
#                             html.Br(),
#                             html.H3('Test_longueur'),
                             dcc.Graph(
                                 id='graph-8',
                                 figure=figlong,
                                 #style={'display': 'flex'}
                                 )
                             ], style={'display': 'inline_block', 'flex-direction': 'row'}),
                         
                         html.Label('Entrez le considérant  : '),
                         dcc.Input(value=None, id='consid_text', type='text'),
                         html.Div(id='output_text'),
                         
                         html.Br(),
                         html.P('Ces deux graphiques nous montrent que les considérants les plus ressemblants au CP '
                                "sélectionné sont moins sujets à varier avec l'ajout de mot, ce qui est moins le cas "
                                " avec les considérants qui sont les plus courts"),
                         html.P("Dans un second temps, nous avons cherché à voir comment fonctionnait la reconnaissance par cosinus en 'mixant' "
                                "des considérant de principe entre eux, et observer comment évolue le cosinus et donc la similarité"),
                         
                         html.Br(),
                         html.H3('Test_mix'),
                         dcc.Graph(
                             id='example-graph-4',
                             figure=fig_mix_cp
                             ),
                         
                         html.P("En se basant sur ces premières observations, nous remarquons que nos programmes de reconnaissance des similarités "
                                "semblent fonctionner assez significativement. Cependant, sans avoir de corpus annoté par 'jurisprudence', "
                                "il est difficile d'être sûr des résultats. De manière générale, nous travaillons sur des considérants qui ont "
                                "un cosinus > 0.2, afin d'écarter les individus les plus susceptibles de ne pas correspondre à notre considérant de principe en entrée."),
                         html.P("Que nous permettent donc ces observations ? "),
                         html.P("D'abord, de sélectionner les considérants qui ressemblent à des considérants de principe, ce qu'on peut nommer "
                                "'jurisprudence', dans la mesure où le texte est repris plus ou moins de manière identique. "
                                "A partir de ces sous-ensemble par jurisprudence, nous pouvons chercher les métadonnées présentes dans les fichiers "
                                "XML du corpus, et créer un tableau rassemblant les considérants, leur métadonnées, et le considérant de principe "
                                "(la jurisprudence) auquel ils sont affiliés"),
                         
                         html.Br(),
                         html.H2("Construction d'un tableau pour chaque considérant de principe"),
                         
                         html.Br(),
                         html.Div(
                             children=[html.H2("Tableau des considérants"),
                                       dash_table.DataTable(
                                           df2.to_dict('records'), 
                                           columns=[{"name": i, "id": i} for i in df2.columns],
                                           page_size=15,
                                           style_table={'overflowX': 'auto'},
                                           ),
                                       html.Br(),
                                       dcc.Graph(
                                           id='example-graph-7',
                                           figure=fig_tab,
                                           )]),
                         
                         html.Br(),
                         html.H2("Autres pistes avancées"),
                         html.P("Nous avons également pu avancer sur plusieurs pistes"),
                         dcc.Markdown(''' 
                         * Nous avons pu récupérer les visas de chaque décision de justice, ainsi que les motifs par blocs,
                         avec leur titre, et les considérants qui composent chacun des motifs.
                         * Nous avons pu récupérer les dernières phrases de chacun de ces blocs, afin de retrouver les formules
                         qui sont les mêmes. ex : "Le moyen est dès lors entaché d'illégalité et doit par conséquent être rejeté/Le moyen doit être rejeté"
                         * Cependant, nous n'avons pas obtenu de bons résultats : "Il y a lieu/Il n'y a pas lieu'
                         * Utiliser des embeddings dans ce cas ? -> Se tourner vers Doc2Vec/CamemBERT/JuriBERT.
                         '''),
                         
                         html.Br(),
                         html.H2("Pistes à explorer"),
                         dcc.Markdown('''
                         Plusieurs pistes sont à envisager
                         * D'abord on peut continuer à tester le cosinus, en particulier pour voir quel mots caractérise le plus 
                         un considérant de principe.
                         * On peut ensuite travailler avec les visas, et les codes PCJA pour essayer d'affilier les considérants
                         de principe à un contentieux, et les ajouter au tableau que nous avons précédemment créer.
                         * Transformer et analyser le tableau pour en tirer des savoirs sur l'évolution de jurisprudence dans le temps
                         * Travailler sur l'ensemble des décisions de justice, et non pas sur un corpus réduit (2017-2022).
                         '''),
                         
                         html.Br(),
                         dcc.Markdown('''
                         # Ensemble des fonctions créées jusqu'ici         
                         ```python
                         
                         def rand_consid(level, nb_doc):         ### Ajoute de manière aléatoire un level de mots à un considérant (ALL) nb_doc
                         
                         
                         def load_pickle(pickle_id):             ### Charger un fichier pickle
                         
                         
                         def get_xml_ref(xml_id):                ### Obtenir le nom du fichier d'origine (supprime "_nb")
                                                                ### Retourne le nom doc, et "nb", identifiant du consid
                                                        
                         def get_titre(textxml, doc):          ### Extrait le titre d'une décision de justice


                         def read_considerant(textxml, doc, idx): ### Extrait le texte d'un considérant


                         #### CONSIDERANT - SOM

                         def load_best_scoreSOM():       ### Charge en mémoire les considérant (ALL)
                                                         ### qui sont similaires à chaque CP (SOM)


                         def find_best_score_consid_SOM(cos):  #### Cherche et enregistre les similarités (>= cos)
                                                              ####  avec les considérants (ALL) pour chaque CP (SOM)


                         def find_similariteSOM(nb_SOM, cos):   ### Cherche les considérants qui sont similaires (cos >= 0.2) à un CP nb_SOM


                         def get_nb_consid(nb_SOM, cos):     ### Donne le nomre de considérant similaire par rapport au cosinus (cos)
                                                            ### et affiche tout les consiédrants similaires au CP (nb_SOM)
 

                         def find_similitudesSOM(nb_SOM, cos):   ### Cherche les considérants de principe qui sont ressemble (cos >= 0.2) à un CP nb_SOM
                

                         def find_jurisprudence(nb_SOM):      ### Cherche les CP les plus proches du CP nb_SOM


                         def get_ref_considSOM(nb_consid):    ### Renvoit les différentes informations d'un CP nb_SOM (id_xml, id_consid, text)


                         #### CONSIDERANT - ALL

                         def find_similariteALL(nb_ALL, cos):   ### Cherche les CP qui sont similaires (cos) à un considérant nb_ALL


                         def format_considALL(nb_consid):    ### Formatte les considérants d'une décision de justice
                

                         def get_ref_considALL(nb_consid):     ### Renvoi les différentes informations d'un considérant nb_consid (id_xml, id_consid, text)

                
                         #### VISAS

                         def get_visa_from_consid(nb_consid):     ### Renvoi les visas d'un considérant nb_consid
            
            
                         def format_visa(xmlfile):              ### Fromatte les visas d'une décision de justice


                         def get_visa_from_file(xmlfile):       ### Renvoi les visas d'un fichier xml


                         def process_visa(procedure, elt, xmlfile): ### Découpe en partie les visas en fonction de s'il s'agit d'un visa du CE ou des CAA
                                                                   ### Fonctionne avec get_sub_visa

                         def get_sub_visa(xmlfile, print=True):    ### Renvoi les visas, découpé par partir d'un fichier xml
                
                                                     
                         def print_visa(visa, visa_id):        ### Affiche les différentes parties des visas
                                                              ### Fonctionne avec get_sub_visa


                         #### METADONNEES

                         def format_date(date, year = True, month = False):  ### Formatte la date d'une décision de justice (récupère uniquement l'année)


                         def get_metadata(xmlfile):            ### Extrait les métadonnées d'une décision de justice


                         def get_date_ante(visa, xmlfile):     ### Récupère les dates présente dans les visas


                         def get_tokenize_juri_items(visa):    ### Récupère les références légales dans les visas


                         #### TABLEAU

                         def corpus_to_db():

                
                         def to_tab(picklefile):

                
                         def mining_visa(export=True):
                             ```''')
                             ])
                                      ])
    

    ### CALLBACKS
                     
    @app.callback(
        Output(component_id='output_consid', component_property='children'),
        Input('input_val', 'value'),
        )
    
    def output_consid(value):
        stock = Le.get_ref_considALL(value)
        return(stock[2])

    @app.callback(
        Output(component_id='output_text', component_property='children'),
        Input('consid_text', 'value')
        )
    
    def output_text(nb_consid):
        ret = []
        if nb_consid == None:
            return('')
        else:
            tokens = nltk.tokenize.word_tokenize(nb_consid)
            for token in tokens:
                if len(token) > 6:
                    stock = "Cela ne correspond pas à un identifiant d'un considérant"
                    ret.append(html.P(stock))
                else:
                    stock = Le.get_ref_considALL(int(token))
                    ret.append(html.P(stock[2]))
            #ret = ''.join(ret)
            return(ret)

    ### LAUNCH DASH SERVER

    if __name__ == '__main__':
        app.run_server(debug=True)
        
main()