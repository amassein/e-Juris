# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
# import the wget module
import wget
from bs4 import BeautifulSoup
import requests
import os
import gzip
import shutil
url= "https://echanges.dila.gouv.fr/OPENDATA/JADE/"
local_dir="../jade_gz/"

r = requests.get(url)

soup = BeautifulSoup(r.text, features="html.parser")
txt = soup.get_text()
list_files_raw = txt.split()
list_files=[]
for w in list_files_raw:
    if w.startswith('J'):
        list_files.append(w)
print(list_files)

list_files_local=os.listdir(local_dir)
print(local_dir)

for f in list_files:
    already_in=False
    for fl in list_files_local:
     if f==fl:
        already_in=True
    if not already_in:
        wget.download(url+f,local_dir+f)


