#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 11 09:24:34 2022

@author: aljoscha
"""
#from numba import jit, cuda
from gensim import corpora, models, similarities, downloader
import numpy as np
from gensim.models.word2vec import Word2Vec
import gensim.downloader as api
import re, nltk
import random
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import xml.etree.ElementTree as ET
import jade_setup_class
import time
import os
from nltk.corpus import XMLCorpusReader


##### FUNC

def load_pickle(pickle_id):                ### Charge un fichier pickle, dump en 1 coup
    
    with open(pickle_id, 'rb') as pickle_file:
        pick = pickle.load(pickle_file)
    pickle_file.close()
    return(pick)

def load_pickle_list(pickle_path):             ### Charger un fichier pickle en entier

    data = []
    count = 0
    with open(pickle_path, 'rb') as picklefile:
        try:
            while True:
                data.append(pickle.load(picklefile))
                count += 1
        except EOFError:
            pass
    picklefile.close()
    
    return(data)

def load_pickle_list_elem(pickle_path, nb_elem):   ### Charger uniquement un élément d'un pickle file
    i = 0
    with open(pickle_path, 'rb') as picklefile:
        try:
            while i < nb_elem:
                pickle.load(picklefile)
                i += 1
            if i == nb_elem:
                pick = pickle.load(picklefile)
                picklefile.close()
        except:
            return(i)
    return(pick)

def load_pickle_tab(picklepath):
    df = pd.DataFrame()
    data = []
    count = 0

    with open(picklepath, 'rb') as picklefile:
        try:
            while True:
                data = pickle.load(picklefile)
                tab = pd.DataFrame(data)
                df=df.append(tab,ignore_index=True)
                count += 1
                print(f"\r{count}. lenght : {len(df)}", end="")
        except EOFError:
            print('\n')

    return(df)

##### VAR

codes = jade_setup_class.Jade_cod()
patt = jade_setup_class.Jade_pattern()
rw_list = jade_setup_class.Jade_rewrite()
article = jade_setup_class.Jade_art()

corpusdir = "../jade_all/" # Directory of corpus.
jade_lebon_dir = "../jade_lebon/" # Directory of corpus.
corpus = XMLCorpusReader(corpusdir, '.*')

stopwords = nltk.corpus.stopwords.words('french')
stopwords.append('considérant')
stopwords.append('qu')

dictionarySOM = corpora.Dictionary.load('./Jade_dependency/jade2_somCE.dict') #('./jade_somCE2.dict')
corpusSOM = corpora.MmCorpus('./Jade_dependency/jadecorpus_somCE2.mm') #('./jadecorpus_somCE2.mm')

corpusSOM2ALL = corpora.MmCorpus("./Jade_dependency/jadecorpus_SOM2ALL.mm")
corpusALL2SOM =corpora.MmCorpus('./Jade_dependency/jadecorpus_ALL2SOM.mm')

dictionaryALL = corpora.Dictionary.load('./Jade_dependency/jade_all2.dict') #('./jade_all2.dict')
corpusALL = corpora.MmCorpus('./Jade_dependency/jadecorpus_all2.mm') #('./jadecorpus_all2.mm')

indexALL = similarities.Similarity.load("./Jade_index_all/jade_index_all2.idx",  mmap='r') #("jade_index_all2.idx")
indexSOM = similarities.Similarity.load("Jade_index_all/jade_index_som2.idx") #('jade_index_som2.idx)

idSOM = load_pickle('./Jade_dependency/jadecorpus2_som.id')
idALL = load_pickle('./Jade_dependency/jadecorpus_all2.id')

#idTxtSOM = load_pickle('./Jade_dependency/jadecorpus2_som.cs')
#idTxtALL = load_pickle('./Jade_dependency/jadecorpus_all2.cs')

##### LIBRAIRIE - FONCTIONS

#### UTILITAIRES : 

#@jit(target="cuda")

def ALL2SOM():
    
    stock_ALL2SOM = [[] for i in range(len(corpusALL))]
    
    for i, item in enumerate(corpusALL):
        print(f"\r{i}/{len(corpusALL)}", end="")
        temp = [dictionaryALL[wd] for wd, nb in corpusALL[i]]
        vec = dictionarySOM.doc2bow(temp)
        stock_ALL2SOM[i] = vec

    print('\n',len(stock_ALL2SOM))
    corpora.MmCorpus.serialize('./Jade_dependency/jadecorpus_ALL2SOM.mm', stock_ALL2SOM)
    return()


def SOM2ALL():
    
    stock_SOM2ALL = [[] for i in range(len(corpusSOM))]
    
    for i, item in enumerate(corpusSOM):
        print(f"\r{i}/{len(corpusSOM)}", end="")
        sent = [dictionarySOM[wd] for wd,_ in corpusSOM[i]] #corpusSOM2ALL[nb_SOM]
        vec = dictionaryALL.doc2bow(sent)
        stock_SOM2ALL[i] = vec
    
    print(len(stock_SOM2ALL))
    corpora.MmCorpus.serialize('./Jade_dependency/jadecorpus_SOM2ALL.mm', stock_SOM2ALL)
    return()

    
def rand_consid(level, nb_doc):         ### Ajoute de manière aléatoire un level de mots à un considérant (ALL) nb_doc
    
    temp = corpusALL[nb_doc] #random.randint(0, len(corpus))
    
    temp = [dictionaryALL[wd] for wd,_ in temp]
    
    for i in range(level):
        temp.append(dictionarySOM[random.randint(0, len(dictionarySOM)-1)])
    
    return(temp)


def get_xml_ref(xml_id):                ### Obtenir le nom du fichier d'origine (supprime "_nb")
                                        ### Retourne le nom doc, et "nb", identifiant du consid
    pat = re.compile(".xml")
    res = pat.finditer(xml_id)
    pos = [m.end() for m in res]

    doc = xml_id[:pos[0]]
    idx = int(xml_id[pos[0]+1:])
    
    return(doc, idx)


def merge_token(match_obj,tk):
    repl=""
    txt=match_obj.group().strip()
    txt=re.sub(r'\s{1,20}'," ",txt)
    #on supprime les citations"
    if tk=="QOT":
        #pour les sommaires on n'enlève pas les citations
        repl=txt
    elif tk=="PCJA":
        #on doit enlever les 2 derniers caractères qui sont du token suivant puis les restituer
        #print(txt)
        #last=txt[len(txt)-2:len(txt)]
        #txt=txt[0:len(txt)-2].strip()

        txt=re.sub("[^0-9-]"," ",txt)
        #print(txt)
        pcja=txt.split(" ")
        pcja = [pcj for pcj in pcja if pcj != ""]
        for p in pcja:
            repl+=" PCJA_"+p.strip()+" "
    else:
        repl=tk+"_"+re.sub(" ","_",txt)
        repl=re.sub("'","_",repl)+" "
    return repl


def tokenize_entities(codes, patt, rw_list, article, text):
    #il faut évite les blancs en début pour les regex
    text=text.strip()
    #on reprend certaines expressions ex: CGI pour code général des impôts
    for rw, rw_rep in rw_list.rw_par:
        raw_s = r'{0}'.format(rw)
        text=re.sub(raw_s,rw_rep,text,flags=re.I)
    for cod, cod_tk in codes.cod_par:
        text=re.sub(cod,lambda m: merge_token(m,cod_tk),text,flags=re.I)
    for pat, pat_tk in patt.pat_par:
        raw_s = r'{0}'.format(pat)
        text=re.sub(raw_s,lambda m: merge_token(m,pat_tk),text,flags=re.I)
    for art, art_tk in article.art_par:
        raw_s = r'{0}'.format(art)
        text=re.sub(raw_s,lambda m: merge_token(m,art_tk)+' '+m.group(0),text,flags=re.I)
        
    return text


def tokenize_only(text):

    text=tokenize_entities(codes, patt, rw_list, article, text)
    text = text.replace("'"," ")
    tokens = [word.lower() for sent in nltk.sent_tokenize(text) for word in nltk.word_tokenize(sent)]
    filtered_tokens = []
    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    for token in tokens:
            if re.search('[a-zA-Z]', token):
                filtered_tokens.append(token)
    filtered_tokens_stop = [w for w in filtered_tokens if not w in stopwords]

    
    return filtered_tokens_stop


def get_sommaire(textxml,doc):
    sommaire = []
    idx=[]
    lebon ="C"
    for elt in textxml.iter('PUBLI_RECUEIL'):
        lebon = elt.text
    nbsom=0
    if (lebon == "A") or (lebon == "B"):
        for elt2 in textxml.iter('ANA'):
            nbsom+=1
            if elt2.text is not None:
                sommaire.append(elt2.text)
                idx.append(doc+"_"+str(nbsom))
    return sommaire, idx


def get_content(doc):
    
    xmldoc = corpus.xml(doc)
    for elt in xmldoc.iter('CONTENU'):
        if elt.text is not None:
            return("Not Void")
        else:
            return(None)
    return(None)


def get_publi(textxml,doc):
    for elt in textxml.iter('PUBLI_RECUEIL'):
        lebon = elt.text
    return(lebon)


def get_considerant(doc, idx):
    
    consid_stock = []
    xmldoc = corpus.xml(doc)
    for elt in xmldoc.iter('CONTENU'):
        if elt.text is not None:
            read, doc = format_consid(doc)
            result=re.search('Considérant(.*?)DECIDE',read,re.MULTILINE)
            if result is not None: 
                motif="Considérant "+result.group(1)
                considerantraw = motif.split('#')
                considerantraw = [consid for consid in considerantraw if consid != '' and consid != ' ']

                for consid in considerantraw:
                    if len(consid.strip())>0:
                        consid_stock.append(consid)
    
    if len(consid_stock) > 0:
        return(consid_stock[idx-1])
    else:
        return(None)

def get_considerant_part(doc):
    
    consid_stock = []
    consid_id = []
    nb_consid = 1
    xmldoc = corpus.xml(doc)
    
    for elt in xmldoc.iter('CONTENU'):
        if elt.text is not None:
            read, doc = format_consid(doc)
            result=re.search('Considérant(.*?)DECIDE',read,re.MULTILINE)
            if result is not None: 
                motif="Considérant "+result.group(1)
                considerantraw = motif.split('#')
                considerantraw = [consid for consid in considerantraw if consid != '' and consid != ' ']

                for consid in considerantraw:
                    if len(consid.strip())>0:
                        consid_stock.append(consid)
                        consid_id.append(doc+"_"+str(nb_consid))
                        nb_consid += 1

    return(consid_stock, consid_id)

def get_titre(textxml, doc):
    titre = []
    idx= []
    lebon = "C"
    for elt in textxml.iter('TITRE'):
        lebon = elt.text
    return(lebon)

### Do not use
def read_considerant(textxml, doc, idx):
    considerant = []
    cons_id = []
    #nbconsid=1
    for elt2 in textxml.iter('CONTENU'):
        if elt2.text is not None:
            result=re.search('Considérant(.*?)DECIDE',elt2.text,re.MULTILINE)
            if result is not None: 
                motif="Considérant "+result.group(1)
                considerantraw = motif.split('#')
                for consid in considerantraw:
                    if len(consid.strip())>0:
                        considerant.append(consid.strip())
                        #cons_id.append(doc+"_"+str(nbconsid))
                        #nbconsid+=1
    return(considerant[idx])


def comp_juris_ALL(nb_ALL, nb_SOM):

    print("-- CP --")
    get_ref_considSOM(nb_SOM)
    print("-- Condidérant --")
    get_ref_considALL(nb_ALL)
    stock_sim = find_similariteALL(nb_ALL, 0)

    print("\n------------ JURISPRUDENCES --------------\n")

    for wd, score in stock_sim[:5]:
        get_ref_considSOM(wd)
        print(wd, score)


########## CONSIDERANT - SOM

def load_best_scoreSOM():       ### Charge en mémoire les considérant (ALL)
                                ### qui sont similaires à chaque CP (SOM)

    texte = ""
    stock_sim = {}

    with open("../listes_good_sim_by_SOM.txt", 'r') as f:
        texte = [line.rstrip("\n").split() for line in f.readlines()]
    
    for i, sim in enumerate(texte):
#       print(i, sim)
        idx = sim[0]

        if len(sim) > 1:
            cible = (sim[1], sim[2])
            if idx in stock_sim.keys():
                stock_sim[idx].append(cible)
            else:
                stock_sim[idx] = [cible]

    return(stock_sim)


def find_best_score_consid_SOM(cos):  #### Cherche et enregistre les similarités (>= cos)
                                      ####  avec les considérants (ALL) pour chaque CP (SOM)

#    f = open('../listes_good_sim_by_SOM.txt', 'w')

    dico = {}   

    for i, item in enumerate(corpusSOM):
        print('Reste : ', len(corpusSOM)-i, '/{}'.format(len(corpusSOM)))
        stock_sim = []
        temp = [dictionarySOM[wd] for wd,_ in item]
        vec_bow = dictionaryALL.doc2bow(temp)
        sims=indexALL[vec_bow]
        sims = sorted(enumerate(sims), key=lambda item: -item[1])

        for doc_pos, doc_score in sims:
            if doc_score >= cos:
                print(i, doc_score)
                stock_sim.append((doc_pos, doc_score))
#                f.writelines(str(i)+' '+str(doc_pos)+' '+str(doc_score)+'\n')
            else:
                break;                
#    f.close()
    return()

def find_similariteSOM(nb_SOM, cos):   ### Cherche les considérants qui sont similaires (cos >= 0.2) à un CP nb_SOM
      
    sent = corpusSOM2ALL[nb_SOM]
    sims=indexALL[sent]
    condition = sims >= cos
    list_cos = np.extract(condition, sims)
    list_id = np.extract(condition, np.arange(0,len(sims),1))
    stock_sim = list(zip(list_id, list_cos))
    stock_sim = sorted(stock_sim, key=lambda item: -item[1])
    
    return(stock_sim)


def get_nb_consid(nb_SOM, cos):     ### Donne le nomre de considérant similaire par rapport au cosinus (cos)
                                    ### et affiche tout les consiédrants similaires au CP (nb_SOM)
    sent = [dictionarySOM[wd] for wd, nb in corpusSOM[nb_SOM] for i in range(int(nb))]
    vec_bow = dictionaryALL.doc2bow(sent)
    sims=indexALL[vec_bow]
    condition = sims >= cos
    list_cos = np.extract(condition, sims)
    doc = len(list_cos)
    
    return(doc)


def find_similitudesSOM(nb_SOM, cos):   ### Cherche les considérants de principe qui sont ressemble (cos) à un CP nb_SOM
    
    stock_sim = []
#    temp = [dictionarySOM[wd] for wd,_ in corpusSOM[nb_SOM]]
#    vec_bow = dictionarySOM.doc2bow(temp)
    sims=indexSOM[corpusSOM[nb_SOM]]
    condition = sims >= cos
    list1 = np.extract(condition, sims)
    list2 = np.extract(condition, np.arange(0,len(sims),1))
    stock_sim = list(zip(list2, list1))
    stock_sim = sorted(stock_sim, key=lambda item: -item[1])

    return(stock_sim)

def find_jurisprudence(nb_SOM, cos):
    
    stock_sim = find_similitudesSOM(nb_SOM, cos)
    temp = []
    
    for doc_pos, doc_score in stock_sim:
        stock_temp = find_similitudesSOM(doc_pos, cos)
        stock_temp.pop(0)
        pos = [pos for pos,score in stock_temp]
        if nb_SOM in pos and doc_score >= cos:
            cosi = np.array([score for pos,score in stock_temp])
            if len(np.where(cosi >= 0.55)[0]) > 0:
                temp.append(doc_pos)
        else:
            continue
    if nb_SOM not in temp:
        temp.append(nb_SOM)
    temp = sorted(temp, key=lambda item: item)

    return(temp)


def get_refxml_SOM(nb_consid):
    
    doc, idx = get_xml_ref(idSOM[nb_consid])
    return(doc, idx)

def get_ref_considSOM(nb_consid):


    corpus = XMLCorpusReader(jade_lebon_dir, '.*')    
    doc, idx = get_xml_ref(idSOM[nb_consid])
    xmldoc =  corpus.xml(doc)
    
    for i, elt in enumerate(xmldoc.iter('ANA')):
        if i == idx-1:
            element = elt.text
    for elt in xmldoc.iter('META_JURI_ADMIN'):
        for child in elt:
            if child.tag == 'PUBLI_RECUEIL':
                tag = child.text

#    print('\n', get_titre(xmldoc,doc))
    return(doc, idx, element, tag)


########## CONSIDERANT - ALL

def find_similariteALL(nb_ALL, cos):   ### Cherche les CP qui sont similaires (cos) à un considérant nb_ALL

    vec_bow = corpusALL2SOM[nb_ALL]
    sims=indexSOM[vec_bow]
    condition = sims >= cos
    list_cos = np.extract(condition, sims)
    list_id = np.extract(condition, np.arange(0,len(sims),1))
    stock_sim = list(zip(list_id, list_cos))
    stock_sim = sorted(stock_sim, key=lambda item: -item[1])
    
    return(stock_sim)

def format_consid(doc):
    
    case_file=open(corpusdir+doc,"r",encoding="utf8")
    case_raw = case_file.read()
    case_file.close()
    decid = ["DÉCIDE", "Decide", "Décide", "D E C I D E", "D É C I D E", "D E C  ID E", "D  E  C  I  D  E",
            "D EC I D E", "D ÉC I D E", "D E C I DE", "DE C I D E", "D É C I DE", "D É CI D E", "Article 1er"]
    for dec in decid:
        case_raw=case_raw.replace(dec, "DECIDE") 
    case_raw=case_raw.replace("\n"," ") 
    case_raw=case_raw.replace("<br/><br/>","#")
    case_raw=case_raw.replace("<br/>","#")
    case_raw=case_raw.replace("O R D O N N E", "DECIDE ORDONNE")
    case_raw=case_raw.replace("ORDONNE", "DECIDE ORDONNE")
    case_raw=case_raw.replace("Il sera publié au Journal officiel de la République française", "DECIDE JO")
    case_raw=case_raw.replace("REND L'AVIS SUIVANT", "Considérant")
    case_raw=case_raw.replace("Considerant", "Considérant")
    case_raw=case_raw.replace("considérant", "Considérant")
    case_raw=case_raw.replace("considerant", "Considérant")
    case_raw=case_raw.replace("CONS.", "Considérant")
    case_raw=case_raw.replace("CONSIDERANT", "Considérant")
    pat = re.compile("<(.*?)>")
    case_raw= re.sub(pat, "#", case_raw)
    pat = re.compile("\s{3,}")
    case_raw= re.sub(pat, "#", case_raw)
    
    
    return(case_raw, doc)


def format_considALL(nb_consid):
    
    with open('./Jade_dependency/jadecorpus_all2.id', 'rb') as pickle_file:
        case_some_id = pickle.load(pickle_file)
    pickle_file.close()
    
    doc, idx = get_xml_ref(case_some_id[nb_consid])
        
    case_file=open(corpusdir+doc,"r",encoding="utf8")
    case_raw = case_file.read()
    case_file.close()
    decid = ["DÉCIDE", "Decide", "Décide", "D E C I D E", "D É C I D E", "D E C  ID E", "D  E  C  I  D  E",
            "D EC I D E", "D ÉC I D E", "D E C I DE", "DE C I D E", "D É C I DE", "D É CI D E", "Article 1er"]
    for dec in decid:
        case_raw=case_raw.replace(dec, "DECIDE") 
    case_raw=case_raw.replace("\n"," ") 
    case_raw=case_raw.replace("<br/><br/>","#")
    case_raw=case_raw.replace("<br/>","#")
    case_raw=case_raw.replace("O R D O N N E", "DECIDE ORDONNE")
    case_raw=case_raw.replace("ORDONNE", "DECIDE ORDONNE")
    case_raw=case_raw.replace("Il sera publié au Journal officiel de la République française", "DECIDE JO")
    case_raw=case_raw.replace("REND L'AVIS SUIVANT", "Considérant")
    case_raw=case_raw.replace("Considerant", "Considérant")
    case_raw=case_raw.replace("considérant", "Considérant")
    case_raw=case_raw.replace("considerant", "Considérant")
    case_raw=case_raw.replace("CONS.", "Considérant")
    case_raw=case_raw.replace("CONSIDERANT", "Considérant")
    pat = re.compile("<(.*?)>")
    case_raw= re.sub(pat, "#", case_raw)
    pat = re.compile("\s{3,}")
    case_raw= re.sub(pat, "#", case_raw)
    
    return(case_raw, doc, idx)

def get_refxml_ALL(nb_consid):
    
    doc, idx = get_xml_ref(idALL[nb_consid])
    return(doc, idx)

def get_ref_considALL(nb_consid):

    doc, idx = get_xml_ref(idALL[nb_consid])
    return(doc, idx, get_considerant(doc, idx))
    
############## VISAS


def get_visa_from_consid(nb_consid, prt=False):
    
    case_raw = format_visa(xmlfile)
    xmldoc = ET.fromstring(case_raw)
    
    visa = []
    visa_id = []
    nbvisa=1
    
    for elt in xmldoc.iter('CONTENU'):
        if elt.text is not None:
            result=re.search('Vu(.*?)Considérant',elt.text,re.MULTILINE)
            motif="Vu "+result.group(1)
            visaraw = motif.split('#')
            for visas in visaraw:
                if len(visas.strip())>0:
                    visa.append(visas.strip())
                    visa_id.append(doc+"_"+str(nbvisa))
                    nbvisa+=1
    if prt:
        for i, visas in enumerate(visa):
            pass
            #print(i, visas)
    return(visa)

def format_visa(xmlfile):
    
    case_file=open(corpusdir+xmlfile,"r",encoding="utf8")
    case_raw = case_file.read()
    case_file.close()
    decid = ["DÉCIDE", "Decide", "Décide", "D E C I D E", "D É C I D E", "D E C  ID E", "D  E  C  I  D  E",
            "D EC I D E", "D ÉC I D E", "D E C I DE", "DE C I D E", "D É C I DE", "D É CI D E"]
    for dec in decid:
        case_raw=case_raw.replace(dec, "DECIDE") 
    case_raw=case_raw.replace("\n"," ") 
    case_raw=case_raw.replace("<br/><br/>","#")
    case_raw=case_raw.replace("<br/>","#")
    case_raw=case_raw.replace("O R D O N N E", "DECIDE ORDONNE")
    case_raw=case_raw.replace("ORDONNE", "DECIDE ORDONNE")
    case_raw=case_raw.replace("Il sera publié au Journal officiel de la République française", "DECIDE JO")
    case_raw=case_raw.replace("REND L'AVIS SUIVANT", "Considérant")
    case_raw=case_raw.replace("Considerant", "Considérant")
    case_raw=case_raw.replace("considérant", "Considérant")
    case_raw=case_raw.replace("considerant", "Considérant")
    case_raw=case_raw.replace("CONS.", "Considérant")
    pat = re.compile("\s{3,}")
    case_raw= re.sub(pat, "#", case_raw)
    
    return(case_raw)


def get_visa_from_file(xmlfile):

    case_raw = format_visa(xmlfile)
    xmldoc = ET.fromstring(case_raw)
    
    visa = []
    visa_id = []
    nbvisa=1
    
    for elt in xmldoc.iter('CONTENU'):
        if elt.text is not None:
            result1=re.search('Vu(.*?)Considérant',elt.text,re.MULTILINE)
            
            if result1 is None:
                result=re.search('Vu(.*?)Sur',elt.text,re.MULTILINE)
                if result is None:
                    break;
                motif="Vu "+result.group(1)
                visaraw = motif.split('#')
                visaraw = [visa for visa in visaraw if visa != '' and visa != ' ']
                visaraw = visaraw[:len(visaraw)-1]

            else:
                motif="Vu "+result1.group(1)
                visaraw = motif.split('#')
                visaraw = [visa for visa in visaraw if visa != '' and visa != ' ']

            
            for visas in visaraw:
                if len(visas.strip())>0:
                    visa.append(visas.strip())
                    visa_id.append(xmlfile+"_"+str(nbvisa))
                    nbvisa+=1

    for i, visas in enumerate(visa):
        print(i, visas)
    return(visas)


def get_sub_visa(xmlfile, print=True):
    
    case_raw = format_visa(xmlfile)
    xmldoc = ET.fromstring(case_raw)

    procedure_CA = ["Vu", "Procédure devant la [cC]our", "soutien\w* que :", "(Vu :|V?u? les autres pièces d[ue]s? dossier\w?)", "Considérant"]
    procedure_CE = ["Vu", "(Vu :|V?u? les autres pièces d[ue]s? dossier\w?)", "Considérant"]
    proced = []
    visa_id = []
    
    for elt in xmldoc.iter('CONTENU'):
        if elt.text is not None:
            result1=re.search('Vu(.*?)Considérant',elt.text,re.MULTILINE)
            
            if result1 is not None:
                motif="Vu "+result1.group(1)
                visaraw = motif.split('#')
                visa = [visa for visa in visaraw if visa != '' and visa != ' ']
                visa.append("Considérant")
            else:
                result=re.search('Vu(.*?)Sur',elt.text,re.MULTILINE)
                if result is None:
                    return(proced, visa_id)
                    break;
                motif="Vu "+result.group(1)
                visaraw = motif.split('#')
                visa = [visa for visa in visaraw if visa != '' and visa != ' ']
                visa.append("Considérant")

            stock = [juri.text for juri in xmldoc.iter('JURIDICTION')]
            if "Conseil" in stock[0] or "CETAT" in stock[0]:
                    proced, visa_id = process_visa(procedure_CE, '#'.join(visa), xmlfile)
            else:
                    proced, visa_id = process_visa(procedure_CA, '#'.join(visa), xmlfile)
        if print:            
            print_visa(proced, visa_id)
        return(proced, visa_id)


def process_visa(procedure, elt, xmlfile):
    
    visa_id = []
    proced = []
    nbvisa = 1
    i= 0
    
    while i < len(procedure):
        visa = []
        if i == 0:
            i += 1
            continue;
        else:
            result=re.search(procedure[i-1]+'(.*?)'+procedure[i],elt,re.MULTILINE)
            if result is None:
                procedure.remove(procedure[i])
                result=re.search(procedure[i-1]+'(.*?)'+procedure[i],elt,re.MULTILINE)
                continue;
            else:
                motif=result.group(0)
                visaraw = motif.split('#')
                for j, visas in enumerate(visaraw):
                    if j == len(visaraw)-1:
                        break;
                    elif len(visas.strip())>0:
                        visa.append(visas.rstrip())
                        visa_id.append(xmlfile+"_"+str(nbvisa))
                        nbvisa+=1
                proced.append(visa)
            i += 1
    return(proced, visa_id)      


def print_visa(visa, visa_id):
    
    for i, visas in enumerate(visa):
        print(visa_id[i])
        for j, vis in enumerate(visas):
            print(vis)

def get_visa_from_consid(nb_consid):
    
    doc, idx = get_refxml_ALL(nb_consid)
    get_sub_visa(doc)



########## METADONNEES

def format_date(date, year = True, month = False):
    
    if year == True:
        pat = re.compile("\d{4}")
        result = re.search(pat, date)
#    print(result.group(0))
        return(result.group(0))
    if month == True:
        pass;         
        return()      #Faire une fonction pour récupérer les mois ? Pas forcément nécessaire

def get_metadata(xmlfile):
    
    xmldoc =  corpus.xml(xmlfile)
    metadata = {}
    for SPEC in xmldoc.iter("META_SPEC"):
        for JURI in SPEC:
            if JURI.tag == "META_JURI_ADMIN":
                for child_admin in JURI:
                    if child_admin.text != None:
                        if child_admin.tag not in ["FORMATION", "TYPE_REC","PUBLI_RECUEIL"]:
                            continue
                        else:
                            metadata[child_admin.tag] = child_admin.text.lower()
                    else:
                        metadata[child_admin.tag] = "Void"
                            
            elif JURI.tag == "META_JURI":
                for child in JURI:
                    if child.text != None:
#                    print(child.tag, ": ", child.text)
                        temp = child.text.lower()
                        if child.tag == "DATE_DEC":
                            metadata[child.tag] = format_date(child.text)
                            continue
                        if child.tag == "JURIDICTION":
                            temp = temp.replace("cour administrative d'appel", "caa")
                            temp = temp.replace("conseil d'etat", "conseil d'état")
                            temp = temp.replace("caa de versailless", "caa de versailles")
                        if child.tag == 'TYPE_REC':
                            temp = temp.replace("exces de pouvoir", "excès de pouvoir")
                        metadata[child.tag] = temp
                    else:
                        metadata[child.tag] = "Void"
    
    return(metadata)

def get_date_ante(visa, xmlfile):
    
    pat = re.compile("\d{1,2}(er){0,1}(\s){1,10}(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)(\s){1,10}(\d){4}")
    date = []
    
    for i, visas in enumerate(visa):
        result = re.search(pat, visas)
        if result is None:
            continue;
        else:
            print(i, visas)#Le.tokenize_only(visas))
            pos = re.finditer(pat, visas)
            pos = [m.span() for m in pos]
            for po in pos:
                date.append(visas[po[0]:po[1]])
                
    return(date)


def get_tokenize_juri_items(visa):
    
    if len(visa) > 0:
        visa = ' '.join(visa[len(visa)-1])
    else:
        return(["No Visa"])
    visa = tokenize_only(visa)
    temp = []
    for word in visa:
        word = word.replace("°", "_")
        word = word.replace("-", "_")
        word = word.replace(".", "_")
        if 'cod_' not in word and "loi_" not in word and 'ord_' not in word and "dec_" not in word and 'art_' not in word:
            continue
        if "akcod_" in word or "lcod_" in word:
            word.replace("ak", "")
            word.replace("l", "")
        else:
            if word in temp:
                pass
            else:
#            print("LEGAL_ITEMS",word, '\n')
                
                temp.append(word)
    
    return(temp)

def cod_sorted(temp):
    
    stock_item = []
    stock_item = [tokenize_only(cod) for cod, cod_tk in codes.cod_par]
    ret = []
    
    if temp == ["No Visa"]:
        return(temp)
    
    for cod_item in stock_item:
        loop = False
        for legal_item in temp:
            if legal_item == '_'.join(cod_item):
                ret.append(legal_item)
                temp.pop(temp.index(legal_item))
                loop = True
                break
        if loop:
            pass
        else:
            ret.append("VOID")
                
    for reste in temp:
        ret.append(reste)
            
    return(ret)


def get_pcja(doc, idx):
    
    stack = []
    
    if doc.endswith('.xml'):
        xmldoc = corpus.xml(doc)
        for i, elt in enumerate(xmldoc.iter('ANA')):
            if i == idx-1:
                if elt.text != None:
                    stock = tokenize_only(elt.text)
                    stock = [word for word in stock if "pcja_" in word]
                    if len(stock) > 0:
                        stack.extend(stock)
                    else:
                        continue
    else:
        stack = []
    
    return(stack)
        


#### TABLEAU


def corpus_to_db():
        
    db = []
    
    for i, item in enumerate(corpusALL):
        print(i, len(corpusALL))
        stock = find_similariteALL(i, 0.3)
        if stock:
            print(stock)
            idx, score = stock[0]
            db.append([i, score, idx])
        else:
            db.append([i])
    
    tab = pd.DataFrame(db)
    tab.columns = ['index_all', 'cos', 'index_som']
    print(tab)
    return(tab)

def to_tab(picklefile):
    tab = []
    stock = load_pickle(picklefile)
    for steak in stock:
        if stock[steak] != None:
            for item in stock[steak]:
                tab.append(item)
    
    tab = pd.DataFrame(tab)
    print(tab)
#    tab.to_excel('./Result_temp/tab.xlsx')
    return(tab)

def mining_visa(export=True):

    j = 0
    stock_visa = {}

    doc_list=list()
    for f in os.listdir(corpusdir):
        doc_list.append(f)
#
    for i, doc in enumerate(doc_list):
        print(i, doc)
        proced, visa_id = get_sub_visa(doc)
        if proced != []:
            stock_id = {}
            for i, proc in enumerate(proced):
                _, idx = get_xml_ref(visa_id[i])
                stock_id[idx] = proc
            stock_visa[doc] = stock_id

        else:
            j+=1
            print("-----------------------------------", doc)
    
    print(j, len(doc_list), j/len(doc_list)*100)
    print(len(stock_visa.keys()))
    if export:
        pickle.dump(stock_visa, open("./jade_visa.xp", 'wb'))
    return(stock_visa)
