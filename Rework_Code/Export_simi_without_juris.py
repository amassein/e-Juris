import time
import Librairie_ejuris as Le
import numpy as np
import pickle
import multiprocessing
import os


##### CONST

TAB = "./Result_temp/futur_tab_without_juris.xport"
TEST = "TAB_TEST.XPORT"

##### FUNC

class batch:     ## Création d'un batch pour les similarités
    def __init__(self, size, cos, mini, nb_start):
        self.size = size    
        self.cos = cos
        self.mini = mini
        self.var = list(np.arange(nb_start, len(Le.corpusSOM2ALL), self.size)) ## --> Réduit le temps de travail
        self.var.append(len(Le.corpusSOM2ALL))
        self.nb_SOM = 0

class ALL:
    def __init__(self):
        self.pos = None
        self.cos = 0
        self.tot = None
    

class LIST:
    def __init__(self):
        self.SOM = None
        self.bool = None


def test_cos(SimiC, condition):
    
    temp = np.extract(condition, SimiC.pos)
    for i, item in enumerate(temp):
        temp[i] = len(Le.corpusALL[item])
    condition = np.array(temp) > 10
    if True in condition:
        return(True)
    else:
        return(False)
        
def formatSOM(LISTE, i, cos, idx):

    if LISTE.bool:
        SOM = LISTE.SOM[i]
        condition = SOM >= cos
        SOM_COS = np.extract(condition, SOM)
        SOM_POS = np.extract(condition, np.arange(0,len(SOM),1))
        SOM = list(zip(SOM_POS, SOM_COS))
        SOM = sorted(SOM, key=lambda item: -item[1])
        if len(SOM) > 0:
            SOM = SOM[0][0]
        else:
            SOM = None
        
    return(SOM)


def first_loop(similarity, count, batch_SOM, chunk, picklefile):
    
    SimiC = ALL()
    
    for j, simi in enumerate(similarity):      ## Pour chaque individu nb_SOM (dans similarity)
        batch_SOM.nb_SOM = j+(batch_SOM.var[count-1])
        read = Le.corpusSOM[batch_SOM.nb_SOM]
        
        if batch_SOM.nb_SOM in stop:
            continue
        elif batch_SOM.nb_SOM in Doublons.keys():
            stop.extend(Doublons[batch_SOM.nb_SOM])
        
        if len(read) > 10:                     ## Si le CP est trop court, on ne le prend pas en compte
            
            SimiC.pos = np.where(simi > batch_SOM.cos)[0]  ## Si le CP n'a pas de consid > 0.7 de cos, on ne le prend pas en compte
            SimiC.cos = simi[SimiC.pos]
            SimiC.tot = list(zip(SimiC.pos, SimiC.cos))
            SimiC.tot = sorted(SimiC.tot, key=lambda item: -item[1])
            condition = SimiC.cos >= 0.7
            
            if test_cos(SimiC, condition):       ### On test s'il y a des considérants > 0.7, et s'ils sont plus long que 20 mots.
                if len(SimiC.pos) > batch_SOM.mini:       ## Si on a pas assez de considérants, on ne prend pas en compte
                    stock = second_loop(SimiC, batch_SOM)
                    if len(stock) > 0:
                        pickle.dump(stock, picklefile)
                        del stock

                else:
                    print(f"\r{batch_SOM.nb_SOM}-----------> BREAK 1", end="")
                    continue
            else:
                print(f"\r{batch_SOM.nb_SOM}-----------> BREAK 2", end="")
                continue
            
        else:
            print(f"\r{batch_SOM.nb_SOM}-----------> BREAK 3", end="")
            continue
    
    return()


def second_loop(SimiC, batch_SOM):

    stock = []
    LISTE = LIST()
                                              ## Ici, ce sont les similarités des consids avec les CP qu'on teste
    print('\n', batch_SOM.nb_SOM, len(SimiC.pos), '\n')    
    if len(SimiC.pos) > 10000:             ## Si on a plus de 100000 considérant, on fait plusieurs batchs pour ne pas                                                    surcharger la RAM
        batch = [i for i in range(len(SimiC.pos)) if i%10000 == 0 or i == len(SimiC.pos)-1] ## Batch de 10.000
        for i, item in enumerate(batch):
            if item != 0:
                LISTE.bool = True
                LISTE.SOM = Le.indexSOM[Le.corpusALL2SOM[SimiC.pos[batch[i-1]:batch[i]]]]
                print(f"\n--{i}--\n")
                stock = third_loop(SimiC.pos[batch[i-1]:item], SimiC.cos[batch[i-1]:item], batch_SOM, LISTE, stock)
                    
                                
    else:
        LISTE.SOM = Le.indexSOM[Le.corpusALL2SOM[SimiC.pos]]
        LISTE.bool = True
        stock = third_loop(SimiC.pos, SimiC.cos, batch_SOM, LISTE, stock)     
    
    if len(stock) < batch_SOM.mini:       ## <--- Si le nombre de considérant retrouvé est trop petit, on ne prend pas en compte
        stock = [] 
    
    return(stock)


def third_loop(ALL_pos, ALL_cos, batch_SOM, LISTE, stock):
            
    for i, idx in enumerate(ALL_pos):         ## ALL_pos est différent si on est une sur un ou plusieurs batchs
        print(f"\r{i} / {len(ALL_pos)}", end="")
        doc, doc_id = Le.get_refxml_ALL(idx)  # On récupére le nom du fichier
        read = Le.corpusALL[idx]              # On récupère les mots du corpus
        
        if len(read) <= 10: # Si le consid est trop court, on ne le prend pas en compte (10 mots)
            continue;
        
        else:
            SOM = formatSOM(LISTE, i, batch_SOM.cos, idx) ## Pour les sommaires auxquels ressemblent le plus le consid
            
            if batch_SOM.nb_SOM == SOM:       ## Si le CP d'origine est dans les CP retrouvés, c'est good.
                stock.append([idx, ALL_cos[i], batch_SOM.nb_SOM, doc])

    print(f"\nLENGHT --- {len(stock)}, ALL_LENGHT --- {len(ALL_pos)}, NB_SOM ---> {batch_SOM.nb_SOM}")     
    
    return(stock)

###### MAIN

if __name__ == '__main__':

    PATH_FILE = TAB
    cos = 0.27
    mini = 5
    stop = []
    Doublons = Le.load_pickle("./Result_temp/Dico_simi.xp")

    if False:#os.path.exists(PATH_FILE):
        print("LOADING 1ST EXPORT")
        stock = Le.load_pickle_list(PATH_FILE)
        batch_SOM = batch(100, cos, mini, stock[len(stock)-1][0][2])
    
        with open(PATH_FILE, "wb") as picklefile:
            print("DUMPING")
            for item in stock:
                pickle.dump(item, picklefile)
                  ## On donne ces simi à notre première boucle
            del stock
            for count, chunk in enumerate(batch_SOM.var):
                similarity = None       ## --> Pour libérer la RAM
                if count == 0:
                    continue
                print("\n",batch_SOM.var[count-1]," - ", chunk, ":\n")
                similarity = Le.indexALL[Le.corpusSOM2ALL[batch_SOM.var[count-1]:chunk]] ## On stoque les x similarités
                first_loop(similarity, count, batch_SOM, chunk, picklefile)
            
    else:
        with open(PATH_FILE, "wb") as picklefile:
            batch_SOM = batch(1000, cos, mini, 0)
            
            for count, chunk in enumerate(batch_SOM.var):
                if count == 0:
                    continue
                print("\n",batch_SOM.var[count-1]," - ", chunk, ":\n")
                similarity = Le.indexALL[Le.corpusSOM2ALL[batch_SOM.var[count-1]:chunk]] ## On stoque les x similarités
                first_loop(similarity, count, batch_SOM, chunk, picklefile)       ## On donne ces simi à notre première boucle
                del similarity      ## --> Pour libérer la RAM
