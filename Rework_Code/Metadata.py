import Librairie_ejuris as Le
import os
import pickle
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

##### CONST VAR

XP_META_TEST = "./Result_temp/export_metadata_test.xp"
XP_META = "./Result_temp/export_metadata.xp"
XP_META_TAB = "./Result_temp/export_metadata_tab.xp"
XP_META_PA = './Result_temp/metadata.parquet'

##### FUNC


def Export_metadata():
        
    files = os.listdir(Le.corpusdir)
    stock = [[] for i in range(len(files))]
    
    for i, file in enumerate(files):
        if file.endswith(".xml"):
            print(f"\r ------- {i}/{len(files)-1}", end="")
            metadata = Le.get_metadata(file)
            stock[i] = [file]
            stock[i].extend(list(metadata.values()))
        if i%50000 == 0:
            print(stock[i])
    
    pickle.dump(stock, open(XP_META, 'wb'))

def pickle_to_tab():
    
    files = os.listdir(Le.corpusdir)
    metadata_col = Le.get_metadata(files[len(files)-2])
    
    stock = Le.load_pickle(XP_META)
    tab = pd.DataFrame(stock)
    tab.columns = ["XMLFILE"]+list(metadata_col.keys())
    tab = tab.iloc[:len(tab)-1,:9]
    print(tab)
    tab.to_parquet("./Result_temp/metadata.parquet.gzip", compression="gzip")
    

###### MAIN

Export_metadata()
pickle_to_tab()

#conversion_pandas_pa('metadata.csv', 'metadata.parquet.gzip')

