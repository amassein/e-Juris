#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 09:05:03 2022

@author: Aljoscha
"""

####### DASH RENDERER

import dash
from dash import dcc
from dash import html
from dash import dash_table
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
from skimage import io
from dash.dependencies import Input, Output
import re
import numpy as np
import pandas as pd
import string
import re
import nltk
import numpy as np
import gensim
import matplotlib.pyplot as plt
import Librairie_ejuris as Le
import all_fig as AF

pd.options.plotting.backend = "plotly"


### VAR & GRAPH
    
meta = pd.read_parquet("./Result_temp/metadata_recod.parquet.gzip")
meta.DATE_DEC = meta.DATE_DEC.astype('int')
meta = meta.loc[(meta.DATE_DEC >= 1965) & (meta.DATE_DEC < 2999)]
del meta['TITRE'], meta['NUMERO']
df = pd.read_parquet("./Result_temp/tab_without_juris.parquet.gzip")
merged = pd.read_parquet("./Result_temp/Result_tab/merge_tab.parquet.gzip")
merged.DATE_DEC = merged.DATE_DEC.astype('int')
merged = merged.loc[(merged.DATE_DEC >= 1965) & (merged.DATE_DEC < 2999)]
del merged['TITRE'], merged['NUMERO']
cp = pd.read_parquet("./Result_temp/Result_tab/merge_tab_SOM.parquet.gzip")
pcja = pd.read_parquet("./code_pcja.parquet.gzip")
pcja_descri = pd.read_parquet("./Result_temp/pcja_with_descri_1_level.parquet.gzip")
#mat_cos = pd.read_parquet("./Result_temp/Matrice_cos_som.parquet.gzip")
legal = pd.read_parquet("./Result_temp/legal_cod.parquet.gzip")



def main():
    
    ### STYLESHEET
    
    app = dash.Dash(__name__, 
                    title="E-Juris_projet", 
                    external_stylesheets=[dbc.themes.SPACELAB])

    
    ### COMPONENTS
    
    
    ### LAYOUT
    
    app.layout = dbc.Container([
      html.Div(children=[html.H1("Analyse des jurisprudences"),
                         html.Br(),
                         html.H2('étude sur le contentieux administratif'),
                         html.H3('Retour sur la sélection de l’information'),
                         html.P("Nous nous intéressons aux associations entre considérants au sein des décisions de justices administratives et les jurisprudences administratives."),
                         html.P("On considère que les considérants de principe sont des jurisprudences, une fois le travail d’association réalisé "),
                         html.P("Jurisprudence = texte issu du Conseil d’Etat, publié en « A » ou « B » au recueil Lebon, et qui est réutilisé par les juridictions administratives. Implique une forme de diffusion et de sélection préalable : Evelyne Serverin"),
                         html.P("Pourquoi ces textes sont comparables ? : « les structures des textes juridiques sont des points d'appui précieux pour construire une analyse automatique »"),
                         html.Br(),
                         
                         html.H3("Présentation des données"),
                         html.Br(),
                         html.Div(
                             children=[html.H2("Tableau des considérants"),
                                       dash_table.DataTable(
                                           merged[:1000].to_dict('records'), 
                                           columns=[{"name": i, "id": i} for i in merged[:1000].columns],
                                           page_size=15,
                                           style_table={'overflowX': 'auto'},
                                           )]),        
                         
                         html.Label('Sélectionner la jurisprudence'),
                         dcc.Dropdown(options=merged['NB_CP'].unique(), value=merged['NB_CP'].unique()[0], id='input_val'),
                         html.Div(id='output_consid'),
                         
                         html.Br(),
                         
                         html.Label('Entrez le considérant  : '),
                         dcc.Input(value=None, id='consid_text', type='text'),
                         html.Div(id='output_text'),
                         
                         html.Br(),
                         
                         html.Div(
                             children=[html.H2("Tableau des métadonnées"),
                                       dash_table.DataTable(
                                           meta[:1000].to_dict('records'), 
                                           columns=[{"name": i, "id": i} for i in meta[:1000].columns],
                                           page_size=7,
                                           style_table={'overflowX': 'auto'},
                                           )]),
                         
                         
                         html.Div(
                             children=[html.H2("Tableau des PCJA"),
                                       dash_table.DataTable(
                                           pcja_descri[:1000].to_dict('records'), 
                                           columns=[{"name": i, "id": i} for i in pcja_descri[:1000].columns],
                                           page_size=7,
                                           style_table={'overflowX': 'auto'},
                                           )]),
                         
                         
                         html.Label('Entrez le PCJA  : '),
                         dcc.Dropdown(options=pcja['Code'].unique(), value=pcja['Code'].unique()[0], id='input_code_pcja'),
                         html.Div(id='output_code_pcja'),
                         
                         html.Br(),
                         
                         html.H2("Etudes sur l'ensemble des documents"),
                         html.Div(children=[
                             dcc.Graph(
                                 id='graph_cp',
                                 figure=AF.fig_count_CP,
                                 style={'display': 'flex'}
                                 )]),
                         
                         html.Br(),
                         
                         html.H3('Distribution des codes en fonction de la jurisprudence'),
                         html.Label("Entrez le considérant  : "),
                         dcc.Dropdown(options=merged['NB_CP'].unique(), value=merged['NB_CP'].unique()[0], id='input_code'),
                         html.Div(id='output_code'),
                         
                         html.Br(),
                         
                         html.H3('PCJA'),
                         html.Div(children=[
                             dcc.Graph(
                                 id='graph_pcja',
                                 figure=AF.fig_pcja1,
                                 style={'display': 'flex'}
                                 ),
                             dcc.Graph(
                                 id='graph_pcja2',
                                 figure=AF.fig_pcja2,
                                 style={'display': 'flex'}
                                 ),
                             dcc.Graph(
                                 id='graph_pcja3',
                                 figure=AF.fig_pcja3,
                                 style={'display': 'flex'}
                                 ),
                             ]),
                         html.Br(),
                         dcc.Markdown('''
                         # Ensemble des fonctions créées jusqu'ici         
                         ```python
                         
                         def rand_consid(level, nb_doc):         ### Ajoute de manière aléatoire un level de mots à un considérant (ALL) nb_doc
                         
                         
                         def load_pickle(pickle_id):             ### Charger un fichier pickle
                         
                         
                         def get_xml_ref(xml_id):                ### Obtenir le nom du fichier d'origine (supprime "_nb")
                                                                ### Retourne le nom doc, et "nb", identifiant du consid
                                                        
                         def get_titre(textxml, doc):          ### Extrait le titre d'une décision de justice


                         def read_considerant(textxml, doc, idx): ### Extrait le texte d'un considérant


                         #### CONSIDERANT - SOM

                         def load_best_scoreSOM():       ### Charge en mémoire les considérant (ALL)
                                                         ### qui sont similaires à chaque CP (SOM)


                         def find_best_score_consid_SOM(cos):  #### Cherche et enregistre les similarités (>= cos)
                                                              ####  avec les considérants (ALL) pour chaque CP (SOM)


                         def find_similariteSOM(nb_SOM, cos):   ### Cherche les considérants qui sont similaires (cos >= 0.2) à un CP nb_SOM


                         def get_nb_consid(nb_SOM, cos):     ### Donne le nomre de considérant similaire par rapport au cosinus (cos)
                                                            ### et affiche tout les consiédrants similaires au CP (nb_SOM)
 

                         def find_similitudesSOM(nb_SOM, cos):   ### Cherche les considérants de principe qui sont ressemble (cos >= 0.2) à un CP nb_SOM
                

                         def find_jurisprudence(nb_SOM):      ### Cherche les CP les plus proches du CP nb_SOM


                         def get_ref_considSOM(nb_consid):    ### Renvoit les différentes informations d'un CP nb_SOM (id_xml, id_consid, text)


                         #### CONSIDERANT - ALL

                         def find_similariteALL(nb_ALL, cos):   ### Cherche les CP qui sont similaires (cos) à un considérant nb_ALL


                         def format_considALL(nb_consid):    ### Formatte les considérants d'une décision de justice
                

                         def get_ref_considALL(nb_consid):     ### Renvoi les différentes informations d'un considérant nb_consid (id_xml, id_consid, text)

                
                         #### VISAS

                         def get_visa_from_consid(nb_consid):     ### Renvoi les visas d'un considérant nb_consid
            
            
                         def format_visa(xmlfile):              ### Fromatte les visas d'une décision de justice


                         def get_visa_from_file(xmlfile):       ### Renvoi les visas d'un fichier xml


                         def process_visa(procedure, elt, xmlfile): ### Découpe en partie les visas en fonction de s'il s'agit d'un visa du CE ou des CAA
                                                                   ### Fonctionne avec get_sub_visa

                         def get_sub_visa(xmlfile, print=True):    ### Renvoi les visas, découpé par partir d'un fichier xml
                
                                                     
                         def print_visa(visa, visa_id):        ### Affiche les différentes parties des visas
                                                              ### Fonctionne avec get_sub_visa


                         #### METADONNEES

                         def format_date(date, year = True, month = False):  ### Formatte la date d'une décision de justice (récupère uniquement l'année)


                         def get_metadata(xmlfile):            ### Extrait les métadonnées d'une décision de justice


                         def get_date_ante(visa, xmlfile):     ### Récupère les dates présente dans les visas


                         def get_tokenize_juri_items(visa):    ### Récupère les références légales dans les visas


                         #### TABLEAU

                         def corpus_to_db():

                
                         def to_tab(picklefile):

                
                         def mining_visa(export=True):
                             ```''')
                             ])
                                      ])
    

    ### CALLBACKS
                     
    @app.callback(
        Output(component_id='output_consid', component_property='children'),
        Input('input_val', 'value'),
        )
    
    def output_consid(nb_consid):
        ret = []
        if nb_consid == None:
            return('')
        else:
            stock = Le.get_ref_considSOM(int(nb_consid))
            ret.append(html.P(stock[2]))
            #ret = ''.join(ret)
            return(ret)

    @app.callback(
        Output(component_id='output_text', component_property='children'),
        Input('consid_text', 'value')
        )
    
    def output_text(nb_consid):
        ret = []
        if nb_consid == None:
            return('')
        else:
            tokens = nltk.tokenize.word_tokenize(nb_consid)
            for token in tokens:
                if len(token) > 7:
                    stock = "Cela ne correspond pas à un identifiant d'un considérant"
                    ret.append(html.P(stock))
                else:
                    stock = Le.get_ref_considALL(int(token))
                    ret.append(html.P(stock[2]))
            #ret = ''.join(ret)
            return(ret)
        
    @app.callback(
        Output(component_id='output_code_pcja', component_property='children'),
        Input('input_code_pcja', 'value')
        )
    
    def output_code_pcja(nb_pcja):
        ret = []
        if nb_pcja == None:
            return('')
        else:
            stock = pcja['Description'].loc[pcja.Code == nb_pcja].values[0]
            ret.append(html.P(stock))
            #ret = ''.join(ret)
            return(ret)
    
    @app.callback(
        Output(component_id='output_code', component_property='children'),
        Input('input_code', 'value')
        )
        
    def output_code(nb_consid):
        if nb_consid == None:
            return('')
        return(AF.get_cod_by_consid(nb_consid))
    ### LAUNCH DASH SERVER

    if __name__ == '__main__':
        app.run_server(debug=True)
        
main()