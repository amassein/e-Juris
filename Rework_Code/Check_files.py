import Librairie_ejuris as Le
import pandas as pd
import pickle
import numpy as np
import re


def Check_nb_consid():
    
    count = 0
    
    doc_list = Le.corpus.fileids()
    f = open("./Result_temp/no_consid.txt", "w")
    for i, doc in enumerate(doc_list):
        if doc.endswith('.xml'):
            temp, temp_id = Le.get_considerant_part(doc)
            pat = re.compile('\w')
            if i != 0 and len(temp_id) > 0:
                print(f"\r{count}, {i} {(count/i)*100}", end="")
                count += 1
            else:
                f.write(doc+'\n')
    print("\n")
    f.close()


def Check_nb_visa():
    
    count = 0
    
    doc_list = Le.corpus.fileids()
    f = open("./Result_temp/no_visa.txt", "w")
    for i, doc in enumerate(doc_list):
        if doc.endswith('.xml'):
            visa,_ = Le.get_sub_visa(doc, print=False)
            legal_item = Le.get_tokenize_juri_items(visa)
            if i != 0:
                print(f"\r{count}, {i} {(count/i)*100}", end="")
            if legal_item == ['No Visa']:
                f.write(doc+'\n')
                continue
            else:
                count += 1
    print("\n")
    f.close()


def Check_content():

    count = 0
    
    doc_list = Le.corpus.fileids()
    f = open("./Result_temp/no_content.txt", "w")
    for i, doc in enumerate(doc_list):
        if doc.endswith('.xml'):
            content = Le.get_content(doc)
            pat = re.compile("\w")
            if i != 0:
                print(f"\r{count}, {i} {(count/i)*100}", end="")
            if content is not None and re.search(pat, content) is not None:
                count += 1
            else:
                f.write(doc+'\n')
    print("\n")
    f.close()
    
######### MAIN

#Check_nb_consid()
#Check_nb_visa()
#Check_content()

